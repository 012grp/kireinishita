## Install
```
$npm install
```

## Usage
```
$gulp
```

    
###開発環境 môi trường làm việc
- gulpを使用 dùng Gulp
- module追加時 --save-dev でpackage.jsonに khi thêm module thì thêm option --save-dev để thêm vô package.json
- task gulp/tasks/以下に機能ごと分けて追加 chia nhỏ các chức năng dưới tasks
- ディレクトリ構成 cấu trúc thư mục
    - src以下 コンパイル前のソース  trong folder src : source trước compile
    - dist コンパイル後のソース  trong dist: source sau compile
    - gulp gulpタスク    trong gulp: task của gulp
    - .babelrc babelのプリセットを指定  file .babelrc  preset của babel
    
###html
- html5  dùng html5
- プリプロセッサ:jade   pre-processor : dùng jade
- 非同期に依存する部分以外html5lintをチェックする			ngoài phần không sync thì check html5lint
- 細かくparts化し使い回す								cắt thành nhiều parts nhỏ để tái sử dụng được
- includeをwrapするblockにutillityクラスのmargin-topを使用して余白は調節	câu này hơi khó: dùng margin-top của class utility để wrap cái include của Block, còn spacing thì tùy 
chỉnh
- partialファイルはプリフィックスに_をつける					file partial thì phải đính prefix
- 出し分けなどは出来るだけjadeのif分岐やblock機能を使う		nên dùng if, block của jade khi thay đổi phần xuất dữ liệu
- phpになると思うのでこれはあくまでテンプレートのモック用		
    
###css
- プリプロセッサ:stylus			pre-pre-processor	stylus
- 設計 	thiết kế
    - flocss
    - flocssでは他レイヤー間のextendを禁止しているがfoudationのmixinからのみ許容とする	flocss thì cấm extend giữa các player , nhưng riêng mixin của foudation là ngoại lệ
    - variable	
        - color
            - 汎用的な名前とする 複数ある場合_をサフィックスに付ける	dùng tên thông dụng.
            - 名前と該当しないblockへの適用は禁止 			kg dùng tên tương ứng với block???
    - utillity        
        - text
            - utilityクラスとして扱う		dùng class utility
        - margin
            - margin-topの10~60まで10刻みでのみ使用可 例 u-mt10		margin-top 10~60 là chỉ dùng bội số của 10
            - パターンが増えたらclassを増やす							nếu lượng pattern tăng thì tăng class
        - state
            - is-プリフィックスを使用する 								dùng is-prefix 
            - state自体にはstyleを持たない							state kg có style
            - project,componentでセレクターがないjs用のstateのみをここには追加する	những state mà project, component kg có selector  thì để ở đây
- 画像	ảnh
    - spriteを使用する	dùng sprite
        - /img/src 以下にsp用画像を配置 gulp sprite コマンドで作成	/img/src thì dùng command gulp sprite để tạo
        - stylファイルは/stylus/object/component/sprite/以下に吐き出される file styl thì được tạo dưới folder sprite
        - componentレイヤーのicon.stylにimportしてコンポーネントとしてclass化       import comnponent layer vào incon.styl rồi tạo component , class
- 命名規則  rule đặt tên
    - bemを使用 	dùng bem
    - 単語が4つ以上並ぶ場合はプリフィックスで_ - をつけ省略OK		nhiều hơn 4 từ đơn thì gắn prefix_ - , rồi viết tắt ok
    - -pプリフィックスのみつけない		-p prefix là kg gắn
    
- 使用mixin	dùm mixin
    - rupture https://github.com/jescalan/rupture 

###js
- es6,babel
- 使用ライブラリ	thư viện
    - jquery
    - lodash.js
    - layzer.js
- ルール	rule
    - セレクター名はdata属性を指定する		tên selector là dùng thuộc tính "data"
    - functionの頭でセレクターの存在判定を入れる	ở đầu của function thì phải có phán định có selector hay kg
    - 基本classまたはfunction式で外部化する		cơ bản là dùng công thức?? class, function để ngoại bộ hóa??? chịu!!!!!
        - class内でセレクターを指定せずimportしたapp.jsでdomにイベントをBINDする trong class thì kg chỉ định selector, bind event trong dom của app.js
    - triggerセレクター,セレクターtarget,整数リテラル,flgなど名前空間を切ってひとまとめに定義する	trigger selector, selector target, literal , flg etc... thì dùng namespace để định nghĩa
    - es6を使用しbabelでコンパイルする		compile es6 bằng babel
    - form
        - validateはスクラッチ			validate là tạo scratch (tạo từ đầu)
        - ValidateModelでmodel部分	
        - ValidateViewでviewを実装
        - 基本ValidateModelのsubmit時のみdomのclassを参照している		cơ bản là khi submit validatemodel thì coi class của dom
        
###img
- 使用画像、svgは全て圧縮する(buildタスクで圧縮)  ảnh, svg là tất cả phải nén lại. ( dùng task build để nén)
- 命名規則 複数単語は-で繋ぐこと					cách đặt tên: gắn - vào tác từ đơn
    - 例 icon-search						ví dụ: icon-search
    
###yaml
- カテゴリー名,検索listなどconfig.yamlにて変数として持つ		tên category, search list etc.. là phải có biến số ở config.yaml
- taskはjadeのfileに入っているがjsonに吐きだす都合で更新時にsrc/jade/config.jsonを削除しjadeを更新する    	khi tạo json là phải xóa json trước rồi update jade

###開発フロー	develope flow
- bitbucketにて開発			=Bitbucket 
- 現状想定 	
    - こちらの書き出したhtmlをphpに展開		Wiz tạo html -> JFHR chuyển sang PHP
    - cssはwiz側のみ作業		CSS là chỉ có Wiz làm 
    - javascriptはes6を使うためこちらの用意したlocal環境にて開発が望ましい	dùng es6 nên  dev ở local
    - このルールは仮							rule này là tạm thời