import $               from 'jquery'
import Layzr           from 'layzr.js'
import SlickFunc       from './plugin/slick'
import textCount       from './utill/textcount'
import ua              from './utill/ua'
import _touch          from './utill/touch'
import ToggleFunc      from './utill/toggle'
import ToTopFunc       from './utill/totop'
import count_search    from './utill/count_search'
import bookmark     	 from './utill/bookmark'
import advance_search_for_ie  from './utill/advance_search_for_ie'
import google_map      from './utill/google_map'
import NumLengthFunc   from './utill/numlength'
import ScrollFunc      from './utill/scrollRightFix'
import TabFunc         from './utill/tab'
import {ValidateView,ValidateViewModal} from './utill/formValid'
import {SearchModalFunc} from './utill/modal'
import {realTimeSearch} from './utill/realTImeSearch'
import searchSubmit     	 from './utill/searchSubmit'

if($('[data-remodal-id]').length) {
	require('remodal');
	window.REMODAL_GLOBALS = {
		NAMESPACE: 'modal',
		DEFAULTS: {
			hashTracking: false
		}
	};
}

/*triggerDom*/
var trigger = trigger || {};
trigger = {
	toTop             : $('[data-trigger-totop]'),
	toggleHamburger   : $('[data-trigger-hamburgermenu]'),
	toggleQuickSearch : $('[data-trigger-quicksearch]'),
	tabSelect         : $('[data-trigger-tab]'),
	slider            : $('[data-trigger-slider]'),
	submit            : $('[data-submit]'),
	submitModal       : $('[data-submit-modal]'),
	reset             : $('[data-reset]'),
	radioname         : $('[data-trigger-radioname]'),
	modalPref         : $('[data-modal-trigger="prefmodal"]'),
	modalWayside      : $('[data-modal-trigger="waysidemodal"]'),
	location          : $('[data-trigger-location]')
};

/*targetDom*/
var target = target || {};
target = {
	toggleHamburger        : $('[data-target-hamburgermenu]'),
	toggleQuickSearch      : $('[data-target-quicksearch]'),
	textCount_2            : $('[data-target-textCount="2"]'),
	numLength_6            : $('[data-target-numcheck="6"]'),
	numLength_4            : $('[data-target-numcheck="3"]'),
	rightColumn            : $('[data-target-rightcolumn]'),
	leftColumn             : $('[data-target-leftcolumn]'),
	mainColumn             : $('[data-target-maincolumn]'),
	tabTarget              : $('[data-target-table]'),
	slider                 : $('[data-target-slider]'),
	validate               : $('[data-validate]'),
	validateMulti          : $('[data-validate-multi]'),
	validateModalRegister  : $('[data-validate-register]'),
	inputcompany           : $('[data-target-inputcompany]'),
	window                 : $(window),
	initReturn             : $('[data-return]')
};


var init = init || {};
init = {
	delay: 300,
	loadTargetPos: target.rightColumn.outerHeight() + target.rightColumn.offset().top,
	loadWrapperPos: target.mainColumn.outerHeight() + target.mainColumn.offset().top,
	rightColumnHeight: target.rightColumn.outerHeight(),
	leftColumnHeight: target.leftColumn.height(),
	loadBodyHeight: target.window.outerHeight()
};

// if( window.location.href.indexOf('404') < 0 || window.location.href.indexOf('commingsoon') < 0) {
	// console.log('hogehoge');
// }

/*newInstance*/
var ToTopFuncInstance = new ToTopFunc(init.delay),
	ToggleFuncInstanceHambuger = new ToggleFunc(init.delay,target.toggleHamburger),
	ToggleFuncInstanceQuickSearch = new ToggleFunc(init.delay,target.toggleQuickSearch),
	NumLengthFuncInstanceLong = new NumLengthFunc(target.numLength_6, 2.8, 7),
	NumLengthFuncInstanceShort = new NumLengthFunc(target.numLength_4, 1.6, 5),
	// ScrollFuncInstance = new ScrollFunc(target.rightColumn,init.loadTargetPos,init.loadWrapperPos),
	// ScrollFuncInstance = new ScrollFunc(target.rightColumn,init.loadTargetPos,init.leftColumnHeight,init.loadBodyHeight),
	// ScrollFuncInstance = new ScrollFunc(target.rightColumn,init.loadTargetPos,init.leftColumnHeight),
	TabFuncInstance = new TabFunc(trigger.tabSelect,target.tabTarget),
	ModalFuncInstance = new SearchModalFunc(),
	SearchModalFuncInstanceJob = new SearchModalFunc(),
	SearchModalFuncInstancePref = new SearchModalFunc(),
	SearchModalFuncInstanceHangup = new SearchModalFunc(),
	SearchModalFuncInstanceWayside = new SearchModalFunc();

/*layzer*/
var instance = Layzr();
instance.on('src:after', function(element) {
	if(!element.hasAttribute('data-layzr-bg')) return;
	var srcValue = element.getAttribute('data-layzr-bg');
	element.removeAttribute('src');
	element.style.backgroundImage = `url(${srcValue})`;
});
const layzerFnc = () => {
	instance
		.update()
		.check()
		.handlers(true);
};

/*eventBind*/
var toTopEvt = () => {
	if(!trigger.toTop.length) return;
	trigger.toTop.on(_touch,() => {
		ToTopFuncInstance.toTop();
	});
};
var toggleEvt = () => {
	trigger.toggleHamburger.on(_touch, () => {
		ToggleFuncInstanceHambuger.slide();
		if(target.toggleQuickSearch.is(':visible')) {
			ToggleFuncInstanceQuickSearch.slideUp();
		}
	});
	trigger.toggleQuickSearch.on(_touch, () => {
		ToggleFuncInstanceQuickSearch.slide();
		if(target.toggleHamburger.is(':visible')) {
			ToggleFuncInstanceHambuger.slideUp();
		}
	});
	target.window.on('scroll',() => {
		if(target.toggleQuickSearch.is(':visible')) {
			ToggleFuncInstanceQuickSearch.slideUp();
		}
		if(target.toggleHamburger.is(':visible')) {
			ToggleFuncInstanceHambuger.slideUp();
		}
	})
};
var numLengthEvt = () => {
	NumLengthFuncInstanceLong.sizeChange();
	if(ua.Tablet) {
		NumLengthFuncInstanceLong.tabletSizeChange();
	}
	NumLengthFuncInstanceShort.sizeChange();
};
var scrollEvt = () => {
	// console.log($(`[data-target-leftcolumn]`).height());
	// console.log(init.leftColumnHeight);
	if(!target.rightColumn.length || ua.Tablet) return;
	if(init.rightColumnHeight < init.leftColumnHeight) {
		var ScrollFuncInstance = new ScrollFunc(target.rightColumn,init.loadTargetPos,$('[data-target-leftcolumn]').height());
		// console.log();
		target.window.on('scroll', () => {
			var y = target.window.scrollTop() + target.window.height();
			ScrollFuncInstance.setEvent(y)
		});
	}
};
var tabEvt = () => {
	if(!trigger.tabSelect) return;
	trigger.tabSelect.on(_touch,() => {
		if($(this).hasClass('is-active')) return;
		TabFuncInstance.itelatorFunc('is-active','is-disable');
		//scrollFunc初期化
		ScrollFuncInstance.init(target.mainColumn.outerHeight() + target.mainColumn.offset().top);
	});
};

//slick用
var secretSlider = () => {
	if(!target.slider.length) return;
	SlickFunc();
	/*slick plugin*/
	target.slider.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '[data-trigger-slider]'
	});
	trigger.slider.slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '[data-target-slider]',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		vertical: true
	});
};

var formValidEvt = function(){
	if(!target.validate.length) return;
	var formViewFunc;
	target.validate.each(function() {
		formViewFunc = new ValidateView(this);
	});
	target.validateMulti.each(function(){
		formViewFunc.multipleValid($(this),'select');
	});
	trigger.submit.on(_touch,function(e){
		if($('[data-trigger-radioname="name"]').prop('checked')) {
			target.inputcompany.addClass('valid');
		}
		formViewFunc.submit($('[data-validate]'),e);
		target.validateMulti.each(function(){
			formViewFunc.onSubmitMultipleValid($(this),'select');
		});
	});
	trigger.reset.on(_touch,function(){
		formViewFunc.reset($('[data-validate="target"]'));
	});
	if($('[data-trigger-radioname="company"]').prop('checked')){
		target.inputcompany.show();
	};
	trigger.radioname.on('change',function(){
		if(!$('[data-trigger-radioname="company"]').prop('checked')) {
			target.inputcompany.hide();
			target.inputcompany.val("");
		}else{
			target.inputcompany.show();
			target.inputcompany.removeClass('valid');
		}
	});
};

var formValidEvtRegistration = function(){
	if(!target.validateModalRegister.length) return;
	var formViewModalFunc;
	target.validateModalRegister.each(function(){
		formViewModalFunc = new ValidateViewModal(this);
	});
	trigger.submitModal.on(_touch,function(e){
		formViewModalFunc.submit(target.validateModalRegister,e);
	});
};

//@todo searchModalFuncにmearge
var modalEvt = () => {
	ModalFuncInstance.delegateModalEvt('on','jobmodal');
	ModalFuncInstance.delegateModalEvt('on','hangupmodal');
	trigger.location.on('change',function(){
		if($(this).val() == '') {
			ModalFuncInstance.delegateModalEvt('off','prefmodal');
			ModalFuncInstance.delegateModalEvt('off','waysidemodal');
			trigger.modalPref.addClass('is-disable');
			trigger.modalWayside.addClass('is-disable');
		}
		//else
		//{
		// 	ModalFuncInstance.delegateModalEvt('on','prefmodal');
		// 	ModalFuncInstance.delegateModalEvt('on','waysidemodal');
		// 	trigger.modalPref.removeClass('is-disable');
		// 	trigger.modalWayside.removeClass('is-disable');
		// }
	});
	if(trigger.location.val() != ''){
		ModalFuncInstance.delegateModalEvt('on','prefmodal');
		ModalFuncInstance.delegateModalEvt('on','waysidemodal');
		SearchModalFuncInstanceWayside.init('wayside',realTimeSearch);
		SearchModalFuncInstancePref.init('pref',realTimeSearch);
		//remove disable of city button
		trigger.modalPref.removeClass('is-disable');
	}
};

var searchModalEvt = () => {
	SearchModalFuncInstanceJob.init('job',realTimeSearch);
	SearchModalFuncInstanceHangup.init('hangup',realTimeSearch);
};

//@todo リファクター from Vietnam
//Get values of checked waysite from a input hidden tag

//Change waysidemodal checkboxes of prefmodal (沿線・駅)
//Get waysite when province changed
//And add checkboxes to modal


//var __addWaysites = function (_value, _text, _request) {
var __addWaysites = function (value) {
	var url = '/train-api/line-station/' + value;
	$.ajax({
		'url': url,
		'dataType': 'json'
	}).then(function (lines) {
		var index = 0;
		var html = '';
		for (var key in lines) {
			var value = lines[key],
				prefNames =  value.name,
				prefCode  =  value.code,
				stations = value.stations;
			var prefNamesTemp = `<input form="searchForm" value="${prefCode}" type="checkbox" name="way[]" id="${prefCode}" data-num="${index}" class="search-checkbox waysite-checkbox">` +
				`<label for="${prefCode}" class="search-checkbox__label--modal waysite-checkbox__subTtl u-mt10">${prefNames}</label>`;
			html += prefNamesTemp + '<div class="waysite-checkbox__station" data-target-station="target">';
			index++;
			for (var key in stations) {
				var stationValue = stations[key],
					stationNames = stationValue.name,
					stationCode = stationValue.code;
				var stationTemp = `<input form="searchForm" value="${stationCode}" type="checkbox" name="station[]" id="${stationCode}" data-num="${index}" class="search-checkbox waysite-checkbox">` +
					`<label for="${stationCode}" class="search-checkbox__label--modal waysite-checkbox__sub u-mt10">${stationNames}</label>`;
				html += stationTemp;
				index++;
			}
			html += '</div>';
		}
		$('#waysite_checkboxes').html(html);
	}).then(function () {
		// modalPrefWayEvt();
		ModalFuncInstance.delegateModalEvt('on','waysidemodal');
		trigger.modalWayside.removeClass('is-disable');
		setTimeout(function () {
			SearchModalFuncInstanceWayside.init('wayside',realTimeSearch);
		});
	}).done(function () {
		//find selected value and set checked for checkbox waysite and station after click "件を検索"
		var waysite = $("#requestWaysite").val();
		var station = $("#requestStation").val();
		if(waysite)
		{
			var arrWaysite = waysite.split(",");
			arrWaysite = arrWaysite.filter(__array_unique);
			for(var i in arrWaysite)
			{
				$('input[name="way[]"][value="' + arrWaysite[i] +'"]').prop("checked", true);
			}
			$("#requestWaysite").val("");
		}
		
		
		if(station)
		{
			var arrStation = station.split(",");
			arrStation = arrStation.filter(__array_unique);
			for(var j in arrStation)
			{
				$('input[name="station[]"][value="' + arrStation[j] +'"]').prop("checked", true);
			}
			$("#requestStation").val("");
		}
	});
};

//unique values in an array
var __array_unique = function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
};

var __checkbox_template = function (index, id, name, value, text, className, isChecked) {
	return '<input ' + (isChecked ? 'checked' : '') + ' form="searchForm" value="' + value + '" type="checkbox" name="' + name + '" id="' + id + '" data-num="' + index + '" class="search-checkbox ' + className + '">' +
		'<label for="' + id + '" class="search-checkbox__label--modal u-mt10">' + text + '</label>'
};
//Get citys when province changed
//And add checkboxes to modal
var __addCities = function (_value, _text) {
	var url = '/area/city-by-province?province_id=' + _value;
	$.ajax({
		'url': url,
		'dataType': 'json'
	}).then(function (jsonData) {
		var html = '';
		for (var i in jsonData) {
			html += __checkbox_template(i, 'city' + jsonData[i].id, 'city[]', jsonData[i].id, jsonData[i].name, 'city-checkbox', false);
		}
		$('#city_checkboxes').html(html);
	}).done(function(){
		ModalFuncInstance.delegateModalEvt('on','prefmodal');
		trigger.modalPref.removeClass('is-disable');
		setTimeout(function () {
			SearchModalFuncInstancePref.init('pref',realTimeSearch);
		});
	});
};

var __addCitiesSelect = function (_value) {
	var url = '/area/city-by-province?province_id=' + _value;
	$.ajax({
		'url': url,
		'dataType': 'json'
	}).done(function (jsonData) {
		var html = `<option value="">市区町村</option>`;
		for (var i in jsonData) {
			/* 
			    edited at 2017-10-04 by Bui Huy Binh 
			    check if exist variable cityId (cityId is passed from view) => set selected city
			*/
			if(typeof cityId !== 'undefined' && jsonData[i].id == cityId) {
				html += `<option value="${jsonData[i].id}" id="${jsonData[i].id}" selected>${jsonData[i].name}</option>`
			}else{
				html += `<option value="${jsonData[i].id}" id="${jsonData[i].id}">${jsonData[i].name}</option>`
			}
		}
		$('#city_checkboxes').html(html);
	});
};

var selectAjaxEvt = () => {
	if(!trigger.modalPref.length && !trigger.modalWayside.length) return;

	var __requestWaysite = $('#requestWaysite').val() || "";
	__requestWaysite = __requestWaysite == "" ? [] : __requestWaysite.split(",");

	//Checkbox template
	//ready: if province not empty will add waysite into modal
	if ($('#pro_select').val() != '') {
		// __addWaysites($('#pro_select').val(), '', __requestWaysite);
		__addWaysites($('#pro_select').val());
	}
	//event change of province select tag
	//Change checkboxes of city and waysite modal
	trigger.location.change(function () {
		if (trigger.location.val() == '') {
			$('[data-target-addtag="pref"]').empty();
			$('[data-target-addtag="wayside"]').empty();
			return;
		}
		trigger.modalPref.addClass('is-disable');
		trigger.modalWayside.addClass('is-disable');

		/*
			edited at 2016/11/04 by Bui Huy Binh
			reason: dual Event made sending Ajax so many timnes
			delete event _touch after change #pro_select
		*/
		$('[data-target-addtag="pref"]').off(_touch);
		$('[data-target-addtag="wayside"]').off(_touch);
		$(document).off('closing', '[data-search-id=wayside]');
		$(document).off('closing', '[data-search-id=pref]');

		__addCities(this.value, '');
		// __addWaysites(this.value, '', __requestWaysite);
		__addWaysites(this.value);

	});
};

var selectAjaxCitiesEvt = function(){
	if(!$('#pro_select_cities').length) return;
	if($('#pro_select_cities').val() != ''){
		__addCitiesSelect($('#pro_select_cities').val());
	}
	$('#pro_select_cities').change(function () {
		__addCitiesSelect(this.value);
	})
};

var textHeightEvt = function(){
	if(!$('.-wide__list__detail--multiline').length) return;
	var $target =  $('.-wide__list__detail--multiline'),
		minH = 22,
		maxH = minH * 2;
	// var isIE11 = (ua.indexOf('trident/7') > -1);
	// var isEdge = (ua.indexOf('edge') > -1);
	$target.each(function(){
		var targetHeight = $(this).height();
		if(targetHeight <= minH) {
			// $(this).css('height',minH + 'px');
			$(this).height('22px');
			//ieだけ効かない
		}else if(targetHeight > minH){
			$(this).height('44px');
		}
	});
};

var jobResultTagEvt = function(){
	var $targetTag  = $('._list__multiTag__block');
	$targetTag.each(function(){
		var len = $(this).children().length;
		if(!len > 0) return;
			$(this).show();
			$(this).prev().css('width','465px');
	});
};

//実行
$(function(){
	textCount(target.textCount_2,2);
	numLengthEvt();
	toTopEvt();
	toggleEvt();
	// scrollEvt();
	layzerFnc();
	tabEvt();
	secretSlider();
	modalEvt();
	formValidEvt();
	formValidEvtRegistration();
	searchModalEvt();
	selectAjaxEvt();
	selectAjaxCitiesEvt();
	jobResultTagEvt();
});

$(document).ready(function(){
	textHeightEvt();
	scrollEvt();
	
	//Vietnam Js, edited at 2016-10-20 by Banh Tran Xuong
	//Import from src/Util/count_search.js
	count_search();
	//Advance search submit
	searchSubmit();
	//Import from src/Util/advance_search_for_ie.js
	advance_search_for_ie();
	//Import from src/Util/bookmark.js
	var _bookmark = new bookmark();
	_bookmark.add_event_for_bookmark_job();
	_bookmark.check_login_to_load_bookmark(_bookmark.bookmark_load_event());
});

//Vietnam Js, edited at 2016-10-19 by Trinh Manh Cuong


//Import from src/Util/advance_search.js
var _google_map = new google_map();