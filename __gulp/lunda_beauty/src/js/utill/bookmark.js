 /******START Bookmark JOB JS
  * date 18/Oct/2016
  * Bookmark for secret_kyujin
  * update by Trinh Manh Cuong
  * 
  * default do 2 actions in app.js:
  * 1. add_event_for_bookmark_job()
  * 2. check_login_to_load_bookmark(bookmark_load_event);
  * **/

 import $ from 'jquery'
 export default class bookmark {
	constructor() {
		this.isBookmarkPage = window.location.pathname == '/mybox' ? true : false;
	}

	load_event_action(url, _button) {
		$.get(url, function (data) {
			if (data == 1) {
				_button.innerHTML = '削除する';
			}
		});
	}
	 
	//create click event for a button
	click_event_action(button) {
		var _this = this;
		$(button).on('click', function () {
			var id = $(this).data("id");
			var isSecretJob = $(this).data("secret");

			if (id && isSecretJob != null) {
				var url = '/account/bookmark?id=' + id + '&is_secret_job=' + isSecretJob;
				$.get(url, function (result) {
					console.log(typeof result);
					switch(result) {
						//un-login
						case "-1":
							//go to log-in page
							window.location = '/account/';
							break;
							
						//no bookmark
						case "0":
							
							//remove job if this is bookmark page
							if (_this.isBookmarkPage) {
								$(button).parents('.column-box--wide__list').remove();
								var totalJobBookmark = parseInt($('#totalJobBookmark').text());
								totalJobBookmark -= 1;
								$('#totalJobBookmark').text(totalJobBookmark);
							} else {
								button.innerHTML = '<span class="c-font-icon--attachment"></span>キープする';
							}
							
							break;
							
						//bookmarked
						case "1":
							button.innerHTML = '削除する';
							break;
							
						default:
							alert('Something went wrong please try again!');
							break;
					}
				});
				
			} else {
				alert('Button is invalid');
			}
		});
	}


	//declare click event
	//data-id. data-is-secret
	//ajax 0,1,-1
	//-1 -> login
	//0 -> bookmark => unbookmark
	//1 -> unbookmark => bookmark
	//-2 -> message error
	add_event_for_bookmark_job(){
		var _this = this;
		$('.bookmark_job').each(function () {
			_this.click_event_action(this);
		});
	}


	//load STATUS of bookmark buttons when loading page
	//without checking log-in
	bookmark_load_event() {
		//if not mybook
		//if login
		//selector ...
		//if bookmarking => chang to delete
		var _this = this;
		var _next = 1;
		$('.bookmark_job').each(function (index, element) {
			var id = $(element).data("id");
			var isSecretJob = $(this).data("secret");
			if (id && isSecretJob != null) {
				var url = '/account/check-bookmark?id=' + id + '&is_secret_job=' + isSecretJob;
				setTimeout(function(){
										_this.load_event_action(url, element)
									 },
						   _next);
				_next = _next + 120;
			} else {
				alert('Button is invalid');
			}

		});

	}

	//checking log-in or not and CALL a function if logged-in
	//ex: check_login_to_load_bookmark(bookmark_load_event);
	check_login_to_load_bookmark(callback) {
		if (callback !== undefined && $('.bookmark_job').length) {
			var path = window.location.pathname;
			var _url = '/account/check-login-to-load-bookmark?path=' + path;
			$.get(_url, function (result) {
				if (result == 1) {
					callback();
				}
			})
		}
	}



	
	/******END Bookmark JOB JS****/

 }