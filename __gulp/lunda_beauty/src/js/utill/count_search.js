import $ from 'jquery'
var count_search = function () {
	/******START COUNT SEARCH JS
	 * date 18/Oct/2016
	 * job number statistics
	 * update by TRINH MANH CUONG
	 * ****/
	if ($('#search_keyword').length) {
		/*Get values to make URL*/
		var __makeUrl = function () {

			//checkboxs
			var uri = [];
			var checkedUnique = [];
			var textboxKeyword = $('#search_keyword');
			var selectProvince = $('#pro_select');

			if (textboxKeyword.val() != '') {
				uri.push('k=' + encodeURIComponent(textboxKeyword.val()));
			}

			if (selectProvince.val() != '') {
				uri.push('pro=' + selectProvince.val());
			}
			$(".search-checkbox:checked").each(function (index, el) {
				var par = encodeURIComponent(el.name)+'='+el.value;
				if($.inArray(par, checkedUnique) === -1)
				{
					checkedUnique.push(par);
					uri.push(par);
				}
			});

			var url = '/job/ajax-count-search?' + uri.join('&');

			return url;
		}

		/*Count function*/
		var __count = function () {
			var url = __makeUrl();
			$('#btnAdvanceSubmit').val('...件を検索');
			$.get(url, function (numberResult) {
				$('#btnAdvanceSubmit').val(numberResult + '件を検索');
			});
		}

		var employmentCheckboxes = $('.em-checkbox');
		var closeModalButtons = $('.close_modal_button');

		/*Events*/
		var eventFunc = function (obj, action) {
			obj.on(action, function () {
				__count();
			});
		};

		//When done press keyword
		$('#search_keyword').length &&
			$('#search_keyword').on("blur", function () {
				__count();
			});

		//When select 都道府県
		$('#pro_select').length &&
			eventFunc($('#pro_select'), 'change');

		// //When close modal: 職種, こだわり, 市区町村の選択, 沿線・駅の選択
		// closeModalButtons.each(function () {
		// 	eventFunc($(this), 'click');
		// })

		//When checkbox 雇用形態
		employmentCheckboxes.each(function () {
			eventFunc($(this), 'change');
		});
	}
	/******END COUNT SEARCH JS****/
}

export default count_search;