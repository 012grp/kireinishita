import $ from 'jquery'
export default class google_map {
    constructor(){
        this.func();
    }

    func(){
        /******START library google map JS
         * date 18/Oct/2016
         * load google map
         * update by Trinh Manh Cuong
         * ****/
        function initMap( latVal, lngVal, inforwindow, zoom, id) {
            var x = latVal =! '' && latVal != undefined ? latVal: 0;
            var y = lngVal =! '' && lngVal != undefined ? lngVal: 0;
            var location = {lat: x, lng: y};

            //
            var map = new google.maps.Map(document.getElementById(id), {
                zoom: zoom,
                center: location
            });

            var contentString = inforwindow;

            var marker = new google.maps.Marker({
                position: location,
                map: map,
                title: ''
            });

            if(contentString)
            {
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });


                google.maps.event.addListener(marker, 'mouseover', function () {
                    infowindow.open(map, marker);
                });

                infowindow.open(map, marker);
            }
        }

        $(document).ready(function(){
            // set google map
            // input lat, lng , inforwindow, zoom

            //map for content
            if($("#lat").length && $("#lng").length)
            {
                var lat = parseFloat($("#lat").val()) || null ;
                var lng = parseFloat($("#lng").val()) || null ;
                var company_name = ($("#company_name").val()) ? $("#company_name").val() : null ;
                var map_location = ($("#map_location").val()) ? $("#map_location").val() : null ;
                if(lat && lng)
                {
                    var contentString = '<div style="display: inline-block;">';
                    //check if exists company name
                    if(company_name){
                        contentString += "<h4 style='font-weight: bold; color: black;'>"+company_name+"</h4>";
                    }
                    if(map_location){
                        contentString += "<p>"+map_location+"</p>";
                    }
                    var googleLink = '<a target="_blank" style="color:#427fed" href="http://maps.google.com/maps?q='+lat+','+lng+'">Googleマップで見る</a>';
                    contentString += googleLink;
                    contentString += '</div>';
                    initMap( lat, lng, contentString, 14, 'map');
                }

            }

            //map for secret
            if($("#job_lat").length && $("#job_lng").length)
            {
                var job_lat = parseFloat($("#job_lat").val()) || null ;
                var job_lng = parseFloat($("#job_lng").val()) || null ;
                var google_map_company_name = ($("#google_map_company_name").val()) ? $("#google_map_company_name").val() : null ;
                var job_location = ($("#job_location").val()) ? $("#job_location").val() : null ;

                if(job_lat && job_lng)
                {
                    var contentString = '<div style="display: inline-block;">';
                    //check if exists company name
                    if(google_map_company_name){
                        contentString += "<h4 style='font-weight: bold; color: black;'>"+google_map_company_name+"</h4>";
                    }

                    if(job_location){
                        contentString += "<p>"+job_location+"</p>";
                    }
                    var googleLink = '<a target="_blank" style="color:#427fed" href="http://maps.google.com/maps?q='+job_lat+','+job_lng+'">Googleマップで見る</a>';
                    contentString += googleLink;
                    contentString += '</div>';
                    initMap( job_lat, job_lng, contentString, 14, 'job_map');
                }

            }


            //map for company
            if($("#company_lat").length && $("#company_lng").length)
            {

                var company_lat = parseFloat($("#company_lat").val()) || null ;
                var company_lng = parseFloat($("#company_lng").val()) || null ;
                var address_com = ($("#address_com").val()) ? $("#address_com").val() : null ;
                if(company_lat && company_lng)
                {
                    var contentString = '<div style="display: inline-block;">';
                    //check if exists company name
                    if(google_map_company_name){
                        contentString += "<h4 style='font-weight: bold; color: black;'>"+google_map_company_name+"</h4>";
                    }

                    if(address_com){
                        contentString += "<p>"+address_com+"</p>";
                    }

                    var googleLink = '<a target="_blank" style="color:#427fed" href="http://maps.google.com/maps?q='+company_lat+','+company_lng+'">Googleマップで見る</a>';
                    contentString += googleLink;
                    contentString += '</div>';

                    initMap( company_lat, company_lng, contentString, 14, 'company_map');
                }


            }

        });
        /******END  library google map JS ****/
    }
}