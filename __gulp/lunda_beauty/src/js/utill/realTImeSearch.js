import $ from 'jquery'
//リアルタイム検索値
/*Ajax function*/
var __getAjax = function (url, callback, callback_error) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            try {
                callback(xhttp.responseText);
            } catch (e) {
                console.log(e);
            }
        }
        else
        {
            if(callback_error !== undefined)
            {
                callback_error(xhttp.status);
            }
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
};

/*Get values to make URL*/
/* 
    edited at 2016-11-03 by Bui Huy Binh 
    change javascript to jquery
*/
var __makeUrl = function () {

    //checkboxs
    var uri = [];
	var checkedUnique = [];
    var textboxKeyword = $('.search-table__text');
    var selectProvince = $('#pro_select');
    var checkboxes = $(".search-checkbox:checked");

    if(textboxKeyword.val() != '')
    {
        uri.push('k='+encodeURIComponent(textboxKeyword.val()));
    }

    if(selectProvince.val() != '')
    {
        uri.push('pro='+selectProvince.val());
    }

    checkboxes.each(function (index, el) {
			var par = encodeURIComponent(el.name)+'='+el.value;
			if($.inArray(par, checkedUnique) === -1)
			{
				checkedUnique.push(par);
				uri.push(par);
			}
	});

    var url = '/job/ajax-count-search?'+uri.join('&');

    return url;
};

/*Count function*/
export function realTimeSearch() {
    var url = __makeUrl();

    __getAjax(url,
        function (numberResult) {
            $('#btnAdvanceSubmit').val(numberResult + '件を検索');
        },
        function() {
            $('#btnAdvanceSubmit').val('...件を検索');
        });
};

export function realTimeSearchBind(obj, action) {
    obj.addEventListener(action, function(){
        __count();
    });
};