/*
	Search Submit: control sent parametters before submit
	-> Remove duplicated values
*/
import $ from 'jquery'
var searchSubmit = function(){
	$('form[data-search="advance"]').submit(function (e) {
		//checkboxs
		var uri = [];
		var checkedUnique = [];
		var textboxKeyword = $('.search-table__text');
		var selectProvince = $('#pro_select');
		var checkboxes = $(".search-checkbox:checked");
		
		if(textboxKeyword.val() != '')
		{
			uri.push('k='+encodeURIComponent(textboxKeyword.val()));
		}

		if(selectProvince.val() != '')
		{
			uri.push('pro='+selectProvince.val());
		}
		checkboxes.each(function (index, el) {
			var par = encodeURIComponent(el.name)+'='+el.value;
			if($.inArray(par, checkedUnique) === -1)
			{
				checkedUnique.push(par);
				uri.push(par);
			}
		});
		var action = $(this).attr('action') || '/form/result';
		var url = action+'?'+uri.join('&');
		window.location = url;
		e.preventDefault();
	});
}

export default searchSubmit;