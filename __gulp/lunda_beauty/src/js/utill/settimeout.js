import $ from 'jquery'

export default function setTimeOut(delay){
	var defer_time = $.Deferred();
	setTimeout(function() {
		defer_time.resolve();
	}, delay);
	return defer_time.promise();
}

