import $ from 'jquery'
export default class ToTopFunc {
    constructor(delay){
        this.delay = delay;
    }
    toTop(){
        $('body,html').animate({
            scrollTop: 0
        }, this.delay);
    }
}
