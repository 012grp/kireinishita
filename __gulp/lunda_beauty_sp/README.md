## Install
```
$npm install
```

## Usage
```
$gulp
```

    
###開発環境
- gulpを使用
- module追加時 --save-dev でpackage.jsonに
- task gulp/tasks/以下に機能ごと分けて追加
- ディレクトリ構成
    - src以下 コンパイル前のソース
    - dist コンパイル後のソース
    - gulp gulpタスク
    - .babelrc babelのプリセットを指定
    
###html
- html5
- プリプロセッサ:jade
- 非同期に依存する部分以外html5lintをチェックする
- 細かくparts化し使い回す
- includeをwrapするblockにutillityクラスのmargin-topを使用して余白は調節
- partialファイルはプリフィックスに_をつける
- 出し分けなどは出来るだけjadeのif分岐やblock機能を使う
- phpになると思うのでこれはあくまでテンプレートのモック用
    
###css
- プリプロセッサ:stylus
- 設計
    - flocss
    - flocssでは他レイヤー間のextendを禁止しているがfoudationのmixinからのみ許容とする
    - variable
        - color
            - 汎用的な名前とする 複数ある場合_をサフィックスに付ける
            - 名前と該当しないblockへの適用は禁止
    - utillity        
        - text
            - utilityクラスとして扱う
        - margin
            - margin-topの10~60まで10刻みでのみ使用可 例 u-mt10
            - パターンが増えたらclassを増やす
        - state
            - is-プリフィックスを使用する 
            - state自体にはstyleを持たない
            - project,componentでセレクターがないjs用のstateのみをここには追加する
- 画像
    - spriteを使用する
        - /img/src 以下にsp用画像を配置 gulp sprite コマンドで作成
        - stylファイルは/stylus/object/component/sprite/以下に吐き出される
        - componentレイヤーのicon.stylにimportしてコンポーネントとしてclass化      
- 命名規則 
    - bemを使用
    - 単語が4つ以上並ぶ場合はプリフィックスで_ - をつけ省略OK
    - -pプリフィックスのみつけない
    
- 使用mixin
    - rupture https://github.com/jescalan/rupture

###js
- es6,babel
- 使用ライブラリ
    - jquery
    - lodash.js
    - layzer.js
- ルール
    - セレクター名はdata属性を指定する
    - functionの頭でセレクターの存在判定を入れる
    - 基本classまたはfunction式で外部化する
        - class内でセレクターを指定せずimportしたapp.jsでdomにイベントをBINDする
    - triggerセレクター,セレクターtarget,整数リテラル,flgなど名前空間を切ってひとまとめに定義する
    - es6を使用しbabelでコンパイルする
    - form
        - validateはスクラッチ
        - ValidateModelでmodel部分
        - ValidateViewでviewを実装
        - 基本ValidateModelのsubmit時のみdomのclassを参照している
        
###img
- 使用画像、svgは全て圧縮する(buildタスクで圧縮)
- 命名規則 複数単語は-で繋ぐこと
    - 例 icon-search
    
###yaml
- カテゴリー名,検索listなどconfig.yamlにて変数として持つ
- taskはjadeのfileに入っているがjsonに吐きだす都合で更新時にsrc/jade/config.jsonを削除しjadeを更新する

###開発フロー
- bitbucketにて開発
- 現状想定 
    - こちらの書き出したhtmlをphpに展開
    - cssはwiz側のみ作業
    - javascriptはes6を使うためこちらの用意したlocal環境にて開発が望ましい
    - このルールは仮