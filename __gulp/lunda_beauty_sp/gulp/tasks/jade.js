import gulp    from 'gulp';
import jade    from 'gulp-jade';
import notify  from 'gulp-notify';
import plumber from 'gulp-plumber';
import paths   from '../config';
import data    from 'gulp-data';
import yaml    from 'gulp-yaml';

gulp.task('yaml', function() {
	return gulp.src('./*y{,a}ml')
		.pipe(yaml()).on('error', console.log)
		.pipe(gulp.dest('./src/jade/'));
});

gulp.task('jade',['yaml'], function() {
	return gulp.src(paths.jade)
		.pipe(data(function(file) {
			return require('../../src/jade/config.json');
		}))
		.pipe(jade({
			pretty: true
		}))
		.pipe(plumber({
			errorHandler: notify.onError('Error: <%= error.message %>')
		}))
		.pipe(gulp.dest(paths.dist_dir));
});


