import $               from 'jquery'
import Layzr           from 'layzr.js'
import SlickFunc       from './plugin/slick'
import _touch          from './utill/touch'
import TabFunc         from './utill/tab'
import bookmark        from './utill/bookmark'
import googlemap       from './utill/google_map'
import countsearch     from './utill/count_search'
import moment          from 'moment'
import {ValidateView,ValidateViewModal} from './utill/formValid'
import SearchModalFunc from './utill/modal'
import {realTimeSearch} from './utill/count_search'
// import bookmark_click_event from './utill/keepAction'
require('remodal');

import setTimeOut      from './utill/settimeout'

if($('[data-remodal-id]').length) {
	window.REMODAL_GLOBALS = {
		NAMESPACE: 'modal',
		DEFAULTS: {
			hashTracking: false
		}
	};
}

/*layzer*/
var instance = Layzr();
instance.on('src:after', function(element) {
	if(!element.hasAttribute('data-layzr-bg')) return;
	var srcValue = element.getAttribute('data-layzr-bg');
	element.removeAttribute('src');
	element.style.backgroundImage = `url(${srcValue})`;
});
const layzerFnc = () => {
	instance
		.update()
		.check()
		.handlers(true);
};

/*triggerDom*/
var trigger = trigger || {};
trigger = {
	toggleHamburger   : $('[data-trigger-hamburgermenu]'),
	toggleQuickSearch : $('[data-trigger-quicksearch]'),
	tabSelect         : $('[data-trigger-tab]'),
	slider            : $('[data-trigger-slider]'),
	submit            : $('[data-submit]'),
    submitModal       : $('[data-submit-modal]'),
	submitModalOther  : $('[data-submit-modalother]'),
	reset             : $('[data-reset]'),
	radioname         : $('[data-trigger-radioname]'),
	modalPref         : $('[data-modal-trigger="prefmodal"]'),
	modalWayside      : $('[data-modal-trigger="waysidemodal"]'),
	location          : $('[data-trigger-location]')
};

/*targetDom*/
var target = target || {};
target = {
	toggleHamburger        : $('[data-target-hamburgermenu]'),
	rightColumn            : $('[data-target-rightcolumn]'),
	leftColumn             : $('[data-target-leftcolumn]'),
	tabTarget              : $('[data-target-tabinner]'),
	slider                 : $('[data-target-slider]'),
	validate               : $('[data-validate]'),
	validateMulti          : $('[data-validate-multi]'),
	validateCheck          : $('[data-validate-check]'),
	validateModalRegister  : $('[data-validate-register]'),
	validateModalPassword  : $('[data-validate-password]'),
	insert                 : $('[data-target-insert]'),
	inputcompany           : $('[data-target-inputcompany]'),
	window                 : $(window)
};

var TabFuncInstance = new TabFunc(trigger.tabSelect,target.tabTarget),
	ModalFuncInstance = new SearchModalFunc(),
	SearchModalFuncInstancePref = new SearchModalFunc(),
	SearchModalFuncInstanceWayside = new SearchModalFunc();

//$('[data-target-close]')
var headerFunc = function(){
	var scrollNum = 0;

	var add = function() {
		$('[data-target-close]').addClass('is-open');
		$('[data-target-wrap] a').addClass('is-noClick');
		$('[data-target-wrap] form').addClass('is-noClick');
	};
	
	var remove = function() {
		$('[data-target-close]').removeClass('is-open');
		var promise = setTimeOut(200);
		promise.done( () => {
			$('[data-target-wrap] a').removeClass('is-noClick');
			$('[data-target-wrap] form').removeClass('is-noClick');
		});
	};

	trigger.toggleHamburger.on(_touch,function(){
		$('[data-target-wrap]').addClass('is-open');
		$('[data-target-hamburgermenu]').addClass('is-open');
		add();
	});
	$('[data-target-close]').on(_touch,function(){
		$('[data-target-wrap]').removeClass('is-open');
		$('[data-target-hamburgermenu]').removeClass('is-open');
		remove();
	});

	target.window.on('scroll',() => {
		scrollNum = target.window.scrollTop();
		$('[data-target-searchmenu]').css('top', scrollNum);
	});

	trigger.toggleQuickSearch.on(_touch,function(){
		$('[data-target-wrap]').addClass('is-open--left');
		$('[data-target-searchmenu]').addClass('is-open');
		target.window.on('touchmove.noScroll', function(e) {e.preventDefault();});
		add();
	});
	$('[data-target-close]').on(_touch,function(){
		target.window.off('.noScroll');
		$('[data-target-wrap] ').removeClass('is-open--left');
		$('[data-target-searchmenu]').removeClass('is-open');
		remove();
	});
};

var tabEvt = () => {
	if(!trigger.tabSelect) return;
	trigger.tabSelect.on(_touch,() => {
		if($(this).hasClass('is-active')) return;
		TabFuncInstance.itelatorFunc('is-active','is-disable');
	});
};

var formValidEvt = function(){
	if(!target.validate.length) return;
	var formViewFunc;
	target.validate.each(function() {
		formViewFunc = new ValidateView(this);
	});
	target.validateMulti.each(function(){
		formViewFunc.multipleValid($(this),'select');
	});
	trigger.submit.on(_touch,function(e){
		// formViewFunc.submit($('[data-validate]'),e);
		if($('[data-trigger-radioname="name"]').prop('checked')) {
			target.inputcompany.addClass('valid');
		}
		formViewFunc.submit(target.validate,e);
		target.validateMulti.each(function(){
			formViewFunc.onSubmitMultipleValid($(this),'select');
		});
	});
	trigger.reset.on(_touch,function(){
		formViewFunc.reset($('[data-validate="target"]'));
	});
	if($('[data-trigger-radioname="company"]').prop('checked')){
		target.inputcompany.show();
	};
	trigger.radioname.on('change',function(){//ここ外でvalidateするしかない
		if(!$('[data-trigger-radioname="company"]').prop('checked')) {
			target.inputcompany.hide();
			target.inputcompany.val("");
		}else{
			target.inputcompany.show();
			target.inputcompany.removeClass('valid');
		}
	});
};

var formValidEvtRegistration = function(){
	if(!target.validateModalRegister.length) return;
	var formViewModalFunc;
	target.validateModalRegister.each(function(){
		formViewModalFunc = new ValidateViewModal(this);
	});
    trigger.submitModal.on(_touch,function(e){
        formViewModalFunc.submit(target.validateModalRegister,e);
    });
};

var formValidEvtPassword = function(){
	if(!target.validateModalPassword.length) return;
	var formViewModalFunc;
	target.validateModalPassword.each(function(){
		formViewModalFunc = new ValidateViewModal(this);
	});
	trigger.submitModalOther.on(_touch,function(e){
		formViewModalFunc.submit(target.validateModalPassword,e);
	});
};

//slick用
var secretSlider = () => {
	if(!target.slider.length) return;
	SlickFunc();
	/*slick plugin*/
	target.slider.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		swipe: true
	});
};

//@todo 使用しないなら削除
var browserBackEvt = function(){
	if(!trigger.location.val()) return;
	SearchModalFuncInstancePref.init('pref');
};


//modalEvtに追加すればいい
var modalEvt = () => {
	ModalFuncInstance.delegateModalEvt('on','login');
	ModalFuncInstance.delegateModalEvt('on','password');
	ModalFuncInstance.delegateModalEvt('on','registration');
	// ModalFuncInstance.delegateModalEvt('on','complete');

	var modalFunc = function() {
		if(trigger.location.val() == '') {
			ModalFuncInstance.delegateModalEvt('off','prefmodal');
			ModalFuncInstance.delegateModalEvt('off','waysidemodal');
			trigger.modalPref.addClass('is-disable');
			trigger.modalWayside.addClass('is-disable');
		}
		// else{
		// 	ModalFuncInstance.delegateModalEvt('on','prefmodal');
		// 	ModalFuncInstance.delegateModalEvt('on','waysidemodal');
		// 	trigger.modalPref.removeClass('is-disable');
		// 	trigger.modalWayside.removeClass('is-disable');
		// }
	};
	
	if(trigger.location.val() != ''){
		ModalFuncInstance.delegateModalEvt('on','prefmodal');
		ModalFuncInstance.delegateModalEvt('on','waysidemodal');
		SearchModalFuncInstanceWayside.init('wayside',realTimeSearch);
		SearchModalFuncInstancePref.init('pref',realTimeSearch);
	}
	
	$(document).on('opened', '.waysidemodal', function () {
		if($('.remodal-overlay').is(':hidden')){
			$('.remodal-overlay').css('display','block');
		}
	});
	trigger.location.on('change',function(){
		modalFunc();
	});
	if(!trigger.location.val()) return;
	modalFunc();
};

//@todo リファクター from

/*
	Edited at 2016-10-18 03:00
	by Banh Tran Xuong
	reason: change Javascript to Jquery
*/

//Get values of checked waysite from a input hidden tag
//Change waysidemodal checkboxes of prefmodal (沿線・駅)
//Get waysite when province changed
//And add checkboxes to modal


var __addWaysites = function (value) {
	//var url = '/data/train.json'; //local
	var url = 'http://i-eat.jellyfishtech.tk/train-api/line-station/' + value;
	$.ajax({
		'url': url,
		'dataType': 'json'
	}).then(function (lines) {
		var index = 0,
			html = '';
		for (var key in lines) {
			var value = lines[key],
				prefNames =  value.name,
				prefCode  =  value.code,
				stations = value.stations;
			var prefNamesTemp = `<div class="waysite-checkbox__ttl">	` +
				`<input form="searchForm" value="${prefCode}" type="checkbox" name="way[]" id="${prefCode}" data-num="${index}" class="search-checkbox waysite-checkbox">` +
				`<label for="${prefCode}" class="search-checkbox__label--modal waysite-checkbox__subTtl">${prefNames}</label>` +
				`<span data-trigger-stations="${key}" class="waysite-checkbox__btn u-fs--xxs">駅選択</span></div>`;
			html += prefNamesTemp + `<div data-target-stations="${key}" class="waysite-checkbox__station">`;
			index++;
			for (var key in stations) {
				var stationValue = stations[key],
					stationNames = stationValue.name,
					stationCode = stationValue.code;
				var stationTemp = `<input form="searchForm" value="${stationCode}" type="checkbox" name="station[]" id="${stationCode}" data-num="${index}" class="search-checkbox waysite-checkbox">` +
					`<label for="${stationCode}" class="search-checkbox__label--modal waysite-checkbox__sub u-mt10">${stationNames}</label>`;
				html += stationTemp;
				index++;
			}
			html += '</div>';
		}
		$('#waysite_checkboxes').html(html);
	}).done(function () {
		ModalFuncInstance.delegateModalEvt('on','waysidemodal');
		trigger.modalWayside.removeClass('is-disable');
		setTimeout(function () {
			/* 
			    edited at 2016-11-08 by Bui Huy Binh
			    add param realTimeSearch in init function
			*/
			SearchModalFuncInstanceWayside.init('wayside', realTimeSearch);
		});
	}).done(function () {
		//find selected value and set checked for checkbox waysite and station after click "件を検索"
		var waysite = $("#requestWaysite").val();
		var station = $("#requestStation").val();
		if(waysite)
		{
			var arrWaysite = waysite.split(",");
			arrWaysite = arrWaysite.filter(__array_unique);
			for(var i in arrWaysite)
			{
				$('input[name="way[]"][value="' + arrWaysite[i] +'"]').prop("checked", true);
			}
			$("#requestWaysite").val("");
		}
		
		
		if(station)
		{
			var arrStation = station.split(",");
			arrStation = arrStation.filter(__array_unique);
			for(var j in arrStation)
			{
				$('input[name="station[]"][value="' + arrStation[j] +'"]').prop("checked", true);
			}
			$("#requestStation").val("");
		}
	});

	var delegateStationToggle = () => {
		$('#waysite_checkboxes').on(_touch,['data-trigger-stations'],function(e){
			var targetCode = $(e.target).attr('data-trigger-stations');
			if(targetCode == undefined) return;
			var $target = $(`[data-target-stations=${targetCode}]`);
			$target.toggle();
		});
	};
	delegateStationToggle();
};

//unique values in an array
var __array_unique = function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
};

var __checkbox_template = function (index, id, name, value, text, className, isChecked) {
	return '<input ' + (isChecked ? 'checked' : '') + ' form="searchForm" value="' + value + '" type="checkbox" name="' + name + '" id="' + id + '" data-num="' + index + '" class="search-checkbox ' + className + '">' +
		'<label for="' + id + '" class="search-checkbox__label--modal u-mt10">' + text + '</label>'
};
//Get citys when province changed
//And add checkboxes to modal
var __addCities = function (_value, _text) {
	var url = '/area/city-by-province?province_id=' + _value;
	//var url = '/data/test.json'; //local
	$.ajax({
		'url': url,
		'dataType': 'json'
	}).then(function (jsonData) {
		var html = '';
		for (var i in jsonData) {
			html += __checkbox_template(i, 'city' + jsonData[i].id, 'city[]', jsonData[i].id, jsonData[i].name, 'city-checkbox', false);
		}
		$('#city_checkboxes').html(html);
	}).done(function(){
		ModalFuncInstance.delegateModalEvt('on','prefmodal');
		trigger.modalPref.removeClass('is-disable');
		setTimeout(function () {
			/* 
			    edited at 2016-11-08 by Bui Huy Binh
			    add param realTimeSearch in init function
			*/
			SearchModalFuncInstancePref.init('pref', realTimeSearch);
		});
	});
};

var __addCitiesSelect = function (_value) {
	var url = '/area/city-by-province?province_id=' + _value;
	//var url = '/data/test.json'; //local
	$.ajax({
		'url': url,
		'dataType': 'json'
	}).done(function (jsonData) {
		var html = `<option value="">市区町村</option>`;
		for (var i in jsonData) {
			/* 
			    edited at 2017-10-04 by Bui Huy Binh 
			    check if exist variable cityId (cityId is passed from view) => set selected city
			*/
			if(typeof cityId !== 'undefined' && jsonData[i].id == cityId) {
				html += `<option value="${jsonData[i].id}" id="${jsonData[i].id}" selected>${jsonData[i].name}</option>`
			}else{
				html += `<option value="${jsonData[i].id}" id="${jsonData[i].id}">${jsonData[i].name}</option>`
			}
		}
		$('#city_checkboxes').html(html);
	});
};


var selectAjaxEvt = () => {
	if(!trigger.modalPref.length && !trigger.modalWayside.length) return;

	var __requestWaysite =$('#requestWaysite').val() || "";
	__requestWaysite = __requestWaysite == "" ? [] : __requestWaysite.split(",");

	//Checkbox template
	//ready: if province not empty will add waysite into modal
	if (document.getElementById('pro_select').value != '') {
		// __addWaysites(document.getElementById('pro_select').value, '', __requestWaysite);
		__addWaysites($('#pro_select').val());
	}
	//event change of province select tag
	//Change checkboxes of city and waysite modal
	trigger.location.change(function () {
		if (trigger.location.val() == '') {
			$('[data-target-addtag="pref"]').empty();
			$('[data-target-addtag="wayside"]').empty();
			return;
		}
		trigger.modalPref.addClass('is-disable');
		trigger.modalWayside.addClass('is-disable');
		/*
			edited at 2016/11/04 by Bui Huy Binh
			reason: dual Event made sending Ajax so many timnes
			delete event _touch after change #pro_select
		*/
		$('[data-target-addtag="pref"]').off(_touch);
		$('[data-target-addtag="wayside"]').off(_touch);

		/*
			edited at 2016/11/09 by Bui Huy Binh
			delete event _touch after change #pro_select
		*/
		$('#waysite_checkboxes').off(_touch);

		__addCities(this.value, '');
		// __addWaysites(this.value, '', __requestWaysite);
		__addWaysites(this.value);
	});
};

var selectAjaxCitiesEvt = function(){
	if(!$('#pro_select_cities').length) return;

	if($('#pro_select_cities').val() != ''){
		__addCitiesSelect($('#pro_select_cities').val());
	}
	$('#pro_select_cities').change(function () {
		__addCitiesSelect(this.value);
	});
};

//localstrage
//user 結果使ってない
var insertEvt = function(){
	var isStrage = localStorage.date,
		now = moment(),
		outputNow = now.format('YYYYMMDDHH');
	var screenFade = function() {
		target.insert.addClass('is-fade');
		target.insert.on('animationend webkitAnimationEnd oAnimationEnd mozAnimationEnd',function(){
			$(this).addClass('is-disable');
		});
		localStorage.date = outputNow;
	};
	if(isStrage == undefined) {
		screenFade();
	}else{
		var a = outputNow - localStorage.date;
		if(a > 24) {
			screenFade();
		}else{
			target.insert.addClass('is-disable');
		}
	}
};


//Vietnam js
/*
	Edited at 2016-10-18 03:13
	by Banh Tran Xuong
	reason: change native Javascript to Jquery
*/
var isBookmarkPage = window.location.pathname == '/mybox'?true:false;

function click_event_action(buttons)
{
	$(buttons).click(function(){

		
		var _button = $(buttons);
		var id = $(this).data("id");
		var isSecretJob = $(this).data("secret");
				
		if(id != null && isSecretJob != null)
		{
			var url = '/account/bookmark?id=' + id + '&is_secret_job=' +isSecretJob;
			$.get(url, function (result) {
				if(result == -1)
				{
					ModalFuncInstance.openModalEvt('login');
				}
				else //logged
				{
					if(result == 0) //no bookmark
					{
						//remove job if this is bookmark page
						if(isBookmarkPage)
						{
							try{
								_button.parents('.column-box--wide__list ').remove();
								var totalJobBookmark = parseInt($('#totalJobBookmark').text());
								totalJobBookmark -= 1;
								$('#totalJobBookmark').text(totalJobBookmark);
							}
							catch(e)
							{
								//if edit this HTML structure, it will go to here
							}
						}
						else
						{
							_button.html('<span class="c-font-icon--attachment"></span>キープする');
						}
					}
					else if(result == 1) //bookmarked
					{
						_button.html('削除する');
					}
					else
					{
						// alert('Something went wrong please try again!');
					}
				}
			});
		}
		else
		{
			// alert('Button is invalid');
		}
	});
}

function bookmark_click_event() {
	//click
	//data-id. data-is-secret
	//ajax 0,1,-1
	//-1 -> login
	//0 -> bookmark => unbookmark
	//1 -> unbookmark => bookmark
	//-2 -> message error
	if(!$('.bookmark_job')) return;
	$('.bookmark_job').each(function () {
		click_event_action(this);
	})

}

$('[data-remodal-id="password"]').remodal().close();
//account.js

/*
 Login
 => Success close pop-up
 */

var accountFunc = function() {
	if ($('#btnLogin')) {
		$('#btnLogin').click(function () {
			var email = $('#txtEmail').val();
			var password = $('#txtPassword').val();

			var url = '/account/ajax-login?email=' + email + '&password=' + password;
			$.get(url, function (result) {
				if (result == "1") {
					//success
					$('#btnCloseLoginModal').click();
					$('#naxBoxTop').html('<div class="c-button-square--s" id="btnAuthen"><a href="/mybox">お気に入り BOX</a></div>');
				}
				else {
					//error
					$('#alertLoginMessage').css('display','block');
				}
			})
		});
	}
	
	/*
	 Register
	 => Success close pop-up
	 */
	if ($('#btnRegister')) {
		$('#btnRegister').click(function () {
			var email = $('#txtRegisterEmail').val();
			var password = $('#txtRegisterPassword').val();

			var url = '/account/ajax-register?email=' + email + '&password=' + password;
			$.get(url, function (result) {
				if (result == "1") {
					//success
					$('#btnCloseRegisterModal').click();
					$('#naxBoxTop').html('<div class="c-button-square--s" id="btnAuthen"><a href="/mybox">お気に入り BOX</a></div>');
				}
				else {
					//error
					$('#alertRegisterMessage').html(result).css('display','block');
				}
			})
		});
	}
	
	/*
	 Forgot password
	 => Success close pop-up
	 */
	if ($('#btnForgetPassword')) {
		$('#btnForgetPassword').click(function () {
			var email = $('#txtUserEmail').val();

			var url = '/account/ajax-forgot-password?email=' + email;
			$.get(url, function (result) {
				if (result == "1") {
					//success
					//message => 'リセット確認メールが送信されました'
					$('formForgetPassword').remove();
					$('alertForgetPasswordSendMail').css('display',"block");
					$('[data-remodal-id="password"]').remodal().close();
				}
				else {
					//error
					$('#alertForgetPasswordMessage').html(result).css('display','block');
				}
			});
		});
	}
	// document.forms["formRegister"].onSubmit = function (evt) {
	// 	evt.preventDefault();
	// };
	/*
	 Log-out
	 => Success close pop-up
	 => change to log-in button
	 */
	if($('#btnLogout'))
	{
		$('#btnLogout').click(function () {

			//if log-in => log-out
			$.get('/account/ajax-logout', function (result) {
				window.location = "/";
			})
		});
	}
	
	// document.forms["formRegister"].onsubmit = function (evt) {
	// 	evt.preventDefault();
	// };
	$(document).ready(function() {
		$(document.forms["formRegister"]).submit(function(evt) {
			evt.preventDefault();
		});
	});

};



//実行
$(function(){
	headerFunc();
	tabEvt();
	modalEvt();
	layzerFnc();
	secretSlider();
	formValidEvt();
    formValidEvtRegistration();
	formValidEvtPassword();
	selectAjaxEvt();
	selectAjaxCitiesEvt();
	// insertEvt();
	bookmark_click_event();
	accountFunc();
	
	//Vietnam Js, edited at 2016-10-19 by Trinh Manh Cuong, Banh Tran Xuong
	//Import from src/Util/count_search.js
	//@TODO 一時的にコメントアウト
	countsearch();
	//Import from src/Util/bookmark.js
	var _bookmark = new bookmark();
	_bookmark.check_login_to_load_bookmark(_bookmark.bookmark_load_event());
});



//Import from src/Util/google_map.js
var _google_map = new googlemap();