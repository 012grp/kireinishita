/*
	Name: Bookmark
	Features: change キープする to 削除する if user logged-in and the job's checked before
	
	Default to do:
	check_login_to_load_bookmark(bookmark_load_event);
*/
import $ from 'jquery'
export default class Custom {
	/*
	 Edited at 2016-10-19
	 by Trinh Manh Cuong 
	*/

	//set STATUS of a button bookmark
	load_event_action(url, _button) {
		$.get(url, function (data) {
			if (data == 1) {
				_button.html('削除する');
			}
		});
	}
	
	//set all STATUS of buttons bookmark
	bookmark_load_event() {
		//if not mybook
		//if login
		//selector ...
		//if bookmarking => chuyen sang delete
		var _this = this;
		var buttons = $('.bookmark_job');
		$('.bookmark_job').each(function () {
			var id = $(this).data("id");
			var isSecretJob = $(this).data("secret");

			if (id != null && isSecretJob != null) {
				var url = '/account/check-bookmark?id=' + id + '&is_secret_job=' + isSecretJob;
				_this.load_event_action(url, $(this));
			} else {
				alert('Button is invalid');
			}
		});
	}
	
	//check log-in before call function
	check_login_to_load_bookmark(callback) {
		if (callback !== undefined && $('.bookmark_job').length) {
			var path = window.location.pathname;
			var _url = '/account/check-login-to-load-bookmark?path=' + path;
			$.get(_url, function (result) {
				if (result == 1) {
					callback();
				}
			});
		}
	}
}