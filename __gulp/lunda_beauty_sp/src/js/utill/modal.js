import $ from 'jquery'
import _touch from './touch'
import setTimeOut from './settimeout'

export default class SearchModalFunc {
	/* 
	    edited at 2016-11-08 by Bui Huy Binh 
	    add param func to init functio
	    delete comment //this.func = func; in init function
	*/
	init(target, func){
		this.$targetModal = $(`[data-search-id=${target}]`);
		this.$targetList = this.$targetModal.find('input');
		this.$addTagDom = $(`[data-target-addtag=${target}]`);
		this.checklist = {};
		// this.flg = target;
		/* 
		    edited at 2016-11-08 by Bui Huy Binh 
		    move this.targetString and this.func above this.reset because after call function addTag()
		    targetString was be remove
		*/
		this.targetString = target;
		this.func = func; //@TODO initの引数で渡す処理 pcではrealTimeSearch

		this.reset();
		this.addTag();
		this.eventTag();
	}
	delegateModalEvt(state,namespace) {
		if(state == 'on') {
			$(`[data-modal-trigger=${namespace}]`).on(_touch + `.${namespace}`, () => {
				var prefModal = $(`[data-remodal-id=${namespace}]`).remodal();
				var promise = setTimeOut(700);
				promise.done( () => {
					prefModal.open();
				});
			});
		}else{
			$(`[data-modal-trigger=${namespace}]`).off(_touch + `.${namespace}`);
		}
	}
	openModalEvt(namespace){
		var prefModal = $(`[data-remodal-id=${namespace}]`).remodal();
		prefModal.open();
	}
	checkselectFunc(target,len) {
		var flg = target;
		if(flg == 'prefmodal') {
			$('[data-modal-trigger="waysidemodal"]').on(_touch,(e) =>{
				this.resetTag('prefmodal');
				this.func(e);
			});
		}else if(flg == 'waysidemodal') {
			$('[data-modal-trigger="prefmodal"]').on(_touch,(e) =>{
				this.resetTag('waysidemodal');
				this.func(e);
			});
		}else{
			return;
		}
	}
	addTag(){
		var self = this;
		var appendTagFunc = function(target) {
			var datanum = target.data('num'),
				checkText = target.next('label').text();
			// self.checklist[datanum] = checkText;
			if(self.targetString != 'wayside') {
				self.checklist[datanum] = checkText;
				if(target.prop('checked') == false) {
					delete self.checklist[datanum];
				}
			}else{
				var dataId = target.attr('id');
				self.checklist[dataId] = checkText;
				if(target.prop('checked') == false) {
					delete self.checklist[dataId];
				}
			}

			self.reset();
			self.checkselectFunc(target.parents('[data-remodal-id]').attr('data-remodal-id'));

			for(var key in self.checklist){
				var innerText = self.checklist[key];
				self.$addTagDom.append(`<span data-num='${key}' class='c-tag--detail--table'>${innerText}</span>`);
			}
		};
		//@todo
		this.$targetList.each(function(){
			$(this).on('change',(e) => {
				appendTagFunc($(this));
				if(self.targetString != 'wayside') return;
				var $target = $(e.target),
					targetId = $target.attr('id'),
					checkFlg = $target.prop('checked');
				self.$targetList.each(function(){
					if($(this).attr('id') == targetId && checkFlg) {
						$(this).prop('checked',true);
					}else if($(this).attr('id') == targetId && !checkFlg) {
						$(this).prop('checked',false);
					}
				});
			});
			if($(this).prop('checked') == true){//load時
				appendTagFunc($(this));
			}
		});
	}
	/*deletyagevent*/
	eventTag(){
		this.$addTagDom.on(_touch, $('[data-num]'), (e) => {
			/* 
			    edited at 2016-11-08 by Bui Huy Binh
			    check if click outside tag is return 
			*/
			if($(e.target).data('num') == undefined)
			{
				return;
			}

			if(this.targetString != 'wayside') {
				var eventTargetNum = $(e.target).data('num'),
					$target = this.$targetModal.find($(`[data-num=${eventTargetNum}]`));
				$target.prop('checked', false);
				delete this.checklist[eventTargetNum];
				$(e.target).remove();
				this.checkselectFunc(this.targetModal);
				/* 
				    edited at 2016-11-08 by Bui Huy Binh
				    call this.func(e) to count job after remove tag (call __count function in count_search.js)
				*/
				this.func(e);
			}else{
				var eventTargetNum = $(e.target).data('num');

				this.$targetList.each(function() {
					if($(this).attr('id') == eventTargetNum) {
						$(this).prop('checked', false);
					}
				});
				delete this.checklist[eventTargetNum];
				$(e.target).remove();
				this.checkselectFunc(this.targetModal);
				/* 
				    edited at 2016-11-08 by Bui Huy Binh
				    call this.func(e) to count job after remove tag (call __count function in count_search.js)
				*/
				this.func(e);
			}
		});
	}
	resetTag(targetModal){
		this.checklist = {};
		this.reset();
		var $targetResetCheck = $(`[data-remodal-id='${targetModal}']`).find('input');
		$targetResetCheck.each(function(){
			$(this).prop('checked', false);
		});
	}
	reset(){
		this.$addTagDom.empty();
	}
}



