export default class NumLengthFunc {
    constructor(target,fontSize,digit){
        this.$target = target;
        this.fontSize = fontSize + 'rem';
        this.digit = digit;
    }
    sizeChange(){
        if(this.$target.text().length < this.digit) return;
        this.$target.css('font-size', this.fontSize);
    }
    tabletSizeChange() {
        this.$target.css('font-size', this.fontSize);
    }
}



