import $ from 'jquery'
export default class ToggleFunc {
    constructor(delay,target){
        this.target = target;
        this.delay = delay;
    }
    slide(){
        if(!this.target.length) return;
        this.target.slideToggle(this.delay,() => {
            $(this).queue([]);
            $(this).stop();
        });
    }
    slideUp(){
        if(!this.target.length) return;
        this.target.slideUp(this.delay,() => {
            $(this).queue([]);
            $(this).stop();
        });
    }
}
