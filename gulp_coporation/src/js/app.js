import $ from 'jquery';
import jQuery from 'jquery';
// export for others scripts to use
window.$ = $;
window.jQuery = jQuery;
import RwdTable   from './plugin/jquery.rwd-tables'

$(document).ready(function(){

  $('select').change(function(){
    if($(this).val()==="") {
      $(this).css('background-color', '#f5f5f5');
    }else{
      $(this).css('background-color', '#fff');
    }
  });

  $('.select_with_icon').change(function(){
    if($(this).val()==="") {
      $(this).css('background-color', '#f5f5f5');
      $(this).css('color', '#7e7d7d');
    }else{
      if($(this).val()==1){
        $(this).css('color', '#006bb3');
      }else if($(this).val()==2){
        $(this).css('color', '#07ab46');
      }else if($(this).val()==3){
        $(this).css('color', '#d04950');
      }else{
        $(this).css('color', '#7e7d7d');
      }
      $(this).css('background-color', '#fff');
    }
  });


  $( ".rwd-tables table, .table.rwd-tables" ).rwdTables();

  $('.dropdown-toggle').on("click", function(e){
    $(this).next('ul.dropdown-menu').toggle();
    if($(this).hasClass('open-menu')){
      $(this).removeClass('open-menu');
    }else{
      $(this).addClass('open-menu');
    }
    e.stopPropagation();
    e.preventDefault();
  });

  $(".nav--hamburger").click(function () {
    if($('#nav-box-mobile').hasClass('is-open')){
      $('#nav-box-mobile').removeClass('is-open');
    }else{
      $('#nav-box-mobile').addClass('is-open');
    }
    if ($(".nav-box-mobile").css("right") != "0px") {
      $(".nav-box-mobile").animate({
        right: 0
      }, 0);
    }else{
      $(".nav-box-mobile").animate({
        right: -270
      }, 0);
    }
    $("body").append("<div class='overlay'></div>");
    $(".overlay").click(function(e){
      if(e.target.className !== "nav-box-mobile")
      {
        $(".nav-box-mobile").animate({
          right: -270
        }, 100);
        $('#nav-box-mobile').removeClass('is-open');
        $( ".overlay" ).remove();
      }
    });
  });
  $("._btn_close_nav_box").click(function (e) {
    if(!$(e.target).is('#nav-box-mobile')) {
      $(".nav-box-mobile").animate({
        right: -270
      }, 100);
      $('#nav-box-mobile').removeClass('is-open');
      $( ".overlay" ).remove();
    }

  });




  // show more notification
  var $ShowHideMore = $('.box_notificate__ul');
  $ShowHideMore.each(function() {
    var $times = $(this).children('li');
    if ($times.length > 3) {
      $ShowHideMore.children(':nth-of-type(n+4)').addClass('moreShown').hide();
      $('span.btn_show_more').addClass('more-times').html('すべて表示');
      $ShowHideMore.addClass('more');
    }
  });

  $(document).on('click', '.btn_show_more', function() {
    var that = $(this);
    if (that.hasClass('more-times')) {
      $(".box_notificate__ul").find('.moreShown').show();
      that.addClass('less-times').removeClass('more-times').html('閉じる');
      $ShowHideMore.addClass('less').removeClass('more');
    } else {
      $(".box_notificate__ul").find('.moreShown').hide();
      that.removeClass('less-times').addClass('more-times').html('すべて表示');
      $ShowHideMore.addClass('more').removeClass('less');
    }
  });



  if($("#gulp").length){

    $('.popup-trigger, .btn_search_mobile  a').click(function(e) {
      e.stopPropagation();
      if($(window).width() < 769) {
        // $(this).after( $( ".popup" ) );
        $('.popup').show().addClass('popup-mobile');
        $("body").append("<div class='overlay'></div>");
        $("body").addClass('open-popup');
      } else {
        $('.popup').removeClass('popup-mobile').show();
        $("body").append("<div class='overlay'></div>");
        $("body").addClass('open-popup');
      };
    });


  }else{

    $('.btn_search_mobile  a').click(function(e) {
      e.stopPropagation();
      if($(window).width() < 769) {
        // $(this).after( $( ".popup" ) );
        $('.popup').show().addClass('popup-mobile');
        $("body").append("<div class='overlay'></div>");
        $("body").addClass('open-popup');
      } else {
        $('.popup').removeClass('popup-mobile').show();
        $("body").append("<div class='overlay'></div>");
        $("body").addClass('open-popup');
      };
    });


    //rate
    $(".btn_update_rate").click(function(e) {
      e.preventDefault();
      $('.btn_update_rate').removeClass('active');
      $(this).addClass('active');
      $("#rate").val($(this).val());
    });

    $("#btn_save").click(function(e) {
      e.preventDefault();
      var id_entry = $('#id_entry').val();
      var id_rate = $('#rate').val();
      var status = $( ".select-status option:selected" ).val();
      var ok = 1;
      if(status=="" || status == undefined){
        alert('この項目は必須です。');
        var ok = 0;
      }
      if(ok==1){
        $.ajax({
          type: "POST",
          url: "/corporation/company/update_rate/",
          data: {
            id: id_rate,
            id_entry: id_entry,
            id_status: status
          },
          dataType: 'json',
          success: function(result) {
            //alert('ok');
            //window.location.reload();
            $('#span_rate').html(result.rate);
            $('#span_status').html(result.status_span);
            if($(window).width() < 769) {
              $('.popup').show().addClass('popup-mobile');
              $("body").append("<div class='overlay'></div>");
              $("body").addClass('open-popup');
            } else {
              $('.popup').removeClass('popup-mobile').show();
              $("body").append("<div class='overlay'></div>");
              $("body").addClass('open-popup');
            };
          },
          error: function(result) {
            alert('error');
          }
        });

      }

      
    });

  }




  $('.popup-btn-close').click(function(e){
    $('.popup').hide();
    $( ".overlay" ).remove();
    $("body").removeClass('open-popup');
  });

});



