/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jellyfish.it.crawler4j.kirenisurunda.basic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.sleepycat.je.txn.LockerFactory;
import com.jellyfish.it.crawler4j.crawler.CrawlConfig;
import com.jellyfish.it.crawler4j.crawler.CrawlController;
import com.jellyfish.it.crawler4j.crawler.Page;
import com.jellyfish.it.crawler4j.crawler.WebCrawler;
import com.jellyfish.it.crawler4j.crawler.exceptions.ContentFetchException;
import com.jellyfish.it.crawler4j.crawler.exceptions.PageBiggerThanMaxSizeException;
import com.jellyfish.it.crawler4j.crawler.exceptions.ParseException;
import com.jellyfish.it.crawler4j.crawler.exceptions.RedirectException;
import com.jellyfish.it.crawler4j.fetcher.PageFetchResult;
import com.jellyfish.it.crawler4j.fetcher.PageFetcher;
import com.jellyfish.it.crawler4j.parser.HtmlParseData;
import com.jellyfish.it.crawler4j.parser.NotAllowedContentException;
import com.jellyfish.it.crawler4j.parser.ParseData;
import com.jellyfish.it.crawler4j.robotstxt.RobotstxtConfig;
import com.jellyfish.it.crawler4j.robotstxt.RobotstxtServer;
import com.jellyfish.it.crawler4j.storage.MysqlCrawler;
import com.jellyfish.it.crawler4j.url.WebURL;

import org.apache.http.HttpStatus;
import org.apache.http.impl.EnglishReasonPhraseCatalog;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import uk.org.lidalia.slf4jext.Level;

/**
 * @author Yasser Ganjisaffar [lastname at gmail dot com]
 */
public class JellyfishCrawlSiteMainController {
	private static FileHandler fh = null;
	private static Logger logger = LoggerFactory
			.getLogger(JellyfishCrawlSiteMainController.class);
	private static PageFetcher pageFetcher;
	public static String pathXmlFile = "./src/main/java/com/jellyfish/it/crawler4j/kirenisurunda/config/JellyfishCrawlConfig.xml";
	/* declare read config pagenumber end */
	private static String totalProductSelect = "";
	private static int totalProductIndexBegin = -1;
	private static int totalProductIndexEnd = -1;
	private static String numberProductInPageSelect = "";
	private static String regex = "";
	private static int regexFromIndexBegin = -1;
	private static int regexFromIndexEnd = -1;
	private static int regexToIndexBegin = -1;
	private static int regexToIndexEnd = -1;
	private static int totalProductPosition = -1;
	private static String totalProductSelectPosition = "";
	private static int numberProductInPagePosition = -1;
	private static String numberProductInPageSelectPosition = "";
	private static int timeOut = 30000;
	private static String decimalFormatTotalProduct = "";
	/* declare total job and total job new */
	public static int totalJob = 0;
	public static int totalJobNew = 0;
	// more
	// public static int totalJobNewSuccess = 0;
	// public static int totalJobUpdateAll = 0;
	// public static int totalJobUpdateAllSuccess = 0;
	// public static int totalJobUpdate_UpdateDate = 0;
	// public static int totalJobUpdate_UpdateDate_Success = 0;

	private static int site_id = -1;
	/*
	 * customize crawler ieat - move data from contents to contents_crawl
	 */
	private static String tagConnection = "databaseConnection";
	private static String tagInfoEmail = "InformationEmailConfig";
	private static String host = "";
	private static String port = "";
	private static String dbName = "";
	private static String dbUser = "";
	private static String dbPwd = "";

	/* declare send email */
	private static String host_email;
	private static String port_email;
	private static String fromEmailAddress;
	private static String passwordFromEmailAddesss;
	private static String toEmailAddess;
	private static String subject;
	private static String urlSendEmail;
	private static String bodySelect = "";

	/* process send email */
	public static void sendEmail(String host, String port,
			final String fromAddress, final String password, String toAddress,
			String subject, String message) throws AddressException,
			MessagingException {

		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromAddress, password);
			}
		};

		Session session = Session.getInstance(properties, auth);

		// creates a new e-mail message
		Message msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(fromAddress));
		InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
		msg.setRecipients(Message.RecipientType.TO, toAddresses);
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		// set plain text message
		msg.setContent(message, "text/html");

		// sends the e-mail
		Transport.send(msg);

	}

	public static Boolean checkWebSite(URL url) {
		HttpURLConnection connection;
		int code = 0;
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			code = connection.getResponseCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (code == 200) {
			return true;
		} else {
			return false;
		}

	}

	public static void ReadConfigPageNumberEnd(org.w3c.dom.Element eElement) {
		
		bodySelect = eElement
				.getElementsByTagName("bodySelect").item(0)
				.getTextContent();
		totalProductSelect = eElement
				.getElementsByTagName("TotalProductSelect").item(0)
				.getTextContent();
		if (!eElement.getElementsByTagName("TotalProductIndexBegin").item(0)
				.getTextContent().isEmpty()) {
			totalProductIndexBegin = Integer.parseInt(eElement
					.getElementsByTagName("TotalProductIndexBegin").item(0)
					.getTextContent());
		}
		if (!eElement.getElementsByTagName("TotalProductIndexEnd").item(0)
				.getTextContent().isEmpty()) {
			totalProductIndexEnd = Integer.parseInt(eElement
					.getElementsByTagName("TotalProductIndexEnd").item(0)
					.getTextContent());
		}
		numberProductInPageSelect = eElement
				.getElementsByTagName("NumberProductInPageSelect").item(0)
				.getTextContent();
		if (!eElement.getElementsByTagName("TotalProductPosition").item(0)
				.getTextContent().isEmpty()) {
			totalProductPosition = Integer.parseInt(eElement
					.getElementsByTagName("TotalProductPosition").item(0)
					.getTextContent());
		}
		totalProductSelectPosition = eElement
				.getElementsByTagName("TotalProductSelectPosition").item(0)
				.getTextContent();
		// regex=
		// eElement.getElementsByTagName("regex").item(0).getTextContent();
		if (!eElement.getElementsByTagName("regexFromIndexBegin").item(0)
				.getTextContent().isEmpty()) {
			regexFromIndexBegin = Integer.parseInt(eElement
					.getElementsByTagName("regexFromIndexBegin").item(0)
					.getTextContent());
		}
		if (!eElement.getElementsByTagName("regexFromIndexEnd").item(0)
				.getTextContent().isEmpty()) {
			regexFromIndexEnd = Integer.parseInt(eElement
					.getElementsByTagName("regexFromIndexEnd").item(0)
					.getTextContent());
		}
		if (!eElement.getElementsByTagName("regexToIndexBegin").item(0)
				.getTextContent().isEmpty()) {
			regexToIndexBegin = Integer.parseInt(eElement
					.getElementsByTagName("regexToIndexBegin").item(0)
					.getTextContent());
		}
		if (!eElement.getElementsByTagName("regexToIndexEnd").item(0)
				.getTextContent().isEmpty()) {
			regexToIndexEnd = Integer.parseInt(eElement
					.getElementsByTagName("regexToIndexEnd").item(0)
					.getTextContent());
		}
		if (!eElement.getElementsByTagName("NumberProductInPagePosition")
				.item(0).getTextContent().isEmpty()) {
			numberProductInPagePosition = Integer.parseInt(eElement
					.getElementsByTagName("NumberProductInPagePosition")
					.item(0).getTextContent());
		}
		numberProductInPageSelectPosition = eElement
				.getElementsByTagName("NumberProductInPageSelectPosition")
				.item(0).getTextContent();
		decimalFormatTotalProduct = eElement
				.getElementsByTagName("DecimalFormatTotalProduct").item(0)
				.getTextContent();
	}

	public static void resetConfigPageNumberEnd() {
		totalProductSelect = "";
		totalProductIndexBegin = -1;
		totalProductIndexEnd = -1;
		numberProductInPageSelect = "";
		regex = "";
		regexFromIndexBegin = -1;
		regexFromIndexEnd = -1;
		regexToIndexBegin = -1;
		regexToIndexEnd = -1;
		totalProductPosition = -1;
		totalProductSelectPosition = "";
		numberProductInPagePosition = -1;
		numberProductInPageSelectPosition = "";
		decimalFormatTotalProduct = "";
	}

	public static Boolean tryParseIntByString(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}
	
	public static Boolean checkTotalJobForSite6(String url){
		org.jsoup.nodes.Document doc = MysqlCrawler.convertUrlToDocument(url);
		Element body = doc.body();
		Elements listDetail = body.select(bodySelect);
		if(listDetail.size() > 0)
			return true;
		return false;
	}

	public static Boolean checkUrlRedirect(String url) {
		try {

			org.jsoup.Connection.Response response = Jsoup.connect(url)
			// enable for error urls
					.ignoreHttpErrors(true).followRedirects(false)
					// MAXIMUN TIME
					.timeout(30000)
					// This is to prevent producing garbage by attempting to
					// parse a JPEG binary image
					.ignoreContentType(true).execute();
			int status_code = response.statusCode();
			if (status_code != 200) {
				return true;
				// is redirect
			} else {
				return false;
			}

		} catch (SocketTimeoutException se) {
			System.out.println("getContentOnly: SocketTimeoutException");
			System.out.println(se.getMessage());
			return false;
		} catch (Exception e) {
			System.out.println("getContentOnly: Exception");
			e.printStackTrace();
			return false;
		}
	}

	public static int getPageNumberEnd(String url)
			throws SocketTimeoutException {
		try {
			Page page = null;
			int status = 0;
			WebURL url2 = new WebURL();
			url2.setURL(url);
			WebCrawler crawler = new WebCrawler();
			page = crawler.getProcessPage(url2);
			status = page.getStatusCode();
			if (status == 200) {
				Document doc;
				HtmlParseData htmlParseData = (HtmlParseData) page
						.getParseData();
				String html = htmlParseData.getHtml();
				doc = Jsoup.parse(html, "UTF-8");
				String strTotalProduct = "";
				if (totalProductPosition > -1) {
					if (totalProductSelectPosition.isEmpty()) {
						strTotalProduct = doc.select(totalProductSelect)
								.get(totalProductPosition).text();
					} else {
						strTotalProduct = doc.select(totalProductSelect)
								.get(totalProductPosition)
								.select(totalProductSelectPosition).text();
					}
				} else {
					strTotalProduct = doc.select(totalProductSelect).text()
							.toString().trim();
				}
				// System.out.println(doc);
				int totalProduct = -1;
				int i;
				// get totalproduct
				// System.out.print(doc.toString());
				if (totalProductIndexBegin < 0) {
					String strNumberTotalProduct = "0";
					for (i = 0; i < strTotalProduct.length(); i++) {
						String ch = String.valueOf(strTotalProduct.charAt(i));
						if (ch.isEmpty() || ch.equals(" ")) {
							break;
						}
						if (tryParseIntByString(ch)) {
							strNumberTotalProduct += ch;
						}
					}
					totalProduct = Integer.parseInt(strNumberTotalProduct);
				} else {
					String strSplit = "0";
					for (i = totalProductIndexBegin; i <= totalProductIndexEnd; i++) {
						String ch = String.valueOf(strTotalProduct.charAt(i));
						if (tryParseIntByString(ch)) {
							strSplit += ch;
						}
					}
					totalProduct = Integer.parseInt(strSplit);
				}
				// get number product in page
				int numberPage = -1;
				String strNumberProductInPage = "";
				if (numberProductInPagePosition > -1) {
					if (numberProductInPageSelectPosition.isEmpty()) {
						strNumberProductInPage = doc
								.select(numberProductInPageSelect)
								.get(numberProductInPagePosition).text();
					} else {
						strNumberProductInPage = doc
								.select(numberProductInPageSelect)
								.get(numberProductInPagePosition)
								.select(numberProductInPageSelectPosition)
								.text();
					}
				} else {
					if (!numberProductInPageSelect.isEmpty())
						strNumberProductInPage = doc.select(
								numberProductInPageSelect).text();
				}

				if (regexFromIndexEnd < 0 && regexToIndexEnd < 0) {
					String strSplit = "0";
					for (i = 0; i < strNumberProductInPage.length(); i++) {
						String ch = String.valueOf(strNumberProductInPage
								.charAt(i));
						if (ch.isEmpty() || ch.equals(" ")) {
							break;
						}
						if (tryParseIntByString(ch)) {
							strSplit += ch;
						}
					}
					int numberProductInPage = Integer.parseInt(strSplit);
					// site001
					if (site_id == 1)
						numberProductInPage = 20;
					if (totalProduct > -1 && numberProductInPage > 0) {
						if ((totalProduct % numberProductInPage) == 0) {
							numberPage = totalProduct / numberProductInPage;
						} else if (totalProduct > numberProductInPage) {
							numberPage = totalProduct / numberProductInPage + 1;
						}
					}
					return numberPage;
				} else {
					// String[] arrStrNumberProductInPage =
					// strNumberProductInPage.split(regex);
					// String strNumberProductFrom =
					// arrStrNumberProductInPage[0];
					String strTotalProductReplace = String
							.valueOf(totalProduct);
					if (!decimalFormatTotalProduct.isEmpty()) {
						DecimalFormat formatter = new DecimalFormat(
								decimalFormatTotalProduct);
						strTotalProductReplace = formatter.format(totalProduct);
					}
					strNumberProductInPage = strNumberProductInPage.replace(
							strTotalProductReplace, "");
					int numberFrom = -1;
					String strSplitFrom = "0";
					for (i = regexFromIndexBegin; i <= regexFromIndexEnd; i++) {
						String ch = String.valueOf(strNumberProductInPage
								.charAt(i));
						if (tryParseIntByString(ch)) {
							strSplitFrom += ch;
						}
					}
					numberFrom = Integer.parseInt(strSplitFrom);

					// String strNumberProductTo = arrStrNumberProductInPage[1];
					int numberTo = -1;
					String strSplitTo = "0";
					for (i = regexToIndexBegin; i <= regexToIndexEnd; i++) {
						String ch = String.valueOf(strNumberProductInPage
								.charAt(i));
						if (tryParseIntByString(ch)) {
							strSplitTo += ch;
						}
					}
					numberTo = Integer.parseInt(strSplitTo);
					int numberProductInPage = numberTo - numberFrom + 1;
					// some site special can't have number product in page
					if (totalProduct > -1 && numberProductInPage > 0) {
						if ((totalProduct % numberProductInPage) == 0) {
							numberPage = totalProduct / numberProductInPage;
						} else if (totalProduct > numberProductInPage) {
							numberPage = totalProduct / numberProductInPage + 1;
						}
					}
					return numberPage;
				}

			} else {
				return -1;
			}

		} catch (Exception e) {

			System.out.println("getContentOnly: Exception");
			e.printStackTrace();
			return -1;
		}

	}

	public static String hashHTML(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			byte[] enc = md.digest();
			String md5Sum = new sun.misc.BASE64Encoder().encode(enc);
			return md5Sum;

		} catch (NoSuchAlgorithmException nsae) {

			System.out.println(nsae.getMessage());
			return null;
		}

	}

	public static Boolean checkSiteXml(String tag_sise) {
		try {
			// File fXmlFile = new File(System.getProperty("user.dir")
			// + pathXmlFile);
			File fXmlFile = new File(pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc.getElementsByTagName(tag_sise);
			System.out.print(nList);
			if (nList.getLength() < 1) {
				return false;
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			System.out.print("error read xml: " + e.getMessage());
			return false;
		}
		return true;
	}

	public static Boolean readXmlConfigDatabase() {

		try {
			// File fXmlFile = new File(System.getProperty("user.dir")
			// + JellyfishCrawlSite13Controller.pathXmlFile);
			File fXmlFile = new File(
					JellyfishCrawlSiteMainController.pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc
					.getElementsByTagName(tagConnection);
			org.w3c.dom.Node nNode = nList.item(0);
			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
				host = eElement.getElementsByTagName("host").item(0)
						.getTextContent();
				port = eElement.getElementsByTagName("port").item(0)
						.getTextContent();
				dbName = eElement.getElementsByTagName("dbName").item(0)
						.getTextContent();
				dbUser = eElement.getElementsByTagName("dbUser").item(0)
						.getTextContent();
				dbPwd = eElement.getElementsByTagName("dbPassword").item(0)
						.getTextContent();
			}
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("can not load xml file");
			return false;
		}

	}

	private static Boolean readConfigEmailSend() {
		try {
			// File fXmlFile = new File(System.getProperty("user.dir")
			// + JellyfishCrawlSite13Controller.pathXmlFile);
			File fXmlFile = new File(
					JellyfishCrawlSiteMainController.pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc.getElementsByTagName(tagInfoEmail);
			org.w3c.dom.Node nNode = nList.item(0);
			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
				host_email = eElement.getElementsByTagName("hostEmail").item(0)
						.getTextContent();
				port_email = eElement.getElementsByTagName("portEmail").item(0)
						.getTextContent();
				fromEmailAddress = eElement.getElementsByTagName("emailFrom")
						.item(0).getTextContent();
				passwordFromEmailAddesss = eElement
						.getElementsByTagName("passwordEmailFrom").item(0)
						.getTextContent();
				toEmailAddess = eElement.getElementsByTagName("emailTo")
						.item(0).getTextContent();
				subject = eElement.getElementsByTagName("subject").item(0)
						.getTextContent();
			}
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("can not load xml file");
			return false;
		}
	}

	public static String tag_size_03_share;

	public static void startSite(String tag_size) throws Exception {

		String crawlStorageFolder = "storage";// args[0];
		int numberOfCrawlers = 1;

		CrawlConfig config = new CrawlConfig();

		config.setCrawlStorageFolder(crawlStorageFolder);

		/*
		 * Be polite: Make sure that we don't send more than 1 request per
		 * second (1000 milliseconds between requests).
		 */
		config.setPolitenessDelay(1000);
		// config.setFollowRedirects(false);

		/*
		 * You can set the maximum crawl depth here. The default value is -1 for
		 * unlimited depth
		 */
		config.setMaxDepthOfCrawling(1);// ( use -1 for unlimited depth )

		/*
		 * You can set the maximum number of pages to crawl. The default value
		 * is -1 for unlimited number of pages
		 */
		config.setMaxPagesToFetch(-1);// ( use -1 for unlimited pages )

		/**
		 * Do you want crawler4j to crawl also binary data ? example: the
		 * contents of pdf, or the metadata of images etc
		 */
		config.setIncludeBinaryContentInCrawling(false);

		/*
		 * Do you need to set a proxy? If so, you can use:
		 * config.setProxyHost("proxyserver.example.com");
		 * config.setProxyPort(8080);
		 * 
		 * If your proxy also needs authentication:
		 * config.setProxyUsername(username); config.getProxyPassword(password);
		 */

		/*
		 * This config parameter can be used to set your crawl to be resumable
		 * (meaning that you can resume the crawl from a previously
		 * interrupted/crashed crawl). Note: if you enable resuming feature and
		 * want to start a fresh crawl, you need to delete the contents of
		 * rootFolder manually.
		 */
		config.setResumableCrawling(false);

		/*
		 * Overwrite user ddagent
		 */
		config.setUserAgentString("Test");

		/*
		 * Instantiate the controller for this crawl.
		 */
		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		// by me
		robotstxtConfig.setEnabled(false);
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig,
				pageFetcher);
		// by me
		CrawlController controller = new CrawlController(config, pageFetcher,
				robotstxtServer);

		/*
		 * For each crawl, you need to add some seed urls. These are the first
		 * URLs that are fetched and then the crawler starts following links
		 * which are found in these pages
		 */
		// controller.addSeed("http://careerbuilder.vn/");
		try {

			int sizeIDXML = -1;
			String provinceYESNO, linkCrawlerBegin, linkCrawlerPage;
			int pageNumberBegin = -1, pageNumberEnd = -1, pageLoopInit = -1, pageLoop = -1;

			// File fXmlFile = new File(System.getProperty("user.dir")
			// + pathXmlFile);
			File fXmlFile = new File(pathXmlFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
			org.w3c.dom.NodeList nList = doc.getElementsByTagName(tag_size);
			org.w3c.dom.Node nNode = nList.item(0);
			if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
				sizeIDXML = Integer.parseInt(eElement.getAttribute("id"));
				site_id = sizeIDXML;
				provinceYESNO = eElement.getElementsByTagName("provinceYESNO")
						.item(0).getTextContent();
				// readConfigPagenumber
				resetConfigPageNumberEnd();
				ReadConfigPageNumberEnd(eElement);
				if (!provinceYESNO.isEmpty()
						&& provinceYESNO.toUpperCase().equals("YES")) {
					// have sevent province
					org.w3c.dom.NodeList nListProvince = eElement
							.getElementsByTagName("province");
					for (int index = 0; index < nListProvince.getLength(); index++) {
						org.w3c.dom.Element eElementProvince = (org.w3c.dom.Element) nListProvince
								.item(index);
						linkCrawlerBegin = eElementProvince
								.getElementsByTagName("linkCrawlerBegin")
								.item(0).getTextContent();
						urlSendEmail = linkCrawlerBegin;
						linkCrawlerPage = eElementProvince
								.getElementsByTagName("linkCrawlerPage")
								.item(0).getTextContent();
						if (!eElementProvince
								.getElementsByTagName("pageNumberBegin")
								.item(0).getTextContent().isEmpty()) {
							pageNumberBegin = Integer.parseInt(eElementProvince
									.getElementsByTagName("pageNumberBegin")
									.item(0).getTextContent());
						}
						if (!eElementProvince
								.getElementsByTagName("pageLoopInit").item(0)
								.getTextContent().isEmpty()) {
							pageLoopInit = Integer.parseInt(eElementProvince
									.getElementsByTagName("pageLoopInit")
									.item(0).getTextContent());
						}
						if (!eElementProvince.getElementsByTagName("pageLoop")
								.item(0).getTextContent().isEmpty()) {
							pageLoop = Integer.parseInt(eElementProvince
									.getElementsByTagName("pageLoop").item(0)
									.getTextContent());
						}
						if (!linkCrawlerBegin.isEmpty()) {
							// controller.addSeed("http://woman.type.jp/job-condition/58/p22/");
							controller.addSeed(linkCrawlerBegin);
							pageNumberEnd = getPageNumberEnd(linkCrawlerBegin);
							if (pageNumberEnd > 1) {
								for (; pageNumberBegin <= pageNumberEnd; pageNumberBegin++) {
									String convertlinkCrawlerPage = linkCrawlerPage
											.replace("%s", String
													.valueOf(pageLoopInit));
									controller.addSeed(convertlinkCrawlerPage);
									pageLoopInit += pageLoop;
								}
							}
						}

					}
				} else if (!provinceYESNO.isEmpty()
						&& provinceYESNO.toUpperCase().equals("NO")) {
					// don't have sevent province
					org.w3c.dom.NodeList nListOnewebsite = eElement
							.getElementsByTagName("website");
					org.w3c.dom.Element eElementOnewebsite = (org.w3c.dom.Element) nListOnewebsite
							.item(0);
					// read config of pagenumber end
					linkCrawlerBegin = eElementOnewebsite
							.getElementsByTagName("linkCrawlerBegin").item(0)
							.getTextContent();
					urlSendEmail = linkCrawlerBegin;
					linkCrawlerPage = eElementOnewebsite
							.getElementsByTagName("linkCrawlerPage").item(0)
							.getTextContent();
					if (!eElementOnewebsite
							.getElementsByTagName("pageNumberBegin").item(0)
							.getTextContent().isEmpty()) {
						pageNumberBegin = Integer.parseInt(eElementOnewebsite
								.getElementsByTagName("pageNumberBegin")
								.item(0).getTextContent());
					}
					if (!eElementOnewebsite
							.getElementsByTagName("pageLoopInit").item(0)
							.getTextContent().isEmpty()) {
						pageLoopInit = Integer.parseInt(eElementOnewebsite
								.getElementsByTagName("pageLoopInit").item(0)
								.getTextContent());
					}
					if (!eElementOnewebsite.getElementsByTagName("pageLoop")
							.item(0).getTextContent().isEmpty()) {
						pageLoop = Integer.parseInt(eElementOnewebsite
								.getElementsByTagName("pageLoop").item(0)
								.getTextContent());
					}
					if (!linkCrawlerBegin.isEmpty()) {
						if (sizeIDXML == 6) {
							// special size
							controller.addSeed(linkCrawlerBegin);
							Boolean loop = true;
							while(loop){
								String convertlinkCrawlerPage = linkCrawlerPage
										.replace("%s", String
												.valueOf(pageLoopInit));
								if(checkTotalJobForSite6(convertlinkCrawlerPage)){
									controller.addSeed(convertlinkCrawlerPage);
									pageLoopInit += pageLoop;
								}else
									loop = false;
							}
							
						}else if (sizeIDXML == 9) {
							// special size
							controller.addSeed(linkCrawlerBegin);
							pageNumberEnd = getPageNumberEnd(linkCrawlerBegin);
						}
						else {
							pageNumberEnd = getPageNumberEnd(linkCrawlerBegin);
							controller.addSeed(linkCrawlerBegin);
							if (pageNumberEnd > 1) {
								for (; pageNumberBegin <= pageNumberEnd; pageNumberBegin++) {
									String convertlinkCrawlerPage = linkCrawlerPage
											.replace("%s", String
													.valueOf(pageLoopInit));
									controller.addSeed(convertlinkCrawlerPage);
									pageLoopInit += pageLoop;
								}
							}
						}
					}
				}
				// delete content_crawl before insert
				readXmlConfigDatabase();
				MysqlCrawler.createConn(host, port, dbName, dbUser, dbPwd);
				if (sizeIDXML == 1) {
					JellyfishCrawlerSite001.tag_size = tag_size;
					JellyfishCrawlerSite001.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite001.class,
							numberOfCrawlers);
				} else if (sizeIDXML == 3) {
					JellyfishCrawlerSite003.tag_size = tag_size;
					JellyfishCrawlerSite003.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite003.class,
							numberOfCrawlers);
				} else if (sizeIDXML == 4) {
					JellyfishCrawlerSite004.tag_size = tag_size;
					JellyfishCrawlerSite004.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite004.class,
							numberOfCrawlers);
				} else if (sizeIDXML == 5) {
					JellyfishCrawlerSite005.tag_size = tag_size;
					JellyfishCrawlerSite005.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite005.class,
							numberOfCrawlers);
				}else if (sizeIDXML == 6) {
					JellyfishCrawlerSite006.tag_size = tag_size;
					JellyfishCrawlerSite006.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite006.class,
							numberOfCrawlers);
				}else if (sizeIDXML == 2) {
					JellyfishCrawlerSite002.tag_size = tag_size;
					JellyfishCrawlerSite002.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite002.class,
							numberOfCrawlers);
				}else if (sizeIDXML == 8) {
					JellyfishCrawlerSite008.tag_size = tag_size;
					JellyfishCrawlerSite008.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSite008.class,
							numberOfCrawlers);
				}else if (sizeIDXML == 9) {
						JellyfishCrawlerSite009.tag_size = tag_size;
						JellyfishCrawlerSite009.siteIDXML = sizeIDXML;
						JellyfishCrawlerSite009.pageNumberEnd = pageNumberEnd;
						controller.start(JellyfishCrawlerSite009.class,
								numberOfCrawlers);
				} else {
					JellyfishCrawlerSiteMain.tag_size = tag_size;
					JellyfishCrawlerSiteMain.siteIDXML = sizeIDXML;
					controller.start(JellyfishCrawlerSiteMain.class,
							numberOfCrawlers);
				}
				// tam thoi ko cho update status
				// Boolean result = MysqlCrawler.getInstance()
				// .updateStatusContentsByCrawlDate(sizeIDXML);
				// String log;
				// if (result)
				// log = tag_size
				// + " update status = 0 all job yesterday crawler SUCCESS";
				// else
				// log = tag_size
				// + " update status = 0 all job yesterday crawler FAIL";
				// writeLogCrawler(log);

			}
		} catch (Exception ex) {
			System.out.print("FAIL SITE:" + tag_size);
			System.out.print("can't read config xml, review xml file !!");
			System.out.print(ex.getMessage());
		}

	}

	public static void writeLogCrawler(String value) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter("Log_Crawler.txt", true)));
			out.println(value);
			out.close();

		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private static void processExeuteUpDateStatusAndCallStore() {
		readXmlConfigDatabase();
		MysqlCrawler.createConn(host, port, dbName, dbUser, dbPwd);
		Boolean result;
		String log = "";
		// update status all job <> 3
		writeLogCrawler("BEGIN CALL UPDATE STATUS AND CALL STORE");
		result = MysqlCrawler.getInstance()
				.updateStatusAllContentsByCrawlDate();
		if (result)
			log = "execute update status all job yesterday is 0 SUCCESS";
		else
			log = "execute update status all job yesterday is 0 FAIL";
		writeLogCrawler(log);

		// call store if have
		DateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date dateBegin = new Date();
		Date dateFinish = new Date();
		result = MysqlCrawler.getInstance().exe_store_prc_add_priority();
		dateFinish = new Date();
		if (result)
			log = " exe_store_prc_add_priority "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_store_prc_add_priority error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_store_prc_add_list_city();
		dateFinish = new Date();
		if (result)
			log = " exe_store_prc_add_list_city "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_store_prc_add_list_city error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_store_prc_add_list_em();
		dateFinish = new Date();
		if (result)
			log = " exe_store_prc_add_list_em "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_store_prc_add_list_em error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_store_prc_add_list_hangup();
		dateFinish = new Date();
		if (result)
			log = " exe_store_prc_add_list_hangup "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_store_prc_add_list_hangup error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_store_prc_add_list_job();
		dateFinish = new Date();
		if (result)
			log = " exe_store_prc_add_list_job "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_store_prc_add_list_job error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_prc_add_list_job_parent();
		dateFinish = new Date();
		if (result)
			log = " exe_prc_add_list_job_parent "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_prc_add_list_job_parent error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_prc_add_list_province();
		dateFinish = new Date();
		if (result)
			log = " exe_prc_add_list_province "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_prc_add_list_province error";
		writeLogCrawler(log);

		dateBegin = new Date();
		result = MysqlCrawler.getInstance().exe_prc_prc_add_waysite();
		dateFinish = new Date();
		if (result)
			log = " exe_prc_prc_add_waysite "
					+ dateTimeFormat.format(dateBegin) + " - "
					+ dateTimeFormat.format(dateFinish);
		else
			log = " exe_prc_prc_add_waysite error";
		writeLogCrawler(log);

	}

	public static void main(String[] args) {

		// logger.info("Start...: ");
		// if(args.length < 1){
		// System.out.println("Please input site crawler !!");
		// System.out.println("format input: siteid or siteid-siteid-siteid-siteid-v..v....");
		// return;
		// }
		DateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String listSize = args[0];// "site4";//; //"site3";// //
									// //"site8";// // "site011";//
		String configXML = "";
		if (args.length > 1) {
			configXML = args[1];// "site9";
		}
		if (!configXML.isEmpty()) {
			pathXmlFile = configXML;
		}

		// if call store and update status rieng thi call moi
		// if (listSize.equals("site100")) {
		// // process update status and call store
		// processExeuteUpDateStatusAndCallStore();
		// return;
		// }

		System.out.print(pathXmlFile);
		if (!listSize.isEmpty()) {
			String[] arrListSize = listSize.split("-");
			Boolean flagSendEmail = false;
			String message = "<i>Dear IT Team !</i><br><br>";
			for (int i = 0; i < arrListSize.length; i++) {
				logger.info("Start...: " + arrListSize[i]);
				Date datestart = new Date();
				long millisecondStart = datestart.getTime();
				totalJob = 0;
				totalJobNew = 0;

				try {

					if (checkSiteXml(arrListSize[i])) {
						startSite(arrListSize[i]);
						Date dateEnd = new Date();
						long millisecondEnd = dateEnd.getTime();
						long millisecondTotal = millisecondEnd
								- millisecondStart;
						int totalMinute = (int) ((millisecondTotal / 1000) / 60);
						int totalSecond = (int) ((millisecondTotal / 1000));
						String logCrawler = "";
						logCrawler += arrListSize[i] + "\t" + "FromDate:"
								+ dateTimeFormat.format(datestart) + "\t"
								+ "ToDate:" + dateTimeFormat.format(dateEnd)
								+ "\t" + "TotalJob:" + totalJob + "\t"
								+ "TotalJobNew:" + totalJobNew + "\t"
								+ "Second:" + totalSecond + "\t" + "Minute:"
								+ totalMinute;
						writeLogCrawler(logCrawler);
						// send email if job empty
						if (totalJob <= 0) {
							// message contains HTML markups
							flagSendEmail = true;
							message += "<b>" + arrListSize[i] + " with url:"
									+ urlSendEmail + " have 0 job </b><br><br>";
						}
					} else {
						System.out
								.println("can't find site:"
										+ arrListSize[i]
										+ " in JellyfishCrawlConfig.xml. please check again.");
						String logCrawler = dateTimeFormat.format(datestart)
								+ "\t"
								+ "can't find :"
								+ arrListSize[i]
								+ " in JellyfishCrawlConfig.xml. please check again";
						writeLogCrawler(logCrawler);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.print("FAIL SITE:" + arrListSize[i]);
					e.printStackTrace();
					String logCrawler = dateTimeFormat.format(datestart) + "\t"
							+ arrListSize[i] + "\t" + e.getMessage();
					writeLogCrawler(logCrawler);
				}
			}
			message += "<b>Please check it</b><br>";
			message += "<b>Thank you</b><br>";
			if (flagSendEmail) {
				try {
					readConfigEmailSend();
					sendEmail(host_email, port_email, fromEmailAddress,
							passwordFromEmailAddesss, toEmailAddess, subject,
							message);
					System.out.println("Email sent.");
				} catch (Exception ex) {
					System.out.println("Failed to sent email.");
					ex.printStackTrace();
				}
			}

		}

	}
}