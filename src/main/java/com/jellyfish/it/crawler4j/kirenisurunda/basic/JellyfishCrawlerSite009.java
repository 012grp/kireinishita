/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jellyfish.it.crawler4j.kirenisurunda.basic;

import com.jellyfish.it.crawler4j.crawler.Page;
import com.jellyfish.it.crawler4j.crawler.WebCrawler;
import com.jellyfish.it.crawler4j.parser.ExtractedUrlAnchorPair;
import com.jellyfish.it.crawler4j.parser.HtmlParseData;
import com.jellyfish.it.crawler4j.parser.HtmlContentHandler;
import com.jellyfish.it.crawler4j.storage.MysqlCrawler;
import com.jellyfish.it.crawler4j.url.WebURL;
import com.sun.syndication.feed.atom.Link;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author Yasser Ganjisaffar [lastname at gmail dot com]
 */
public class JellyfishCrawlerSite009 extends WebCrawler {
	private Boolean status_read_xml = false;
	/* Job Img */
	private String jobImgUrl = "";
	private String jobImgQuery = "";
	private String jobImageFormatAttr = "";
	/* Job url */
	private String joburl_url = "";
	private String jobUrlQuery = "";
	private String jobUrlFormatAttr = "";
	/* Job Name */
	private String jobNameQuery = "";
	private String jobNameFormatData = "";
	private String jobNameGetByTitle = "";
	/* Job Location */
	private String jobLocationQuery = "";
	private String jobLocationFormatData = "";
	private String jobLocationGetByTitle = "";
	/* Job Salary */
	private String JobSalaryQuery = "";
	private String jobSalaryFormatData = "";
	private String JobSalaryGetByTitle = "";
	/* Station name */
	private String StationNameQuery = "";
	private String StationNameFormatData = "";
	private String StationNameGetByTitle = "";
	/* Job Expire */
	private String JobExpireQuery = "";
	private String jobExpireFormatData = "";
	private String JobExpireGetByTitle = "";
	/* Job Company */
	private String JobCompanyQuery = "";
	private String jobCompanyFormatData = "";
	private String JobCompanyGetByTitle = "";
	/* Job Type */
	private String JobTypeUrl = "";
	private String JobTypeQuery = "";
	private String jobTypeFormatAttr = "";
	private String jobTypeFormatData = "";
	private String jobTypeGetByTitle = "";
	private org.w3c.dom.Element elementListJobType;
	/* Job Address */
	private String JobAddressQuery = "";
	private String jobAddressFormatData = "";
	private String JobAddressGetByTitle = "";
	/* Job Career */
	private String JobCareerQuery = "";
	private String jobCareerFormatData = "";
	private String JobCareerGetByTitle = "";
	/* JobRequirementSpecial */
	private String JobRequirementSpecialSelect = "";
	private String JobRequirementSpecialFormatData = "";
	private String JobRequirementSpecialGetByTitle = "";
	private org.w3c.dom.Element elementListJobRequirementSpecial;

	/* config database */
	private String host = "";
	private String port = "";
	private String dbName = "";
	private String dbUser = "";
	private String dbPwd = "";
	private String tagConnection = "databaseConnection";
	public static String tag_size = "";
	public static int siteIDXML = 0;
	/* read config province_id */
	private String provinceYESNO;
	private String provinceName7;
	private String provinceName6;
	private String provinceName5;
	private String provinceName4;
	private String provinceName3;
	private String provinceName2;
	private String provinceName1;

	public static int pageNumberEnd = -1;

	/* query select body */
	private String bodySelect = "";
	private static Boolean status_check_table_logdata = false;

	private final static Pattern BINARY_FILES_EXTENSIONS = Pattern
			.compile(".*\\.(bmp|gif|jpe?g|png|tiff?|pdf|ico|xaml|pict|rif|pptx?|ps"
					+ "|mid|mp2|mp3|mp4|wav|wma|au|aiff|flac|ogg|3gp|aac|amr|au|vox"
					+ "|avi|mov|mpe?g|ra?m|m4v|smil|wm?v|swf|aaf|asf|flv|mkv"
					+ "|zip|rar|gz|7z|aac|ace|alz|apk|arc|arj|dmg|jar|lzip|lha)"
					+ "(\\?.*)?$"); // For url Query parts ( URL?q=... )

	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */

	public Boolean readXmlConfigDatabase() {
		if (status_read_xml == false) {
			try {
				// File fXmlFile = new File(System.getProperty("user.dir")
				// + JellyfishCrawlSite13Controller.pathXmlFile);
				File fXmlFile = new File(
						JellyfishCrawlSiteMainController.pathXmlFile);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
				org.w3c.dom.NodeList nList = doc
						.getElementsByTagName(tagConnection);
				org.w3c.dom.Node nNode = nList.item(0);
				if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					host = eElement.getElementsByTagName("host").item(0)
							.getTextContent();
					port = eElement.getElementsByTagName("port").item(0)
							.getTextContent();
					dbName = eElement.getElementsByTagName("dbName").item(0)
							.getTextContent();
					dbUser = eElement.getElementsByTagName("dbUser").item(0)
							.getTextContent();
					dbPwd = eElement.getElementsByTagName("dbPassword").item(0)
							.getTextContent();
				}
				return true;

			} catch (Exception e) {
				e.printStackTrace();
				System.out.print("can not load xml file");
				return false;
			}
		}
		return true;
	}

	public Boolean ReadXmlConfig() {
		if (status_read_xml == false) {
			try {
				// File fXmlFile = new File(System.getProperty("user.dir")
				// + JellyfishCrawlSite13Controller.pathXmlFile);
				File fXmlFile = new File(
						JellyfishCrawlSiteMainController.pathXmlFile);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
				org.w3c.dom.NodeList nList = doc
						.getElementsByTagName(this.tag_size);
				org.w3c.dom.Node nNode = nList.item(0);
				if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					/* element List JobType */
					org.w3c.dom.NodeList nListJobType = eElement
							.getElementsByTagName("list_job_type");
					org.w3c.dom.Node nNodeJobType = nListJobType.item(0);
					elementListJobType = (org.w3c.dom.Element) nNodeJobType;
					/* element List JobType */
					org.w3c.dom.NodeList nListJobRequirementSpecial = eElement
							.getElementsByTagName("list_job_requirement_special");
					org.w3c.dom.Node nNodeListJobRequirementSpecial = nListJobRequirementSpecial
							.item(0);
					elementListJobRequirementSpecial = (org.w3c.dom.Element) nNodeListJobRequirementSpecial;
					/* body select */
					bodySelect = eElement.getElementsByTagName("bodySelect")
							.item(0).getTextContent();
					/* Job Img */
					jobImgUrl = eElement.getElementsByTagName("JobImage_Url")
							.item(0).getTextContent();
					jobImgQuery = eElement
							.getElementsByTagName("JobImageSelect").item(0)
							.getTextContent();
					jobImageFormatAttr = eElement
							.getElementsByTagName("JobImageFormatAttr").item(0)
							.getTextContent();
					/* Job url */
					joburl_url = eElement.getElementsByTagName("JobUrl_Url")
							.item(0).getTextContent();
					jobUrlQuery = eElement.getElementsByTagName("JobUrlSelect")
							.item(0).getTextContent();
					jobUrlFormatAttr = eElement
							.getElementsByTagName("JobUrlFormatAttr").item(0)
							.getTextContent();
					/* Job name */
					jobNameQuery = eElement
							.getElementsByTagName("JobNameSelect").item(0)
							.getTextContent();
					jobNameFormatData = eElement
							.getElementsByTagName("JobNameFormatData").item(0)
							.getTextContent();
					jobNameGetByTitle = eElement
							.getElementsByTagName("JobNameGetByTitle").item(0)
							.getTextContent();
					/* Job location */
					jobLocationQuery = eElement
							.getElementsByTagName("JobLocationSelect").item(0)
							.getTextContent();
					jobLocationFormatData = eElement
							.getElementsByTagName("JobLocationFormatData")
							.item(0).getTextContent();
					jobLocationGetByTitle = eElement
							.getElementsByTagName("JobLocationGetByTitle")
							.item(0).getTextContent();
					/* Job Salary */
					JobSalaryQuery = eElement
							.getElementsByTagName("JobSalarySelect").item(0)
							.getTextContent();
					jobSalaryFormatData = eElement
							.getElementsByTagName("JobSalaryFormatData")
							.item(0).getTextContent();
					JobSalaryGetByTitle = eElement
							.getElementsByTagName("JobSalaryGetByTitle")
							.item(0).getTextContent();
					/* Job Detail */
					StationNameQuery = eElement
							.getElementsByTagName("StationNameSelect").item(0)
							.getTextContent();
					StationNameFormatData = eElement
							.getElementsByTagName("StationNameFormatData")
							.item(0).getTextContent();
					StationNameGetByTitle = eElement
							.getElementsByTagName("StationNameGetByTitle")
							.item(0).getTextContent();
					/* Job Expire */
					JobExpireQuery = eElement
							.getElementsByTagName("JobExpireSelect").item(0)
							.getTextContent();
					jobExpireFormatData = eElement
							.getElementsByTagName("JobExpireFormatData")
							.item(0).getTextContent();
					JobExpireGetByTitle = eElement
							.getElementsByTagName("JobExpireGetByTitle")
							.item(0).getTextContent();
					/* Job Company */
					JobCompanyQuery = eElement
							.getElementsByTagName("JobCompanySelect").item(0)
							.getTextContent();
					jobCompanyFormatData = eElement
							.getElementsByTagName("JobCompanyFormatData")
							.item(0).getTextContent();
					JobCompanyGetByTitle = eElement
							.getElementsByTagName("JobCompanyGetByTitle")
							.item(0).getTextContent();
					/* Job type */
					JobTypeUrl = eElement.getElementsByTagName("JobType_Url")
							.item(0).getTextContent();
					JobTypeQuery = eElement
							.getElementsByTagName("JobTypeSelect").item(0)
							.getTextContent();
					jobTypeFormatAttr = eElement
							.getElementsByTagName("JobTypeFormatAttr").item(0)
							.getTextContent();
					jobTypeFormatData = eElement
							.getElementsByTagName("JobTypeFormatData").item(0)
							.getTextContent();
					jobTypeGetByTitle = eElement
							.getElementsByTagName("JobTypeGetByTitle").item(0)
							.getTextContent();
					/* Job Address */
					JobAddressQuery = eElement
							.getElementsByTagName("JobAddressSelect").item(0)
							.getTextContent();
					jobAddressFormatData = eElement
							.getElementsByTagName("JobAddressFormatData")
							.item(0).getTextContent();
					JobAddressGetByTitle = eElement
							.getElementsByTagName("JobAddressGetByTitle")
							.item(0).getTextContent();
					/* Job Career */
					JobCareerQuery = eElement
							.getElementsByTagName("JobCareerSelect").item(0)
							.getTextContent();
					jobCareerFormatData = eElement
							.getElementsByTagName("JobCareerFormatData")
							.item(0).getTextContent();
					JobCareerGetByTitle = eElement
							.getElementsByTagName("JobCareerGetByTitle")
							.item(0).getTextContent();
					/* JobRequirementSpecial */
					JobRequirementSpecialSelect = eElement
							.getElementsByTagName("JobRequirementSpecialSelect")
							.item(0).getTextContent();
					JobRequirementSpecialFormatData = eElement
							.getElementsByTagName(
									"JobRequirementSpecialFormatData").item(0)
							.getTextContent();

					JobRequirementSpecialGetByTitle = eElement
							.getElementsByTagName(
									"JobRequirementSpecialGetByTitle").item(0)
							.getTextContent();
				}
				return true;

			} catch (Exception e) {
				e.printStackTrace();
				System.out.print("can not load xml file");
				return false;
			}
		}
		return true;
	}

	ArrayList<String> listSelector = new ArrayList<String>();

	private Boolean getListSelectorGetContentForConvertUTF8() {
		if (status_read_xml == false) {
			try {
				// File fXmlFile = new File(System.getProperty("user.dir") +
				// JellyfishCrawlSite13Controller.pathXmlFile);
				File fXmlFile = new File(
						JellyfishCrawlSiteMainController.pathXmlFile);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
				org.w3c.dom.NodeList nList = doc
						.getElementsByTagName(this.tag_size);
				org.w3c.dom.Node nNode = nList.item(0);
				if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
					org.w3c.dom.NodeList nListSelector = eElement
							.getElementsByTagName("SelectorGetContentForUTF8");
					for (int index = 0; index < nListSelector.getLength(); index++) {
						String selector = nListSelector.item(index)
								.getTextContent();
						listSelector.add(selector);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out
						.print("list selector for get content for convert utf 8 is empty");

			}
		}
		return true;
	}

	@Override
	public boolean shouldVisit(Page page, WebURL url) {
		return false;
		// String href = url.getURL().toLowerCase();

		// return !BINARY_FILES_EXTENSIONS.matcher(href).matches() &&
		// href.startsWith("http://careerbuilder.vn/vi") && href.endsWith("jd");
		// return !BINARY_FILES_EXTENSIONS.matcher(href).matches() &&
		// href.startsWith("http://careerbuilder.vn/vi") &&
		// href.endsWith(".html");
		// return !BINARY_FILES_EXTENSIONS.matcher(href).matches() &&
		// href.startsWith("http://jobsearch.living.jp/kyujin/");
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */

	public void getData(Document doc, String url) {
		Element body = doc.body();
		// System.out.print(body.html());
		Elements listDetail = body.select(bodySelect);
		Integer i = 0;
		Integer siteID = siteIDXML;
		Integer provinceID = 0;
		// end
		int totalJob = 0;
		int totalJobNew = 0;
		String jobImage, jobUrl, aJobName, cJobLocation = null, cLocationNear = "", stationName = "", bJobCompany = "", dJobCareer, eJobSalary, gJobDetail = "", hJobExpire = null;
		String jobRequirementSpecial = "";
		for (Element detail : listDetail) {
			i++;
			try {
				jobUrl = "";
				/* job url */
				if (!jobUrlQuery.isEmpty()) {
					if (!detail.select(jobUrlQuery).attr(jobUrlFormatAttr)
							.isEmpty()) {
						if (!joburl_url.isEmpty())
							jobUrl = joburl_url
									+ detail.select(jobUrlQuery).first()
											.attr(jobUrlFormatAttr);
						else
							jobUrl = detail.select(jobUrlQuery).first()
									.attr(jobUrlFormatAttr);
					}
				}
				org.jsoup.nodes.Document detailJobUrlNew;
				detailJobUrlNew = MysqlCrawler.convertUrlToDocument(jobUrl);
				jobImage = "";
				/* job img */
				if (!jobImgQuery.isEmpty()) {
					if (!detailJobUrlNew.select(jobImgQuery)
							.attr(jobImageFormatAttr).isEmpty()) {
						if (!jobImgUrl.isEmpty()) {
							jobImage = jobImgUrl
									+ detailJobUrlNew.select(jobImgQuery)
											.first().attr(jobImageFormatAttr);

						} else {
							jobImage = detailJobUrlNew.select(jobImgQuery)
									.first().attr(jobImageFormatAttr);

						}
					}
				}
				/* job name */
				aJobName = "";
				if (!jobNameGetByTitle.isEmpty()) {
					// process get data
					Elements list = detailJobUrlNew.select(jobNameQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(jobNameGetByTitle)) {
							aJobName = detailList.select("td").text().trim();
							break;
						}
					}
				} else {
					if (jobNameFormatData.toUpperCase().equals("TEXT")) {
						aJobName = detailJobUrlNew.select(jobNameQuery).text();
					} else if (jobNameFormatData.toUpperCase().equals("HTML")) {
						aJobName = detailJobUrlNew.select(jobNameQuery).html();
					}
				}
				/* job location */
				cJobLocation = "";
				if (!jobLocationGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(jobLocationQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(jobLocationGetByTitle)) {
							cJobLocation = detailList.select("td").text()
									.trim();
							break;
						}
					}
				} else {
					if (!jobLocationQuery.isEmpty()) {
						if (jobLocationFormatData.toUpperCase().equals("TEXT")) {
							cJobLocation = detailJobUrlNew.select(
									jobLocationQuery).text();
						} else if (jobLocationFormatData.toUpperCase().equals(
								"HTML")) {
							cJobLocation = detailJobUrlNew.select(
									jobLocationQuery).html();
						}

					}
				}
				/* job salary */
				eJobSalary = "";
				if (!JobSalaryGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(JobSalaryQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(JobSalaryGetByTitle)) {
							eJobSalary = detailList.select("td").text().trim();
							break;
						}
					}
				} else {
					if (!JobSalaryQuery.isEmpty()) {
						if (jobSalaryFormatData.toUpperCase().equals("TEXT")) {
							eJobSalary = detailJobUrlNew.select(JobSalaryQuery)
									.text();
						} else if (jobSalaryFormatData.toUpperCase().equals(
								"HTML")) {
							eJobSalary = detailJobUrlNew.select(JobSalaryQuery)
									.html();
						}

					}
				}
				/* job detail */
				stationName = "";
				if (!StationNameGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(StationNameQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(StationNameGetByTitle)) {
							stationName = detailList.select("td").text().trim();
							break;
						}
					}
				} else {
					if (!StationNameQuery.isEmpty()) {
						if (StationNameFormatData.toUpperCase().equals("TEXT")) {
							stationName = detailJobUrlNew.select(
									StationNameQuery).text();
						} else if (StationNameFormatData.toUpperCase().equals(
								"HTML")) {
							stationName = detailJobUrlNew.select(
									StationNameQuery).html();
						}
					}
				}
				/* job expire */
				hJobExpire = "";
				if (!JobExpireGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(JobExpireQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(JobExpireGetByTitle)) {
							hJobExpire = detailList.select("td").text().trim();
							break;
						}
					}
				} else {
					if (!JobExpireQuery.isEmpty()) {
						if (jobExpireFormatData.toUpperCase().equals("TEXT")) {
							hJobExpire = detailJobUrlNew.select(JobExpireQuery)
									.text();
						} else if (jobExpireFormatData.toUpperCase().equals(
								"HTML")) {
							hJobExpire = detailJobUrlNew.select(JobExpireQuery)
									.html();
						}

					}
				}
				/* job company */
				bJobCompany = JobCompanyGetByTitle;
				/* job type */
				String fJobType = "";
				if (!jobTypeGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(JobTypeQuery);
					if (siteID == 7) {
						for (Element detailList : list) {
							String checkTitle = detailList
									.select("th:nth-child(3)").text().trim();
							if (checkTitle.equals(jobTypeGetByTitle)) {
								fJobType = detailList.select("td:nth-child(4)")
										.text().trim();
								break;
							}
						}
					} else {
						for (Element detailList : list) {
							String checkTitle = detailList.select("th").text()
									.trim();
							if (checkTitle.equals(jobTypeGetByTitle)) {
								fJobType = detailList.select("td").text()
										.trim();
								break;
							}
						}
					}
				} else {
					if (!JobTypeQuery.isEmpty()) {
						if (!detailJobUrlNew.select(JobTypeQuery)
								.attr(jobTypeFormatAttr).isEmpty()) {
							if (!JobTypeUrl.isEmpty()) {
								fJobType = JobTypeUrl
										+ detailJobUrlNew.select(JobTypeQuery)
												.attr(jobTypeFormatAttr);
							} else {
								fJobType = detailJobUrlNew.select(JobTypeQuery)
										.attr(jobTypeFormatAttr);
							}
						} else if (!detailJobUrlNew.select(JobTypeQuery)
								.isEmpty()) {
							if (jobTypeFormatData.toUpperCase().equals("TEXT")) {
								fJobType = detailJobUrlNew.select(JobTypeQuery)
										.text();
							} else if (jobTypeFormatData.toUpperCase().equals(
									"HTML")) {
								fJobType = detailJobUrlNew.select(JobTypeQuery)
										.html();
							}
						}
					}
				}
				fJobType = getJobtypeByCondition(fJobType);
				/* job address */
				String jobAddress = "";
				if (!JobAddressGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(JobAddressQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(JobAddressGetByTitle)) {
							jobAddress = detailList.select("td").text().trim();
							break;
						}
					}
				} else {
					if (!JobAddressQuery.isEmpty()) {
						if (jobAddressFormatData.toUpperCase().equals("TEXT")) {
							jobAddress = detailJobUrlNew
									.select(JobAddressQuery).text();
						} else if (jobAddressFormatData.toUpperCase().equals(
								"HTML")) {
							jobAddress = detailJobUrlNew
									.select(JobAddressQuery).html();
						}

					}
				}
				dJobCareer = "";
				if (!JobCareerGetByTitle.isEmpty()) {
					Elements list = detailJobUrlNew.select(JobCareerQuery);
					for (Element detailList : list) {
						String checkTitle = detailList.select("th").text()
								.trim();
						if (checkTitle.equals(JobCareerGetByTitle)) {
							dJobCareer = detailList.select("td").text().trim();
							break;
						}
					}
				} else {
					if (!JobCareerQuery.isEmpty()) {
						if (jobCareerFormatData.toUpperCase().equals("TEXT")) {
							dJobCareer = detailJobUrlNew.select(JobCareerQuery)
									.text();
						} else if (jobCareerFormatData.toUpperCase().equals(
								"HTML")) {
							dJobCareer = detailJobUrlNew.select(JobCareerQuery)
									.html();
						}
					}
				}
				// new -- jobRequirementSpecial
				jobRequirementSpecial = "";
				// System.out.println("\n Url : " + jobUrl);
				// System.out.println("\n Image : " + jobImage);
				// System.out.println("\n Title : " + aJobName);
				// System.out.println("\n Location : " + cJobLocation + "\n"
				// + cLocationNear);
				// System.out.println("\n CareerPath : " + dJobCareer);
				// System.out.println("\n Detail : " + gJobDetail);
				// System.out.println("\n Salary : " + eJobSalary);
				// System.out.println("\n expire Date : " + hJobExpire);
				// System.out.println("\n Company : " + bJobCompany);
				// System.out.println("\n JobType : " + fJobType);
				// System.out.println("\n Full I : " + i);

				String content = MysqlCrawler
						.getContentFromDocumentCrawler(detailJobUrlNew);
				String hash_code = MysqlCrawler.hashBodyText(content);

				String contentForConvertUTF8 = MysqlCrawler
						.getStringForConvertUTF8(detailJobUrlNew, listSelector);

				if (contentForConvertUTF8.isEmpty())
					contentForConvertUTF8 = content;
				// tam thoi set rong
				contentForConvertUTF8 = "";
				MysqlCrawler.getInstance().processInsertContentsCrawler(siteID,
						provinceID, jobUrl, aJobName, gJobDetail, bJobCompany,
						dJobCareer, eJobSalary, hJobExpire, cJobLocation,
						cLocationNear, jobAddress, jobImage, fJobType, content,
						hash_code, url, contentForConvertUTF8,
						jobRequirementSpecial, stationName);
				if (MysqlCrawler.statusJobNew)
					totalJobNew += 1;
				totalJob += 1;

			} catch (Exception ex) {
				// System.out.println("\n Fail I : " + i);
				// System.out.println("\n Ex : " + ex);
				logger.error(ex.getMessage());
				// insert log error
				MysqlCrawler.getInstance().insertLogError(
						siteID,
						"File: " + ex.getStackTrace()[1].getFileName()
								+ " Line: "
								+ ex.getStackTrace()[1].getLineNumber(),
						ex.getMessage());
			}
		}

		// System.out.println("\n Total Div: --" + listDetail.size());
		JellyfishCrawlSiteMainController.totalJob += totalJob;

	}

	@Override
	public void visit(Page page) {
		if (ReadXmlConfig() && readXmlConfigDatabase()
				&& getListSelectorGetContentForConvertUTF8()) {
			status_read_xml = true;
		} else {
			return;
		}
		String url = page.getWebURL().getURL();
		MysqlCrawler.createConn(host, port, dbName, dbUser, dbPwd);
		System.out.println("\n URL visit: " + page.getWebURL().getURL());
		if (page.getParseData() instanceof HtmlParseData) {
			// delete old data before insert new data
			for (int pageIndex = 1; pageIndex <= pageNumberEnd; pageIndex++) {
				try {
					System.out.println("page:" + pageIndex);
					Document doc = Jsoup.connect(url)
							.data("pa", String.valueOf(pageIndex)).post();
					getData(doc, url);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

	private String getJobtypeByCondition(String jobtype) {
		String resultJobtype = "";
		String[] arrJobType = jobtype.split(" ");
		String jobtypeSelect = "";
		for (int index = 0; index < arrJobType.length; index++) {
			try {
				jobtypeSelect = elementListJobType
						.getElementsByTagName(arrJobType[index]).item(0)
						.getTextContent();
			} catch (Exception ex) {
				jobtypeSelect = "";
			}
			if (!jobtypeSelect.isEmpty()
					&& !resultJobtype.contains(jobtypeSelect))
				resultJobtype += jobtypeSelect + " ";
		}
		return resultJobtype;
	}

	private String getJobRequirementSpecialByCondition(
			String jobRequirementSpecial) {
		String resultjobRequirementSpecial = "";
		String[] arrjobRequirementSpeciale = jobRequirementSpecial.split(" ");
		String jobRequirementSpecialSelect = "";
		String value;
		for (int index = 0; index < arrjobRequirementSpeciale.length; index++) {
			value = arrjobRequirementSpeciale[index];
			value = value.replace("・", "").replace("（", "").replace("）", "")
					.trim();
			try {
				jobRequirementSpecialSelect = elementListJobRequirementSpecial
						.getElementsByTagName(value).item(0).getTextContent();
			} catch (Exception ex) {
				jobRequirementSpecialSelect = "";
			}
			if (!jobRequirementSpecialSelect.isEmpty()) {
				String[] arrjobRequirementSpecialSelect = jobRequirementSpecialSelect
						.split(" ");
				for (int i = 0; i < arrjobRequirementSpecialSelect.length; i++) {
					if (!resultjobRequirementSpecial
							.contains(arrjobRequirementSpecialSelect[i]))
						resultjobRequirementSpecial += arrjobRequirementSpecialSelect[i]
								+ " ";
				}

			}

		}
		return resultjobRequirementSpecial;
	}

	private static String getTagValues(final String str, String tagStart,
			String tagEnd) {
		String pattern = tagStart + "(.+?)" + tagEnd;
		Pattern TAG_REGEX = Pattern.compile(pattern);
		// final List<String> tagValues = new ArrayList<String>();
		final Matcher matcher = TAG_REGEX.matcher(str);
		if (matcher.find()) {
			return matcher.group(1).toString();
		} else
			return "";
		/*
		 * while (matcher.find()) { tagValues.add(matcher.group(1)); } return
		 * tagValues;
		 */
	}

	private static List<String> getListTagValues(final String str,
			String tagStart, String tagEnd) {
		String pattern = tagStart + "(.+?)" + tagEnd;
		Pattern TAG_REGEX = Pattern.compile(pattern);
		final List<String> tagValues = new ArrayList<String>();
		final Matcher matcher = TAG_REGEX.matcher(str);
		while (matcher.find()) {
			tagValues.add(matcher.group(1));
		}
		return tagValues;
	}
}