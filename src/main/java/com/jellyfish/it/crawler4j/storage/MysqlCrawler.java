package com.jellyfish.it.crawler4j.storage;

import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.james.mime4j.dom.datetime.DateTime;
import org.apache.poi.ss.formula.functions.Now;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.jellyfish.it.crawler4j.crawler.Page;
import com.jellyfish.it.crawler4j.crawler.WebCrawler;
import com.jellyfish.it.crawler4j.kirenisurunda.basic.JellyfishCrawlSiteMainController;
import com.jellyfish.it.crawler4j.kirenisurunda.basic.JellyfishCrawlerSiteMain;
import com.jellyfish.it.crawler4j.parser.HtmlParseData;
import com.jellyfish.it.crawler4j.sql.DbMySql;
import com.jellyfish.it.crawler4j.url.WebURL;
import com.mysql.jdbc.CallableStatement;
import com.mysql.jdbc.Connection;

public class MysqlCrawler {

	public String host = "127.0.0.1";
	public String port = "3306";
	public String dbName = "crawler";
	public String dbUser = "root";
	public String dbPwd = "123456";
	DbMySql mysql = null;

	private static MysqlCrawler instance = null;

	public static MysqlCrawler getInstance() {
		if (instance == null) {
			instance = new MysqlCrawler();
		}
		return instance;
	}

	public MysqlCrawler(String host, String port, String dbName, String dbUser,
			String dbPwd) {
		this.host = host;
		this.port = port;
		this.dbName = dbName;
		this.dbUser = dbUser;
		this.dbPwd = dbPwd;
		mysql = new DbMySql(host, port, dbName, dbUser, dbPwd);
	}

	public MysqlCrawler() {

	}

	public static MysqlCrawler createConn(String host, String port,
			String dbName, String dbUser, String dbPwd) {
		if (instance == null) {
			instance = new MysqlCrawler(host, port, dbName, dbUser, dbPwd);
		}
		return instance;
	}

	public Boolean insertURL(String url, String title, String content) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("url", url);
		params.put("title", title);
		params.put("content", content);
		params.put("date", date);

		String sqlUpdate = "" + "insert into " + "urls(" + "		url,"
				+ "		title," + "		content" + "	) " + "	values" + "	("
				+ "		${url}," + "		${title}," + "		${content}" + "	)";

		// System.out.println("sql" + sqlUpdate);

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean checkJobUrlHRContents(String jobUrl) {
		String sql = "select * from jfhr_contents_ra where job_url like '"
				+ jobUrl + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return true;
		}

	}

	public Boolean checkJobUrlHRContents_Tan(String jobUrl, String companyName) {
		String sql = "select * from jfhr_contents_ra where job_url like '"
				+ jobUrl + "' or company_name like '" + companyName + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			// return true;
			return false;
		}

	}

	// for ha
	public Boolean checkJobUrlHRContents_HA(String jobUrl, String companyName) {
		String sql = "select * from jfhr_contents_ra_ha where job_url like '"
				+ jobUrl + "' or company_name like '" + companyName + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			// return true;
			return false;
		}

	}

	public Boolean insertJFHRContents(Integer siteID, String jobUrl,
			String jobName, String jobLocation, String companyName,
			String companyAddress, String companyPhone, String companyContact,
			String companyWebsite, String job_posted, int count)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date job_PosterDate = new Date();
		if (!job_posted.isEmpty()) {
			try {
				job_PosterDate = df.parse(job_posted);
			} catch (Exception ex) {
				job_PosterDate = new Date();
			}

		}

		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_location", jobLocation);
		params.put("company_name", companyName);
		params.put("company_address", companyAddress);
		params.put("company_phone", companyPhone);
		params.put("company_contact", companyContact);
		params.put("company_website", companyWebsite);
		params.put("count", count);
		params.put("job_posted", job_PosterDate);
		params.put("created", date);
		// ki da jfhr_contents_vietnamwork
		// main jfhr_contents_ra
		String sqlUpdate = "" + "insert into " + "jfhr_contents_ra("
				+ "  site_id," + "  job_url," + "  job_name,"
				+ "  job_location," + "  company_name," + "  company_address,"
				+ "  company_phone," + "  company_contact,"
				+ "  company_website," + "  count," + "  job_posted,"
				+ "  created" + " ) " + " values" + " (" + "  ${site_id},"
				+ "  ${job_url}," + "  ${job_name}," + "  ${job_location},"
				+ "  ${company_name}," + "  ${company_address},"
				+ "  ${company_phone}," + "  ${company_contact},"
				+ "  ${company_website}," + "  ${count}," + "  ${job_posted},"
				+ "  ${created}" + " )";
		if (job_posted.isEmpty()) {
			sqlUpdate = "" + "insert into " + "jfhr_contents_ra("
					+ "  site_id," + "  job_url," + "  job_name,"
					+ "  job_location," + "  company_name,"
					+ "  company_address," + "  company_phone,"
					+ "  company_contact," + "  company_website," + "  count,"
					+ "  created" + " ) " + " values" + " (" + "  ${site_id},"
					+ "  ${job_url}," + "  ${job_name}," + "  ${job_location},"
					+ "  ${company_name}," + "  ${company_address},"
					+ "  ${company_phone}," + "  ${company_contact},"
					+ "  ${company_website}," + "  ${count}," + "  ${created}"
					+ " )";
		}

		// System.out.println("sql" + sqlUpdate);

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean UpdateJFHRContents(String job_url, String job_posted)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date job_PosterDate = df.parse(job_posted);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("job_url", job_url);
		params.put("job_posted", job_PosterDate);
		String sqlUpdate = "" + "update " + "jfhr_contents_ra" + " " + "set "
				+ " count = count + 1," + " job_posted = ${job_posted} "
				+ " where " + "job_url like ${job_url}";
		if (job_posted.isEmpty()) {
			sqlUpdate = "" + "update " + "jfhr_contents_ra" + " " + "set "
					+ " count = count + 1" + " where "
					+ "job_url like ${job_url}";
		}
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	// for vietnamwork 001
	public Boolean checkJobUrlHRContents_VNW_001(String jobUrl) {
		String sql = "select * from jfhr_contents_ra_nha where job_url like '"
				+ jobUrl + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return true;
		}

	}

	// for ha
	public Boolean checkJobUrlHRContents_HA(String jobUrl) {
		String sql = "select * from jfhr_contents_ra_ha where job_url like '"
				+ jobUrl + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return true;
		}

	}

	public Boolean insertJFHRContents_VNW_001(Integer siteID, String jobUrl,
			String jobName, String jobLocation, String companyName,
			String companyAddress, String companyPhone, String companyContact,
			String companyWebsite, String job_posted, int count)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date job_PosterDate = new Date();
		try {
			if (!job_posted.isEmpty()) {
				job_PosterDate = df.parse(job_posted);
			}
		} catch (Exception ex) {
			job_PosterDate = new Date();
		}

		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_location", jobLocation);
		params.put("company_name", companyName);
		params.put("company_address", companyAddress);
		params.put("company_phone", companyPhone);
		params.put("company_contact", companyContact);
		params.put("company_website", companyWebsite);
		params.put("count", count);
		params.put("job_posted", job_PosterDate);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "jfhr_contents_ra_nha("
				+ "  site_id," + "  job_url," + "  job_name,"
				+ "  job_location," + "  company_name," + "  company_address,"
				+ "  company_phone," + "  company_contact,"
				+ "  company_website," + "  count," + "  job_posted,"
				+ "  created" + " ) " + " values" + " (" + "  ${site_id},"
				+ "  ${job_url}," + "  ${job_name}," + "  ${job_location},"
				+ "  ${company_name}," + "  ${company_address},"
				+ "  ${company_phone}," + "  ${company_contact},"
				+ "  ${company_website}," + "  ${count}," + "  ${job_posted},"
				+ "  ${created}" + " )";
		if (job_posted.isEmpty()) {
			sqlUpdate = "" + "insert into " + "jfhr_contents_ra_nha("
					+ "  site_id," + "  job_url," + "  job_name,"
					+ "  job_location," + "  company_name,"
					+ "  company_address," + "  company_phone,"
					+ "  company_contact," + "  company_website," + "  count,"
					+ "  created" + " ) " + " values" + " (" + "  ${site_id},"
					+ "  ${job_url}," + "  ${job_name}," + "  ${job_location},"
					+ "  ${company_name}," + "  ${company_address},"
					+ "  ${company_phone}," + "  ${company_contact},"
					+ "  ${company_website}," + "  ${count}," + "  ${created}"
					+ " )";
		}

		// System.out.println("sql" + sqlUpdate);

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	// for ha
	public Boolean insertJFHRContents_HA(Integer siteID, String jobUrl,
			String jobName, String jobLocation, String companyName,
			String companyAddress, String companyPhone, String companyContact,
			String companyWebsite, String job_posted, int count)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date job_PosterDate = new Date();
		try {
			if (!job_posted.isEmpty()) {
				job_PosterDate = df.parse(job_posted);
			}
		} catch (Exception ex) {
			job_PosterDate = new Date();
		}

		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_location", jobLocation);
		params.put("company_name", companyName);
		params.put("company_address", companyAddress);
		params.put("company_phone", companyPhone);
		params.put("company_contact", companyContact);
		params.put("company_website", companyWebsite);
		params.put("count", count);
		params.put("job_posted", job_PosterDate);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "jfhr_contents_ra_ha("
				+ "  site_id," + "  job_url," + "  job_name,"
				+ "  job_location," + "  company_name," + "  company_address,"
				+ "  company_phone," + "  company_contact,"
				+ "  company_website," + "  count," + "  job_posted,"
				+ "  created" + " ) " + " values" + " (" + "  ${site_id},"
				+ "  ${job_url}," + "  ${job_name}," + "  ${job_location},"
				+ "  ${company_name}," + "  ${company_address},"
				+ "  ${company_phone}," + "  ${company_contact},"
				+ "  ${company_website}," + "  ${count}," + "  ${job_posted},"
				+ "  ${created}" + " )";
		if (job_posted.isEmpty()) {
			sqlUpdate = "" + "insert into " + "jfhr_contents_ra_ha("
					+ "  site_id," + "  job_url," + "  job_name,"
					+ "  job_location," + "  company_name,"
					+ "  company_address," + "  company_phone,"
					+ "  company_contact," + "  company_website," + "  count,"
					+ "  created" + " ) " + " values" + " (" + "  ${site_id},"
					+ "  ${job_url}," + "  ${job_name}," + "  ${job_location},"
					+ "  ${company_name}," + "  ${company_address},"
					+ "  ${company_phone}," + "  ${company_contact},"
					+ "  ${company_website}," + "  ${count}," + "  ${created}"
					+ " )";
		}

		// System.out.println("sql" + sqlUpdate);

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean UpdateJFHRContents_VNW_001(String job_url, String job_posted)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date job_PosterDate = new Date();
		if (!job_posted.isEmpty())
			job_PosterDate = df.parse(job_posted);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("job_url", job_url);
		params.put("job_posted", job_PosterDate);
		String sqlUpdate = "" + "update " + "jfhr_contents_ra_nha" + " "
				+ "set " + " count = count + 1,"
				+ " job_posted = ${job_posted} " + " where "
				+ "job_url like ${job_url}";
		if (job_posted.isEmpty()) {
			sqlUpdate = "" + "update " + "jfhr_contents_ra_nha" + " " + "set "
					+ " count = count + 1" + " where "
					+ "job_url like ${job_url}";
		}
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	// for ha
	public Boolean UpdateJFHRContents_HA(String job_url, String job_posted)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date job_PosterDate = new Date();
		if (!job_posted.isEmpty())
			job_PosterDate = df.parse(job_posted);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("job_url", job_url);
		params.put("job_posted", job_PosterDate);
		String sqlUpdate = "" + "update " + "jfhr_contents_ra_ha" + " "
				+ "set " + " count = count + 1,"
				+ " job_posted = ${job_posted} " + " where "
				+ "job_url like ${job_url}";
		if (job_posted.isEmpty()) {
			sqlUpdate = "" + "update " + "jfhr_contents_ra_ha" + " " + "set "
					+ " count = count + 1" + " where "
					+ "job_url like ${job_url}";
		}
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	// for get all url jfhr_contents_all_url
	public Boolean insertJFHRContents_All_Url(String Url) throws ParseException {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("job_url", Url);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "jfhr_contents_all_url("
				+ "  job_url," + "  created" + " ) " + " values" + " ("
				+ "  ${job_url}," + "  ${created}" + " )";

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			// ex.printStackTrace(System.out);
		}
		return false;
	}

	// public Boolean insertCrawlerByProcedure(Integer siteID, Integer
	// provinceID,
	// String jobUrl, String jobName, String jobDetail, String jobCompany,
	// String jobCareer, String jobSalary, String jobExpire,
	// String jobLocation, String jobAddress, String jobImage,
	// String jobType,String mainUrl) {
	// return mysql.insertCrawlerByProcedure(siteID, provinceID, jobUrl,
	// jobName, jobDetail, jobCompany, jobCareer, jobSalary, jobExpire,
	// jobLocation, jobAddress, jobImage, jobType, mainUrl);
	// }

	// public Boolean checkJobUrlContents(String jobUrl) {
	// String sql = "select * from contents where job_url like '" + jobUrl
	// + "'";
	// try {
	// List<Map<String, Object>> result = mysql
	// .executeStatement(sql, null);
	// if (result.size() > 0) {
	// return true;
	// } else {
	// return false;
	// }
	// } catch (Exception ex) {
	// System.out.print("loi connection is:" + ex.getMessage());
	// ex.printStackTrace(System.out);
	//
	// return true;
	// }
	//
	// }

	public String checkJobUrlContents(String jobUrl, int site_id) {
		String strResult = "";
		String sql = "select * from contents where job_url like '" + jobUrl
				+ "' and site_id =" + site_id + " limit 1";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return result.get(0).get("hash_code").toString();
			} else {
				return strResult;
			}
		} catch (Exception ex) {
			System.out.print("loi connection is:" + ex.getMessage());
			ex.printStackTrace(System.out);
		}
		return strResult;

	}

	public Boolean checkJobNameContents(String joName) {
		String sql = "select * from contents where job_name like '" + joName
				+ "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return true;
		}

	}

	public void deleteContentsBySite(int site) {
		String sql = "delete from contents where site_id = " + site;
		try {
			mysql.executeUpdate(sql, null);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
	}

	public void resetMySQL() {
		String sql = "RESET QUERY CACHE";
		try {
			mysql.executeUpdate(sql, null);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
	}

	public Boolean insertContents_bk(Integer siteID, Integer provinceID,
			String jobUrl, String jobName, String jobDetail, String jobCompany,
			String jobCareer, String jobSalary, String jobExpire,
			String jobLocation, String jobAddress, String jobImage,
			String jobType) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("province_id", provinceID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_detail", jobDetail);
		params.put("job_company", jobCompany);
		params.put("job_career", jobCareer);
		params.put("job_salary", jobSalary);
		params.put("job_expire", jobExpire);
		params.put("job_location", jobLocation);
		params.put("job_address", jobAddress);
		params.put("job_image", jobImage);
		params.put("job_type", jobType);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "contents(" + "		site_id,"
				+ "		province_id," + "		job_url," + "		job_name,"
				+ "		job_detail," + "		job_company," + "		job_career,"
				+ "		job_salary," + "		job_expire," + "		job_location,"
				+ "		job_address," + "		job_image," + "		job_type,"
				+ "		created" + "	) " + "	values" + "	(" + "		${site_id},"
				+ "		${province_id}," + "		${job_url}," + "		${job_name},"
				+ "		${job_detail}," + "		${job_company}," + "		${job_career},"
				+ "		${job_salary}," + "		${job_expire},"
				+ "		${job_location}," + "		${job_address},"
				+ "		${job_image}," + "		${job_type}," + "		${created}" + "	)"
				+ " WHERE job_url NOT IN (" + " SELECT job_url FROM contents)";

		// System.out.println("sql" + sqlUpdate); select * from contents where
		// job_url like '" + jobUrl

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean insertContents_Company(Integer siteID, Integer provinceID,
			String jobUrl, String jobName, String jobDetail, String jobCompany,
			String jobCareer, String jobSalary, String jobExpire,
			String jobLocation, String jobAddress, String jobImage,
			String jobType) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("province_id", provinceID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_detail", jobDetail);
		params.put("job_company", jobCompany);
		params.put("job_career", jobCareer);
		params.put("job_salary", jobSalary);
		params.put("job_expire", jobExpire);
		params.put("job_location", jobLocation);
		params.put("job_address", jobAddress);
		params.put("job_image", jobImage);
		params.put("job_type", jobType);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "contents(" + "		site_id,"
				+ "		province_id," + "		job_url," + "		job_name,"
				+ "		job_detail," + "		job_company," + "		job_career,"
				+ "		job_salary," + "		job_expire," + "		job_location,"
				+ "		job_address," + "		job_image," + "		job_type,"
				+ "		created" + "	) " + "	values" + "	(" + "		${site_id},"
				+ "		${province_id}," + "		${job_url}," + "		${job_name},"
				+ "		${job_detail}," + "		${job_company}," + "		${job_career},"
				+ "		${job_salary}," + "		${job_expire},"
				+ "		${job_location}," + "		${job_address},"
				+ "		${job_image}," + "		${job_type}," + "		${created}" + "	)"
				+ " WHERE job_url NOT IN (" + " SELECT job_url FROM contents)";

		// System.out.println("sql" + sqlUpdate); select * from contents where
		// job_url like '" + jobUrl

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	// using for get content for new request for convert uft 8
	public static String getStringForConvertUTF8(
			org.jsoup.nodes.Document document, ArrayList<String> listSelector) {
		String value = "";
		for (int i = 0; i < listSelector.size(); i++) {
			String result = "";
			try {
				result = document.select(listSelector.get(i)).text();
				value += result;
			} catch (Exception ex) {
				result = "";
			}

		}
		return value;
	}

	public Boolean insertContents(Integer siteID, Integer provinceID,
			String jobUrl, String jobName, String jobDetail, String jobCompany,
			String jobCareer, String jobSalary, String jobExpire,
			String jobLocation, String jobAddress, String jobImage,
			String jobType, String content, String hash_code,
			String contentForConvertUTF8, String jobRequirementSpecial,String stationName) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("province_id", provinceID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_detail", jobDetail);
		params.put("job_company", jobCompany);
		params.put("job_career", jobCareer);
		params.put("job_salary", jobSalary);
		params.put("job_expire", jobExpire);
		params.put("job_location", jobLocation);
		params.put("job_address", jobAddress);
		params.put("job_image", jobImage);
		params.put("job_type", jobType);
		params.put("created", date);
		params.put("content", content);
		params.put("hash_code", hash_code);
		params.put("contentForConvertUTF8", contentForConvertUTF8);
		params.put("requirement_special", jobRequirementSpecial);
		params.put("station_name", stationName);

		String sqlUpdate = ""
				+ "insert into "
				+ "contents("
				+ "		site_id,"
				+ "		province_id,"
				+ "		job_url,"
				+ "		job_name,"
				+ "		job_detail,"
				+ "		job_company,"
				+ "		job_career,"
				+ "		job_salary,"
				+ "		job_expire,"
				+ "		job_location,"
				+ "		job_address,"
				+ "		job_image,"
				+ "		job_type,"
				+ "		created"
				+ ",		content"
				+ ",		hash_code"
				+ ",		requirement_special"
				+ ",content_utf8,date_crawl,station_name) "
				+ "	values"
				+ "	("
				+ "		${site_id},"
				+ "		${province_id},"
				+ "		${job_url},"
				+ "		${job_name},"
				+ "		${job_detail},"
				+ "		${job_company},"
				+ "		${job_career},"
				+ "		${job_salary},"
				+ "		${job_expire},"
				+ "		${job_location},"
				+ "		${job_address},"
				+ "		${job_image},"
				+ "		${job_type},"
				+ "		${created},"
				+ "		${content},"
				+ "		${hash_code},"
				+ "		${requirement_special}"
				+ ",convert(${contentForConvertUTF8} using utf8) collate utf8_unicode_ci,${created},${station_name}) ";

		// System.out.println("sql_insert:" + sqlUpdate);

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.println("insert content");
			System.out.println(ex);
			// System.exit(0);
			ex.printStackTrace(System.out);
//			JellyfishCrawlSiteMainController.writeLogCrawler("site:" + siteID
//					+ " error insert: " + ex.getMessage());
			try {
				return mysql.executeUpdate(sqlUpdate, params);
			} catch (Exception ex2) {
				System.out.println("try insert content");
				System.out.println(ex2);
				// System.exit(0);
//				JellyfishCrawlSiteMainController.writeLogCrawler("site:"
//						+ siteID + " error insert: " + ex.getMessage());
			}
		}
		return false;
	}

	public Boolean insertContents_Crawl(Integer siteID, Integer provinceID,
			String jobUrl, String jobName, String jobDetail, String jobCompany,
			String jobCareer, String jobSalary, String jobExpire,
			String jobLocation, String jobAddress, String jobImage,
			String jobType, String content, String hash_code,
			String contentForConvertUTF8, String jobRequirementSpecial) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("province_id", provinceID);
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_detail", jobDetail);
		params.put("job_company", jobCompany);
		params.put("job_career", jobCareer);
		params.put("job_salary", jobSalary);
		params.put("job_expire", jobExpire);
		params.put("job_location", jobLocation);
		params.put("job_address", jobAddress);
		params.put("job_image", jobImage);
		params.put("job_type", jobType);
		params.put("created", date);
		params.put("content", content);
		params.put("hash_code", hash_code);
		params.put("contentForConvertUTF8", contentForConvertUTF8);
		params.put("requirement_special", jobRequirementSpecial);

		String sqlUpdate = ""
				+ "insert into "
				+ "contents_crawl("
				+ "		site_id,"
				+ "		province_id,"
				+ "		job_url,"
				+ "		job_name,"
				+ "		job_detail,"
				+ "		job_company,"
				+ "		job_career,"
				+ "		job_salary,"
				+ "		job_expire,"
				+ "		job_location,"
				+ "		job_address,"
				+ "		job_image,"
				+ "		job_type,"
				+ "		created"
				+ ",		content"
				+ ",		hash_code"
				+ ",		requirement_special"
				+ ",content_utf8,date_crawl) "
				+ "	values"
				+ "	("
				+ "		${site_id},"
				+ "		${province_id},"
				+ "		${job_url},"
				+ "		${job_name},"
				+ "		${job_detail},"
				+ "		${job_company},"
				+ "		${job_career},"
				+ "		${job_salary},"
				+ "		${job_expire},"
				+ "		${job_location},"
				+ "		${job_address},"
				+ "		${job_image},"
				+ "		${job_type},"
				+ "		${created},"
				+ "		${content},"
				+ "		${hash_code},"
				+ "		${requirement_special}"
				+ ",convert(${contentForConvertUTF8} using utf8) collate utf8_unicode_ci,${created} ) ";

		// System.out.println("sql_insert:" + sqlUpdate);

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.println("insert content");
			System.out.println(ex);
			// System.exit(0);
			ex.printStackTrace(System.out);
			try {
				return mysql.executeUpdate(sqlUpdate, params);
			} catch (Exception ex2) {
				System.out.println("try insert content");
				System.out.println(ex2);
				// System.exit(0);
			}
		}
		return false;
	}

	public Boolean UpdateContents(String jobUrl, String jobName,
			String jobDetail, String jobCompany, String jobCareer,
			String jobSalary, String jobExpire, String jobLocation,
			String jobAddress, String jobImage, String jobType) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_detail", jobDetail);
		params.put("job_company", jobCompany);
		params.put("job_career", jobCareer);
		params.put("job_salary", jobSalary);
		params.put("job_expire", jobExpire);
		params.put("job_location", jobLocation);
		params.put("job_address", jobAddress);
		params.put("job_image", jobImage);
		params.put("job_type", jobType);
		params.put("updated_at", date);

		String sqlUpdate = "" + "update " + "contents" + " " + "set "
				+ " job_name =  ${job_name}," + " job_detail = ${job_detail},"
				+ " job_company =  ${job_company},"
				+ " job_career = ${job_career},"
				+ " job_salary =  ${job_salary},"
				+ " job_expire = ${job_expire},"
				+ " job_location =  ${job_location},"
				+ " job_address = ${job_address},"
				+ " updated_at = ${updated_at},"
				+ " job_image =  ${job_image}," + " job_type = ${job_type} "
				+ " where " + "job_url like ${job_url}";

		// System.out.println("sqlUpdate:" + sqlUpdate);

		try {
			System.out.println("update content ok");
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.println("update content");
			System.out.println(ex);
			try {
				return mysql.executeUpdate(sqlUpdate, params);
			} catch (Exception ex2) {
				System.out.println("try update content");
				System.out.println(ex2);
			}
			// System.exit(0);
			ex.printStackTrace(System.out);
		}
		return false;
	}

	// public Boolean createTableLogDataByMonth() {
	// try {
	// DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
	// Date date = new Date();
	// String tableName = "datalog_" + dateFormat.format(date);
	// String sql = " ";
	// sql += "CREATE TABLE IF NOT EXISTS `" + tableName + "` (";
	// sql += "`id` int(10) unsigned NOT NULL AUTO_INCREMENT,";
	// sql += "`site_id` int(11) DEFAULT NULL,";
	// sql += "`main_url` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,";
	// sql += "`job_url` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,";
	// sql += "`count` int(10) DEFAULT NULL,";
	// sql += "`created` datetime DEFAULT NULL,";
	// sql += "`updated` datetime DEFAULT NULL,";
	// sql += "PRIMARY KEY (`id`)";
	// sql +=
	// ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
	// mysql.executeUpdate(sql, null);
	// return true;
	// } catch (Exception ex) {
	// ex.printStackTrace(System.out);
	// return false;
	// }
	// }
	public static Boolean statusJobNew = false;
	public static Boolean statusUpdateAll = false;
	public static Boolean statusUpdate_UpdateAt = false;

	public static Boolean statusInsertSuccess = false;
	public static Boolean statusUpdateALLSuccess = false;
	public static Boolean statusUpdateUpdateAtSuccess = false;

	public void delete_contents_crawl_BySite(int site) {
		String sql = "delete from contents_crawl where site_id = " + site;
		try {
			mysql.executeUpdate(sql, null);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
	}

	public int getCountRowDataFromContent_CrawlBySite(int site_id) {
		String sql = "select count(*) as count from contents_crawl where site_id = "
				+ site_id;
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return Integer.parseInt(result.get(0).get("count").toString());
			} else {
				return 0;
			}
		} catch (Exception ex) {
			System.out.print("loi connection is:" + ex.getMessage());
			ex.printStackTrace(System.out);

			return 0;
		}
	}

	public List<Map<String, Object>> getDataRowFromContentCrawlByQuery(
			String sql) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			list = result;
		} catch (Exception ex) {
			System.out.print("loi connection is:" + ex.getMessage());
			ex.printStackTrace(System.out);
		}
		return list;
	}

	public void processInsertContentsCrawler(int siteID, int provinceID,
			String jobUrl, String aJobName, String gJobDetail,
			String bJobCompany, String dJobCareer, String eJobSalary,
			String hJobExpire, String cJobLocation, String cLocationNear,
			String jobAddress, String jobImage, String fJobType,
			String content, String hash_code, String url,
			String contentForConvertUTF8, String jobRequirementSpecial,String stationName) {
		
		String hash_code_check = MysqlCrawler.getInstance()
				.checkJobUrlContents(jobUrl, siteID);
		if (hash_code_check.isEmpty()) {
			MysqlCrawler.getInstance().insertContents(siteID,
					provinceID, jobUrl, aJobName, gJobDetail, bJobCompany,
					dJobCareer, eJobSalary, hJobExpire,
					cJobLocation + "<br />" + cLocationNear, jobAddress,
					jobImage, fJobType, content, hash_code,
					contentForConvertUTF8, jobRequirementSpecial,stationName);
			statusJobNew = true;
			// insert first log
//			 MysqlCrawler.getInstance().insertLogData(siteID,
//			 url,
//			 jobUrl, 1);
		} else if (jobUrl.isEmpty() == false) {
			// source main
			if (!hash_code_check.equals(hash_code)) {
				MysqlCrawler.getInstance().Update_All_Contents(siteID,
						jobUrl, aJobName, gJobDetail, bJobCompany, dJobCareer,
						eJobSalary, hJobExpire,
						cJobLocation + "<br />" + cLocationNear, jobAddress,
						jobImage, fJobType, content, hash_code,
						contentForConvertUTF8, jobRequirementSpecial,stationName);
			} else
				MysqlCrawler.getInstance()
						.Update_Updated_at_Contents(siteID,jobUrl);
			statusJobNew = false;
			// insert count log
//			 MysqlCrawler.getInstance().UpdateLogData(jobUrl);
		}

	}

	public Boolean checkJobUrlContentsCrawl(String jobUrl) {
		String sql = "select * from contents_crawl where job_url like '"
				+ jobUrl + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			System.out.print("loi connection is:" + ex.getMessage());
			ex.printStackTrace(System.out);
			return true;
		}

	}

	/*
	 * chinh sua 16/08/2015 - chinh sua theo cach moi nhat insert ra table
	 * content_crawl khi insert xong duyet lai move qua table chinh tam thoi
	 * frame lai vi co the cach nay khong kha thi
	 */

	public Boolean updateStatusContentsByCrawlDate(int site_id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", site_id);
		// Date dateNow = new Date();
		// Calendar cal = Calendar.getInstance();
		// cal.setTime(dateNow);
		// cal.add(Calendar.DATE, -1); // minus number would decrement the days
		// Date dateBefore2Days = cal.getTime();
		// params.put("dateBefore2Days", dateBefore2Days);
		// String sqlUpdate =
		// "update contents set status = 0  where  date(date_crawl) < date(${dateBefore2Days}) and site_id = ${site_id}";
		String sqlUpdate = "update contents set status = 0  where  date(date_crawl) < date(now()) and site_id = ${site_id}";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			System.out.print("update datalog");
			// String log = site_id +
			// " update status date before one day ERROR";
			// JellyfishCrawlSiteMainController.writeLogCrawler(log);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	// update all site <> site3 status is 0 before one day.
	public Boolean updateStatusAllContentsByCrawlDate() {
		Map<String, Object> params = new HashMap<String, Object>();
		// Date dateNow = new Date();
		// Calendar cal = Calendar.getInstance();
		// cal.setTime(dateNow);
		// cal.add(Calendar.DATE, -1); // minus number would decrement the days
		// Date dateBefore2Days = cal.getTime();
		// params.put("dateBefore2Days", dateBefore2Days);
		// String sqlUpdate =
		// "update contents set status = 0  where  date(date_crawl) < date(${dateBefore2Days}) and site_id = ${site_id}";
		String sqlUpdate = "update contents set status = 0  where  date(date_crawl) < date(now()) and status = 1";
		try {
			mysql.executeUpdate(sqlUpdate, params);
			return true;
		} catch (Exception ex) {
			System.out.print(ex);
			// System.out.print("update datalog");
			// String log =
			// "update status date before one day all site <> 3 and status = 1 ERROR";
			// JellyfishCrawlSiteMainController.writeLogCrawler(log);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	// update status for site 3
	public Boolean updateStatusContentsForSite3ByCrawlDate() {
		Map<String, Object> params = new HashMap<String, Object>();
		Date dateNow = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateNow);
		cal.add(Calendar.DATE, -2); // minus number would decrement the days
		// Date dateBefore1Days = cal.getTime();
		int day = cal.get(cal.DAY_OF_MONTH);
		int month = cal.get(cal.MONTH) + 1;
		int year = cal.get(cal.YEAR);
		String sqlUpdate = "update contents set status = 0  where  date_crawl < STR_TO_DATE('%mm/%dd/%yy 14:00:00', '%m/%d/%Y %H:%i:%s') and site_id = 3 and status = 1";
		sqlUpdate = sqlUpdate.replace("%mm", String.valueOf(month))
				.replace("%dd", String.valueOf(day))
				.replace("%yy", String.valueOf(year));
		try {
			mysql.executeUpdate(sqlUpdate, params);
			return true;
		} catch (Exception ex) {
			System.out.print(ex);
			// System.out.print("update datalog");
			// String log =
			// "update status date before one day all site <> 3 and status = 1 ERROR";
			// JellyfishCrawlSiteMainController.writeLogCrawler(log);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public int countStatusContentsForSite3ByCrawlDate() {
		Date dateNow = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateNow);
		cal.add(Calendar.DATE, -2); // minus number would decrement the days
		// Date dateBefore1Days = cal.getTime();
		int day = cal.get(cal.DAY_OF_MONTH);
		int month = cal.get(cal.MONTH) + 1;
		int year = cal.get(cal.YEAR);
		String sql = "select count(*) as count from contents where  date_crawl >= STR_TO_DATE('%mm/%dd/%yy 14:00:00', '%m/%d/%Y %H:%i:%s') and site_id = 3 and status = 1";
		sql = sql.replace("%mm", String.valueOf(month))
				.replace("%dd", String.valueOf(day))
				.replace("%yy", String.valueOf(year));
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return Integer.parseInt(result.get(0).get("count").toString());
			} else {
				return 0;
			}
		} catch (Exception ex) {
			System.out.print("loi connection is:" + ex.getMessage());
			ex.printStackTrace(System.out);
			return 0;
		}
	}

	public Boolean exe_store_prc_add_list_city() {
		String sqlUpdate = "call prc_add_list_city";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore =
			// "exeute store exe_store_prc_add_list_city error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_store_prc_add_priority() {
		String sqlUpdate = "call prc_add_priority";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_priority error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_store_prc_add_list_em() {
		String sqlUpdate = "call prc_add_list_em";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_list_em error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_store_prc_add_list_hangup() {
		String sqlUpdate = "call prc_add_list_hangup";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_list_hangup error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_store_prc_add_list_job() {
		String sqlUpdate = "call prc_add_list_job";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_list_job error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_prc_add_list_job_parent() {
		String sqlUpdate = "call prc_add_list_job_parent";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_list_job_parent error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_prc_add_list_province() {
		String sqlUpdate = "call prc_add_list_province";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_list_province error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public Boolean exe_prc_prc_add_waysite() {
		String sqlUpdate = "call prc_add_waysite";
		try {
			return mysql.executeUpdate(sqlUpdate, null);
		} catch (Exception ex) {
			// System.out.print(ex);
			// System.out.print("update datalog");
			// String callStore = "exeute store prc_add_waysite error";
			// JellyfishCrawlSiteMainController.writeLogCrawler(callStore);
			// System.exit(0);
			ex.printStackTrace(System.out);
			return false;
		}
	}

	public int countSite3ByCrawlDateAndSiteId(int site_id) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", site_id);
		String sql = "select count(*) as count from contents where date(date_crawl) = date(now()) and site_id = ${site_id} and status = 1";
		try {
			List<Map<String, Object>> result = mysql.executeStatement(sql,
					params);
			if (result.size() > 0) {
				return Integer.parseInt(result.get(0).get("count").toString());
			} else {
				return 0;
			}
		} catch (Exception ex) {
			System.out.print("loi connection is:" + ex.getMessage());
			ex.printStackTrace(System.out);
			return 0;
		}
	}

	public static String hashBodyText(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			byte[] enc = md.digest();
			String md5Sum = new sun.misc.BASE64Encoder().encode(enc);
			return md5Sum;

		} catch (NoSuchAlgorithmException nsae) {

			System.out.println(nsae.getMessage());
			return null;
		}
	}

	public static org.jsoup.nodes.Document convertUrlToDocument(String url) {
		try {

			org.jsoup.Connection.Response response = Jsoup.connect(url)
			// enable for error urls
					.ignoreHttpErrors(true).followRedirects(false)
					// MAXIMUN TIME
					.timeout(30000)
					// This is to prevent producing garbage by attempting to
					// parse a JPEG binary image
					.ignoreContentType(true).execute();
			Page page = null;
			int status = 0;
			Document doc = null;
			if (response.statusCode() != 200) {
				WebURL url2 = new WebURL();
				url2.setURL(url);
				WebCrawler crawler = new WebCrawler();
				page = crawler.getProcessPage(url2);
				status = page.getStatusCode(); // response.statusCode();
				if (status == 200) {
					HtmlParseData htmlParseData = (HtmlParseData) page
							.getParseData();
					String html = htmlParseData.getHtml();
					doc = Jsoup.parse(html, "UTF-8");
				}
			} else
				doc = response.parse();
			// after done
			return doc;
		} catch (SocketTimeoutException se) {
			System.out.println("getContentOnly: SocketTimeoutException");
			System.out.println(se.getMessage());
			return null;
		} catch (Exception e) {
			System.out.println("getContentOnly: Exception");
			e.printStackTrace();
			return null;
		}
	}

	public static org.jsoup.nodes.Document convertUrlToDocumentSpecial(
			String url) {
		try {
			Page page = null;
			int status = 0;
			WebURL url2 = new WebURL();
			url2.setURL(url);
			WebCrawler crawler = new WebCrawler();
			page = crawler.getProcessPage(url2);
			status = page.getStatusCode(); // response.statusCode();
			// after done
			if (status == 200) {
				Document doc;
				HtmlParseData htmlParseData = (HtmlParseData) page
						.getParseData();
				String html = htmlParseData.getHtml();
				doc = Jsoup.parse(html, "UTF-8");
				return doc;
			} else
				return null;
		} catch (Exception e) {
			System.out.println("getContentOnly: Exception");
			e.printStackTrace();
			return null;
		}
	}

	public static String getContentFromDocumentCrawler(
			org.jsoup.nodes.Document detailJobUrlNew) {
		return detailJobUrlNew.body().text();
	}

	public Boolean insertEmail(String content_email_1, String content_email_2,
			String content_email_3, String content_email_4,
			String content_email_5, String content_email_6,
			String featured_picture) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("content_email_1", content_email_1);
		params.put("content_email_2", content_email_2);
		params.put("content_email_3", content_email_3);
		params.put("content_email_4", content_email_4);
		params.put("content_email_5", content_email_5);
		params.put("content_email_6", content_email_6);
		params.put("featured_picture", featured_picture);

		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "contents_email" + "("
				+ "		status," + "		header," + "		Title," + "		Subtitle,"
				+ "		Excerpt," + "		content," + "		relation_link,"
				+ "		featured_picture," + "		created" + "	) " + "	values"
				+ "	(" + "		1," + "		${content_email_1},"
				+ "		${content_email_2}," + "		${content_email_3},"
				+ "		${content_email_4}," + "		${content_email_5},"
				+ "		${content_email_6}," + " ${featured_picture},"
				+ "	${created}" + "	)";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			// System.exit(0);
			System.out.print("insert datalog");
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean insertLogData(int siteID, String main_url, String job_url,
			int count) {
		// DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		// String tableName = "datalog_" + dateFormat.format(date);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("main_url", main_url);
		params.put("job_url", job_url);
		params.put("count", count);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "data_log" + "("
				+ "		site_id," + "		main_url," + "		job_url," + "		count,"
				+ "		created" + "	) " + "	values" + "	(" + "		${site_id},"
				+ "		${main_url}," + "		${job_url}," + "		${count},"
				+ "		${created}" + "	)";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			// System.exit(0);
			System.out.print("insert datalog");
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean UpdateLogData(String job_url) {
		// DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		// String tableName = "datalog_" + dateFormat.format(date);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("updated", date);
		params.put("job_url", job_url);
		String sqlUpdate = "" + "update " + "data_log" + " " + "set "
				+ " count = count + 1," + " updated = ${updated} " + " where "
				+ "job_url like ${job_url}";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			System.out.print("update datalog");
			// System.exit(0);
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean checkJobContentNull_Contents(String job_url, String hasd_code) {
		String sql = "select * from contents where (content is null or hash_code <> '"
				+ hasd_code + "') and job_url like '" + job_url + "'";
		try {
			List<Map<String, Object>> result = mysql
					.executeStatement(sql, null);
			if (result.size() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return true;
		}

	}

	public Boolean Update_Updated_at_Contents(Integer siteID, String job_url) {
		// DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		// String tableName = "datalog_" + dateFormat.format(date);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("updated", date);
		params.put("job_url", job_url);
		String sqlUpdate = "" + "update " + "contents" + " " + "set "
				+ "status = 1,updated_at = ${updated},date_crawl = ${updated}"
				+ " where " + "job_url like ${job_url}";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			// System.exit(0);
			ex.printStackTrace(System.out);
			JellyfishCrawlSiteMainController.writeLogCrawler("site:" + siteID
					+ " error update at: " + ex.getMessage());
		}
		return false;
	}

	public Boolean Update_All_Contents(Integer siteId, String jobUrl,
			String jobName, String jobDetail, String jobCompany,
			String jobCareer, String jobSalary, String jobExpire,
			String jobLocation, String jobAddress, String jobImage,
			String fJobType, String content, String hash_code,
			String contentForConvertUTF8, String jobRequirementSpecial,String stationName) {
		// DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		// String tableName = "datalog_" + dateFormat.format(date);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("job_url", jobUrl);
		params.put("job_name", jobName);
		params.put("job_detail", jobDetail);
		params.put("job_company", jobCompany);
		params.put("job_career", jobCareer);
		params.put("job_salary", jobSalary);
		params.put("job_expire", jobExpire);
		params.put("job_location", jobLocation);
		params.put("job_address", jobAddress);
		params.put("job_image", jobImage);
		params.put("job_type", fJobType);
		params.put("updated_at", date);
		params.put("content", content);
		params.put("hash_code", hash_code);
		params.put("contentForConvertUTF8", contentForConvertUTF8);
		params.put("requirement_special", jobRequirementSpecial);
		params.put("station_name", stationName);

		String sqlUpdate = ""
				+ "update "
				+ "contents"
				+ " "
				+ "set "
				+ "status = 1,job_name = ${job_name},job_detail = ${job_detail},job_company = ${job_company} "
				+ ",job_career = ${job_career},job_salary = ${job_salary},job_expire = ${job_expire} "
				+ ",job_location = ${job_location},job_address = ${job_address},job_image = ${job_image} "
				+ ",job_type = ${job_type},updated_at = ${updated_at},content = ${content} "
				+ ",hash_code = ${hash_code} "
				+ ",station_name = ${station_name} "
				+ ",requirement_special = ${requirement_special} "
				+ ",content_utf8 = convert(${contentForConvertUTF8} using utf8) collate utf8_unicode_ci "
				+ ",date_crawl = ${updated_at} " + " where "
				+ "job_url like ${job_url}";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			// System.exit(0);
			ex.printStackTrace(System.out);
//			JellyfishCrawlSiteMainController.writeLogCrawler("site:" + siteId
//					+ " error update all: " + ex.getMessage());
		}
		return false;
	}

	public Boolean insertLogError(int siteID, String file, String error) {
		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("site_id", siteID);
		params.put("file", file);
		params.put("error", error);
		params.put("created", date);

		String sqlUpdate = "" + "insert into " + "error_log(" + "		site_id,"
				+ "		file," + "		error," + "		created" + "	) " + "	values"
				+ "	(" + "		${site_id}," + "		${file}," + "		${error},"
				+ "		${created}" + "	)";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			System.out.print(ex);
			// System.exit(0);
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public boolean insertDetail(Date calcDate, String gameCode,
			String idClassification, String timing, String timeValue,
			long totalAccount, long totalAccountAllClassification,
			float totalBehavior, float totalBehaviorAllClassification,
			String status) {

		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();

		// | IdBehaviorDetail | int(11) | NO | PRI | NULL | auto_increment |
		// | GameCode | varchar(5) | NO | MUL | NULL | |
		// | CalculateBy | varchar(20) | YES | | NULL | |
		// | CalculateValue | varchar(20) | YES | | NULL | |
		// | IdClassification | varchar(30) | NO | | NULL | |
		// | Status | varchar(20) | NO | | NULL | |
		// | AccountTotal | int(11) | YES | | NULL | |
		// | AccountTotalAllClassification | float | YES | | NULL | |
		// | BehaviorTotal | double | YES | | NULL | |
		// | BehaviorTotalAllClassification | float | YES | | NULL | |
		// | CreatedDate | datetime | NO | | NULL | |
		// | CreatedBy | varchar(10) | NO | | NULL | |

		params.put("GameCode", gameCode);
		params.put("CalculateBy", timing);
		params.put("CalculateValue", timeValue);
		params.put("CalculateDate", calcDate);

		params.put("IdClassification", idClassification);
		params.put("Status", status);

		params.put("AccountTotal", totalAccount);
		params.put("AccountTotalAllClassification",
				totalAccountAllClassification);
		params.put("BehaviorTotal", totalBehavior);
		params.put("BehaviorTotalAllClassification",
				totalBehaviorAllClassification);

		params.put("CreatedDate", date);
		params.put("CreatedBy", "ubsystem");
		String sqlUpdate = ""
				+ "insert into RC_Behavior_Detail"
				+ "("
				+ "	GameCode,"
				+ // com.jellyfish.it.crawler4j.crawler
				"	IdClassification," + "	CalculateBy," + "	CalculateValue,"
				+ "	AccountTotal," + "	AccountTotalAllClassification,"
				+ "	BehaviorTotal," + "	BehaviorTotalAllClassification,"
				+ "	Status," + "	CreatedDate," + "	CreatedBy,"
				+ "	CalculateDate" + ") " + "values" + "(" + "	${GameCode},"
				+ "	${IdClassification}," + "	${CalculateBy},"
				+ "	${CalculateValue}," + "	${AccountTotal},"
				+ "	${AccountTotalAllClassification}," + "	${BehaviorTotal},"
				+ "	${BehaviorTotalAllClassification}," + "	${Status},"
				+ "	${CreatedDate}," + "	${CreatedBy}," + "	${CalculateDate}"
				+ ")";

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public boolean insertDetailGrade(Date calcDate, String gameCode,
			String IdGrade, String preIdGrade, String timing, String timeValue,
			long totalAccount, String status) {

		Date date = new Date();
		Map<String, Object> params = new HashMap<String, Object>();

		// +-----------------------+-------------+------+-----+---------+----------------+
		// | Field | Type | Null | Key | Default | Extra |
		// +-----------------------+-------------+------+-----+---------+----------------+
		// | IdBehaviorDetailGrade | int(11) | NO | PRI | NULL | auto_increment
		// |
		// | GameCode | varchar(5) | NO | MUL | NULL | |
		// | CalculateBy | varchar(20) | YES | | NULL | |
		// | CalculateValue | varchar(20) | YES | | NULL | |
		// | IdGrade | varchar(20) | NO | | NULL | |
		// | Status | varchar(20) | NO | | NULL | |
		// | IdGradePrevious | varchar(20) | YES | | NULL | |
		// | AccountTotal | double | YES | | NULL | |
		// | CreatedDate | datetime | NO | | NULL | |
		// | CreatedBy | varchar(10) | NO | | NULL | |
		// | CalculateDate | date | YES | | NULL | |
		// +-----------------------+-------------+------+-----+---------+----------------+

		params.put("GameCode", gameCode);
		params.put("CalculateBy", timing);
		params.put("CalculateValue", timeValue);
		params.put("CalculateDate", calcDate);
		params.put("IdGrade", IdGrade);
		params.put("IdGradePrevious", preIdGrade);
		params.put("Status", status);
		params.put("AccountTotal", totalAccount);
		params.put("CreatedDate", date);
		params.put("CreatedBy", "ubsystem");
		String sqlUpdate = "" + "insert into RC_Behavior_Detail_Grade" + "("
				+ "	GameCode," + "	IdGrade," + "	IdGradePrevious,"
				+ "	CalculateBy," + "	CalculateValue," + "	AccountTotal,"
				+ "	Status," + "	CreatedDate," + "	CreatedBy,"
				+ "	CalculateDate" + ") " + "values" + "(" + "	${GameCode},"
				+ "	${IdGrade}," + "	${IdGradePrevious}," + "	${CalculateBy},"
				+ "	${CalculateValue}," + "	${AccountTotal}," + "	${Status},"
				+ "	${CreatedDate}," + "	${CreatedBy}," + "	${CalculateDate}"
				+ ")";

		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public Boolean updateTotalBehavior(String gameCode, String timing,
			String timeValue, long totalAccount, float totalBehavior) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("GameCode", gameCode);
		params.put("CalculateBy", timing);
		params.put("CalculateValue", timeValue);
		params.put("AccountTotalAllClassification", totalAccount);
		params.put("BehaviorTotalAllClassification", totalBehavior);
		String sqlUpdate = ""
				+ "update RC_Behavior "
				+ "set "
				+ "	AccountTotalAllClassification=${AccountTotalAllClassification}, "
				+ "	BehaviorTotalAllClassification=${BehaviorTotalAllClassification} "
				+ "where " + "GameCode=${GameCode} and "
				+ "CalculateBy=${CalculateBy} and "
				+ "CalculateValue=${CalculateValue}";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
		return false;
	}

	public Boolean updateTotalBehaviorDetail(String gameCode, String timing,
			String timeValue, long totalAccount, float totalBehavior) {
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("GameCode", gameCode);
		params.put("CalculateBy", timing);
		params.put("CalculateValue", timeValue);
		params.put("AccountTotalAllClassification", totalAccount);
		params.put("BehaviorTotalAllClassification", totalBehavior);
		String sqlUpdate = ""
				+ "update RC_Behavior_Detail "
				+ "set "
				+ "	AccountTotalAllClassification=${AccountTotalAllClassification}, "
				+ "	BehaviorTotalAllClassification=${BehaviorTotalAllClassification} "
				+ "where " + "GameCode=${GameCode} and "
				+ "CalculateBy=${CalculateBy} and "
				+ "CalculateValue=${CalculateValue}";
		try {
			return mysql.executeUpdate(sqlUpdate, params);
		} catch (Exception ex) {
			ex.printStackTrace(System.out);

		}
		return false;
	}
	/*
	 * public Boolean insert(Map<String, Object> params){ Date date = new
	 * Date(); params.put("CreatedDate", date); params.put("CreatedBy",
	 * "ubsystem"); String sqlUpdate =
	 * "insert into RC_Behavior(GameCode,IdClassification,CalculateBy,CalculateValue,AccountTotal,AccountTotalPercent,BehaviorTotal,BehaviorTotalPercent,CreatedDate,CreatedBy) values(${GameCode},${IdClassification},${CalculateBy},${CalculateValue},${AccountTotal},${AccountTotalPercent},${BehaviorTotal},${BehaviorTotalPercent},${CreatedDate},${CreatedBy})"
	 * ; try{ return mysql.executeUpdate(sqlUpdate, params); }catch(Exception
	 * ex){ ex.printStackTrace();
	 * 
	 * } return false; }
	 */

}
