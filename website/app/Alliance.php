<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;
use App\Http\Components\Search;

class Alliance extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    protected $table = 'alliance_job';
    public $timestamps = true;
	
	protected $list_job_a = array(
			4 => 5,
			21 => 1,
			22 => 3,
			23 => 2,
			13 => 4,
			17 => 4,
		);

    public $selectFields = array(
			'alliance_job.id',
			'alliance_job.job_requirement_id',
			'alliance_job.job_name',
			'alliance_job.list_job',
			'alliance_job.list_em',
			'alliance_job.list_station',
			'alliance_job.shop_id',
			'alliance_job.shop_name',
			'alliance_job.job_pr',
			'alliance_job.job_summary',
			'alliance_job.job_salary',
			'alliance_job.list_province',
			'alliance_job.list_city',
			'alliance_job.job_location',
			'alliance_job.access_type',
            'alliance_job.duration',
            'alliance_job.job_holiday',
            'alliance_job.job_wanted_people',
            'alliance_job.job_celebration',
            'alliance_job.company_name',
            'alliance_job.job_image',
            'alliance_job.job_thumbnail',
            'alliance_job.url',
            'alliance_job.job_career',
            'alliance_job.list_job_parent',
            'alliance_job.list_hangup',
            'alliance_job.is_new',
            'alliance_job.created_at',
	);
    
	
    protected $fillable = array(
            'id'
			,'job_requirement_id'
			,'priority'
			,'status'
			,'province_id'
			,'access_type'
			,'duration'
			,'shop_id'
			,'shop_name'
			,'company_name'
			,'job_url_clicked'
			,'job_name'
			,'job_pr'
			,'job_summary'
			,'job_detail'
			,'job_career'
			,'job_qualification'
			,'job_salary'
			,'job_salary_detail'
			,'job_expire'
			,'job_access_manual'
			,'job_location'
			,'job_location_detail'
			,'job_image'
			,'job_type'
			,'job_category'
			,'job_thumbnail'
			,'job_holiday'
			,'job_wanted_people'
			,'job_celebration'
			,'store_category'
			,'store_name'
			,'job_tag'
			,'employment_status'
			,'job_description'
			,'treatment_welfare'
			,'is_new'
			,'content'
			,'map_location'
			,'lng'
			,'lat'
			,'meta_desc'
			,'meta_keyword'
			,'list_province'
			,'list_city'
			,'list_waysite'
			,'list_job'
			,'list_job_parent'
			,'list_hangup'
			,'list_station'
			,'list_em'
			,'list_industry'
			,'application_and_selection'
			,'business_hours'
			,'regular_holiday'
			,'per_customer_price'
			,'number_of_seats'
			,'url'
			,'start_at'
			,'end_at'
		);
	public function getRelated($id, $company_id, $limit)
	{
		$result = 
			 $this->select('id','company_id','job_name')
			 ->where('company_id',$company_id)
			 ->where('id','!=', $id)
			 ->orderBy('created_at', 'desc')
			 ->skip(0)
			 ->take($limit)
			 ->get();
		
		return $result;
	}

	public function getStringCompany($id)
	{
		$resultCompany = $this->select('c.address', 'c.access', 'c.map_location')
					->from($this->table.' as s')
					->where('s.id', $id)
					->leftJoin('company as c', 'c.id', '=', 's.company_id')
					->get()->toArray();
		$resultCompany = $resultCompany[0];
		$stringCompany = $resultCompany['address'] . ' ' .  $resultCompany['access'] . ' ' . $resultCompany['map_location'];

		return $stringCompany;
	}

	public function updateListWaySite($id, $string='')
	{
		$result = DB::select( 
						DB::raw("select concat(',',group_concat(ws.line_cd separator ','),',') as listWaySite	
								from (select distinct waysite.line_cd, waysite.keywords from waysite) ws 
								where '$string' like concat('%',ws.`keywords`,'%')") 
						);
		if($result)
		{
			$result = $result[0]->listWaySite;
			$this->where('id', $id)
				->update(['list_waysite' => $result]);
		}
	}

	public function updateListProvince($id, $string='')
	{
		$result = DB::select( 
						DB::raw("select concat(',',group_concat(area.id separator ','),',') as listProvince
							from area 
							where '$string' like concat('%',area.`keywords`,'%') 
							and level = 0") 
						);
		
		if($result)
		{
			$result = $result[0]->listProvince;
			$this->where('id', $id)
				->update(['list_province' => $result]);
		}
	}

	public function updateListCity($id, $string='')
	{
		$result = DB::select( 
						DB::raw("select concat(',',group_concat(area.id separator ','),',') as listCity
							from area 
							where '$string' like concat('%',area.`keywords`,'%') 
							and level = 1") 
						);

		$result = $result[0]->listCity;

		$resultParent = DB::select( 
						DB::raw("select concat(',',group_concat(area.parent_id separator ','),',') as listParent
							from area 
							where '$string' like concat('%',area.`keywords`,'%') 
							and level = 1") 
						);
		if($resultParent)
		{
			$resultParent = $resultParent[0]->listParent;
		}
		else
		{
			$resultParent = '';
		}

		$resultProvince = $this->select('list_province')
							->where('id', $id)
							->get()->toArray();
		if($resultProvince)
		{
			$resultProvince = $resultProvince[0]['list_province'];
		}
		else
		{
			$resultProvince = '';
		}

		
		
		$arrProvince = explode(',', trim($resultProvince, ','));
		$arrParent = explode(',', trim($resultParent, ','));

		$arrResult = array_merge($arrProvince, $arrParent);
		$arrResult = array_unique($arrResult);
		
		if($arrResult)
		{
			$listProvince = implode(",", $arrResult);
			$listProvince = trim($listProvince, ',');
			$listProvince = ','.$listProvince.',';
			$this->where('id', $id)
				->update(['list_province' => $listProvince]);
		}
		else
		{
			$listProvince = null;
		}

		if($result)
		{
			$this->where('id', $id)
				->update(['list_city' => $result]);
		}
	}

	public function getDetail($id, $checkStatus = true)
	{
		$data = $this->select(
        	//secrec_job
            's.id',
            's.job_name',
            's.job_detail',
            's.job_career',
            's.job_salary',
            's.job_location',
            's.job_image',
            's.job_type',
            's.site_id',
            's.created_at as job_created_at',
            's.company_id', 's.content',
            's.job_notice_header',
            's.job_notice_content',
            's.job_notice_image',
            's.job_notice_image_sp',
            's.job_category',
            's.employment_status',
            's.job_description',
            's.job_salary_detail',
            's.job_qualification',
            's.job_location_detail',
            's.treatment_welfare',
            's.job_holiday',
            's.access',
            's.map_location',
            's.lng',
            's.lat',
            's.job_tag',
            's.list_em',
            's.list_hangup',
            's.list_job',
            's.list_job_parent',
            's.meta_desc',
            's.meta_keyword',
            's.list_province',

        	// v2 update
            's.store_name',
            's.start_at',
            's.end_at',

            //company
            'c.name',
            'c.logo',
            'c.category',
            'c.celebration',
            'c.average_price',
            'c.website',
            'c.address',
            'c.access_manual',
            'c.seat_number',
            'c.holiday',
            'c.hp',
            'c.short_introduction',
            'c.introduction',
            'c.access as company_access',
            'c.lat as company_lat',
            'c.lng as company_lng',
            'c.map_location as company_map_location'
        )
		->from($this->table.' as s')
		->where('s.id', $id)
		->leftJoin('company as c', 's.company_id', '=', 'c.id');
		
		$date = Carbon::now();
		
		if($checkStatus)
		{
            $data = $data->where('s.status', 1);
			$data = self::invaliddate($data);
		}
        $data = $data->get()->toArray();
		return $data;
	}


	public static function invaliddate($request){
	    return $request->whereRaw('date(start_at)<= date(now()) and date(end_at)>=date(now())');
    }
	
	
	public static function insertOrUpdate($record)
	{
		$find = self::where('job_requirement_id', $record['job_requirement_id'])->first();
		
		if($find)
		{
			$rs = $find->update($record);
			
			$record['id'] = $rs['id'];
			
			if($rs)
			{
				$record['add_status_message'] = 'update';
				$record['add_status'] = true;
			}
			else
			{
				$record['add_status_message'] = 'update_failed';
				$record['add_status'] = false;
			}
			return $record;
		}
		else
		{
			$rs = self::create($record);

			$record['id'] = $rs->id;
			$record['add_status_message'] = 'insert';
			$record['add_status'] = true;
			return $record;
		}
	}




    public function searchRandom($request, $paginate)
	{
		return $this->searchQuery($request)
					->orderByRaw("RAND()")
					->take($paginate)
					->get();
	}


	public function getAllianceData($request, $fromIndex, $toIndex, $randomNumber)
	{
		if($fromIndex == 0 && $toIndex == 0)
		{
			return [];
		}
		
		return $this->searchQuery($request)
					->orderByRaw("RAND($randomNumber)")
					->skip($fromIndex)
					->take($toIndex)
					->get();
	}

	public function getAllianceDataByFilter($conditions, $fromIndex, $toIndex, $randomNumber)
	{
		/*
		 * $toIndex => 0 => return []
		 * $toIndex => 1 => $fromIndex => 0 => return []
		 * $toIndex => 1 => $fromIndex => 1 => ...
		 * */
		if($toIndex)
		{
			if($fromIndex > -1){
				$rs = $this->filter($conditions)
									->orderByRaw("RAND($randomNumber)")
									->skip($fromIndex)
									->take($toIndex)
									->get();
				return $rs;
			}else{
				return [];
			}

		}else{
			return [];
		}



	}
	
	
	public function filter($conditions)
	{
		//TODO: filter		
		
		
		/*
			is_secret_job
			0: no
			1: yes
		*/
		$select = $this->selectFields;
		$select[] = \DB::raw('1 as is_secret');
		
		
		$contents = $this::select($select)
					->where('alliance_job.status','=','1');
		
		foreach( $conditions as $key=>$val)
		{
			if($key == 'list_job')
			{
				if(isset($this->list_job_a[$val]))
				{
					$val = $this->list_job_a[$val];
				}
			}
			
			$contents = $contents->where('alliance_job.'.$key, '=', $val);
		}
		return $contents;
	}

	public function countOfFilter($conditions)
	{	
		//Stop Secret Job at 2017-03-14
		//return 0;
		return $this->filter($conditions)
					->count();
	}


	/*
		For count only
	*/
	public function countOfSearch($request)
	{
		//Stop Secret Job at 2017-03-14
		//return 0;
		return $this->searchQuery($request)
					->count();
	}
	
	
	public function searchQuery($request)
	{
		//TODO: search
		$keyword 		= $request->input('k');
		$pro 			= $request->input('pro');
		$hangup 		= $request->input('hang');
		$jobCategory 	= $request->input('cat');
		$employment 	= $request->input('em');
		$city 			= $request->input('city');
		$way 			= $request->input('way');
		$station 		= $request->input('station');
		$site_id 		= $request->input('site_id');
		$no_image 		= $request->input('no_image');
		
		
		if($jobCategory)
		{
			if(is_array($jobCategory))
			{
				foreach( $jobCategory as $index => $val)
				{
					if(isset($this->list_job_a[$val]))
					{
						$jobCategory[$index] = $this->list_job_a[$val];
					}
				}
			}
			else
			{
				if(isset($this->list_job_a[$jobCategory]))
				{
					$jobCategory = $this->list_job_a[$jobCategory];
				}
			}
		}
		
        //SET current page
        /*if(isset($pars['page']) && $pars['page'] > 1)
        {
            $page = $pars['page'];
            Paginator::currentPageResolver(function() use ($page) {
                return $page;
            });
        }*/
        
		//#Enable show query log, in version 5, false is default value of QueryLog
		//DB::enableQueryLog();
		
		/*
			is_secret_job
			0: no
			1: yes
		*/
		$select = $this->selectFields;
		$select[] = \DB::raw('1 as is_secret');
		            		
		$contents = $this::select($select)
					->where('alliance_job.status','=','1');
		
		//Find jobs without IMAGE
		if(!empty($no_image))
		{
			$contents = $contents->where('alliance_job.job_image', '');
		}
		
		//Search by site_id
//		if(!empty($site_id))
//		{
//			$contents = $contents->where('secret.site_id', $site_id);
//		}
		
		//Search by keyword
		if(!empty($keyword))
		{
			//Advance keyword
			$searchComponent = new Search();
			$keywords = $searchComponent->makeKeyWord($keyword);
			
			foreach($keywords as $key)
			{
				$key = '%'.$key.'%';
				//$contents = $contents->where('contents.content_utf8', 'like', $key);
				$contents = $contents->where(function($query) use($key){
					$query->orWhere('alliance_job.job_name', 'like', $key);
					$query->orWhere('alliance_job.company_name', 'like', $key);
					$query->orWhere('alliance_job.job_career', 'like', $key);
					$query->orWhere('alliance_job.job_salary', 'like', $key);
					$query->orWhere('alliance_job.job_location', 'like', $key);
				});
			}
		}
		
		
		
		//Search by province
		if(!empty($pro))
		{
			$pro = '%,'.$pro.',%';
			$contents = $contents->where('alliance_job.list_province', 'like', $pro);
				
			//Search by Cities
			$contents = $this->conditionInListField($contents,'alliance_job.list_city', $city);
			
			//Search by Waysites
			//Search by station
			$contents = $this->conditionInOrGroup($contents, array(
                'alliance_job.list_waysite' => $way,
                'alliance_job.list_station' => $station,
            ));
			
		}
		
		//Search by employment
		$contents = $this->conditionInListField($contents,'alliance_job.list_em', $employment);

		//Search by job category
		$contents = $this->conditionInListField($contents,'alliance_job.list_job', $jobCategory);
        
		//Search by hang up
		$contents = $this->conditionInListField($contents,'alliance_job.list_hangup', $hangup);
		
        if (empty($station)) {
            $contents = $contents
                //->orderBy('contents.created_at','desc')
                ->orderBy('alliance_job.priority', 'desc');
        } else {
            $oderString = $this->orderByStation($station);
            $contents = $contents
                ->orderByRaw($oderString)
                ->orderBy('alliance_job.priority', 'desc') ;
        }
		return $contents;
	}
	
	private function conditionInListField($contents, $fieldName, $pars)
	{
		if(!empty($pars))
		{
			if(is_array($pars))
			{
				$pars = array_unique($pars);
				$contents = $contents->where(function ($query) use($pars, $fieldName){
								foreach( $pars as $value)
								{
									$query->orWhere($fieldName, '=', $value);
								}
							});
			}
			else
			{
				$contents = $contents->where($fieldName, '=', $pars);
			}
		}
		return $contents;
	}


	/*
        Input:
        array(
            'field_1' => array(1,2),
            'field_2' => '10',
            'field_3' => '',
            'field_4' => array()
        )
        ---
        Output:
        ... and ( field_1 like '%,1,%' or field_1 like '%,2,%' or field_2 like '%,10,%' ) ...

    */
    private function conditionInOrGroup($contents, $pars)
    {
        if (!empty($pars)) {
            $contents = $contents->where(function ($query) use ($pars) {
                foreach ($pars as $fieldName => $values) {
                    if (!empty($values)) {
                        if (is_array($values)) {
                            $values = array_unique($values);
                            foreach ($values as $value) {
                                $value = '%,' . $value . ',%';
                                $query->orWhere($fieldName, 'like', $value);
                            }

                        } else {
                            $values = '%,' . $values . ',%';
                            $query->orWhere($fieldName, 'like', $values);
                        }
                    }
                }
            });
        }
        return $contents;
    }

    private function orderByStation( $pars)
    {
        if (is_array($pars)) {
            $result = '(';
            $count = 0;
            $total = count($pars);
            foreach ($pars as $fieldName => $values) {
                $count++;
                $result .= $count == $total ? " alliance_job.list_station like '%," . $values . ",%'" : " alliance_job.list_station like '%," . $values . ",%' or";
            }
            $result .= ') desc';

        }

        return $result;
    }


			
	
	public static function updateByShopId($shopId, $strIndustryIds)
	{
		$rs = self::where('shop_id', $shopId)
			  //->where('status', 1)
			  ->update(['list_industry' => $strIndustryIds]);
		return $rs;
	}
	
}
