<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceBusinessCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    protected $table = 'aliance_business_category';
    //protected $fillable = array('keyword', 'count');
	
	

	public function getOnlyJobCategory()
	{
		//TODO: get only job cate gory (style = 1)
		return $this->select(['id','name'])
					->orderBy('id')
					->get();	
	}
}
