<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceEmploymentType extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    protected $table = 'aliance_employment_type';
    //protected $fillable = array('keyword', 'count');
	
	

	public function getEmployment()
	{
		//TODO: getEmployment
		return $this->select(['id','name'])
					->orderBy('id')
					->get();
	}
}
