<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllianceJobCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    protected $table = 'aliance_job_category';
    //protected $fillable = array('keyword', 'count');
	
	

	public function getOnlyJobCategory()
	{
		//TODO: get only job cate gory (style = 1)
		return $this->select(['id','name'])
					->orderBy('id')
					->get();	
	}

    public function getJobCategoryByStyle($style,$onTop =1,$status = 1)
    {
        $result= $this->select(['id','name', 'parent_id'])
            ->orderBy('id');

        $result =  $status!= 1 ? $result : $result->where('status',$status);
        return $result;
    }

    public function getParentCategoryByStyle($style,$onTop =1,$status = 1)
    {
        $result= $this->select(['id','name', 'parent_id'])
            ->where('parent_id',0)
            ->orderBy('parent_id')
            ->orderBy('id');
        $result =  $status!= 1 ? $result : $result->where('status',$status);
        return $result->get();
    }

    public function getById($id)
    {
        return $this->select(['id','name', 'parent_id'])
            ->where('id',$id)
            ->where('status',1)
            ->first();
    }
}
