<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    //
	protected $table = 'area';
	
	
	public function getProvince()
	{
		//TODO: getProvince
		return $this->select(['id','name','alias', 'group','job_amount'])
					->where('level',0)
					->orderBy('group')
					->orderBy('priority')
					->get();	
	}
	
	public function getAllCity()
	{
		//TODO: getAllCity
		return $this->select(['id','parent_id','name','alias', 'group','job_amount'])
					->where('level',1)
					->orderBy('group')
					->orderBy('parent_id')
					->get();	
	}
	
	public function getCitiesByProvince($provinceId)
	{
		//TODO: getCityByProvince
		return $this->select(['id','parent_id','name','alias', 'group','job_amount'])
					->where('parent_id',$provinceId)
					->orderBy('id')
					->get();	
	}
	
	public function getByAlias($alias)
	{
		return $this->select(['id','name','alias', 'parent_id','job_amount'])
					->where('alias',$alias)
					->first();
	}
	
	public function getById($id)
	{
		return $this->select(['id','name','alias', 'parent_id','job_amount'])
					->where('id',$id)
					->first();
	}
	
	public function getCityByIdAndParentId($id, $parent_id)
	{
		return $this->select(['id','name','alias', 'parent_id','job_amount'])
					->where('id',$id)
					->where('parent_id',$parent_id)
					->first();
	}
}
