<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategories extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    protected $table = 'article_content_category';
    public $timestamps = true;

    public function getListCategory()
    {
    	$categories = $this->select('category', 'category_alias', 'class_name', 'menu_class','menu_class_sp')
    					->where('status', 1)
    					->orderBy('priority')
    					->get()->toArray();

    	return $categories;
    }
}
