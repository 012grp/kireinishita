<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    protected $table = 'article_content';
    public $timestamps = true;

    protected $fillable = [
      'title',
      'description'
    ];

	/*
		@return latest Aritcles (default 5 items)
	*/
	public function getLatestContents($limit=5)
	{
		$newArticle = $this::select('a.id',
                                    'a.image',
                                    'a.image_thumb',
                                    'a.image_sp',
                                    'a.image_sp_thumb',
                                    'a.title',
                                    'a.view',
                                    'a.created_at',
                                    'acc.category', 
                                    'acc.category_alias',
                                    'acc.class_name',
                                    'acc.description as cat_description')
                            ->from('article_content as a')
                            ->where('a.status', 1)
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->orderBy('a.created_at', 'desc')
                            ->skip(0)->take($limit)->get();
		return $newArticle;
	}
}
