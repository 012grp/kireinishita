<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmarks extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
	protected $table = 'bookmarks';

	public function getBookmark($userId)
	{
		return $this->select('id', 'contentId', 'secretId', 'type', 'status')
                        ->from('bookmarks')
                        ->where('userId', $userId)
                        ->where('status', 1)
                        ->get();	
	}

    public function getBookmarkSecret($userId, $secretId)
    {
        return $this->select('id', 'contentId', 'secretId', 'type', 'status')
                    ->from('bookmarks')
                    ->where('userId', $userId)
                    ->where('status', 1)
                    ->where('type', 1)
                    ->where('secretId', $secretId)
                    ->first();
    }

    public function getBookmarkContent($userId, $contentId)
    {
        return $this->select('id', 'contentId', 'secretId', 'type', 'status')
                    ->from('bookmarks')
                    ->where('userId', $userId)
                    ->where('status', 1)
                    ->where('type', 0)
                    ->where('contentId', $contentId)
                    ->first();
    }

    public function getBookmarkAllience($userId, $contentId)
    {
        return $this->select('id', 'contentId', 'secretId','allience_id', 'type', 'status')
            ->from('bookmarks')
            ->where('userId', $userId)
            ->where('status', 1)
            ->where('type', 2)
            ->where('allience_id', $contentId)
            ->first();
    }

    public function checkBookmarkSecret($userId, $secretId)
    {
        return $this->select('id', 'contentId', 'secretId', 'type', 'status')
                    ->from('bookmarks')
                    ->where('userId', $userId)
                    //->where('status', 1)
                    ->where('type', 1)
                    ->where('secretId', $secretId)
                    ->first();
    }

    public function checkBookmarkContent($userId, $contentId)
    {
        return $this->select('id', 'contentId', 'secretId', 'type', 'status')
                    ->from('bookmarks')
                    ->where('userId', $userId)
                    //->where('status', 1)
                    ->where('type', 0)
                    ->where('contentId', $contentId)
                    ->first();
    }

    public function checkBookmarkAllience($userId, $contentId)
    {
        return $this->select('id', 'contentId', 'secretId','allience_id', 'type', 'status')
            ->from('bookmarks')
            ->where('userId', $userId)
            ->where('type', 2)
            ->where('allience_id', $contentId)
            ->first();
    }
}
