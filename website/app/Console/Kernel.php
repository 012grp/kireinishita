<?php

namespace App\Console;

use App\Http\Components\AllianceComponent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use Monolog\Logger as Monolog;
use Monolog\Handler\StreamHandler;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('inspire')
//            ->hourly();

        $schedule->call(function () {
            $alliance = new AllianceComponent();
            $listFile = $alliance->showListCSV();
            if(!empty($listFile[0])){
                $results = $alliance->processCrontab($listFile[0]);
            }

        })->cron('* */2 * * * *');
    }
}
