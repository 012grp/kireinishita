<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Components\Search;
use DB;

class Contents extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = true;
    protected $table = 'contents';

    public $selectFields = array(
        'id',
        'job_career',
        'job_name',
        'job_salary',
        'job_location',
        'job_url',
        'job_image',
        'job_type',
        'job_company',
        'job_career',
        'requirement_special',
        'is_new',
        'site_id',
        'list_job',
        'list_job_parent',
        'list_hangup',
        'list_em',
    );

    /*
        $conditions = [
            'field_name1' => value,
            'field_name2' => value,
        ]
    */
    public function filter($conditions)
    {
        //TODO: filter

        /*
            is_secret_job
            0: no
            1: yes
        */
        $select = $this->selectFields;
        $select[] = DB::raw('0 as is_secret');

        $contents = $this::select($select)->where('contents.status', '=', '1');

        foreach ($conditions as $key => $val) {
            $val = '%,' . $val . ',%';
            $contents = $contents->where('contents.' . $key, 'like', $val);
        }

        $contents = $contents
            //->orderBy('contents.created_at','desc')
            ->orderBy('contents.priority', 'desc');
        return $contents;
    }

    /*
        For search
    */
    public function search($request, $paginate)
    {
        return $this->searchQuery($request)
            ->paginate($paginate)
            ->appends($request->all());
    }

    public function getJobData($request, $fromIndex, $toIndex)
    {
        if($fromIndex == 0 && $toIndex == 0)
        {
            return [];
        }
        return $this->searchQuery($request)->skip($fromIndex)->take($toIndex)->get();
    }


    public function getJobDataByFilter($request, $fromIndex, $toIndex)
    {
        //\DB::enableQueryLog();
        if($fromIndex == 0 && $toIndex == 0)
        {
            return [];
        }

        $result= $this->filter($request)->skip($fromIndex)->take($toIndex)->get();

//        $queries = \DB::getQueryLog();
//        $formattedQueries = [];
//        foreach ($queries as $query) :
//            $prep = $query['query'];
//            foreach ($query['bindings'] as $binding) :
//                $binding = is_numeric($binding) ? $binding : "'$binding'";
//                $prep = preg_replace("#\?#", $binding, $prep, 1);
//            endforeach;
//            $formattedQueries[] = $prep;
//        endforeach;
//        print_r($formattedQueries);
//        print_r($queries);

        return $result;
    }


    /*
        For count only
    */
    public function countOfFilter($request)
    {
        return $this->filter($request)
                    ->count();
    }


    /*
        For count only
    */
    public function countOfSearch($request)
    {
        return $this->searchQuery($request)
            ->count();
    }

    /*
        @return Content collection
    */
    public function searchQuery($request)
    {
        //TODO: search

        $keyword = $request->input('k');
        $pro = $request->input('pro');
        $hangup = $request->input('hang');
        $jobCategory = $request->input('cat');
        $employment = $request->input('em');
        $city = $request->input('city');
        $way = $request->input('way');
        $station = $request->input('station');
        $site_id = $request->input('site_id');
        $no_image = $request->input('no_image');


        //SET current page
        /*if(isset($pars['page']) && $pars['page'] > 1)
        {
            $page = $pars['page'];
            Paginator::currentPageResolver(function() use ($page) {
                return $page;
            });
        }*/

        //#Enable show query log, in version 5, false is default value of QueryLog
        //DB::enableQueryLog();

        /*
            is_secret_job
            0: no
            1: yes
        */
        $select = $this->selectFields;
        $select[] = DB::raw('0 as is_secret');

        $contents = $this::select($select)->where('contents.status', '=', '1');

        //Find jobs without IMAGE
        if (!empty($no_image)) {
            $contents = $contents->where('contents.job_image', '');
        }

        //Search by site_id
        if (!empty($site_id)) {
            $contents = $contents->where('contents.site_id', $site_id);
        }

        //Search by keyword
        if (!empty($keyword)) {
            //Advance keyword
            $searchComponent = new Search();
            $keywords = $searchComponent->makeKeyWord($keyword);

            foreach ($keywords as $key) {
                $key = '%' . $key . '%';
                //$contents = $contents->where('contents.content_utf8', 'like', $key);
                $contents = $contents->where(function ($query) use ($key) {
                    $query->orWhere('contents.job_name', 'like', $key);
                    $query->orWhere('contents.job_company', 'like', $key);
                    $query->orWhere('contents.job_career', 'like', $key);
                    $query->orWhere('contents.job_salary', 'like', $key);
                    $query->orWhere('contents.job_location', 'like', $key);
                    $query->orWhere('contents.requirement_special', 'like', $key);
                });
            }
        }


        //Search by province
        if (!empty($pro)) {
            $pro = '%,' . $pro . ',%';
            $contents = $contents->where('contents.list_province', 'like', $pro);

            //Search by Cities
            $contents = $this->conditionInListField($contents, 'contents.list_city', $city);

            //Search by Waysites
            //Search by station
            $contents = $this->conditionInOrGroup($contents, array(
                'contents.list_waysite' => $way,
                'contents.list_station' => $station,
            ));
        }

        //Search by employment
        $contents = $this->conditionInListField($contents, 'contents.list_em', $employment);

        //Search by job category
        $contents = $this->conditionInListField($contents, 'contents.list_job', $jobCategory);

        //Search by hang up
        $contents = $this->conditionInListField($contents, 'contents.list_hangup', $hangup);

        if (empty($station)) {
            $contents = $contents
                //->orderBy('contents.created_at','desc')
                ->orderBy('contents.priority', 'desc');
        } else {
            $oderString = $this->orderByStation($station);
            $contents = $contents
                ->orderByRaw($oderString)
                ->orderBy('contents.priority', 'desc') ;
        }
        return $contents;
    }


    /*
        Structure of search, allow find in Contents and Secret table
    */
    private function searchOnTable($objTable)
    {

    }

    private function conditionInListField($contents, $fieldName, $pars)
    {
        if (!empty($pars)) {
            if (is_array($pars)) {
                $pars = array_unique($pars);
                $contents = $contents->where(function ($query) use ($pars, $fieldName) {
                    foreach ($pars as $value) {
                        $value = '%,' . $value . ',%';
                        $query->orWhere($fieldName, 'like', $value);
                    }
                });
            } else {
                $pars = '%,' . $pars . ',%';
                $contents = $contents->where($fieldName, 'like', $pars);
            }
        }
        return $contents;
    }

    /*
        Input:
        array(
            'field_1' => array(1,2),
            'field_2' => '10',
            'field_3' => '',
            'field_4' => array()
        )
        ---
        Output:
        ... and ( field_1 like '%,1,%' or field_1 like '%,2,%' or field_2 like '%,10,%' ) ...

    */
    private function conditionInOrGroup($contents, $pars)
    {
        if (!empty($pars)) {
            $contents = $contents->where(function ($query) use ($pars) {
                foreach ($pars as $fieldName => $values) {
                    if (!empty($values)) {
                        if (is_array($values)) {
                            $values = array_unique($values);
                            foreach ($values as $value) {
                                $value = '%,' . $value . ',%';
                                $query->orWhere($fieldName, 'like', $value);
                            }

                        } else {
                            $values = '%,' . $values . ',%';
                            $query->orWhere($fieldName, 'like', $values);
                        }
                    }
                }
            });
        }
        return $contents;
    }

    private function orderByStation( $pars)
    {
        if (is_array($pars)) {
            $result = '(';
            $count = 0;
            $total = count($pars);
            foreach ($pars as $fieldName => $values) {
                $count++;
                $result .= $count == $total ? " contents.list_station like '%," . $values . ",%'" : " contents.list_station like '%," . $values . ",%' or";
            }
            $result .= ') desc';

        }

        return $result;
    }
}
