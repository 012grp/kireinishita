<?php

namespace App\Http\Components;

use App\Alliance;
use File;
use Lang;
use Illuminate\Support\Facades\Log;
use Monolog\Logger as Monolog;
use Monolog\Handler\StreamHandler;

class AllianceComponent
{

    protected $csvDir = '/_alliance';
    protected $csvProcessDir = '/_alliance_process';

    protected $maximum_row = 1000;
    protected $maximum_row_message = 'cm.maximum_1000';

    protected $not_found_message = 'cm.not_found';

    protected $header = array(
        "求人id" => "job_requirement_id",
        "ジョブ名" => "job_name",
        "職種ID" => "list_job",
        "雇用形態ID" => "list_em",
        "駅ID" => "list_station",
        "店舗ID" => "shop_id",
        "店舗名" => "shop_name",
        "注目ポイント" => "job_pr",
        "仕事内容" => "job_summary",
        "給与" => "job_salary",
        "都道府県ID" => "list_province",
        "市区町村ID" => "list_city",
        "勤務地" => "job_location",
        "アクセスタイプ" => "access_type",
        "アクセス時間" => "duration",
        "アクセス地図(緯度)" => "lat",
        "アクセス地図(経度)" => "lng",
        "休日休暇" => "job_holiday",
        "求める人物について" => "job_wanted_people",
        "入社お祝い金" => "job_celebration",
        "企業名" => "company_name",
        "一覧画像" => "job_image",
        "詳細画像1" => "job_thumbnail",
        "送客URL" => "url",
    );

    protected $shopHeader = array(
        '店舗ID' => 'shop_id',
        '業種ID' => 'industry_id',
    );

    public function getCsvDir()
    {
        return base_path() . $this->csvDir;
    }

    public function getcsvProcessDir()
    {
        return base_path() . $this->csvProcessDir;
    }

    public function showLogFoder(){
        $root = !empty($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : env('DOCUMENT_ROOT','');
        $name = '_allience-'. date("Y-m-d-h-i-sa").'.log';
        echo $root. '/_alliance_process/'.$name;
        return $root. '/_alliance_process/'.$name;
    }

    public function showListCSV()
    {
        $files = scandir($this->getCsvDir());
        $rs = array();
        foreach ($files as $file) {
            if (strpos($file, 'jobnote_2') === 0) {
                $rs[] = $file;
            }
        }
        rsort($rs);
        return $rs;
    }

    public function checkFile($content)
    {
        $csv = new Csv();
        $rs = $csv->checkMaximumRow($content, $this->maximum_row);
        return $rs;
    }

    public function addAlliance($file_name)
    {
        $rsResult =array();

        $csv = new Csv();
        $path = $this->getCsvDir() . '/' . $file_name;
        $newPath = $this->getcsvProcessDir() . '/' . $file_name;
        $isMoveFile = File::move($path, $newPath);
//        $isMoveFile = false;
        if($isMoveFile) {
            $csv->setEncode($newPath);
            $handle = fopen($newPath, 'r');
        }else{
            $csv->setEncode($path);
            $handle = fopen($path, 'r');
        }
        //add log
        $monolog = Log::getMonolog();
        $monolog->pushHandler(new StreamHandler($this->showLogFoder(), Monolog::WARNING, false));

        $row = fgetcsv($handle, 1000, ',');
        $objHeader = $csv->makeHeaderPosition($this->header, $row);
        while (($row = fgetcsv($handle, 0, ',')) !== false) {
            $alliance = $csv->convertToObjectByHeader($objHeader, $row);
            $alliance = $this->processAlliance($alliance);
            $result = Alliance::insertOrUpdate($alliance);
            if($result['add_status_message'] == 'update_failed') {
                $rsResult[]['status'] = false;
                $rsResult[]['alliance'] = $alliance;
                $monolog->error('Allience job '.$result['add_status_message'].': ',$alliance);

            }else{
                $rsResult[]['status'] = true;
                $monolog->warning('Allience job '.$result['add_status_message'].': ',$alliance);
            }
        }
        fclose($handle);

        //$rsResult['data'] = $result;
        return $rsResult;
    }


    public function addShopBusinessCategories($jobnote_file_name)
    {
        $rsResult = array(
            'status' => true,
            'message' => '',
            'data' => null
        );

        $jobnoteShopCategory = str_replace('jobnote_2', 'jobnote_shop_business_categories_2', $jobnote_file_name);
        $path = $this->getCsvDir() . '/' . $jobnoteShopCategory;
        $newPath = $this->getcsvProcessDir() . '/' . $jobnoteShopCategory;
        $isMoveFile = File::move($path, $newPath);

        $csv = new Csv();
        if($isMoveFile) {
            $csv->setEncode($newPath);
            $allianceContent = $csv->getFileByPath($newPath);
        }else{
            $csv->setEncode($path);
            $allianceContent = $csv->getFileByPath($path);
        }
        $objHeader = $csv->makeHeaderPosition($this->shopHeader, $csv->getHeader());

        //add log
        $monolog = Log::getMonolog();
        $monolog->pushHandler(new StreamHandler($this->showLogFoder(), Monolog::WARNING, false));

        $newCategory = array();
        foreach ($allianceContent as $record) {
            $shop = $csv->convertToObjectByHeader($objHeader, $record);

            if (empty($newCategory[$shop['shop_id']])) {
                $newCategory[$shop['shop_id']] = array();
            }

            $newCategory[$shop['shop_id']][] = $shop['industry_id'];
        }


        //update
        $result = array();
        foreach ($newCategory as $_shop_id => $_list_industry_id) {
            $strIndustryIds = implode(',', $_list_industry_id);
            $strIndustryIds = ',' . $strIndustryIds . ',';
            $result = Alliance::updateByShopId($_shop_id, $strIndustryIds);
            if(!$result){
                $monolog->error('Allience shop update failed: ',array('shop_id'=>$_shop_id,'list_industry_id'=>$strIndustryIds));
            }else{
                $monolog->warning('Allience shop updated: ',array('shop_id'=>$_shop_id,'list_industry_id'=>$strIndustryIds));
            }
        }

        return !empty($result) ? $result : 1;
    }

    /*
        pass when exists:

        jobnote_20170512.csv
        jobnote_shop_business_categories_20170512.csv
    */
    public function checkExistFile($file_name)
    {
        $message = array();

        $jobnote = $this->getCsvDir() . '/' . $file_name;
        $jobnoteShopCategory = str_replace('jobnote_2', 'jobnote_shop_business_categories_2', $file_name);
        $jobnoteShopCategoryPath = $this->getCsvDir() . '/' . $jobnoteShopCategory;
        if (!file_exists($jobnote)) {
            $message[] = Lang::get($this->not_found_message, ['name' => $file_name]);
        }

        if (!file_exists($jobnoteShopCategoryPath)) {
            $message[] = Lang::get($this->not_found_message, ['name' => $jobnoteShopCategory]);
        }


        return $message;
    }


    //---
    public function processAlliance($alliance)
    {
		return $alliance;
        $alliance['job_location'] = str_replace('‾','~',$alliance['job_location']);
		
//        //list_city
//        $alliance['list_city'] = substr_replace($alliance['list_city'], '', 0, strlen($alliance['list_province']));
//        $alliance['list_city'] = ',' . $alliance['list_city'] . ',';
//
//        //list_station
//        $stationLen = strlen($alliance['list_station']);
//        $alliance['list_station'] = substr($alliance['list_station'], $stationLen - 5, $stationLen);
//        $alliance['list_station'] = ',' . $alliance['list_station'] . ',';
//
//        //list_province
//        $alliance['list_province'] = ',' . $alliance['list_province'] . ',';
//        $alliance['list_em'] = ',' . $alliance['list_em'] . ',';
//        return $alliance;
    }


    //---
    protected function file_get_contents_chunked($file, $chunk_size, $callback)
    {
        try {
            $handle = fopen($file, "r");
            $i = 0;
            while (!feof($handle)) {
                call_user_func_array($callback, array(fread($handle, $chunk_size), &$handle, $i));
                $i++;
            }

            fclose($handle);

        } catch (Exception $e) {
            trigger_error("file_get_contents_chunked::" . $e->getMessage(), E_USER_NOTICE);
            return false;
        }

        return true;
    }

    public function processCrontab($fileCsv){
        //Add job
        $rs = $this->addAlliance($fileCsv);

        //Add category
        $rsShopBusinessCategory = $this->addShopBusinessCategories($fileCsv);
        $result =  array('rs'=> $rs, 'rsShopBusinessCategory'=>$rsShopBusinessCategory);
        return $result;
    }
}