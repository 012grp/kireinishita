<?php
namespace App\Http\Components;

use App\Bookmarks;
use Session;

//component

//modal

class BookmarkComponent
{

    public static function check_login_to_load_bookmark($path)
    {
        //if not mybox
        if (strpos($path, 'mybox') === false) {
            if (UserComponent::is_login()) {
                return 1;
            }
        }
        return 0;
    }

    public static function check_bookmark($id, $is_secret_job)
    {
        /*
            1: bookmark
            0: no bookmark
        */
        $bookmark = new Bookmarks();

        $user = UserComponent::get_info();
        if ($is_secret_job) {
            if ($is_secret_job == 2) {
                if ($bookmark->getBookmarkAllience($user['id'], $id)) {
                    return 1;
                }
            } else {
                if ($bookmark->getBookmarkSecret($user['id'], $id)) {
                    return 1;
                }
            }
        } else {
            if ($bookmark->getBookmarkContent($user['id'], $id)) {
                return 1;
            }
            return 0;
        }
    }

    public static function set_bookmark_or_unbookmark($bookmark)
    {
        /*
            1: bookmark
            0: un-bookmark
            -2: error
        */
        try {
            if ($bookmark['attributes']['status'] == 0) //no bookmark
            {
                $bookmark->status = 1;
                $bookmark->save();
                return 1;
            } else //bookmarked
            {
                $bookmark->status = 0;
                $bookmark->save();
                return 0;
            }
        } catch (\Exception $e) {
            return -2;
        }

    }

    public static function bookmark_job($job_id, $is_secret_job)
    {
        /*
            -1: un-login
            1: bookmark
            0: un-bookmark
            2:allience job
        */
        if (!UserComponent::is_login()) {
            $url = app('Illuminate\Routing\UrlGenerator')->previous();
            Session::put('bookmarkJob.previous_page', $url);
            Session::put('bookmarkJob.id', $job_id);
            Session::put('bookmarkJob.is_secret_job', $is_secret_job);
            return -1;
        } else {
            $bookmark = new Bookmarks();
            $user = UserComponent::get_info();

            switch ($is_secret_job) {
                case 0:
                    $bookmark = $bookmark->checkBookmarkContent($user['id'], $job_id);
                    if ($bookmark) //exists in bookmarks table
                    {
                        return self::set_bookmark_or_unbookmark($bookmark);
                    } else //not exists in bookmarks table
                    {
                        try {
                            $bookmark = new Bookmarks();
                            $bookmark->insert([
                                'contentId' => $job_id,
                                'userId' => $user['id'],
                                'type' => 0,
                                'status' => 1
                            ]);

                            return 1;
                        } catch (\Exception $e) {
                            return -2;
                        }

                    }
                    break;
                case 1:
                    $bookmark = $bookmark->checkBookmarkSecret($user['id'], $job_id);
                    if ($bookmark) //exists in bookmarks table
                    {
                        return self::set_bookmark_or_unbookmark($bookmark);
                    } else //not exists in bookmarks table
                    {
                        try {
                            $bookmark = new Bookmarks();
                            $bookmark->insert([
                                'secretId' => $job_id,
                                'userId' => $user['id'],
                                'type' => 1,
                                'status' => 1
                            ]);
                            return 1;
                        } catch (\Exception $e) {
                            return -2;
                        }
                    }
                    break;
                case 2:
                    $bookmark = $bookmark->checkBookmarkAllience($user['id'], $job_id);
                    if ($bookmark) //exists in bookmarks table
                    {
                        return self::set_bookmark_or_unbookmark($bookmark);
                    } else //not exists in bookmarks table
                    {
                        try {
                            $bookmark = new Bookmarks();
                            $bookmark->insert([
                                'allience_id' => $job_id,
                                'userId' => $user['id'],
                                'type' => 2,
                                'status' => 1
                            ]);
                            return 1;
                        } catch (\Exception $e) {
                            return -2;
                        }
                    }
                    break;
            }
        }
    }

    public static function bookmark_job_after_login($job_id, $is_secret_job)
    {
        $bookmark = new Bookmarks();
        $user = UserComponent::get_info();

        if ($is_secret_job) {
            $bookmark = $bookmark->checkBookmarkSecret($user['id'], $job_id);
            if ($bookmark) //exists in bookmarks table
            {
                self::set_bookmark($bookmark);
            } else //not exists in bookmarks table
            {
                $bookmark = new Bookmarks();
                $bookmark->insert([
                    'secretId' => $job_id,
                    'userId' => $user['id'],
                    'type' => 1,
                    'status' => 1
                ]);
            }
        } else {
            $bookmark = $bookmark->checkBookmarkContent($user['id'], $job_id);
            if ($bookmark) //exists in bookmarks table
            {
                self::set_bookmark($bookmark);
            } else //not exists in bookmarks table
            {
                $bookmark = new Bookmarks();
                $bookmark->insert([
                    'contentId' => $job_id,
                    'userId' => $user['id'],
                    'type' => 1,
                    'status' => 1
                ]);
            }
        }
    }


    public static function set_bookmark($bookmark)
    {
        if ($bookmark['attributes']['status'] == 0) //no bookmark
        {
            $bookmark->status = 1;
            $bookmark->save();
        }
    }
}
