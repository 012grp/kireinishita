<?php

namespace App\Http\Components;


use Mockery\Exception;

class Csv
{

    var $delimiter = ',';
    var $enclosure = '"';
    var $filename = 'Export.csv';
    var $line = array();
    var $buffer;
    var $needToEncode = false;
    var $header = array();
    var $containHeader = true;

    function Csv()
    {
        $this->clear();
    }

    function clear()
    {
        $this->line = array();
        $this->buffer = fopen('php://temp/maxmemory:' . (5 * 1024 * 1024), 'r+');
    }

    function addField($value)
    {
        $this->line[] = $value;
    }

    function endRow()
    {
        $this->addRow($this->line);
        $this->line = array();
    }

    function addRow($row)
    {
        fputcsv($this->buffer, $row, $this->delimiter, $this->enclosure);
    }

    function renderHeaders()
    {
        header("Content-type:application/vnd.ms-excel");
        header("Content-disposition:attachment;filename=" . $this->filename);
    }

    function setFilename($filename)
    {
        $this->filename = $filename;
        if (strtolower(substr($this->filename, -4)) != '.csv') {
            $this->filename .= '.csv';
        }
    }

    function render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto")
    {
        if ($outputHeaders) {
            if (is_string($outputHeaders)) {
                $this->setFilename($outputHeaders);
            }
            $this->renderHeaders();
        }
        rewind($this->buffer);
        $output = stream_get_contents($this->buffer);
        if ($to_encoding) {
            $output = mb_convert_encoding($output, $to_encoding, $from_encoding);
        }
        return $output;
    }


    /*---------------------*/
    function checkFile($filename = 'csv')
    {
        if (isset($_FILES[$filename]))
            if ($_FILES[$filename]['type'] == 'application/vnd.ms-excel' || $_FILES[$filename]['type'] == 'text/csv')
                if ($_FILES[$filename]['error'] == 0)
                    if ($_FILES[$filename]['size'] > 0) {
                        return 1;
                    }
        return 0;
    }

    function setEncode($path)
    {
        $file = file_get_contents($path);
        if (!mb_detect_encoding($file)) {
            $this->needToEncode = true;
        } else {
            $this->needToEncode = false;
        }
    }

    function getFile($filename = 'csv')
    {
        $path = $_FILES[$filename]['tmp_name'];
        //get CSV
        $arrCsv = array_map('str_getcsv', file($path));
        return $arrCsv;
    }

    function checkMaximumRow($file, $maxNumber)
    {
        $fileTotal = count($file);
        if ($maxNumber < $fileTotal) {
            return false;
        }
        return true;
    }

    function getFileByPath($path)
    {
        //get CSV
        $arrCsv = array_map('str_getcsv', file($path));

        if ($this->containHeader) {
            $this->header = $arrCsv[0];
            unset($arrCsv[0]);
        }

        return $arrCsv;
    }

    function getHeader()
    {
        return $this->header;
    }

    function convertToObjectByHeader($header, $record)
    {
        $pushObj = [];
        //Need to Encode before return
        if ($this->needToEncode) {
            foreach ($header as $key => $index) {
                if (isset($record[$index])) {

                    //$pushObj[$key] = trim(iconv("SHIFT_JIS", "UTF-8//IGNORE//TRANSLIT", $record[$index]));
                    try{
                        $pushObj[$key] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE', $record[$index]));
                    }catch (\Exception $e){
                        error_log($e->getTraceAsString());
                        //echo 'Lỗi: ' .$e->getMessage();
                        throw $e;
                    }

//                    if($record[$index]=='38851'){
//                        //dd($header);
//                        $pushObj['job_name'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+1]));
//                        $pushObj['list_job'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+2]));
//                        $pushObj['list_em'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+3]));
//                        $pushObj['list_station'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+4]));
//                        $pushObj['shop_id'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+5]));
//                        $pushObj['shop_name'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+6]));
//                        $pushObj['job_pr'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+7]));
//                        $pushObj['job_summary'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+8]));
//                        $pushObj['job_salary'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+9]));
//                        $pushObj['list_province'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+10]));
//                        $pushObj['list_city'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+11]));
//                        $pushObj['job_location'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+12]));
//                        $pushObj['access_type'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+13]));
//                        $pushObj['duration'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+14]));
//                        $pushObj['lat'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+15]));
//                        $pushObj['lng'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+16]));
//                        $pushObj['job_holiday'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+17]));
//                        $pushObj['job_wanted_people'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE', $record[$index+18]));
//                        $pushObj['job_celebration'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+19]));
//                        $pushObj['company_name'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+20]));
//                        $pushObj['job_image'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+21]));
//                        $pushObj['job_thumbnail'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+22]));
//                        $pushObj['url'] = trim(iconv('shift-jis', 'utf-8'.'//IGNORE',  $record[$index+23]));
//                        dd($pushObj);
//                    }

                } else {
                    $pushObj[$key] = '';
                }
            }
        } else {
            foreach ($header as $key => $index) {
                if (isset($record[$index])) {
                    $pushObj[$key] = trim($record[$index]);
                } else {
                    $pushObj[$key] = '';
                }
            }
        }
        return $pushObj;
    }

    function makeHeaderPosition($obj, $header)
    {
        $newHeader = [];
        //find pos and set
        if ($this->needToEncode) {
            foreach ($header as $index => $name) {
                $name = trim(iconv("SHIFT_JIS", "UTF-8//IGNORE", $name));
                if (isset($obj[$name])) {
                    $newHeader[$obj[$name]] = $index;
                }
            }
        } //-----------------
        else {
            foreach ($header as $index => $name) {
                $name = trim($name);
                if (isset($obj[$name])) {
                    $newHeader[$obj[$name]] = $index;
                }
            }
        }

        return $newHeader;
    }

    /**
     * Array to Array key
     */
    function changeToObj($obj, $file, $trim = true)
    {
        /*
            OBJ:
            [
                'header 1' => 'column1',
                'header 2' => 'column2',
                'header 3' => 'column3',
            ]
            
            FILE:
            [
                0 => ['header 1', 'header 2', 'header 3'],
                1 => ['data1', 'data 2', 'data 3'],
                2 => ['data1', 'data 2', 'data 3'],
            ]
            
            RESULT:
            [
                0 => ['column1' => 'data1', 'column2' => 'data 2', 'column3' => 'data 3'],
                1 => ['column1' => 'data1', 'column2' => 'data 2', 'column3' => 'data 3'],
            ]
        */

        $header = $file[0];
        $newHeader = [];

        //find pos and set
        foreach ($header as $index => $name) {
            $name = trim($name);
            if (isset($obj[$name])) {
                $newHeader[$obj[$name]] = $index;
            }
        }

        unset($file[0]);
        $resultFile = [];
        foreach ($file as $record) {
            $pushObj = [];
            foreach ($newHeader as $key => $index) {
                if (isset($record[$index])) {
                    $pushObj[$key] =
                        $trim ?
                            trim($record[$index]) :
                            $record[$index];
                } else {
                    $pushObj[$key] = '';
                }
            }
            if ($pushObj) {
                $resultFile[] = $pushObj;
            }
        }

        return $resultFile;
    }

    function backup($prefix = 'payment', $filename = 'csv', $folderName = 'payment_logs')
    {
        $ul = new Util();
        $microtime = $ul->microtime();
        $newname = $prefix . '_' . $microtime . '.csv';
        $path = storage_path() . $folderName . '/' . $newname;

        return move_uploaded_file($_FILES[$filename]['tmp_name'], $path);
    }
}