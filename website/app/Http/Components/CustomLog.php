<?php

namespace App\Http\Components;

use Carbon\Carbon;

class CustomLog{
    
    //Write log to /storage/payment_logs/register_unpayment.log
    public function registerUnpayment($content, $path = '/payment_logs/register_unpayment.log')
    {
        $this->writeLog($content, $path);
    }
    
    public function writeLog($content, $path)
    {
        //Ex: /storage/logs/sample.log
        $path = storage_path().$path;
        error_log($content, 3, $path);
    }
}