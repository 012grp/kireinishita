<?php
namespace App\Http\Components;
use Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class EntryComponent
{

	public static function setSessionEntry($request)
	{
		$entryURL = Session::get('entry.entryURL');
				
				
        $info = array();

        $info['id'] = Session::get('entry.secret_id');
        $info['entryURL'] = $entryURL;
        $info['company_name'] = Session::get('entry.company_name');
        $info['average_price'] = Session::get('entry.average_price');
        $info['address'] = Session::get('entry.address');
        $info['company_email'] = Session::get('entry.company_email');
        $info['job_name'] = Session::get('entry.job_name');
        $info['province'] = $request->province;
		$info['city'] = $request->city;

		$info['year'] = $request->year;
        $info['month'] = $request->month;
        $info['day'] = $request->day;
        

        $info['name'] = $request->name;
        $info['kana'] = $request->kana;
        

        $arrGender = lang::get('entry.gender');

        $info['gender_value'] = $request->gender;
        $info['gender'] = $arrGender[$request->gender];

        $info['email'] = $request->email;
        $info['tel'] = $request->tel;

        $info['job_objective'] = $request->job_objective;
        $info['hope_employment'] = $request->hope_employment;

        $info['job_objective'] = isset($info['job_objective']) ? array_unique($info['job_objective']) : [];
        
        $info['hope_employment'] = isset($info['hope_employment']) ? array_unique($info['hope_employment']) : [];
        
		$info['job_objective_value'] = $info['job_objective'];
		$info['hope_employment_value'] = $info['hope_employment'];
		
        //handle job objective
        $info['job_objective'] = implode('、', $info['job_objective']);

        //handle hope employment
        $info['hope_employment'] = implode('、', $info['hope_employment']);
		
		//pr
		$info['pr'] = $request->pr;
		
		//contact
		$info['contact'] = $request->contact;
		//capabilities
		$info['capabilities'] = $request->capabilities;
		
		//checked
		$info['situation'] = $request->situation;
		$info['join'] = $request->join;
		
		$info['situation_value'] = Util::getLangByIndex('entry.current_situation', $info['situation']);
		$info['join_value'] = Util::getLangByIndex('entry.available_join', $info['join']);
		
		
		
        Session::put('entry', $info);
	}
}
