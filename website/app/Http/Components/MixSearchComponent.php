<?php
namespace App\Http\Components;
use Session;
use Illuminate\Support\Facades\Request;

//model
use App\Contents;
use App\SecretJob;
use App\Alliance;

//Components
use App\Http\Components\Util;

class MixSearchComponent
{
    /*
        Search by Conditions A
        Result:
            Job: 50
            Secret: 15
            
        Pagination: 20
        1. 
    */
    public $components = array('CloudSearch', 'ViewSearchProcess');
    
    protected $fromSecret = 0;
    protected $toSecret = 0;
    protected $fromJob = 0;
    protected $toJob = 0;
    protected $fromAlliance = 0;
    protected $toAlliance = 0;

    protected $total = 0;
    
    protected $totalSearch = 0;
    protected $totalSecret = 0;
    protected $size = 20;
    protected $ratioSecret = 2;
    protected $ratioJob = 16;
    protected $ratioAlliance = 2;
    
    public function initialize(Controller $controller) {
        //Add Secret tables
        $this->Secret = ClassRegistry::init('CareerOpportunity');
    }
    
	function __construct()
	{
		if (Util::isMobile()) {
			$this->setRatioSp(true);
		}
	}
	
    public function setSize($_size)
    {
        $this->size = $_size;
    }
    
    public function setRatio($secret, $job, $alliance)
    {
        $this->ratioSecret = $secret;
        $this->ratioJob = $job;
        $this->ratioAlliance = $alliance;
    }
    
	public function setRatioSp($isSp)
	{
		if($isSp)
		{
			$this->setRatio(2,6,2);
		}
	}
	
    public function getCountOnly($conditions)
    {
        $totalJob = $this->CloudSearch->countOnly($conditions);
        $totalSecret = $this->getSecretCountOnly($conditions);
        
        return ($totalJob + $totalSecret);
    }
    
    public function asynProcess($data)
    {
        $current_total = $data['current_total'];
        $total_job = $data['current_job'];
        $total_secret = $data['current_secret'];
        $index_job = 0;
        $index_secret = 0;
        
        $arrData = array();
        for($i=0; $i<$current_total; $i++)
        {
            if($i%5 == 0 && $index_secret < $total_secret)
            {
                $arrData[] = $this->ViewSearchProcess->secret($data['data_secret'][$index_secret]);
                $index_secret++;
            }
            else
            {
                if($index_job < $total_job)
                {
                    $arrData[] = $this->ViewSearchProcess->search($data['data_job'][0][$index_job]);
                    $index_job++;
                }
            }
        }
        return $arrData;
    }
        
    public function getMixSearch($conditions, $page=1, $size)
    {
        $ratioSecret = $this->ratioSecret;
        $ratioJob = $this->ratioJob;
        $ratioAlliance = $this->ratioAlliance;
        
        $contents =  new Contents();
        $totalJob = $contents->countOfSearch($conditions);

        $secretJob = new SecretJob();
        $totalSecret = $secretJob->countOfSearch($conditions);

        $alliance = new Alliance();
        $totalAlliance = $alliance->countOfSearch($conditions);
        
        //$this->mixTwoDataPagination($totalSecret, $totalJob, $page, $size, $ratioSecret, $ratioJob);
        $this->fibonacci($totalSecret, $totalJob, $totalAlliance, $ratioSecret, $ratioJob, $ratioAlliance, $page);
//      print_r(array(
//          // $size,
//          // $page,
//          $this->fromJob,
//          $this->toJob,
//          $this->fromSecret,
//          $this->toSecret,
//          // $totalJob,
//          // $totalSecret
//      ));
//      die;

        //Get Job 
        $data_job = $contents->getJobData($conditions, $this->fromJob, $this->toJob);
        if($data_job)
        {
            $data_job = $data_job->toArray();
        }
        //Get Secret
        if(!Session::has('mixSearchRamdom'))
        {
            Session::put('mixSearchRamdom', 1);
        }

        if($page == 1)
        {
            Session::put('mixSearchRamdom', rand(1, 100));
        }

        $numberRandom = Session::get('mixSearchRamdom');
        $data_secret = $secretJob->getSecretData($conditions, $this->fromSecret, $this->toSecret, $numberRandom);
        if($data_secret)
        {
            $data_secret = $data_secret->toArray();
        }

        $data_alliance = $alliance->getAllianceData($conditions, $this->fromAlliance, $this->toAlliance, $numberRandom);
        if($data_alliance)
        {
            $data_alliance = $data_alliance->toArray();
        }
        
        $total = $totalJob + $totalSecret + $totalAlliance;
        $current_job = count($data_job);
        $current_secret =  count($data_secret);
        $current_alliance =  count($data_alliance);
        $current_total = $current_job+$current_secret+$current_alliance;

        return array(
            "total_job" => $totalJob,
            "total_secret" => $totalSecret,
            "total_alliance" => $totalAlliance,
            "total_data" => $total,
            "total_page" => ceil($total/$size),
            "current_page" => $page,
            "current_total" => $current_total,
            "current_job" => $current_job,
            "current_secret" => $current_secret,
            "current_alliance" => $current_alliance,
            "size" => $size,
            "ratioSecret" => $ratioSecret,
            "ratioJob" => $ratioJob,
            "ratioAlliance" => $ratioAlliance,
            "data_job" => $data_job,
            "data_secret" => $data_secret,
            "data_alliance" => $data_alliance,
        );
    }
    
    public function getMixFilter($conditions, $page=1, $size)
    {
        $ratioSecret = $this->ratioSecret;
        $ratioJob = $this->ratioJob;
        $ratioAlliance = $this->ratioAlliance;
        
        $contents =  new Contents();
        $totalJob = $contents->countOfFilter($conditions);
        
        $secretJob = new SecretJob();
        $totalSecret = $secretJob->countOfFilter($conditions);
        
        $alliance = new Alliance();
        $totalAlliance = $alliance->countOfFilter($conditions);

        //$this->mixTwoDataPagination($totalSecret, $totalJob, $page, $size, $ratioSecret, $ratioJob);
        $this->fibonacci($totalSecret, $totalJob, $totalAlliance, $ratioSecret, $ratioJob, $ratioAlliance, $page);
//      print_r(array(
//          // $size,
//          // $page,
//          $this->fromJob,
//          $this->toJob,
//          $this->fromSecret,
//          $this->toSecret,
//          // $totalJob,
//          // $totalSecret
//      ));
//      die;

        //Get Job 
        $data_job = $contents->getJobDataByFilter($conditions, $this->fromJob, $this->toJob);
        if($data_job)
        {
            $data_job = $data_job->toArray();
        }

        //Get Secret
        if(!Session::has('mixSearchRamdom'))
        {
            Session::put('mixSearchRamdom', 1);
        }

        if($page == 1)
        {
            Session::put('mixSearchRamdom', rand(1, 100));
        }

        $numberRandom = Session::get('mixSearchRamdom');
        $data_secret = $secretJob->getSecretDataByFilter($conditions, $this->fromSecret, $this->toSecret, $numberRandom);
        if($data_secret)
        {
            $data_secret = $data_secret->toArray();
        }

        $data_alliance = $alliance->getAllianceDataByFilter($conditions, $this->fromAlliance, $this->toAlliance, $numberRandom);
        if($data_alliance)
        {
            $data_alliance = $data_alliance->toArray();
        }

        $total = $totalJob + $totalSecret + $totalAlliance;
        $current_job = count($data_job);
        $current_secret =  count($data_secret);
        $current_alliance =  count($data_alliance);

        $current_total = $current_job + $current_secret + $current_alliance;
        return array(
            "total_job" => $totalJob,
            "total_secret" => $totalSecret,
            "total_alliance" => $totalAlliance,
            "total_data" => $total,
            "total_page" => ceil($total/$size),
            "current_page" => $page,
            "current_total" => $current_total,
            "current_job" => $current_job,
            "current_secret" => $current_secret,
            "current_alliance" => $current_alliance,
            "size" => $size,
            "ratioSecret" => $ratioSecret,
            "ratioJob" => $ratioJob,
            "ratioAlliance" => $ratioAlliance,
            "data_job" => $data_job,
            "data_secret" => $data_secret,
            "data_alliance" => $data_alliance,
        );
    }

    public function getTotal()
    {
        $conditions = array(
                'page' => 1,
                'em'   => 0,
            );

        $this->Secret = ClassRegistry::init('CareerOpportunity');
        $size = $this->size;
        $ratioSecret = $this->ratioSecret;
        $ratioJob = $this->ratioJob;

        $totalJob = $this->CloudSearch->countOnly($conditions);
        $totalSecret = $this->getSecretCountOnly($conditions);
        return $totalJob + $totalSecret;
    }

    protected function mixTwoDataPagination($totalSecret, $totalJob, $page, $size, $ratioSecret, $ratioJob)
    {
        $totalRatio = $ratioSecret + $ratioJob;
        $totalSecretInPage = $size / $totalRatio * $ratioSecret;
        $totalJobInPage = $size / $totalRatio * $ratioJob;

        $noFullJobPage = intval($totalJob / $totalJobInPage) + 1;
        $incompleteAtNoFullJobPage = $totalJobInPage - ($totalJob % $totalJobInPage);
        $endSecretAtNoFullJobPage = $totalSecretInPage * $noFullJobPage + $incompleteAtNoFullJobPage;

        $noFullSecretPage = intval($totalSecret / $totalSecretInPage) + 1;
        $incompleteAtNoFullSecretPage = $totalSecretInPage - ($totalSecret % $totalSecretInPage);
        $endJobAtNoFullSecretPage = $totalJobInPage * $noFullSecretPage + $incompleteAtNoFullSecretPage;

        if($page > $noFullJobPage && $page > $noFullSecretPage)
        {
            $currentSecretInPage = 0;
            $currentJobInPage = 0;
        }
        else
        {
            if($noFullSecretPage < $noFullJobPage)
            {
                if($page < $noFullSecretPage)
                {
                    $currentSecretInPage = $totalSecretInPage;
                    $currentJobInPage = $totalJobInPage;
                }
                else if($page == $noFullSecretPage)
                {
                    $currentSecretInPage = $totalSecretInPage - $incompleteAtNoFullSecretPage;
                    $currentJobInPage = $size - ($totalSecretInPage - $incompleteAtNoFullSecretPage);
                }
                else //$page > $noFullSecretPage
                {
                    $currentSecretInPage = 0;
                    
                    if($totalJob >= $endJobAtNoFullSecretPage + $size * ($page - $noFullSecretPage))
                    {
                        $currentJobInPage = $size;
                    }
                    else
                    {
                        $currentJobInPage =  $totalJob - ($endJobAtNoFullSecretPage + ($page - $noFullSecretPage -1) * $size);
                        if($currentJobInPage < 0)
                        {
                            $currentJobInPage = 0;
                        }
                    }
                }
            }
            else if($noFullSecretPage == $noFullJobPage)
            {
                $currentSecretInPage = $totalSecretInPage - $incompleteAtNoFullSecretPage;
                $currentJobInPage = $totalJobInPage - $incompleteAtNoFullJobPage;
            }
            else //$noFullSecretPage > $noFullJobPage
            {

                $currentSecretInPage = $totalSecretInPage;
                if($noFullJobPage == $page)
                {
                    $currentJobInPage = $totalJobInPage - $incompleteAtNoFullJobPage;
                    if($currentJobInPage + $totalSecret < $size)
                    {
                        $currentSecretInPage = $totalSecret;
                    }

                }
                else if($noFullJobPage > $page)
                {
                    $currentJobInPage = $totalJobInPage;
                }
                else
                {
                    $currentJobInPage = 0;
                    $currentSecretInPage = $size;
                }
            }
        }
        
        if($currentSecretInPage == $totalSecretInPage && $currentJobInPage == $totalJobInPage)
        {
            $fromSecret = $totalSecretInPage * $page - $totalSecretInPage;
            $toSecret = $totalSecretInPage * $page;

            $fromJob = $totalJobInPage * ($page - 1);
            $toJob = $totalJobInPage * $page;
        }
        else 
        {
            if($currentJobInPage >= $totalJobInPage) //&& currentSecretInPage < totalSecretInPage
            {
                $incompleteSecret = $totalSecretInPage - $currentSecretInPage;
                if($currentSecretInPage != 0)
                {
                    $fromSecret = $totalSecretInPage * ($page - 1);
                    $toSecret = $totalSecretInPage * ($page - 1) + $currentSecretInPage;

                    $fromJob = $totalJobInPage * ($page - 1);
                    $toJob = $totalJobInPage * $page + $incompleteSecret;
                }
                else // $currentSecretInPage == 0
                {
                    $fromSecret = 0;
                    $toSecret = 0;
                    if($page == $noFullSecretPage)
                    {   
                        $fromJob = $totalJobInPage * ($page - 1);
                        $toJob = $totalJobInPage * $page + $incompleteSecret;
                    }
                    else //$page > $noFullSecretPage
                    {
                        $fromJob = $endJobAtNoFullSecretPage + $size * ($page - $noFullSecretPage - 1);
                        $toJob = $fromJob + $size;
                    }
                }

                if($toJob > $totalJob)
                {
                    $toJob = $totalJob;
                }
            }
            else if($currentSecretInPage >= $totalSecretInPage) //&& currentJobInPage < totalJobInPage
            {
                $incompleteJob = $totalJobInPage - $currentJobInPage;
                
                if($currentJobInPage != 0)
                {
                    $fromJob = $totalJobInPage * ($page - 1);
                    $toJob = $fromJob + $currentJobInPage;

                    $fromSecret = $totalSecretInPage * ($page - 1);
                    $toSecret = $totalSecretInPage * $page + $incompleteJob;

                    if($toSecret > $totalSecret)
                    {
                        $toSecret = $totalSecret;
                    }
                }
                else // $currentSecretInPage == 0
                {
                    $fromJob = 0;
                    $toJob = 0;

                    if($page == $noFullJobPage)
                    {
                        $fromSecret = $totalSecretInPage * ($page - 1);
                        $toSecret = $totalSecretInPage * $page + $incompleteJob;

                        if($toSecret > $totalSecret)
                        {
                            $toSecret = $totalSecret;
                        }
                    }
                    else
                    {
                        $incompleteAtNoFullJobPage = $totalJobInPage - ($totalJob % $totalJobInPage);

                        //$fromSecret = $totalSecretInPage * ($page - 1) + $incompleteAtNoFullJobPage;
                        $fromSecret = $endSecretAtNoFullJobPage + ($page - $noFullJobPage - 1) * $size;
                        $toSecret = $fromSecret + $currentSecretInPage;


                        if($toSecret > $totalSecret)
                        {
                            $toSecret = $totalSecret;
                        }

                        if($fromSecret >= $totalSecret)
                        {
                            $fromSecret = 0;
                            $toSecret = 0;
                        }
                    }

                }
            }
            else //currentSecretInPage < totalSecretInPage && currentJobInPage < totalJobInPage
            {
                
                if($currentJobInPage == 0)
                {
                    $fromJob = 0;
                    $toJob = 0;
                }

                if($currentSecretInPage ==0)
                {
                    $fromSecret = 0;
                    $toSecret = 0;
                }

                if($currentJobInPage != 0)
                {
                    $fromJob = $endJobAtNoFullSecretPage + ($page - $noFullSecretPage - 1) * $size;
                    $toJob = $fromJob + $currentJobInPage;
                }

                if($endJobAtNoFullSecretPage > $totalJob && $page == 1)
                {
                    $fromJob = 0;
                    $toJob = $totalJob;
                }

                if($endSecretAtNoFullJobPage > $totalSecret && $page == 1)
                {
                    $fromSecret = 0;
                    $toSecret = $totalSecret;
                }

                if($noFullJobPage == $noFullSecretPage && $noFullJobPage != 1)
                {
                    $fromJob = $totalJobInPage * ($page - 1);
                    $fromSecret = $totalSecretInPage * ($page - 1);

                    if($noFullJobPage != $page)
                    {
                        $toJob = $totalJobInPage * $page;
                        $toSecret = $totalSecretInPage * $page;
                    }
                    else
                    {
                        $toJob = $totalJob;
                        $toSecret = $totalSecret;
                    }

                    if($noFullJobPage < $page)
                    {
                        $fromJob = 0;
                        $toJob = 0;
                        $fromSecret = 0;
                        $toSecret = 0;
                    }
                }

                
            }
        }

        $this->fromSecret = $fromSecret;
        $this->toSecret = ($toSecret-$fromSecret);
        $this->fromJob = $fromJob;
        $this->toJob = ($toJob-$fromJob);
    }
    
    protected function getSecretCountOnly($conditions)
    {
        return $this->Secret->getTotalOnly($conditions);
    }
    
    protected function getSecretResult($conditions)
    {
        return $this->Secret->getSecretByRange($conditions, $this->fromSecret, $this->toSecret);
    }

    /**Fibonacci start**/
    public static function calculateDeductions($xTotal, $yTotal, $zTotal, $xDisplay, $yDisplay, $zDisplay){
        $xyz = [];
        $xDeduction; $yDeduction; $zDeduction;
        $buffer = $xDisplay + $yDisplay + $zDisplay;
        
        //if normal
        if($xTotal > $xDisplay){
            $xDeduction = $xDisplay;
        } else {
            $xDeduction = $xTotal;
        }
        
        if($yTotal > $yDisplay){
            $yDeduction = $yDisplay;
        } else {
            $yDeduction = $yTotal;
        }
        
        if($zTotal > $zDisplay){
            $zDeduction = $zDisplay;
        } else {
            $zDeduction = $zTotal;
        }
        
        $buffer = $buffer - $xDeduction - $yDeduction - $zDeduction;
        
        if($buffer > 0){
            if($xDeduction == $xDisplay && $xTotal > $yTotal && $xTotal > $zTotal){
                $xDeduction += $buffer;
            } else if ($yDeduction == $yDisplay && $yTotal > $xTotal && $yTotal > $zTotal){
                $yDeduction += $buffer;
            } else if ($zDeduction == $zDisplay && $zTotal > $xTotal && $zTotal > $yTotal){
                $zDeduction += $buffer;
            }
        }
        
        $xyz[0] = $xDeduction;
        $xyz[1] = $yDeduction;
        $xyz[2] = $zDeduction;
        
        
        return $xyz;
    }



    /*
     *          *Total - is the total number of items per variable
     *          *Display - is the number of items that can be displayed per variable.
     */
    public function fibonacci ($xTotal, $yTotal, $zTotal, $xDisplay, $yDisplay, $zDisplay, $currentPage){
        $page = 1;
        $buffer = [];
        //all variables start at 0
        $xTemp = $yTemp = $zTemp = 0;//the current positions of their pages.

        $totalPage = ($xTotal + $yTotal + $zTotal) / ($xDisplay + $yDisplay + $zDisplay);
        $totalPage = intval($totalPage) + 1;
        if($currentPage <= $totalPage)
        {
            while( ($xTotal > 0 || $yTotal > 0 || $zTotal > 0) && $page <= $currentPage){
                //echo ("Page "+$page)."<br>";
                $buffer = self::calculateDeductions( $xTotal, $yTotal, $zTotal, $xDisplay, $yDisplay, $zDisplay);
                $xDisplay = $buffer[0];
                $yDisplay = $buffer[1];
                $zDisplay = $buffer[2];
                
                //echo("x: ". $xTemp . "," . $xDisplay)."<br>";
                $this->fromSecret = $xTemp;
                $xTemp += $xDisplay;
                
                //echo ("y: ". $yTemp . "," . $yDisplay)."<br>";
                $this->fromJob = $yTemp;
                $yTemp += $yDisplay;

               // echo("z: ". $zTemp . "," . $zDisplay)."<br>";
                $this->fromAlliance = $zTemp;
                $zTemp += $zDisplay;
                
                
                
                $xTotal -= $xDisplay;
                $yTotal -= $yDisplay;
                $zTotal -= $zDisplay;
                $page++;
                
            }

            if($xDisplay == 0)
            {
                $this->fromSecret = 0;
            }
            if($yDisplay == 0)
            {
                $this->fromJob = 0;
            }
            if($zDisplay == 0)
            {
                $this->fromAlliance = 0;
            }
            $this->toSecret = $xDisplay;
            $this->toJob = $yDisplay;
            $this->toAlliance = $zDisplay;
        }
        else
        {
            $this->fromSecret = 0;
            $this->toSecret = 0;
            $this->fromJob = 0;
            $this->toJob = 0;
            $this->fromAlliance = 0;
            $this->toAlliance = 0;
        }
        
    }
    /**Fibonacci end**/
}
