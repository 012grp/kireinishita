<?php

namespace App\Http\Components;

class Search{
	
	public function makeKeyWord($keyword)
	{
		$allKeys = array();
		
		$keyword = $this->clearKeyWord($keyword);
		$splitKeyword = $this->splitKeyword($keyword);
		
		foreach( $splitKeyword as $key)
		{
			$multiKeys = $this->mutiKeyword($key);
			
			$allKeys = $allKeys + $multiKeys;
		}
		
		return $allKeys;
	}
	
	private function splitKeyword($keyword)
	{
		if (
                strpos($keyword,'“') !== false
                ||
                strpos($keyword,'”') !== false
                ||
                strpos($keyword,'ー') !== false
                ||
                strpos($keyword,'「') !== false
                ||
                strpos($keyword,'」') !== false
                ||
                strpos($keyword,'￥') !== false
                ||
                strpos($keyword,'’') !== false
                ||
                strpos($keyword,'、') !== false
                ||
                strpos($keyword,'。') !== false
                ||
                strpos($keyword,'・') !== false
                ||
                strpos($keyword,'（') !== false
                ||
                strpos($keyword,'）') !== false
                ||
                //NOTE: 2016-01-27 change "あ、い", "あ。い", "あ・い" to => あ い
                //strpos($keyword,'、') !== false
                //||
                //strpos($keyword,'。') !== false
                //||
                //strpos($keyword,'・') !== false
                //||
                strpos($keyword,'"') !== false
                ||
                strpos($keyword,'”') !== false
                ||
                strpos($keyword,'-') !== false
                ||
                strpos($keyword,'[') !== false
                ||
                strpos($keyword,']') !== false
                ||
                strpos($keyword,'\\') !== false
                ||
                strpos($keyword,'\'') !== false
                ||
                strpos($keyword,',') !== false
                ||
                strpos($keyword,'.') !== false
                ||
                strpos($keyword,'/') !== false
                ||
                strpos($keyword,'(') !== false
                ||
                strpos($keyword,')') !== false
            ) {
                $spc1B = ['"','"','-','[',']','\\','\'',',','.','/','（','）'];
                $spc2B = ['“','”','ー','「','」','￥','’','、','。','・','(',')'];
                
                $keywordTo2B = str_replace($spc1B, $spc2B, $keyword);
                $keywordTo1B = str_replace($spc2B, $spc1B, $keyword);
                
				return array($keywordTo2B,$keywordTo1B);
            }
		
			return array($keyword);
	}
	
	/*
		Make array keyword with white-space
	*/
	private function mutiKeyword($keyword)
	{
		$arrKeys = explode(' ', $keyword);
		foreach( $arrKeys as $index=>$key)
		{
			if(empty($key))
			{
				unset($arrKeys[$index]);
			}
		}
		return $arrKeys;
	}
	
	
	public function clearKeyWord($keyword)
    { 
        /*
            Replace multiple space to 1 space
        */
        /*
            東京　渋谷
            東京　　渋谷
            東京　　　渋谷
            東京　　　　　　渋谷
            東京 渋谷
            東京 　渋谷
        */
        $keyword = str_replace('　', ' ', $keyword);
        
        $keyword = preg_replace('/\s\s+/', ' ', $keyword);
        
        
        /*
            Remove spaces on head and tail of keyword
        */
        $keyword = trim($keyword);
        
        return $keyword;
    }
	
	private function checkInvalidKeyword($keyword)
	{
		switch($keyword)
		{
			case 'ul':
			case 'li':
			case 'dd': return false;
			default:
				return true;
		}
	}
}

