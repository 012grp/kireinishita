<?php

namespace App\Http\Components;
use Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
//model
use App\Articles;
use App\ArticleCategories;
use App\SecretJob;

use Illuminate\Support\Facades\Lang;
class SiteMap{

	public static function renew()
	{
		//list contents category
		$listCategorySite = ArticleCategories::select('id', 'category_alias')
											->where('status', 1)
											->get()
											->toArray();


		/* $arrCategories = [
				1 => "trend"
			    2 => "interview"
			    3 => "know-how"
			    4 => "community"
			    5 => "skill-up"
			    7 => "work"
			]
		*/
		$priorityCat = "0.8";
		$priorityDetail = "0.7";
		$arrCategories = [];						
		$contentURL = '/contents/';
		$secretJobURL = '/' . Util::getUrlName('secretPage') . '/';
		$dynamicSite = [];
		foreach ($listCategorySite as $cat) {
			$arrCategories[$cat['id']] = $cat['category_alias'];
			$arrSite = [$priorityCat, $contentURL . $cat['category_alias']];
			array_push($dynamicSite, $arrSite);
		}

		//list detail
		$listContentsDetailSite = Articles::select('id', 'cat_id')
										->where('status', 1)
										->orderBy('cat_id', 'asc')
										->orderBy('id', 'desc')
										->get()
										->toArray();

		foreach ($listContentsDetailSite as $detail) {
			$catID = $detail['cat_id'];
			$detailID = $detail['id'];
			$urlDetail = $contentURL . $arrCategories[$catID] . '/' . $detailID;
			$arrSite = [$priorityDetail, $urlDetail];
			array_push($dynamicSite, $arrSite);
		}

		//list Secret job 
		$listSecretJob = SecretJob::select('id')
								->where('status', 1)
								->get()
								->toArray();

		foreach ($listSecretJob as $secret) {
			$arrSite = [$priorityDetail, $secretJobURL . $secret['id']];
			array_push($dynamicSite, $arrSite);
		}
		$staticSite = lang::get('static_site');
		$staticSiteMix1 = lang::get('static_site_mix1');
		$arrSiteMap = [];
		$arrSiteMap = array_merge($staticSite, $dynamicSite);
		$arrSiteMap = array_merge($arrSiteMap, $staticSiteMix1);

		return $arrSiteMap;
	}

	public static function handleSiteMapRequest($string)
	{
		$arr = explode("\r\n", $string);
		$url = '';
		$priority = '';
		$output = array();
		foreach($arr as $line)
		{
			$line = trim($line);
		    if(!$line){
		    	continue; 
		    }
		    preg_match("/^\d\.\d+$/", $line, $match);
		    //is priority
		    if($match)
		    {
		        $priority = $line;
		        if($url)
		        {
		            $output[] = array('url'=>$url, 'priority' => $priority);
		            $url = '';
		        }
		    }
		    else
		    {
		        if($url)
		        {
		            $output[] = array('url'=>$url, 'priority' => '');
		        }
		        $url = $line;
		    }
		}

		if($url)
		{
			$output[] = array('url'=>$url, 'priority' => '');
		}

		return $output;
	}

	public static function create($arrSiteMap)
	{
		if(file_exists("resources/views/pages/sitemap.blade.php"))
		{
			$site_map_file = 'resources/views/pages/sitemap.blade.php';
			
			self::backupSiteMapFile($site_map_file);

			$streamResource = fopen($site_map_file, 'w') or die('Cannot open file:  '.$site_map_file);

			$data = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
			$data .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' . "\n\n";

			foreach ($arrSiteMap as $site) {
				$url = $site['url'];
				$url = htmlspecialchars($url, ENT_QUOTES);

				if($url)
				{
					$data .= '<url>' . "\n";
					$data .= "\t" . "<loc>$url</loc>" . "\n";
					if($site['priority'])
					{
						$data .= "<priority>$site[priority]</priority>" . "\n";
					}
					$data .= '</url>'. "\n";
				}
			}
			$data .= '</urlset>';
			return fwrite($streamResource, $data);
		}
		return false;
	}

	/*
		function use for auto create site map
		input url: web url
		arrSiteMap: 

			[
				["1.0","/"],
		 		["0.8","/contents"],
		 		....
			]
	*/
	public static function createStringSiteMap($arrSiteMap)
	{
		$url = Request::root();
		
		$stringSiteMap = "";
		foreach($arrSiteMap as $site)
		{
			if($site[1])
			{
				$stringSiteMap .= $url . $site[1] . "\r\n";
			}
			if($site[0])
			{
				$stringSiteMap .= $site[0] . "\r\n";
			}
		}

		return $stringSiteMap;
	}

	public static function backupSiteMapFile($siteMapPath)
	{
		$backupSiteMapPath = 'public/bk_sitemap/'. date('Y-m-d h-i-s') .'-sitemap.blade.php';
		$folderBackup = 'public/bk_sitemap';
		if(file_exists($folderBackup))
		{
			if (is_writable('public/bk_sitemap'))
			{
				copy($siteMapPath, $backupSiteMapPath);
			}
			else
			{
				Log::info("Folder backup site map not have write permission");
			}
		}
		else
		{
			Log::info("Not exists folder backup site map");
		}
	}
}