<?php

namespace App\Http\Components;

class ThumbsComponent {

  /**
   * thumbs
   *
   * @params  string
   * @params  string
   *
   * @return
   */
public function thumbs ($imgfile = "", $path, $maxWidth, $maxHeight="",$crop=0, $arr_more=array())
  {

    $info = @getimagesize($imgfile);
    $mime = $info[2];
    $fext = ($mime == 1 ? 'image/gif' : ($mime == 2 ? 'image/jpeg' : ($mime == 3 ? 'image/png' : NULL)));
    switch ($fext)
    {
      case 'image/jpeg':
        if (! function_exists('imagecreatefromjpeg'))
        {
          die('No create from JPEG support');
        } else
        {
          $img['src'] = @imagecreatefromjpeg($imgfile);
        }
        break;
      case 'image/png':
        if (! function_exists('imagecreatefrompng'))
        {
          die("No create from PNG support");
        } else
        {
          $img['src'] = @imagecreatefrompng($imgfile);
        }
        break;
      case 'image/gif':
        if (! function_exists('imagecreatefromgif'))
        {
          die("No create from GIF support");
        } else
        {
          $img['src'] = @imagecreatefromgif($imgfile);
        }
        break;
    }
    $img['old_w'] = @imagesx($img['src']);
    $img['old_h'] = @imagesy($img['src']);

    if($crop){
      // Ratio cropping
      $offsetX	= 0;
      $offsetY	= 0;

      if (!$maxWidth && $maxHeight)	{
        $maxWidth	= 99999999999999;
      }elseif ($maxWidth && !$maxHeight){
        $maxHeight	= 99999999999999;
      }

      $cropRatio		= explode(':', (string) $crop );
      if (count($cropRatio) == 2)
      {
        $ratioComputed		= $img['old_w'] /  $img['old_h'];
        $cropRatioComputed	= (float) $cropRatio[0] / (float) $cropRatio[1];

        if ($ratioComputed < $cropRatioComputed)
        { // Image is too tall so we will crop the top and bottom
          $origHeight	= $img['old_h'];
          $img['old_h']		= $img['old_w'] / $cropRatioComputed;
          $offsetY	= ($origHeight - $img['old_h']) / 2;
        }
        else if ($ratioComputed > $cropRatioComputed)
        { // Image is too wide so we will crop off the left and right sides
          $origWidth	= $img['old_w'];
          $img['old_w']		= $img['old_h'] * $cropRatioComputed;
          $offsetX	= ($origWidth - $img['old_w']) / 2;
        }
      }

      $xRatio		= $maxWidth / $img['old_w'];
      $yRatio		= $maxHeight / $img['old_h'];
      $height = 1;
      if ($xRatio * $height < $maxHeight)
      { // Resize the image based on width
        $new_h	= ceil($xRatio * $img['old_h']);
        $new_w	= $maxWidth;
      }
      else // Resize the image based on height
      {
        $new_w	= ceil($yRatio * $img['old_w']);
        $new_h	= $maxHeight;
      }

    }else{
      $new_h = $img['old_h'];
      $new_w = $img['old_w'];
      $offsetX=0;
      $offsetY = 0 ;

      if(!empty($arr_more["fix_width"])==1)
      {
        $new_w = $maxWidth;
        $new_h = ($maxWidth / $img['old_w']) * $img['old_h'];
      }elseif(!empty($arr_more["fix_height"])==1)	{
        $new_h = $maxHeight;
        $new_w = ($maxHeight / $img['old_h']) * $img['old_w'];
      }elseif(!empty($arr_more["fix_min"])==1)
      {
        if ($img['old_w'] > $img['old_h'])
        {
          $new_h = $maxHeight;
          $new_w = ($maxHeight / $img['old_h']) * $img['old_w'];

          if($new_w < $maxWidth)
          {
            $new_w = $maxWidth;
            $new_h = ($maxWidth / $img['old_w']) * $img['old_h'];
          }
        }
        else
        {
          $new_w = $maxWidth;
          $new_h = ($maxWidth / $img['old_w']) * $img['old_h'];

          if($new_h < $maxHeight)
          {
            $new_h = $maxHeight;
            $new_w = ($maxHeight / $img['old_h']) * $img['old_w'];
          }
        }
      }
      elseif(!empty($arr_more["fix_max"])==1)
      {
        if($maxWidth > 0 && $maxHeight > 0)
        {
          $tl = $img['old_w'] / $img['old_h'];
          $tl_get = $maxWidth / $maxHeight;

          if ($tl > $tl_get)
          {
            if ($img['old_w'] > $maxWidth)
            {
              $new_w = $maxWidth;
              $new_h = ($maxWidth / $img['old_w']) * $img['old_h'];
            }
          }
          else
          {
            if ($img['old_h'] > $maxHeight)
            {
              $new_h = $maxHeight;
              $new_w = ($new_h / $img['old_h']) * $img['old_w'];
            }
          }
        }
      }
      elseif(!empty($arr_more["zoom_max"])==1)
      {
        $tl = $img['old_w'] / $img['old_h'];
        $tl_get = $maxWidth / $maxHeight;

        if ($tl_get > $tl)
        {
          $new_h = $maxHeight;
          $new_w = ($maxHeight / $img['old_h']) * $img['old_w'];
        }
        else
        {
          $new_w = $maxWidth;
          $new_h = ($maxWidth / $img['old_w']) * $img['old_h'];
        }
      }
      else
      {
        if($maxWidth && $maxHeight)
        {
          $new_w = $maxWidth;
          $new_h = $maxHeight;
        }else{
          $new_h = $img['old_h'];
          $new_w = $img['old_w'];
          $offsetX=0;
          $offsetY = 0 ;

          if ($img['old_w'] > $maxWidth)
          {
            $new_w = $maxWidth;
            $new_h = ($maxWidth / $img['old_w']) * $img['old_h'];
          }
          if ($new_h > $maxWidth)
          {
            $new_h = $maxWidth;
            $new_w = ($new_h / $img['old_h']) * $img['old_w'];
          }
        }
      }
    }

    $img['des'] = @imagecreatetruecolor($new_w, $new_h);
    if($fext=="image/png"){
      @imagealphablending($img['des'], false );
      @imagesavealpha($img['des'], true);
    }else{
      $white = @imagecolorallocate($img['des'], 255, 255, 255);
      @imagefill($img['des'], 1, 1, $white);
    }
    @imagecopyresampled($img['des'], $img['src'], 0, 0, $offsetX, $offsetY, $new_w, $new_h, $img['old_w'], $img['old_h']);
    if(!empty($arr_more['watermark'])){

      $info_watermark['image_watermark'] = $arr_more['watermark'];
      $info_watermark['image_watermark_no_zoom_out'] = true;
      $info_watermark['image_watermark_no_zoom_in']  = true;
      $info_watermark['image_watermark_position'] = ($arr_more['watermark_position']) ? $arr_more['watermark_position'] : "TL";
      $info_watermark['image_watermark_x'] = ($arr_more['image_watermark_x']) ? $arr_more['image_watermark_x'] : "";
      $info_watermark['image_watermark_y'] = ($arr_more['image_watermark_y']) ? $arr_more['image_watermark_y'] : "";
      $info_watermark['image_dst'] = $img['des'] ;
      $info_watermark['image_dst_x'] = $new_w;
      $info_watermark['image_dst_y'] = $new_h;
      $this->add_watermark($info_watermark);
    }
    //	print "path = ".$path."<br>";
    @touch($path);
    switch ($fext)
    {
      case 'image/pjpeg':
      case 'image/jpeg':
      case 'image/jpg':
        @imagejpeg($img['des'], $path, 90);
        break;
      case 'image/png':
        @imagepng($img['des'], $path);
        break;
      case 'image/gif':
        @imagegif($img['des'], $path, 90);
        break;
    }
    // Finally, we destroy the images in memory.
    @imagedestroy($img['des']);
  }

  /*-------------- add_watermark --------------------*/
  function add_watermark ($info)
  {
    // add watermark image
    if (@file_exists($info['image_watermark'])) {
      $info['image_watermark_position'] = strtolower($info['image_watermark_position']);
      $watermark_info = getimagesize($info['image_watermark']);
      $watermark_type = (array_key_exists(2, $watermark_info) ? $watermark_info[2] : null); // 1 = GIF, 2 = JPG, 3 = PNG
      $filter = @imagecreatefrompng($info['image_watermark']);
      $watermark_checked = ($filter)  ? true : false ;
      if ($watermark_checked) {
        $watermark_dst_width  = $watermark_src_width  = @imagesx($filter);
        $watermark_dst_height = $watermark_src_height = @imagesy($filter);

        // if watermark is too large/tall, resize it first
        if ((!$info['image_watermark_no_zoom_out'] && ($watermark_dst_width > $info['image_dst_x'] || $watermark_dst_height > $info['image_dst_y']))
          || (!$info['image_watermark_no_zoom_in'] && $watermark_dst_width < $info['image_dst_x'] && $watermark_dst_height < $info['image_dst_y'])) {
          $canvas_width  = $info['image_dst_x'] - abs($info['image_watermark_x']);
          $canvas_height = $info['image_dst_y'] - abs($info['image_watermark_y']);
          if (($watermark_src_width/$canvas_width) > ($watermark_src_height/$canvas_height)) {
            $watermark_dst_width = $canvas_width;
            $watermark_dst_height = intval($watermark_src_height*($canvas_width / $watermark_src_width));
          } else {
            $watermark_dst_height = $canvas_height;
            $watermark_dst_width = intval($watermark_src_width*($canvas_height / $watermark_src_height));
          }
        }
        // determine watermark position
        $watermark_x = 0;
        $watermark_y = 0;
        if (is_numeric($info['image_watermark_x'])) {
          if ($info['image_watermark_x'] < 0) {
            $watermark_x = $info['image_dst_x'] - $watermark_dst_width + $info['image_watermark_x'];
          } else {
            $watermark_x = $info['image_watermark_x'];
          }
        } else {
          if (strpos($info['image_watermark_position'], 'r') !== false) {
            $watermark_x = $info['image_dst_x'] - $watermark_dst_width;
          } else if (strpos($info['image_watermark_position'], 'l') !== false) {
            $watermark_x = 0;
          } else {
            $watermark_x = ( $info['image_dst_x'] - $watermark_dst_width) / 2;
          }
        }
        if (is_numeric($info['image_watermark_y'])) {
          if ($info['image_watermark_y'] < 0) {
            $watermark_y = $info['image_dst_y'] - $watermark_dst_height + $info['image_watermark_y'];
          } else {
            $watermark_y = $info['image_watermark_y'];
          }
        } else {
          if (strpos($info['image_watermark_position'], 'b') !== false) {
            $watermark_y = $info['image_dst_y'] - $watermark_dst_height;
          } else if (strpos($info['image_watermark_position'], 't') !== false) {
            $watermark_y = 0;
          } else {
            $watermark_y = ($info['image_dst_y'] - $watermark_dst_height) / 2;
          }
        }
        @imagealphablending($info['image_dst'], true);
        @imagecopyresampled($info['image_dst'], $filter, $watermark_x, $watermark_y, 0, 0, $watermark_dst_width, $watermark_dst_height, $watermark_src_width, $watermark_src_height);
      }
    }
  }

  /*-------------- get_pic_modules --------------------*/
  function get_pic_modules ($picture, $w = "", $h = "", $ext="",$thumb=1 ,$crop=0, $arr_more=array())
  {
    $out = "";
    $pre = $w ;
    if($h)	{
      $pre = "(".$w."x".$h.")" ;
    }
    if(!empty($arr_more['fix_min'])) {
      $pre .= "_fm" ;
    }
    if(!empty($arr_more['fix_max'])) {
      $pre .= "_fmax" ;
    }
    if(!empty($arr_more['fix_width'])) {
      $pre .= "_fw" ;
    }
    if(!empty($arr_more['fix_height'])) {
      $pre .= "_fh" ;
    }
    if(!empty($arr_more['zoom_max'])) {
      $pre .= "_zmax" ;
    }
    if(!empty($crop)) {
      $pre .= "_crop" ;
    }

    $linkhinh = "uploads/".$picture;
    $linkhinh = str_replace("//","/",$linkhinh);
    $dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
    $pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;

    if ($w)
    {
      if($thumb && file_exists(WWW_ROOT . $linkhinh)){
        $file_thumbs = $dir . "/thumbs/{$pre}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
        $linkhinhthumbs = WWW_ROOT . $file_thumbs;
        if (! file_exists($linkhinhthumbs)) {
          if (@is_dir(WWW_ROOT . $dir . "/thumbs")) {
            @chmod(WWW_ROOT . $dir . "/thumbs", 0777);
          } else {
            @mkdir(WWW_ROOT . $dir . "/thumbs", 0777);
            @chmod(WWW_ROOT . $dir . "/thumbs", 0777);
          }
          // thum hinh
          $this->thumbs(WWW_ROOT . $linkhinh, $linkhinhthumbs, $w, $h, $crop, $arr_more);
        }
        $src =  $this->webroot . $file_thumbs;
      }else{
        $src = $this->webroot . $dir ."/thumbs/".$pic_name;
      }
    } else {
      $src = $this->webroot . 'uploads/' . $picture;
    }

    $alt = substr($pic_name, 0, strrpos($pic_name, "."));
    $out = "<img  src=\"{$src}\"  {$ext} />";
    return $out;
  }

  /*-------------- get_src_modules --------------------*/
  public function get_src_modules ($picture, $w = "", $h = "",$thumb=1 ,$crop=0, $arr_more=array())
  {
    $out = "";
    $overwrite = (!empty($arr_more['overwrite'])) ? $arr_more['overwrite'] : 0 ;
    $pre = $w ;
    if(!empty($h))	{
      $pre = "(".$w."x".$h.")" ;
    }
    if(!empty($arr_more['fix_min'])) {
      $pre .= "_fm" ;
    }
    if(!empty($arr_more['fix_max'])) {
      $pre .= "_fmax" ;
    }
    if(!empty($arr_more['fix_width'])) {
      $pre .= "_fw" ;
    }
    if(!empty($arr_more['fix_height'])) {
      $pre .= "_fh" ;
    }
    if(!empty($arr_more['zoom_max'])) {
      $pre .= "_zmax" ;
    }
    if(!empty($crop)) {
      $pre .= "_crop" ;
    }

    $linkhinh = $picture;
    $linkhinh = str_replace("//","/",$linkhinh);
    $dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
    $pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;

    if ($w)
    {
      if($thumb && file_exists(public_path() . $linkhinh)){
        $file_thumbs = $dir . "/thumbs/{$pre}_" . substr($linkhinh, strrpos($linkhinh, "/") + 1);
        $linkhinhthumbs = public_path()  . $file_thumbs;
        if (! file_exists($linkhinhthumbs) || $overwrite ==1 ) {
          if (@is_dir(public_path()  . $dir . "/thumbs")) {
            @chmod(public_path()  . $dir . "/thumbs", 0777);
          } else {
            @mkdir(public_path()  . $dir . "/thumbs", 0777);
            @chmod(public_path()  . $dir . "/thumbs", 0777);
          }
          // thum hinh
          $this->thumbs(public_path() . $linkhinh, $linkhinhthumbs, $w, $h, $crop, $arr_more);
        }
        $src =   $file_thumbs;
      }else{
        $src = $dir ."/thumbs/".$pic_name;
      }

    } else {
      $src = 'uploads/' . $picture;
    }

    return $src;
  }


}
