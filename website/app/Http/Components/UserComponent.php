<?php
namespace App\Http\Components;
use Session;
use Mail;
use Illuminate\Support\Facades\Lang;
//modal
use App\User;

class UserComponent
{

	public static function is_login()
	{
		if(Session::has('user'))
        {
            return true;
        }
        else
        {
        	return false;
        }

	}

    public static function login($email, $password)
    {
    	$password = Util::hash($password);

    	$user = User::where('email', $email)
    		->where('status', 1)
    		->where('style', 0)
    		->first();
    	if($user)
    	{
    		if($user->password === $password)
    		{
                $user = $user->toArray();
    			Session::put('user', $user);
    			return true;
    		}
    	}

    	return false;
    }

    //$info of user(array)
    public static function login_facebook($info)
    {
		$user = User::where('style_id', $info['style_id'])
			->where('style', 1)
			->where('status', 1)->first();

		if($user)
		{
			$user->email = $info['email'];
			$user->nick_name = $info['nick_name'];
			$user->name = $info['name'];
			$user->avatar = $info['avatar'];
			$user->save();
            $user = $user->toArray();
		}
		else
		{
			try{
				$info["created_at"] = \Carbon\Carbon::now();
				$id = User::insertGetId($info);
				$user = User::find($id);
	            $user = $user->toArray();
	            self::registerSendmail($user);
			}
			catch(\Exception $e){
			    Session::flash('flash_message', lang::get('message.contact_sendmail_exception_error'));
			    return redirect('/account/');
			}
		}

		Session::put('user', $user);
    }

    //$info of user(array)
    public static function login_twitter($info)
    {
		$user = User::where('style_id', $info['style_id'])
			->where('style', 2)
			->where('status', 1)->first();
			
		if($user)
		{
			$user->nick_name = $info['nick_name'];
			$user->name = $info['name'];
			$user->save();
            $user = $user->toArray();
		}
		else
		{
			$info["created_at"] = \Carbon\Carbon::now();
			$id = User::insertGetId($info);
			$user = User::find($id);
            $user = $user->toArray();
		}

		Session::put('user', $user);
    }

    public static function logout()
    {
    	Session::forget('user');
    }


    /* 
        TODO: return id user 
    */
    public static function register($email, $password)
    {
    	try{
    		$password = Util::hash($password);

	    	$info = array(
		    		'email' => $email, 
		    		'password' => $password,
					'created_at' =>  \Carbon\Carbon::now(),
	    		);
			
	    	$id = User::insertGetId($info);
	    	$user = User::find($id);
	        $user = $user->toArray();
	    	Session::put('user', $user);
    		self::registerSendmail($user);
    	}
		catch(\Exception $e){
		    Session::flash('flash_message', lang::get('message.contact_sendmail_exception_error'));
		    return redirect('/account/');
		}
    }

    public static function registerSendmail($user)
    {
    	$subject = env('REGISTER_MAIL_TO_USER_SUBJECT');
		$template = env('REGISTER_MAIL_TO_USER_TEMPLATE');
		$fromName = env('FROM_NAME');
		$fromAddress = env('FROM_ADDRESS');
		$adminAddress = env('ADMIN_ADDRESS');
		
		$debugMode = env('SENDMAIL_TEST_MODE');
        $stringBCC = env('TEST_EMAIL_BCC');
		$arrMailBCC = [];
		
        if(isset($stringBCC) && $debugMode == true)
        {
            $arrMailBCC = explode(',', $stringBCC);
        }

        array_push($arrMailBCC,env('ADMIN_ADDRESS'));

        Mail::queue(['text' => $template], ['user' => $user], function($message) use($adminAddress, $user, $subject, $fromName, $fromAddress, $arrMailBCC)
        {
			$emails = array($user['email'], $adminAddress);
            $message->from($fromAddress, $fromName);
            $message->to($user['email'])
            		->bcc($arrMailBCC)
					->subject($subject);
        });
    }

    public static function reset_password()
    {

    }

    public static function get_info()
    {	
    	$user = Session::get('user');
    	return $user;
    }
	
	public static function loginOrSignup($email, $password)
	{		
		if(self::is_login())
		{
			return 1;
		}
		
		$_user = User::where('email', $email)
				 ->where('status', 1)
				 ->where('style', 0)
				 ->first();
		try
		{
			if($_user)
			{
				$password = Util::hash($password);
				if($_user->password === $password)
				{
					Session::put('user', $_user);
					return 1;
				}
			}
			else
			{
				self::register($email, $password);
				return 1;
			}
		}
		catch(\Exception $e)
		{
			return -1;
		}
		return 0;
	}
}
