<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contents;
use App\Keywords;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Session;




class AboutController extends Controller
{

	public function company()
  {

        if($this->is_mobile)
            return view('about.company_sp');
        else
            return view('about.company');
	}


    public function privacypolicy()
    {

        if($this->is_mobile)
            return view('about.privacypolicy_sp');
        else
            return view('about.privacypolicy');
    }


}
