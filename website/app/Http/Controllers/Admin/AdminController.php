<?php

namespace App\Http\Controllers\Admin;

use DB;
use Redirect;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

//model
use App\Keywords;
use App\Contents;
use App\BaseUrls;
use App\User;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO: show login form of admin
        return view('admin.pages.login');
    }
    
    /* 
        Author: Xuong
        Last code: 2015-10-02
        Parameters: []
        Description: activity
        @return: 
    */
    public function login(Request $request) {
        //TODO: process login
        $userName = $request->input('name');
        $password = $request->input('password');
        if($userName == 'admin' && $password == 'kire@123!@#')
        {
            Session::put('admin', array('name' => $userName, 'password' => $password));
            return Redirect::action('Admin\AdminController@dashboard');
        }
        return Redirect::action('Admin\AdminController@index')->withErrors(['user name or password is not correct']);
    }//End function
    
    /* 
        Author: Xuong
        Last code: 2015-09-24
        Parameters: []
        Description: activity
        @return: 
    */
    public function logout() {
        //TODO: process logout
        Session::forget('admin');
        return redirect('/admin');
    }//End function
    
    /* 
        Author: Xuong
        Last code: 2015-09-day
        Parameters: []
        Description: activity
        @return: 
    */
    public function dashboard() {
        //TODO:
        $keywords = Keywords
                    ::orderBy('priority', 'desc')
                    ->limit(5)
                    ->get();
        
       /* $websites = Contents::
            join('base_urls', 'base_urls.id','=','contents.site_id')
            ->groupBy('contents.site_id')
            ->select(DB::raw(
                        'count(contents.site_id) as job_number,'.
                        'sum(contents.job_url_clicked) as clicked_total,'.
                        'base_urls.*'
                    ))
            ->orderBy('contents.job_url_clicked', 'desc')
            ->limit(5)
            ->get();
        */


        $contentCount = Contents::where('status','=', 1)->count();
        $userCount = User::where('status', '=', 1)->count();
        
        return view('admin.pages.dashboard', array(
          'keywords' => $keywords,
            /*   'websites' => $websites,*/
          'contentCount' => $contentCount,
          'userCount' => $userCount
        ));
    }//End function


    public function upload() {
        if(Input::hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('file');
            $tmpFilePath = '/uploads/content/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path = '/public'.$tmpFilePath . $tmpFileName;

            return response()->json(array('link'=> $path), 200);
        } else {
            return response()->json(false, 200);
        }
    }
}
