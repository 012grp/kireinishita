<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Session;
use Lang;

//
use App\Http\Components\AllianceComponent;

//model
use App\Alliance;
use DB;

class AllianceController extends Controller
{
	public function getIndex(Request $request)
	{

		$list = Alliance::where('status', 1)
					->orderBy('priority', 'desc')
					->orderBy('created_at', 'desc');
		
        $keyword = Input::get('keyword');
        if($keyword){
            $list = $list->where(function ($query) use($keyword){
                $keywords = '%'.trim($keyword).'%';
                $query->orWhere('job_name', 'like', $keywords);
                $query->orWhere('job_summary', 'like', $keywords);
                $query->orWhere('job_pr', 'like', $keywords);
            });
        }
		
		$from_date = Input::get('from_date');
		if($from_date)
		{
			$list = $list->whereRaw('date(created_at) >= ?', [$from_date]);
		}
		
		$to_date = Input::get('to_date');
		if($to_date)
		{
			$list = $list->whereRaw('date(created_at) <= ?', [$to_date]);
		}
		
		$category = Input::get('job_category');
		if($category)
		{
			$category = '%'.$category.'%';
			$list = $list->where('list_job','LIKE',$category);
		}
		//dd($list->toSql());
		$list = $list->paginate(50)->appends($request->all());

		return view('admin.pages.alliance.index', array(
			'list' => $list,
			'title_page' => 'アライアンス求人の管理',
		));
	}
	
    public function getUploadCsv(Request $request, $report = array())
	{
		$alliance = new AllianceComponent();
		$listCSV = $alliance->showListCSV();

		return view('admin.pages.alliance.upload_csv', array(
			'listCSV' => $listCSV,
			'title_page' => 'アライアンス求人の管理',
			'report' => $report,
		));
	}
	
	public function postUploadCsv(Request $request)
	{
		set_time_limit(14400);
		/*Check and get contents of CSV by Csv.php*/
		$alliance = new AllianceComponent();
		$fileCsv = $request->input('file_csv');
		
		$checked = $alliance->checkExistFile($fileCsv);
		if($checked)
		{
			return redirect('/admin/alliance-manager/upload-csv')
				   ->withErrors($checked);
		}

        $results = $alliance->processCrontab($fileCsv);
        $rs = $results['rs'];
        $rsShopBusinessCategory= $results['rsShopBusinessCategory'];
		
		/*Check insert and update, save log*/
//		if(!$rs['status'])
//		{
//			return redirect('/admin/alliance-manager/upload-csv')
//				   ->withErrors([$rs['message']]);
//		}
//		return $this->getUploadCsv($request, $rs['data']);
        return redirect()->back();
	}
	
	
	public function getDelete($id){
        $ok = Alliance::find($id)->delete();
        Session::flash('flash_message', Lang::get('message.delete_success'));
        return redirect()->back();
    }
}