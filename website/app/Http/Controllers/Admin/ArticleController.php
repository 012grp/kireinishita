<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Html\HtmlBuilder;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

//components
use App\Http\Components\ThumbsComponent;
use App\Http\Components\Util;
//model
use App\Articles;
use App\ArticleCategories;


class ArticleController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-09-29
        Parameters: []
        Description: activity
        @return: 
    */
    public function index() {
        //TODO:
        //DB::enableQueryLog();
        $content = Articles::orderBy('priority', 'desc')->orderBy('created_at', 'desc');
        $keyword = Input::get('keyword');
        $cat_id = Input::get('cat_id');
        if($keyword){
            $content = $content->where(function ($query) use($keyword){
                $keywords = '%'.$keyword.'%';
                $query->orWhere('title', 'like', $keywords);
                $query->orWhere('description', 'like', $keywords);
                $query->orWhere('content', 'like', $keywords);
            });
        }
        if($cat_id){
            $content = $content->where('cat_id','=', $cat_id);
        }



        $articles = $content->paginate(20);
        //dd(DB::getQueryLog());
        $list_cat = ArticleCategories::all()->where('status', 1);
        $arr_list =array();
        if($list_cat){
            $arr_list[] = 'All';
            foreach ($list_cat as $v){
                $arr_list[$v['id']] = $v['category'];
            }
        }
         //dd($articles);
        return view('admin.pages.article.index',
                    array(
                        'articles' => $articles,
                        'title_page' => 'コンテンツまとめ一覧',
                        'list_cat'=> $arr_list,
                    )
                    );
    }//End function

    protected function getCreate()
    {

        $list_cat = ArticleCategories::all()->where('status', 1);;
        $arr_list =array();
        if($list_cat){
            foreach ($list_cat as $v){
                $arr_list[$v['id']] = $v['category'];
            }
        }
        
        return view('admin.pages.article.form')->with(['list_cat'=> $arr_list,
          'title_page' => 'コンテンツまとめを追加',
          'link_action' => 'admin/article-manager/create'
        ]);

    }

    protected function postCreate(Request $request)
    {
        $article = new Articles;

        $this->validate($request, [
          'title' => 'required',
          //'description' => 'required'
        ]);

        //upload images
        if(Input::hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('file');
            $tmpFilePath = '/uploads/article/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path = $tmpFileName;
            $thumb = new ThumbsComponent();
            $src_thumb = $thumb->get_src_modules($tmpFilePath . $tmpFileName,280,0,1,0,array('fix_width' => 1));

        }else{
            $path = "";
        }


        //upload images thumb for pc
        if(Input::hasFile('image_thumb')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_thumb');
            $tmpFilePath_thumb = '/uploads/article/';
            $tmpFileName_thumb = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_thumb, $tmpFileName_thumb);
            $article->image_thumb = $tmpFileName_thumb;
        }

        //upload images for sp
        if(Input::hasFile('image_sp')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_sp');
            $tmpFilePath_sp = '/uploads/article/';
            $tmpFileName_sp = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_sp, $tmpFileName_sp);
            $article->image_sp =  $tmpFileName_sp;
            $thumb = new ThumbsComponent();
            $src_thumb = $thumb->get_src_modules($tmpFilePath_sp . $tmpFileName_sp,280,0,1,0,array('fix_width' => 1));

        }

        //upload images thumb for sp
        if(Input::hasFile('image_sp_thumb')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_sp_thumb');
            $tmpFilePath_sp_thumb = '/uploads/article/';
            $tmpFileName_sp_thumb = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_sp_thumb, $tmpFileName_sp_thumb);
            $article->image_sp_thumb =  $tmpFileName_sp_thumb;
        }




        $article->cat_id = $request->get('cat_id');
        $article->title = $request->get('title');
        $article->description = $request->get('description');
        $article->meta_desc = $request->get('meta_desc');
        $article->meta_keyword = $request->get('meta_keyword');
        $article->content = $request->get('content');
        $article->url = $request->get('url');
        $article->company_name = $request->get('company_name');
        $article->address = $request->get('address');
        $article->map_location = $request->get('map_location');
        $article->lat = $request->get('lat');
        $article->lng = $request->get('lng');
        $article->status = $request->get('status');
        $article->store_infomation = $request->get('store_infomation');
        $article->company_profile = $request->get('company_profile');
		$article->ceo = $request->get('ceo');
        $article->image = $path;

        
        $date_created = $request->get('created_at');
        if($date_created)
        {
            $date_created = Util::createDateCreatedAt($date_created);
            $article->created_at = $date_created;
        }
        
		//advise
		$article->adviser_image 	= $this->uploadImage('adviser_image');
        $article->adviser_image_sp 	= $this->uploadImage('adviser_image_sp');
        $article->adviser_header 	= $request->get('adviser_header');
        $article->adviser_address 	= $request->get('adviser_address');
        $article->adviser_ceo 		= $request->get('adviser_ceo');
        $article->adviser_content 	= $request->get('adviser_content');
        $article->adviser_email 	= $request->get('adviser_email');
        $article->adviser_website 	= $request->get('adviser_website');
        $article->adviser_contact 	= $request->get('adviser_contact');

        $article->save();
        Session::flash('flash_message', lang::get('message.create_success'));
        return redirect('admin/article-manager');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        // get the Articles
        $article = Articles::find($id);

        $list_cat = ArticleCategories::all()->where('status', 1);;
        $arr_list =array();
        if($list_cat){
            foreach ($list_cat as $v){
                $arr_list[$v['id']] = $v['category'];
            }
        }

        return view('admin.pages.article.form',
          array(
            'title_page' => 'コンテンツまとめを修正',
            'article' => $article,
            'list_cat' => $arr_list,
            'link_action' => 'admin/article-manager/update/'.$id
          )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request){
        // get the Articles
        $article = Articles::findOrFail($id);
        $article->cat_id = $request->get('cat_id');
        $article->title = $request->get('title');
        $article->description = $request->get('description');
        $article->meta_desc = $request->get('meta_desc');
        $article->meta_keyword = $request->get('meta_keyword');
        $article->content = $request->get('content');
        $article->url = $request->get('url');
        $article->company_name = $request->get('company_name');
        $article->address = $request->get('address');
        $article->map_location = $request->get('map_location');
        $article->lat = $request->get('lat');
        $article->lng = $request->get('lng');
        $article->status = $request->get('status');
        $article->store_infomation = $request->get('store_infomation');
        $article->company_profile = $request->get('company_profile');
		$article->ceo = $request->get('ceo');

        //date created
        $date_created = Util::createDateCreatedAt($request->get('created_at'));
        $article->created_at = $date_created;

        //upload images
        if(Input::hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('file');
            $tmpFilePath = '/uploads/article/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $article->image = $tmpFileName;
            $thumb = new ThumbsComponent();
            $src_thumb = $thumb->get_src_modules($tmpFilePath . $tmpFileName,280,0,1,0,array('fix_width' => 1));
        }

        //upload images thumb for pc
        if(Input::hasFile('image_thumb')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_thumb');
            $tmpFilePath_thumb = '/uploads/article/';
            $tmpFileName_thumb = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_thumb, $tmpFileName_thumb);
            $article->image_thumb =  $tmpFileName_thumb;
        }


        //upload images for sp
        if(Input::hasFile('image_sp')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_sp');
            $tmpFilePath_sp = '/uploads/article/';
            $tmpFileName_sp = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_sp, $tmpFileName_sp);
            $path_image_sp = $tmpFileName_sp;
            $article->image_sp = $path_image_sp;
            $thumb = new ThumbsComponent();
            $src_thumb = $thumb->get_src_modules($tmpFilePath_sp . $tmpFileName_sp,280,0,1,0,array('fix_width' => 1));
        }

        //upload images thumb for sp
        if(Input::hasFile('image_sp_thumb')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_sp_thumb');
            $tmpFilePath_sp_thumb = '/uploads/article/';
            $tmpFileName_sp_thumb = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_sp_thumb, $tmpFileName_sp_thumb);
            $article->image_sp_thumb =  $tmpFileName_sp_thumb;
        }



        $delete_image_sp = $request->get('delete_image_sp');
        if(!empty($delete_image_sp)){
            $article->image_sp = "";
        }

        $image = $request->get('delete_image');
        if(!empty($image)){
            $article->image = "";
        }


        $delete_image_thumb = $request->get('delete_image_thumb');
        if(!empty($delete_image_thumb)){
            $article->image_thumb = "";
        }

        $delete_sp_thumb = $request->get('delete_sp_thumb');
        if(!empty($delete_sp_thumb)){
            $article->image_sp_thumb	 = "";
        }
		
		
		//advise
		if($this->allowUpdateImage('adviser_image','delete_image_adviser')) {
			$article->adviser_image 	= $this->uploadImage('adviser_image','delete_image_adviser');
		}
		
		if($this->allowUpdateImage('adviser_image_sp','delete_thumb_adviser')) {
			$article->adviser_image_sp 	= $this->uploadImage('adviser_image_sp', 'delete_thumb_adviser');
		}
		$article->adviser_header 	= $request->get('adviser_header');
        $article->adviser_address 	= $request->get('adviser_address');
        $article->adviser_ceo 		= $request->get('adviser_ceo');
        $article->adviser_content 	= $request->get('adviser_content');
        $article->adviser_email 	= $request->get('adviser_email');
        $article->adviser_website 	= $request->get('adviser_website');
        $article->adviser_contact 	= $request->get('adviser_contact');
		
		
        $article->save();
		//dd($article);
        Session::flash('flash_message', lang::get('message.update_success'));
        return redirect()->back();
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id){

        Articles::find($id)->delete();
        Session::flash('flash_message', lang::get('message.delete_success'));
        return redirect()->back();
    }


    public function preview(Request $request)
    {
        $kind = $request->get('preview_kind');
		
		$currentArt = null;
		$currentAdviserImage = '';
		$currentAdviserImageSp = '';
		if($request->get('id'))
		{
			$currentArt = Articles::find($request->get('id'));
			if($currentArt)
			{
				$currentAdviserImage = $currentArt->adviser_image;
				$currentAdviserImageSp = $currentArt->adviser_image_sp;
			}
		}
		
        $article = new Articles();

        $article->cat_id = $request->get('cat_id');
		
		$category = ArticleCategories::find($article->cat_id);
		if($category)
		{
			$article->category_alias = $category->category_alias;
			$article->class_name 	 = $category->class_name;
			$article->category 	 = $category->category;
		}
		else
		{
			$article->category_alias = 'default';
			$article->class_name = '';
			$article->category = '';
		}
		
        $article->title = $request->get('title');
        $article->description = $request->get('description');
        $article->content = $request->get('content');
        $article->url = $request->get('url');
        $article->company_name = $request->get('company_name');
        //$article->address = $request->get('address');
        $article->map_location = $request->get('map_location');
        $article->lat = $request->get('lat');
        $article->lng = $request->get('lng');
		$article->ceo = $request->get('ceo');
		
		//advise
		$article->adviser_image 	= $this->previewImage($currentAdviserImage, 'adviser_image','delete_image_adviser');
		
		
		$article->adviser_image_sp 	= $this->previewImage($currentAdviserImageSp, 'adviser_image_sp', 'delete_thumb_adviser');
		$article->adviser_header 	= $request->get('adviser_header');
        $article->adviser_address 	= $request->get('adviser_address');
        $article->adviser_ceo 		= $request->get('adviser_ceo');
        $article->adviser_content 	= $request->get('adviser_content');
        $article->adviser_email 	= $request->get('adviser_email');
        $article->adviser_website 	= $request->get('adviser_website');
        $article->adviser_contact 	= $request->get('adviser_contact');
		
		
        //upload images
        $base64 = "";

        if($kind == "PC")
        {
            if(Input::hasFile('file')) {
                //upload an image to the /img/tmp directory and return the filepath.
                $tmp_image_path = Input::file('file')->getRealPath();

                $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                $data = file_get_contents($tmp_image_path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            }
			else
			{
				 //if update without change IMAGE
				 if($request->get('id'))
				 {
				 	$currentArt = Articles::find($request->get('id'));
					$article->image = $currentArt['image'];
				 }
			}
        }
        else
        {
			//Upload 
            if(Input::hasFile('image_sp')) {
                //upload an image to the /img/tmp directory and return the filepath.
                $tmp_image_path = Input::file('image_sp')->getRealPath();
                
                $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                $data = file_get_contents($tmp_image_path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            }
			else
			{				

					 //if update without change IMAGE of SP
					 if($request->get('id'))
					 {
						$currentArt = Articles::find($request->get('id'));
						//if has not uploaded before yet
						//or image_sp_delete flag is on
						if(empty($currentArt['image_sp']) || $request->input('delete_image_sp') == 'image_sp_delete')
						{
							//PC image prioritizes 
							if(Input::hasFile('file')) {
								//upload an image to the /img/tmp directory and return the filepath.
								$tmp_image_path = Input::file('file')->getRealPath();

								$type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
								$data = file_get_contents($tmp_image_path);
								$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
							}
							//finally
							else
							{
								$article->image = $currentArt['image'];
							}
						}
						else
						{
							$article->image = $currentArt['image_sp'];
						}
					 }
					 else
					 {
					 	if(Input::hasFile('file')) {
							//upload an image to the /img/tmp directory and return the filepath.
							$tmp_image_path = Input::file('file')->getRealPath();

							$type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
							$data = file_get_contents($tmp_image_path);
							$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
						}
					 }
			}
        }

        $category = ArticleCategories::where('id', $article->cat_id)
                                            ->where('status', 1)
                                            ->get();

        $article->image_base64 = $base64;

        

        
        if($kind == "PC")
        {
            $related_content = ArticleCategories::select('a.id',
                                                    'a.image',
                                                    'a.image_thumb',
                                                    'a.image_sp',
                                                    'a.image_sp_thumb',
                                                    'a.title',
                                                    'a.view',
                                                    'a.created_at',
                                                    'acc.category', 
                                                    'acc.category_alias',
                                                    'acc.meta_desc',
                                                    'acc.class_name',
                                                    'acc.description as cat_description')
                            ->from('article_content as a')
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->where('acc.id', $article->cat_id)
                            ->where('a.status', 1)
                            ->orderBy('a.created_at', 'desc')
                            ->skip(0)->take(5)->get();

            return view('contents.preview')
                    ->with([
                        'article_content'=> $article, 
                        'related_content' => $related_content,
                        'category' => $category[0]
                    ]);
        }
        
        else
        {
            return view('contents.preview_sp')
                    ->with([
                        'article_content'=> $article,
                        'category' => $category[0]
                    ]);
        }
                    
    }
	
	private function allowUpdateImage($image_name, $allowDelete = '')
	{
		if($allowDelete)
		{
			if(\Illuminate\Support\Facades\Request::input($allowDelete) == "ok")
			{
				return true;
			}
		}
		
		if(Input::hasFile($image_name))
		{
			return true;
		}
		return false;
	}
	
    private function uploadImage($image_name, $allowDelete = '', $path = '/uploads/article/')
    {
//		if($allowDelete)
//		{
//			if(\Illuminate\Support\Facades\Request::input($allowDelete) == "ok")
//			{
//				return "";
//			}
//		}
		
        if(Input::hasFile($image_name)) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file($image_name);
            $tmpFilePath_sp_thumb = $path;
            $tmpFileName_sp_thumb = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_sp_thumb, $tmpFileName_sp_thumb);
			return $tmpFileName_sp_thumb;
        }else{
            return "";
        }
    }
	
	private function previewImage($current_image, $image_name ,$allowDelete = '', $circle = false)
	{
		//new
		if(Input::hasFile($image_name)) {
			//upload an image to the /img/tmp directory and return the filepath.
			$tmp_image_path = Input::file($image_name)->getRealPath();
			$type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
			$data = file_get_contents($tmp_image_path);
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
			
			return $base64;
		}
		
		//empty
		if($allowDelete)
		{
			if(\Illuminate\Support\Facades\Request::input($allowDelete) == "ok")
			{
				return "";
			}
		}
		//current
		return $current_image;
	}
}
