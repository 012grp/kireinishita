<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\JobCategory;
use App\AllianceJobCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Mail;
use Session;

//model

class CategoryController extends Controller
{

    public function getIndex()
    {
        //TODO:

        $jobCategory = new AllianceJobCategory();
        $content = $jobCategory->getJobCategoryByStyle(1,0,0);
        $keyword = Input::get('keyword');
        if ($keyword) {
            $content = $content->where(function ($query) use ($keyword) {
                $keywords = '%' . $keyword . '%';
                $query->orWhere('name', 'like', $keywords);
                $query->orWhere('description', 'like', $keywords);
                $query->orWhere('keywords', 'like', $keywords);
            });
        }

        //dd($content->get());

        return view('admin.pages.category.index',
            array(
                'content' => $content->get(),
                'title_page' => '職種の説明一覧',
            )
        );
    }//End function

    public function getCreate()
    {
        $jobCategory = new AllianceJobCategory();
        $listParent = $jobCategory->getParentCategoryByStyle(1);
        $listCat = $jobCategory->getJobCategoryByStyle(1);

        return view('admin.pages.category.form')->with([
            'listParent' => $listParent,
            'listCat' => $listCat,
            'title_page' => '職種の説明詳細',
            'link_action' => 'admin/category-manager/create'
        ]);

    }

    protected function postCreate(Request $request)
    {
        $message = lang::get('message.create_success');
        $action = $request->get('action');
        $JobCategory = new JobCategory();

        $this->validate($request, [
            'name' => 'required',
        ]);
        $pass = $request->get('password');
        $JobCategory->name = $request->get('name');
        $JobCategory->category = $request->get('category');
        $JobCategory->celebration = $request->get('celebration');
        $JobCategory->average_price = $request->get('average_price');
        $JobCategory->website = $request->get('website');
        $JobCategory->address = $request->get('address');
        $JobCategory->email = $request->get('email');

        $rs = JobCategory::select('id')
            ->where('email', $JobCategory->email)
            ->first();
        if ($rs) {
            Session::flash('flash_error', lang::get('message.error_compnany_email_exits'));
            return redirect()->back()->withInput();
        }

        $JobCategory->password = md5($request->get('password'));

        if (!empty($pass)) {
            if (!preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', $pass)) {
                Session::flash('flash_error', lang::get('message.error_JobCategory_password'));
                return redirect()->back()->withInput();
            }

            if ($action == 2) //action send mail
            {
                //send mail
                $info = array();
                $arrMailBCC = array();
                $templatemMail = "mail.templates.info_account_JobCategory";
                $info['email'] = $request->get('email');
                $info['JobCategory_name'] = $request->get('name');
                $info['password'] = $pass;
                $subject = '【キレイにスルンダ】求人管理画面のご案内';
                $fromAddress = env('FROM_ADDRESS');
                $fromName = env('FROM_NAME');
                $email_to = $request->get('email');
                $name_to = $request->get('name');


                Mail::queue(['text' => $templatemMail], ['info' => $info], function ($message) use ($subject, $fromName, $fromAddress, $email_to, $name_to, $arrMailBCC) {
                    $message->from($fromAddress, $fromName);
                    $message->to($email_to, $name_to)
                        ->bcc($arrMailBCC)
                        ->subject($subject);
                });

                $message = lang::get('message.create_success_and_send_mail');
            }
        }

        $JobCategory->access_manual = $request->get('access_manual');
        $JobCategory->holiday = $request->get('holiday');
        $JobCategory->seat_number = $request->get('seat_number');
        $JobCategory->hp = $request->get('hp');
        $JobCategory->access = $request->get('access');
        $JobCategory->map_location = $request->get('map_location');
        $JobCategory->lat = $request->get('lat');
        $JobCategory->lng = $request->get('lng');
        $JobCategory->status = $request->get('status');
        $JobCategory->save();
        Session::flash('flash_message', $message);
        return redirect('admin/category-manager');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function getEdit($id)
    {
        // get the JobCategory
        $allienceJobCategory = new AllianceJobCategory();
        $jobCategory = new JobCategory();
        $JobCategory = $allienceJobCategory->find($id);
        $catParent = $jobCategory->getById($JobCategory->parent_id);
        $listCat = $allienceJobCategory->getJobCategoryByStyle(1,0,0);
        $listParent = $jobCategory->getParentCategoryByStyle(1,0);
        //dd($JobCategory->description);
        return view('admin.pages.category.form',
            array(
                'title_page' => '職種の説明詳細',
                'JobCategory' => $JobCategory,
                'catParent' => $catParent,
                'listCat' => $listCat->get(),
                'listParent' => $listParent,
                'link_action' => 'admin/category-manager/update/' . $id
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function postUpdate($id, Request $request)
    {
        //$id= $request->get('id');
        $message = lang::get('message.update_success');

        // get the JobCategory
        $jobCategory = new AllianceJobCategory();
        $JobCategory = $jobCategory->find($id);
        $JobCategory->description = $request->get('description');
        $JobCategory->status = !empty($request->get('status')) ? $request->get('status') : 0;
        $isSave = $JobCategory->save();
        Session::flash('flash_message', $message);
        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function delete($id)
    {
        $jobCategory = new AllianceJobCategory();
        $JobCategory = $jobCategory->find($id);
        $JobCategory->description = -1;
        $JobCategory->save();
        Cache::flush();

        Session::flash('flash_message', lang::get('message.delete_success'));
        return redirect()->back();
    }


}
