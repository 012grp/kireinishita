<?php
/**
 * Created by PhpStorm.
 * User: cuong
 * Date: 8/8/2016
 * Time: 3:09 PM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cache;
use Session;

class ClearCacheController extends Controller
{


  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function index(Request $request)
  {
    $name = $request->get('name');
    if(!empty($name)){
      if($name == 'all'){
        Cache::flush();
        Session::flash('flash_message', 'Clear Cache successfully!');
        return redirect('admin/clear-cache');
      }else{
        Cache::forget($name);
        Session::flash('flash_message', 'Delete cache name '.$name);
        return redirect('admin/clear-cache');
      }
    }
    return view('admin.pages.clear_cache',array( 'title_page' => 'Clear Cache'));
  }


}