<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Html\HtmlBuilder;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
//model
use App\Company;
use Mail;

class CompanyController extends Controller
{

    public function index() {
        //TODO:

        //DB::enableQueryLog();
        $content = Company::orderBy('created_at', 'desc');
        $keyword = Input::get('keyword');
        if($keyword){
            $content = $content->where(function ($query) use($keyword){
                $keywords = '%'.$keyword.'%';
                $query->orWhere('name', 'like', $keywords);
                $query->orWhere('address', 'like', $keywords);
                $query->orWhere('introduction', 'like', $keywords);
            });
        }

        $companys = $content->paginate(20);
        //dd(DB::getQueryLog());

        return view('admin.pages.company.index',
                    array(
                        'companys' => $companys,
                        'title_page' => '会社情報一覧',
                    )
                    );
    }//End function

    protected function getCreate()
    {

        return view('admin.pages.company.form')->with([
          'title_page' => '会社情報追加',
          'link_action' => 'admin/company-manager/create'
        ]);

    }

    protected function postCreate(Request $request)
    {
        $message = lang::get('message.create_success');
        $action = $request->get('action');
        $company = new Company();

        $this->validate($request, [
          'name' => 'required',
        ]);
        $pass = $request->get('password');
        $url = $request->get('website');

        $company->name = $request->get('name');
        $company->representative = $request->get('representative');
        $company->work_content = $request->get('work_content');
        $company->address = $request->get('address');
        $company->email = $request->get('email');
        //$company->representative = $request->get('representative');
        //$company->work = $request->get('work');

        $rs = Company::select('id')
                        ->where('email', $company->email)
                        ->first();
        if($rs)
        {
            Session::flash('flash_error', lang::get('message.error_compnany_email_exits'));
            return redirect()->back()->withInput();
        }

        $company->password = md5($request->get('password'));

        if(!empty($pass)){
            if (!preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', $pass))
            {
                Session::flash('flash_error', lang::get('message.error_company_password'));
                return redirect()->back()->withInput();
            }

            if($action == 2) //action send mail
            {
                //send mail
                $info = array();
                $arrMailBCC = array();
                $templatemMail = "mail.templates.info_account_company";
                $info['email'] = $request->get('email');
                $info['company_name'] = $request->get('name');
                $info['password'] = $pass;
                $subject = '【キレイにスルンダ】求人管理画面のご案内';
                $fromAddress = env('FROM_ADDRESS');
                $fromName = env('FROM_NAME');
                $email_to =  $request->get('email');
                $name_to = $request->get('name');


                Mail::queue(['text' => $templatemMail], ['info' => $info], function($message) use( $subject, $fromName, $fromAddress,$email_to,$name_to, $arrMailBCC)
                {
                    $message->from($fromAddress, $fromName);
                    $message->to($email_to, $name_to)
                      ->bcc($arrMailBCC)
                      ->subject($subject);
                });

                $message = lang::get('message.create_success_and_send_mail');
            }
        }

        if(!empty($url)){
            if (!preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/", $url))
            {
                Session::flash('flash_error', lang::get('message.error_company_not_url'));
                return redirect()->back()->withInput();
            }

            else if (strpos($url, 'http://') === false && strpos($url, 'https://') === false)
            {
                $url = "http://" . $url;
            }
        }

        $company->website = $url;
        $company->map_location = $request->get('map_location');
        $company->lat = $request->get('lat');
        $company->lng = $request->get('lng');
        $company->status = $request->get('status');
        $company->save();
        Session::flash('flash_message', $message);
        return redirect('admin/company-manager');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        // get the Company
        $company = Company::find($id);
        return view('admin.pages.company.form',
          array(
            'title_page' => '会社情報修正',
            'company' => $company,
            'link_action' => 'admin/company-manager/update/'.$id
          )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request){
        $action = $request->get('action');
        $message = lang::get('message.update_success');

        // get the Company
        $company = Company::findOrFail($id);
        $pass = $request->get('password');
        $url = $request->get('website');

        $company->name = $request->get('name');
        $company->representative = $request->get('representative');
        $company->work_content = $request->get('work_content');
        $company->address = $request->get('address');
        $company->email = $request->get('email');

        $rs = Company::select('id')
                        ->where('email', $company->email)
                        ->where('id', '<>', $id)
                        ->first();

        if($rs)
        {
            Session::flash('flash_error', lang::get('message.error_compnany_email_exits'));
            return redirect()->back()->withInput();
        }
        if(!empty($pass)){
            if (!preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', $pass))
            {
                Session::flash('flash_error', lang::get('message.error_company_password'));
                return redirect()->back()->withInput();
            }
            $company->password = md5($pass);

            if($action == 2) //action send mail
            {
                //send mail
                $info = array();
                $arrMailBCC = array();
                $templatemMail = "mail.templates.info_account_company";
                $info['email'] = $request->get('email');
                $info['company_name'] = $request->get('name');
                $info['password'] = $pass;
                $subject = '【キレイにスルンダ】求人管理画面のご案内';
                $fromAddress = env('FROM_ADDRESS');
                $fromName = env('FROM_NAME');
                $email_to =  $request->get('email');
                $name_to = $request->get('name');


                Mail::queue(['text' => $templatemMail], ['info' => $info], function($message) use( $subject, $fromName, $fromAddress,$email_to,$name_to, $arrMailBCC)
                {
                    $message->from($fromAddress, $fromName);
                    $message->to($email_to, $name_to)
                      ->bcc($arrMailBCC)
                      ->subject($subject);
                });

                $message = lang::get('message.update_success_and_send_mail');
            }
        }

        if(!empty($url)){
            if (!preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/", $url))
            {
                Session::flash('flash_error', lang::get('message.error_company_not_url'));
                return redirect()->back()->withInput();
            }

            else if (strpos($url, 'http://') === false && strpos($url, 'https://') === false)
            {
                $url = "http://" . $url;
            }
        }

        $company->website = $url;
        $company->map_location = $request->get('map_location');
        $company->lat = $request->get('lat');
        $company->lng = $request->get('lng');
        $company->status = $request->get('status');
        $company->save();
        Session::flash('flash_message', $message);
        return redirect()->back();
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id){

        Company::find($id)->delete();
        Session::flash('flash_message', lang::get('message.delete_success'));
        return redirect()->back();
    }


    public function preview(Request $request)
    {
        $kind = $request->get('preview_kind');

        $company = new Company();

        $company->name = $request->get('name');
        $company->representative = $request->get('representative');
        $company->work_content = $request->get('work_content');
        $company->website = $request->get('website');
        $company->address = $request->get('address');
        $company->email = $request->get('email');
        $company->map_location = $request->get('map_location');
        $company->company_lat = $request->get('lat');
        $company->company_lng = $request->get('lng');
        $company->status = $request->get('status');
        $company->celebration = $request->get('celebration');
        $company->category = $request->get('category');
        $status = $request->get('status');

        $detail = $company['attributes'];

        if($kind == "PC")
        {
            return view('company.preview')->with([
                        'detail'=> $detail, 'status' => $status
                    ]);
        }
        
        else
        {
            return view('company.preview_sp')->with([
                        'detail'=> $detail, 'status' => $status
                    ]);
        }
    }
    
}
