<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

//model
use App\Contents;
use DB;

class JobController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-09-29
        Parameters: []
        Description: activity
        @return: 
    */
    public function index(Request $request) {
       $action = $request->input('action');
		$value = $request->input('value');
        $requirementSpecial = $request->input('requirement_special');
        $value = urldecode($value);
		$website = intval($request->input('website'));
		$fromDate = $request->input('from_date');

        $contents = Contents::where('status', '=', 1)->orderBy('priority', 'desc');
		
		if( !empty($value) )
		{
			switch($action)
			{
				case "ID":  
					$contents = $contents->where('id','=', $value);
				break;
				case "Name":  
					$contents = $contents->where('job_name','LIKE', '%'.$value.'%');
				break;
				case "Detail":  
					$contents = $contents->where('job_detail','LIKE', '%'.$value.'%');
				break;
				case "Company":  
					$contents = $contents->where('job_company','LIKE', '%'.$value.'%');
				break;
				case "Career":
					$contents = $contents->where('job_career','LIKE', '%'.$value.'%');
				break;
				case "Salary": 
					$contents = $contents->where('job_salary','LIKE', '%'.$value.'%');
				break;
				case "Expire":  
					$contents = $contents->where('job_expire','LIKE', '%'.$value.'%');
				break;
				case "Location":  
					$contents = $contents->where('job_location','LIKE', '%'.$value.'%');
				break;
				case "Type":  
					$contents = $contents->where('job_type','LIKE', '%'.$value.'%');
				break;
                case "RequirementSpecial":
					$contents = $contents->where('requirement_special','LIKE', '%'.$value.'%');
				break;
					
//				case "Image":  
//					$contents = $contents->where('job_image','LIKE', '%'.$value.'%');
//				break;	

				//Any
				default:
					$contents = $contents->where(function ($query) use($value){
							$query->orWhere('contents.id', '=', $value);
							$value = '%'.$value.'%';
							$query->orWhere('contents.job_name', 'like', $value);
							$query->orWhere('contents.job_detail', 'like', $value);
							$query->orWhere('contents.job_company', 'like', $value);
							$query->orWhere('contents.job_career', 'like', $value);
							$query->orWhere('contents.job_salary', 'like', $value);
							$query->orWhere('contents.job_expire', 'like', $value);
							$query->orWhere('contents.job_location', 'like', $value);
							$query->orWhere('contents.job_type', 'like', $value);
					});
				break;
			}
		}
		
		if( $website > 0 )
		{
			$contents = $contents->where('site_id', '=', $website);
		}
		
		if(!empty($fromDate))
		{
			$created = date('Y-m-d H:i:s', strtotime($fromDate));
			$contents = $contents->where('created', '>=', $created);
		}
		
		$contents = $contents->paginate(21)->appends($request->all());

		//statistic
		$statistic = DB::select( DB::raw("SELECT s.site_url, s.site_name, tbl.clicked
			FROM 
			 (SELECT site_id, SUM(job_url_clicked) AS clicked
			 FROM contents
			 GROUP BY site_id) tbl
			LEFT JOIN crawl_sites s ON s.site_id = tbl.site_id") );



        return view('admin.pages.job',[
			'title' 		=> 'Jobs',
			'pagename' 		=> 'Jobs',
			'contents' 	=> $contents,
			'action'	=> $action,		
			'value'		=> $value,				
			'website'	=> $website,		
			'fromDate'	=> $fromDate,
			'statistic' => $statistic,
			'requirementSpecial' => $requirementSpecial,

        ]);
    }//End function
}