<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

//model
use App\Keywords;

class KeywordController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-09-29
        Parameters: []
        Description: activity
        @return: 
    */
    public function index() {
        //TODO:
        $keywords = Keywords
                    ::orderBy('priority', 'desc')
                    ->paginate(10);
        
        return view('admin.pages.keyword', [
            'keywords' => $keywords
        ]);
    }//End function
}