<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

//model
use App\Area;
use App\JobCategory;
use App\WaySite;


class ListKeywordFilterController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2016-09-21
        Parameters: [
			list_type: list_job or list_job_parent or list_waysite or list_hangup
			fname: text or empty (find name)
			fkeyword: text or empty (find keyword)
		]
        Description: activity
        @return: 
    */
    public function getIndex(Request $request) {
		/* #regoin: REQUEST */
		{
			$listType		=$request->input('list_type');
			$fname			=$request->input('fname');
			$fkeyword		=$request->input('fkeyword');
		}
		/* #end regoin */
		
		$paginatePage 	= 100;
		
		$list = null;
		
		$ColumnIdName = 'id';
		switch($listType)
		{
			
			case 'list_hangup':
				$listName = 'こだわり';
				$list = JobCategory::where('level', '=', 1)
								   ->where(function($query){
								   		$query->orWhere('style',2);
								   		$query->orWhere('style',3);
								   });
			break;
				
			case 'list_job_parent':
				$listName = '職種+こだわりの親';
				$list = JobCategory::where('level', '=', 0)
								   ->where(function($query){
								   		$query->orWhere('style',1);
								   		$query->orWhere('style',2);
								   });
			break;
			
			case 'list_waysite':
				$listName = '沿線・駅';
				$list = WaySite::select('line_cd', 'name', 'keywords', 'updated_at')->groupBy('line_cd');
				$ColumnIdName = 'line_cd';
			break;
				
			case 'list_province':
				$listName = '都道府県';
				$list = Area::where('level',0);
			break;
			
			case 'list_city':
				$listName = '市区町村';
				$list = Area::where('level',1);
			break;
				
			case 'list_em':
				$listName = '雇用形態';
				$list = JobCategory::where('level', '=', 1)->where('style', '=', 3);
			break;
						
			//list_job
			default:
				$listName = '職種';
				$list = JobCategory::where('level', '=', 1)->where('style', '=', 1);
			break;
		}
		
		
		
	
		
		/* #regoin: name */
		{
			if(!empty($fname))
			{
				$list 		= $list->where('name', 'LIKE', "%$fname%");
			}
		}
		/* #end regoin */
		
		/* #regoin: name */
		{
			if(!empty($fkeyword))
			{
				$list 		= $list->where('keywords', 'LIKE', "%$fkeyword%");
			}
		}
		/* #end regoin */
		$list				= $list->orderBy('updated_at', 'desc');
		$list 				= $list->paginate($paginatePage)->appends($request->all());

		

		
		
		
		return view('admin.pages.list_keyword_filter_user', [
			'title' 		=> $listName,
			'pagename' 		=> 'List',
			
			'fname' 		=> $fname,
			
			'fkeyword' 		=> $fkeyword,
			
			'listType' 		=> $listType,
			
			'list' 		=> $list,
			
			'ColumnIdName' => $ColumnIdName
		]);
    }//End function
	
	
	public function getUpdate(Request $request)
	{
		
		$listType	=$request->input('list_type');
		$id			=$request->input('id');
		$keywords	=$request->input('keywords');
		$searching	=$request->input('searching');
			
		if(!$id)
		{
			return redirect('/admin/list-keyword?list_type='.$listType);
		}
				
		$rs = 0;
		switch($listType)
		{
			case '':
			case 'list_em':				
			case 'list_job':				
			case 'list_hangup':				
			case 'list_job_parent':
				$tb = JobCategory::find($id);
				$tb->keywords = $keywords;
				$rs = $tb->save();
			break;
			
			case 'list_waysite':
				$rs = $rs = Waysite::where('line_cd','=', $id)->update(array('keywords' => $keywords));
			break;
				
			case 'list_province':
			case 'list_city':				
				$tb = Area::find($id);
				$tb->keywords = $keywords;
				$rs = $tb->save();
			break;
		}
		$response = array(
			'status' => $rs,
			'message' => '更新に失敗しました'
		);
		if($rs)
		{
			$updatedField = $listType;
			$foundColumn = '';
			set_time_limit(300);
			switch($listType)
			{
				case '':
				case 'list_job':
					$updatedField = 'list_job';
					$foundColumn = 'job_career';
					
				break;
					
				case 'list_em':	
					$foundColumn = 'job_type';
				break;
					
				case 'list_hangup':
					$foundColumn = 'requirement_special';
				break;
					
				case 'list_job_parent':
					$foundColumn = 'concat(job_type, requirement_special)';
				break;
					
				case 'list_waysite':
					$foundColumn = 'job_location';
				break;
				case 'list_province':
				case 'list_city':	
					$foundColumn = 'job_location';
				break;
			}
			
			/*
			#remove ALL existing keys
			update contents set list_job = replace(list_job, ',10,', ',') 
			where status = 1 and list_job like '%,10,%';
			
			#update ALL for which are empty
			update contents set list_job = concat(list_job,',',10,',')
			where list_job = '' and status = 1 and job_career regexp '年齢不問' ;
			
			#update ALL for which are not empty
			update contents set list_job = concat(list_job,10,',')
			where list_job != '' and status = 1 and job_career regexp '年齢不問' ;
			
			#count again
			select count(*)  from contents where list_job like '%,10,%';
			*/
			try
			{
				$start = microtime(true);
				$response['list_type'] = $listType;
				$response['id'] = $id;
				$response['keywords'] = $keywords;

				//0
				$response['before'] = \DB::select("select count(id) as count_rs from contents where status = 1 and $updatedField like ?", 
									 ['%,'.$id.',%']);
				$response['before'] = $response['before'][0]->count_rs;
				//1
				$response['remove_status'] = \DB::statement("update contents set $updatedField = replace($updatedField, ? , ',') 
								where status = 1 and $updatedField like ?", 
							   [','.$id.',','%,'.$id.',%']);
					
				//2
				$response['update_status1'] = \DB::statement("update contents set $updatedField = concat($updatedField,',',?,',')
								where $updatedField = '' and status = 1 and $foundColumn regexp ?", 
							   [$id,$keywords]);

				//3
				$response['update_status2'] = \DB::statement("update contents set $updatedField = concat($updatedField,?,',')
								where $updatedField != '' and status = 1 and $foundColumn regexp ?", 
							   [$id,$keywords]);
				//4
				$response['after'] = \DB::select("select count(id) as count_rs from contents where status = 1 and $updatedField like ?", 
									 ['%,'.$id.',%']);
				$response['after'] = $response['after'][0]->count_rs;

				$end = microtime(true);
				$response['time']  = number_format($end - $start);
				$response['message'] = '更新成功';
				
			}
			catch(\Exception $e)
			{
				$response['status']  = 0;
				$response['message'] = "Change keywords success but crawler is running, Jobs couldn't index with new keywords, please try later";
			}
		}
		
		
		
		//return redirect()->back();
		return redirect('/admin/list-keyword?list_type='.$listType.'&'.$searching)
			   ->withErrors($response);
	}
}