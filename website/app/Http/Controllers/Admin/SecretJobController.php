<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Html\HtmlBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Validator;
use Session;
use DB;
use Illuminate\Support\Facades\Lang;
//components
use App\Http\Components\ThumbsComponent;
use App\Http\Components\Util;
//model
use App\SecretJob;
use App\SecretJobImage;
use App\Company;


class SecretJobController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-09-29
        Parameters: []
        Description: activity
        @return: 
    */
    public function index() {
        //TODO:

        //dd($articles);
        $content = SecretJob::orderBy('priority', 'desc')->orderBy('created_at', 'desc');
        $keyword = Input::get('keyword');
        if($keyword){
            $content = $content->where(function ($query) use($keyword){
                $keywords = '%'.trim($keyword).'%';
                $query->orWhere('job_name', 'like', $keywords);
                $query->orWhere('job_detail', 'like', $keywords);
                $query->orWhere('job_description', 'like', $keywords);
            });
        }

        $secretjob = $content->paginate(20);

        return view('admin.pages.secretjob.index',
                    array(
                        'secretjob' => $secretjob,
                        'title_page' => 'オリジナル求人一覧',
                    )
                    );
    }//End function

    protected function getCreate()
    {   
        $list_company = Company::all('id','name','status');
        $arr_list =array();
        if($list_company){
            foreach ($list_company as $v){
                $arr_list[$v['id']] = $v['name'];
            }
        }
        return view('admin.pages.secretjob.form')->with(['list_company'=> $arr_list,
          'title_page' => 'オリジナル求人追加',
          'link_action' => 'admin/secret-job-manager/create'
        ]);
    }

    protected function postCreate(Request $request)
    {
        $SecretJob = new SecretJob();
        $this->validate($request, [
          'job_name' => 'required',
          'job_salary' => 'required'
        ]);

        //upload images
        if(Input::hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('file');
            $FilePath = '/uploads/job/';
            $FileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $FilePath, $FileName);
            $path = '/public'.$FilePath . $FileName;
        }else{
            $path = "";
        }

        //upload images thumbs for sp
        if(Input::hasFile('image_sp')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_sp');
            $tmpFilePath = '/uploads/job/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $SecretJob->job_image_sp = '/public'.$tmpFilePath . $tmpFileName;
        }else{

            if(Input::hasFile('file')) {
                $thumb = new ThumbsComponent();
                $src_thumb = $thumb->get_src_modules($FilePath . $FileName,280,0,1,0,array('fix_width' => 1));
                $SecretJob->job_image_sp = "/public".$src_thumb;
            }

        }
        

        //upload images notice
        if(Input::hasFile('job_notice_image')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('job_notice_image');
            $tmpFilePath_notice = '/uploads/job/';
            $tmpFileName_notice = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath_notice, $tmpFileName_notice);
            $job_notice_image = '/public'.$tmpFilePath_notice . $tmpFileName_notice;
        }else{
            $job_notice_image = "";
        }

        //upload images notice thumbs for sp
        if(Input::hasFile('job_notice_image_sp')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('job_notice_image_sp');
            $tmpFilePath = '/uploads/job/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $SecretJob->job_notice_image_sp = '/public'.$tmpFilePath . $tmpFileName;
        }else{

            if(Input::hasFile('job_notice_image')) {
                $thumb = new ThumbsComponent();
                $src_thumb_notice = $thumb->get_src_modules($tmpFilePath_notice . $tmpFileName_notice,280,0,1,0,array('fix_width' => 1));
                $SecretJob->job_notice_image_sp = "/public".$src_thumb_notice;
            }

        }

        if (Input::has('cat'))
        {
            $list_cat = implode(",", Input::get('cat'));
            $SecretJob->list_job = ",".$list_cat.",";
        }
        if (Input::has('hang'))
        {
            $list_hang = implode(",", Input::get('hang'));
            $SecretJob->list_hangup = ",".$list_hang.",";
        }

        if (Input::has('em'))
        {
            $list_em = implode(",", Input::get('em'));
            $SecretJob->list_em = ",".$list_em.",";
        }

        if(Input::has('list_job_parent'))
        {
            $SecretJob->list_job_parent = $request->get('list_job_parent');
        }

        $url = $request->job_url;


        if(!empty($url)){
            if (!preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/", $url))
            {
                Session::flash('flash_error', lang::get('message.error_company_not_url'));
                return redirect()->back()->withInput();
            }

            else if (strpos($url, 'http://') !== 0 || strpos($url, 'https://' !== 0))
            { 
                $url = "http://" . $url;
            }
        }

        $SecretJob->job_name = $request->get('job_name');
        $SecretJob->company_id = $request->get('company_id');
        $SecretJob->store_name = $request->get('store_name');
        $SecretJob->meta_desc = $request->get('meta_desc');
        $SecretJob->meta_keyword = $request->get('meta_keyword');
        $SecretJob->job_benefit = $request->get('job_benefit');
        $SecretJob->job_image = $path;

        $SecretJob->job_salary = $request->get('job_salary');
        $SecretJob->job_qualification = $request->get('job_qualification');
        $SecretJob->job_notice_header = $request->get('job_notice_header');
        $SecretJob->job_notice_content = $request->get('job_notice_content');
        $SecretJob->job_notice_image = $job_notice_image;
        $SecretJob->content = $request->get('content');
        $SecretJob->job_description = $request->get('job_description');
        $SecretJob->treatment_welfare = $request->get('treatment_welfare');
        $SecretJob->job_holiday = $request->get('job_holiday');

        $SecretJob->access = $request->get('access');
        $SecretJob->job_location = $request->get('job_location');
        $SecretJob->map_location = $request->get('map_location');

        $SecretJob->lat = $request->get('lat');
        $SecretJob->lng = $request->get('lng');
        $SecretJob->status = $request->get('status');
        $SecretJob->start_at = $request->get('start_at');
        $SecretJob->end_at = $request->get('end_at');

        //After job_image
        $SecretJob->job_genre = $request->get('job_genre');

        //After job_holiday
        $SecretJob->job_personnel = $request->get('job_personnel');
        $SecretJob->job_appSelection = $request->get('job_appSelection');

        //After map_location
        $SecretJob->shopStyle = $request->get('shopStyle');
        $SecretJob->business_hrs = $request->get('business_hrs');
        $SecretJob->reg_holiday = $request->get('reg_holiday');
        $SecretJob->customer_base = $request->get('customer_base');
        $SecretJob->job_atmosphere = $request->get('job_atmosphere');
        $SecretJob->job_education = $request->get('job_education');
        $SecretJob->job_challenges = $request->get('job_challenges');
        $SecretJob->male_female_ratio = $request->get('male_female_ratio');
        $SecretJob->job_url = $request->get('job_url');

        $date_created = $request->get('created_at');
        if($date_created)
        {
            $date_created = Util::createDateCreatedAt($date_created);
            $SecretJob->created_at = $date_created;
        }

        $ok = $SecretJob->save();

        //TODO: update list way site, list province, list city
        $string = $SecretJob['job_name'] .' '. $SecretJob['content'] .' '. $SecretJob['treatment_welfare']
                    .' '. $SecretJob['access'] .' '. $SecretJob['job_location'] .' '. $SecretJob['map_location'];
        $stringCompany = $SecretJob->getStringCompany($SecretJob['id']);
        $string = $string . ' ' . $stringCompany;

        //update list waysite
        $SecretJob->updateListWaySite($SecretJob['id'], $string);
        //update list province
        $SecretJob->updateListProvince($SecretJob['id'], $string);
        //update list city
        $SecretJob->updateListCity($SecretJob['id'], $string);

        if($ok){
            if ($request->hasFile('images')) {
                $files = Input::file('images');
                // Making counting of uploaded images
                $file_count = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                if ($file_count){
                    foreach($files as $file) {
                        $SecretJobImage = new SecretJobImage();
                        $rules = array('file' => 'required|mimes:png,gif,jpeg,txt,pdf,doc'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                        $validator = Validator::make(array('file'=> $file), $rules);
                        if($validator->passes()){
                            $tmpFilePath = '/uploads/job/';
                            $filename = time() . '-' .$file->getClientOriginalName();
                            $upload_success = $file->move(public_path() .$tmpFilePath, $filename);
                            $job_notice_image = '/public'.$tmpFilePath . $filename;
                            $SecretJobImage->secret_job_id	 = $SecretJob->id;
                            $SecretJobImage->image = $job_notice_image;
                            $SecretJobImage->save();
                            $uploadcount ++;
                        }
                    }
                }

            }

            //thumbnail for list images
            if ($request->hasFile('images_sp')) {
                $files_thumbs = Input::file('images_sp');
                // Making counting of uploaded images
                $file_count_t = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                if ($file_count_t){
                    foreach($files_thumbs as $file_t) {
                        $SecretJobImage = new SecretJobImage();
                        $rules = array('file' => 'required|mimes:png,gif,jpeg,txt,pdf,doc'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                        $validator = Validator::make(array('file'=> $file_t), $rules);
                        if($validator->passes()){
                            $tmpFilePath_t = '/uploads/job/';
                            $filename_t = time() . '-' .$file_t->getClientOriginalName();
                            $upload_success = $file_t->move(public_path() .$tmpFilePath_t, $filename_t);
                            $job_notice_image = '/public'.$tmpFilePath_t . $filename_t;
                            $SecretJobImage->secret_job_id	 = $SecretJob->id;
                            $SecretJobImage->image = $job_notice_image;
                            $SecretJobImage->type = 1;
                            $SecretJobImage->save();
                            $uploadcount ++;
                        }
                    }
                }

            }else{
                if ($request->hasFile('images')) {
                    $thumb = new ThumbsComponent();
                    $files_t = Input::file('images');
                    // Making counting of uploaded images
                    $file_count = count($files);
                    $uploadcount = 0;
                    if ($file_count){
                        foreach($files_t as $filesp) {
                            $tmpFilePath = '/uploads/job/';
                            $filename = time() . '-' .$filesp->getClientOriginalName();
                            $SecretJobImage_sp = new SecretJobImage();
                            $src_thumb_images = $thumb->get_src_modules($tmpFilePath . $filename,280,0,1,0,array('fix_width' => 1));
                            $SecretJobImage_sp->secret_job_id	 = $SecretJob->id;
                            $SecretJobImage_sp->image = "/public".$src_thumb_images;
                            $SecretJobImage_sp->type = 1;
                            $SecretJobImage_sp->save();
                            $uploadcount ++;

                        }
                    }


                }
            }


        }
        Session::flash('flash_message', lang::get('message.create_success'));
        return redirect('admin/secret-job-manager');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        // get the SecretJob
        $secret = SecretJob::find($id);

        $list_company = Company::all('id','name');
        $arr_list =array();
        if($list_company){
            foreach ($list_company as $v){
                $arr_list[$v['id']] = $v['name'];
            }
        }
        //dd($secret);
        $list_images = SecretJobImage::select('id','image','type')->where('secret_job_id',$id)->get();
        return view('admin.pages.secretjob.form',
          array(
            'title_page' => 'オリジナル求人修正',
            'secret' => $secret,
            'list_company' => $arr_list,
            'list_images' => $list_images,
            'link_action' => 'admin/secret-job-manager/update/'.$id
          )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request){
        // get the Articles
        //dd(Input::file('images'));
        $SecretJob = SecretJob::findOrFail($id);

        $SecretJob->job_name = $request->get('job_name');
        $SecretJob->company_id = $request->get('company_id');
        $SecretJob->store_name = $request->get('store_name');

        $SecretJob->meta_desc = $request->get('meta_desc');
        $SecretJob->meta_keyword = $request->get('meta_keyword');
        $SecretJob->job_benefit = $request->get('job_benefit');

        $SecretJob->job_salary = $request->get('job_salary');
        $SecretJob->job_qualification = $request->get('job_qualification');
        $SecretJob->job_notice_header = $request->get('job_notice_header');
        $SecretJob->job_notice_content = $request->get('job_notice_content');
        $SecretJob->content = $request->get('content');
        $SecretJob->job_description = $request->get('job_description');
        $SecretJob->treatment_welfare = $request->get('treatment_welfare');
        $SecretJob->job_holiday = $request->get('job_holiday');

        $SecretJob->access = $request->get('access');
        $SecretJob->job_location = $request->get('job_location');
        $SecretJob->map_location = $request->get('map_location');

        $SecretJob->lat = $request->get('lat');
        $SecretJob->lng = $request->get('lng');
        $SecretJob->status = $request->get('status');
        $SecretJob->start_at = $request->get('start_at');
        $SecretJob->end_at = $request->get('end_at');
        //dd($SecretJob->start_at);
        //After job_image
        $SecretJob->job_genre = $request->get('job_genre');

        //After job_holiday
        $SecretJob->job_personnel = $request->get('job_personnel');
        $SecretJob->job_appSelection = $request->get('job_appSelection');

        //After map_location
        $SecretJob->shopStyle = $request->get('shopStyle');
        $SecretJob->business_hrs = $request->get('business_hrs');
        $SecretJob->reg_holiday = $request->get('reg_holiday');
        $SecretJob->customer_base = $request->get('customer_base');
        $SecretJob->job_atmosphere = $request->get('job_atmosphere');
        $SecretJob->job_education = $request->get('job_education');
        $SecretJob->job_challenges = $request->get('job_challenges');
        $SecretJob->male_female_ratio = $request->get('male_female_ratio');
        $SecretJob->job_url = $request->get('job_url');

        //date created
        $date_created = Util::createDateCreatedAt($request->get('created_at'));
        $SecretJob->created_at = $date_created;

        $url = $request->job_url;

        if(!empty($url)){
            if (!preg_match("/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/", $url))
            {
                Session::flash('flash_error', lang::get('message.error_company_not_url'));
                return redirect()->back()->withInput();
            }

            else if (strpos($url, 'http://') !== 0 || strpos($url, 'https://' !== 0))
            { 
                $url = "http://" . $url;
            }
        }



        if (Input::has('cat'))
        {
            $list_cat = implode(",",Input::get('cat'));
            $SecretJob->list_job = ",".$list_cat.",";
        }
        if (Input::has('hang'))
        {
            $list_hang = implode(",", Input::get('hang'));
            $SecretJob->list_hangup = ",".$list_hang.",";
        }

        if (Input::has('em'))
        {
            $list_em = implode(",", Input::get('em'));
            $SecretJob->list_em = ",".$list_em.",";
        }

        if(Input::has('list_job_parent'))
        {
            $SecretJob->list_job_parent = $request->get('list_job_parent');
        }

        //upload images
        if(Input::hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('file');
            $FilePath = '/uploads/job/';
            $FileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $FilePath, $FileName);
            $path = '/public'.$FilePath . $FileName;
            $SecretJob->job_image = $path;
        }

        //upload images for sp
        if(Input::hasFile('image_sp')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('image_sp');
            $tmpFilePath = '/uploads/job/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path_image_sp = '/public'.$tmpFilePath . $tmpFileName;
            $SecretJob->job_image_sp = $path_image_sp;
        }else{

            if(Input::hasFile('file') && empty($SecretJob->job_image_sp)) {
                $thumb = new ThumbsComponent();
                $src_thumb = $thumb->get_src_modules($FilePath . $FileName,280,0,1,0,array('fix_width' => 1));
                $SecretJob->job_image_sp = "/public".$src_thumb;
            }

        }

        //upload job_notice_image
        if(Input::hasFile('job_notice_image')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('job_notice_image');
            $tmpFilePath = '/uploads/job/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $job_notice_image = '/public'.$tmpFilePath . $tmpFileName;
            $SecretJob->job_notice_image = $job_notice_image;
        }


        //upload job_notice_image for sp
        if(Input::hasFile('job_notice_image_sp')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = Input::file('job_notice_image_sp');
            $tmpFilePath = '/uploads/job/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path_image_sp = '/public'.$tmpFilePath . $tmpFileName;
            $SecretJob->job_notice_image_sp = $path_image_sp;
        }else{

            if(Input::hasFile('job_notice_image') && empty($SecretJob->job_notice_image)) {
                $thumb = new ThumbsComponent();
                $src_thumb = $thumb->get_src_modules($tmpFilePath . $tmpFileName,280,0,1,0,array('fix_width' => 1));
                $SecretJob->job_notice_image_sp = "/public".$src_thumb;
            }

        }



        $image = $request->get('delete_image');
        if(!empty($image)){
            $SecretJob->job_image = "";
        }

        $delete_thumb = $request->get('delete_thumb');
        if(!empty($delete_thumb)){
            $SecretJob->job_image_sp = "";
        }


        $delete_image_notice = $request->get('delete_image_notice');
        if(!empty($delete_image_notice)){
            $SecretJob->job_notice_image = "";
        }

        $delete_thumb_notice = $request->get('delete_thumb_notice');
        if(!empty($delete_thumb_notice)){
            $SecretJob->job_notice_image_sp = "";
        }


        $ok = $SecretJob->save();


        //TODO: update list way site, list province, list city
        $string = $SecretJob['job_name'] .' '. $SecretJob['content'] .' '. $SecretJob['treatment_welfare']
                    .' '. $SecretJob['access'] .' '. $SecretJob['job_location'] .' '. $SecretJob['map_location'];
        $stringCompany = $SecretJob->getStringCompany($id);
        $string = $string . ' ' . $stringCompany;

        //update list waysite
        $SecretJob->updateListWaySite($id, $string);
        //update list province
        $SecretJob->updateListProvince($id, $string);
        //update list city
        $SecretJob->updateListCity($id, $string);


        if($ok){
            if ($request->hasFile('images')) {
                $files = Input::file('images');
                // Making counting of uploaded images
                $file_count = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                if ($file_count){
                    //SecretJobImage::where('secret_job_id',$id)->delete();
                    foreach($files as $file) {
                        $SecretJobImage = new SecretJobImage();
                        $rules = array('file' => 'required|mimes:png,gif,jpeg,txt,pdf,doc'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                        $validator = Validator::make(array('file'=> $file), $rules);
                        if($validator->passes()){
                            $tmpFilePath = '/uploads/job/';
                            $filename = time() . '-' .$file->getClientOriginalName();
                            $upload_success = $file->move(public_path() .$tmpFilePath, $filename);
                            $job_notice_image = '/public'.$tmpFilePath . $filename;
                            $SecretJobImage->secret_job_id	 = $SecretJob->id;
                            $SecretJobImage->image = $job_notice_image;
                            $SecretJobImage->save();
                            $uploadcount ++;
                        }
                    }
                }

            }


            if ($request->hasFile('images_sp')) {
                $files = Input::file('images_sp');
                // Making counting of uploaded images
                $file_count = count($files);
                // start count how many uploaded
                $uploadcount = 0;
                if ($file_count){
                    //SecretJobImage::where('secret_job_id',$id)->delete();
                    foreach($files as $file) {
                        $SecretJobImage = new SecretJobImage();
                        $rules = array('file' => 'required|mimes:png,gif,jpeg,txt,pdf,doc'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                        $validator = Validator::make(array('file'=> $file), $rules);
                        if($validator->passes()){
                            $tmpFilePath = '/uploads/job/';
                            $filename = time() . '-' .$file->getClientOriginalName();
                            $upload_success = $file->move(public_path() .$tmpFilePath, $filename);
                            $job_notice_image = '/public'.$tmpFilePath . $filename;
                            $SecretJobImage->secret_job_id	 = $SecretJob->id;
                            $SecretJobImage->image = $job_notice_image;
                            $SecretJobImage->type = 1;
                            $SecretJobImage->save();
                            $uploadcount ++;
                        }
                    }
                }

            }else{
                /*if ($request->hasFile('images')) {
                    $thumb = new ThumbsComponent();
                    $files_t = Input::file('images');
                    // Making counting of uploaded images
                    $file_count = count($files);
                    $uploadcount = 0;
                    if ($file_count){
                        foreach($files_t as $filesp) {
                            $tmpFilePath = '/uploads/job/';
                            $filename = time() . '-' .$filesp->getClientOriginalName();
                            $SecretJobImage_sp = new SecretJobImage();
                            $src_thumb_images = $thumb->get_src_modules($tmpFilePath . $filename,280,0,1,0,array('fix_width' => 1));
                            $SecretJobImage_sp->secret_job_id	 = $SecretJob->id;
                            $SecretJobImage_sp->image = "/public".$src_thumb_images;
                            $SecretJobImage_sp->type = 1;
                            $SecretJobImage_sp->save();
                            $uploadcount ++;

                        }
                    }

                }*/
            }

        }

        Session::flash('flash_message', lang::get('message.update_success'));
        return redirect()->back();
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id){

        $ok = SecretJob::find($id)->delete();
        if($ok){
            SecretJobImage::where('secret_job_id',$id)->delete();
        }
        Session::flash('flash_message', lang::get('message.delete_success'));
        return redirect()->back();
    }

    public function updateListWaySite(Request $request, $id)
    {
        $secretJob = new SecretJob();
        $resultCompany = $secretJob->updateListCity($id);
        $resultCompany = $resultCompany[0];
        dd($resultCompany);
    }

    public function delete_image(Request $request) {
        if ($request->ajax()) {
            $data = Input::all();
            $id = (int) $data['id'];
            DB::table('secret_job_img')->where('id',$id)->delete();
            return "success";
        }
        return "false";

    }


    public function upload() {
        if(Input::hasFile('images')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file =  Input::file('images');
            $tmpFilePath = '/uploads/job/';
            $tmpFileName = time() . '-' . $file[0]->getClientOriginalName();
            if($file[0]->move(public_path() . $tmpFilePath, $tmpFileName)) {
                $success = true;
                $path = '/public'.$tmpFilePath . $tmpFileName;
                $name = $tmpFileName;
            } else {
                $success = false;
            }
            if ($success === true) {
                $SecretJobImage = new SecretJobImage();
                $SecretJobImage->name = $name;
                $SecretJobImage->image = $path;
                $SecretJobImage->save();
                return response()->json(array('path'=> $path), 200);
            } elseif ($success === false) {
                // delete any uploaded files
                unlink($path);
                return response()->json(false, 200);

            } else {
                return response()->json(false, 200);
            }

        } else {
            return response()->json(false, 200);
        }
    }

    public function preview(Request $request)
    {
        $kind = $request->get('preview_kind');

        $SecretJob = new SecretJob();
        $SecretJob->job_name = $request->get('job_name');
        $SecretJob->store_name = $request->get('store_name');
        $SecretJob->company_id = $request->get('company_id');
        $SecretJob->meta_desc = $request->get('meta_desc');
        $SecretJob->meta_keyword = $request->get('meta_keyword');
        $SecretJob->job_benefit = $request->get('job_benefit');
        $SecretJob->job_salary = $request->get('job_salary');
        $SecretJob->job_qualification = $request->get('job_qualification');
        $SecretJob->job_notice_header = $request->get('job_notice_header');
        $SecretJob->job_notice_content = $request->get('job_notice_content');
        $SecretJob->content = $request->get('content');
        $SecretJob->job_description = $request->get('job_description');
        $SecretJob->treatment_welfare = $request->get('treatment_welfare');
        $SecretJob->job_holiday = $request->get('job_holiday');
        $SecretJob->access = $request->get('access');
        $SecretJob->job_location = $request->get('job_location');
        $SecretJob->map_location = $request->get('map_location');
        $SecretJob->lat = $request->get('lat');
        $SecretJob->lng = $request->get('lng');
        $SecretJob->status = $request->get('status');
        $SecretJob->job_created_at = $request->get('date_create');

        $SecretJob->start_at = $request->get('start_at');
        $SecretJob->end_at = $request->get('end_at');
        
        //After job_image
        $SecretJob->job_genre = $request->get('job_genre');

        //After job_holiday
        $SecretJob->job_personnel = $request->get('job_personnel');
        $SecretJob->job_appSelection = $request->get('job_appSelection');

        //After map_location
        $SecretJob->shopStyle = $request->get('shopStyle');
        $SecretJob->business_hrs = $request->get('business_hrs');
        $SecretJob->reg_holiday = $request->get('reg_holiday');
        $SecretJob->customer_base = $request->get('customer_base');
        $SecretJob->job_atmosphere = $request->get('job_atmosphere');
        $SecretJob->job_education = $request->get('job_education');
        $SecretJob->job_challenges = $request->get('job_challenges');
        $SecretJob->male_female_ratio = $request->get('male_female_ratio');
        $SecretJob->job_url = $request->get('job_url');

        $status =  Company::find($request->get('company_id'))['status'];

        if (Input::has('cat'))
        {
            $list_cat = implode(",",Input::get('cat'));
            $SecretJob->list_job = ",".$list_cat.",";
        }
        else
        {
            $SecretJob->list_job = "";
        }

        if (Input::has('hang'))
        {
            $list_hang = implode(",", Input::get('hang'));
            $SecretJob->list_hangup = ",".$list_hang.",";
        }
        else
        {
            $SecretJob->list_hangup = "";
        }

        if (Input::has('em'))
        {
            $list_em = implode(",", Input::get('em'));
            $SecretJob->list_em = ",".$list_em.",";
        }
        else
        {
            $SecretJob->list_em = "";
        }

        if(Input::has('list_job_parent'))
        {
            $SecretJob->list_job_parent = $request->get('list_job_parent');
        }
        else
        {
            $SecretJob->list_job_parent = "";
        }

        $slider_image = [];

        //PC
        if($kind == "PC")
        {
            //slider image PC
            if ($request->hasFile('images')) {
                $files = Input::file('images');
                // Making counting of uploaded images
                $file_count = count($files);

                if ($file_count){
                    foreach($files as $key => $file) {

                        $tmp_image_path = $file->getRealPath();
                        
                        $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                        $data = file_get_contents($tmp_image_path);
                        $slider_image[$key] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }
                }
            }
            else
            {
                //if update without change IMAGE slider PC
                if($request->get('id'))
                {
                    $slider_image_PC = DB::table('secret_job_img')
                                        ->select('image')
                                        ->where('secret_job_id', $request->get('id'))
                                        ->where('type', 0)
                                        ->get();
                    if(count($slider_image_PC) > 0)
                    {
                        foreach ($slider_image_PC as $key => $value) {
                            $slider_image[$key] = $value->image;
                        }
                    }
                }
            }

            //notice image PC
            if(Input::hasFile('job_notice_image')) {
                //upload an image to the /img/tmp directory and return the filepath.
                $tmp_image_path = Input::file('job_notice_image')->getRealPath();

                $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                $data = file_get_contents($tmp_image_path);
                $SecretJob->job_notice_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
            }
            else
            {

                $delete_image_notice = $request->get('delete_image_notice');
                //delete notice image PC
                if(!empty($delete_image_notice)){
                    $SecretJob->job_notice_image = "";
                }
                else
                {
                    //if update without change IMAGE
                    if($request->get('id'))
                    {
                        $currentSecretJob = SecretJob::find($request->get('id'));
                        $SecretJob->job_notice_image = $currentSecretJob['job_notice_image'];
                    }
                }
            }
        }
        else //SP
        {
            //slider image SP
            if ($request->hasFile('images_sp')) {
                $files = Input::file('images_sp');
                // Making counting of uploaded images
                $file_count = count($files);

                if ($file_count){
                    foreach($files as $key => $file) {

                        $tmp_image_path = $file->getRealPath();
                        
                        $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                        $data = file_get_contents($tmp_image_path);
                        $slider_image[$key] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }
                }
            }
            else
            {
                //if update without change IMAGE slider
                if($request->get('id'))
                {
                    $slider_image_SP = DB::table('secret_job_img')
                                        ->select('image')
                                        ->where('secret_job_id', $request->get('id'))
                                        ->where('type', 1)
                                        ->get();
                    if(count($slider_image_SP) > 0)
                    {
                        foreach ($slider_image_SP as $key => $value) {
                            $slider_image[$key] = $value->image;
                        }
                    }
                    else
                    {
                        //get slider PC
                        if ($request->hasFile('images')) {
                            $files = Input::file('images');
                            // Making counting of uploaded images
                            $file_count = count($files);

                            if ($file_count){
                                foreach($files as $key => $file) {

                                    $tmp_image_path = $file->getRealPath();
                                    
                                    $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                                    $data = file_get_contents($tmp_image_path);
                                    $slider_image[$key] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                }
                            }
                        }
                        else
                        {
                            //if update without change IMAGE slider PC
                            if($request->get('id'))
                            {
                                $slider_image_PC = DB::table('secret_job_img')
                                                    ->select('image')
                                                    ->where('secret_job_id', $request->get('id'))
                                                    ->where('type', 0)
                                                    ->get();
                                if(count($slider_image_PC) > 0)
                                {
                                    foreach ($slider_image_PC as $key => $value) {
                                        $slider_image[$key] = $value->image;
                                    }
                                }
                            }
                        }
                        
                    }
                }
                else //insert
                {
                    //get slider PC
                    if ($request->hasFile('images')) {
                        $files = Input::file('images');
                        // Making counting of uploaded images
                        $file_count = count($files);

                        if ($file_count){
                            foreach($files as $key => $file) {

                                $tmp_image_path = $file->getRealPath();
                                
                                $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                                $data = file_get_contents($tmp_image_path);
                                $slider_image[$key] = 'data:image/' . $type . ';base64,' . base64_encode($data);
                            }
                        }
                    }
                }
            }

            //notice image SP
            if(Input::hasFile('job_notice_image_sp')) {
                //upload an image to the /img/tmp directory and return the filepath.
                $tmp_image_path = Input::file('job_notice_image_sp')->getRealPath();

                $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                $data = file_get_contents($tmp_image_path);
                $SecretJob->job_notice_image_sp = 'data:image/' . $type . ';base64,' . base64_encode($data);
            }
            else //not has file
            {
                //delete notice image SP => get notice image PC
                $delete_thumb_notice = $request->get('delete_thumb_notice');
                if(!empty($delete_thumb_notice)){
                    $SecretJob->job_notice_image_sp = "";
                }
                else
                {
                    //if update without change IMAGE
                    if($request->get('id'))
                    {
                        $currentSecretJob = SecretJob::find($request->get('id'));
                        $SecretJob->job_notice_image_sp = $currentSecretJob['job_notice_image_sp'];
                    }
                }
                
                //get notice image PC 
                if(!$SecretJob->job_notice_image_sp)
                {
                    if(Input::hasFile('job_notice_image')) {
                        //upload an image to the /img/tmp directory and return the filepath.
                        $tmp_image_path = Input::file('job_notice_image')->getRealPath();

                        $type = pathinfo($tmp_image_path, PATHINFO_EXTENSION);
                        $data = file_get_contents($tmp_image_path);
                        $SecretJob->job_notice_image = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    }
                    else
                    {

                        $delete_image_notice = $request->get('delete_image_notice');
                        //delete notice image PC
                        if(!empty($delete_image_notice)){
                            $SecretJob->job_notice_image = "";
                        }
                        else
                        {
                            //if update without change IMAGE
                            if($request->get('id'))
                            {
                                $currentSecretJob = SecretJob::find($request->get('id'));
                                $SecretJob->job_notice_image = $currentSecretJob['job_notice_image'];
                            }
                        }
                    }
                }
            }
        }     

        $company = Company::select(
                                'c.name',
                                'c.logo',
                                'c.category',
                                'c.celebration',
                                'c.average_price',
                                'c.website',
                                'c.address',
                                'c.access_manual',
                                'c.seat_number',
                                'c.holiday',
                                'c.hp',
                                'c.short_introduction',
                                'c.introduction',
                                'c.access as company_access',
                                'c.lat as company_lat',
                                'c.lng as company_lng',
                                'c.map_location as company_map_location')
                            ->from('company as c')
                            ->where('c.id', $SecretJob->company_id)
                            ->get()->toArray();

        $detail = array();

        $detail = array_merge($SecretJob['attributes'], $company[0]);
                   
        
        if($kind == "PC")
        {
            return view('secret_kyujin.preview')->with([
                        'detail'=> $detail,
                        'slider_image' => $slider_image,
                        'status' => $status
                    ]);
        }
        
        else
        {
            return view('secret_kyujin.preview_sp')->with([
                        'detail'=> $detail,
                        'slider_image' => $slider_image,
                        'status' => $status
                    ]);
        }
    }

}
