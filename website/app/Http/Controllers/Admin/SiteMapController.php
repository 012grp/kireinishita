<?php
/**
 * User: Binh
 * Date: 2016/10/05
 * Time: 1:09 PM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Session;
//model
use App\Articles;
use App\ArticleCategories;
use App\SecretJob;
//component
use App\Http\Components\SiteMap;

class SiteMapController extends Controller
{
	public function index(Request $request)
	{
		$site_map_file = 'resources/views/pages/sitemap.blade.php';

		$xml=simplexml_load_file($site_map_file);

		return view('admin.pages.site_map')->with('xml',$xml);
	}

	public function renew(Request $request)
	{
		$arrSiteMap = SiteMap::renew();	
		return view('admin.pages.site_map')->with('arrSiteMap',$arrSiteMap);
	}


	public function create(Request $request)
	{
		$arrSiteMap = SiteMap::handleSiteMapRequest($request->site_map);
		if(SiteMap::create($arrSiteMap))
		{
			Session::flash('flash_message', '正常に更新');
			return view('admin.pages.site_map_message');
		}
	}
}