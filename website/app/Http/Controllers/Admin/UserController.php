<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

//model
use App\User;
use App\UserKeywords;

class UserController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-12-17
        Parameters: []
        Description: activity
        @return: 
    */
    public function getIndex(Request $request) {
		/* #regoin: REQUEST */
		{
			$fromYear	=$request->input('fromYear');
			$fromMonth	=$request->input('fromMonth');
			$toYear		=$request->input('toYear');
			$toMonth	=$request->input('toMonth');
			
			$findEmail	=$request->input('find_email');
			$findName	=$request->input('find_name');
			$findType	=$request->input('find_type');
		}
		/* #end regoin */
		
		$paginatePage 	= 100;
		$users 			= User::where('status', '=', 1);
		$usersAll		= User::where('status', '=', 1);
		
		
		/* #regoin: email */
		{
			if(!empty($findEmail))
			{
				$users 		= $users->where('email', 'LIKE', "%$findEmail%");
				$usersAll 	= $users->where('email', 'LIKE', "%$findEmail%");
			}
		}
		/* #end regoin */
		
		/* #regoin: name */
		{
			if(!empty($findName))
			{
				$users 		= $users->where('name', 'LIKE', "%$findName%");
				$usersAll 	= $users->where('name', 'LIKE', "%$findName%");
			}
		}
		/* #end regoin */
		
		/* #regoin: type */
		{
			if($findType !== '' && $findType !== null)
			{
				$findType = intval($findType);
				$users 		= $users->where('style', '=', $findType);
				$usersAll 	= $users->where('style', '=', $findType);
			}
		}
		/* #end regoin */
		
		/* #regoin: filter by DATE */
		{
			$fromDate = '';
			$toDate = '';
			if(!empty($fromYear) && !empty($fromMonth))
			{
				$fromDate 	= "$fromYear-$fromMonth-01";
				$users 		= $users->where('created_at', '>=', $fromDate);
				$usersAll 	= $usersAll->where('created_at', '>=', $fromDate);
			}
			if(!empty($toYear) && !empty($toMonth))
			{
				$toDate 	= "$toYear-$toMonth-01";
				$users 		= $users->where('created_at', '<=', $toDate);
				$usersAll 	= $usersAll->where('created_at', '<=', $toDate);
			}
		}
		/* #end regoin */
		
		
		
		
		
		
		$users 			= $users->paginate($paginatePage);
		$usersAll		= $usersAll->get();
		
		$emailTotal 	= 0;
		$facebookTotal 	= 0;
		$twitterTotal 	= 0;
		
		foreach( $usersAll as $x)
		{
			switch($x->style)
			{
				case 0: $emailTotal ++; break; 
				case 1: $facebookTotal ++; break;
				case 2: $twitterTotal ++; break;
			}	
		}
		
		return view('admin.pages.user', [
			'title' 		=> 'User',
			'pagename' 		=> 'User',
			
			'emailTotal' 	=> $emailTotal,
			'facebookTotal' => $facebookTotal,
			'twitterTotal' 	=> $twitterTotal,
			
			'users' 		=> $users
		]);
    }//End function
}