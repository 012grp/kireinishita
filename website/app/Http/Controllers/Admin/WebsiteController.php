<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

//model
use App\Contents;
use App\BaseUrls;

class WebsiteController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-09-29
        Parameters: []
        Description: activity
        @return: 
    */
    public function index() {
        //TODO:
        /*
            Select count(c.site_id) as job_number, sum(c.job_url_clicked) as clicked_total, b.* 
            from contents c join base_urls b on c.site_id = b.id
            group by c.site_id;
        */
        $websites = Contents::
                    join('base_urls', 'base_urls.id','=','contents.site_id')
                    ->groupBy('contents.site_id')
                    ->select(DB::raw(
                                'count(contents.site_id) as job_number,'.
                                'sum(contents.job_url_clicked) as clicked_total,'.
                                'base_urls.*'
                            ))
                    ->orderBy('contents.job_url_clicked', 'desc')
                    ->get();
        
        return view('admin.pages.website', [
            'websites' => $websites
        ]);
    }//End function
}