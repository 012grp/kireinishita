<?php

namespace App\Http\Controllers;

//
use Illuminate\Http\Request;
use App\Http\Requests;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Router;
use Auth;
use Input;
use Session;
use DB;
//modal
use App\Alliance;
use App\JobCategory;
use App\AllianceJobCategory;
//Helper
use App\Http\Helpers\common;

//component
use App\Http\Components\Util;

class AllianceController extends Controller{

	protected $pc_pagination = 5;
	protected $sp_pagination = 10;
    /*
        TODO: show secret job 
    */
    public function index(Router $router, Request $request)
    {
		if ($this->is_mobile)
		{
			$pagination = $this->sp_pagination;
			$view_tail = '_sp';
		}
		else
		{
			$pagination = $this->pc_pagination;
			$view_tail = '';
		}
		
        $secret_jobs = Alliance::select(
            'alliance_job.id',
            'alliance_job.job_name',
            'alliance_job.job_salary',
            'alliance_job.job_location',
            'alliance_job.job_thumbnail',
            'alliance_job.job_image',
            'alliance_job.job_celebration',
            'alliance_job.job_holiday',
            'alliance_job.created_at',
            'alliance_job.is_new',
            'alliance_job.list_em',
            'alliance_job.list_job',
            'alliance_job.list_job_parent',
            'alliance_job.list_hangup',
            'alliance_job.list_province',
            'alliance_job.list_city',
            'alliance_job.company_name',
            'alliance_job.shop_name',
            'alliance_job.shop_id',
            'alliance_job.url'
        );
        $secret_jobs= $secret_jobs->where('alliance_job.status', 1)
            ->orderBy('alliance_job.created_at', 'desc')
            ->paginate($pagination);

        $total = $secret_jobs->total();

		return view('alliance.index'.$view_tail)->with(['secret_jobs' => $secret_jobs, 'total' => $total]);

    }

    public function share(Router $router, Request $request, $id)
    {
        $expire = $request->input('e');
        $hash = $request->input('h');
        $checkValid = common::checkShareLink($id, $expire, $hash);
        if($checkValid)
        {
            //$checkStatus=false
            return $this->detail($router,$request, $id, false);
        }
        else
        {
            return view('errors.outofdate');
        }
    }

    /*
        TODO: show secret job detail
    */
    public function detail(Router $router, Request $request, $id, $checkStatus=true)
    {
        $secretJob = new Alliance();
        $detail = $secretJob->select(
                                        'a.*',
                                        'c.id as company_id',
                                        'c.hp',
                                        'c.address'
                                    )
                            ->from('alliance_job as a')
                            ->where('a.id', $id)
                            ->leftJoin('company as c', 'c.name', '=', 'a.company_name')
                            ->first();
        
        if(!$detail)
        {
            return redirect('/alliance');
        }
        //update view
        $secretJob->where('id', $id)->update(['view' => $detail['view'] + 1]);

        $cat_id = $detail['list_job'];
        $cat_id = intval(str_replace(',', '', $cat_id));


        $category = new AllianceJobCategory();
        $category = $category->find($cat_id);

        if (!$detail) {
            $allianceUrl = '/' . Util::getUrlName('alliancePage');
            return redirect($allianceUrl);
        }
        if ($this->is_mobile)
            return view('alliance.detail_sp')->with(['detail' => $detail, 'category' => $category]);
        else
            return view('alliance.detail')->with(['detail' => $detail, 'category' => $category]);
    }
	
	
	public function reference(Request $request)
    {
        $contentId = $request->route('job_id');
        $content = Alliance::select(array('id', 'url', 'job_url_clicked'))
            ->where('id', $contentId)
            ->where('status', 1)
            ->first();
        if ($content != null) {
            $url = $content->url;
            if (!empty($url)) {
                try {
                    $content->job_url_clicked = $content->job_url_clicked + 1;
                    $content->save();
                    return redirect($url);
                } catch (Exception $e) {
                    return redirect($url);
                }
            }
        }
        return redirect('/alliance');
    }
}
