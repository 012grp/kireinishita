<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;

//model
use App\Area;

class AreaController extends Controller
{
    public function getCityByProvince(Request $request)
    {
    	//TODO: getCityByProvince
		$provinceId = intval($request->input('province_id'));
		if($provinceId)
		{
    		$area = new Area();
			$data = $area->getCitiesByProvince($provinceId);
			return json_encode($data);
		}
		return '';
	}
}
