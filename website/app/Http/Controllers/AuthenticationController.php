<?php

namespace App\Http\Controllers;

//
use Illuminate\Http\Request;
use App\Http\Requests;
use Redirect;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use Session;
use Socialite;
use URL;
use Mail;
use Validator;
use App\Http\Requests\AccountRequest;

//component
use App\Http\Components\UserComponent;
use App\Http\Components\BookmarkComponent;

//modal
use App\User;
use App\Bookmarks;
use Illuminate\Routing\UrlGenerator;
use App\Http\Components\Util;


class AuthenticationController extends Controller
{
    var $loginError = '/account/';
    var $loginSuccess = '/mybox';

    // To redirect facebook
    public function facebook_redirect()
    {
        return Socialite::driver('facebook')->redirect();
        // return Socialite::with('facebook')->redirect();
    }

    // to get authenticate user data
    public function facebook(Request $request)
    {
        try {
            if($request->query('code'))
            {
                $user = Socialite::driver('facebook')->user();
                //$user = Socialite::with('facebook')->user();
                if($user->id && $user->email)
                {
                    $info = array(
                        'style_id' => $user->id,
                        'email' => $user->email,
                        'nick_name' => $user->nickname,
                        'name' => $user->name,
                        'avatar' => $user->avatar,
                        'style' => 1
                    );

                    UserComponent::login_facebook($info);
                    //if has session previous page is redirect previous page
                    if (Session::has('bookmarkJob')) {
                        $previousPage = Session::get('bookmarkJob.previous_page');
                        return Redirect::to($previousPage);
                    }
                    return Redirect::to($this->loginSuccess);
                }
            }
            else
            {
                return Redirect::to($request->root() . '/account/');
            }

        } catch (\Exception $e) {

            echo '<!--';
            print_r($e);
            echo '-->';
        }

    }

    // To redirect twitter
    public function twitter_redirect()
    {
        return Socialite::driver('twitter')->redirect();
    }

    // to get authenticate user data
    public function twitter()
    {
        try {
            $user = Socialite::driver('twitter')->user();

            $info = array(
                'style_id' => $user->id,
                'nick_name' => $user->nickname,
                'name' => $user->name,
                'style' => 2
            );

            UserComponent::login_twitter($info);

            //if has session previous page is redirect previous page
            if (Session::has('bookmarkJob')) {
                $previousPage = Session::get('bookmarkJob.previous_page');
                return Redirect::to($previousPage);
            }
            return Redirect::to($this->loginSuccess);

        } catch (Exception $e) {
            echo '<!--';
            print_r($e);
            echo '-->';
        }

    }

    public function login(Request $request)
    {
        if (UserComponent::is_login()) {
            return Redirect::to($this->loginSuccess);
        }

        if ($request->isMethod('post')) {
            $input = $request->only('email', 'password');
            foreach ($input as $key => $val) {
                //TODO
                if (empty($val)) {
                    if($key ==  'email')
                    {
                        return Redirect::to($this->loginError)->withErrors(['login' => 'メールかパスワードが間違っています']);
                    }
                    else
                    {
                        return Redirect::to($this->loginError)->withErrors(['login' => 'メールかパスワードが間違っています']);
                    }
                    break;
                }
            }

            if (UserComponent::login($input['email'], $input['password'])) {
                //if has session previous page is redirect previous page
                if (Session::has('bookmarkJob')) {
                    $previousPage = Session::get('bookmarkJob.previous_page');
                    return Redirect::to($previousPage);
                }
                return Redirect::to($this->loginSuccess);
            } else {
                return Redirect::to($this->loginError)->withErrors(['login' => 'メールかパスワードが間違っています']);
            }

        }
        
        return redirect("/account/");
    }
	
	
	
    public function register(AccountRequest $request)
    {
        //TODO:
        $input = $request->only('email', 'password');

        // foreach ($input as $key => $val) {
        //     //TODO
        //     if (empty($val)) {
        //         if($key == 'email')
        //         {
        //             return Redirect::to($this->loginError)->withErrors(['register' => 'メールアドレスを入力してください。']);
        //         }
        //         else
        //         {
        //             return Redirect::to($this->loginError)->withErrors(['register' => 'パスワードを入力してください。']);
        //         }
        //         break;
        //     }
        // }

        if (User::where(['email' => $input['email'], 'status' => 1, 'style' => 0])->exists()) {
            return Redirect::to($this->loginError)->withErrors(['register' => "メールアドレスは既に登録されています。"]);
        }

        UserComponent::register($input['email'], $input['password']);

        return Redirect::to($this->loginSuccess);
    }//End function

    /* 
        Author: yourName
        Last code: 2015-09-16
        Parameters: []
        Description: activity
    */
    public function email()
    {
        try {
            $input = Request::only('email', 'password');

            $user = User::where(array(
                'email' => $input['email'],
                'password' => $input['password'],
                'status' => 1
            ))->first();

            if (empty($user)) {
                return Redirect::to($this->loginError)->withErrors(['email or password is not correct']);
            }

            return $this->setSession($user);
        } catch (Exception $e) {
            echo '<!--';
            print_r($e);
            echo '-->';
        }
    }//End function

    public function logout()
    {
        UserComponent::logout();
        return Redirect::to($this->loginSuccess);
    }

	
	/*---------------------AJAX----------------------*/
	public function ajaxLogin(Request $request)
    {
        if (UserComponent::is_login()) {
            return 1;
        }
		$input = $request->only('email', 'password');
		foreach ($input as $key => $val) {
			//TODO
			if (empty($val)) {
				if($key ==  'email')
				{
					return 'メールアドレスを入力してください。';
				}
				else
				{
					return 'パスワードを入力してください。';
				}
				break;
			}
		}

		if (UserComponent::login($input['email'], $input['password'])) {
			//if has session previous page is redirect previous page
			if (Session::has('bookmarkJob')) {
				$previousPage = Session::get('bookmarkJob.previous_page');

			}
			return 1;
		} else {
			return 'Email or password is incorrect';
		}

    }
	public function ajaxlogout()
	{
		UserComponent::logout();
		return 1;
	}
	public function ajaxRegister(Request $request)
	{		//TODO:
        $input = $request->only('email', 'password');
        $accountRequest = new AccountRequest();
        
        $validator = \Validator::make($input, $accountRequest->rules(), $accountRequest->messages());
        if ($validator->fails()) {
            return $validator->messages()->all()[0];
        }
        // foreach ($input as $key => $val) {
        //     //TODO
        //     if (empty($val)) {
        //         if($key == 'email')
        //         {
        //             return 'メールアドレスを入力してください。';
        //         }
        //         else
        //         {
        //             return 'パスワードを入力してください。';
        //         }
        //         break;
        //     }
        // }

        if (User::where(['email' => $input['email'], 'status' => 1, 'style' => 0])->exists()) {
            return "メールアドレスは既に登録されています。";
        }

        UserComponent::register($input['email'], $input['password']);
        return 1;
	}
	
	public function ajaxForgotPassword(Request $request)
    {
            $email = $request->email;
            $user = User::where('status', '=', 1)
                ->where('email', '=', $email)
                ->first();
			//Return with Error
            if (!$user)
                return 'このメールアドレスはパスワードリセットリクエストに利用できません。もう一度確認してください。';

            $user->reset_password = Util::microtime(43200);
            $user->save();
            $userId = $user['id'];
            $hash = Util::hash($user['reset_password'], 100);
            $resetPasswordLink = URL::to('/') . "/account/reset-password?u=" . $userId . "&h=" . $hash;

            $rs = Mail::queue(array('text' => env('RESET_PASSWORD_MAIL_TEMPLATE')),
                ['mai' => $email, 'resetPasswordLink' => $resetPasswordLink],
                function ($message) use ($user) {
                    $subject = env('RESET_PASSWORD_SUBJECT');
                    $userName = $user['name'];

                    $message->from(env('FROM_ADDRESS'), env('FROM_NAME'));
                    $message->to($user['email'], $userName)
                        ->subject($subject);
                });
		
			//Return with succes
            return 1;

    }
	
	
    /* 
        Author: Xuong
        Last code: 2015-09-17
        Parameters: $user[id: name: email: password:]
        Description: ...
        
        @return: boolean
    */
    private function setSession($user)
    {
        //TODO:
        /*
            social: after successful
            > Check in database to find this user
            > if not exist -> insert
            > else get id
            normal: just save
        */
        if (isset($user['id'])) {
            Session::put('user', $user);
        } //for social
        else {
            if (!empty($user['email'])) {
                $_user = User::where('email', $user['email'])
                    ->where('status', 1)
                    ->first();
                //not Exist
                if (!isset($_user) && empty($_user)) {
                    $id = User::insertGetId($user);
                    $_user = User::find($id);
                }

                Session::put('user', $_user);
            } else {
                return Redirect::to($this->loginError)->withErrors(['can not login by it']);
            }
        }
        return Redirect::to($this->loginSuccess);
    }//End function

    /* 
        Author: Xuong
        Last code: 2015-09-day
        Parameters: Model User
        Description: activity
    */
    private function getSessionStyle($user)
    {
        //TODO:
        return array(
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email
        );
    }//End function

    public function bookmarkJob(Request $request)
    {
        $job_id = $request->id;
        $is_secret_job = $request->is_secret_job;

        return BookmarkComponent::bookmark_job($job_id, $is_secret_job);
    }

    public function checkBookmark(Request $request)
    {
        //$path = $request->path;
        $id = $request->id;
        $is_secret_job = $request->is_secret_job;

        return BookmarkComponent::check_bookmark($id, $is_secret_job);
    }

    public function checkLoginToLoadBookmark(Request $request)
    {
        $path = $request->path;
        return BookmarkComponent::check_login_to_load_bookmark($path);
    }

    public function isLogin(Request $request)
    {
        return UserComponent::is_login();
    }

    public function forgotPassword(Request $request)
    {

        if ($request->isMethod('Post')) {
            $email = $request->email;
            $user = User::where('status', '=', 1)
                ->where('email', '=', $email)
                ->first();
			
			//Return with Error
            if (!$user)
                return redirect('/account/forget-password')
				->withErrors(['fogot_password' => 'このメールアドレスはパスワードリセットリクエストに利用できません。もう一度確認してください。']);

            $user->reset_password = Util::microtime(43200);
            $user->save();
            $userId = $user['id'];
            $hash = Util::hash($user['reset_password'], 100);
            $resetPasswordLink = URL::to('/') . "/account/reset-password?u=" . $userId . "&h=" . $hash;

            $rs = Mail::queue(array('text' => env('RESET_PASSWORD_MAIL_TEMPLATE')),
                ['mai' => $email, 'resetPasswordLink' => $resetPasswordLink],
                function ($message) use ($user) {
                    $subject = env('RESET_PASSWORD_SUBJECT');
                    $userName = $user['name'];

                    $message->from(env('FROM_ADDRESS'), env('FROM_NAME'));
                    $message->to($user['email'], $userName)
                        ->subject($subject);
                });
			
			//Return with succes
            return redirect('/account/forget-password')
				->withErrors(['sent_reset_mail' => 'リセット確認メールが送信されました']);
        }

        return view('forgot_password.index');
    }

    public function reset_password(Request $request)
    {

        $requestId = intval($request->input('u'));
        $requestHash = $request->input('h');

        if (empty($requestId) || empty($requestHash))
            return redirect('/');

        $user = User::where('id', '=', $requestId)
            ->where('reset_password', '<>', '')
            ->where('status', '=', 1)
            ->first();

        //NO EXISTS
        if (!$user) {

            if($this->is_mobile)
            {
                return view('reset_password.index_sp', array(
                    'invalid_message' => 'このメールアドレスはパスワードリセットリクエストに利用できません。もう一度確認してください。'
                ));
            }

			return view('reset_password.index', array(
				'invalid_message' => 'このメールアドレスはパスワードリセットリクエストに利用できません。もう一度確認してください。'
			));
        }

        $hash = Util::hash($user['reset_password'], 100);
        if ($requestHash != $hash) {
			
            if($this->is_mobile)
            {
                return view('reset_password.index_sp', array(
                    'invalid_message' => 'このメールアドレスはパスワードリセットリクエストに利用できません。もう一度確認してください。'
                ));
            }

			return view('reset_password.index', array(
				'invalid_message' => 'このメールアドレスはパスワードリセットリクエストに利用できません。もう一度確認してください。'
			));
        }

        if($this->is_mobile)
        {
            return view('reset_password.index_sp', [
                'requestId' => $requestId,
                'requestHash' => $requestHash,
            ]);
        }

        return view('reset_password.index', [
            'requestId' => $requestId,
            'requestHash' => $requestHash,
        ]);
    }

    public function set_new_password(Request $request)
    {
        if ($request->isMethod('Post')) {
            $requestId = intval($request->input('requestId'));
            $requestHash = $request->input('requestHash');
            $password = $request->input('password');
            $passwordConfirm = $request->input('password_confirmation');

            if ($password != $passwordConfirm)
                return redirect('/account/reset-password?u=' . $requestId . '&h=' . $requestHash)->withErrors(['reset_password' => 'パスワードと確認のパスワードが一致していません']);

            $user = User::where('id', '=', $requestId)
                ->where('reset_password', '<>', '')
                ->where('status', '=', 1)
                ->first();
            //NO EXISTS
            if(!$user)
                return redirect('/account/reset-password?u=' . $requestId . '&h=' . $requestHash)->withErrors(['reset_password' => 'このメールアドレスはパスワードリーセットリクエストに利用できません。もう一度確認してください']);

            $user->password = Util::hash($password);
            $user->reset_password = '';
            $user->save();

            if($this->is_mobile)
            {
                return view('reset_password.success_sp');
            }
            return view('reset_password.success');
        }
    }

    public function testSendMail()
    {
        $rs = Mail::queue(array('text' => env('RESET_PASSWORD_MAIL_TEMPLATE')),
            ['mai' => "it@jellyfishhr.com", 'resetPasswordLink' => "http://i-eat.localhost/account/reset-password?"],
            function ($message) {
                $subject = env('RESET_PASSWORD_SUBJECT');
                $message->from(env('FROM_ADDRESS'), env('FROM_NAME'));
                $message->to("it@jellyfishhr.com", 'Test User')
                    ->subject($subject);
            });
        return view('contact.thanks');
    }
	
	public function loginOrSignup(Request $request)
	{
		$input = $request->only('email', 'password');
		$validator = Validator::make($input, [
									'email' => 'required|email',
									'password' => 'required',
								]);
		if($validator->fails())
		{
			return -2;
		}
		return UserComponent::loginOrSignup($input['email'],$input['password']);
	}
}