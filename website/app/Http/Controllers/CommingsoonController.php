<?php

namespace App\Http\Controllers;


use View;
use Route;
use Response;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;

class CommingsoonController extends Controller
{
	public function index()
	{
		//TODO: top
		if($this->is_mobile)
			return view('commingsoon_sp');
		else
			return view('commingsoon');
	}
}