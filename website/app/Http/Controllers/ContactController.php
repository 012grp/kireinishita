<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Session;
use App\Http\Requests\ContactRequest;
use Mail;
use Illuminate\Support\Facades\Lang;

//model
use App\Company;


class ContactController extends Controller
{
    /*
        TODO: return view enter contact information
    */
	public function index(Router $router, Request $request)
    {
    	$base64_ID = $request->query('id');

    	if($base64_ID)
    	{
    		$id = base64_decode($base64_ID);
    		$company = Company::select('name', 'email')
    							->where('id', $id)
    							->get()->toArray();
    		if($company)
    		{
    			$company = $company[0];
    			$contact = array(
    				'kind_id'		=> 2,
    				'kind_name' 	=> '法人',
    				'company' 		=> $company['name'],
    				'mail'	  		=> $company['email'],
    				'name'	  		=> '',
    				'content'	 	=> ''
    			);

    			Session::put('contact', $contact);

    			return redirect('/contact');
    		}
    	}
    	
		if($this->is_mobile)
		{
			return view('contact.index_sp');
		}
		return view('contact.index');
	}

	/*
        TODO: confirm contact infomation
    */
	public function confirm(Router $router, ContactRequest $request)
    {		
    	if ($request->isMethod('post')) {

    		$select_kind = $request->kind;

    		//if not company or choose kind is user
    		// if(!$company || $select_kind == 1)
    		// {
    		// 	$company = null;
    		// }

    		if($select_kind == 1)
    		{
    			$kind_name = '個人';
    		}
    		else
    		{
    			$kind_name = '法人';
    		}

    		$contact = array(
    				'kind_id'		=> $request->kind,
    				'kind_name' 	=> $kind_name,
    				'company' 		=> $request->company,
    				'name'    		=> $request->name,
    				'mail'	  		=> $request->mail,
    				'content'	 	=> $request->content
    			);
			
    		Session::put('contact', $contact);
			
			if($this->is_mobile)
			{
				return view('contact.confirm_sp');
			}
			
		    return view('contact.confirm');
		}
		return redirect('/');
	}

	/*
        TODO: sendmail to admin and user after send contact info
    */
	public function thanks(Router $router, Request $request)
    {
    	if ($request->isMethod('post')) {
    		try{
			    if (Session::has('contact'))
				{
					$contact = Session::get('contact');
					
					$subjectThanks = env('CONTACT_THANKS_SUBJECT');
					$subjectToAdmin = env('CONTACT_TO_ADMIN_SUBJECT');
					$templateUserThanksMail = env('CONTACT_USER_THANKS_MAIL_TEMPLATE');
					$templateUserToAdmin = env('CONTACT_USER_TO_ADMIN_TEMPLATE');
					$templateCompanyThanksMail = env('CONTACT_COMPANY_THANKS_MAIL_TEMPLATE');
					$templateCompanyToAdmin = env('CONTACT_COMPANY_TO_ADMIN_TEMPLATE');

					$fromName = env('FROM_NAME');
					$fromAddress = env('FROM_ADDRESS');

					$adminName = env('ADMIN_NAME');
					$adminAddress = env('ADMIN_ADDRESS');

					//set template if choose company
					if($contact['kind_id'] == 2)
					{
						$templateThanksMail = $templateCompanyThanksMail;
						$templateToAdmin = $templateCompanyToAdmin;
					}
					else //set template if choose user
					{
						$templateThanksMail = $templateUserThanksMail;
						$templateToAdmin = $templateUserToAdmin;
					}

					$debugMode = env('SENDMAIL_TEST_MODE');
                    $stringBCC = env('TEST_EMAIL_BCC');
					$arrMailBCC = [];
                    if(isset($stringBCC) && $debugMode == true)
                    {
                        $arrMailBCC = explode(',', $stringBCC);
                    }

                    Mail::queue(['text' => $templateToAdmin], ['contact' => $contact], function($message) use($contact, $subjectToAdmin, $fromName, $fromAddress, $adminName, $adminAddress, $arrMailBCC)
			        {
			            $message->from($fromAddress, $fromName);
			            $message->to($adminAddress, $adminName)
			            		->bcc($arrMailBCC)
								->subject($subjectToAdmin);
			        });

					Mail::queue(['text' => $templateThanksMail], ['contact' => $contact], function($message) use($contact, $subjectThanks, $fromName, $fromAddress, $arrMailBCC)
			        {
			            $message->from($fromAddress, $fromName);
			            $message->to($contact['mail'], $contact['name'])
			            		->bcc($arrMailBCC)
								->subject($subjectThanks);
			        });

			        //detete session
					Session::forget('contact');
					
					if($this->is_mobile)
					{
						return view('contact.thanks_sp');
					}
					
			        return view('contact.thanks');
				}
			} 
			catch(\Exception $e){
			    Session::flash('flash_message', lang::get('message.contact_sendmail_exception_error'));
			    return redirect('/contact');
			}
    		
    	}

    	return redirect('/');
	}


}
