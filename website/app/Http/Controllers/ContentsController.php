<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contents;
use App\Keywords;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Session;

use App\Articles;
use App\ArticleCategories;
use DB;
use Illuminate\Pagination\Paginator;



class ContentsController extends Controller
{
    /*
        TODO: show 9(PC) or 10(SP) new article content by create time
    */
	public function index(Router $router, Request $request)
    {
        //if($this->is_mobile)
        //    $limit = 10;
        //else
        //    $limit = 9;
        $newArticle = Articles::select('a.id',
                                    'a.image',
                                    'a.image_thumb',
                                    'a.image_sp',
                                    'a.image_sp_thumb',
                                    'a.title',
                                    'a.view',
                                    'a.created_at',
                                    'acc.category', 
                                    'acc.category_alias',
                                    'acc.class_name',
                                    'acc.description as cat_description')
                            ->from('article_content as a')
                            ->where('a.status', 1)
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->orderBy('a.created_at', 'desc')
                            //->skip(0)->take($limit)->get();
                            ->paginate(10);
        
        if($this->is_mobile)
            return view('contents.index_sp')->with(['newArticle' => $newArticle,
              "description" => trans('webinfo.meta_desc_content')
              ]);
        else
            return view('contents.index')->with(['newArticle' => $newArticle,
              "description" => trans('webinfo.meta_desc_content')
              ]);
	}

    /*
        TODO: load all article contents by category name
    */
    public function category(Router $router, Request $request, $cat_name)
    {
        $articles = ArticleCategories::select('acc.category',
                                        'acc.category_name',
                                        'acc.category_alias',
                                        'acc.class_name',
                                        'acc.description as cat_description',
                                        'acc.meta_desc',
                                        'acc.web_title',
                                        'acc.meta_key',
                                        'a.id',
                                        'a.image',
                                        'a.image_thumb',
                                        'a.image_sp',
                                        'a.image_sp_thumb',
                                        'a.title',
                                        'a.view',
                                        'a.created_at'
                                        )
                            ->from('article_content_category as acc')
                            ->leftJoin('article_content as a', 'acc.id', '=', 'a.cat_id')
                            ->where('acc.category_alias', $cat_name)
                            ->where(function($query){
                                 $query->where('a.status', 1);
                                 $query->orWhereNull('a.status');
                             })
                            ->orderBy('a.created_at', 'desc')->paginate(10);

        if($this->is_mobile)
            return view('contents.category_sp')
						->with('current_cat',$cat_name)
						->with('articles',$articles);
        else
            return view('contents.category')->with('articles',$articles);
    }

    public function detail(Router $router, Request $request, $cat_name, $id)
    {
        $article_content = ArticleCategories::select('a.*', 
                                                'acc.category', 
                                                'acc.category_alias',
                                                'acc.class_name',
                                                'acc.web_title',
                                                'acc.description as cat_description')
                            ->from('article_content as a')
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->where('acc.category_alias', $cat_name)
                            ->where('a.status', 1)
                            ->where('a.id', $id)->get();

        if(count($article_content) > 0)
        {
            $related_content = ArticleCategories::select('a.id',
                                                    'a.image',
                                                    'a.image_thumb',
                                                    'a.image_sp',
                                                    'a.image_sp_thumb',
                                                    'a.title',
                                                    'a.view',
                                                    'a.created_at',
                                                    'acc.category', 
                                                    'acc.category_alias',
                                                    'acc.class_name',
                                                    'acc.meta_desc',
                                                    'acc.description as cat_description')
                            ->from('article_content as a')
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->where('acc.category_alias', $cat_name)
                            ->where('a.status', 1)
                            ->where('a.id','<>', $id)
                            ->orderBy('a.created_at', 'desc')
                            ->skip(0)->take(5)->get();

            Articles::where('id', $id)->update(['view' => $article_content[0]['view'] + 1]);
        
            if($this->is_mobile)
                return view('contents.detail_sp')
					->with('article_content',$article_content[0])
					->with('current_cat',$article_content[0]['category_alias'])
                    ->with('related_content',$related_content)                   ;
            else
                return view('contents.detail')->with('article_content',$article_content[0])
                    ->with('related_content',$related_content);
        }
        else
        {
            return redirect()->action('ContentsController@category', $cat_name);
        }
        
    }
}
