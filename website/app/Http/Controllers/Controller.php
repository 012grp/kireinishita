<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Components\Util;
use View;
use Session;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    protected $is_mobile = false;
    /* 
        Author: Xuong
        Last code: 2015-09-18
        Parameters: []
        Description: start all of controllers
    */
    public function __construct()
    {
        View::share('user', Session::get('user'));
        if (Util::isMobile()) {
            $this->is_mobile = true;
            View::share('is_mobile', true);
        } else {
            View::share('is_mobile', false);
        }
    }//End function

}
