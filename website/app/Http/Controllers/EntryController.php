<?php

namespace App\Http\Controllers;

//
use Illuminate\Http\Request;
use App\Http\Requests;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Router;
use Auth;
use Input;
use Session;
use DB;

use App\Http\Requests\EntryRequest;
use Illuminate\Support\Facades\Lang;
use Mail;
//modal
use App\Entry;
use App\SecretJob;

//component
use App\Http\Components\Util;
use App\Http\Components\EntryComponent;



class EntryController extends Controller
{
    /*
        TODO: show secret job 
    */
    public function index(Router $router, Request $request, $secret_id)
    {		
        $secret = SecretJob::select('s.id', 
									's.job_name', 
									's.list_em', 
									's.list_job', 
									'c.name', 
									'c.average_price', 
									'c.address', 
									'c.email')
                    ->from('secret_job as s')
                    ->leftJoin('company as c', 's.company_id', '=', 'c.id')
                    ->where('s.id', $secret_id)
                    ->where('s.status', 1)
                    ->first();
        //if not exist secret job
        if(!$secret)
        {
            return redirect('/');
        }
        
        $entryURL = $request->URL();
        Session::put('entry.secret_id', $secret['id']);
        Session::put('entry.entryURL', $entryURL);
        Session::put('entry.company_name', $secret['name']);
        Session::put('entry.average_price', $secret['average_price']);
        Session::put('entry.address', $secret['address']);
        Session::put('entry.company_email', $secret['email']);
        Session::put('entry.job_name', $secret['job_name']);
		
		if($this->is_mobile)
		{
			return view('entry.index_sp')->with(['secret'=> $secret]);
		}
		
        return view('entry.index')->with(['secret'=> $secret]);
    }


    public function confirm(Router $router, EntryRequest $request)
    {
        if ($request->isMethod('post')) 
        {
            if (Session::has('entry'))
            {
                $entryURL = Session::get('entry.entryURL');
                
                $info = array();

                $info['id'] = Session::get('entry.id');
                $info['entryURL'] = $entryURL;
                $info['company_name'] = Session::get('entry.company_name');
                $info['average_price'] = Session::get('entry.average_price');
                $info['address'] = Session::get('entry.address');
                $info['company_email'] = Session::get('entry.company_email');
                $info['job_name'] = Session::get('entry.job_name');
                $info['province'] = $request->province;
                $info['city'] = $request->city;

                $info['year'] = $request->year;
                $info['month'] = $request->month;
                $info['day'] = $request->day;

                $provinceInfo = Util::getProvince($info['province']);
                if(!$provinceInfo)
                {
                    return Redirect::to($entryURL)->withErrors(['province_invalid'=>'error']);
                }
                
                $cityInfo = Util::getCity($info['province'], $info['city']);

                if(!$cityInfo)
                {
                    return Redirect::to($entryURL)->withErrors(['city_invalid'=>'error']);
                }

                $info['province_name'] = $provinceInfo['name'];
                $info['city_name'] = $cityInfo['name'];

                if(! checkdate($info['month'], $info['day'], $info['year']))
                {
                    return Redirect::to($entryURL)->withErrors(['birthday_invalid'=>'error']);
                }
                

                $info['name'] = $request->name;
                $info['kana'] = $request->kana;
                

                $arrGender = lang::get('entry.gender');

                $info['gender_value'] = $request->gender;
                $info['gender'] = $arrGender[$request->gender];

                $info['email'] = $request->email;
                $info['tel'] = $request->tel;

                $info['job_objective'] = $request->job_objective;
                $info['hope_employment'] = $request->hope_employment;

                $info['job_objective'] = isset($info['job_objective']) ? array_unique($info['job_objective']) : [];
                
                $info['hope_employment'] = isset($info['hope_employment']) ? array_unique($info['hope_employment']) : [];
                
                $info['job_objective_value'] = $info['job_objective'];
                $info['hope_employment_value'] = $info['hope_employment'];
                
                //handle job objective
                $info['job_objective'] = implode('、', $info['job_objective']);

                //handle hope employment
                $info['hope_employment'] = implode('、', $info['hope_employment']);
                
                //pr
                $info['pr'] = $request->pr;
                
                //contact
                $info['contact'] = $request->contact;
                //capabilities
                $info['capabilities'] = $request->capabilities;
                
                //checked
                $info['situation'] = $request->situation;
                $info['join'] = $request->join;
                
                $info['situation_value'] = Util::getLangByIndex('entry.current_situation', $info['situation']);
                $info['join_value'] = Util::getLangByIndex('entry.available_join', $info['join']);
                
                
                
                Session::put('entry', $info);
				
				if($this->is_mobile)
				{
					return view('entry.confirm_sp');
				}
				
                return view('entry.confirm');
                
            }
            else
            {
                return redirect('/');
            }
        }
    }

    public function thanks(Router $router, Request $request)
    {
        if ($request->isMethod('post')) 
        {
            try
            {
                if (Session::has('entry'))
                {
                    $entryInfo = Session::get('entry');
                    Session::forget('entry');
                    $subjectUser = env('ENTRY_MAIL_TO_USER_SUBJECT');
                    $subjectAdmin = env('ENTRY_MAIL_TO_ADMIN_SUBJECT');
                    $subjectCompany = env('ENTRY_MAIL_TO_COMPANY_SUBJECT');
					
					
					
                    $templateMailToUser = env('ENTRY_MAIL_TO_USER_TEMPLATE');
                    $templateMailToCompany = env('ENTRY_MAIL_TO_COMPANY_TEMPLATE');
                    $templateMailToAdmin = env('ENTRY_MAIL_TO_ADMIN_TEMPLATE');
                    $fromName = env('FROM_NAME');
                    $fromAddress = env('FROM_ADDRESS');
					
                    $adminName = env('ADMIN_NAME');
                    $adminAddress = env('ADMIN_ADDRESS');
					
                    $debugMode = env('SENDMAIL_TEST_MODE');
                    $stringBCC = env('TEST_EMAIL_BCC');
					
                    $arrMailBCC = [];
                    if(isset($stringBCC) && $debugMode == true)
                    {
                        $arrMailBCC = explode(',', $stringBCC);
                    }

                    $result_user = Mail::queue(['text' => $templateMailToUser], ['entryInfo' => $entryInfo], function($message) use($entryInfo, $subjectUser, $fromName, $fromAddress, $arrMailBCC)
                    {
                        $message->from($fromAddress, $fromName);
                        $message->to($entryInfo['email'], $entryInfo['name'])
                                ->bcc($arrMailBCC)
                                ->subject($subjectUser);
                    });
                    
                    $result_company = Mail::queue(['text' => $templateMailToCompany], ['entryInfo' => $entryInfo], function($message) use($entryInfo, $subjectCompany, $fromName, $fromAddress, $arrMailBCC)
                    {
                        $message->from($fromAddress, $fromName);
                        $message->to($entryInfo['company_email'], $entryInfo['company_name'])
                                ->bcc($arrMailBCC)
                                ->subject($subjectCompany);
                    });

                    Mail::queue(['text' => $templateMailToAdmin], ['entryInfo' => $entryInfo], function($message) use($entryInfo, $subjectAdmin, $fromName, $fromAddress, $adminAddress, $adminName, $arrMailBCC)
                    {
                        $message->from($fromAddress, $fromName);
                        $message->to($adminAddress, $adminName)
                                ->bcc($arrMailBCC)
                                ->subject($subjectAdmin);
                    });

                    // set birthday
                    $birthday = $entryInfo['year']. '-' . $entryInfo['month'] . '-' .$entryInfo['day'];
                    $birthday = date("Y-m-d", strtotime($birthday));

                    $entry = new Entry();

                    $entry->insert([
                            'secret_job_id'     => $entryInfo['id'],
                            'name'              => $entryInfo['name'],
                            'kana'              => $entryInfo['kana'],
                            'gender'            => $entryInfo['gender'],
                            'birthday'          => $birthday,
                            'email'             => $entryInfo['email'],
                            'province'          => $entryInfo['province'],
                            'city'              => $entryInfo['city'],
                            'tel'               => $entryInfo['tel'],
                            'tel'               => $entryInfo['tel'],
                            'job_objective'     => $entryInfo['job_objective'],
                            'hope_employment'   => $entryInfo['hope_employment'],
                            'company_email'     => $entryInfo['company_email'],
                            'situation'     	=> $entryInfo['situation_value'],
                            'join'     			=> $entryInfo['join_value'],
                            'pr'     			=> $entryInfo['pr'],
                            'contact'     		=> $entryInfo['contact'],
                            'capabilities'      => $entryInfo['capabilities'],
                            'result_company'    => $result_company,
                            'result_user'       => $result_user
                        ]);

                    
					if($this->is_mobile)
					{
						return view('entry.thanks_sp');
					}
				
                    return view('entry.thanks');
                }
            }
            catch(\Exception $e){
                Session::flash('flash_message', lang::get('message.contact_sendmail_exception_error'));
                return redirect($entryInfo['entryURL']);
            }
        }

        return redirect('/');
    }
}
