<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Illuminate\Pagination\Paginator;

//Models
use App\Contents;
use App\Keywords;
use App\JobCategory;
use App\Area;

use App\Http\Components\MixSearchComponent;

class JobFilterController extends Controller
{
	protected $pc_paginate = 20;
	protected $sp_paginate = 10;

	protected $pc_secret_paginate = 4;
    protected $sp_secret_paginate = 2;
	
	protected $custom_web_title = '';
	protected $custom_breadcrumb = array('page' => '', 'parent' => array());
	protected $description = '';
	protected $keywords = '';
	protected $parent_id = 0;
	protected $featureName = '';
	
	protected $filter = array();
	protected $request = array();
	protected $title = array();
	protected $alias = array();
    protected $cat1 = 'biyo'; //planning
    protected $cat2 = 'relaxation'; //chouri
    protected $cat3 = 'chiryo'; //service
    protected $catHangParentId = array(50,51,52);
	
	//feature: 正社員,契約社員,業務委託,..
	public function feature(Router $router, Request $request)
	{
		if($this->is_mobile)
		{
			$size = 10;
            $view_tail = '_sp';
		}
		else
		{
			$size = 20;
            $view_tail = '';
		}
		$alias = $request->route('feature');
		$jobCate = new JobCategory();
		$cateRecore = $jobCate->getByAlias($alias, array(2,3));
		if($cateRecore)
		{
			$cateId = $cateRecore['id'];
			$description = '';
			$keywords = '';
			$parKey = 'hang';
			$filterField = '';
			
			if($cateRecore['style'] == 3)
			{
				$parKey = 'em';
				$description = trans('webinfo.meta_desc_job_result_em');
				$keywords= trans('webinfo.keywords_job_result_em');
				$filterField = 'list_em';
			}
			else
			{
				$description = trans('webinfo.meta_desc_job_result_hang');
				$keywords= trans('webinfo.keywords_job_result_hang');
				$filterField = in_array($cateId, $this->catHangParentId) ? 'list_job_parent' : 'list_hangup';
				
			}

			$page = $request->page;
	        if (!$page) {
	            $page = 1;
	        }
	        $mixSearch = new MixSearchComponent();
	        $searchResult = $mixSearch->getMixFilter(array($filterField => $cateId), $page, $size);
	        $secrets = $searchResult['data_secret'];
	        $contents = $searchResult['data_job'];
	        $alliances = $searchResult['data_alliance'];
			$total = $searchResult['total_data'];

			$description = str_replace('○○', $cateRecore['name'], $description); 
			$keywords = str_replace('○○', $cateRecore['name'], $keywords);

            $catChildId = in_array($cateId, $this->catHangParentId) ? $this->catChildToArr($cateId) : null;
			$request = in_array($cateId, $this->catHangParentId) ?  array($parKey => $catChildId ) : array($parKey => $cateId);
			$custom_web_title = $cateRecore['name'] == '派遣'? $cateRecore['name'].'社員の求人' : $cateRecore['name'].'の求人';
			$custom_breadcrumb = array('page' => $cateRecore['name'] == '派遣'? $cateRecore['name'].'社員の求人'  : $cateRecore['name'].'の求人');

			return view("job.index".$view_tail)->with([
					"style" => "filter",
					"secrets" => $secrets,
					"contents" => $contents,
					"alliances" => $alliances,
					"total" => $total,
					"request" => $request,
					"custom_web_title" => $custom_web_title,
					"custom_breadcrumb" => $custom_breadcrumb,
					"description" => $description,
					"keywords"	=> $keywords,
					"search_result" => $searchResult,
            		"is_custom_paginate" => true
    		]);
		}
		return redirect('/');
	}
	
	//for all category level 1
	public function planning(Request $request)
	{
		return $this->categoryLevel1($this->cat1, $request);
	}
	public function chouri(Request $request)
	{
		return $this->categoryLevel1($this->cat2, $request);
	}
	public function service(Request $request)
	{
		return $this->categoryLevel1($this->cat3, $request);
	}
	
	//Route::get('{cat_name}', 'JobFilterController@categoryLv2');
	public function categoryLv2(Router $router, Request $request)
	{
		$aliasFeature = $request->route('feature');
		if(!empty($aliasFeature) && !$this->filterFeature($aliasFeature))
		{
			return redirect('/');
		}
		
		$aliasCatName = $request->route('cat_name');
		if(!$this->filterCategoryLv2($aliasCatName))
		{
			return redirect('/');
		}

		if(!$this->filterCategoryParent())
		{
			return redirect('/');
		}
		
		$parent = array(
							array(
								'link' => $this->getFilterUrl('catlv1'),
								'title'=> $this->title['catlv1'].'の求人'
							),
						);
		if(!empty($this->featureName))
		{
			$parent[] = array(
				'link'  => $this->getFilterUrl('catlv2'),
				'title' => $this->getFilterNavTitle('catlv2'),
			);
		}
        //dd($this->getFilterNavTitle('catlv2', true));
		$this->custom_web_title = $this->title['catlv2'] == 'エステ' ? $this->title['catlv2'].'・エステティシャンの求人' : $this->title['catlv2'].'の求人';
		$this->custom_breadcrumb = array(
			'page'   => $this->getFilterNavTitle('catlv2', true),
			'parent' => $parent
		);
		
		$description = trans('webinfo.meta_desc_job_result_cat');
		$keywords= trans('webinfo.keywords_job_result_cat');
		$this->description = str_replace('○○', $this->title['catlv2'], $description); 
		$this->keywords = str_replace('○○', $this->title['catlv2'], $keywords); 

		return $this->filterProcess($request);
	}
	
	//Route::get('{cat_name}/area/{province}', 'JobFilterController@province');
	public function province(Router $router, Request $request)
	{
		$aliasFeature = $request->route('feature');
		if(!empty($aliasFeature) && !$this->filterFeature($aliasFeature))
		{
			return redirect('/');
		}
		
		if(!$this->filterCategoryLv2($request->route('cat_name')))
		{
			return redirect('/');	
		}
		
		if(!$this->filterCategoryParent())
		{
			return redirect('/');
		}
		
		if(!$this->filterProvince($request->route('province')))
		{
			return redirect('/');	
		}
		
		
		$this->custom_web_title =  $this->title['catlv2'] == 'エステ' ? $this->title['catlv2'].'・エステティシャン'.'・'.$this->title['pro'].'の求人' : $this->title['catlv2'].'・'.$this->title['pro'].'の求人';
		
		$parent = array(
								array(
									'link' => $this->getFilterUrl('catlv1'),
									'title'=> $this->title['catlv1'].'の求人'
								),
								array(
									'link' => $this->getFilterUrl('catlv2'),
									'title'=> $this->title['catlv2'] == 'エステ' ? $this->title['catlv2'].'・エステティシャンの求人' : $this->title['catlv2'].'の求人'
								)
							 );
		
		if(!empty($this->featureName))
		{
			$parent[] = array(
				'link'  => $this->getFilterUrl('pro'),
				'title' => $this->getFilterNavTitle('pro'),
			);
		}
		
		$this->custom_breadcrumb = array(
			'page'   => $this->getFilterNavTitle('pro', true),
			'parent' => $parent
		);
		
		$description = trans('webinfo.meta_desc_job_result_cat');
		$keywords= trans('webinfo.keywords_job_result_cat');
		$this->description = str_replace('○○', $this->title['catlv2'], $description); 
		$this->keywords = str_replace('○○', $this->title['catlv2'], $keywords); 

		return $this->filterProcess($request);
	}
	
	//Route::get('{cat_name}/area/{province}/city/{city}', 'JobFilterController@city');
	public function city(Router $router, Request $request)
	{
		
		$aliasFeature = $request->route('feature');
		if(!empty($aliasFeature) && !$this->filterFeature($aliasFeature))
		{
			return redirect('/');
		}
		
		if(!$this->filterCategoryLv2($request->route('cat_name')))
		{
			return redirect('/');	
		}
		
		if(!$this->filterCategoryParent())
		{
			return redirect('/');
		}

		if(!$this->filterProvince($request->route('province')))
		{
			return redirect('/');	
		}
		
		if(!$this->filterCity($request->route('city')))
		{
			return redirect('/');	
		}

        $this->custom_web_title =  $this->title['catlv2'] == 'エステ' ? $this->title['catlv2'].'・エステティシャン'.'・' .$this->title['pro'] .'の求人・' .$this->title['city'] : $this->title['catlv2'] .'・' .$this->title['pro'] .'の求人・' .$this->title['city'];

		
		$parent = array(
								array(
									'link' => $this->getFilterUrl('catlv1'),
									'title'=> $this->getFilterNavTitle('catlv1')
								),
								array(
									'link' => $this->getFilterUrl('catlv2'),
									'title'=> $this->getFilterNavTitle('catlv2')
								),
								array(
									'link' => $this->getFilterUrl('pro'),
									'title'=> $this->getFilterNavTitle('pro')
								)
						);
		if(!empty($this->featureName))
		{
			$parent[] = array(
				'link'  => $this->getFilterUrl('city'),
				'title' => $this->getFilterNavTitle('city'),
			);
		}
		
		$this->custom_breadcrumb = array(
			'page'   => $this->getFilterNavTitle('city', true),
			'parent' => $parent
		);
		
		$description = trans('webinfo.meta_desc_job_result_cat');
		$keywords= trans('webinfo.keywords_job_result_cat');
		$this->description = str_replace('○○', $this->title['catlv2'], $description); 
		$this->keywords = str_replace('○○', $this->title['catlv2'], $keywords); 

		return $this->filterProcess($request);
	}
	
	
	private function categoryLevel1($cateName, $request)
	{
		if($this->is_mobile)
		{
			$view_tail = '_sp';
            $size = 10;
		}
		else
		{
			$view_tail = '';
			$size = 20;
		}
		$jobCate = new JobCategory();
		$cateRecore = $jobCate->getByAlias($cateName);
		if($cateRecore)
		{
			$cateId = $cateRecore['id'];


			$page = $request->page;
	        if (!$page) {
	            $page = 1;
	        }
	        $mixSearch = new MixSearchComponent();
	        $searchResult = $mixSearch->getMixFilter(array('list_job_parent' => $cateId), $page, $size);
	        $secrets = $searchResult['data_secret'];
	        $contents = $searchResult['data_job'];
	        $alliances = $searchResult['data_alliance'];
			$total = $searchResult['total_data'];
			
			$request = array('cat' =>$this->catChildToArr($cateId));
			$custom_web_title = $cateRecore['name'].'の求人';
			$custom_breadcrumb = array('page' => $cateRecore['name'].'の求人');
			$description = trans('webinfo.meta_desc_job_result_cat');
			$keywords= trans('webinfo.keywords_job_result_cat');
			$description = str_replace('○○', $cateRecore['name'], $description); 
			$keywords = str_replace('○○', $cateRecore['name'], $keywords);

			return view("job.index".$view_tail)->with([
					"style" 			=> "filter",
					"contents" 			=> $contents,
                	"secrets" 			=> $secrets,
                	"alliances"			=> $alliances,
					"total" 			=> $total,
					"request" 			=> $request,
					"custom_web_title" 	=> $custom_web_title,
					"custom_breadcrumb" => $custom_breadcrumb,
					"description" 		=> $description,
					"keywords" 			=> $keywords,
					"search_result" 	=> $searchResult,
            		"is_custom_paginate" => true
    		]);
		}
		return redirect('/');
	}
	
	private function catChildToArr($parentId){
        $jobCate = new JobCategory();
        $request = array();
        $select = $jobCate->getJobCategoryByParent($parentId);
        foreach ($select as $item){
            $request[]= $item['id'];
        }
        return $request;

    }
	private function filterFeature($alias)
	{
		$jobCate = new JobCategory();
		$cateRecore = $jobCate->getByAlias($alias, array(3,2));
		if(!$cateRecore)
		{
			return false;
		}
		if($cateRecore['style'] == 3)
		{
			$this->filter['list_em'] = $cateRecore['id'];
			$this->request['em'] = $cateRecore['id'];
		}
		else
		{
			$this->filter['list_hangup'] = $cateRecore['id'];
			$this->request['hang'] = $cateRecore['id'];
		}	
		$this->title['feature'] = $cateRecore['name'];
		$this->alias['feature'] = $cateRecore['alias'];
		$this->featureName = '・'.$cateRecore['name'];
		
		return true;
	}
	
	private function filterCategoryLv2($alias)
	{
		$jobCate = new JobCategory();
		$cateRecore = $jobCate->getByAlias($alias, array(1));
		
		if(!$cateRecore)
		{
			return false;
		}
		
		$this->parent_id =  $cateRecore['parent_id'];
		$this->filter['list_job'] = $cateRecore['id']; 
		$this->request['cat']  =  $cateRecore['id'];
		$this->title['catlv2'] = $cateRecore['name'];
		$this->alias['catlv2'] = $cateRecore['alias'];
		
		return true;
	}

	
	private function filterCategoryParent()
	{
		$jobCate = new JobCategory();
		$cateRecore = $jobCate->getById($this->parent_id);
		
		if(!$cateRecore)
		{
			return false;
		}
		
		//$this->filter[] = array('list_job_parent' => $cateRecore['id']); 
		$this->title['catlv1'] = $cateRecore['name'];
		$this->alias['catlv1'] = $cateRecore['alias'];
		return true;
	}
	
	private function filterProvince($alias)
	{
		$area = new Area();
		$areaProvince = $area->getByAlias($alias);
		
		if(!$areaProvince)
		{
			return false;
		}
		
		$this->filter['list_province'] 	= $areaProvince['id']; 
		$this->request['pro']  		= $areaProvince['id'];
		$this->title['pro'] 		= $areaProvince['name'];
		$this->alias['pro'] 		= $areaProvince['alias'];
		
		return true;
	}
	
	private function filterCity($id)
	{
		$area = new Area();
		$areaCity = $area->getCityByIdAndParentId($id, $this->request['pro']);
		if(!$areaCity)
		{
			return false;
		}			
		
		$this->filter['list_city'] 	= $id; 
		$this->request['city']  	= $id;
		$this->title['city'] 		= $areaCity['name'];
		$this->alias['city'] 		= $areaCity['id'];
		return true;
	}
	
	private function filterProcess($request)
	{
		if($this->is_mobile)
		{
			$view_tail = '_sp';
			$paginate = $this->sp_paginate;
            $secretPaginate = $this->sp_secret_paginate;
            $size = 10;
		}
		else
		{
			$size = 20;
			$view_tail = '';
			$paginate = $this->pc_paginate;
            $secretPaginate = $this->pc_secret_paginate;
		}
		//$content = new Contents();
		//$result = $content->filter($this->filter ,20);
		//$total = $result->total();
		$page = $request->page;
        if (!$page) {
            $page = 1;
        }
        $mixSearch = new MixSearchComponent();
        $searchResult = $mixSearch->getMixFilter($this->filter, $page, $size);
        $secrets = $searchResult['data_secret'];
        $contents = $searchResult['data_job'];
        $alliances = $searchResult['data_alliance'];

        $total = $searchResult['total_data'];
		return view("job.index".$view_tail)->with([
				"style" => "filter",
				"secrets" => $secrets,
				"contents" => $contents,
				"alliances" => $alliances, 
				"total" => $total,
				"request" => $this->request,
				"custom_web_title" => $this->custom_web_title,
				"custom_breadcrumb" => $this->custom_breadcrumb,
				"description" => $this->description,
				"keywords"	=> $this->keywords,
				"search_result" => $searchResult,
            	"is_custom_paginate" => true
		]);
	}
	
	private function getFilterUrl($start)
	{
		//'/'.$this->alias['catlv1'].'/'.$this->alias['catlv2'].'/area/'.$this->alias['pro'].'/city/'.$this->alias['city']
		$url = '';
		switch($start)
		{
			case 'city':
				$url = '/city/'.$this->alias['city'];
			case 'pro':
				$url = '/area/'.$this->alias['pro'].$url;
			case 'catlv2':
				$url = '/'.$this->alias['catlv2'].$url;
				break;
			case 'catlv1':
				$url = '/'.$this->alias['catlv1'].$url;			
		}
		return $url;
	}
	
	private function getFilterNavTitle($start, $allFeatureName = false)
	{
		//$this->title['catlv1'].'の求人'
		//$this->title['catlv2'].'の求人'
		//$this->title['pro'].'の'.$this->title['catlv2'].'の求人'
		//$this->title['pro'].$this->title['city'].'の'.$this->title['catlv2'].'の求人',
		
		//$this->title['pro'].$this->title['city'].$this->featureName.'の'.$this->title['catlv2'].'の求人',
		//$this->title['pro'].$this->featureName.'の'.$this->title['catlv2'].'の求人',
		//$this->title['catlv2'].$this->featureName.'の求人',
		$title = '';
		switch($start)
		{
			case 'city':
				$title = $this->title['city'];
			case 'pro':
				$title = $this->title['pro'].$title;
				if($allFeatureName)
				{
					$title = $title.$this->featureName;
				}
                $title =  $this->title['catlv2'] == 'エステ' ? $title.'の'.$this->title['catlv2'].'・エステティシャンの求人' : $title.'の'.$this->title['catlv2'].'の求人';
				break;
			case 'catlv2':
				$title =  $this->title['catlv2'] == 'エステ' ? $this->title['catlv2'].'・エステティシャン' : $this->title['catlv2'];
				if($allFeatureName)
				{
					$title = $title.$this->featureName;
				}
				$title = $title.'の求人';
				break;
			case 'catlv1':
				$title = $this->title['catlv1'].'の求人';
				break;
		}
		return $title;
	}
}
