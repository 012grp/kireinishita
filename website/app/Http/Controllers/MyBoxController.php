<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Paginator;
use Illuminate\Support\Facades\Input;
use DB;
//component
use App\Http\Components\UserComponent;



class MyBoxController extends Controller
{
    /**
     * Instantiate a new MyBoxController instance.
     */
    public function __construct()
    {
        parent::__construct();
        
        //Check authencation of actions in this user
        $this->beforefilter(function($e)
        {
            //TODO: has session yet ?
            if(!UserComponent::is_login())
            {
				if($this->is_mobile)
				{
					return redirect('/');
				}
				else
				{
                	return redirect('/account/');  
				}
            }
        });
    }


    /*
        TODO: 
    */
	public function index(Router $router, Request $request)
    {
        $userId = UserComponent::get_info()['id'];
        
        $totalBookmark = DB::select("select count(*) as total from    
        (
            select 0 as is_secret, cont.id, bm.created_at, bm.userId
            from contents cont
            join (select * from bookmarks where contentId > 0 and status=1 and userId = $userId) 
			bm on cont.id = bm.contentId
            where cont.`status` = 1
			
            union 

            select 1 as is_secret, cont.id, bm.created_at, bm.userId
            from secret_job cont 
            join (select * from bookmarks where secretId > 0 and status=1 and userId = $userId) 
			bm on cont.id = bm.secretId
            where cont.`status` = 1
        ) un
        order by un.created_at");

        $totalBookmark = $totalBookmark[0]->total;

        $jobs = DB::select("select * from    
        (
            select 0 as is_secret, 
                    bm.created_at, 
                    bm.userId,
                    cont.id,
                    cont.job_name,
                    cont.job_salary, 
                    cont.job_location, 
                    cont.list_em, 
                    cont.list_hangup, 
                    cont.list_job, 
                    cont.list_job_parent,
                    cont.job_career,
                    '' as store_name,
                    cont.job_type,
                    cont.job_url,
                    cont.job_image,
                    cont.job_company,
					cont.requirement_special,
                    cont.site_id,
                    cont.is_new
            from contents cont 
            join (select * from bookmarks where contentId > 0 and status=1 and userId = $userId) bm 
                on cont.id = bm.contentId
            where cont.`status` = 1

            union 

            select 1 as is_secret, 
                    bm.created_at, 
                    bm.userId,
                    cont.id,
                    cont.job_name,
                    cont.job_salary, 
                    cont.job_location, 
					cont.list_em, 
					cont.list_hangup, 
					cont.list_job, 
                    cont.list_job_parent,
					cont.job_career,
					cont.job_type,
					cont.store_name,
                    '' as job_url,
                    cont.job_image,
                    com.name as job_company,
                    '' as requirement_special,
                    cont.site_id,
                    cont.is_new
            from secret_job cont 
            join (select * from bookmarks where secretId > 0 and status=1 and userId = $userId) bm 
                on cont.id = bm.secretId
            join company com 
                on cont.company_id = com.id
            where cont.`status` = 1
        ) un
        
        
        order by un.created_at");

        //dd($jobs);

  //       $paging = Paginator::make($jobs, 22, 5);
  //       dd($paging);

  //       $paginate = 5;
  //       $page = \Input::get('page', 1);
  //       $offSet = ($page * $paginate) - $paginate;  
  //       $itemsForCurrentPage = array_slice($jobs, $offSet, $paginate, true);  
  //       $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($jobs), $paginate, $page);
  //       $result = $result->toArray();
  //       dd($result);
        if($this->is_mobile)
            return view('mybox.index_sp')->with(['jobs' => $jobs, 'totalBookmark' => $totalBookmark]);
        else
            return view('mybox.index')->with(['jobs' => $jobs, 'totalBookmark' => $totalBookmark]);

	}

    
}
