<?php

namespace App\Http\Controllers;


use View;
use Route;
use Response;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;

//model
use App\Contents;
use App\Keywords;

use Session;

class PagesController extends Controller
{
	public function top()
	{
		//TODO: top
		if($this->is_mobile)
			return view('pages.top_sp');
		else
			return view('pages.top');
	}
	
    public function aboutme(){
		return view("pages.aboutme");
	}
	public function contact()
	{
		return view("pages.contact");
	}
    
	public function search(Request $request) {        
		$action = $request->input('action');
		$value = $request->input('value');
        $value = urldecode($value);
		$website = intval($request->input('website'));
		$fromDate = $request->input('from_date');

        $contents = Contents::where('status', '=', 1)->orderBy('priority', 'desc');
		
		if( !empty($value) )
		{
			switch($action)
			{
				case "ID":  
					$contents = $contents->where('id','=', $value);
				break;
				case "Name":  
					$contents = $contents->where('job_name','LIKE', '%'.$value.'%');
				break;
				case "Detail":  
					$contents = $contents->where('job_detail','LIKE', '%'.$value.'%');
				break;
				case "Company":  
					$contents = $contents->where('job_company','LIKE', '%'.$value.'%');
				break;
				case "Career":
					$contents = $contents->where('job_career','LIKE', '%'.$value.'%');
				break;
				case "Salary": 
					$contents = $contents->where('job_salary','LIKE', '%'.$value.'%');
				break;
				case "Expire":  
					$contents = $contents->where('job_expire','LIKE', '%'.$value.'%');
				break;
				case "Location":  
					$contents = $contents->where('job_location','LIKE', '%'.$value.'%');
				break;
				case "Type":  
					$contents = $contents->where('job_type','LIKE', '%'.$value.'%');
				break;
					
//				case "Image":  
//					$contents = $contents->where('job_image','LIKE', '%'.$value.'%');
//				break;	

				//Any
				default:
					$contents = $contents->where(function ($query) use($value){
							$query->orWhere('contents.id', '=', $value);
							$value = '%'.$value.'%';
							$query->orWhere('contents.job_name', 'like', $value);
							$query->orWhere('contents.job_detail', 'like', $value);
							$query->orWhere('contents.job_company', 'like', $value);
							$query->orWhere('contents.job_career', 'like', $value);
							$query->orWhere('contents.job_salary', 'like', $value);
							$query->orWhere('contents.job_expire', 'like', $value);
							$query->orWhere('contents.job_location', 'like', $value);
							$query->orWhere('contents.job_type', 'like', $value);
					});
				break;
			}
		}
		
		if( $website > 0 )
		{
			$contents = $contents->where('site_id', '=', $website);
		}
		
		if(!empty($fromDate))
		{
			$created = date('Y-m-d H:i:s', strtotime($fromDate));
			$contents = $contents->where('created', '>=', $created);
		}
		
		$contents = $contents->paginate(21)->appends($request->all());
        $view = 'search_demo.index';
        return view($view,[
			'title' 		=> 'Jobs',
			'pagename' 		=> 'Jobs',
			
            'contents' 	=> $contents,
			'action'	=> $action,		
			'value'		=> $value,				
			'website'	=> $website,		
			'fromDate'	=> $fromDate,		
        ]);
    }//End function
	
    public function building()
    {
		return view("pages.building");
	}
    public function sitemap()
    {

        $content = View::make('pages.sitemap');
		return Response::make($content, '200')->header('Content-Type', 'text/xml');
	}
	
	
	
	public function contents()
	{
		//TODO: contents
		return view('contents.index');
	}
	
	public function trend()
	{
		//TODO: contents
		return view('contents.trend');
	}
	
	
	public function searchDemo(Request $request) {        
		$action = $request->input('action');
		$value = $request->input('value');
        $value = urldecode($value);
		$website = intval($request->input('website'));
		$fromDate = $request->input('from_date');

        $contents = Contents::where('status', '=', 1)->orderBy('priority', 'desc')->limit(1000);
		
		if( !empty($value) )
		{
			switch($action)
			{
				case "ID":  
					$contents = $contents->where('id','=', $value);
				break;
				case "Name":  
					$contents = $contents->where('job_name','LIKE', '%'.$value.'%');
				break;
				case "Detail":  
					$contents = $contents->where('job_detail','LIKE', '%'.$value.'%');
				break;
				case "Company":  
					$contents = $contents->where('job_company','LIKE', '%'.$value.'%');
				break;
				case "Career":
					$contents = $contents->where('job_career','LIKE', '%'.$value.'%');
				break;
				case "Salary": 
					$contents = $contents->where('job_salary','LIKE', '%'.$value.'%');
				break;
				case "Expire":  
					$contents = $contents->where('job_expire','LIKE', '%'.$value.'%');
				break;
				case "Location":  
					$contents = $contents->where('job_location','LIKE', '%'.$value.'%');
				break;
				case "Type":  
					$contents = $contents->where('job_type','LIKE', '%'.$value.'%');
				break;
					
//				case "Image":  
//					$contents = $contents->where('job_image','LIKE', '%'.$value.'%');
//				break;	

				//Any
				default:
					$contents = $contents->where(function ($query) use($value){
							$query->orWhere('contents.id', '=', $value);
							$value = '%'.$value.'%';
							$query->orWhere('contents.job_name', 'like', $value);
							$query->orWhere('contents.job_detail', 'like', $value);
							$query->orWhere('contents.job_company', 'like', $value);
							$query->orWhere('contents.job_career', 'like', $value);
							$query->orWhere('contents.job_salary', 'like', $value);
							$query->orWhere('contents.job_expire', 'like', $value);
							$query->orWhere('contents.job_location', 'like', $value);
							$query->orWhere('contents.job_type', 'like', $value);
							$query->orWhere('contents.requirement_special', 'like', $value);
					});
				break;
			}
		}
		
		if( $website > 0 )
		{
			$contents = $contents->where('site_id', '=', $website);
		}
		
		if(!empty($fromDate))
		{
			$created = date('Y-m-d H:i:s', strtotime($fromDate));
			$contents = $contents->where('created', '>=', $created);
		}
		
		$contents = $contents->paginate(21)->appends($request->all());
        $view = 'admin.pages.top';
        return view($view,[
			'title' 		=> 'Jobs',
			'pagename' 		=> 'Jobs',
			
            'contents' 	=> $contents,
			'action'	=> $action,		
			'value'		=> $value,				
			'website'	=> $website,		
			'fromDate'	=> $fromDate,		
        ]);
    }//End function
	
}
