<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Illuminate\Pagination\Paginator;
use Log;
use Cookie;
use Lang;

//Models
use App\Contents;
use App\Keywords;
use App\JobCategory;
use App\Area;
use App\SecretJob;
use App\Alliance;

//Component
use App\Http\Components\Util;
use App\Http\Components\Search;
use App\Http\Components\MixSearchComponent;

class SearchController extends Controller
{
    protected $pc_paginate = 20;
    protected $sp_paginate = 10;

    protected $pc_secret_paginate = 4;
    protected $sp_secret_paginate = 2;

    protected $_keyword = '';
    protected $_jobs = '';
    protected $_perpage = '';
    protected $_queryExcuteTime = '';

    public function search(Router $router, Request $request)
    {
        //return to VIEW
        Cookie::queue('history_search', $request->all());
        if ($this->is_mobile) {
            return view('job.search_sp', array('request' => $request->all()));
        } else {
            return view("job.index")->with([
                "style" => "search",
                "contents" => array(),
                "total" => 0,
                "request" => array(),
                "description" => trans('webinfo.meta_desc_search'),
            ]);
        }
    }

    public function api(Request $request)
    {
        $contents = $this->getContent($request, 50);
        $total = $contents->total();

        return view('test.index', array(
            'contents' => $contents,
            'total' => $total
        ));
    }

    public function result(Router $router, Request $request)
    {
        $util = new Util();
        //$searchCom = new Search();
        $start = $util->microtime();

        //Redirect to single link for pars
        if (true && $ref = $this->routerSeachSheet1($request)) {
            return redirect($ref);
        }
        $end = $util->microtime();
        echo '<!--1. ' . ($end - $start) . '-->';

        //#enable query log
        if ($request->input('querylog')) {
            \DB::enableQueryLog();
        }

        if ($this->is_mobile) {
            $size = 10;
            $paginate = $this->sp_paginate;
            $secretPaginate = $this->sp_secret_paginate;
            $view_tail = '_sp';
        } else {
            $size = 20;
            $paginate = $this->pc_paginate;
            $secretPaginate = $this->pc_secret_paginate;
            $view_tail = '';
        }
        $start = $util->microtime();

        $page = $request->page;
        if (!$page) {
            $page = 1;
        }
        $mixSearch = new MixSearchComponent();
        $searchResult = $mixSearch->getMixSearch($request, $page, $size);
        //dd($searchResult);
        $secrets = $searchResult['data_secret'];
        $contents = $searchResult['data_job'];
        $alliances = $searchResult['data_alliance'];
        $total = $searchResult['total_data'];

        //$contents = $this->getContent($request, $paginate);

        //$total = $contents->total();
        //$lastPage = $contents->lastPage();

        $keyword = $request->input('k');
        $this->keyword($keyword, $total);

        $this->_jobs = $total;
        $this->_perpage = $paginate;
        $this->_keyword = $keyword;
        $this->writeLogExcuteQuery();
        $end = $util->microtime();
        echo '<!--2. ' . ($end - $start) . '-->';

        //#show query as SQL
        if ($request->input('querylog')) {
            $queries = \DB::getQueryLog();
            $formattedQueries = [];
            foreach ($queries as $query) :
                $prep = $query['query'];
                foreach ($query['bindings'] as $binding) :
                    $binding = is_numeric($binding) ? $binding : "'$binding'";
                    $prep = preg_replace("#\?#", $binding, $prep, 1);
                endforeach;
                $formattedQueries[] = $prep;
            endforeach;
            print_r($formattedQueries);
            print_r($queries);
        }

        //return to VIEW
        Cookie::queue('history_search', $request->all());
        return view("job.index" . $view_tail)
            ->with([
                "style" => "result",
                "contents" => $contents,
                "secrets" => $secrets,
                "alliances" => $alliances,
                "total" => $total,
                "request" => $request->all(),
                "custom_web_title" => trans('webinfo.title_job_result'),
                "description" => trans('webinfo.meta_desc_result'),
                "keywords" => trans('webinfo.keywords_job_result'),
                "search_result" => $searchResult,
                "is_custom_paginate" => true
            ]);


    }

    /*
        @input: alias

        @output: search result and its request
    */
    public function result_advance(Router $router, Request $request)
    {
        /*Unknow alias*/
        $alias = $request->route('alias');
        $title = '';
        $description = '';
        $keywords = '';

        $titleLangDir = 'webinfo.title_job_result_';
        $descLangDir = 'webinfo.meta_desc_job_result_';
        $keywordsLangDir = 'webinfo.keywords_job_result_';
        $pageLangDir = '';

        //Find in employment
        //Find in category
        //Find in hang up
        $rs = JobCategory::where('name', $alias)->where('level', 1)->first();
        if ($rs) {
            //get style
            $style = $rs->style;
            $parsName = Util::getJobCategoryParName($style);
            $catId = $rs->id;
            $request->merge(array($parsName => $catId));
            $pageLangDir = $parsName;

            // $parent = JobCategory::where('id', $rs->parent_id)->where('style', 1)->first();
            // if($parent)
            // {
            // 	$alias = $parent->name;
            // }
        } //Find in area
        else {
            $rs = Area::where('name', $alias)->where('level', 0)->first();
            if ($rs) {
                $request->merge(array('pro' => $rs->id));

                $pageLangDir = 'pro';
            } //Free search keyword
            else {
                $request->merge(array('k' => $alias));
                $pageLangDir = 'k';
            }

        }
        Cookie::queue('history_search', $request->all());

        if ($this->is_mobile) {
            $paginate = $this->sp_paginate;
            $secretPaginate = $this->sp_secret_paginate;
            $view_tail = '_sp';
            $size = 10;
        } else {
            $paginate = $this->pc_paginate;
            $secretPaginate = $this->pc_secret_paginate;
            $view_tail = '';
            $size = 20;
        }

        $titleTem = Lang::get($titleLangDir . $pageLangDir);
        $descTem = Lang::get($descLangDir . $pageLangDir);
        $keywordsTem = Lang::get($keywordsLangDir . $pageLangDir);

        $title = str_replace('○○', $alias, $titleTem);
        $description = str_replace('○○', $alias, $descTem);
        $keywords = str_replace('○○', $alias, $keywordsTem);

        //$contents = $this->getContent($request, $paginate);
        //$total = $contents->total();
        $page = $request->page;
        if (!$page) {
            $page = 1;
        }
        $mixSearch = new MixSearchComponent();
        $searchResult = $mixSearch->getMixSearch($request, $page, $size);
        $secrets = $searchResult['data_secret'];
        $contents = $searchResult['data_job'];

        $total = $searchResult['total_data'];
        return view("job.index" . $view_tail)
            ->with([
                "style" => "result",
                "secrets" => $secrets,
                "contents" => $contents,
                "total" => $total,
                "request" => $request->all(),
                "custom_web_title" => $title,
                "description" => $description,
                "keywords" => $keywords,
                "custom_breadcrumb" => array('page' => $title),
                "search_result" => $searchResult,
                "is_custom_paginate" => true
            ]);
    }

    public function reference(Request $request)
    {
        $contentId = $request->route('job_id');
        $content = Contents::select(array('id', 'job_url', 'job_url_clicked'))
            ->where('id', $contentId)
            ->where('status', 1)
            ->first();
        if ($content != null) {
            $url = $content->job_url;
            if (!empty($url)) {
                try {
                    $content->job_url_clicked = $content->job_url_clicked + 1;
                    $content->save();
                    return redirect($url);
                } catch (Exception $e) {
                    return redirect($url);
                }

            }
        }
        return redirect('/');
    }


    public function ajaxCountSearch(Request $request)
    {
        //TODO: ajaxCountSearch
        $content = new Contents();
        $totalJob = $content->countOfSearch($request);
        $secret = new SecretJob();
        $totalSecret = $secret->countOfSearch($request);
        $alliance = new Alliance();
        $totalAlliance = $alliance->countOfSearch($request);

        $total = $totalJob + $totalSecret + $totalAlliance;
        return $total;
    }

    /* 
        Author: Xuong
        Last code: 2016-07-11
        Parameters: [array()]
        Description: get contents function
		
		@structure
		array(
			//Conditions
			
			//job category
			'cat' => [1,2,4,5,6,7,8,9,10],
			
			//work_location
			'province' => 1,
			'city' => 1,
			'wayside' => 1,
			
			//employment
			'em' => [1,2,3,4,5],
			
			//carrer about
			'carrer' => [1,2,34,5,6],
			
			//keyword
			'k' => 'some keyword',
			
			//Page size
			'paginate' => 20
			
			//Current page
			'page' => 1
		)
		
		@return
    */
    private function getContent($request, $paginate)
    {

        //TODO:getContent
        /*
            Start of watching query process time    
        */
        $start = Util::microtime();

        $content = new Contents();
        return $content->search($request, $paginate);

        /*
            End of watching query process time
        */
        $end = Util::microtime();
        $this->_queryExcuteTime = $end - $start;
        $this->_keyword = $keyword;

        return $contents;
    }//End function

    /* 
        Author: Xuong
        Last code: 2015-09-23
        Parameters: [String $keyword]
        Description: save all keywords and raise up if exists
    */
    private function keyword($keyword, $total = 0)
    {
        //TODO:
        if (!empty($keyword)) {
            $kw = Keywords::where('keyword', '=', $keyword)->first();
            if ($kw == null) {
                $kw = new Keywords();
                $kw->keyword = $keyword;

            } else {
                $kw->count = $kw->count + 1;
            }
            $kw->result = $total;
            $kw->save();
        }
    }//End function

    /**
     * This function will write log about search
     */
    private function writeLogExcuteQuery($name = 'MAIN')
    {

        Log::info('(' . $name . ') Keyword: ' . $this->_keyword . ' | TotalJobs: ' . $this->_jobs . ' | PerPage: ' . $this->_perpage . ' | QueryTime: ' . $this->_queryExcuteTime . 'ms');
    }

    /**
     * Control paramaters to redirect good link SEO of SHEET 01
     * @param  Request $request
     */
    private function routerSeachForCategory($request)
    {

        $cat = $request->input('cat');
        $pro = $request->input('pro');
        $city = $request->input('city');
        $em = $request->input('em');
        $hang = $request->input('hang');
        $k = $request->input('k');
        $page = $request->input('page');
        $waysite = $request->input('way');
        $station = $request->input('station');
        //invalid to redirect
        //#when 雇用形態 and こだわり checked

        if (!empty($em) && !empty($hang)) {

            return false;
        }


        if (empty($cat) || count($cat) > 1
            ||
            count($pro) > 1
            ||
            count($city) > 1
            ||
            count($em) > 1
            ||
            count($hang) > 1
            ||
            !empty($k)
            ||
            !empty($waysite)
            ||
            !empty($station)
        ) {
            return false;
        }

        $link = '';

        //end
        $fncPage = function () use ($page, &$link) {
            if (empty($page)) {
                return $link;
            }
            return $link . '?page=' . $page;
        };

        $fncEm = function () use ($em, $hang, &$link, $fncPage) {
            if (empty($em) && empty($hang)) {
                return $fncPage();
            }

            $rs = null;
            if (empty($hang)) {
                $rs = JobCategory::where('id', $em)
                    ->where('level', 1)
                    ->where('style', 3)
                    ->first();
            } else {
                $rs = JobCategory::where('id', $hang)
                    ->where('level', 1)
                    ->where('style', 2)
                    ->first();
            }

            //get alias
            if ($rs) {
                $link = $link . '/feature/' . $rs->alias;
                return $fncPage();
            }
            return false;
        };

        $fncCity = function () use ($city, &$link, $fncEm) {
            if (empty($city)) {
                return $fncEm();
            }
            //file exist in db
            $rs = Area::where('id', $city)->where('level', 1)->first();
            if ($rs) {
                $link = $link . '/city/' . $rs->id;
                return $fncEm();
            }
            return false;
        };

        $fncPro = function () use ($pro, &$link, $fncCity, $fncEm) {
            if (empty($pro)) {
                return $fncEm();
            }
            //get Pro alias
            $rs = Area::where('id', $pro)->where('level', 0)->first();
            if ($rs) {
                $link = $link . '/area/' . $rs->alias;
                return $fncCity();
            }
            return false;
        };

        $fnCatParent = function ($catParent) use (&$link, $fncPro) {
            $rs = JobCategory::where('id', $catParent)
                ->where('level', 0)
                ->first();
            if ($rs) {
                $link = '/' . $rs->alias . $link;
                return $fncPro();
            }
            return false;
        };

        //start
        $fncCat = function () use ($cat, &$link, $fncPro) {
            //get Cat alias
            $rs = JobCategory::where('id', $cat)
                ->where('style', 1)
                ->where('level', 1)
                ->first();
            if ($rs) {
                $link = '/' . $rs->alias;
                //return $fnCatParent($rs->parent_id);
                return $fncPro();
            }
            return false;
        };
        return $fncCat();
    }


    //work with only one
    private function routerSeachSheet1($request)
    {
        $all = $request->all();
        $page = $request->input('page');

        //remove all empty
        unset($all['page']);
        foreach ($all as $key => $val) {
            if (empty($val)) {
                unset($all[$key]);
            }

        }
        $count = count($all);
        switch ($count) {

            case 1:
                $alias = '';
                $key = '';
                $val = '';
                foreach ($all as $_key => $_val) {
                    $key = $_key;
                    $val = $_val;
                    break;
                }
                if (is_array($_val)) {
                    if (count($_val) == 1) {
                        $_val = $_val[0];
                    } else {
                        return false;
                    }
                }

                $rs = null;
                switch ($key) {
                    //get name in jobcategory tbl
                    case 'em':
                    case 'cat':
                    case 'hang':
                        $rs = JobCategory::where('id', $val)->where('level', 1)->first();
                        break;

                    //get name in area
                    case 'pro':
                        $rs = Area::where('id', $val)->where('level', 0)->first();
                        break;
                }

                if ($rs) {
                    switch ($key) {
                        case 'em':
                        case 'hang':
                            $alias = '/feature/' . $rs->alias;
                            break;
                        case 'cat':
                            $parent = JobCategory::where('id', $rs->parent_id)->first();

                            if ($parent) {
                                $alias = $rs->alias;
                            } else {
                                $alias = '/job/result/' . $rs->name;
                            }

                            break;


                        //get name in area
                        //case 'pro':
                        default:
                            $alias = '/job/result/' . $rs->name;
                            break;
                    }
                } else {
                    $alias = '/job/result/' . $val;
                }

                return $alias;

            default:
                return $this->routerSeachForCategory($request);
        }
    }

    /**
     * Control paramaters to redirect good link SEO
     *
     * @param Request $request
     */
    private function routerSeach($request)
    {
        $all = $request->all();

        //remove all empty
        foreach ($all as $key => $val) {
            if (empty($val)) {
                unset($all[$key]);
            }

        }

        if (count($all) == 1) {

            $alias = '';
            foreach ($all as $key => $val) {
                $rs = null;
                switch ($key) {
                    //get name in jobcategory tbl
                    case 'em':
                    case 'cat':
                    case 'hang':
                        $rs = JobCategory::where('id', $val)->first();
                        break;

                    //get name in area
                    case 'pro':
                        $rs = Area::where('id', $val)->where('level', 0)->first();
                        break;
                }

                if ($rs) {
                    $alias = $rs->name;
                } else {
                    $alias = $val;
                }
                break;
            }
            return '/job/result/' . $alias;
        } else {
            return false;
        }
    }
}
