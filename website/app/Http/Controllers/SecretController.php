<?php

namespace App\Http\Controllers;

//
use Illuminate\Http\Request;
use App\Http\Requests;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Router;
use Auth;
use Input;
use Session;
use DB;
//modal
use App\SecretJob;
use App\Company;
//Helper
use App\Http\Helpers\common;


//component
use App\Http\Components\Util;

class SecretController extends Controller{

	protected $pc_pagination = 5;
	protected $sp_pagination = 10;
    /*
        TODO: show secret job 
    */
    public function index(Router $router, Request $request)
    {
		if ($this->is_mobile)
		{
			$pagination = $this->sp_pagination;
			$view_tail = '_sp';
		}
		else
		{
			$pagination = $this->pc_pagination;
			$view_tail = '';
		}
		
        $secret_jobs = SecretJob::select(
            's.id',
            's.job_name',
            's.job_salary',
            's.job_location',
            's.job_image',
            's.job_image_sp',
            's.site_id',
            's.created_at',
            's.is_new',
            's.list_em',
            's.list_job',
            's.list_job_parent',
            's.list_hangup',
            's.store_name',
            'c.name as job_company',
            'c.celebration as celebration'
        )
            ->from('secret_job as s')
            ->leftJoin('company as c', 's.company_id', '=', 'c.id');
        $secret_jobs = SecretJob::invaliddate($secret_jobs);
        $secret_jobs= $secret_jobs->where('s.status', 1)
            ->orderBy('s.created_at', 'desc')
            ->paginate($pagination);

        $total = $secret_jobs->total();

		return view('secret_kyujin.index'.$view_tail)->with(['secret_jobs' => $secret_jobs, 'total' => $total]);

    }

    public function share(Router $router, Request $request, $id)
    {
        $expire = $request->input('e');
        $hash = $request->input('h');
        $checkValid = common::checkShareLink($id, $expire, $hash);
        if($checkValid)
        {
            //$checkStatus=false
            return $this->detail($router,$request, $id, false);
        }
        else
        {
            return view('errors.outofdate');
        }
    }

    /*
        TODO: show secret job detail
    */
    public function detail(Router $router, Request $request, $id, $checkStatus=true)
    {
        $secretJob = new SecretJob();
        $detail = $secretJob->getDetail($id, $checkStatus);

        if ($this->is_mobile){
          $slider_image_sp = DB::table('secret_job_img')
            ->select('image')
            ->where('secret_job_id', $id)
            ->where('type', 1)
            ->get();
          if(count($slider_image_sp) > 0){
            $slider_image = $slider_image_sp;
          }else{
            $slider_image = DB::table('secret_job_img')
              ->select('image')
              ->where('secret_job_id', $id)
              ->where('type', 0)
              ->get();
          }
        }else{
          $slider_image = DB::table('secret_job_img')
            ->select('image')
            ->where('secret_job_id', $id)
            ->where('type', 0)
            ->get();
        }


        if (!$detail) {
            $secretUrl = '/' . Util::getUrlName('secretPage');
            return redirect($secretUrl);
        }
        $detail = $detail[0];

        $status =  Company::find($detail['company_id'])['status'];

        if ($this->is_mobile)
            return view('secret_kyujin.detail_sp')->with(['detail' => $detail, 'slider_image' => $slider_image, 'status' => $status]);
        else
            return view('secret_kyujin.detail')->with(['detail' => $detail, 'slider_image' => $slider_image, 'status' => $status]);


    }
}
