<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
//component
use App\Http\Components\SiteMap;


class SiteMapController extends Controller
{
	public function createSiteMap(Request $request)
    {
        if($request->key == "hhxpU2RnXym98BBQW70k3D83Uxb03fs7")
        {
            $arrSiteMap = SiteMap::renew();

            $arrSiteMap = SiteMap::createStringSiteMap($arrSiteMap);
            
            $arrSiteMap = SiteMap::handleSiteMapRequest($arrSiteMap);
            
			if(SiteMap::create($arrSiteMap))
            {
				echo "Create site map success";
                Log::info("Create site map success");
            }
            else
            {
				echo "Create site map fail";
                Log::info("Create site map fail");
            }
        }
    } 
}

