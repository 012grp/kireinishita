<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
//component
use App\Http\Components\MixSearchComponent;


class TestController extends Controller
{
	public function MixSearch($xTotal, $yTotal, $zTotal, $xDisplay, $yDisplay, $zDisplay, $currentPage)
	{
		$mixsearch = new MixSearchComponent();
		$mixsearch->fibonacci($xTotal, $yTotal, $zTotal, $xDisplay, $yDisplay, $zDisplay, $currentPage);
	}
}