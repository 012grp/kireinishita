<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;

//model
use App\Area;
use App\WaySite;
use App\Station;

class TrainApiController extends Controller
{
	private $api = 'http://api.ekispert.jp/v1/';
	//test key
	//private	$key = 'LE_g8MAPgavA9pYF';
	//original key
	private	$key = 'LE_ecxNPZEdyDxR5';
	private	$type = 'json';
	
    public function getProvince()
    {
    	//TODO: getProvince
    }
	
	/**
	 * UPDATE STATION
	 * @building
	 */
	public function getAllStation()
	{
		//TODO: getProvince
		$api = $this->api;
		$key = $this->key;
		$type = $this->type;
		set_time_limit ( 2000 );
		$waysites = Waysite::select('line_cd', 'name')->groupBy('line_cd')->get();
		echo '<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />';
		foreach( $waysites as $waysite)
		{				
			echo $waysite['line_cd'].' '.$waysite['name'].'<br>------------------<br><br>';
			$waysiteId = $waysite['line_cd'];
			$url = $api.'/'.$type.'/station?key='.$key.'&lineCode='.$waysiteId;
			$source = file_get_contents($url);
			$jsonSource = json_decode($source);
	
			if(empty($jsonSource->ResultSet->Point))
			{
				echo 'Nopoints 1'.'<br>------------------<br><br>';
				continue;
			}
			$points = $jsonSource->ResultSet->Point;
			foreach( $points as $point)
			{
				if(empty($point->Station->code) || 
				   empty($point->Station->Name) ||
				   empty($point->Prefecture->code) 
				  )
				{
					echo 'Nopoint 2'.'<br>------------------<br><br>';
					continue;
				}
				$this->saveStation($point->Prefecture->code,$waysiteId,$point->Station);
			}
			
			
			echo '<br>------------------<br><br>';
		}
	}
	
	/**
	 * Update LINES
	 */
	public function getAllLine()
	{
		//TODO: getProvince
		$api = $this->api;
		$key = $this->key;
		$type = $this->type;
		set_time_limit ( 2000 );
		$provinces = Area::where('parent_id', 0)->get();
		echo '<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />';
		foreach( $provinces as $province)
		{	
			echo $province['id'].' '.$province['name'].'<br>------------------<br><br>';
			$provinceId = $province['id'];
			$url = $api.'/'.$type.'/line?key='.$key.'&prefectureCode='.$provinceId;
			$source = file_get_contents($url);
			$jsonSource = json_decode($source);
			
			if(!$jsonSource){
				echo "<div>Lost: $provinceId</div>";
				continue;
			}

			$lines = $jsonSource->ResultSet->Line;
			/*
				format
				array(
					parent_id: @id
					line_cd: @number
					name: @name
					keywords: @name
				)
			*/
			
			if(!empty($lines->Code))
			{				
				$this->saveWaysite($provinceId,$lines);
			}
			else
			foreach( $lines as $line)
			{
				if(empty($line->Code) || empty($line->Name))
				{
					echo 'Noline'.'<br>------------------<br><br>';
					continue;
				}
				
				$this->saveWaysite($provinceId,$line);
			}
			
		}
	}
	
	
	/**
	 * INSERT ALL KEYWORD AUTOMACALLY FOR WAYSITE TABLE
	 * @building
	 */
	public function getKeywordForAllLine()
	{
		//TODO: getProvince
		$api = $this->api;
		$key = $this->key;
		$type = $this->type;
		set_time_limit ( 2000 );
		$waysites = Waysite::select('line_cd', 'name', 'keywords')->groupBy('line_cd')->get();
		echo '<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />';
		foreach( $waysites as $waysite)
		{				
			echo $waysite['line_cd'].' '.$waysite['name'].'<br>------------------<br><br>';
			$provinceId = $waysite['line_cd'];
			$url = $api.'/'.$type.'/station?key='.$key.'&lineCode='.$provinceId;
			$source = file_get_contents($url);
			$jsonSource = json_decode($source);
			if(empty($jsonSource->ResultSet->Point))
			{
				echo 'Nopoints 1'.'<br>------------------<br><br>';
				continue;
			}
			$points = $jsonSource->ResultSet->Point;
			$keywords = '';
			foreach( $points as $point)
			{
				if(empty($point->Station->code))
				{
					echo 'Nopoint 2'.'<br>------------------<br><br>';
					continue;
				}
				$keywords .= ','.$point->Station->code.',|';
			}
			$keywords = rtrim($keywords, '|');
			
			$rs = WaySite::where('line_cd', $waysite['line_cd'])
					->update(['keywords' => $keywords]);
			
			echo $keywords;
			echo '<br>';
			echo $rs;
			echo '<br>------------------<br><br>';
		}
	}
	
	/*
		format
		array(
			array(
				'name' => @line_name
				'code' => @line_code
				'station' => array(
					array(
						'name' => @station_name,
						'code' => @station_code,
					),
					...
				)
			),
			...
		)
	*/
	public function getLineStation($provinceId=null)
	{
		header("Access-Control-Allow-Origin: *");
    	header('Access-Control-Allow-Credentials: true');
		
		if(!$provinceId)
		{
			return response()->json(array());
		}
		/*
			select  
			w.line_cd, w.name as w_name,
			s.code, s.name as s_name
			
			from waysite w
			join station s on w.line_cd = s.waysite_id

			where w.parent_id = 1

			order by w.line_cd, s.code
		*/
		$lineStation = WaySite::select(
									   'waysite.line_cd', 'waysite.name as line_name', 
									   'station.code as station_cd', 'station.name as station_name',
									   'station.area_id', 'waysite.parent_id'
									  )
					   ->leftJoin('station', 'waysite.line_cd','=','station.waysite_id')
					   ->where('waysite.parent_id', $provinceId)
					   ->where('station.area_id', $provinceId)
					   ->where('waysite.status','>',0)
					   ->orderBy('waysite.line_cd')
					   ->orderBy('station.code')
					   ->get();
		$arrLS = array();
		$line_code = 0;
		$index = -1;
		foreach( $lineStation as $record)
		{
			if($record->line_cd != $line_code)
			{
				$line_code = $record->line_cd;
				
				$arrLS[$line_code]['name'] = $record->line_name;
				$arrLS[$line_code]['code'] = $record->line_cd;
				$arrLS[$line_code]['stations'] = array();
			}
			$arrLS[$line_code]['stations'][] = array(
				'name' => $record->station_name,
				'code' 	=> $record->station_cd
			);
			
		}
		
		return response()->json($arrLS);
	}
	
	/**
	 * Api: /train-api/line/@number
	 * @return json
	 */
	public function getLine($provinceId=null)
	{
		if(!$provinceId)
		{
			return response()->json(array());
		}
		
		//TODO: getTrain
		$waySite = WaySite::select('line_cd as line_cd', 'name as line_name')
					->where('parent_id', $provinceId)->get();
		//return json_encode($waySite);
		return response()->json($waySite);
	}
	
	
	/**
	 * Api: /train-api/station/@number
	 * @return json
	 */
	public function getStation($lineId=null)
	{
		if(!$lineId)
		{
			return response()->json(array());
		}
		
		//TODO: getTrain
		$station = Station::select('code as code', 'name as name', 'yomi as yomi')
					->where('waysite_id', $lineId)->get();
		//return json_encode($waySite);
		return response()->json($station);
	}
	
	/**
	 * save data in station table
	 * @param int $lineId
	 * @param collection $station
	 */
	private function saveStation($areaId, $lineId, $station)
	{
		$stationId 	 = $station->code;
		$stationName = $station->Name;
		$stationYomi = $station->Yomi;
		
		$tbstation = Station::where('code', $stationId)
					->where('waysite_id', $lineId)
					->where('area_id', $areaId)
					->first();
		$action = '';
		$status = '';
		if($tbstation) //update
		{
			if($tbstation->status == 0 || $tbstation->status == 2)
			{
				return;
			}
			
			$action = 'Edited';
			try
			{
				$tbstation->name = $lineName;
				$tbstation->yomi = $stationYomi;
				$tbstation->area_id = $areaId;
				$tbstation->keywords = $lineName.'|'.$stationYomi;
				$tbstation->save();

				$status = 'OK';
			}
			catch(Exception $e)
			{
				$status = 'Failed';
			}
		}
		else //insert
		{
			$action = 'Inserted';
			try
			{
				$_station = new Station();
				$_station->insert([
					'code' => $stationId,
					'name' => $stationName,
					'yomi' => $stationYomi,
					'area_id' => $areaId,
					'keywords' => $stationName.'|'.$stationYomi,
					'waysite_id' => $lineId
				]);

				$status = 'OK';
			}
			catch(Exception $e)
			{
				$status = 'Failed';
			}

		}
		$station->lineId = $lineId;
		$station->area_id = $areaId;
		$station->action = $action;
		$station->status = $status;
		echo print_r($station).'<br>------------------<br><br>';
	}
	
	
	/**
	 * save data into waysite table
	 * @param int $provinceId 
	 * @param collection $line       
	 */
	private function saveWaysite($provinceId, $line)
	{
		$lineCd = $line->Code;
		$lineName = $line->Name;

		$waySite = WaySite::where('line_cd', $lineCd)
					->where('parent_id', $provinceId)
					->first();


		$action = '';
		$status = '';
		if($waySite) //update
		{
			$action = 'Edited';
			try
			{
				$waySite->name = $lineName;
				$waySite->save();

				$status = 'OK';
			}
			catch(Exception $e)
			{
				$status = 'Failed';
			}
		}
		else //insert
		{
			$action = 'Inserted';
			try
			{
				$waysite = new WaySite();
				$waysite->insert([
					'line_cd' => $lineCd,
					'name' => $lineName,
					'parent_id' => $provinceId
				]);

				$status = 'OK';
			}
			catch(Exception $e)
			{
				$status = 'Failed';
			}

		}
		$line->provinceId = $provinceId;
		$line->action = $action;
		$line->status = $status;
		echo print_r($line).'<br>------------------<br><br>';
	}
}
