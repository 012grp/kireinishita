<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contents;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;

class UrlsController extends Controller
{
    /* 
        Author: Xuong
        Last code: 2015-09-23
        Parameters: [int contentId]
        Description: activity
        @return: redirect to specify location 
    */
    public function index(Router $router) 
    {
        //TODO:
        $contentId = $router->input('url');
        $content = Contents::find($contentId);
        if($content != null)
        {
            $url = $content->job_url;
            if(!empty($url))
            {
                $content->job_url_clicked = $content->job_url_clicked +1;
                $content->save();
                return redirect($url);
            }
        }
        return redirect('/');
    }//End function
}
