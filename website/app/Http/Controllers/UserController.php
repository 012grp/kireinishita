<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
//model
use App\Bookmarks;
use App\User;

class UserController extends Controller
{
    
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        parent::__construct();
        
        //Check authencation of actions in this user
        $this->beforefilter(function($e){
            //TODO: has session yet ?
            if(!Session::has('user'))
            {
                return redirect('/login');   
            }
        
        //except login no check
        },  array('except' => 'login'));
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    
    /* 
        Author: Xuong
        Last code: 2015-09-15
        Parameters: []
        Description: activity
    */
    public function login() {
        //TODO:
        return view('login.index');
    }//End function
    
    
    /* 
        Author: Xuong
        Last code: 2015-09-15
        Parameters: []
        Description: activity
        @return template
    */
    public function me() {
        //TODO:
        return view('user.me');
    }//End function
    
   /* 
       Author: Xuong
       Last code: 2015-09-15
       Parameters: []
       Description: activity
   */
   public function mark(Request $request) {
       //TODO:
        $contentId =  $request->input('id');
        $userId = Session::get('user')['id'];
        $bookmarkId = 0;
       
        if(!Bookmarks::where(['contentId'=>$contentId, 'userId' => $userId])->exists())
        {
            $bookmark = new Bookmarks();
            $bookmark->contentId = $contentId;
            $bookmark->userId = $userId;
            $bookmark->save();
            $bookmarkId = $bookmark->id;
        }
       
        echo $bookmarkId;
        exit;
   }//End function
    
    /* 
        Author: Xuong
        Last code: 2015-09-15
        Parameters: []
        Description: activity
    */
    public function unmark(Request $request) {
        //TODO:
        $bookmarkId =  $request->input('id');
        $userId = Session::get('user')['id'];
//        $rs = Bookmarks::where(['id'=>$bookmarkId, 'userId' => $userId])
//                        ->update(['status'=> -1]);
        $rs = Bookmarks::where(['id'=>$bookmarkId, 'userId' => $userId])
                       ->delete();
        echo $rs;
        exit;
    }//End function
    
    
        /* 
        Author: Xuong
        Last code: 2015-09-15
        Parameters: []
        Description: activity
    */
    public function unmarkfind(Request $request) {
        //TODO:
        $contentId =  $request->input('id');
        $userId = Session::get('user')['id'];
//        $rs = Bookmarks::where(['id'=>$bookmarkId, 'userId' => $userId])
//                        ->update(['status'=> -1]);
        $rs = Bookmarks::where(['contentId'=>$contentId, 'userId' => $userId])
                       ->delete();
        echo $rs;
        exit;
    }//End function
    
    /* 
        Author: Xuong
        Last code: 2015-09-22
        Parameters: [name, email]
        Description: method: post
    */
    public function edit(Request $request) {
        //TODO:
        $name = $request->input('name');
        //$email = $request->input('email');
        $cUser = Session::get('user');
        
        if(empty($name))
        {
            return Redirect::to('/account/me')->withErrors('Name can not be empty !');
        }
        
//        if(User::where(['email' => $email,'status'=> 1])->where('Email', '!=', $cUser->email)->exists())
//        {
//            return Redirect::to('/account/me')->withErrors([$email. ' is early exist']);
//        }

        $rs = User::find($cUser->id)->update([
            'name' => $name
        ]);
        
        if($rs)
        {
            Session::forget('user');
            Session::put('user', User::find($cUser->id));
            return Redirect::to('/account/me')->withErrors(['Profile updated']);
        }
        else
        {
            return Redirect::to('/account/me')->withErrors(['Can not update profile right now']);
        }
    }//End function
    
    
    /* 
        Author: Xuong
        Last code: 2015-09-22
        Parameters: [name, email]
        Description: method: post
    */
    public function changepassword(Request $request) {
        //TODO:

        $cUser = Session::get('user');
        $changePassword = array();
        
        if(!empty($cUser->password))
        {
            $opw = $request->input('oldPassword');
            if($opw != $cUser->password)
            {
                return Redirect::to('/account/me')->withErrors('Old password is not correct');
            }
        }
        
        $pw = $request->input('newPassword');
        $rpw = $request->input('repeatNewPassword');
        
        if(empty($pw) || empty($rpw))
        {
            return Redirect::to('/account/me')->withErrors('Input new password please !');
        }
        
        if($pw != $rpw)
        {
            return Redirect::to('/account/me')->withErrors('New password and repeat one are not the same');
        }
        
        $rs = User::find($cUser->id)->update([
            'password' => $pw
        ]);
        
        if($rs)
        {
            Session::forget('user');
            Session::put('user', User::find($cUser->id));
            return Redirect::to('/account/me')->withErrors(['Password changed']);
        }
        else
        {
            return Redirect::to('/account/me')->withErrors(['Can not change password right now']);
        }
    }//End function
    
    
    /* 
        Author: Xuong
        Last code: 2015-09-day
        Parameters: []
        Description: activity
    */
    public function bookmark() {
        //TODO:
        $user = Session::get('user');
        $bookmarks = Bookmarks::join('contents', 'contents.id', '=', 'bookmarks.contentId')
                                ->where(['bookmarks.userId'     => $user['id']
                                         ,'bookmarks.status'    => 1
                                        ])
                                ->orderBy('bookmarks.id','desc')
                                ->select('bookmarks.*', 'contents.job_name', 'contents.job_url')
                                ->get();
        return view('user.bookmark')
                ->with(['bookmarks' => $bookmarks]);
    }//End function
}
