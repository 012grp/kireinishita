<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Router;
use App\Http\Controllers\Controller;

//model
use App\WaySite;

class WaySiteController extends Controller
{
	public function updateWaySite(Router $router, Request $request)
    {
        if ($request->isMethod('post')) 
        {
        	$provinceId = $request->provinceId;
        	$stationData = json_decode($request->stationData);
        	$arrData = array();
        	
        	foreach ($stationData as $key => $value) {
        		$waySite = WaySite::where('line_cd', $value->line_cd)
    						->where('parent_id', $provinceId)
    						->first();

    			$lineCd = $value->line_cd;
    			$lineName = $value->line_name;
    			$action = '';
    			$status = '';
    			if($waySite) //update
    			{
    				$action = 'Edited';
    				try
					{
						$waySite->name = $lineName;
						$waySite->save();
						
						$status = 'OK';
					}
					catch(Exception $e)
					{
						$status = 'Failed';
					}
					
    			}
    			else //insert
    			{
    				$action = 'Inserted';
    				try
					{
						$waysite = new WaySite();
    					$waysite->insert([
						    'line_cd' => $lineCd,
						    'name' => $lineName,
						    'parent_id' => $provinceId
						]);

						$status = 'OK';
					}
					catch(Exception $e)
					{
						$status = 'Failed';
					}
    				
    			}
        		
        		array_push($arrData, array(
        				'line_cd' => $lineCd,
        				'line_name' => $lineName,
        				'province_id' => $provinceId,
        				'action' => $action,
        				'status' => $status
        			));
        	}
        	
        	return json_encode($arrData);
        	//dd($stationData);
            //dd(json_decode($request->stationData));
            //return view('run')->with(['data' => $data]);
        }

        return view('waysite.update');
    }

    public function getListLineCd(Router $router, Request $request)
    {
        $arrLineCd = WaySite::select('line_cd')
                            ->get()->toArray();
        
        $arrResult = [];
        foreach ($arrLineCd as $value) {
            array_push($arrResult, $value['line_cd']);
        }

        $arrResult = array_unique($arrResult);

        return json_encode($arrResult);
    }


    public function updateKeyword(Router $router, Request $request)
    {
        if ($request->isMethod('post')) 
        {
            $lineCd = $request->lineCd;
            $stationData = json_decode($request->stationData);

            //dd($stationData);

            $keyword = '';
            if($stationData)
            {
                $keyword = $stationData->line_name;
                foreach ($stationData->station_l as $key => $value) {
                    if($value->station_name)
                    {
                        $keyword .= '|' . $value->station_name .'駅';
                    }
                }
            }

            $waySite = WaySite::where('line_cd', $lineCd)
                                ->update(['keywords' => $keyword]);

            //dd($keyword);
        }
        return view('waysite.update_keyword');
    }
}

