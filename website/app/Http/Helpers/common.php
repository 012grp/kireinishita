<?php
namespace App\Http\Helpers;

/*
	Usage:
	Ex: _common::test();
*/
use App\Http\Components\Util;

class common
{
    public static $_salt = '*_%%!~DFDSD^%@!@&#@(&@!^(%*^*%^&$#@&#*@*!@#**()CVXNCCKX';
    //one day
    public static function getShareLink($link,$id,$day=1)
    {
        $time=86400;
        $expire = time() + ($day*$time);
        $hash = md5($id.$expire.self::$_salt);
        return $link."?day=$day&e=$expire&h=$hash";
    }
    
    public static function checkShareLink($id, $expire, $hash)
    {
        $_expire = time();
        if($expire < $_expire)
        {
            //time out
            return false;
        }
        
        $_hash = md5($id.$expire.self::$_salt);
        if($_hash != $hash)
        {
            //access deny
            return false;
        }
        
        return true;
    }

    public static function proxy_image($url)
    {
        return '/proxy?url=' . rawurlencode($url);
    }

    public static function noImageUrl()
    {
        $randNumb = rand(1, 10);
        $imgName = $randNumb < 10 ? 'pict0' . $randNumb . '.jpg' : 'pict' . $randNumb . '.jpg';
        return '/public/img/no-image/' . $imgName;
    }

    public static function noImageUrlContents($screen = 'pc')
    {
        $img = $screen == 'pc' ? '/public/img/media/' : '/public/sp/img/media/';
        $img = $img . 'trend-01.jpg';
        return $img;
    }

    public static function noImageUrlContentsBySiteId($siteId = null)
    {
        $randNumb = null;
        $imgName = null;
        switch ($siteId) {
            //リジョブ, 美プロ, Salon career, BeAle(ビアーレ), ココネクト, GIRLS WOMAN
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 10:
                $imgName = 'logo.gif';
                break;
            //ブライダル・ヒューマン・ソリューション株式会社
            case 9:
                $randNumb = rand(1, 5);
                $imgName = 'pict0' . $randNumb . '.jpg';
                break;
            //彩職
            case 8:
                $randNumb = rand(16, 20);
                $imgName = 'pict' . $randNumb . '.jpg';
                break;
            //美容外科求人ガイド, 美容整形ジョブ！
            case 6:
            case 7:
                $randNumb = rand(6, 10);
                $imgName = $randNumb < 10 ? 'pict0' . $randNumb . '.jpg' : 'pict' . $randNumb . '.jpg';
                break;
            //GＵＰＰＹ
            case 11:
                $randNumb = rand(11, 15);
                $imgName = 'pict' . $randNumb . '.jpg';
                break;
            default:
                $imgName = 'logo.gif';
                break;
        }
        return '/public/img/no-image/' . $imgName;
    }

    public static function groupConcat($symbol, $group)
    {
        $concat = '';
        foreach ($group as $val) {
            if (!empty($val)) {
                $concat .= $val . ',';
            }
        }
        $concat = rtrim($concat, ',');
        return $concat;
    }

    public static function test()
    {
        return 'common';
    }

    public static function changeToArray($requestEm)
    {
        if (!is_array($requestEm)) {
            $requestEm = [$requestEm => $requestEm];
        } else {
            $requestEmTmp = [];
            foreach ($requestEm as $em) {
                $requestEmTmp[$em] = $em;
            }
            $requestEm = $requestEmTmp;
        }
        return $requestEm;
    }

    /*
        Input:
        array(
            1 => array(1,2,4)
            2 => array(5,6,7)
        )
        Ouput:
        array(1,2,4,5,6,7)
    */
    public static function mergeArray($array)
    {
        $merged = array();
        foreach ($array as $arr) {
            $merged = $merged + $arr;
        }
        return $merged;
    }

    public static function createIdNameList($array)
    {
        $merged = array();
        foreach ($array as $arr) {
            $idNames = array();
            foreach ($arr as $record) {
                $idNames[$record['id']] = $record['name'];
            }
            $merged = $merged + $idNames;
        }
        return $merged;
    }

    public static function makeUrlPars($obj, $name)
    {
        $em = '';
        if (is_array($obj)) {
            foreach ($obj as $val) {
                $em .= $name . '[]=' . $val . '&';
            }
        } else {
            $em = $name . '=' . $obj;
        }

        if (!empty($em)) {
            $em = '?' . $em;
        }

        return $em;
    }

    /*
        array(
            'a' => 1,
            'b' => 2,
            'c' => array(2,4,5)
        )
        => ?a=1&b=2&c[]=2&c[]=4&c[]=5
    */
    public static function makeFullPars($request)
    {
        if (!is_array($request)) {
            return '';
        }

        $uri = '';
        foreach ($request as $key => $val) {
            if (is_array($val)) {
                foreach ($val as $xsVal) {
                    $uri .= $key . '[]=' . $xsVal . '&';
                }
            } else {
                $uri .= $key . '=' . $val . '&';
            }

        }
        if (!empty($uri)) {
            $uri = '?' . $uri;
        }

        return $uri;
    }

    /**
     * Handling $job_name of crawler jobs
     */
    public static function getJobName($jobName)
    {
        $jobName = strip_tags($jobName);
        $jobName = str_replace(array('　', ' '), '', $jobName);
        return $jobName;
    }

    /**
     * Handling $job_name of crawler jobs
     * @param  [[Type]] $jobCareer [[Description]]
     * @return [[Type]] [[Description]]
     */
    public static function getJobCareer($jobCareer)
    {
        //TODO: youshouldwritesomethinghere
        $jobCareer = str_replace('急募未経験OK', '急募 ', $jobCareer);
        $jobCareer = preg_replace("/(未経験OK|年齢不問|外国人歓迎|WワークOK|待遇|社会保険あり|寮・社宅あり|各種手当充実|交通費支給|研修あり|制服貸与|社員登用あり|週休2日|特徴|独立希望者歓迎|オープニング|高収入|定休日あり|服装自由|まかないあり|個人経営|海外勤務あり|リゾート)/",
            "$1 ",
            $jobCareer);

        return $jobCareer;
    }

    /**
     * Change CSS h2, h3, p tag of secret detail content
     */
    public static function changeCSSContentSecret($content)
    {
        $content = preg_replace("/<h3 class=\"(\w|-|\s)+\"/i", '<h3 class="contents-box--recruit__ttl u-fs--xlh u-mt25 u-fwb"', $content);
        $content = preg_replace("/<h2 class=\"(\w|-|\s)+\"/i", '<h2 class="contents-box--recruit__ttl u-fs--xlh u-mt25 u-fwb"', $content);
        $content = preg_replace("/<(p|div) class=\"(\w|-|\s)*_contents__img__block (\w|-|\s)+\"/i", '<$1 class="contents-box__img u-mt15"', $content);
        $content = preg_replace("/<(p|div) class=\"(\w|-|\s)*_contents__img_text (\w|-|\s)+\"/i", '<$1 class="contents-box__caption u-mt15 u-fs--xsh"', $content);
        $content = preg_replace("/<p class=\"(\w|-|\s)+\"/i", '<p class="contents-box__text u-fs--mh u-mt15"', $content);

        return $content;
    }

    public static function changeCSSContentArticle($artContent, $category = '')
    {
        switch ($category) {
            case 'interview':
                $artContent = preg_replace("/<h2 class=\"(\w|-|\s)*column-box_contents__ttl(\w|-|\s)+\"/i", '<h2 class="contents-box__ttl--border u-fs--xlh--contents u-fwb u-mt35"', $artContent);
                $artContent = preg_replace("/<h3 class=\"(\w|-|\s)*u-fs--xxlh--contents(\w|-|\s)+\"/", '<h3 class="contents-box__ttl--sub--interview u-fs--xlh u-fwb u-mt30"', $artContent);
                break;

            case 'qa':
                $artContent = preg_replace("/<div class=\"_detail__block__qa\"/i", '<div class="contents-box__qa u-mt30"', $artContent);
                $artContent = preg_replace("/<h2 class=\"u-fs--xxlh--contents u-fwb u-mt5\"/i", '<h2 class="u-fs--xlh u-fwb u-mt10"', $artContent);
                $artContent = preg_replace("/<div class=\"_contents__overview u-mt30 u-fs--lh--contents\"/i", '<div class="contents-box__overview u-mt20 u-fs--mh"', $artContent);
                $artContent = preg_replace("/<h3 class=\"u-fwb _contents__overview__otherTtl\"/i", '<h3 class="u-fwb contents-box__overview__otherTtl"', $artContent);
                $artContent = preg_replace("/<h2 class=\"(\w|-|\s)*column-box_contents__subTtl(\w|-|\s)+\"/i", '<h2 class="contents-box__ttl--sub u-fs--xlh u-mt35"', $artContent);
                break;

            default:
                $artContent = preg_replace("/<h2 class=\"(\w|-|\s)*column-box_contents__subTtl(\w|-|\s)+\"/i", '<h2 class="contents-box__ttl--sub u-fs--xlh u-mt20 u-fwb"', $artContent, 1);
                $artContent = preg_replace("/<h2 class=\"(\w|-|\s)*column-box_contents__ttl(\w|-|\s)+\"/i", '<h2 class="contents-box__ttl u-fs--xlh u-mt35 u-fwb"', $artContent);
                $artContent = preg_replace("/<h2 class=\"(\w|-|\s)*column-box_contents__subTtl(\w|-|\s)+\"/i", '<h2 class="contents-box__ttl--sub u-fs--xlh u-mt35 u-fwb"', $artContent);
                $artContent = preg_replace("/<h3 class=\"(\w|-|\s)*column-box_contents__subTtl(\w|-|\s)+\"/", '<h3 class="contents-box__ttl--sub u-fs--xlh u-mt35 u-fwb"', $artContent);
                break;
        }


        $artContent = preg_replace("/<(p|div) class=\"(\w|-|\s)*column-box_contents__subTtl (\w|-|\s)+\"/i", '<$1 class="contents-box__ttl--sub u-fs--xlh u-mt35"', $artContent);
        $artContent = preg_replace("/<(p|div) class=\"(\w|-|\s)*_contents__img__block (\w|-|\s)+\"/i", '<$1 class="contents-box__img u-mt15"', $artContent);
        $artContent = preg_replace("/<(p|div) class=\"(\w|-|\s)*_contents__img_text (\w|-|\s)+\"/i", '<$1 class="contents-box__caption u-mt15 u-fs--xsh"', $artContent);
        $artContent = preg_replace("/<p class=\"column-box_contents__text (\w|-|\s)+\"/i", '<p class="contents-box__text u-fs--mh u-mt15"', $artContent);
        $artContent = str_replace('<p class="contents-box__text u-fs--mh u-mt15"><br></p>', '', $artContent);
        $artContent = str_replace('<p class="contents-box__img u-mt15"><br></p>', '', $artContent);
        $artContent = str_replace('"_contents__overview u-mt30 u-fs--lh--contents--other"', '"contents-box__overview u-mt20 u-fs--mh"', $artContent);
        return $artContent;
    }

    public static function removeImageOfPC($artContent)
    {
        //TODO: removeImageOfPC
        $artContent = preg_replace("/<img class=\"fr-pc[^>]+>/", '', $artContent);
        return $artContent;
    }

    public static function removeImageOfSP($artContent)
    {
        //TODO: removeImageOfSP
        $artContent = preg_replace("/<img class=\"fr-sp[^>]+>/", '', $artContent);
        return $artContent;
    }

    public static function getArticleCatDir($catAlias)
    {
        switch ($catAlias) {
            case 'qa':
                return 'qa';
                break;
            case 'interview':
                return 'interview';
                break;
            default:
                return 'default';
        }
    }

    public static function microtime()
    {
        $util = new Util();
        return $util->microtime();
    }
	
	public static function getJobImage($job_image, $site_id)
	{
		//TODO: check image
		if(!isset($job_image) || empty($job_image))
		{
			return self::noImageUrlContentsBySiteId($site_id);
		}
		else
		{
			$rs = strpos($job_image, 'http');
			if($rs===false && $job_image[0] != '/')
			{
				$job_image = '//'.$job_image;
			}
			return self::proxy_image($job_image);
		}
	}
	
	public static function getSecretImage($job_image)
	{
		if(!isset($job_image) || empty($job_image))
		{
			return  self::noImageUrlContentsBySiteId(0);
		}
		else
		{
			$rs = strpos($job_image, 'http');
			if($rs===false && $job_image[0] != '/')
			{
				$job_image = '//'.$job_image;
			}
			return $job_image;
		}
	}
}

?>