<?php
namespace App\Http\Helpers;

use App\Alliance;
use App\Area;
use App\ArticleCategories;
use App\Contents;
use App\Http\Components\BookmarkComponent;
use App\Http\Components\UserComponent;
use App\JobCategory;
use App\SecretJob;
use App\Station;
use App\WaySite;

use App\AllianceJobCategory;
use App\AllianceEmploymentType;
use App\AllianceBusinessCategory;

use Cache;
use Illuminate\Support\Facades\Lang;
use Session;
use Illuminate\Support\Facades\DB;

//model

//component

/*
	Usage:
	Ex: _helper::test();
*/
class helper
{
    public static $_onlyJobCategory = null;
    public static $_allCategory = null;
    public static $_allEmployment = null;
    public static $_allStations = null;
    public static $_provinceNames = null;


    //alliance job
    public static $_onlyAllianceJobCategory = null;
    public static $_allAllianceEmployment = null;
    public static $_allAllianceBusinessCategory = null;

    public static function getEmploymentNamesByListId($list_em)
    {
        if (self::$_allEmployment == null) {
            $arrEmploment = [];
            $dbEm = self::getEmployment();
            foreach ($dbEm as $val) {
                $arrEmploment[$val['id']] = $val['name'];
            }
            self::$_allEmployment = $arrEmploment;
        }

        $arrKeyEmployment = [];
        if (!empty($list_em)) {
            $list_em = trim($list_em, ',');
            $arrKeyEmployment = explode(',', $list_em);
        }

        $employment = '';
        foreach ($arrKeyEmployment as $value) {
            if ($value) {
                if (isset(self::$_allEmployment[$value])) {
                    $employment .= self::$_allEmployment[$value] . ',';
                }
            }
        }
        if ($employment) {
            $employment = rtrim($employment, ',');
        }
        return $employment;
    }

    public static function getCategoryNamesByListId($list_job_parent, $job_category)
    {
        if (self::$_allCategory == null) {
            self::$_allCategory = self::getAllJobCategory();
        }
        $arrKeyParent = [];
        $arrKeyChild = [];
        if (!empty($list_job_parent)) {
            $list_job_parent = trim($list_job_parent, ',');
            $arrKeyParent = explode(',', $list_job_parent);
        }

        if (!empty($job_category)) {
            $job_category = trim($job_category, ',');
            $arrKeyChild = explode(',', $job_category);
        }

        $arrKey = array_merge($arrKeyParent, $arrKeyChild);


        //dd(self::$_allCategory);
        $job_category = '';
        foreach ($arrKey as $value) {
            if (!empty($value)) {
                if (isset(self::$_allCategory[$value])) {
                    $job_category .= self::$_allCategory[$value] . ',';
                }
            }
        }
        if ($job_category) {
            $job_category = rtrim($job_category, ',');
        }
        return $job_category;
    }


    //only get job category (職種)
    public static function getJobCategoryNameByListId($list_job_parent, $job_category)
    {
        if (self::$_onlyJobCategory == null) {
            self::$_onlyJobCategory = self::getOnlyJobCategory();
        }
        $arrKeyParent = [];
        $arrKeyChild = [];
        if (!empty($list_job_parent)) {
            $list_job_parent = trim($list_job_parent, ',');
            //$arrKeyParent = explode(',', $list_job_parent);
        }

        if (!empty($job_category)) {
            $job_category = trim($job_category, ',');
            $arrKeyChild = explode(',', $job_category);
        }

        $arrKey = array_merge($arrKeyParent, $arrKeyChild);


        $job_category = '';
        foreach ($arrKey as $value) {
            if (!empty($value)) {
                if (isset(self::$_onlyJobCategory[$value])) {
                    $job_category .= self::$_onlyJobCategory[$value] . ',';
                }
            }
        }
        if ($job_category) {
            $job_category = rtrim($job_category, ',');
        }
        return $job_category;
    }

    public static function get_info()
    {
        if (Session::has('user')) {
            $user = UserComponent::get_info();
            return $user;
        }

        return false;
    }

    public static function getJobCategory()
    {
        $data = self::getJobCategoryLevel1GroupByParentId();
        return array(
            '美容' => $data[1],
            'リラクゼーション' => $data[2],
            '治療' => $data[3],
        );

    }

    public static function getJobCategoryList()
    {
        $data = self::getJobCategoryLevel1GroupByParentId();
        $list = array();
        array_push($list, $data[1]);
        array_push($list, $data[2]);
        array_push($list, $data[3]);

        return $list;
    }

    public static function getHangUp()
    {
        $data = self::getJobCategoryLevel1GroupByParentId();
        return array(
            '応募条件' => $data[50],
            '待遇' => $data[51],
            '特徴' => $data[52],
        );
    }

    public static function getEmployment()
    {
        //TODO: getEmployment
        if (Cache::has('__employments')) {
            //Return cached file
            return Cache::get('__employments');
        } else {
            $minutes = 10080;
            $jobCategory = new JobCategory();
            $data = $jobCategory->getEmployment();
            Cache::put('__employments', $data, $minutes);
            return $data;
        }
    }

    public static function getEmploymentNameById($id)
    {
        $data = self::getEmployment();
        foreach ($data as $record) {
            if ($record['id'] == $id) {
                return $record['name'];
            }
        }
        return '';
    }

    public static function getJobCategoryLevel1GroupByParentId()
    {
        //TODO: getJobCategoryLevel1GroupByParentId
        //Cache::forget('__category_level1');
        if (Cache::has('__category_level1')) {
            //Return cached file
            return Cache::get('__category_level1');
        } else {
            $minutes = 10080;
            $jobCategory = new JobCategory();
            $result = $jobCategory->getJobCategoryLevel1();
            $data = array();
            foreach ($result as $record) {
                if (!isset($data[$record['parent_id']])) {
                    $data[$record['parent_id']] = array();
                }
                $data[$record['parent_id']][] = $record;
            }

            Cache::put('__category_level1', $data, $minutes);

            return $data;
        }

    }

    public static function getAllJobCategory()
    {
        //TODO: getJobCategoryLevel1GroupByParentId
        //Cache::forget('__job_category');
        if (Cache::has('__job_category')) {
            //Return cached file
            return Cache::get('__job_category');
        } else {
            $minutes = 10080;
            $jobCategory = new JobCategory();

            $categories = $jobCategory->getJobCategory();


            $result = array();
            foreach ($categories as $category) {
                $category = $category->toArray();
                $result[$category['id']] = $category['name'];
            }

            Cache::put('__job_category', $result, $minutes);

            return $result;
        }

    }

    public static function getOnlyJobCategory()
    {
        //TODO: getJobCategoryLevel1GroupByParentId
        //Cache::forget('__job_category');
        if (Cache::has('__only_job_category')) {
            //Return cached file
            return Cache::get('__only_job_category');
        } else {
            $minutes = 10080;
            $jobCategory = new JobCategory();

            $categories = $jobCategory->getOnlyJobCategory();


            $result = array();
            foreach ($categories as $category) {
                $category = $category->toArray();
                $result[$category['id']] = $category['name'];
            }

            Cache::put('__only_job_category', $result, $minutes);

            return $result;
        }

    }

    public static function getOnlyAllienceJobCategory()
    {
        //TODO: getJobCategoryLevel1GroupByParentId
        //Cache::forget('__job_category');
        if (Cache::has('__only_allience_job_category')) {
            //Return cached file
            return Cache::get('__only_allience_job_category');
        } else {
            $minutes = 10080;
            $jobCategory = new AllianceJobCategory();

            $categories = $jobCategory->getOnlyJobCategory();

            $result = array();
            foreach ($categories as $category) {
                $category = $category->toArray();
                $result[$category['id']] = $category['name'];
            }

            Cache::put('__only_allience_job_category', $result, $minutes);

            return $result;
        }

    }

    /*
        @note: cached 1 days
        @return list of province
    */
    public static function getProvinceNames()
    {
        //TODO: getProvinceNames
        if (Cache::has('__province_names')) {
            //Return cached file
            return Cache::get('__province_names');
        } else {
            $minutes = 1440;
            $area = new Area();
            $data = $area->getProvince()->toArray();
            Cache::put('__province_names', $data, $minutes);

            return $data;
        }
    }

    /*
        @note: cached 1 day
        @return list of city
    */
    public static function getCityNames()
    {
        //Cache::forget('__city_names');
        //TODO: getCityNamesAsJson
        if (Cache::has('__city_names')) {
            //Return cached file
            return Cache::get('__city_names');
        } else {
            $minutes = 1440;
            $area = new Area();
            $data = $area->getAllCity()->toArray();
            //$data = json_encode($data);
            Cache::put('__city_names', $data, $minutes);
            return $data;
        }
    }

    public static function getProvinceNameById($provinceId)
    {
        //TODO: getProvinceNameById
        if (self::$_provinceNames == null) {
            self::$_provinceNames = self::getProvinceNames();
        }
        foreach (self::$_provinceNames as $province) {
            if ($province['id'] == $provinceId) {
                return $province['name'];
            }
        }
        return '';
    }

    public static function getCityNameById($cityId)
    {
        //TODO: getCityNameById
        $cities = self::getCityNames();
        foreach ($cities as $city) {
            if ($city['id'] == $cityId) {
                return $city['name'];
            }
        }
        return '';
    }

    public static function getCitiesByProvince($provinceId)
    {
        if (empty($provinceId)) return array();
        //TODO: getCitiesByProvince
        $area = new Area();
        $data = $area->getCitiesByProvince($provinceId);
        return $data;
    }

    public static function getWaysitesByProvince($provinceId)
    {
        if (empty($provinceId)) return array();
        //TODO: getCitiesByProvince
        $area = new WaySite();
        $data = $area->getWaySitesByProvince($provinceId);
        return $data;
    }

    /*
        @fields: id, line_cd, line_name
        @return Array waysite with line_cd is unique
    */
    public static function getAllWaysite()
    {
        //Cache::forget('__all_waysites');
        //TODO: getCityNamesAsJson
        if (Cache::has('__all_waysites')) {
            //Return cached file
            return Cache::get('__all_waysites');
        } else {
            $minutes = 7440;
            $waysite = new WaySite();
            $data = $waysite->getAllWaySite();
            Cache::put('__all_waysites', $data, $minutes);
            return $data;
        }
    }

    /*
        @fields: id, code, name
        @return Array station with code is unique
    */
    public static function getAllStation()
    {
        if (Cache::has('__all_stations')) {
            //Return cached file
            return Cache::get('__all_stations');
        } else {
            $minutes = 7440;
            $station = new Station();
            $data = $station->getAllStation();
            Cache::put('__all_stations', $data, $minutes);
            return $data;
        }
    }

    public static function convertStationToKeyValue()
    {
        $stations = self::getAllStation();
        $keyValueStation = (object)[];
        foreach ($stations as $station) {
            $keyValueStation->$station['code'] = $station['name'];
        }
        return $keyValueStation;
    }

    public static function getWaysiteNameById($line_cd)
    {
        $waysites = self::getAllWaysite();
        foreach ($waysites as $waysite) {
            if ($waysite['line_cd'] == $line_cd) {
                return $waysite['name'];
            }
        }
        return '';
    }


    public static function getStationByIds($arrCode)
    {
        $station = new Station();
        $data = $station->getStationByIds($arrCode);
        return $data;
    }

    public static function getStationNameById($code)
    {
        if (self::$_allStations == null) {
            self::$_allStations = self::convertStationToKeyValue();
        }
        if (true && $rs = self::$_allStations->{$code}) {
            return $rs;
        }
        return '';
    }

    public static function explode_hp($paragraph)
    {
        if (empty($paragraph)) {
            return '';
        }

        //TODO: explode_hp
        $html = '';
        $arrParagraph = explode("\r\n", $paragraph);
        $name = '';
        $link = '';
        foreach ($arrParagraph as $value) {
            if (!empty($value)) {
                if (!empty($name)) {
                    $link = $value;
                } else {
                    $name = $value;
                }
            }

            if (!empty($name) && !empty($link)) {
                $html .= '<li><a target="_blank" href="' . $link . '">' . $name . '</a></li>';
                $name = '';
                $link = '';
            }
        }
        if (!empty($html)) {
            $html = '<ul>' . $html . '</ul>';
        }
        return $html;
    }

    public static function explode_ul($paragraph, $ul_class_first = '', $ul_class = 'u-mt20')
    {
        $arrParagraph = explode("\r\n\r\n", $paragraph);
        foreach ($arrParagraph as $key => $value) {
            $value = explode("\r\n", $value);
            if ($key == 0) {
                echo "<ul class='_detail__table__text--list $ul_class_first'>";
            } else {
                echo "<ul class='_detail__table__text--list $ul_class'>";
            }
            foreach ($value as $key => $item) {
                echo "<li>$item</li>";
            }
            echo '</ul>';
        }
    }

    public static function explode_character($paragraph, $character = ',')
    {
        $paragraph = explode($character, $paragraph);
        foreach ($paragraph as $item) {
            $item = trim($item);
            echo "<span class='c-tag--detail--table'>$item</span>";
        }
    }


    public static function addBr($paragraph)
    {
        $search = [	
					'】 ', 
					' 【',
					' ◇', 
					' ※', 
					' 　　', 
					' ・',
					' ★',
					' ■',
					'‾',
				];
		$replace = [
					'】 ' . "\n", 
					"\n" . ' 【',
					"\n\n" . ' ◇', 
					"\n" . ' ※', 
					"\n" . ' 　　', 
					"\n" . ' ・',
					"\n" . ' ★',
					"\n\n" . ' ■',
					'~',
		];
		
		$paragraph = str_replace($search, $replace, $paragraph);
		return $paragraph;
    }

    public static function addOneBr($paragraph)
    {
        $search = [ 
                    '】 ', 
                    ' 【',
                    ' ◇', 
                    ' ※', 
                    ' 　　', 
                    ' ・',
                    ' ★',
                    ' ■',
                ];
        $replace = [
                    '】 ' . "\n", 
                    "\n" . ' 【',
                    "\n" . ' ◇', 
                    "\n" . ' ※', 
                    "\n" . ' 　　', 
                    "\n" . ' ・',
                    "\n" . ' ★',
                    "\n" . ' ■',
        ];
        
        $paragraph = str_replace($search, $replace, $paragraph);
        return $paragraph;
    }

    /*
        @decription: set title for web
        @param: $string before │キレイにスルンダ
        @result:  "$string │キレイにスルンダ"
    */
    public static function set_title($tring = null)
    {
        return $tring . '│キレイにスルンダ';
    }


    public static function classActivePath($path)
    {
        $path = explode('.', $path);
        $segment = 1;
        foreach ($path as $p) {
            if ((request()->segment($segment) == $p) == false) {
                return '';
            }
            $segment++;
        }
        return 'class="active"';
    }


    /*
        @input $job_id
        @output /job/{$job_id}
    */
    public static function make_job_url($job_id)
    {
        return '/job/' . $job_id;
    }

    public static function bookmark_job_after_login($job_id, $is_secret_job)
    {
        BookmarkComponent::bookmark_job_after_login($job_id, $is_secret_job);
    }

    public static function get_list_menu()
    {
        $cat = new ArticleCategories();
        $contentsURL = '/contents/';
        $categories = $cat->getListCategory();
        $arrCategory = [];
        for ($i = 0; $i < count($categories); $i++) {
            $category_alias = $categories[$i]['category_alias'];
            $arrCategory[$i] = array(
                'name' => $categories[$i]['category'],
                'class_name' => $categories[$i]['menu_class'],
                'class_name_sp' => $categories[$i]['menu_class_sp'],
                'category_alias' => $category_alias,
                'url' => $contentsURL . $category_alias . '/',
                'i_tag' => '<i class="c-icon--l"></i>'
            );
        }
        return $arrCategory;
    }

    public static function countOfSearch($request)
    {
        $content = new Contents();
        return $content->countOfSearch($request);
    }

    public static function checkNew($dateTime, $day)
    {
        if ($dateTime > (new \DateTime("-$day day"))->format('Y-m-d H:i:s')) {
            return true;
        }
        return false;
    }

    /*
        @pars: job_id, company_id
        @return List of secret jobs for HP
    */
    public static function getRelatedSecretJobs($secret_id, $company_id)
    {
        //TODO: getRelatedSecretJobs
        $secret = new SecretJob();
        return $secret->getRelated($secret_id, $company_id, 2);
    }

    public static function entryJobObjective()
    {
        return lang::get('entry.job_objective');
    }

    public static function entryHopeEmployment()
    {
        return lang::get('entry.hope_employment');
    }

    public static function getWebInfo($key = '')
    {
        return lang::get('webinfo.' . $key);
    }

    public static function getArrayEmByListEm($list_em)
    {
        $result = self::getEmploymentNamesByListId($list_em);
        return explode(',', $result);
    }

    public static function getArrayJobCategoryByListJob($list_job)
    {
        $result = self::getJobCategoryNameByListId('', $list_job);
        return explode(',', $result);
    }


    public static function getUrlName($pageName)
    {
        switch ($pageName) {
            case 'secretPage':
                return 'limited_kyujin';
                break;
            case 'alliancePage':
                return 'osusume_kyujin';
                break;

            default:
                return '/';
                break;
        }
    }


    //------------------Support VIEW
    public static function viewMixData($data, $callbackJob, $callbackSecret, $callbackAlliance, $callbackInjection = null)
    {

        $current_total = $data['current_total'];
        $total_job = $data['current_job'];
        $total_secret = $data['current_secret'];
        $total_alliance = $data['current_alliance'];
        $index_job = 0;
        $index_secret = 0;
        $index_alliance = 0;
        $isInjection = $callbackInjection != null && is_callable($callbackInjection);


        for ($i = 0; $i < $current_total; $i++) {
            if ($index_secret < $total_secret) {
                $callbackSecret($data['data_secret'][$index_secret], $index_secret, $i);
                $index_secret++;
            } else if ($index_alliance < $total_alliance) {
                $callbackAlliance($data['data_alliance'][$index_alliance], $index_alliance, $i);
                $index_alliance++;
            } else {
                $callbackJob($data['data_job'][$index_job], $index_job, $i);
                $index_job++;
            }


            if ($isInjection) {
                $callbackInjection($data, $i);
            }
        }
    }

    public static function getJobCategoryById($id)
    {
        $jobCategory = new JobCategory();
        return $jobCategory->getById($id);
    }

    //only get job category (職種)
    public static function getAllianceJobCategoryNameByListId($job_category)
    {
        if (self::$_onlyAllianceJobCategory == null) {
            self::$_onlyAllianceJobCategory = self::getOnlyAllianceJobCategory();
        }
		
        $arrKeyChild = [];
        if (!empty($job_category)) {
            $job_category = trim($job_category, ',');
            $arrKeyChild = explode(',', $job_category);
        }
		
        $job_category = '';
        foreach ($arrKeyChild as $value) {
            if (!empty($value)) {
                if (isset(self::$_onlyAllianceJobCategory[$value])) {
                    $job_category .= self::$_onlyAllianceJobCategory[$value] . ',';
                }
            }
        }
		
		
		
        if ($job_category) {
            $job_category = rtrim($job_category, ',');
        }
        return $job_category;
    }

    public static function getOnlyAllianceJobCategory()
    {
        //TODO: getJobCategoryLevel1GroupByParentId
        //Cache::forget('__job_category');
        if (Cache::has('__only_alliance_job_category')) {
            //Return cached file
            return Cache::get('__only_alliance_job_category');
        } else {
            $minutes = 10080;
            $jobCategory = new AllianceJobCategory();

            $categories = $jobCategory->getOnlyJobCategory();


            $result = array();
            foreach ($categories as $category) {
                $category = $category->toArray();
                $result[$category['id']] = $category['name'];
            }

            Cache::put('__only_alliance_job_category', $result, $minutes);

            return $result;
        }

    }


    public static function getAllianceEmploymentNamesByListId($list_em)
    {
        if (self::$_allAllianceEmployment == null) {
            $arrEmploment = [];
            $dbEm = self::getAllianceEmployment();
            foreach ($dbEm as $val) {
                $arrEmploment[$val['id']] = $val['name'];
            }
            self::$_allAllianceEmployment = $arrEmploment;
        }

        $arrKeyEmployment = [];
        if (!empty($list_em)) {
            $list_em = trim($list_em, ',');
            $arrKeyEmployment = explode(',', $list_em);
        }

        $employment = '';
        foreach ($arrKeyEmployment as $value) {
            if ($value) {
                if (isset(self::$_allAllianceEmployment[$value])) {
                    $employment .= self::$_allAllianceEmployment[$value] . ',';
                }
            }
        }
        if ($employment) {
            $employment = rtrim($employment, ',');
        }
        return $employment;
    }


    public static function getAllianceEmployment()
    {
        //TODO: getEmployment
        if (Cache::has('__alliance_employments')) {
            //Return cached file
            return Cache::get('__alliance_employments');
        } else {
            $minutes = 10080;
            $jobCategory = new AllianceEmploymentType();
            $data = $jobCategory->getEmployment();
            Cache::put('__alliance_employments', $data, $minutes);
            return $data;
        }
    }


    public static function getAllianceBusinessCategoryNamesByListId($job_category)
    {
        if (self::$_allAllianceBusinessCategory == null) {
            self::$_allAllianceBusinessCategory = self::getAllAllianceBusinessCategory();
        }
        
        $arrKeyChild = [];
        if (!empty($job_category)) {
            $job_category = trim($job_category, ',');
            $arrKeyChild = explode(',', $job_category);
        }


        //dd(self::$AllianceBusinessCategory);
        $job_category = '';
        foreach ($arrKeyChild as $value) {
            if (!empty($value)) {
                if (isset(self::$_allAllianceBusinessCategory[$value])) {
                    $job_category .= self::$_allAllianceBusinessCategory[$value] . ',';
                }
            }
        }
        if ($job_category) {
            $job_category = rtrim($job_category, ',');
        }
        return $job_category;
    }


    public static function getAllAllianceBusinessCategory()
    {
        //TODO: getJobCategoryLevel1GroupByParentId
        //Cache::forget('__job_category');
        if (Cache::has('__alliance_business_category')) {
            //Return cached file
            return Cache::get('__alliance_business_category');
        } else {
            $minutes = 10080;
            $jobCategory = new AllianceBusinessCategory();

            $categories = $jobCategory->getOnlyJobCategory();


            $result = array();
            foreach ($categories as $category) {
                $category = $category->toArray();
                $result[$category['id']] = $category['name'];
            }

            Cache::put('__alliance_business_category', $result, $minutes);

            return $result;
        }

    }

	public static function getJobAddressSpecify($list_province, $list_city, $job_location){
		$prefecture=DB::table('aliance_prefecture')->where('id', str_replace(',','',$list_province))->first();
        $city = DB::table('aliance_city')->where('id', str_replace(',','',$list_city))->first();
        $location= strip_tags($job_location);

        $result = $location;
        $result = !empty($city->name) ? $city->name.$result : $result;
        $result = !empty($prefecture->name) ? $prefecture->name.$result : $result;
        return $result;
	}
	
    public static function getJobAddress($detail){
        return self::getJobAddressSpecify($detail['list_province'], $detail['list_city'], $detail['job_location']);
    }

    public static function getAllianceListHangup($detail){
        $result = array();
        $shop = DB::table('aliance_shop')->where('id', $detail->shop_id)->first();
        if(self::checkAllianceHoliday($detail->job_holiday))
        {
            $result[] = self::checkAllianceHoliday($detail->job_holiday);
        }
        if(!empty($shop)) {
            if ($shop->access_type == 1 && $shop->duration <= 5) {
                $result[] = '駅近';
            }
        }

        return $result;
    }

    public static function checkAllianceHoliday($allienceJobShareHoliday){
        $listholiday = array('週休二日制','週休2日制','週休２日制','週休二日','週休2日','週休２日');
        foreach ($listholiday as $holiday){
            if (strpos($allienceJobShareHoliday,$holiday ) ) { // Yoshi version
                return '週休2日';
            }
        }
        return null;
    }

    public static function getAccessGuide($detail){
        $sation = DB::table('aliance_station')->where('id', $detail->list_station)->first();
        $accessType = DB::table('aliance_access_type')->where('id', $detail->access_type)->first();
        
		$result = '';
		if(!empty($sation) and !empty($accessType))
		{
			$result = '最寄り駅('.$sation->name.')から'.$accessType->name.'でおおよそ'.$detail->duration.'分' ;	
		}
        return $result;

    }

    public static function getAllienceParentCategory($job_category){
        $cat = DB::table('aliance_job_category')->where('id', $job_category)->first();
        $catParent = DB::table('job_category')->where('id', $cat->parent_id)->first();
        return !empty($catParent) ? $catParent->name : '';
    }

    public static function getAllienceJobBookmark(){
        $user = UserComponent::get_info();
        if(!empty($user)) {
            $bookmarkJob = DB::table('bookmarks')
                ->where('userId', $user['id'])
                ->where('type', 2)
                ->where('status', 1)
                ->get();
            $tmp = array();
            foreach ($bookmarkJob as $job){
                $tmp[] = $job->allience_id;
            }
            $listJob =Alliance::whereIn('id', $tmp)->where('status', 1)->take(3)->get()->toArray();
            return $listJob;
        }
        return '';
    }

    
}

?>