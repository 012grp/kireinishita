<?php 
namespace App\Http\Helpers;
use Session;
use Cache;
use App\Contents;
use App\SecretJob;
use App\Articles;
use App\ArticleCategories;
use App\Alliance;
use DB;

//component
use App\Http\Components\Util;

/*
	Usage:
	Ex: modules::test();
*/
class modules{
	
	public static function test()
	{
		return 'modules';
	}

    /*
        TODO: count total jobs & count new jobs from the last 7 day
    */
	public static function total_job_today()
    {
		if(Cache::has('__countJob'))
    	{
    		$job = Cache::get('__countJob');
    	}
    	else
    	{

    		$totalJobs = Contents::where('status',1)->count();
	        $newJobs = DB::select(DB::raw('SELECT count(id) as countNewJob 
											FROM contents 
											WHERE created_at BETWEEN CURDATE()-INTERVAL 1 WEEK AND CURDATE() AND status=1'));
            //total alience job
            $totalAllienceJobs = Alliance::where('status',1)->count();
            $newAllienceJobs = DB::select(DB::raw('SELECT count(id) as countNewJob 
											FROM alliance_job 
											WHERE created_at BETWEEN CURDATE()-INTERVAL 1 WEEK AND CURDATE() AND status=1'));
	        $job = array();
	        $job['total'] = $totalJobs + $totalAllienceJobs;
	        $job['new'] = $newJobs[0]->countNewJob + $newAllienceJobs[0]->countNewJob;
			
			$minutes = 1000;
			Cache::put('__countJob', $job, $minutes);
        }
        return $job;
    }

    /*
        TODO: show 6 new jobs by create time
    */
    public static function top_new_jobs()
    {
		if(Cache::has('__top_new_jobs_crawler'))
    	{
    		$jobs = Cache::get('__top_new_jobs_crawler');
			return $jobs;
    	}
    	else
    	{
			$mixedJobs = collect([]);
			$jobs = Contents::where('status', 1)
							->orderBy('priority', 'desc')
							->skip(0)->take(3)->get();
			
			foreach( $jobs as $job)
			{
				$job['url'] = '';
				$job['job_image'] = common::getJobImage($job['job_image'], $job['site_id']);
				$job['job_url'] = helper::make_job_url($job['id']);
				$mixedJobs->push($job);
			}
			
		
			$alliances = Alliance::where('status', 1)
							->orderBy('created_at', 'desc')
							->skip(0)->take(3)->get();
			
			$allianceURL = Helper::getUrlName('alliancePage');
			foreach( $alliances as $alliance)
			{
				$alliance['job_location'] = helper::getJobAddress($alliance);
				$alliance['url'] = str_replace('?fr=kirei', '', $alliance['url']);
				$alliance['job_career'] = helper::getAllianceJobCategoryNameByListId($alliance['list_job']);
				$alliance['job_url'] = '/' . $allianceURL . '/' . $alliance['id'];
				$mixedJobs->push($alliance);
			}
			$mixedJobs = $mixedJobs->sortByDesc('created_at');
			$minutes = 5000;
			Cache::put('__top_new_jobs_crawler', $mixedJobs, $minutes);
			
			return $mixedJobs;
		}
    	
    }


    /*
        TODO: show 3 new secret jobs by create time
    */
    public static function top_secret_jobs()
    {
        $jobs = SecretJob::where('status', 1)
                        ->orderBy('created_at', 'desc')
                        ->skip(0)->take(3)->get();
        return $jobs;
    }

    /*
        TODO: show 5 new article content by create time
    */
    public static function new_article_content()
    {
        $newArticle = Articles::select('a.id',
                                    'a.image',
                                    'a.image_thumb',
                                    'a.image_sp',
                                    'a.image_sp_thumb',
                                    'a.title',
                                    'a.view',
                                    'a.created_at',
                                    'acc.category', 
                                    'acc.category_alias',
                                    'acc.class_name'
									  )
                            ->from('article_content as a')
                            ->where('a.status', 1)
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->orderBy('a.created_at', 'desc')
                            ->skip(0)->take(5)->get();

        return $newArticle;
    }

    /*
        TODO: show 5 popular article content by view
    */
    public static function popular_article_content()
    {
        $popularArticle = Articles::select('a.id',
                                        'a.image',
                                        'a.image_thumb',
                                        'a.image_sp',
                                        'a.image_sp_thumb',
                                        'a.title',
                                        'a.view',
                                        'a.created_at',
                                        'acc.category',
                                        'acc.class_name',
                                        'acc.category_alias')
                            ->from('article_content as a')
                            ->where('a.status', 1)
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->orderBy('a.view', 'desc')
                            ->skip(0)->take(5)->get();
        return $popularArticle;

    }

    /*
        TODO: show article content category
    */
    public static function load_category()
    {
        $cat = ArticleCategories::get();
        
        return $cat;
    }

    /*
        TODO: show 3(PC) or 1(SP) new article content by create time
    */
    public static function load_article_banner()
    {
        if(Util::isMobile())
            $limit = 1;
        else
            $limit = 3;
        $newArticle = Articles::select('a.id',
                                'a.image',
                                'a.image_thumb',
                                'a.title',
                                'acc.category',
                                'acc.class_name',
                                'acc.category_alias',
                                'acc.description as cat_description')
                            ->from('article_content as a')
                            ->where('a.status', 1)
                            ->leftJoin('article_content_category as acc', 'acc.id', '=', 'a.cat_id')
                            ->orderBy('a.created_at', 'desc')
                            ->skip(0)->take($limit)->get();
        return $newArticle;
    }

    /*
        TODO: show random 1 in 10 new article content by create time
    */
    public static function load_random_article_banner()
    {
        $newArticle = DB::select("select art.* 
                                from (select a.id,
                                        a.image_sp,
                                        a.image, 
                                        a.title, 
                                        acc.category,
                                        acc.class_name,
                                        acc.category_alias,
                                        acc.description as cat_description
                                        from article_content as a
                                        left join article_content_category as acc on acc.id = a.cat_id
                                        where a.status = 1
                                        order by a.created_at desc
                                        limit 10
                                        ) as art
                                order by rand()
                                limit 1
                            ");
        if($newArticle)
        {
            $newArticle = (array)$newArticle[0];
        }
        return $newArticle;
    }
	
	public static function load_history_search()
	{
		return \Cookie::get('history_search');
	}
	
	public static function get_latest_contents($limit=5)
	{
		//TODO: youshouldwritesomethinghere
		$article = new Articles();
		$rs = $article->getLatestContents($limit);
		return $rs;
	}

    public static function get_similar_secret_jobs($companyId = null, $jobId)
    {
        $detail = SecretJob::select(
        //secrec_job
            's.id',
            's.job_name',
            's.list_job',

            // v2 update
            's.store_name',

            //company
            'c.name as company_name'
        )
            ->from('secret_job as s');
        $detail = SecretJob::invaliddate($detail);
        $detail = $detail->where('s.company_id', $companyId)
            ->where('s.id', '!=', $jobId)
            ->where('s.status', 1)
            ->leftJoin('company as c', 's.company_id', '=', 'c.id')
            ->get()->toArray();

        return $detail;
    }


    public static function get_related_allliance_by_priority($listJob, $listEm, $listProvince, $allianceId)
    {
        $alliances = Alliance::select(
            'id',
            'job_name',
            'list_job',
            'list_job_parent',
            'list_province',
            'list_city',
            'job_location',
            'job_salary',
            'job_image',
            'job_thumbnail'
        )
        ->where('list_job', $listJob)
        ->where('list_em', $listEm)
        ->where('list_province', $listProvince)
        ->where('id', '<>', $allianceId)
        ->orderBy('priority', 'desc')
        ->skip(0)->take(3)->get()->toArray();
        
        return $alliances;
    }

    public static function get_related_allliance_by_company_or_shop($companyName, $shopId, $allianceId)
    {
        $alliances = Alliance::select(
            'id',
            'job_name',
            'list_job',
            'list_job_parent',
            'list_province',
            'list_city',
            'job_location',
            'job_salary',
            'job_image',
            'job_thumbnail'
        )
        ->where('company_name', $companyName)
        ->where('shop_id', $shopId)
        ->where('id', '<>', $allianceId)
        ->orderBy('created_at', 'desc')
        ->skip(0)->take(3)->get()->toArray();
        
        return $alliances;
    }
    
}
?>