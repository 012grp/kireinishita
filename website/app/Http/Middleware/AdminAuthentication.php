<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use View;

class AdminAuthentication
{
    /**
     * This Middleware for Admin only
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('admin') != 1 
           && $request->path() != 'admin'
           && $request->path() != 'admin/login')
        {
            return redirect('/admin');
        }
        
        else
        {
            if(Session::has('admin') == 1 
            && (
                $request->path() == 'admin'
                || $request->path() == 'admin/login'
            ))
            {
                return redirect('/admin/home');
            }
        }
        
        View::share('admin', Session::get('admin'));
        return $next($request);
    }
}
