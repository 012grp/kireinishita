<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email'            => 'required|email',
            'password'            => 'required|alpha_num|min:6|max:8',
        ];

        return $rules;
    }


    public function messages()
    {
        return [
            'email.required'  => 'メールアドレスを入力して下さい',
            'email.email' => 'メールアドレスの入力に誤りがあります。',
            'password.required'     => 'パスワードを入力してください。',
            'password.alpha_num' => '半角英数で入力して下さい',
            'password.min' => '6桁以上で入力して下さい',
            'password.max' => '8桁以下で入力して下さい',
        ];
    }
}
