<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->request->get('kind') == 1)
        {
            $rules = [
                'kind'            => 'required|integer|between:1,2',
                'name'            => 'required|max:100',
                'mail'            => 'required|email',
                'content'         => 'required|max:2000',
            ];
        }
        else
        {
             $rules = [
                'kind'            => 'required|integer|between:1,2',
                'company'         => 'required|max:300',
                'name'            => 'required|max:100',
                'mail'            => 'required|email',
                'content'         => 'required|max:2000',
            ];
        }
        

        return $rules;
    }


    public function messages()
    {
        return [
            'company.required'  => '会社名を入力してください。',
            'name.required'     => 'お名前を入力してください。',
            'mail.required'     => 'メールアドレスを入力してください。',
            'mail.email'        => 'メールアドレスの入力に誤りがあります。',
            'content.required'  => 'お問い合わせ内容を入力してください。',
        ];
    }
}
