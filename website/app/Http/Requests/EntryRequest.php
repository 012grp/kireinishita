<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

//component
use App\Http\Components\EntryComponent;

class EntryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //set session
        EntryComponent::setSessionEntry($this);

        $strKatakana = "(\xe3\x82[\xa1-\xbf]|\xe3\x83[\x80-\xbe]|\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc]|\x83[\x40-\x96]|\x81[\x52\x53\x5b])";
        $regexForCheckingKatakana = "/^($strKatakana+( $strKatakana+|　$strKatakana+)*)+$/";
        
        $rules = [
            'name'            => 'required|max:100',
            'kana'            => ['required','max:100','Regex:'.$regexForCheckingKatakana,],
            'gender'          => 'required|integer|between:1,2',
            'year'            => 'required|integer',
            'month'           => 'required|integer',
            'day'             => 'required|integer',
            'email'           => 'required|email',
            'province'        => 'required|integer',
            'city'            => 'required|integer',
            // 'tel'             => 'required|digits:11',
        ];

        return $rules;
    }


    public function messages()
    {
        return [
            'name.required'     => 'お名前を入力してください。',
            'kana.required'     => 'お名前を入力してください。',
            'kana.regex'        => 'カタカナで入力してください。',
            'email.required'    => 'メールアドレスを入力してください。',
            'email.email'       => 'メールアドレスの入力に誤りがあります。',
            // 'tel.digits'        => '携帯電話番号は半角数字11桁で入力してください。',
        ];
    }
}
