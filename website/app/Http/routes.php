<?php

//Admin
Route::group([
		'namespace'=>'Admin',
		'middleware' => 'auth.admin'
	]
	, function(){
		//login
		Route::get('admin', 'AdminController@index');
		Route::post('admin/login', 'AdminController@login');

		Route::get('admin/logout', 'AdminController@logout');
		
		
		
		//dashboard
		Route::get('admin/home', 'AdminController@dashboard');
		
		Route::controller('admin/user',  'UserController');
		
		//base_url_manager
//		Route::get('admin/website-manager', 'WebsiteController@index');
		
		//list keyword manager
		Route::controller('admin/list-keyword', 'ListKeywordFilterController');
		
		//jobs
		Route::get('admin/job-manager', 'JobController@index');

		//keywords
		//Route::get('admin/keyword-manager', 'KeywordController@index');

		//config
		Route::get('admin/config-manager', 'ConfigController@index');

		//Email
		Route::get('admin/email-manager', 'EmailController@index');

		//Article
		Route::get('admin/article-manager', 'ArticleController@index');
		Route::get('admin/article-manager/create', 'ArticleController@getCreate');
		Route::post('admin/article-manager/create', 'ArticleController@postCreate');
		Route::get('admin/article-manager/delete/{id}', 'ArticleController@delete');
		Route::get('admin/article-manager/edit/{id}', 'ArticleController@edit');
		Route::post('admin/article-manager/update/{id}', 'ArticleController@update');
		Route::post('admin/article-manager/preview', 'ArticleController@preview');
		Route::get('admin/article-manager/preview', function(){
			return redirect("/");
		});

		//secret job
		Route::get('admin/secret-job-manager', 'SecretJobController@index');
		Route::get('admin/secret-job-manager/create', 'SecretJobController@getCreate');
		Route::post('admin/secret-job-manager/create', 'SecretJobController@postCreate');
		Route::get('admin/secret-job-manager/delete/{id}', 'SecretJobController@delete');
		Route::get('admin/secret-job-manager/edit/{id}', 'SecretJobController@edit');
		Route::post('admin/secret-job-manager/update/{id}', 'SecretJobController@update');
		Route::post('admin/secret-job-manager/preview', 'SecretJobController@preview');
		Route::get('admin/secret-job-manager/preview', function(){
			return redirect("/");
		});
		//Company
		Route::get('admin/company-manager', 'CompanyController@index');
		Route::get('admin/company-manager/create', 'CompanyController@getCreate');
		Route::post('admin/company-manager/create', 'CompanyController@postCreate');
		Route::get('admin/company-manager/delete/{id}', 'CompanyController@delete');
		Route::get('admin/company-manager/edit/{id}', 'CompanyController@edit');
		Route::post('admin/company-manager/update/{id}', 'CompanyController@update');
		Route::post('admin/company-manager/preview', 'CompanyController@preview');
		Route::get('admin/company-manager/preview', function(){
			return redirect("/");
		});

		
		Route::get('admin/clear-cache', 'ClearCacheController@index');

		Route::post('admin/upload', ['as' => 'admin.upload', 'uses' => 'AdminController@upload']);
		Route::post('admin/delete_image', ['as' => 'admin.delete_image', 'uses' => 'SecretJobController@delete_image']);

		//site map
		Route::get('admin/site-map', 'SiteMapController@index');
		Route::get('admin/site-map/renew', 'SiteMapController@renew');
		Route::post('admin/site-map/create', 'SiteMapController@create');
		Route::get('admin/site-map/create', function(){
			return redirect("/admin/site-map");
		});
	
		//alliance jobs
		Route::controller('admin/alliance-manager', 'AllianceController');
        //category
		Route::controller('admin/category-manager', 'CategoryController');
	});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
//WARNING: All SESSION won't work by ROUTES (just CONTROLLER)
//View GLOBAL VARIABLEs in \app\Http\Controller\Controller.php::__construct()
Route::get('/', function () {
    return view('welcome');
});
*/

//
//proxy
Route::get('proxy', function(){	
	
	//Check isset of URL
	if (!Request::input('url'))
	{
		die('empty url');
	}


	$url = Request::input('url');
	if(strpos($url,'//') === 0)
	{
		$url = 'http:'.$url;
	}
//	else
//	{
//		if(strpos($url,'img.') === 0)
//		{
//			$url = 'http://'.$url;
//		}
//	}
//
//	//Check invalid URL
//	if (filter_var($url, FILTER_VALIDATE_URL) === false)
//	{
//		die('invalid url');
//	}
	
	//GET IMAGEs
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$result = curl_exec($ch);
	$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
	//$ctype  = "image/png";
	//header('Content-type: ' . $contentType);
	curl_close($ch);
   	$response = Response::make( $result , 200 );
    $response->header('Content-Type', $contentType);
    return $response;
});

//SITE MAP
Route::get('/sitemap.xml', 'PagesController@sitemap');


//search
Route::get('/', 'PagesController@top');
Route::get('search-demo', 'PagesController@searchDemo');
//Route::get('search', 'PagesController@search');


//Need to remove after this link is on ready
Route::get('contents/chopsticks', 'CommingsoonController@index');
Route::get('contents/community', 'CommingsoonController@index');

//contents
Route::get('contents', 'ContentsController@index');
Route::get('contents/{cat_name}', 'ContentsController@category');
Route::get('contents/{cat_name}/{id}', 'ContentsController@detail');
Route::get('contents-test/{cat_name}/{id}', 'ContentsController@detail_test');

//contact
Route::get('contact', 'ContactController@index');
Route::any('contact/confirm', 'ContactController@confirm');
Route::any('contact/thanks', 'ContactController@thanks');

//account
Route::get('account', function(){
	return view('login.index');
});
Route::post('account', 'AuthenticationController@login');
Route::any('account/login-or-create', 'AuthenticationController@loginOrSignup');
Route::post('account/register', 'AuthenticationController@register');
Route::get('account/logout', 'AuthenticationController@logout');
Route::any('account/forget-password', 'AuthenticationController@forgotPassword');
Route::get('account/reset-password', 'AuthenticationController@reset_password');
Route::post('account/set-new-password', 'AuthenticationController@set_new_password');

//account for ajax
Route::get('account/ajax-login', 'AuthenticationController@ajaxLogin');
Route::get('account/ajax-register', 'AuthenticationController@ajaxRegister');
Route::get('account/ajax-logout', 'AuthenticationController@ajaxLogout');
Route::get('account/ajax-forgot-password', 'AuthenticationController@ajaxForgotPassword');

//Redirect to facebook to authenticate
Route::get('account/facebook-redirect', 'AuthenticationController@facebook_redirect');
//Get back to redirect url
Route::get('account/facebook', 'AuthenticationController@facebook');

//Redirect to twitter to authenticate
Route::get('account/twitter-redirect', 'AuthenticationController@twitter_redirect');
//Get back to redirect url
Route::get('account/twitter', 'AuthenticationController@twitter');

//account bookmark
Route::get('account/bookmark', 'AuthenticationController@bookmarkJob');
Route::get('account/check-bookmark', 'AuthenticationController@checkBookmark');
Route::get('account/check-login-to-load-bookmark', 'AuthenticationController@checkLoginToLoadBookmark');

//test send mail
Route::get('test-send-mail', 'AuthenticationController@testSendMail');

//entry
Route::post('entry/confirm', 'EntryController@confirm');

Route::get('entry/confirm', function(){
	return redirect('/');
});

Route::any('entry/thanks', 'EntryController@thanks');

Route::get('entry', function(){
	return redirect('/errors/404');
});

Route::get('entry/{secret_id}', 'EntryController@index');



//concept
Route::get('concept', function(){
	if(App\Http\Components\Util::isMobile())
	{
		return view('concept.index_sp');
	}
	else
	{
		return view('concept.index');
	}
});

//404
Route::get('404', function(){
	return view('errors.404');
});

//about
Route::get('about/company', 'AboutController@company');
Route::get('about/privacypolicy', 'AboutController@privacypolicy');

//mybox
Route::get('mybox', 'MyBoxController@index');


//secret
Route::get('limited_kyujin', 'SecretController@index');
Route::get('limited_kyujin/{id}', 'SecretController@detail');
Route::get('limited_kyujin/share/{id}', 'SecretController@share');

//secret
Route::get('osusume_kyujin', 'AllianceController@index');
Route::get('osusume_kyujin/{id}', 'AllianceController@detail');
Route::get('osusume_kyujin/share/{id}', 'AllianceController@share');
Route::get('osusume_kyujin/ref/{job_id}', 'AllianceController@reference');

//area
Route::controller('area',  'AreaController');


Route::get('job/search', 'SearchController@search');
Route::get('job/api', 'SearchController@api');
Route::get('job/result', 'SearchController@result');
Route::get('job/result/{alias}', 'SearchController@result_advance');
Route::get('job/ajax-count-search', 'SearchController@ajaxCountSearch');

//job redirect
Route::get('job/{job_id}', 'SearchController@reference')->where('job_id', '[0-9]+');

//feature: 正社員,契約社員,業務委託,..
Route::get('feature/{feature}', 'JobFilterController@feature');

//for all category level 1
$cat1 = 'biyo'; //planning
$cat2 = 'relaxation'; //chouri
$cat3 = 'chiryo'; //service
Route::get($cat1, 'JobFilterController@planning');
Route::get($cat2, 'JobFilterController@chouri');
Route::get($cat3, 'JobFilterController@service');

Route::get('{cat_name}', 'JobFilterController@categoryLv2');
Route::get('{cat_name}/feature/{feature}', 'JobFilterController@categoryLv2');
Route::get('{cat_name}/area/{province}', 'JobFilterController@province');
Route::get('{cat_name}/area/{province}/city/{city}', 'JobFilterController@city');
Route::get('{cat_name}/area/{province}/feature/{feature}', 'JobFilterController@province');
Route::get('{cat_name}/area/{province}/city/{city}/feature/{feature}', 'JobFilterController@city');

//for all category from level 2
//ex:シェフ・キッチン,北海道
//Route::get($cat1.'/{cat_name}', 'JobFilterController@categoryLv2');
//Route::get($cat1.'/{cat_name}/area/{province}', 'JobFilterController@province');
//Route::get($cat1.'/{cat_name}/area/{province}/city/{city}', 'JobFilterController@city');

//Route::get($cat2.'/{cat_name}', 'JobFilterController@categoryLv2');
//Route::get($cat2.'/{cat_name}/area/{province}', 'JobFilterController@province');
//Route::get($cat2.'/{cat_name}/area/{province}/city/{city}', 'JobFilterController@city');
//
//Route::get($cat3.'/{cat_name}', 'JobFilterController@categoryLv2');
//Route::get($cat3.'/{cat_name}/area/{province}', 'JobFilterController@province');
//Route::get($cat3.'/{cat_name}/area/{province}/city/{city}', 'JobFilterController@city');
//
////for all category from level 2 combine feature
//Route::get($cat1.'/{cat_name}/feature/{feature}', 'JobFilterController@categoryLv2');
//Route::get($cat1.'/{cat_name}/area/{province}/feature/{feature}', 'JobFilterController@province');
//Route::get($cat1.'/{cat_name}/area/{province}/city/{city}/feature/{feature}', 'JobFilterController@city');
//
//Route::get($cat2.'/{cat_name}/feature/{feature}', 'JobFilterController@categoryLv2');
//Route::get($cat2.'/{cat_name}/area/{province}/feature/{feature}', 'JobFilterController@province');
//Route::get($cat2.'/{cat_name}/area/{province}/city/{city}/feature/{feature}', 'JobFilterController@city');
//
//Route::get($cat3.'/{cat_name}/feature/{feature}', 'JobFilterController@categoryLv2');
//Route::get($cat3.'/{cat_name}/area/{province}/feature/{feature}', 'JobFilterController@province');
//Route::get($cat3.'/{cat_name}/area/{province}/city/{city}/feature/{feature}', 'JobFilterController@city');


Route::controller('train-api',  'TrainApiController');

Route::get('auto-create-sitemap/{key}', 'SiteMapController@createSiteMap');


///test
Route::get('test/mix-search/{xTotal}/{yTotal}/{zTotal}/{xDisplay}/{yDisplay}/{zDisplay}/{currentPage}', 'TestController@MixSearch');

//
//Route::get('{root_1}/{root_2?}/{root_3?}', function($root_1='', $root_2=null, $root_3=null){
//	$prefix = '';
//	if($root_3 != null)
//	{
//		if (view()->exists($prefix."$root_1.$root_2.$root_3")) {
//    		return view($prefix."$root_1.$root_2.$root_3");
//		}
//	}
//	else
//	{
//		if($root_2 != null)
//		{
//			if (view()->exists( $prefix."$root_1.$root_2")) {
//    			return view($prefix."$root_1.$root_2");
//			}
//		}
//		else
//		{
//			if($root_1 != '')
//			{
//				if (view()->exists($prefix."$root_1.index")) {
//    				return view($prefix."$root_1.index");
//				}
//			}
//		}
//	}
//	
//	return view('errors.404');
//});

//Route::get('/building', 'PagesController@building');
//Route::get('/aboutme', 'PagesController@aboutme');
//Route::get('/contact', 'PagesController@contact');
//
////SITE MAP
//Route::get('/sitemap.xml', 'PagesController@sitemap');
//
//

//

//
////Redirect to google to authenticate
//Route::get('google', 'AuthenticationController@google_redirect');
////Get back to redirect url
//Route::get('account/google', 'AuthenticationController@google');
//
////User
////Route::match(array('GET', 'POST'), 'account/email','AuthenticationController@email');
//Route::post('account/email', 'AuthenticationController@email');
//Route::post('account/register', 'AuthenticationController@register');
//Route::get('account/logout', 'AuthenticationController@forget');
//Route::get('account/me', 'UserController@me');
//Route::get('account/login', 'UserController@login');
//Route::get('account/bookmark', 'UserController@bookmark');
//Route::post('account/edit', 'UserController@edit');
//Route::post('account/changepassword', 'UserController@changepassword');
//
////url
//Route::get('job/{url}', 'UrlsController@index');
//
////Bookmark
//Route::get('bookmark/mark', 'UserController@mark');
//Route::get('bookmark/unmark', 'UserController@unmark');
//Route::get('bookmark/unmarkfind', 'UserController@unmarkfind');
//
////search result
//Route::get('/content', 'ContentsController@index');
//Route::get('/content/{keyword}','ContentsController@index');
//Route::get('/content/{keyword}/{subkeyword}','ContentsController@index');
//Route::post('/jsoncontent', 'ContentsController@jsoncontent');
//


////Admin: ( set admin.xuongxuong.com 127.0.0.1 in file host )
//Route::group(['domain' => 'admin.xuongxuong.com'], function () {
//    Route::get('/', function(){
//        //TODO
//        return Redirect::to('http://www.google.com');
//    });
//    Route::get('admin', 'Admin\AdminController@index');
//});

//catch all of rest
//Route::any('{anything}', function(){
//    return redirect('/');
//})->where('anything', '.*');

