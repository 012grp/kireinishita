<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    protected $table = 'job_category';
    //protected $fillable = array('keyword', 'count');
	
	public function getJobCategoryLevel1()
	{
		//TODO: getProvince
		return $this->select(['id','name','alias', 'parent_id','style'])
					->where('level',1)
					->where('style','!=',3)
					->where('status',1)
					->where('display_on_top',1)
					->orderBy('style')
					->orderBy('parent_id')
					->orderBy('id')
					->get();	
	}

	public function getJobCategory()
	{
		//TODO: getProvince
		return $this->select(['id','name','alias', 'parent_id','style'])
					->where('style','!=',3)
					->where('status',1)
					->orderBy('style')
					->orderBy('parent_id')
					->orderBy('id')
					->get();	
	}
	
	public function getEmployment()
	{
		//TODO: getEmployment
		return $this->select(['id','name','alias', 'parent_id','style'])
					->where('level',1)
					->where('style','=',3)
					->where('status',1)
					->orderBy('id')
					->get();
	}
	
	public function getByAlias($alias, $arrStyle = array())
	{
		$cat = $this->select(['id','name','alias', 'parent_id','job_amount','style'])
					->where('alias',$alias)
					->where('status',1);
		
		if($arrStyle)
		{
			$cat = $cat->where(function($query) use($arrStyle){
				foreach( $arrStyle as $style)
				{
					$query->orWhere('style',$style);
				}
			});
		}
	
		return $cat->first();
	}
	
	public function getById($id)
	{
		return $this->select(['id','name','alias', 'parent_id','job_amount','style'])
					->where('id',$id)
					->where('status',1)
					->first();
	}

	public function getOnlyJobCategory()
	{
		//TODO: get only job cate gory (style = 1)
		return $this->select(['id','name','alias', 'parent_id','style'])
					->where('style','=', 1)
					->where('status',1)
					->orderBy('id')
					->get();	
	}
	public function getOnlyHangup()
	{
		//TODO: get only job cate gory (style = 1)
		return $this->select(['id','name','alias', 'parent_id','style'])
					->where('style','=', 2)
					->where('status',1)
					->orderBy('id')
					->get();	
	}

    public function getJobCategoryByParent($parentId)
    {
        //TODO: getProvince
        return $this->select(['id','name','alias', 'parent_id','style'])
            ->where('style','!=',3)
            ->where('status',1)
            ->where('parent_id',$parentId)
            ->orderBy('id')
            ->get();
    }

    public function getJobCategoryByStyle($style,$onTop =1,$status = 1)
    {
        $result= $this->select(['id','name','alias', 'parent_id','style'])
            ->where('level',1)
            ->where('style',$style)
            ->orderBy('style')
            ->orderBy('parent_id')
            ->orderBy('id');

        $result =  $onTop!= 1 ? $result : $result->where('display_on_top',$onTop);
        $result =  $status!= 1 ? $result : $result->where('status',$status);
        return $result;
    }

    public function getParentCategoryByStyle($style,$onTop =1,$status = 1)
    {
        $result= $this->select(['id','name','alias', 'parent_id','style'])
            ->where('level',0)
            ->where('parent_id',0)
            ->where('style',$style)
            ->orderBy('style')
            ->orderBy('parent_id')
            ->orderBy('id');
        $result =  $onTop!= 1 ? $result : $result->where('display_on_top',$onTop);
        $result =  $status!= 1 ? $result : $result->where('status',$status);
        return $result->get();
    }
}
