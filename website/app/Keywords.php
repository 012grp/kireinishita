<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keywords extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    //protected $fillable = array('keyword', 'count');
}
