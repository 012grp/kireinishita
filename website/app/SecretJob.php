<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Carbon\Carbon;
use App\Http\Components\Search;

class SecretJob extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    protected $table = 'secret_job';
    public $timestamps = true;


    public $selectFields = array(
			'secret_job.id',
			'secret_job.job_career',
			'secret_job.job_name',
			'secret_job.job_salary',
			'secret_job.job_location',
			'secret_job.job_image',
			'secret_job.job_type',
			'secret_job.job_career',
			'secret_job.is_new',
			'secret_job.site_id',
			'secret_job.list_job',
			'secret_job.list_job_parent',
			'secret_job.list_hangup',
			'secret_job.list_em',
			'secret_job.store_name',
			'c.name as job_company',
            'c.celebration as celebration'
	);
    
	public function getRelated($id, $company_id, $limit)
	{
		$result = 
			 $this->select('id','company_id','job_name')
			 ->where('company_id',$company_id)
			 ->where('id','!=', $id)
			 ->orderBy('created_at', 'desc')
			 ->skip(0)
			 ->take($limit)
			 ->get();
		
		return $result;
	}

	public function getStringCompany($id)
	{
		$resultCompany = $this->select('c.address', 'c.access', 'c.map_location')
					->from('secret_job as s')
					->where('s.id', $id)
					->leftJoin('company as c', 'c.id', '=', 's.company_id')
					->get()->toArray();
		$resultCompany = $resultCompany[0];
		$stringCompany = $resultCompany['address'] . ' ' .  $resultCompany['access'] . ' ' . $resultCompany['map_location'];

		return $stringCompany;
	}

	public function updateListWaySite($id, $string='')
	{
		$result = DB::select( 
						DB::raw("select concat(',',group_concat(ws.line_cd separator ','),',') as listWaySite	
								from (select distinct waysite.line_cd, waysite.keywords from waysite) ws 
								where '$string' like concat('%',ws.`keywords`,'%')") 
						);
		if($result)
		{
			$result = $result[0]->listWaySite;
			$this->where('id', $id)
				->update(['list_waysite' => $result]);
		}
	}

	public function updateListProvince($id, $string='')
	{
		$result = DB::select( 
						DB::raw("select concat(',',group_concat(area.id separator ','),',') as listProvince
							from area 
							where '$string' like concat('%',area.`keywords`,'%') 
							and level = 0") 
						);
		
		if($result)
		{
			$result = $result[0]->listProvince;
			$this->where('id', $id)
				->update(['list_province' => $result]);
		}
	}

	public function updateListCity($id, $string='')
	{
		$result = DB::select( 
						DB::raw("select concat(',',group_concat(area.id separator ','),',') as listCity
							from area 
							where '$string' like concat('%',area.`keywords`,'%') 
							and level = 1") 
						);

		$result = $result[0]->listCity;

		$resultParent = DB::select( 
						DB::raw("select concat(',',group_concat(area.parent_id separator ','),',') as listParent
							from area 
							where '$string' like concat('%',area.`keywords`,'%') 
							and level = 1") 
						);
		if($resultParent)
		{
			$resultParent = $resultParent[0]->listParent;
		}
		else
		{
			$resultParent = '';
		}

		$resultProvince = $this->select('list_province')
							->where('id', $id)
							->get()->toArray();
		if($resultProvince)
		{
			$resultProvince = $resultProvince[0]['list_province'];
		}
		else
		{
			$resultProvince = '';
		}

		
		
		$arrProvince = explode(',', trim($resultProvince, ','));
		$arrParent = explode(',', trim($resultParent, ','));

		$arrResult = array_merge($arrProvince, $arrParent);
		$arrResult = array_unique($arrResult);
		
		if($arrResult)
		{
			$listProvince = implode(",", $arrResult);
			$listProvince = trim($listProvince, ',');
			$listProvince = ','.$listProvince.',';
			$this->where('id', $id)
				->update(['list_province' => $listProvince]);
		}
		else
		{
			$listProvince = null;
		}

		if($result)
		{
			$this->where('id', $id)
				->update(['list_city' => $result]);
		}
	}

	public function getDetail($id, $checkStatus = true)
	{
		$data = $this->select(
        	//secrec_job
            's.id',
            's.job_name',
            's.job_detail',
            's.job_career',
            's.job_salary',
            's.job_location',
            's.job_image',
            's.job_type',
            's.site_id',
            's.created_at as job_created_at',
            's.company_id', 's.content',
            's.job_notice_header',
            's.job_notice_content',
            's.job_notice_image',
            's.job_notice_image_sp',
            's.job_category',
            's.employment_status',
            's.job_description',
            's.job_salary_detail',
            's.job_qualification',
            's.job_location_detail',
            's.treatment_welfare',
            's.job_holiday',
            's.access',
            's.map_location',
            's.lng',
            's.lat',
            's.job_tag',
            's.list_em',
            's.list_hangup',
            's.list_job',
            's.list_job_parent',
            's.meta_desc',
            's.meta_keyword',
            's.list_province',

        	// v2 update
            's.store_name',
            's.start_at',
            's.end_at',

            's.job_personnel',
            's.job_appSelection',
            's.job_url',
            's.job_genre',
            's.shopStyle',
            's.business_hrs',
            's.reg_holiday',
            's.customer_base',
            's.job_atmosphere',
            's.job_education',
            's.job_challenges',
            's.male_female_ratio',
            's.job_url',

            //company
            'c.name',
            'c.logo',
            'c.category',
            'c.celebration',
            'c.average_price',
            'c.website',
            'c.address',
            'c.access_manual',
            'c.seat_number',
            'c.holiday',
            'c.hp',
            'c.short_introduction',
            'c.introduction',
            'c.access as company_access',
            'c.lat as company_lat',
            'c.lng as company_lng',
            'c.map_location as company_map_location',
            'c.representative',
            'c.work_content'
        )
		->from('secret_job as s')
		->where('s.id', $id)
		->leftJoin('company as c', 's.company_id', '=', 'c.id');
		
		$date = Carbon::now();
		
		if($checkStatus)
		{
            $data = $data->where('s.status', 1);
			$data = self::invaliddate($data);
		}
        $data = $data->get()->toArray();
		return $data;
	}


	public static function invaliddate($request){
		return $request->whereRaw('
			CASE WHEN start_at = "0000-00-00 00:00:00" AND end_at = "0000-00-00 00:00:00" THEN 
				date(end_at)<=date(now())

			WHEN end_at = "0000-00-00 00:00:00" THEN
			   date(start_at)<= date(now())  

			ELSE
				date(start_at)<= date(now()) and date(end_at)>=date(now())
			END
	    	');
	    //return $request->whereRaw('date(start_at)<= date(now()) and date(end_at)>=date(now())');
    }






    public function searchRandom($request, $paginate)
	{
		return $this->searchQuery($request)
					->orderByRaw("RAND()")
					->take($paginate)
					->get();
	}


	public function getSecretData($request, $fromIndex, $toIndex, $randomNumber)
	{
		
		if($fromIndex == 0 && $toIndex == 0)
		{
			return [];
		}
		
		return $this->searchQuery($request)
					->orderByRaw("RAND($randomNumber)")
					->skip($fromIndex)
					->take($toIndex)
					->get();
	}

	public function getSecretDataByFilter($conditions, $fromIndex, $toIndex, $randomNumber)
	{
		/*
		 * $toIndex => 0 => return []
		 * $toIndex => 1 => $fromIndex => 0 => return []
		 * $toIndex => 1 => $fromIndex => 1 => ...
		 * */
		if($toIndex)
		{
			if($fromIndex > -1){
				$rs = $this->filter($conditions)
									->orderByRaw("RAND($randomNumber)")
									->skip($fromIndex)
									->take($toIndex)
									->get();
				return $rs;
			}else{
				return [];
			}

		}else{
			return [];
		}



	}
	
	
	public function filter($conditions)
	{
		//TODO: filter
		
		/*
			is_secret_job
			0: no
			1: yes
		*/
		$select = $this->selectFields;
		$select[] = \DB::raw('1 as is_secret');
		
		$contents = $this::select($select)->where('secret_job.status','=','1');
		
		$contents = $this::select($select)
					->leftJoin('company as c', 'secret_job.company_id', '=', 'c.id')
					->where('secret_job.status','=','1')
					->whereRaw('CASE WHEN secret_job.start_at = "0000-00-00 00:00:00" AND secret_job.end_at = "0000-00-00 00:00:00" THEN 
							date(secret_job.end_at)<=date(now())
						WHEN secret_job.end_at = "0000-00-00 00:00:00" THEN
						   date(secret_job.start_at)<= date(now())  
						ELSE
							date(secret_job.start_at)<= date(now()) and date(secret_job.end_at)>=date(now())
						END');



		foreach( $conditions as $key=>$val)
		{
			$val = '%,'.$val.',%';
			$contents = $contents->where('secret_job.'.$key, 'like', $val);
		}
		// $contents = $contents
		// 			->orderByRaw("RAND()")
		// 			->take($paginate)
		// 			->get();
		return $contents;
	}

	public function countOfFilter($conditions)
	{	
		//Stop Secret Job at 2017-03-14
		//return 0;
		return $this->filter($conditions)
					->count();
	}


	/*
		For count only
	*/
	public function countOfSearch($request)
	{
		//Stop Secret Job at 2017-03-14
		//return 0;
		return $this->searchQuery($request)
					->count();
	}
	
	
	public function searchQuery($request)
	{
		//TODO: search
		
		$keyword 		= $request->input('k');
		$pro 			= $request->input('pro');
		$hangup 		= $request->input('hang');
		$jobCategory 	= $request->input('cat');
		$employment 	= $request->input('em');
		$city 			= $request->input('city');
		$way 			= $request->input('way');
		$station 		= $request->input('station');
		$site_id 		= $request->input('site_id');
		$no_image 		= $request->input('no_image');
		
		
        //SET current page
        /*if(isset($pars['page']) && $pars['page'] > 1)
        {
            $page = $pars['page'];
            Paginator::currentPageResolver(function() use ($page) {
                return $page;
            });
        }*/
        
		//#Enable show query log, in version 5, false is default value of QueryLog
		//DB::enableQueryLog();
		
		/*
			is_secret_job
			0: no
			1: yes
		*/
		$select = $this->selectFields;
		$select[] = \DB::raw('1 as is_secret');
		            		
		$contents = $this::select($select)
					->leftJoin('company as c', 'secret_job.company_id', '=', 'c.id')
					->where('secret_job.status','=','1')
					->whereRaw('CASE WHEN secret_job.start_at = "0000-00-00 00:00:00" AND secret_job.end_at = "0000-00-00 00:00:00" THEN 
							date(secret_job.end_at)<=date(now())
						WHEN secret_job.end_at = "0000-00-00 00:00:00" THEN
						   date(secret_job.start_at)<= date(now())  
						ELSE
							date(secret_job.start_at)<= date(now()) and date(secret_job.end_at)>=date(now())
						END');
		
		//Find jobs without IMAGE
		if(!empty($no_image))
		{
			$contents = $contents->where('secret_job.job_image', '');
		}
		
		//Search by site_id
//		if(!empty($site_id))
//		{
//			$contents = $contents->where('secret.site_id', $site_id);
//		}
		
		//Search by keyword
		if(!empty($keyword))
		{
			//Advance keyword
			$searchComponent = new Search();
			$keywords = $searchComponent->makeKeyWord($keyword);
			
			foreach($keywords as $key)
			{
				$key = '%'.$key.'%';
				//$contents = $contents->where('contents.content_utf8', 'like', $key);
				$contents = $contents->where(function($query) use($key){
					$query->orWhere('secret_job.job_name', 'like', $key);
					$query->orWhere('c.name', 'like', $key);
					$query->orWhere('secret_job.job_career', 'like', $key);
					$query->orWhere('secret_job.job_salary', 'like', $key);
					$query->orWhere('secret_job.job_location', 'like', $key);
				});
			}
		}
		
		
		
		//Search by province
		if(!empty($pro))
		{
			$pro = '%,'.$pro.',%';
			$contents = $contents->where('secret_job.list_province', 'like', $pro);
				
			//Search by Cities
			$contents = $this->conditionInListField($contents,'secret_job.list_city', $city);
			
			//Search by Waysites
			//Search by station
			$contents = $this->conditionInOrGroup($contents, array(
                'secret_job.list_waysite' => $way,
                'secret_job.list_station' => $station,
            ));
			
		}
		
		//Search by employment
		$contents = $this->conditionInListField($contents,'secret_job.list_em', $employment);
		
		//Search by job category
		$contents = $this->conditionInListField($contents,'secret_job.list_job', $jobCategory);
        
		//Search by hang up
		$contents = $this->conditionInListField($contents,'secret_job.list_hangup', $hangup);
		
        if (empty($station)) {
            $contents = $contents
                //->orderBy('contents.created_at','desc')
                ->orderBy('secret_job.priority', 'desc');
        } else {
            $oderString = $this->orderByStation($station);
            $contents = $contents
                ->orderByRaw($oderString)
                ->orderBy('secret_job.priority', 'desc') ;
        }
		return $contents;
	}
	
	private function conditionInListField($contents, $fieldName, $pars)
	{
		if(!empty($pars))
		{
			if(is_array($pars))
			{
				$pars = array_unique($pars);
				$contents = $contents->where(function ($query) use($pars, $fieldName){
								foreach( $pars as $value)
								{
									$value = '%,'.$value.',%';
									$query->orWhere($fieldName, 'like', $value);
								}
							});
			}
			else
			{
				$pars = '%,'.$pars.',%';
				$contents = $contents->where($fieldName, 'like', $pars);
			}
		}
		return $contents;
	}


	/*
        Input:
        array(
            'field_1' => array(1,2),
            'field_2' => '10',
            'field_3' => '',
            'field_4' => array()
        )
        ---
        Output:
        ... and ( field_1 like '%,1,%' or field_1 like '%,2,%' or field_2 like '%,10,%' ) ...

    */
    private function conditionInOrGroup($contents, $pars)
    {
        if (!empty($pars)) {
            $contents = $contents->where(function ($query) use ($pars) {
                foreach ($pars as $fieldName => $values) {
                    if (!empty($values)) {
                        if (is_array($values)) {
                            $values = array_unique($values);
                            foreach ($values as $value) {
                                $value = '%,' . $value . ',%';
                                $query->orWhere($fieldName, 'like', $value);
                            }

                        } else {
                            $values = '%,' . $values . ',%';
                            $query->orWhere($fieldName, 'like', $values);
                        }
                    }
                }
            });
        }
        return $contents;
    }

    private function orderByStation( $pars)
    {
        if (is_array($pars)) {
            $result = '(';
            $count = 0;
            $total = count($pars);
            foreach ($pars as $fieldName => $values) {
                $count++;
                $result .= $count == $total ? " secret_job.list_station like '%," . $values . ",%'" : " secret_job.list_station like '%," . $values . ",%' or";
            }
            $result .= ') desc';

        }

        return $result;
    }

}
