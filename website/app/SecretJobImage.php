<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecretJobImage extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    protected $table = 'secret_job_img';
    public $timestamps = true;
}
