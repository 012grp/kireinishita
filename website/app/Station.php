<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    protected $table = 'station';
    public $timestamps = true;

    public function getAllStation()
	{
		return $this->select(array('id', 'code', 'name'))
					->groupBy('code')
					->get();
	}
	
	public function getStationByIds($arrCode)
	{
		$arrCode = array_values($arrCode);
		$rs =  $this->select(array('id', 'code', 'name'))
					->whereIn('code', $arrCode)
					->groupBy('code')
					->get();
		return $rs;
	}
}
