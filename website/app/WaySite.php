<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaySite extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    protected $table = 'waysite';
    public $timestamps = true;
	
	public function getAllWaySite()
	{
		return $this->select(array('id', 'line_cd', 'name'))
					->groupBy('line_cd')
					->get();
	}
	
	public function getWaySitesByProvince($provinceId)
	{
		return $this->select(array('id', 'line_cd', 'name'))
					->where('parent_id', $provinceId)
					->groupBy('line_cd')
					->get();
	}
}
