<?php

    $path = 'http://';
    if(isset($_SERVER['HTTPS'])) 
    {
        $path = 'https://';
    }

    $twitter_test_mode = env('TWITTER_TEST_MODE', '');
    if(!$twitter_test_mode)
    {
        $twitter_client_id = env('TWITTER_CLIENT_ID', '');
        $twitter_client_secret = env('TWITTER_CLIENT_SECRET', '');
        $redirect = env('TWITTER_REDIRECT', '');
    }
    else
    {
        if($twitter_test_mode === 'binh')
        {
            $twitter_client_id = 's13lVbA49Sg5c4iZB0e2wjKMT';
            $twitter_client_secret = 'BMPMhT99SQRM1mOwHZZusQnOxKR7n3fUabQts22Oehyv9tRdTo';
            $redirect = 'https://i-eat.localhost/account/twitter';
        }
        else
        {
            $twitter_client_id = 'uXNEO9ZTjqwpXnHFNMRzkdvmS';
            $twitter_client_secret = '0b29yzZFUC4uDRnz0Yct2qlvhjHByQOWcLxboR5C437K9or8hs';
            $redirect = 'http://i-eat.xuongxuong.com:8080/account/twitter';
        }
    }

    if(!empty($_SERVER['SERVER_NAME'])){
        $redirectFb =  $path . $_SERVER['SERVER_NAME'].env('FACEBOOK_REDIRECT','');
    }else{
        $redirectFb =  $path . '/home/kireinishita/public_html/'.env('FACEBOOK_REDIRECT','');
    }




return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],

     'facebook' => [
         'client_id' => env('FACEBOOK_CLIENT_ID',''),
         'client_secret' => env('FACEBOOK_CLIENT_SECRET',''),
         'redirect' => $redirectFb,
     ],

//    'facebook' => [
//        'client_id' => '562683620586458',
//        'client_secret' => 'f8f96eef242b67c959791599fb687223',
//        'redirect' => 'http://i-eat.localhost/account/facebook',
//    ],

    
    'twitter' => [
        'client_id' => $twitter_client_id,
        'client_secret' => $twitter_client_secret,
        'redirect' => $redirect,
    ],
];
