<?php

	$config['myconfig']['list_status']  = array(
		1 => '面接予定',
		2 => '選考中',
		3 => '内定通知済',
		4 => '末返信',
		5 => '連絡中',
		6 => '不採用'
	);
