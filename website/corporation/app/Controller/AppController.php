<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    var $helpers = array('Html','Text','Session');
    var $uses    = array(
        'User',
    );

    public function appError($error) {
     // $this->redirect('/');
    }

    public function beforeFilter()
    {
        parent::beforeFilter();
      
        if(!$this->Session->check('User')){
          //$this->redirect('/login');
        }
//      echo "controller:".$this->params['controller']."<br>";
//      echo "action:".$this->params['action'];

    }

    function beforeRender() {

    }

  public function getSession() {
    return $this->Session;
  }

  public static $_salt = '*_%%!~DFDSD^%@!@&#@(&@!^(%*^*%^&$#@&#*@*!@#**()CVXNCCKX';
  //one day
  public function getShareLink($id,$day=1)
  {
    $time=86400;
    $expire = time() + ($day*$time);
    $hash = md5($id.$expire.self::$_salt);
    return "?e=$expire&h=$hash";
  }

  public function checkShareLink($id, $expire, $hash)
  {
    $_expire = time();
    if($expire < $_expire)
    {
      //time out
      return false;
    }

    $_hash = md5($id.$expire.self::$_salt);

    if($_hash != $hash)
    {
      //access deny
      return false;
    }

    return true;
  }


  public function setFlash($mess, $key = null, $type = 'default') {
    if($key) {
      $this->getSession()->setFlash($mess, $type, array(), $key);
    } else {
      $this->getSession()->setFlash($mess);
    }
  }

  public function send_email($config,$sender,$receipt,$subject,$message,$format,$template,$viewVars=array(), $bcc=null) {
    $Email = new CakeEmail($config);
    $Email->from($sender);
    $Email->to($receipt);

    $Email->emailFormat($format);
    $Email->subject($subject);
    $Email->template($template);
    $Email->viewVars($viewVars);
    $Email->CharSet = "UTF-8";
    if($bcc != '' && $bcc != null){
      $Email->bcc($bcc);
    }

    return $Email->send($message);
  }


  public function get_area_name($id){
    App::import("Model", "Area");
    $model = new Area();
    $data = $model->find('first',array(
      'fields' =>  array('Area.name'),
      'conditions' => array('Area.id' =>$id),
    ));

    return $data['Area']['name'];

  }

}
