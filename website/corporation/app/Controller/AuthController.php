<?php

App::uses('AppController', 'Controller');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class AuthController extends AppController
{
    public $uses = array('User');
    public $helpers = array('Html', 'Form', 'Js' => array('Jquery'));
    public $components = array('Session', 'ExtAuth', 'RequestHandler');

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
    }


    public function logout()
    {

        $this->Session->destroy();
        return $this->redirect($this->Auth->logout());

    }




    public function login()
    {

        $this->set('page_title_default','採用担当者様用ログイン');
        $this->layout = 'login';
        if($this->Session->check('User')){
           $this->redirect(array("controller" => "company", "action" => "job"));
        }

        $this->set('error',0);
        if ($this->request->is('post')) {
            $data = $this->User->find('first', array('conditions'=>array('email'=>$this->request->data['User']['email'], 'password'=>md5($this->request->data['User']['password']))));


            if(sizeof($data)){
                $this->Session->write('User',$data);
                $this->redirect("/job/".$data['User']['id']);
            }else{
                $this->set('error',1);
            }
            $this->set('error',1);


        }
    }

    public function timeout()
    {
        $this->set('page_title_default','Timeout');
        $this->layout = 'login';

    }

    public function forgot_pass_success()
    {
        $this->set('page_title_default','Send mail success');
        $this->layout = 'login';

    }

    public function reset_pass_success()
    {
        $this->set('page_title_default','Reset password success');
        $this->layout = 'login';

    }


    public function forgotpassword()
    {

        $this->set('page_title_default','パスワードを再設定する');
        $this->layout = 'login';
        $this->set('error',0);
        if ($this->request->is('post')) {

            $user = $this->User->find('first', array('conditions' => array('User.email' => $this->request->data['User']['email'])));

            if (!empty($user)) {

                $key = Security::hash(CakeText::uuid(), 'sha512', true);
                $hash = sha1($user['User']['email'] . rand(0, 100));
                $hash_expire = $this->getShareLink($user['User']['email'],1);

                $url = Router::url(array('controller' => 'auth', 'action' => 'resetpassword', $key . '' . $hash, '?' => $hash_expire), true);

                $user['User']['tokenhash'] = $key . '' . $hash;
                $this->User->id = $user['User']['id'];
                if ($this->User->saveField('tokenhash', $user['User']['tokenhash'])) {

                    $name = $user['User']['name'];
                    $contact_email = trim($user['User']['email']);
                    $subject = '【キレイにスルンダ】求人管理画面パスワード再設定のご連絡';
                    $message = '<a href="' . $url . '" target="_blank">Please click here</a>';

                    $AppController = new AppController;
                    $success = $AppController->send_email(
                      'gmail',
                      array(Configure::read('EMAIL') => Configure::read('EMAIL_NAME')),
                      $contact_email,
                      $subject,
                      $message = null,
                      $format = 'text',
                      $template = 'forgot_password',
                      array('fullname' => $contact_email, 'linkreset' => $url)
                    );
                    /*----------------------------------------------------------------------*/
                    //$this->Session->setFlash('リセット確認メールが送信されました', 'flash_success');
                    $this->set('error',0);
                    return $this->redirect(array('controller' => 'Auth','action' => 'forgot_pass_success'));
                    //============EndEmail=============//

                } else {

                    $this->set('error',1);
                    $this->Session->setFlash("トークンが不正です。管理者へご連絡ください", 'flash_success');
                    return $this->redirect(array('action' => 'forgotpassword'));

                }

            } else {
                $this->set('error',1);
                
            }

        }

    }


    public function resetpassword($token = null)
    {

        $this->set('page_title_default','パスワードを変更する');
        $this->layout = 'login';
        $this->User->recursive = -1;
        $this->set('error_retype_pass',0);
        $this->set('error_pass',0);
        $expire = isset($this->request->query['e']) ? $this->request->query['e'] : "" ;
        $hash = isset($this->request->query['h']) ?  $this->request->query['h'] : "";

        if (!empty($token) && !empty($expire) && !empty($hash)) {

            $user = $this->User->find('first', array('conditions' => array('User.tokenhash' => $token)));

            if (!empty($user)) {

                $email = $user['User']['email'];

                $check_expire = $this->checkShareLink($email,$expire,$hash);

                $this->User->id = $user['User']['id'];


                if($check_expire == true){
                    if ($this->request->is('post')) {

                        if( preg_match("/^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/", $this->request->data['User']['password']) ==0){

                            $this->set('error_pass', 1);

                        }else if ($this->request->data['User']['password'] != $this->request->data['User']['password_retype']) {
                            $this->set('error_retype_pass',1);

                            //return $this->redirect(array('action' => 'resetpassword', $token));
                        }else if ($this->User->saveField('password', md5($this->request->data['User']['password']))) {

                            $this->User->saveField('tokenhash', '');

                            //$this->Session->setFlash('Password has been reset!', 'flash_success');

                            ///SEND MAIL RESET PASSWORD ///
                            $name = $user['User']['name'];
                            $contact_email = $user['User']['email'];
                            $subject = '【キレイにスルンダ】求人管理画面パスワード再設定完了のお知らせ';

                            $AppController = new AppController;
                            $success = $AppController->send_email(
                              'gmail',
                              array(Configure::read('EMAIL') => Configure::read('EMAIL_NAME')),
                              $contact_email,
                              $subject,
                              $message = null,
                              $format = 'text',
                              $template = 'reset_password',
                              array('company_name' => $name,
                                'email' => $contact_email,
                                'password' => $this->request->data['User']['password'],
                                'url_page' => "http://" . $_SERVER['SERVER_NAME'] . "/corporation/login"
                              )
                            );

                            $this->redirect(array('controller' => 'Auth','action' => 'reset_pass_success'));

                        } else {

                            $this->Session->setFlash('Error updating values', 'flash_alert');
                            return $this->redirect(array('controller' => 'auth', 'action' => 'resetpassword', $token));

                        }

                    }

                }else{
                    //die("cc");
                    return $this->redirect(array('controller' => 'Auth', 'action' => 'timeout'));
                }

                $this->set('token', $token);

            } else {

               // $this->Session->setFlash('Token Corrupted.', 'flash_alert');
                return $this->redirect(array('controller' => 'auth', 'action' => 'resetpassword', $token));

            }


        } else {
           // die("aa");
//            throw new NotFoundException('Invalid page');
//            return true;

            return $this->redirect(array('controller' => 'Auth', 'action' => 'timeout'));

        }

    }

    

}
