<?php

App::uses('Controller', 'AppController');

class CompanyController extends AppController
{

    var $helpers = array('Html', 'Form', 'Auth','Paginator');
    var $components = array('Session', 'Paginator','ESession','Csv');
    var $uses = array('User','SecretJob','Entry','JobCategory');
    
    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
        if(!$this->Session->check('User')){
             $this->redirect('/login');
        }
    }



    public function job($pages = null)
    {
        $this->set('page_title_default','掲載求人一覧');

        $currentUser = $this->Session->read('User.User');
        $this->User->id = $currentUser['id'];
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }

        $this->paginate = $this->SecretJob->get_all_job($currentUser['id']);
        $list_job= $this->paginate('SecretJob');


        $list_entry_notification = $this->SecretJob->get_all_entry_non_read($currentUser['id']);

       // pr($list_entry_notification);
       // debug($this->SecretJob->getDataSource()->getLog(false, false) );die;
        //pr ($list_entry_notification);
        $this->set('notification',$list_entry_notification);
        $this->set('list_job',$list_job);


    }


    public function entry($id = null)
    {
        $this->set('page_title_default','応募者一覧');
        $id = $this->params['id'];
        $currentUser = $this->Session->read('User.User');
        $id_company = $currentUser['id'];
        if (!$id) {
            $this->redirect('/');
        }

        $data_job =  $this->SecretJob->find('first',
          array(
            'conditions'=>array(
              'id'=> $id,
              'company_id' => $id_company
            )
          ));

        if(empty($data_job)){
            $this->redirect('/');
        }


        $list_job = trim($data_job['SecretJob']['list_job'],",");
        if(!empty($list_job)){
            $list_category = $this->JobCategory->find('all',$this->JobCategory->getCategory_name(array('id IN ('.$list_job.')' )));
        }else{
            $list_category = array();
        }

        $breadcrumb = "";
        if(!empty($list_category)){
            foreach ($list_category as $v){
                $breadcrumb  .= $v['JobCategory']['name']."/";
            }
        }
        $this->set('breadcrumb',$breadcrumb);


       // pr($this->request->query);
        
        $conditions = array();
            if (!empty($this->request->query['gender']))
            {
                $conditions['Entry.gender like'] = "%".$this->request->query['gender']."%";
            }

            if (!empty($this->request->query['type_job_cat']))
            {
                $conditions['Entry.hope_employment like'] = "%".$this->request->query['type_job_cat']."%";
            }

            if (!empty($this->request->query['filter_status']))
            {
                $conditions['Entry.status'] = $this->request->query['filter_status'];
            }

            if (!empty($this->request->query['filter_rate']))
            {
                if($this->request->query['filter_rate'] == 4) {
                    $data_rate = 0;
                }else{
                    $data_rate = $this->request->query['filter_rate'];
                }
                $conditions['Entry.rate'] = $data_rate;
            }

            if (!empty($this->request->query['age']))
            {
                $year = date("Y") - $this->request->query['age'];
                $conditions['AND'] = array('YEAR(birthday) <= ' => $year,
                  'YEAR(birthday) >= ' => $year - 9,
                    );
            }


        
        //pr($conditions);


        $data = $this->Entry->getAllEntry($id,$conditions);
        if (!empty($this->request->query['t']) == 'id'  && !empty($this->request->query['s']))
        {
            $data['order'] = array(
              'is_read' => 'asc',
              'Entry.created_at' => 'DESC'
              );
        }

        $this->paginate = $data;
        $list_entry= $this->paginate('Entry',$conditions);
         // debug($this->SecretJob->getDataSource()->getLog(false, false) );
        $conn['Entry.is_read'] = 0;
        $notification = $this->Entry->find('all',$this->Entry->getAllEntry($id,$conn));

        $this->set('notification',$notification);
        $this->set('title_job',$data_job['SecretJob']['job_name']);
        $this->set('dataJob',$data_job['SecretJob']);
        $this->set('id',$id);
        $this->set('list_entry',$list_entry);

    }

    public function detail($id = null)
    {
        $this->set('page_title_default','応募者詳細');
        $id = $this->params['id'];
        $secret_job_id = $this->params['id_entry'];
        if (!$id) {
            $this->redirect('/');
        }

        $data =  $this->Entry->find('first',
          array(
            'conditions'=>array(
              'id'=> $id,
              'secret_job_id' => $secret_job_id
            )
          ));

        if(empty($data)){
            $this->redirect('/');
        }
        $breadcrumb = "";
        if(!empty($secret_job_id)){
            $data_job =  $this->SecretJob->find('first',
              array(
                'conditions'=>array(
                  'id'=> $secret_job_id,
                )
              ));

               if(!empty($data_job)){
                   $this->set('dataJob',$data_job['SecretJob']);
                   $list_job = trim($data_job['SecretJob']['list_job'],",");
               }

            if(!empty($list_job)){
                $list_category = $this->JobCategory->find('all',$this->JobCategory->getCategory_name(array('id IN ('.$list_job.')' )));
            }else{
                $list_category = array();
            }

            if(!empty($list_category)){
                foreach ($list_category as $v){
                    $breadcrumb  .= $v['JobCategory']['name']."/";
                }
            }
        }

        $this->set('breadcrumb',$breadcrumb);


        if ($this->request->is(array('post', 'put'))) {

            //update status
            $status = $this->data['status'];
            $this->Entry->id = $id;
            $ok = $this->Entry->saveField('status', $status);
            if($ok){
               // $this->Session->setFlash('Update status successfully', 'flash_success');
                return $this->redirect( str_replace("corporation/","" , $this->here));
            }
        }


        //update read
        $this->Entry->id = $id;
        $this->Entry->saveField('is_read', 1);


        $this->set('data',$data);

    }


    /**
     * @return array
     */
    public function update_rate()
    {
        $this->autoRender =false;
        $list_rate  = array(
          1 => '● 良い',
          2 => '▲ 普通',
          3 => '× 悪い',
          4 => '評価しない',
        );

        $list_status = $list_status = Configure::read('myconfig.list_status');
        if($this->request->is('ajax')){

            $id_entry = $this->request->data['id_entry'];
            $rate = $this->request->data['id'];
            $status = $this->request->data['id_status'];
            //update read
            $this->Entry->id = $id_entry;
            $this->Entry->saveField('rate', $rate);
            $this->Entry->saveField('status', $status);

            $output = array();
            if($rate==1){
                $__class = 'row_rate__rate_1';
            }elseif ($rate==2){
                $__class = 'row_rate__rate_2';
            }elseif ($rate==3){
                $__class = 'row_rate__rate_3';
            }else{
                $__class = "row_rate__rate_4";
            }
            if(!empty($rate)){
                $output['rate'] =  '<span class="'.$__class.'">'.$list_rate[$rate].'</span>';
            }else{
                $output['rate'] =  '<span class="row_rate__rate_4">評価しない</span>';
            }


            $output['status_span'] = $list_status[$status];
            echo json_encode($output);
            die;
        }
        return false;
    }


    public function download($id = null)
    {
        $this->autoRender = false;
        if (!$id) {
            $this->redirect('/');
        }
        $currentUser = $this->Session->read('User.User');
        $id_company = $currentUser['id'];
        $data_job =  $this->SecretJob->find('first',
          array(
            'conditions'=>array(
              'id'=> $id,
              'company_id' => $id_company
            )
          ));

        if(empty($data_job)){
            $this->redirect('/');
        }


        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=キレイにスルンダ応募者データ_' . @date('Ymd', time()) . '.csv');
        echo "\xEF\xBB\xBF";
        $fp = fopen('php://output', 'w');


        $list = $this->Entry->find('all', array(
          'fields' => array(),
          'conditions' => array('secret_job_id'=>$id),
        ));

        //debug($this->Jobseeker->getDataSource()->getLog(false, false) );die;

        fputcsv($fp, array("ID",
          "お名前",
          "フリガナ",
          "性別",
          "生年月日",
          "メールアドレス",
          "住所",
          "電話番号",
          "現在の就業状況",
          "入社可能時期",
          "希望連絡時間・連絡方法",
          "取得資格",
          "職務経歴・自己PR",
          "希望職種",
          "希望雇用形態",
          "応募日付"
        ));
        foreach ($list as $data) {

            $entry = $data['Entry'];

            if ($entry['created_at']) {
                $created = date("Y/m/d", strtotime($entry['created_at']));
            }


            $province = "";
            if (!empty($entry['province'])) {
                $province = $this->get_area_name($entry['province']);
            }

            $city = "";
            if (!empty($entry['city'])) {
                $city = $this->get_area_name($entry['city']);
            }


            error_reporting(0);
            fputcsv($fp,
              array($entry["id"],
                $entry["name"],
                $entry["kana"],
                $entry['gender'],
                $entry['birthday'],
                $entry['email'],
                $province.$city,
                $entry['tel']."\r",
                $entry['situation'],
                $entry['join'],
                $entry['contact'],
                $entry['capabilities'],
                $entry['pr'],
                $entry['job_objective'],
                $entry['hope_employment'],
                $created."\r"
              ));
        }
        fclose($fp);
    }



    public function download_detail($id = null)
    {
        $this->autoRender = false;
        if (!$id) {
            $this->redirect('/');
        }

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=キレイにスルンダ応募者データ_' .$id.'_'.@date('Ymd', time()) . '.csv');
        echo "\xEF\xBB\xBF";
        $fp = fopen('php://output', 'w');


        $list = $this->Entry->read(null, $id);

        if(empty($list)){
            $this->redirect('/');
        }
        $currentUser = $this->Session->read('User.User');
        $id_company = $currentUser['id'];
        $serect_job = $list['Entry']['secret_job_id'];
        $data_job =  $this->SecretJob->find('first',
          array(
            'conditions'=>array(
              'id'=> $serect_job,
              'company_id' => $id_company
            )
          ));

        if(empty($data_job)){
            $this->redirect('/');
        }

        //debug($this->Jobseeker->getDataSource()->getLog(false, false) );die;

        fputcsv($fp, array("ID",
          "お名前",
          "フリガナ",
          "性別",
          "生年月日",
          "メールアドレス",
          "住所",
          "電話番号",
          "現在の就業状況",
          "入社可能時期",
          "希望連絡時間・連絡方法",
          "取得資格",
          "職務経歴・自己PR",
          "希望職種",
          "希望雇用形態",
          "応募日付"
        ));

            $entry = $list['Entry'];

            if ($entry['created_at']) {
                $created = date("Y/m/d", strtotime($entry['created_at']));
            }


            $province = "";
            if (!empty($entry['province'])) {
                $province = $this->get_area_name($entry['province']);
            }

            $city = "";
            if (!empty($entry['city'])) {
                $city = $this->get_area_name($entry['city']);
            }



            error_reporting(0);
            fputcsv($fp,
              array($entry["id"],
                $entry["name"],
                $entry["kana"],
                $entry['gender'],
                $entry['birthday'],
                $entry['email'],
                $province.$city,
                $entry['tel']."\r",
                $entry['situation'],
                $entry['join'],
                $entry['contact'],
                $entry['capabilities'],
                $entry['pr'],
                $entry['job_objective'],
                $entry['hope_employment'],
                $created."\r"
              ));

        fclose($fp);
    }




}
