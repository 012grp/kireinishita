<?php

App::uses('Component', 'Controller');

class ESessionComponent extends Component {

	/*
	 * ログイン情報関連
	 */
	function setLoginInfo($user_id, $user_name, $user_company_id, &$controller) {

		$flag = true;

		$flag = $controller->Session->write('user_id', $user_id);
		$flag = $controller->Session->write('user_name', $user_name);
		$flag = $controller->Session->write('user_company_id', $user_company_id);
		//$flag = $controller->Session->write('user_group_id', $user_group_id);

		if(!$flag) {
			$this->logout($controller);
			$controller->redirect(array('controller' => 'error'));
		}
	}
	
	function setUserLoginInfo($user_id, $user_name, $user_company_id, $test_id, &$controller) {

		$flag = true;

		$flag = $controller->Session->write('test_user_id', $user_id);
		$flag = $controller->Session->write('test_user_name', $user_name);
		$flag = $controller->Session->write('test_user_company_id', $user_company_id);
		//$flag = $controller->Session->write('test_user_group_id', $user_group_id);
		$flag = $controller->Session->write('test_id', $test_id);

		if(!$flag) {
			$this->logout($controller);
			$controller->redirect(array('controller' => 'error'));
		}
	}
	
	function setTestCaseHistoryId($test_case_history_id, &$controller) {
		$flag = $controller->Session->write('test_case_history_id', $test_case_history_id);

		if(!$flag) {
			$controller->redirect(array('controller' => 'error'));
		}
	}
	function getTestCaseHistoryId(&$controller) {
		return $controller->Session->read('test_case_history_id');
	}

	function getCompanyId(&$controller) {
		return $controller->Session->read('user_company_id');
	}
	
	function getUserId(&$controller) {
		return $controller->Session->read('user_id');
	}

	function getTestUserId(&$controller) {
		return $controller->Session->read('test_user_id');
	}
	
	function getUserName(&$controller) {
		return $controller->Session->read('user_name');
	}
	
	function getTestUserName(&$controller) {
		return $controller->Session->read('test_user_name');
	}
	
	function getTestCompanyId(&$controller) {
		return $controller->Session->read('test_user_company_id');
	}
	
	//function getGroupId(&$controller) {
		//return $controller->Session->read('user_group_id');
	//}
	
	//function getTestUserGroupId(&$controller) {
		//return $controller->Session->read('test_user_group_id');
	//}
	
	function getTestId(&$controller) {
		return $controller->Session->read('test_id');
	}

	/*
	 * CSVアップロード関連
	 */
	function setCSVData($data, $errMsg, &$controller) {
		$flag = true;

		$flag = $controller->Session->write('csv_data', $data);
		$flag = $controller->Session->write('csv_errMsg', $errMsg);
	}

	function setCSVUserIdList($userIdList, &$controller) {
		$controller->Session->write('csv_userId_list', $userIdList);
	}

	function clearCSVData(&$controller) {
		$controller->Session->delete('csv_data');
		$controller->Session->delete('csv_errMsg');
	}

	function getCSVUserIdList(&$controller) {
		return $controller->Session->read('csv_userId_list');
	}

	function clearCSVUserIdList(&$controller) {
		$controller->Session->delete('csv_userId_list');
	}

	function getCSVData(&$controller) {
		return $controller->Session->read('csv_data');
	}

	function setCSVErrMsg($errMsg, &$controller) {
		$flag = true;
		$flag = $controller->Session->write('csv_errMsg', $errMsg);
	}

	function getCSVErrMsg(&$controller) {
		return $controller->Session->read('csv_errMsg');
	}

	/*
	 * PDFダウンロード関連
	 */
	function setSelectedListByPDFIndex($selected_list, &$controller) {
		$controller->Session->write('selected_list_pdf_index', $selected_list);
	}

	function getSelectedListByPDFIndex(&$controller) {
		return $controller->Session->read('selected_list_pdf_index');
	}

	function clearSelectedListByPDFIndex(&$controller) {
		$controller->Session->delete('selected_list_pdf_index');
	}

	/*
	 * 一覧画面関連
	 */
	function setSelectedList($selected_list, &$controller) {
		$controller->Session->write('selected_list_index', $selected_list);
	}

	function getSelectedList(&$controller) {
		return $controller->Session->read('selected_list_index');
	}

	function save_checked_list($data, &$controller, &$index = null, $tableName = null) {
		//選択された項目のリスト取得
		$selectedList = $this->getSelectedList($controller);
		if(empty($selectedList)) {
			$selectedList = Array();
		}

		if (isset($data['Form']['select'])) {
			foreach ($data['Form']['select'] as $selectId) {
				if (!in_array($selectId, $selectedList)) {
					$selectedList[] = $selectId;
				}
			}
		}

		//var_dump($selectedList);
		//解除された項目の削除
		if(isset($data['Form']['list'])) {
			$tmpArray = Array();

			foreach ($selectedList as $selectId) {
				if (!in_array($selectId, $data['Form']['list'])
				 || in_array($selectId, $data['Form']['list'])
				 	&& isset($data['Form']['select'])
				 	&& in_array($selectId, $data['Form']['select'])) {

				 	$tmpArray[] = $selectId;

				 	if(!empty($index)) {
						foreach($index as &$item) {
							if($item[$tableName]['id'] == $selectId) {
								$item['Form']['selected'] = true;
							}
						}
				 	}
				}
			}
			$selectedList = $tmpArray;
		}

		$this->setSelectedList($selectedList, $controller);
		//var_dump($selectedList);
		//var_dump($this->data);
	}

	/*
	 * 一覧画面タイプ
	 */
	function setIndexType($type, &$controller) {
		$controller->Session->write('index_type', $type);
	}

	function getIndexType(&$controller) {
		return $controller->Session->read('index_type');
	}

	function setIndexInfo($data, &$controller) {
		$controller->Session->write('index_data', $data);
	}

	function getIndexInfo(&$controller) {
		return $controller->Session->read('index_data');
	}

	/* セッション終了 */
	function logout(&$controller) {
		$controller->Session->destroy();
	}
	
	 /*
	 * テスト一覧画面関連
	 */
	function setTestSearchCondition($data, &$controller) {
		$controller->Session->write('test_search_condition', $data);
	}

	function getTestSearchCondition(&$controller) {
		return $controller->Session->read('test_search_condition');
	}
	
	/*
	 * テストユーザ一覧関連
	 */
	function setTestUserSearchCondition($data, &$controller) {
		$controller->Session->write('test_user_search_condition', $data);
	}

	function getTestUserSearchCondition(&$controller) {
		return $controller->Session->read('test_user_search_condition');
	}
	
	 /*
	 * マニュアル一覧画面関連
	 */
	function setManualSearchCondition($data, &$controller) {
		$controller->Session->write('manual_search_condition', $data);
	}

	function getManualSearchCondition(&$controller) {
		return $controller->Session->read('manual_search_condition');
	}
	
}