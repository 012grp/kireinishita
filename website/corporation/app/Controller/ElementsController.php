<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ElementsController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Evaluating', 'CvInformations','EvaluatingCommon', 'EvaluatingJobseekers', 'Jobseeker', 'JobseekerStatus','CompanyMember','Role','JobsAssignees','Job','Stage');
    public $helpers = array('Paginator','Html');
    public $components = array('Session');
    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *  or MissingViewException in debug mode.
     */

    public function getEvaluatingByUser($id){
        $data = $this->Evaluating->find('all',array(
            'conditions' => array(
                'company_id' =>$id,
            )
        ));

        return $data;
    }

    public function getEvaluatingByJob($jobId){
        $data = $this->Evaluating->find('all',array(
            'conditions' => array(
                'job_id' =>$jobId,
            )
        ));
        return $data;
    }

    public function getEvaluatingStatus(){
        $data = $this->JobseekerStatus->find('list',array(
            'conditions' => array(
                'status' =>2,
            )
        ));
        return $data;
    }

    public function DeleteEvaluatingByAjax(){
        $this->autoRender = false;
        $id = $this->request->data['id'];
        $this->Evaluating->id = $id;
        if($this->Evaluating->delete()) {
            echo json_encode(['status' => true, 'description' => 'delete success']);
            die;
        }else {
            echo json_encode(['status' => false, 'description' => 'delete not success']);
            die;
        }
    }

    public function getEvaluatingByCandidate($jobseeker_id){
        $data = $this->CvInformations->find('list',array(
            'fields' => array('evaluating_id', 'value'),
            'conditions' => array(
                'jobseeker_id' =>$jobseeker_id,
            )
        ));
        $score = $this->CvInformations->find('list',array(
            'fields' => array('CvInformations.evaluating_id', 'jobseeker_status.name'),
            'conditions' => array(
                'jobseeker_id' =>$jobseeker_id,
            ),
            'joins' => array(
                array(
                    'table' => 'jobseeker_status',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'jobseeker_status.id = CvInformations.value',
                    ),
                )
            ),
        ));
        $data['total_score']= array_sum($score);
        return $data;
    }

    public function updateCvInformation(){
        $this->autoRender = false;

        $data = $this->request->data;
        $data['status'] =1 ;
        $isExistInCvInformation = $this->CvInformations->find('first',array(
            'conditions' => array(
                'jobseeker_id' =>$data['jobseeker_id'],
                'evaluating_id'=>$data['evaluating_id']
            )
        ));
        //add new
        if(sizeof($isExistInCvInformation)){
            $this->CvInformations->id = $isExistInCvInformation['CvInformations']['id'];
            if($this->CvInformations->save($data)) {
                $this->setFlash($this->setLang('The evaluating has been changed'));
            }
            else {
                $this->set('errors', $this->setLang('The evaluating has been not changed'));
            }
        }else{
            //update
            $this->CvInformations->create();
            if($this->CvInformations->save($data)) {
                $this->setFlash($this->setLang('The evaluating has been changed'));
            }
            else {
                $this->set('errors', $this->setLang('The evaluating has been not changed'));
            }

        }
        
        /**
         * Updated total score for jobseeker.
         */
        // job_id
        $evaluating = $this->Evaluating->find('first',array('conditions' => array( 'id'=>$data['evaluating_id'] )));
        $jobseeker['job_id'] = $evaluating['Evaluating']['job_id'];
        $jobseeker['jobseeker_id'] = $data['jobseeker_id'];

        // all score of jobseeker.
        $jobseeker['score'] = 0;
        $score_all = $this->CvInformations->find('all',array( 'conditions' => array( 'jobseeker_id' =>$data['jobseeker_id'] ) ));        
        foreach ($score_all as $key => $value) 
            if($value['CvInformations']['value'] == 7)
                $jobseeker['score'] ++;

        // Find score jobseeker.
        $job_jobseeker = $this->EvaluatingJobseekers->find('first',array( 'conditions' => array( 'jobseeker_id' =>$data['jobseeker_id'], 'job_id' => $jobseeker['job_id'] ) ));   
        
        // update - add
        if(sizeof($job_jobseeker)){
            $this->EvaluatingJobseekers->id = $job_jobseeker['EvaluatingJobseekers']['id'];
            $this->EvaluatingJobseekers->save($jobseeker);            
        }else{
            $this->EvaluatingJobseekers->create();
            $this->EvaluatingJobseekers->save($jobseeker);
        }
    }

    public function getEvaluatingCommon(){
        $data = $this->EvaluatingCommon->find('all',array(
            'fields' => array('id','name', 'value'),
            'conditions' => array(
                'status' =>1,
            )
        ));

        return $data;
    }

    public function getRoleByLevelUser($userLevel = null){
        $data = $this->Role->find('list',array(
            'fields' => array('id','name'),
            'conditions' => array(
                'level <' =>$userLevel,
            )
        ));

        return $data;
    }

    public function getAssigneeoption($userId = null,$assigneeType = 0){
        $this->autoRender = false;
        if($assigneeType == 1) {
            $data = $this->CompanyMember->find('all', array(
                'fields' => array('CompanyMember.member_id', 'Users.fullname', 'Roles.name', 'Roles.level'),
                'conditions' => array(
                    'company_id' => $userId,
                ),
                'joins' => array(
                    array(
                        'alias' => 'Users',
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Users.id = CompanyMember.member_id',
                        ),
                    ),
                    array(
                        'alias' => 'Roles',
                        'table' => 'roles',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Roles.id = CompanyMember.role_id',
                        ),
                    ),
                ),
            ));
        }else{
            $data = $this->Role->find('all',array(
                'fields' => array('id','name'),
                'conditions' => array(
                    'level <' =>999,
                )
            ));
        }

        echo json_encode($data);
        die;
    }

    public function getJobAssignee($jobId = null){
        $data = $this->JobsAssignees->find('all', array(
            'conditions' => array(
                'job_id' => $jobId,
            ),
        ));

        return $data;
    }

    public function isAccessJob($jobId = null){
        $role = !empty(CakeSession::read('Auth.User.company_role_id')) ? CakeSession::read('Auth.User.company_role_id') : null;
        $originUserId = !empty(CakeSession::read('Auth.User.id')) ? CakeSession::read('Auth.User.id') : null;
        $data = $this->JobsAssignees->find('first', array(
            'conditions' => array(
                'job_id' => $jobId,
                'stage_id is NULL' ,
            ),
        ));

        $isAccessJob = true;
        if(!empty($role) && !empty($originUserId)) {
            if (isset($data['JobsAssignees']['role_id'])) {
                $isAccessJob = $data['JobsAssignees']['role_id'] == 0 || $data['JobsAssignees']['role_id'] == $role ? true : false;
            } elseif (isset($data['JobsAssignees']['user_id'])) {
                $isAccessJob = $data['JobsAssignees']['user_id'] == 0 || $data['JobsAssignees']['role_id'] == $originUserId ? true : false;
            }
        }

        return $isAccessJob;
    }

    public function updateInterviewDate(){

      if ($this->request->is('post')) {
        $post = $this->request['data'];

        $data['Jobseeker']['interview_date'] = $this->stringToDate(trim($post['value']));
        $this->Jobseeker->id = $post['jobseeker_id'];

        if ($this->Jobseeker->save($data)) 
          $this->setFlash($this->setLang('The jobseeker has been saved'));
        else 
          $this->set('errors', 'The jobseeker could not be saved. Please, try again.');

        die;
      }
    }
}
