<?php

App::uses('Controller', 'AppController');

class UsersController extends AppController
{

    public $helpers = array('Html', 'Form', 'Auth');
    public $components = array('Session', 'Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.

    }

    public function logout()
    {
        $this->autoRender = false;
        $this->Session->destroy();
        $this->redirect('/login');

    }

    public function index($pages = null)
    {
        if( $this->Session->read('Auth.User.id') != 1){
            $this->redirect('/');
        }
        $currentUser = $this->Session->read('Auth.User');

        $this->User->id = $currentUser['id'];
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }
        $user = $this->User->find('all');
        $this->set('user', $user);

    }


    public function view($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException('Invalid user');
        }
        $this->set('user', $this->User->read(null, $id));
    }


    public function add()
    {
        if( $this->Session->read('Auth.User.id') != 1){
            $this->redirect('/');
        }
       if ($this->request->is('post')) {
            $this->User->create();

            if(isset($this->request->data['survey']))    {
                $this->request->data['User']['survey'] = implode(",",$this->request->data['survey']);
            }

            if ($this->User->save($this->request->data, true)) {
                $this->setFlash('ユーザーが保存されました');

                return $this->redirect(array('action' => 'add'));
            } else {
                $errors = $this->User->validationErrors;
                $this->set('errors', $errors);
            }
        }


        $research_list =  $this->User->query("SELECT * FROM research_lists WHERE 1");
        $this->set('research_list',$research_list);

    }

    public function edit($id = null)
    {
        $this->set('page_title_default','パスワードを変更する');
        
        if(empty($id)){
            $this->redirect('/');
        }else{
            $data = $this->User->findById($id);
            $this->set('data',$data);
        }
        $this->set('pass_old',0);
        $this->set('error_pass',0);
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->id = $id;
            $params = $this->request->data;
            if ($params && (md5($params['User']['password_old']) == $data['User']['password'] )) {


                if( preg_match("/^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/", $params['User']['password']) ==0){
                    $this->set('error_pass', 1);
                }else{
                    unset($params['User']['email']);
                    if (!empty($params['User']['password'])) {
                        $params['User']['password'] = md5($params['User']['password']);
                    }

                    if ($this->User->save($params, true)) {
                        $this->Session->destroy();
                        $this->Session->setFlash('ユーザーが保存されました', 'flash_success');

                        ///SEND MAIL RESET PASSWORD ///
                        $name = $data['User']['name'];
                        $contact_email = $data['User']['email'];
                        $subject = '【キレイにスルンダ】求人管理画面パスワード再設定完了のお知らせ';
                        //$message = '<a href="' . $url . '" target="_blank">Please click here</a>';

                        $AppController = new AppController;
                        $success = $AppController->send_email(
                          'gmail',
                          array(Configure::read('EMAIL') => Configure::read('EMAIL_NAME')),
                          $contact_email,
                          $subject,
                          $message = null,
                          $format = 'text',
                          $template = 'reset_password',
                          array('company_name' => $name,
                            'email' => $contact_email,
                            'password' => $this->request->data['User']['password'],
                            'url_page' => "http://" . $_SERVER['SERVER_NAME'] . "/corporation/login"
                          )
                        );

                        ///SEND MAIL RESET PASSWORD ///
                        return $this->redirect(array('controller' => 'Auth','action' => 'reset_pass_success'));
                    } else {
                        $this->set('errors', $this->User->validationErrors);
                    }
                    $this->data = $this->User->findById($id);
                }

            }else{
                $this->set('pass_old',1);
                //return $this->redirect(array('action' => 'edit/' . $id));
            }
        }
        $this->set('userId', $id);
    }



    public function delete($id = null)
    {
        $this->request->allowMethod('get');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->setFlash('Invalid User', 'error');
        }

        if ($this->User->delete()) {
            $this->setFlash('User was deleted', 'ok');
        } else {
            $this->setFlash('User was not deleted', 'error');
        }

        return $this->redirect(array('controller' => 'Users', 'action' => 'index'));
    }





}

?>