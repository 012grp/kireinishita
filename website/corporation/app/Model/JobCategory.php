<?php

App::uses('AppModel', 'Model');

class JobCategory extends AppModel {
	
	public $useTable = 'job_category';


	/**
	 * @return string
	 */
	public function getCategory_name($conditions = null)
	{
		return array(
			'fields' => array('name'),
			'conditions'=>array(
				$conditions
			)
		);
	}



}

?>