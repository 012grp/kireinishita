<?php

App::uses('AppModel', 'Model');

class SecretJob extends AppModel {
	
	public $useTable = 'secret_job';

	public function get_all_job($id){

		$this->virtualFields['num_entry_non_read'] = "SELECT count(id) from entry where secret_job_id = SecretJob.id and is_read = 0";
		$this->virtualFields['num_entry_non_read_sort'] = "SELECT if(count(id)>0,1,0) from entry where secret_job_id = SecretJob.id and is_read = 0";
		$this->virtualFields['max_created_at_order'] = "max_entry.max_created_at";

		$out  =array(
			'fields' => array('id','job_name','store_name','list_job','start_at','end_at','num_entry_non_read','max_entry.max_created_at',
				'(SELECT count(id) from entry where secret_job_id = SecretJob.id) as num_entry'
			),
			'joins' => array(
				array(
					'table' => '(
									 select e.secret_job_id, max(e.created_at) as max_created_at
									 from entry e
									 where e.is_read = 0
									 group by e.secret_job_id
									) max_entry',
					'type' => 'LEFT',
					'conditions' => array(
						  'SecretJob.id = max_entry.secret_job_id'
					),

				)
			),
			'conditions'=>array(
				'company_id'=> $id,
			),
			'limit' => 20,
			'order' => array(
				'num_entry_non_read_sort' => 'DESC',
				'max_created_at_order' => 'DESC',
				'SecretJob.created_at' => 'DESC',
				'SecretJob.updated_at' => 'DESC'
			)
		);

		return $out;

	}
	


	public function get_all_entry_non_read($id){

		$this->virtualFields['num_entry_non_read_sort'] = "SELECT if(count(id)>0,1,0) from entry where secret_job_id = SecretJob.id and is_read = 0";
		$this->virtualFields['max_created_at_order'] = "SELECT max(e.created_at)  from entry e where secret_job_id = SecretJob.id and is_read = 0 group by e.secret_job_id";
		$out  =$this->find('all',array(
			'fields' => array('id','job_name','store_name','list_job','num_entry_non_read',
				'(SELECT count(id) from entry where secret_job_id = SecretJob.id) as num_entry'
			),
			'joins' => array(
				array(
					'table' => ' (
									 select e.secret_job_id, max(e.created_at) as max_created_at
									 from entry e
									 where e.is_read = 0
									 group by e.secret_job_id
									) max_entry',
					'type' => 'LEFT',
					'conditions' => array(
						'SecretJob.id = max_entry.secret_job_id'
					),

				)
			),
			'conditions'=>array(
				'company_id'=> $id,
			),
			'limit' => 10,
			'order' => array(
				'num_entry_non_read_sort' => 'DESC',
				'max_created_at_order' => 'DESC',
				'SecretJob.created_at' => 'DESC',
				'SecretJob.updated_at' => 'DESC'
			)
		));

		return $out;

	}




}

?>