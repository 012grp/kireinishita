<div class="primary-login">
  <div class="primary-login__logo"><img src="<?php echo $this->webroot?>img/svg/logo.svg" alt="logo"></div>
  <div class="text-center">
    <h2 class="u-fs--xl"><b>パスワード再設定メールが送信されました</b></h2>
    <p class="u-mt15">メール文に記載のパスワード再設定用URLにアクセスし、<br>
      新しいパスワードを設定してください。</p>
  </div>
  <div class="form--login__middle u-mt20">
      <div class="row padding--top--10">
        <a href="<?php echo Router::url(['controller' => 'Auth', 'action' => 'login']);?>">
        <button type="submit" class="btn btn-success btn--full">ログイン画面に戻る</button>
          </a>
      </div>
  </div>
</div>



