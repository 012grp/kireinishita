<div class="primary-login box--forget--pass">
  <div class="primary-login__logo"><img src="<?php echo $this->webroot?>img/svg/logo.svg" alt="logo"></div>
  <div class="primary-login__short">
    <p>パスワードの再設定、又はパスワードを忘れた方は、<br>登録されたメールアドレスを入力して送信ボタンを押してください。<br>入力したメールアドレスにパスワード再設定用URLが送信されます。</p>
  </div>
  <div class="form--login__middle">
    <?php echo $this->Form->create('User', array('class' => 'form-horizontal'));
      $class = (!empty($error)) ? ' error' : "";

    ?>
      <div class="form-group email">
        <?php
        $email = array('class' => 'form-control'.$class,
          'label' => false,
          'id'    => 'email',
          'type'  => 'text',
          'placeholder' => 'メールアドレス',
          'required' => true,
          'value' => ''
        );
        echo $this->Form->input('email', $email);
        ?>
        <?php if(!empty($error)) {?>
          <p class="label_error u-mt10">※このメールアドレスは登録されていません</p>
        <?php }?>
      </div>
      <div class="row padding--top--10">
        <button type="submit" class="btn btn-success btn--full">送信</button>
      </div>
    <?php echo $this->Form->end(null); ?>
    <div class="text-center"><?php echo $this->Html->link('> ログイン画面に戻る',
        array('controller' => 'auth', 'action' => 'login'), array()); ?></div>
  </div>
</div>



