<div class="primary-login">
    <div class="primary-login__logo"><img src="<?php echo $this->webroot?>img/svg/logo.svg" alt="logo">
        <h1 class="title-login">管理画面ログイン</h1>
    </div>
    <div class="form--login__middle">
        <?php
            $class_error = (!empty($error)) ? ' error' : "";
        ?>
        <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>
            <div class="form-group email">
                <?php echo $this->Form->input('email',
                  array('class' => 'form-control'.$class_error,
                    'label' => false,
                    'id' => 'email',
                     'required' => true,
                    'div' => false,
                    'type' => 'text', 
                    'placeholder' => 'メールアドレス')); ?>
            </div>
            <div class="password">
                <?php echo $this->Form->input('password',
                  array('class' => 'form-control'.$class_error,
                    'label' => false,
                    'id' => 'password',
                    'required' => true,
                    'div' => false,
                    'placeholder' => 'パスワード')); ?>
                <?php if(!empty($error)) {?>
                    <p class="clear-both u-mt10">
                        <span class="label_error">※メールかパスワードが正しくありません。</span>
                    </p>
                <?php }?>
            </div>
            <div class="row u-mt30">
                <button type="submit" class="btn btn-success btn--full">ログイン</button>
            </div>
            <div class="text-center u-mt25">
                <?php echo $this->Html->link('> パスワードを忘れた方はこちら',
                  array('controller' => 'auth', 'action' => 'forgotpassword'), array('class'=>'forget_pass')); ?>

               </div>
        <?php echo $this->Form->end(null); ?>
    </div>
</div>