<div class="primary-login">
  <div class="primary-login__logo"><img src="<?php echo $this->webroot?>img/svg/logo.svg" alt="logo"></div>
  <div class="text-center">
    <h2 class="u-fs--xl"><b>パスワードの再設定が完了しました</b></h2>
    <p class="u-mt15">新しいパスワードでログインしてください。</p>
  </div>
  <div class="form--login__middle u-mt20">
      <div class="row padding--top--10">
        <a href="<?php echo Router::url(['controller' => 'Auth', 'action' => 'login']);?>">
        <button type="submit" class="btn btn-success btn--full">ログイン画面</button>
          </a>
      </div>
  </div>
</div>