<div class="primary-login box--reset--pass">
  <div class="primary-login__logo"><img src="<?php echo $this->webroot?>img/svg/logo.svg" alt="logo">
    <h1 class="title-login">パスワード再設定画面</h1>
  </div>
  <div class="primary-login__short_resetpass text-center">このページはパスワードをリセットするフォームです。<br>覚えがない方は閉じてください。</div>
  <div class="form--login__middle">
    <?php echo $this->Form->create('User', array('class' => 'form-horizontal')); ?>


    <?php
    $class_error ="";
    $class_error_pass = "";
    $alert_error_retype = '';
    $alert_error = '<div class="__lable_note padding--top--10">
          <label>※8桁以上の英数字をご入力ください</label>
        </div>';
    if(!empty($error_retype_pass)) {
      $class_error = ' error';
      $alert_error_retype = '<p class="label_error u-mt10">※入力したパスワードに誤りがあります</p>';
    }

    if(!empty($error_pass)){
      $class_error_pass = ' error';
      $alert_error = '<p class="label_error u-mt10">※8桁以上の英数字をご入力ください</p>';
    }


    $password = array('class' => 'form-control'.$class_error_pass,
      'label' => false,
      'id' => 'password',
      'placeholder' => '新しいパスワード' ,
      'pattern' => '[a-z0-9]{8,}',
      'required' => true
    );
    $password_retype = array('class' => 'form-control'.$class_error,
      'label' => false,
      'type' => 'password',
      'placeholder' => "もう一度新しいパスワードをご入力ください",
      'required' => true
    );

    echo $this->Form->input('token', array('type'=>'hidden', 'value'=>$token));
    ?>
      <div class="form-group">
        <?php echo $this->Form->input('password', $password);
          echo $alert_error;
        ?>

      </div>
      <div class="password">
        <?php  echo $this->Form->input('password_retype', $password_retype);
          echo $alert_error_retype;
        ?>
      </div>


      <div class="row u-mt30">
        <button type="submit" class="btn btn-success btn--full">リセット</button>
      </div>
    <?php echo $this->Form->end(null); ?>
  </div>
</div>