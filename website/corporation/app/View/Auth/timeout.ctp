<div class="primary-login">
  <div class="primary-login__logo"><img src="<?php echo $this->webroot?>img/svg/logo.svg" alt="logo"></div>
  <div class="text-center">
    <h2 class="u-fs--xl"><b>URLの有効期限が切れました</b></h2>
    <p class="u-mt15">もう一度はじめからやり直してください。</p>
  </div>
  <div class="form--login__middle u-mt20">
      <div class="row padding--top--10">
        <a href="<?php echo Router::url(['controller' => 'auth', 'action' => 'forgotpassword']);?>">
        <button type="submit" class="btn btn-success btn--full">パスワードの再設定</button>
          </a>
      </div>


  </div>
</div>



