<?php
$row = $data['Entry'];
$User = $this->Session->read('User');
$list_status = $list_status = Configure::read('myconfig.list_status');

if(empty($dataJob)){
  $dataJob = array('store_name'=>"");
}

?>
<div class="breadcrumb pc"><a href="<?php echo $this->webroot ?>job/<?php echo $User['User']['id'] ?>">掲載求人一覧 ></a><a
    href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'] ?>/entry/<?php echo $row['secret_job_id'] ?>"><?php echo $breadcrumb . $dataJob['store_name'] ?>
    の応募者求人一覧 ></a><span>応募者詳細</span></div>
<div id="page-wrapper" class="container">
  <div class="box_info_detail div_title u-mt20">
    <div class="pull-left">
      <div class="title_page"><?php echo $breadcrumb . $dataJob['store_name'] ?>の応募者詳細</div>
      <div class="id_job u-mt15">求人ID：<?php echo $row['id'] ?></div>
    </div>
    <div class="pull-right pc"><a href="<?php echo $this->webroot ?>company/download_detail/<?php echo $row['id'] ?>"
                                  class="btn-donwload-csv">応募者の詳細データを<br>ダウンロード（CSV形式）</a></div>
  </div>
  <div class="box_content u-mt20">
    <ul class="detail_entry">
      <li class="li_first">基本情報（管理番号：<?php echo $row['id'] ?>）</li>
      <li>
        <ul>
          <li>お名前</li>
          <li><?php echo $row['name'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>フリガナ</li>
          <li><?php echo $row['kana'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>性別</li>
          <li><?php echo $row['gender'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>生年月日</li>
          <li><?php echo $row['birthday'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>年齢</li>
          <li><?php
            $age = date("Y") - date("Y", strtotime($row['birthday']));
            echo $age;
            ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>メールアドレス</li>
          <li><?php echo $row['email'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>住所</li>
          <li><?php echo $this->App->get_area_name($row['province']) ?>
            , <?php echo $this->App->get_area_name($row['city']) ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>電話番号</li>
          <li><?php echo $row['tel'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>現在の就業状況</li>
          <li><?php echo $row['situation'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>入社可能時期</li>
          <li><?php echo $row['join'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>希望連絡時間・連絡方法</li>
          <li><?php echo $row['contact'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>取得資格</li>
          <li><?php echo $row['capabilities'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>職務経歴・自己PR</li>
          <li><?php echo $row['pr'] ?></li>
        </ul>
      </li>
      <li>
        <ul>
          <li>希望雇用形態</li>
          <li><?php echo $row['hope_employment'] ?></li>
        </ul>
      </li>
    </ul>
    <div class="text-center u-mt20"><a href="mailto:<?php echo $row['email'] ?>"
                                       class="btn btn-sendmail"><span>この応募者に連絡する</span></a>
      <p class="font--size--gray u-mt10">※メールソフトが立ち上がります。</p>
    </div>
  </div>
  <div class="box_reviews_entry u-mt45">
    <div class="box_reviews">
      <div class="box_revew__title">応募者の評価</div>
      <div class="box_review__content">
        <div class="list_btn_rate">
          <?php
          $active_1 = "";
          $active_2 = "";
          $active_3 = "";
          $active_4 = "";
          if($row['rate']==1){
            $active_1 = ' active';
          }
          if($row['rate']==2){
            $active_2 = ' active';
          }
          if($row['rate']==3){
            $active_3 = ' active';
          }
          if($row['rate']==0){
            $active_4 = ' active';
          }

          ?>
          <button value="1" class="btn btn_update_rate btn--rate_1<?php echo $active_1?>"><span>● 良い</span></button>
          <button value="2" class="btn btn_update_rate btn--rate_2<?php echo $active_2?>"><span>▲ 普通</span></button>
          <button value="3" class="btn btn_update_rate btn--rate_3<?php echo $active_3?>"><span>× 悪い</span></button>
          <button value="0" class="btn btn_update_rate btn--rate_4<?php echo $active_4?>"><span>評価しない</span></button>
        </div>
        <div class="clear-both"></div>
      </div>
      <p class="box_review__notice">評価をしておくと応募者一覧で絞り込みができるようになります。</p>
    </div>
    <div class="box_status u-mt20">
      <div class="box_revew__title border--top">ステータスの変更</div>
      <div class="box_review__content">
        <?php
        $style_status = (!empty($row['status'])) ? 'style="background-color: rgb(255, 255, 255);"' : "";

        ?>
        <select name="status" class="form-control select-status" <?php echo $style_status?>>
          <?php
          foreach ($list_status as $k => $vs) {
            $status = ($row['status']) ? $row['status'] : 4;
            $select = ($k == $status) ? 'selected' : "";
            echo '<option ' . $select . ' value="' . $k . '">' . $vs . '</option>';
          }

          ?>
        </select>
      </div>
    </div>
    <div class="div_btn_save text-center">
      <input type="hidden" value="<?php echo $row['rate']?>" id="rate">
      <input type="hidden" value="<?php echo $row['id']?>" name="id_entry" id="id_entry">
      <a href="javascript:void(0)" id="btn_save" class="btn btn_save popup-trigger">この状態で登録</a>
    </div>
  </div>
  <div class="row_paging row_paging_detail u-mt15">
    <div class="pull-left">
      <a href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'] ?>/entry/<?php echo $row['secret_job_id'] ?>"
        class="return_page"><<span>応募者一覧に戻る</span></a></div>
  </div>
</div>

<div class="popup">
  <div class="popup__content"><a class="popup-btn-close btn--close--x sp">X</a>
    <p class="row_rate">評価：
      <span id="span_rate"><span class="row_rate__rate_1">良い　</span></span> <br class="sp">ステータス：<span id="span_status">面接予定</span></p>
    <p>に変更しました。</p>
    <div class="text-center u-mt40">
      <a href="mailto:<?php echo $row['email'] ?>" class="btn btn-sendmail"><span>この応募者に連絡する</span></a>
      <p class="font--size--gray u-mt5">※メールソフトが立ち上がります。</p>
    </div>
    <a href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'] ?>/entry/<?php echo $row['secret_job_id'] ?>" class="popup-btn-close u-mt30">閉じる</a>
  </div>
</div>

