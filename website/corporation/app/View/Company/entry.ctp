<?php
$gender = array(0 => '男', 1 => '女');
$User = $this->Session->read('User');
$list_status = Configure::read('myconfig.list_status');
$list_rate  = array(
      0 => '評価',
      1 => '●',
      2 => '▲',
      3 => '×',
      4 => '&nbsp-'
);
$list_age = array(10,20,30,40,50,60,70,80);

$style_1 = (!empty($this->request->query['gender'] )) ? 'style="background-color: rgb(255, 255, 255);"' : "";

$style_2 = (!empty($this->request->query['age'] )) ? 'style="background-color: rgb(255, 255, 255);"' : "";

$style_3 = "";
if(!empty($this->request->query['filter_rate'])){
  if($this->request->query['filter_rate'] == 1) {
    $style_3 = 'style="background-color: rgb(255, 255, 255); color: rgb(0, 107, 179);"';
  }elseif($this->request->query['filter_rate'] == 2){
    $style_3 = 'style="background-color: rgb(255, 255, 255); color: rgb(7, 171, 70);"';
  }elseif($this->request->query['filter_rate'] == 3){
    $style_3 = 'style="background-color: rgb(255, 255, 255); color: rgb(208, 73, 80);"';
  }elseif($this->request->query['filter_rate'] == 4){
    $style_3 = 'style="background-color: rgb(255, 255, 255); color: rgb(126, 125, 125);"';
  }
}

$style_4 = (!empty($this->request->query['type_job_cat'] )) ? 'style="background-color: rgb(255, 255, 255);"' : "";

$style_5 = (!empty($this->request->query['filter_status'] )) ? 'style="background-color: rgb(255, 255, 255);"' : "";

if(!empty($this->request->query['t']) && !empty($this->request->query['s']) ){
  $param = '';
}else{
  $param = '?t=id&s=1';
}

?>

<div class="breadcrumb pc"><a href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'];?>">掲載求人一覧 ></a><span> <?php echo $breadcrumb.$dataJob['store_name']?>の応募者一覧</span></div>
<div id="page-wrapper" class="container">
  <div class="div_title u-mt40">
    <div class="title_page"><?php echo $breadcrumb.$dataJob['store_name']?>の応募者一覧</div>
    <div class="id_job u-mt15">求人ID：<?php echo $id ?></div>
  </div>
  <?php

  if (!empty($notification)) {
    echo '<div class="box_notificate u-mt30"><ul class="box_notificate__ul">';
    foreach ($notification as $notice) {
      //$class = "";
      $class = 'class="warning"';
      if(strtotime('+3 day', strtotime($notice['Entry']['created_at'])) < time()){
        $class= 'class="warning"';
      }
      echo '<li><a '.$class.' href="'.$this->webroot.'job/'.$User['User']['id'].'/entry/'.$id.'/detail/'.$notice['Entry']['id'].'">「'.$notice['Entry']['name'].' さん」から新しい応募がきました。 ></a></li>';
    }
    $showmore = "";
    if(count($notification) >3){
      $showmore = '<div class="text-center u-mt20 sp"><span class="btn_show_more">すべて表示</span></div>';
    }
    echo '</ul>'.$showmore.'</div>';
  }
  ?>

  <div class="box_filter u-mt20 pc">
    <div class="pull-left">
      <form action="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'];?>/entry/<?php echo $id?>" method="get" name="form_filter">
        <div class="col col-1 label">絞り込み：</div>
        <div class="col col-1">
          <select name="gender" class="form-control" <?php echo $style_1?>>
            <option value="">性別</option>
            <?php foreach ($gender as $v){
              $selected = (isset($this->request->query['gender']) && $this->request->query['gender'] == $v) ? "selected" : "" ;
              echo '<option '.$selected.' value="'.$v.'">'.$v.'</option>';
            }?>
          </select>
        </div>

        <div class="col col-2">
          <select name="age" class="form-control" <?php echo $style_2?>>
            <option value="">年齢</option>
            <?php foreach ($list_age as $va){
              $selected = (isset($this->request->query['age']) && $this->request->query['age'] == $va) ? "selected" : "" ;
              echo '<option '.$selected.' value="'.$va.'">'.$va.'代</option>';
            }?>

          </select>
        </div>

        <div class="col col-1">
          <select name="filter_rate" class="form-control select_with_icon" <?php echo $style_3?>>
            <?php foreach ($list_rate as $kr => $vr){
              $selected = (isset($this->request->query['filter_rate'])  && $this->request->query['filter_rate']== $kr) ? "selected" : "" ;
              $cls = "";
              if($kr == 1) {
                $cls = "class='blue'";
              }elseif($kr == 2){
                $cls = "class='green'";
              }elseif($kr == 3){
                $cls = "class='red'";
              }elseif($kr == 4){
                $cls = "class='line'";
              }

              echo '<option '.$selected.' value="'.$kr.'" '.$cls.'>'.$vr.'</option>';
            }?>
          </select>
        </div>
        <div class="col col-2">
          <select name="type_job_cat" class="form-control" <?php echo $style_4?>>
            <option value="">希望雇用形態</option>
            <?php
            $list_type_job_cat = $this->App->get_type_job_category();
            foreach ($list_type_job_cat as $v){
              $selected_t = (isset($this->request->query['type_job_cat']) && $this->request->query['type_job_cat'] == $v['JobCategory']['name']) ? "selected" : "" ;
              echo '<option '.$selected_t.' value="'.$v['JobCategory']['name'].'">'.$v['JobCategory']['name'].'</option>';
            }?>
          </select>
        </div>

        <div class="col col-2">

          <select name="filter_status" class="form-control" <?php echo $style_5?>>
            <option value="">ステータス</option>
            <?php foreach ($list_status as $ks => $v){
              $selected = (isset($this->request->query['filter_status']) && $this->request->query['filter_status'] == $ks) ? "selected" : "" ;
              echo '<option '.$selected.' value="'.$ks.'">'.$v.'</option>';
            }?>
          </select>
        </div>
        <div class="col col-1 col--have--btn">
          <button type="submit" class="btn btn-search"><span>絞り込み</span></button>
        </div>
      </form>
    </div>
    <div class="pull-right"><a href="<?php echo $this->webroot?>company/download/<?php echo $id?>" class="btn-donwload-csv">応募者一覧データを <br> ダウンロード（CSV形式）</a></div>
    <div class="clear-both"></div>
  </div>
  <div class="box_content u-mt20">
    <table class="rwd-tables list_entry">
      <tr class="tr_header">
        <th width="9%" class="radius-left-top"><span><a style="color: #fff;text-decoration: underline" href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'] ?>/entry/<?php echo $id.$param ?>">管理番号</a> </span></th>
        <th width="18%"><span>応募日付</span></th>
        <th width="18%"><span>氏名</span></th>
        <th width="5%"><span>性別</span></th>
        <th width="5%"><span>年齢</span></th>
        <th width="5%"><span>評価</span></th>
        <th><span>希望雇用形態</span></th>
        <th width="10%"><span>ステータス</span></th>
        <th width="13%" class="radius-right-top"></th>
      </tr>

      <?php if (isset($list_entry)):
      $stt =  1;
      foreach ($list_entry as $value) {
      $rs = $value['Entry'];
      $class = "";
      if($rs['status'] == 6){
        $class = 'class="close"';
      }

      ?>
      <tr <?php echo $class?>>
        <td>
          <div class="display_table">
            <span class="rwd-tables thead">管理番号</span>
            <span class="rwd-tables tbody"><?php echo $rs['id']?>
              <?php if($rs['is_read']==0){
                echo "<span class='icon--new'>New</span>";
              }
              ?>
            </span></div>
        </td>
        <td>
          <div class="display_table">
            <span class="rwd-tables thead">応募日付</span>
            <span class="rwd-tables tbody"><?php echo $rs['created_at']?></span></div>
        </td>
        <td>
          <div class="display_table">
            <span class="rwd-tables thead">氏名</span>
            <span class="rwd-tables tbody"><?php echo $rs['name']?></span></div>
        </td>
        <td align="center">
          <div class="display_table">
            <span class="rwd-tables thead">性別</span>
            <span class="rwd-tables tbody"><?php echo $rs['gender']?></span></div>
        </td>
        <td align="center">
          <div class="display_table">
            <span class="rwd-tables thead">年齢</span>
            <span class="rwd-tables tbody">
              <?php
              if($rs['birthday']){
                $age = date("Y") - date("Y", strtotime($rs['birthday']));
                echo $age;
              }
              ?>
            </span></div>
        </td>
        <td align="center">
          <div class="display_table">
            <span class="rwd-tables thead">評価</span>
            <span class="rwd-tables tbody">
              <?php
              if($rs['rate']) {

                if($rs['rate']==1){
                  $__class = 'status__status--circle';
                }elseif ($rs['rate']==2){
                  $__class = 'status__status--triangle';
                }elseif ($rs['rate']==3){
                  $__class = 'status__status--close';
                }else{
                  $__class = "";
                }
                echo '<span class="status '.$__class.'">'.$list_rate[$rs['rate']].'</span>';
              }else{
                echo '<span class="status status__status--nothing">-</span>';
              }
              ?>
            </span>
          </div>
        </td>
        <td>
          <div class="display_table">
            <span class="rwd-tables thead">希望雇用形態</span>
            <span class="rwd-tables tbody"><?php echo $rs['hope_employment']?></span></div>
        </td>
        <td class="radius--bottom">
          <div class="display_table">
            <span class="rwd-tables thead">ステータス</span>
            <span class="rwd-tables tbody">
              <?php
              if($rs['status']){
                  if($rs['status']==3) {
                    echo '<span class="color--blue">' . $list_status[$rs['status']] . '</span>';
                  }elseif($rs['status'] == 4){
                    echo '<span class="color--red">' . $list_status[$rs['status']] . '</span>';
                  }else{
                    echo $list_status[$rs['status']];
                  }
              }else{
                echo '<span class="color--red">末返信</span>';
              }

              ?>
            </span>
          </div>
        </td>
        <td align="center" class="td_btn"><a href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'] ?>/entry/<?php echo $id?>/detail/<?php echo $rs['id']?>" class="btn btn-success btn--arrow">詳細を確認<i class="arrow"></i></a></td>
      </tr>

        <?php $stt++;
      }
        unset($list_entry);
      else: ?>
        <tr>
          <td colspan="9" class="text-center">データなし</td>
        </tr>
      <?php endif; ?>
    </table>
  </div>
  <div class="btn_search_mobile sp"><a class="btn btn-open-popup-search"><span>絞り込み検索</span></a></div>
  <div class="row_paging">
    <div class="pull-left pc"><a href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'];?>" class="return_page">< <span>掲載求人一覧に戻る</span></a></div>

    <div class="paging pull-right">
        <?php
        $urlParams = $this->params['url'];
        unset($urlParams['url']);
        $this->Paginator->options(array(
          'url'=> array('controller' => 'job',
            'action' => $User['User']['id'],
            'entry',
            $id,
            '?' => http_build_query($urlParams)
          )));
        if ($this->Paginator->hasPage(2)) {
          echo $this->Paginator->prev(__('prev'), array('tag' => 'span'), null, array('tag' => 'span', 'class' => 'prev disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'current','class'=>'numbers', 'tag' => 'span', 'first' => 1));
          echo $this->Paginator->next(__('next'), array('tag' => 'span', 'currentClass' => 'disabled'), null, array('tag' => 'span', 'class' => 'next disabled', 'disabledTag' => 'a'));
        }
        ?>
    </div>

  </div>
</div>

<div class="popup popup_search_mobile">
  <div class="popup__content"><a class="popup-btn-close btn--close--x sp">X</a>
    <form action="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'];?>/entry/<?php echo $id?>" method="get" name="form_filter">
      <div class="col col-1">
        <select name="gender" class="form-control">
          <option value="">性別</option>
          <?php foreach ($gender as $v){
            $selected = (isset($this->request->query['gender']) && $this->request->query['gender'] == $v) ? "selected" : "" ;
            echo '<option '.$selected.' value="'.$v.'">'.$v.'</option>';
          }?>
        </select>
      </div>

      <div class="col col-2">
        <select name="age" class="form-control">
          <option value="">年齢</option>
          <?php foreach ($list_age as $va){
            $selected = (isset($this->request->query['age']) && $this->request->query['age'] == $va) ? "selected" : "" ;
            echo '<option '.$selected.' value="'.$va.'">'.$va.'代</option>';
          }?>

        </select>
      </div>

      <div class="col col-1">
        <select name="filter_rate" class="form-control">
          <?php foreach ($list_rate as $kr => $vr){
            $selected = (isset($this->request->query['filter_rate']) && $this->request->query['filter_rate'] == $kr) ? "selected" : "" ;
            echo '<option '.$selected.' value="'.$kr.'">'.$vr.'</option>';
          }?>
        </select>
      </div>
      <div class="col col-2">
        <select name="type_job_cat" class="form-control">
          <option value="">希望雇用形態</option>
          <?php
          $list_type_job_cat = $this->App->get_type_job_category();
          foreach ($list_type_job_cat as $v){
            $selected_t = (isset($this->request->query['type_job_cat']) && $this->request->query['type_job_cat'] == $v['JobCategory']['name']) ? "selected" : "" ;
            echo '<option '.$selected_t.' value="'.$v['JobCategory']['name'].'">'.$v['JobCategory']['name'].'</option>';
          }?>
        </select>
      </div>

      <div class="col col-2">

        <select name="filter_status" class="form-control">
          <option value="">ステータス</option>
          <?php foreach ($list_status as $ks => $v){
            $selected = (isset($this->request->query['filter_status']) && $this->request->query['filter_status'] == $ks) ? "selected" : "" ;
            echo '<option '.$selected.' value="'.$ks.'">'.$v.'</option>';
          }?>
        </select>
      </div>
      <div class="col col-1">
        <button type="submit" class="btn btn-search"><span>絞り込み</span></i></button>
      </div>
    </form>
  </div>
</div>







