<?php
$User = $this->Session->read('User');
?>
<div id="page-wrapper" class="container">
  <div class="div_title u-mt40">
    <div class="title_page">掲載求人一覧</div>
  </div>

  <?php
  if (!empty($notification)) {
    //pr($notification);
    $i = 0;
    $out = "";
    foreach ($notification as $notice) {
      if($notice['SecretJob']['num_entry_non_read'] > 0) {
        $i ++;
        $store_name = $notice['SecretJob']['store_name'];

        if (!empty($notice['SecretJob']['list_job'])) {
          $list_job_note = $this->App->get_name_job(trim($notice['SecretJob']['list_job'], ","));
        } else {
          $list_job_note = array();
        }
        $job_cat_name_note = "";
        $job_cat_out = "";
        if ($list_job_note) {
          foreach ($list_job_note as $v) {
            $job_cat_name_note .= $v['JobCategory']['name'] . "/";
          }

          $job_cat_name_note = substr($job_cat_name_note,0,-1);
          $job_cat_out = "/".$job_cat_name_note;
        }
        if(empty($store_name)){
          $job_cat_out = ltrim($job_cat_out, '/');
        }
        
        $link = $this->webroot.'job/'.$User['User']['id'].'/entry/'.$notice['SecretJob']['id'];
        $out .=  '<li><a href="'.$link.'"> 「' . $store_name. $job_cat_out. '」に新しい応募が' . $notice['SecretJob']['num_entry_non_read'] . '件きました。</a></li>';
      }
    }

    $showmore = "";
    if($i >3){
      $showmore = '<div class="text-center u-mt20 sp"><span class="btn_show_more">すべて表示</span></div>';
    }


    if(!empty($out)){
      echo '<div class="box_notificate u-mt30"><ul class="box_notificate__ul">'.$out.'</ul>'.$showmore.'</div>';
    }
    
  }
  ?>

  <div class="box_content u-mt30">
    <table class="rwd-tables">
      <tr class="tr_header">
        <th width="10%" class="radius-left-top"><span>求人ID</span></th>
        <th width="20%"><span>掲載期間</span></th>
        <th width="28%"><span>店舗名</span></th>
        <th><span>職種</span></th>
        <th width="8%"><span>応募者数</span></th>
        <th width="10%" class="radius-right-top"></th>
      </tr>

      <?php if (isset($list_job)):
        $stt = 1;
        //pr($list_job);

        foreach ($list_job as $value) {
          $rs = $value['SecretJob'];
          if (!empty($rs['list_job'])) {
            $list_job = $this->App->get_name_job(trim($rs['list_job'], ","));
          } else {
            $list_job = array();
          }

          ?>
          <tr>
            <td>
              <div class="display_table">
                <span class="rwd-tables thead">求人ID</span>
                <span class="rwd-tables tbody"><a target="_blank" href="/limited_kyujin/<?php echo $rs['id'] ?>"
                                                  class="link_detail"><?php echo $rs['id'] ?></a></span></div>
            </td>
            <td>
              <div class="display_table">
                <span class="rwd-tables thead">掲載期間</span>
          <span class="rwd-tables tbody">
             <?php
             echo ($rs['start_at']) ? date("Y/m/d", strtotime($rs['start_at'])) : "";
             ?>

             <?php
             echo ($rs['end_at']) ? " ~ " . date("Y/m/d", strtotime($rs['end_at'])) : "";
             ?>

          </span></div>
            </td>
            <td>
              <div class="display_table">
                <span class="rwd-tables thead">店舗名</span>
                <span class="rwd-tables tbody"><?php echo $rs['store_name'] ?></span></div>
            </td>
            <td>
              <div class="display_table">
                <span class="rwd-tables thead">職種</span>
          <span class="rwd-tables tbody">
              <?php
              $job_cat_name = "";
              if ($list_job) {
                foreach ($list_job as $v) {
                  $job_cat_name .= $v['JobCategory']['name'] . ", ";
                }

                $job_cat_name = substr($job_cat_name,0,-2);
              }
              echo $job_cat_name;
              ?>
          </span></div>
            </td>
            <td class="radius--bottom">
              <div class="display_table">
                <span class="rwd-tables thead">応募者数</span>
          <span class="rwd-tables tbody"><?php echo $value[0]['num_entry'] ?>
            <?php
            if ($value['SecretJob']['num_entry_non_read']) {
              echo '<span class="total_new">' . $value['SecretJob']['num_entry_non_read'] . '</span>';
            }

            ?>
          </span></div>
            </td>
            <td class="td_btn"><a
                href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'] ?>/entry/<?php echo $rs['id'] ?>"
                class="btn btn-success btn--arrow">応募者一覧<i class="arrow"></i></a></td>
          </tr>


          <?php $stt++;
        }
        unset($list_job);
      else: ?>
        <tr>
          <td colspan="6">データなし</td>
        </tr>
      <?php endif; ?>

    </table>
  </div>

  <div class="row_paging">
    <div class="paging paging_no_box_search pull-right">
      <?php
      if ($this->Paginator->hasPage(2)) {
        echo $this->Paginator->prev(__('prev'),
          array('tag' => 'span'),
          null,
          array('tag' => 'span', 'class' => 'prev disabled', 'disabledTag' => 'a')
        );
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'current', 'tag' => 'span', 'first' => 1, 'class' => 'numbers'));
        echo $this->Paginator->next(__('next'),
          array('tag' => 'span'),
          null,
          array('tag' => 'span', 'class' => 'next disabled', 'disabledTag' => 'a')
        );
      }
      ?>
    </div>
  </div>

</div>


