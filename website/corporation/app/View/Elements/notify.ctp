<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">

      <!-- Warning -->
      <?php if(!empty($errors)): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?php 
            echo '<strong>'. AppHelper::getLang('Warning') . ' !</strong> '; 
            foreach ($errors as $key => $value) {
              echo '<br/>' . $value[0];
            }
          ?>
        </div>
      <?php endif;?>

      <!-- Success -->
      <?php if($session = $this->Session->flash()): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?php echo '<strong>'. AppHelper::getLang('Success') . ' !</strong> ' . $session; ?>
        </div>
      <?php endif; ?>
      
    </div>
  </div>
</div>