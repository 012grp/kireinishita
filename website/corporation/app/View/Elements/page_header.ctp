<div class="row page_header">
  <div class="col-lg-3 col-lg-offset-1">
    <h3>
    	<?php
    		echo $page_header_title;
    		echo ' <small>' . $page_header_action . '</small>';
    	?>
    </h3>
  </div>
</div>