<?php 
  echo AppHelper::okeFlash($this->Session->flash('ok'));
  echo AppHelper::errorFlash($this->Session->flash('error')); 
  if($session = $this->Session->flash())
    echo AppHelper::okeFlash($session);
  if(!empty($errors))
    echo AppHelper::showErrors($errors);
?>