<?php
    $fullName = !empty($company['CompanyProfiles']['name']) ? $company['CompanyProfiles']['name'] : $curr_user['fullname'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style type="text/css">
        /* Client-specific Styles */
        #outlook a {
            padding: 0;
        }

        /* Force Outlook to provide a "view in browser" button. */
        body {
            font-family: Arial;
            width: 100% !important;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        /* Force Hotmail to display emails at full width */
        body {
            -webkit-text-size-adjust: none;
        }

        /* Prevent Webkit platforms from changing default text sizes. */

    </style>

</head>
<body>

<p>Dear <?php echo $contact_email?>,</p>
<p>You have an invitation from <strong><?php echo $fullName;?></strong></p>
<p><strong><?php echo $fullName?></strong> has invited you to become a member of <strong><?php echo $fullName ?></strong></p>
<p>Please click the link below to accept the invitation</p>
<p><a href="<?php echo $url ?>" target="_blank"><?php echo $url ?></a></p>
<p>p/s: This is auto mail send from JCM system. Please don't reply </p>
<p>Thanks you very much.</p>
<p>Best regards.</p>
<em>Copyright&copy;<?php echo date('Y')?>JCM,All rights reserved</em>
<br/>
<br/>
<br/>
<br/>
<p>Xin chào <?php echo $contact_email;?>,</p>
<p>Bạn có một lời mời từ <strong><?php echo $fullName ?></strong></p>
<p><strong><?php echo $fullName ?></strong> mời bạn trở thành thành viên trong doanh nghiệp của  <strong><?php echo $fullName ?></strong></p>
<p>Bạn vui lòng click vào link bên dưới đây để chấp nhận lời mời</p>
<p><a href="<?php echo $url ?>" target="_blank"><?php echo $url ?></a></p>
<p>p/s: Đây là mail tự động gửi từ hệ thống xin vui lòng không trả lời.</p>
<p>Cảm ơn rất nhiều.</p>
<p>Trân trọng.</p>
<em>Copyright&copy;<?php echo date('Y') ?>JCM,All rights reserved</em>

</body>
</html>