パスワード再設定のご案内

【ご登録のメールアドレス（ID）】
<?php echo $fullname; ?>
以下のパスワード再設定用URLにアクセスして、パスワードを再設定してください。

【パスワード再設定用URL】
<?php echo $linkreset;?>
※URL発行後24時間が過ぎてしまった場合は、パスワード再設定用URLは利用できません。
お手数おかけしますが、再度登録をやり直してください。

<?php echo $this->element('../Emails/text/_signature'); ?>