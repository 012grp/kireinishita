<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include('include/head.ctp'); ?>
</head>

<body>
<?php $User = $this->Session->read('User');?>
<?php include('include/_header.ctp'); ?>

<?php echo $this->fetch('content'); ?>

<ul id="nav-box-mobile" data-target-hamburgermenu="hamburger" class="nav-box-mobile u-fs--m">
    <li id="naxBoxTop" class="nav-box__list--top">
        <div data-target-close="target" class="_btn_close_nav_box"><span class="u-fs--xxxs">MENU</span></div>
    </li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'company', 'action' => 'job', $User['User']['id'])); ?>"><span>管理画面トップ</span><i>></i></a></li>
    <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'edit', $User['User']['id'])); ?>"><span>パスワード変更</span><i>></i></a></li>
    <li><a href="/contact?id=<?php echo base64_encode($User['User']['id'])?>" target="_blank"><span>お問い合わせ</span><i>></i></a></li>
    <li class="logout"><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>"><span>ログアウト</span></a></li>
</ul>
<?php  echo $this->Html->script('app');?>
</body>

</html>
