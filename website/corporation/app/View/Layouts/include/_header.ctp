<header class="header--primary">
    <div class="header--primary__inner u-cf">
        <h1 class="header--primary__logo"><a href="<?php echo $this->webroot; ?>job/<?php echo $User['User']['id'];?>"><img src="<?php echo $this->webroot; ?>img/svg/top-logo.svg" alt="キレイにスルンダ">
            <span>求人管理画面</span>
            </a></h1>
        <div class="header--primary__right">
            <div data-trigger-hamburgermenu="hamburger" class="nav--hamburger"><span class="nav--hamburger__list"></span><span class="nav--hamburger__list"></span><span class="nav--hamburger__list"></span><span class="nav--hamburger__text u-fs--xxxs"></span></div>
            <ul class="navbar--right">
                <li class="dropdown"><a href="#" id="dropdown" data-toggle="dropdown" class="dropdown-toggle"><span class="icon-user"><?php echo $User['User']['name']; ?> 様<i class="arrow-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'edit', $User['User']['id'])); ?>"><i class="fa-arrow"></i>パスワード再設定</a></li>
                        <li><a href="/contact?id=<?php echo base64_encode($User['User']['id']); ?>" target="_blank"><i class="fa-arrow"></i>お問い合わせ</a></li>
                        <li class="logout"><a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>" class="btn-logout">ログアウト</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>