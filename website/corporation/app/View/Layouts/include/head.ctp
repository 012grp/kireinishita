<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo (!empty($page_title_default)) ?  Configure::read('TITLE_PAGE')."-".$page_title_default : Configure::read('TITLE_PAGE')?></title>
<?php
    
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');

    echo $this->Html->css('app');
    echo $this->fetch('css');


    echo $this->fetch('script');

?>