<div class="header_menu">
    <div class="left title-menu">Menu</div>
    <div class="icon-bar"><a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a></div>
    <div class="clearfix"></div>
</div>
<ul class="nav left-menu" id="side-menu">
    <?php if($this->Session->read('Auth.User.id') == 1){?>
    <li>
        <a class="left-menu" href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index'), true);?>">
            <span class="icon-left-menu"><img src="/img/icon/icon_home.png"></span> <span class="title-left-menu">アライアンス管理</span>
        </a>
    </li>
    <?php }?>
    <li>
        <a href="<?php echo $this->Html->url(array('controller' => 'survey', 'action' => 'index'), true);?>" class="left-menu" ><span class="icon-left-menu"><img src="/img/icon/icon_manager_jobs.png"></span> <span class="title-left-menu">アンケート一覧
</span></a>
    </li>

</ul>