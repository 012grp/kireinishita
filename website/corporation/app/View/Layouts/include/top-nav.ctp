<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <span class="cus-icon icon-user"></span>
        <?php
        $currentUser = $this->Session->read('User');
        //pr($currentUser);
        ?>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'edit', $currentUser['User']['id'])); ?>">
                <i class="fa fa-fw fa-user"></i>Profile
            </a>
        </li>

        <li>
            <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>">
                <i class="fa fa-fw fa-power-off"></i> ログアウト
            </a>
        </li>
    </ul>
</li>