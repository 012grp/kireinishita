<?php
  // form.
  $form_create = array('url' =>$action,
                      'role' =>'form',
                      'method' => 'post',
                      'class' => ""
                      );

    $alert_pass_old = "";
    $cls_err_pass_old = "";
    if(!empty($pass_old)){
      $alert_pass_old = '<p class="label_error u-mt10">※パスワードが正しくありません</p>';
      $cls_err_pass_old = ' error';
    }

    $alert_err_pass = "";
    $cls_err_pass = "";
    if(!empty($error_pass)){
      $alert_err_pass = '<p class="label_error u-mt10">※8桁以上の英数字をご入力ください</p>';
      $cls_err_pass = ' error';
    }

  // password.
    $passOldOption = array('class'       => 'form-control'.$cls_err_pass_old,
      'type'        => 'password',
      'label'       => false,
      'required'    => true,
      'error'       => false,
      'placeholder' => '現在のパスワード',
    );

    $passOption = array('class'       => 'form-control'.$cls_err_pass,
                        'type'        => 'password',
                        'label'       => false,
                        'required'    => true,
                        'error'       => false,
                        'placeholder' => '新しいパスワード（8文字以上の半角英数字）',
                        'id'          =>"password",
                        'pattern' => '[a-z0-9]{8,}'
                      );

  $confirmPassOption = array('class'       => 'form-control',
                             'type'        => 'password',
                              'label'       => false,
                             'required'    => true,
                             'error'       => false,
                              'id'          => 'confirm_password',
                             'placeholder' => 'もう一度新しいパスワードをご入力ください'
                             );



?>

<div class="form--login__middle form_resetpass_center">
  <?php echo $this->Form->create('User', $form_create); ?>

    <div class="form-group">
      <?php echo $this->Form->input('password_old', $passOldOption);
            echo $alert_pass_old;
      ?>
    </div>
    <div class="form-group">
      <?php echo $this->Form->input('password', $passOption);
        echo $alert_err_pass;
      ?>
    </div>
    <div class="password">
      <?php echo $this->Form->input('password_confirm', $confirmPassOption); ?>
    </div>
    <div class="row u-mt30">
      <button type="submit" class="btn btn-success btn--full">リセット</button>
    </div>
  <?php echo $this->Form->end(null); ?>
</div>



<script type="text/javascript">
  var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

  function validatePassword(){
    if(password.value != confirm_password.value) {
      confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
      confirm_password.setCustomValidity('');
    }
  }

  password.onchange = validatePassword;
  confirm_password.onkeyup = validatePassword;

</script>
