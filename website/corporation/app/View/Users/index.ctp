<?php
$this->start('css');
echo $this->Html->css('bootstrap-tabs-custome');
echo $this->Html->css('manage-job');
$this->end('css');
?>

<div class="padding-10">
  <a href="<?php $this->webroot?>users/add" class="btn btn-primary">新規追加</a>
</div>

<div class="table-responsive page_table">
  <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <?php
          echo '<th width="20">No</th>';
          echo '<th>ID（Eメール）</th>';
          echo '<th width="100">会社名</th>';
          echo '<th width="120"> ... </th>';
        ?>
      </tr>
    </thead>
    <tbody>

                <?php if (isset($user)):
                    $stt =  1;
                    foreach ($user as $value) {
                        $user = $value['User'];
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $stt; ?></td>
                            <td>
                                <a class="job-title" href="<?php echo $this->html->url(
                                    array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>">
                                    <?php echo $user['email']; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php echo $user['company']; ?></td>
                            <td class="text-center">
                                <?php echo $this->Html->link('編集',
                                    array('controller' => 'users', 'action' => 'edit', $user['id']),
                                    array('class' => 'btn btn-edit btn-xs ')
                                ); ?>

                                <?php
                                
                                echo $this->Html->link('削除',
                                  array('controller' => 'users', 'action' => 'delete', $user['id']),
                                  array('confirm'=>'Are you sure you want to delete?',
                                    'class' => 'btn btn-edit btn-xs ')
                                ); ?>
                            </td>
                        </tr>
                        <?php $stt++;
                    }
                    unset($user);
                else: ?>
                    <tr>
                        <td colspan="10">データなし</td>
                    </tr>
                <?php endif; ?>
    </tbody>
  </table>
</div>





