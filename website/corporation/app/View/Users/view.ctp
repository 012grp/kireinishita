<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?php echo h($user['User']['fullname']); ?> <small>profile</small>
        </h1>
    </div>
</div>
<div class="alert alert-info">
    <h1>Email: <?php echo h($user['User']['email']); ?></h1>
    <p>Birth Date: <?php echo h($user['User']['birthdate']); ?></p>
    <p>Created: <?php echo h($user['User']['created']); ?></p>
</div>