(function () {
	var class_list = {
		interview:{
			h1:'column-box_contents__ttl u-fwb',
			//h2:'column-box_contents__ttl u-fs--xxxlh--interview u-fwb u-mt60',
			h2:'column-box_contents__subTtl u-fs--xxlh--contents u-fwb',
			//h2border:'column-box_contents__ttl column-box_contents__ttl--border u-fs--xxxlh--interview u-fwb u-mt60',
			h2border:'column-box_contents__subTtl column-box_contents__ttl--border u-fs--xxlh--contents u-fwb',
			h3:'column-box_contents__subTtl--interview u-fs--xxlh--contents u-fwb',
			h360:'column-box_contents__subTtl--interview u-fs--xxlh--contents u-fwb u-mt60',
			p:'column-box_contents__text u-fs--lh--contents u-mt15',
		},
		
		default:{
			h1:'column-box_contents__ttl u-fwb',
			h2:'column-box_contents__subTtl u-fs--xxlh--contents u-fwb',
			h2border:'column-box_contents__subTtl column-box_contents__ttl--border u-fs--xxlh--contents u-fwb',
			h3:'column-box_contents__subTtl u-fs--xl u-fwb no-border',
			h360:'column-box_contents__subTtl u-fs--xl u-fwb u-mt60 no-border',
			p:'column-box_contents__text u-fs--lh--contents u-mt15',
		}
	};
	var get_class = function(obj)
	{
		    var cur = getCurrentTypeClassName();
		
		
		switch(cur)
		{
			case 'interview':
				var rs = class_list['interview'][obj] || '';
				return rs;
			break;
				
			default:
				var rs = class_list['default'][obj] || '';
				return rs;
		}
	}

	
	var getAndReplace = function(obj, text)
	{

		if(obj.selection.element())
		{
			text = obj.selection.element().innerText || text;
			obj.selection.element().outerHTML = '';
		}
		return text;
	}
	
	//big H2
	$.FroalaEditor.DefineIcon('runda_h2_wrapper', {NAME: 'H2', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_h2_wrapper', {
      title: 'Default title for all category / すべてのカテゴリのデフォルトのタイトル',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function (c) {
		  
		var txt = getAndReplace(this,'タイトルH2');
		  
		var html ='<h2 class="'+get_class('h2')+'">'+txt+'</h2>';
        this.html.insert(html);
      }
    });
	
	//big H2 border
	$.FroalaEditor.DefineIcon('runda_h2border_wrapper', {NAME: 'H2-Border', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_h2border_wrapper', {
      title: 'the underline title for Interview / インタビューのタイトルを下線',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function (c) {
		  
		var txt = getAndReplace(this,'タイトルH2Border');
		  
		var html ='<h2 class="'+get_class('h2border')+'">'+txt+'</h2>';
        this.html.insert(html);
      }
    });
	
	//special H3 margin 60
	$.FroalaEditor.DefineIcon('runda_h3_60_wrapper', {NAME: 'H3-60', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_h3_60_wrapper', {
      title: 'Small title for Interview with margin top 60 / マージントップ60のインタビューのための小さなタイトル',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var txt = getAndReplace(this,'タイトルH360');
		 var html ='<h3 class="'+get_class('h360')+'">'+txt+'</h3>';
         this.html.insert(html);
      }
    });
	
	//H3
	$.FroalaEditor.DefineIcon('runda_h3_wrapper', {NAME: 'H3', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_h3_wrapper', {
      title: 'the small title for Interview / インタビューのための小さなタイトル',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var txt = getAndReplace(this,'タイトルH3');
		 var html ='<h3 class="'+get_class('h3')+'">'+txt+'</h3>';
         this.html.insert(html);
      }
    });
	
	//P
	$.FroalaEditor.DefineIcon('runda_p_wrapper', {NAME: 'P', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_p_wrapper', {
      title: 'Content style / コンテンツのスタイル',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var txt = getAndReplace(this,'コンテンツ');
		 var html ='<p class="'+get_class('p')+'">'+txt+'</p>';
         this.html.insert(html);
      }
    });
	
	
	//P
	$.FroalaEditor.DefineIcon('runda_address_wrapper', {NAME: '住所Block', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_address_wrapper', {
      title: 'ウェブサイトとアドレスを挿入します',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html = '<div class="_contents__bottom__block--info u-fs--lh--contents u-mt20">'+
		 			'<p class="contents-box__text u-fs--mh">ホームページ：<a href="#">コンテンツ</a></p>'+
                    '<p class="contents-box__text u-fs--mh">住所：コンテンツ</p>'+
                    '</div><p></p>';
         this.html.insert(html);
      }
    });
	
	
	//Q/A
	$.FroalaEditor.DefineIcon('runda_qa_wrapper', {NAME: 'QA Block', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_qa_wrapper', {
      title: 'QAブロック',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html = '<div class="_detail__block__qa"><span class="c-tag--qa--s--contents u-fs--xs u-fwb">Question</span>'+
						'<h2 class="u-fs--xxlh--contents u-fwb u-mt5">質問のタイトル</h2>'+
						'<p class="column-box_contents__text u-fs--lh--contents u-mt15">質問の内容</p>'+
						'<span class="c-tag--qa--s--contents u-fs--xs u-fwb u-mt55">Answer</span>'+
						'<h2 class="u-fs--xxlh--contents u-fwb u-mt5">回答のタイトル</h2>'+
						'<p class="column-box_contents__text u-fs--lh--contents u-mt25">回答内容</p>'+
				  	'</div><p></p>';
         this.html.insert(html);
      }
    });
})()