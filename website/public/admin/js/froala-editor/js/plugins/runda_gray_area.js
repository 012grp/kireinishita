(function () {
	$(document).on('click','._remove__overview', function (){
		$(this).parents('._contents__overview').remove();
	});
	
	$.FroalaEditor.DefineIcon('runda_gray_area', {NAME: 'グレーのパネル', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_gray_area', {
      title: 'グレーのパネル',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html ='<div class="_contents__overview u-mt30 u-fs--lh--contents--other">'
			 	+'<i class="_remove__overview" style="display:none">x</i>'
                +'<h3 class="u-fwb">タイトル1</h3>'
                +'<ul>'
                  +'<li>コンテンツ1</li>'
                  +'<li>コンテンツ2</li>'
                +'</ul>'
              +'</div>'
              +'<p></p>';
         this.html.insert(html);
      }
    });
	
	$.FroalaEditor.DefineIcon('runda_gray_area_top', {NAME: 'グレーのパネルTOP', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_gray_area_top', {
      title: 'グレーのパネルTOP',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html ='<div class="_contents__overview u-mt30 u-fs--lh--contents--other">'
			 	+'<i class="_remove__overview" style="display:none">x</i>'
                +'<h3 class="u-fwb">タイトル1<br>タイトル2</h3>'
                +'<ul class="u-mt10">'
                  +'<li>コンテンツ1</li>'
                  +'<li>コンテンツ2</li>'
                +'</ul>'
              +'</div>'
		  	  +'<p></p>';
         this.html.insert(html);
      }
    });
	
	$.FroalaEditor.DefineIcon('runda_gray_area_qa', {NAME: 'グレーのパネル(QA)', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_gray_area_qa', {
      title: 'グレーのパネルTOP (QA)',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html =
			 '<div class="_contents__overview u-mt30 u-fs--lh--contents">'
                +'<h3 class="u-fwb _contents__overview__otherTtl">タイトル</h3>'
                +'<ul>'
                  +'<li>コンテンツ1</li>'
                  +'<li>コンテンツ2</li>'
                +'</ul>'
              +'</div><p></p>';
			 
         this.html.insert(html);
      }
    });
})()