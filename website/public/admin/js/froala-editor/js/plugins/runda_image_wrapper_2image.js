(function () {
	$.FroalaEditor.DefineIcon('runda_image_wrapper_2image', {NAME: '画像(pc/sp)', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_image_wrapper_2image', {
      title: 'グレーの画像',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html ='<div class="_contents__img__block u-mt25">'
				+'<img class="fr-pc fr-dib fr-draggable" src="/public/admin/img/no_image_pc.jpg">'
				+'<img class="fr-sp fr-dib fr-draggable" src="/public/admin/img/no_image_sp.jpg">'
                +'<div class="_contents__img_text u-mt10">コンテンツ</div>'
              +'</div><p></p>';
         this.html.insert(html);
      }
    });
})()