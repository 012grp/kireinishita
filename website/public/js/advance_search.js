/*
README:
	__requestWaysite: ARRAY gets checked value of WAYSITE in 'input#requestWaysite'
	__addCities: FUNCTION gets list of city follow province id by AJAX and add to modal #city_checkboxes
	__requestWaysite: FUNCTION gets list of siteway follow province id by AJAX from  www.ekidata.jp API and add to modal #waysite_checkboxes 
	
	when 'select#pro_select' change, it will:
	call: __addCities and __addWaysites functions
	
	when load page, if VALUE of 'select#pro_select' is not empty, it will:
	call: __addWaysites
*/




//Get values of checked waysite from a input hidden tag
var __requestWaysite = document.getElementById('requestWaysite').value || "";
__requestWaysite = __requestWaysite == "" ? [] : __requestWaysite.split(",");

//Checkbox template
var __checkbox_template = function (index, id, name, value, text, className, isChecked) {
	return '<input ' + (isChecked ? 'checked' : '') + ' form="searchForm" value="' + value + '" type="checkbox" name="' + name + '" id="' + id + '" data-num="' + index + '" class="search-checkbox ' + className + '">' +
		'<label for="' + id + '" class="search-checkbox__label--modal u-mt10">' + text + '</label>'
}

//ajax json function
var __getJson = function (url, callback) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try {
				var data = JSON.parse(xhttp.responseText);
				callback(data);
			} catch (e) {
				console.log(e);
			}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

//Change waysidemodal checkboxes of prefmodal (沿線・駅)
//Get waysite when province changed
//And add checkboxes to modal
var __addWaysites = function (_value, _text, _request) {
	//sStation: http://www.ekidata.jp/api/l/" + _value + ".json
	var sWaysite = document.getElementsByTagName("head")[0].appendChild(document.createElement("script"));
	sWaysite.type = "text/javascript";
	sWaysite.charset = "utf-8";
	sWaysite.src = "http://www.ekidata.jp/api/p/" + _value + ".json";
	sWaysite.addEventListener('load', function () {
		var data = xml.data.line;
		var html = '';
		for (var i in data) {
			var isChecked = false;
			if (_request.indexOf(data[i].line_cd.toString()) != -1) {

				isChecked = true;
			}
			html += __checkbox_template(i,
				'way' + data[i].line_cd,
				'way[]',
				data[i].line_cd,
				data[i].line_name,
				'waysite-checkbox',
				isChecked);
		}
		document.getElementById('waysite_checkboxes').innerHTML = html;
	});
}

//Get citys when province changed
//And add checkboxes to modal
var __addCities = function (_value, _text) {
	var url = '/area/city-by-province?province_id=' + _value;
	__getJson(url, function (jsonData) {
		var html = '';
		for (var i in jsonData) {
			html += __checkbox_template(i, 'city' + jsonData[i].id, 'city[]', jsonData[i].id, jsonData[i].name, 'city-checkbox', false);
		}

		document.getElementById('city_checkboxes').innerHTML = html;
	});
}

//ready: if province not empty will add waysite into modal
if (document.getElementById('pro_select').value != '') {
	__addWaysites(document.getElementById('pro_select').value, '', __requestWaysite);
}

//event change of province select tag
//Change checkboxes of city and waysite modal
document.getElementById('pro_select').addEventListener('change', function () {
	__addCities(this.value, '');
	__addWaysites(this.value, '', __requestWaysite);
});