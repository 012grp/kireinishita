(function () {

	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer
	{

	    /*Get values to make URL*/
		var __makeUrl = function () {
			
			//checkboxs
			var uri = [];
			var textboxKeyword = document.getElementById('search_keyword');
			var selectProvince = document.getElementById('pro_select');
			var checkboxes = document.querySelectorAll(".search-checkbox");
			
			if(textboxKeyword.value != '')
			{
				uri.push('k='+encodeURIComponent(textboxKeyword.value));
			}
			
			if(selectProvince.value != '')
			{
				uri.push('pro='+selectProvince.value);
			}
			
			for(var i in checkboxes)
			{
				if(checkboxes[i].checked)
				{
					uri.push(encodeURIComponent(checkboxes[i].name)+'='+checkboxes[i].value);
				}
			}
			
			var url = '/job/result?'+uri.join('&');
			return url;
		}

		
		document.forms["searchForm"].onsubmit = function(){
			var url = __makeUrl();
			window.location.href = url;
			return false;
		}
	}
	else  // If another browser
	{
	    //alert('otherbrowser');
	}


})()