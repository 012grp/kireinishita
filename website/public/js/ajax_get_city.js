
var arr_html = [];
var html;

document.getElementById('pro_select').addEventListener('change', function(){
	var xhttp = new XMLHttpRequest();
	var id = this.value;
	var url = '/area/city-by-province?province_id='+id;

	
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try
			{
				if(arr_html[id])
				{
					html = arr_html[id];
				}
				else
				{
					var data = JSON.parse(xhttp.responseText);
					html = '<option value="">市区町村</option>';
					for(var i in data)
					{
						html+='<option value="'+data[i].id+'">' + data[i].name + '</option>';
						 
					}
					arr_html[id] = html;
				} 
				
				//console.log(html);
				document.getElementById('city_select').innerHTML = html;

			}
			catch(e){}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
});


if(provinceId)
{
	var xhttp = new XMLHttpRequest();
	var id = provinceId;
	var url = '/area/city-by-province?province_id='+id;

	
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try
			{
				if(arr_html[id])
				{
					html = arr_html[id];
				}
				else
				{
					var data = JSON.parse(xhttp.responseText);
					html = '<option value="">市区町村</option>';
					for(var i in data)
					{
						if(cityId)
						{
							if(cityId == data[i].id)
							{
								html+='<option value="'+data[i].id+'" selected>' + data[i].name + '</option>';
							}
							else
							{
								html+='<option value="'+data[i].id+'">' + data[i].name + '</option>';
							}
						}
						else
						{
							html+='<option value="'+data[i].id+'">' + data[i].name + '</option>';
						}
					}
					arr_html[id] = html;
				} 
				
				//console.log(html);
				document.getElementById('city_select').innerHTML = html;

			}
			catch(e){}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

