var isBookmarkPage = window.location.pathname == '/mybox'?true:false;

function ajax_get(url, callback)
{

	var xhttp = new XMLHttpRequest();

	if(callback !== undefined)
	{
		xhttp.onreadystatechange = function(){
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				callback(xhttp.responseText);
			}
		}
	}
	xhttp.open("GET", url, true);
	xhttp.send();
}

function load_event_action(url, _button)
{
	ajax_get(url, function(data){
		try
		{
			if(data == 1)
			{
				_button.innerHTML = '削除する';
			}
			
		}
		catch(e){
			console.log(e)

		}
	});
}

function click_event_action(buttons)
{
	buttons.addEventListener('click', function() {
    	var _button = buttons;
		var id = this.getAttribute("data-id");
		var isSecretJob = this.getAttribute("data-secret");
		if(id != null && isSecretJob != null)
		{
			var xhttp = new XMLHttpRequest();
			var url = '/account/bookmark?id=' + id + '&is_secret_job=' +isSecretJob;

			xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					try
					{
						var result = xhttp.responseText;
						//un-login
						if(result == -1)
						{
							if (window.confirm('ログインページに移動しますか？'))
							{
							    window.location = '/account/';
							}
						}
						else //logged
						{
							if(result == 0) //no bookmark
							{
								//remove job if this is bookmark page
								if(isBookmarkPage)
								{
									try{
										_button.parentNode.parentNode.parentNode.remove();
										var totalJobBookmark = parseInt(document.getElementById('totalJobBookmark').innerHTML);
										totalJobBookmark -= 1;
										document.getElementById('totalJobBookmark').innerHTML = totalJobBookmark;
									}
									catch(e)
									{
										//if edit this HTML structure, it will go to here
									}
									
								}
								else
								{
									_button.innerHTML = '<span class="c-font-icon--attachment"></span>キープする';
								}
							}
							else if(result == 1) //bookmarked
							{
								_button.innerHTML = '削除する';
							}
							else
							{
								alert('Something went wrong please try again!');
							}
						}
					}
					catch(e){
						
					}
				}
			};
			xhttp.open("GET", url, true);
			xhttp.send();
		}
		else
		{
			alert('Button is invalid');
		}
    });
}

function bookmark_click_event()
{
	//click
		//data-id. data-is-secret
		//ajax 0,1,-1
		//-1 -> login
		//0 -> bookmark => unbookmark
		//1 -> unbookmark => bookmark
		//-2 -> message error
	var buttons = document.getElementsByClassName('bookmark_job');

	for (var i = 0; i < buttons.length; i++) 
	{
		click_event_action(buttons[i]);
	}
}


function bookmark_load_event()
{
	//if not mybook
	//if login
		//selector ...
		//if bookmarking => chuyen sang delete
	var buttons = document.getElementsByClassName('bookmark_job');
	
	for (var i = 0; i < buttons.length; i++) 
	{
		var id = buttons[i].getAttribute("data-id");
		var isSecretJob = buttons[i].getAttribute("data-secret");

		if(id != null && isSecretJob != null)
		{
			var url = '/account/check-bookmark?id=' + id + '&is_secret_job=' +isSecretJob;
			load_event_action(url, buttons[i]);
		}
		else
		{
			alert('Button is invalid');
		}
	    
	}
	
}

function check_login_to_load_bookmark(callback) {
	var x = new XMLHttpRequest();
	var path = window.location.pathname;
	var _url = '/account/check-login-to-load-bookmark?path='+path;
	x.onreadystatechange = function(){
		if (x.readyState == 4 && x.status == 200) {
			if(x.responseText == 1)
			{
				if(callback !== undefined)
				{
					callback();
				}
			}
			//callback(xhttp.responseText);
		}
	}
	
	x.open("GET", _url, true);
	x.send();
}



check_login_to_load_bookmark(bookmark_load_event);

bookmark_click_event();




