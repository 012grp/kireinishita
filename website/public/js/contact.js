function hideInputCompany()
{
	document.getElementById('company').style.display = 'none';
	document.getElementById('error_company').style.display = 'none';
	document.getElementById('company').removeAttribute("required");
	document.getElementById('company').removeAttribute('data-validate');
}

function showInputCompany()
{
	document.getElementById('company').style.display = 'block';
	document.getElementById('company').required = true;
	document.getElementById('error_company').removeAttribute("style");
	document.getElementById('company').dataset.validate = "target";
}

function comebackToContact()
{
	window.location.href = '/contact';
}

function resetForm()
{
	document.getElementById("company").value = '';
	document.getElementById("name").value = ''; 
	document.getElementById("mail").value = ''; 
	document.getElementById("content").value = '';
}


//check length of input if > 0 => add class valid
//if input change has length = 0 => add class error and remove class valid
var messageErrorCompany = document.getElementById('messageErrorCompany');
var inputCompany = document.getElementById('company');
if(inputCompany.value.length > 0)
{
	inputCompany.className += " valid";
}

var inputName = document.getElementById('name');
if(inputName.value.length > 0)
{
	inputName.className += " valid";
}

var inputEmail = document.getElementById('mail');
if(inputEmail.value.length > 0)
{
	inputEmail.className += " valid";
}

var inputContent = document.getElementById('content');
if(inputContent.value.length > 0)
{
	inputContent.className += " valid";
}

inputCompany.addEventListener("input", function (e) {
    if(inputCompany.value.length == 0)
	{
		inputCompany.className += " error";
		inputCompany.classList.remove("valid");
	}
});

inputName.addEventListener("input", function (e) {
    if(inputName.value.length == 0)
	{
		inputName.className += " error";
		inputName.classList.remove("valid");
	}
});


inputEmail.addEventListener("input", function (e) {
    if(inputEmail.value.length == 0)
	{
		inputEmail.className += " error";
		inputEmail.classList.remove("valid");
	}
});


inputContent.addEventListener("input", function (e) {
    if(inputContent.value.length == 0)
	{
		inputContent.className += " error";
		inputContent.classList.remove("valid");
	}
});

inputCompany.addEventListener("keyup", function(e){
	console.log(this.value);
	if(this.value == '')
	{
		document.getElementById('error_company').style.display = "block";
		this.dataset.validate = "target";
		messageErrorCompany.style.display = 'block';
	}
	else
	{
		document.getElementById('error_company').style.display = "none";
		this.removeAttribute("data-validate");
		messageErrorCompany.style.display = 'none';
	}
});
