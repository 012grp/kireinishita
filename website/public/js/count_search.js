(function () {
/*Ajax function*/
var __getAjax = function (url, callback, callback_error) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try {
				callback(xhttp.responseText);
			} catch (e) {
				console.log(e);
			}
		}
		else
		{
			if(callback_error !== undefined)
			{
				callback_error(xhttp.status);
			}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

/*Get values to make URL*/
var __makeUrl = function () {
	
	//checkboxs
	var uri = [];
	var textboxKeyword = document.getElementById('search_keyword');
	var selectProvince = document.getElementById('pro_select');
	var checkboxes = document.querySelectorAll(".search-checkbox");
	
	if(textboxKeyword.value != '')
	{
		uri.push('k='+encodeURIComponent(textboxKeyword.value));
	}
	
	if(selectProvince.value != '')
	{
		uri.push('pro='+selectProvince.value);
	}
	
	for(var i in checkboxes)
	{
		if(checkboxes[i].checked)
		{
			uri.push(encodeURIComponent(checkboxes[i].name)+'='+checkboxes[i].value);
		}
	}
	
	var url = '/job/ajax-count-search?'+uri.join('&');
	
	return url;
}

/*Count function*/
var __count = function () {
	var url = __makeUrl();
	
	__getAjax(url, 
	function (numberResult) {
		document.getElementById('btnAdvanceSubmit').value = numberResult+'件を検索';
	}, 
	function() {
		document.getElementById('btnAdvanceSubmit').value = '...件を検索';
	});
}

var employmentCheckboxes = document.querySelectorAll('.em-checkbox');
var closeModalButtons 	 = document.querySelectorAll('.close_modal_button');

/*Events*/
var eventFunc = function (obj, action) {
	obj.addEventListener(action, function(){
		__count();
	});
};

//When done press keyword
document.getElementById('search_keyword').addEventListener("blur", function () {
	__count();
});	
	
//When select 都道府県
eventFunc(document.getElementById('pro_select'), 'change');

//When close modal: 職種, こだわり, 市区町村の選択, 沿線・駅の選択
var mLeng = closeModalButtons.length;
for(var c=0; c<mLeng; c++)
{

	eventFunc(closeModalButtons[c], 'click');
}

//When checkbox 雇用形態
var cLeng = employmentCheckboxes.length;
for(var c=0; c<cLeng; c++)
{
	eventFunc(employmentCheckboxes[c], 'change');
}
	

})()