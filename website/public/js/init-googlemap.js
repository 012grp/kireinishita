//require : http://maps.google.com/maps/api/js;
//innit googlemap by location and show inforwindow
//input : latVal, lngVal (location of map), inforwindow (content of inforwindow will show on marker), zoom (number of distant in map)

function initMap( latVal, lngVal, inforwindow, zoom, id) {
    var x = latVal =! '' && latVal != undefined ? latVal: 0;
    var y = lngVal =! '' && lngVal != undefined ? lngVal: 0;
    var location = {lat: x, lng: y};
    
    //
    var map = new google.maps.Map(document.getElementById(id), {
        zoom: zoom,
        center: location
    });

    var contentString = inforwindow;

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: ''
    });

    if(contentString)
    {
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });


        google.maps.event.addListener(marker, 'mouseover', function () {
            infowindow.open(map, marker);
        });

        infowindow.open(map, marker);
    }
}