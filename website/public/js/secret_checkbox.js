function selectAll(checkbox){
	name = checkbox.getAttribute("name");
	group = name + "-child";

	if(checkbox.checked){
		for(i = 0; i < group.length; i++){
			if(!group[i].checked){
				group[i].checked = true;
			}
		}
	}

	else{
		for(i = 0; i < group.length; i++){
			if(group[i].checked){
				group[i].checked = false;
			}
		}
	}
}