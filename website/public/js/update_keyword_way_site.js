function create_obj(){
    var browse = navigator.appName;    
    if(browse == "Microsoft Internrt Explorer"){ 
        obj = new ActiveXObject("XMLHTTP");            
    }else{ 
        obj = new XMLHttpRequest();
    }            
    return obj; 
}

//var token = document.getElementsByTagName('input').item(name="_token").value;
var lineCd; 
var dataSend; 
var url;
var data;

function updateKeywordWaySite(lineCd) {
	var xhttp = create_obj(); 

	url = 'http://www.ekidata.jp/api/l/'+lineCd+'.json';

	var s = document.getElementsByTagName("head")[0].appendChild(document.createElement("script"));
	s.type = "text/javascript";
	s.charset = "utf-8";
	s.src = url;
		
	s.addEventListener('load', function () {
		var data = JSON.stringify(xml.data);
		dataSend = "&lineCd=" + lineCd + "&stationData=" + data;
		xhttp.open("POST", '/update/keyword-waysite', true);
		xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	 	xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xhttp.send(dataSend);
		s.remove();
	});

	// xhttp.onreadystatechange = function() {
	// 	if (xhttp.readyState == 4 && xhttp.status == 200) {
	// 		try
	// 		{
	// 			//show info on view waysite.update
	// 			var info = JSON.parse(xhttp.responseText);
	// 			var html = '';
	// 			for(var i in info)
	// 			{
	// 				html += '<tr>';
	// 				html += '<td>'+info[i].province_id+'</td>';
	// 				html += '<td>'+info[i].line_cd+'</td>';
	// 				html += '<td>'+info[i].line_name+'</td>';
	// 				html += '<td>'+info[i].action+'</td>';
	// 				html += '<td>'+info[i].status+'</td>';
	// 				html += '</tr>';
	// 				var element = document.getElementById('waysite_info');
	// 				element.insertAdjacentHTML('beforeend', html);
	// 			}
	// 		}
	// 		catch(e){
	// 			//console.log('failed');
	// 		}
	// 	}
	// };
};

function updateKeywordWaySiteWithSetTimeOut(lineCd, index)
{
	setTimeout(function(){ 
		console.log(index + " : " +lineCd);
		updateKeywordWaySite(lineCd);
	}, 300*index);
}

var listLineCd;

function updateKeywordListWaySite()
{
	var xhttp = create_obj();

	url = '/waysite/get-list-lineCd';

	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try
			{
				var listLineCd = JSON.parse(xhttp.responseText);
		

				for(var i in listLineCd)
				{
					
					updateKeywordWaySiteWithSetTimeOut(listLineCd[i], i);
			
				}

			}
			catch(e){}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

updateKeywordListWaySite();


