function create_obj(){
    var browse = navigator.appName;    
    if(browse == "Microsoft Internrt Explorer"){ 
        obj = new ActiveXObject("XMLHTTP");            
    }else{ 
        obj = new XMLHttpRequest();
    }            
    return obj; 
}

var token = document.getElementsByTagName('input').item(name="_token").value;
var provinceId; 
var dataSend; 
var url;
var data;

function updateWaySite(provinceId) {
	var xhttp = create_obj(); 

	url = 'http://www.ekidata.jp/api/p/'+provinceId+'.json';

	var s = document.getElementsByTagName("head")[0].appendChild(document.createElement("script"));
	s.type = "text/javascript";
	s.charset = "utf-8";
	s.src = url;
		
	s.addEventListener('load', function () {
		var data = JSON.stringify(xml.data.line);

		dataSend = "_token="+token + "&provinceId=" + provinceId + "&stationData=" + data;
		xhttp.open("POST", '/update/waysite', true);

		xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	 	xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xhttp.send(dataSend);
		s.remove();
	});

	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try
			{
				//show info on view waysite.update
				var info = JSON.parse(xhttp.responseText);
				var html = '';
				for(var i in info)
				{
					html += '<tr>';
					html += '<td>'+info[i].province_id+'</td>';
					html += '<td>'+info[i].line_cd+'</td>';
					html += '<td>'+info[i].line_name+'</td>';
					html += '<td>'+info[i].action+'</td>';
					html += '<td>'+info[i].status+'</td>';
					html += '</tr>';
					var element = document.getElementById('waysite_info');
					element.insertAdjacentHTML('beforeend', html);
				}
			}
			catch(e){
				//console.log('failed');
			}
		}
	};
};

var x = 1;
function intervalTrigger() {
	return window.setInterval( function() {
		if(x > 47)
		{
			var element = document.getElementById('waysite_info');
			element.insertAdjacentHTML('beforeend', 'Done');
			window.clearInterval(__id);
			return;
		}
		updateWaySite(x);
		x++;
	}, 2000 );
};
var __id = intervalTrigger();