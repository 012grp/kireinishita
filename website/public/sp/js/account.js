(function () {
	
	function ajax_get(url, callback)
	{
		var xhttp = new XMLHttpRequest();
		if(callback !== undefined)
		{
			xhttp.onreadystatechange = function(){
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					callback(xhttp.responseText);
				}
			}
		}
		xhttp.open("GET", url, true);
		xhttp.send();
	}
	
	/*
		Login
		=> Success close pop-up
	*/
	if(document.getElementById('btnLogin'))
	{
		document.getElementById('btnLogin').addEventListener('click', function () {
			var email =  document.getElementById('txtEmail').value;
			var password =  document.getElementById('txtPassword').value;

			var url = '/account/ajax-login?email='+email+'&password='+password; 
			ajax_get(url, function (result) {

				if(result == "1")
				{
					//success
					document.getElementById('btnCloseLoginModal').click();
					document.getElementById('naxBoxTop').innerHTML = '<div class="c-button-square--s" id="btnAuthen"><a href="/mybox">お気に入り BOX</a></div>';
				}
				else
				{
					//error
					document.getElementById('alertLoginMessage').style.display = "block";
				}		
			});
		});
	}
	/*
		Register
		=> Success close pop-up
	*/
	if(document.getElementById('btnRegister'))
	{
		document.getElementById('btnRegister').addEventListener('click', function () {
			var email =  document.getElementById('txtRegisterEmail').value;
			var password =  document.getElementById('txtRegisterPassword').value;

			var url = '/account/ajax-register?email='+email+'&password='+password; 
			ajax_get(url, function (result) {

				if(result == "1")
				{
					//success
					document.getElementById('btnCloseRegisterModal').click();
					document.getElementById('naxBoxTop').innerHTML = '<div class="c-button-square--s" id="btnAuthen"><a href="/mybox">お気に入り BOX</a></div>';
				}
				else
				{
					//error
					document.getElementById('alertRegisterMessage').innerHTML = result;
					document.getElementById('alertRegisterMessage').style.display = "block";
				}		
			});
		});
	}

	/*
		Forgot password
		=> Success close pop-up
	*/
	if(document.getElementById('btnForgetPassword'))
	{
		document.getElementById('btnForgetPassword').addEventListener('click', function () {
			var email =  document.getElementById('txtUserEmail').value;

			var url = '/account/ajax-forgot-password?email='+email; 
			ajax_get(url, function (result) {

				if(result == "1")
				{
					//success 
					//message => 'リセット確認メールが送信されました'
					document.querySelector('div[data-remodal-id="password"] i[data-remodal-action="close"]').click();
				}
				else
				{
					//error
					document.getElementById('alertForgetPasswordMessage').innerHTML = result;
					document.getElementById('alertForgetPasswordMessage').style.display = "block";
				}		
			});
		});
	}

	document.forms["formRegister"].onsubmit = function(evt){
	    evt.preventDefault();
	}

	/*
		Log-out
		=> Success close pop-up
		=> change to log-in button
	*/
	if(document.getElementById('btnLogout'))
	{
		document.getElementById('btnLogout').addEventListener('click', function () {

			//if log-in => log-out
			ajax_get('account/ajax-logout', function (result) {
				//document.getElementById('naxBoxTop').innerHTML = '<div data-modal-trigger="login" class="c-button-square--s" id="btnAuthen"><span>ログイン</span></div>';
				window.location = "/";
			});
		});
	}
	
})()