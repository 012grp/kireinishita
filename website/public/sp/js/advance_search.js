var __select_template = function (isSelected, value, name) {
	var opt = '<option '+(isSelected?'selected':'')+' value="'+value+'">'+name+'</option>';
	return opt;
}

//ajax json function
var __getJson = function (url, callback) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try {
				var data = JSON.parse(xhttp.responseText);
				callback(data);
			} catch (e) {
				console.log(e);
			}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

//Change waysidemodal checkboxes of prefmodal (沿線・駅)
//Get waysite when province changed
//And add checkboxes to modal
var __addWaysites = function (_value) {
	//sStation: http://www.ekidata.jp/api/l/" + _value + ".json
	var sWaysite = document.getElementsByTagName("head")[0].appendChild(document.createElement("script"));
	sWaysite.type = "text/javascript";
	sWaysite.charset = "utf-8";
	sWaysite.src = "http://www.ekidata.jp/api/p/" + _value + ".json";
	
	sWaysite.addEventListener('load', function () {
		var data = xml.data.line;
		var obj = document.getElementById('waysite_select');
		var html = __select_template(false, '', '沿線・駅を選択');
		for (var i in data) {
			//var isChecked = false;
			html += __select_template(false, data[i].line_cd, data[i].line_name);
		}
		obj.innerHTML = html;
	});
}

//Get citys when province changed
//And add checkboxes to modal
var __addCities = function (_value, _text) {
	var url = '/area/city-by-province?province_id=' + _value;
	__getJson(url, function (jsonData) {
		var obj = document.getElementById('city_select');
		var html = __select_template(false, '', '市区町村');
		for (var i in jsonData) {
			//var isChecked = false;
			html += __select_template(false, jsonData[i].id, jsonData[i].name);
		}
		obj.innerHTML = html;
	});
}

//event change of province select tag
//Change checkboxes of city and waysite modal
document.getElementById('pro_select').addEventListener('change', function () {
	__addCities(this.value);
	__addWaysites(this.value);
});