(function () {
/*Ajax function*/
var __getAjax = function (url, callback, callback_error) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			try {
				callback(xhttp.responseText);
			} catch (e) {
				console.log(e);
			}
		}
		else
		{
			if(callback_error !== undefined)
			{
				callback_error(xhttp.status);
			}
		}
	};
	xhttp.open("GET", url, true);
	xhttp.send();
}

/*Get values to make URL*/
var __makeUrl = function (obj) {
	
	//checkboxs
	var uri = [];
	var textboxKeyword = document.getElementById('search_keyword');
	var selectProvince = document.getElementById('pro_select');
	var checkboxesList = document.getElementsByClassName('search-checkbox');
	
	if(textboxKeyword.value != '')
	{
		uri.push('k='+encodeURIComponent(textboxKeyword.value));
	}
	
	if(selectProvince.value != '')
	{
		uri.push('pro='+selectProvince.value);
	}
	
	var checkedLeng = checkboxesList.length;
	for(var i=0; i<checkedLeng; i++)
	{
		if(checkboxesList[i].checked)
		{
			if(obj.id == 'pro_select' && (checkboxesList[i].name=='city[]' || checkboxesList[i].name=='way[]'))
			{
				continue;
			}
			uri.push(encodeURIComponent(checkboxesList[i].name)+'='+checkboxesList[i].value);
		}
	}
	
	var url = '/job/ajax-count-search?'+uri.join('&');
	
	return url;
}

/*Count function*/
var __count = function (obj) {

	var url = __makeUrl(obj);
	
	__getAjax(url, 
	function (numberResult) {
		document.getElementById('btnAdvanceSubmit').innerText = numberResult+'件を検索';
	}, 
	function() {
		document.getElementById('btnAdvanceSubmit').innerText = '...件を検索';
	});
}

/*Events*/
var eventFunc = function (obj, action) {
	obj.addEventListener(action, function(){
		__count(obj);
	});
};

//When done press keyword
document.getElementById('search_keyword').addEventListener("blur", function () {
	__count(this);
});	


//When select city checkbox or waysite checkbox
window.addEventListener("change", function(e){
	var target = e.target;
	if(target.className == "search-checkbox city-checkbox" || 
	   target.className == "search-checkbox waysite-checkbox")
	{
		__count(target);
	}
});


	
//When select 都道府県
eventFunc(document.getElementById('pro_select'), 'change');
	
//When checkbox 職種, 雇用形態, こだわり
var checkboxes = document.getElementsByClassName('search-checkbox');
var cLeng = checkboxes.length;
for(var c=0; c<cLeng; c++)
{
	if(checkboxes[c].name == 'em[]' ||
	   checkboxes[c].name == 'cat[]' ||
	   checkboxes[c].name == 'hang[]' )
	{
		eventFunc(checkboxes[c], 'change');
	}
}
	
//When close City or Waysite modal
var closeModalButtons = document.getElementsByClassName('close_modal_button');
var bLeng = closeModalButtons.length;
for(var b=0; b<bLeng; b++)
{
	eventFunc(closeModalButtons[b], 'click');
}
	
})()