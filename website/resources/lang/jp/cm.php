<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common lanuage for Jappanese
    |--------------------------------------------------------------------------
    | Setting to use:
    | 1. Go to 'config/app.php' path then find and set App::setLocale('jp');
    | 2. Use:
    |   echo Lang::get('messages.welcome');
    |   echo trans('messages.welcome');
    |
    | Advance:
    |   //if $count = 0 -> There are none, 1 -> 19 There are some, > 20 There are many
    |   'apples' => '{0} There are none|[1,19] There are some|[20,Inf] There are many',
    |   echo Lang::choice('messages.apples', $count);
    */
    
    'maximum_900' => 'Allow jobnote_yyyymmdd.csv with maximum 900 lines',
    'maximum_1000' => 'Allow jobnote_yyyymmdd.csv with maximum 1000 lines',
    'not_found' => ':name not found',
];
