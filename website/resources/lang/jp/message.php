<?php

return [
    'contact_sendmail_exception_error' => 'Something went wrong please try again',
    'update_success' 				   => '更新完了しました',
    'update_success_and_send_mail'     => '更新およびメールが送信完了しました',
    'create_success' 				   => '新規登録完了しました',
    'create_success_and_send_mail' 	   => '新規登録およびメールが送信完了しました',
    'delete_success' 				   => '削除完了しました',
    'error_compnany_email_exits'	   => 'このメールはすでに登録されておりますので、別のメールアドレスをお使いください',
    'error_company_password'		   => '8文字以上の半角英数字を入力してください',
    'error_company_not_url'			   => 'URLアドレスを入力してください',
];
