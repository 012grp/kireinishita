<?php

//static page 
return [
	//1.0
    ["1.0","/"],

    //0.8
	["0.8","/contents"],
	["0.8","/limited_kyujin"],
	["0.8","/concept"],
	["0.8","/mybox"],
	["0.8","/contact"],
	["0.8","/about/company"],
	["0.8","/about/privacypolicy"],
	//search
	["0.8", "/biyo"],
	["0.8", "/esthe"],
	["0.8", "/nail"],
	["0.8", "/eyelist"],
	["0.8", "/biyoshi"],
	["0.8", "/biyobuin"],
	["0.8", "/hair-makeup"],
	["0.8", "/bridal"],
	["0.8", "/apparel"],
	["0.8", "/reception"],
	["0.8", "/relaxation"],
	["0.8", "/therapist"],
	["0.8", "/aroma"],
	["0.8", "/massageshi"],
	["0.8", "/reflexology"],
	["0.8", "/seitai"],
	["0.8", "/chiropractic"],
	["0.8", "/yoga"],
	["0.8", "/fitness"],
	["0.8", "/chiryo"],
	["0.8", "/jusei"],
	["0.8", "/shinkyu"],
	["0.8", "/massage"],
	["0.8", "/biyo-nurse"],
	["0.8", "/feature/qualifications"],
	["0.8", "/feature/treatment"],
	["0.8", "/feature/characteristic"],
	["0.8", "/feature/regular-member"],
	["0.8", "/feature/arbeit"],
	["0.8", "/feature/part-time"],
	["0.8", "/feature/contract-employee"],
	["0.8", "/feature/dispatch"],
	["0.8", "/feature/other"],
	["0.8", "/feature/inexperienced"],
	["0.8", "/feature/all-ages"],
	["0.8", "/feature/student"],
	["0.8", "/feature/tsushin"],
	["0.8", "/feature/experienced"],
	["0.8", "/feature/new-graduates"],
	["0.8", "/feature/second-new-graduates"],
	["0.8", "/feature/social-insurance"],
	["0.8", "/feature/dormitory"],
	["0.8", "/feature/allowance-enhancement"],
	["0.8", "/feature/transportation-provision"],
	["0.8", "/feature/training-system"],
	["0.8", "/feature/uniform"],
	["0.8", "/feature/two-day-holidays"],
	["0.8", "/feature/employee-recruitment"],
	["0.8", "/feature/double-work"],
	["0.8", "/feature/weocome-independent-hope"],
	["0.8", "/feature/norm-none"],
	["0.8", "/feature/opening"],
	["0.8", "/feature/high-income"],
	["0.8", "/feature/regular-holiday"],
	["0.8", "/feature/free-clothes"],
	["0.8", "/feature/ekichika"],
	["0.8", "/feature/complete-percentage"],
	["0.8", "/feature/recruiting-hurriedly"],
	["0.8", "/feature/self-employed"],
	["0.8", "/feature/homemake"],
	
	//0.7
	["0.7","/account"],

	//job/result/勤務地（ユニーク）
	["0.7","/job/result/北海道"],
	["0.7","/job/result/青森県"],
	["0.7","/job/result/岩手県"],
	["0.7","/job/result/宮城県"],
	["0.7","/job/result/秋田県"],
	["0.7","/job/result/山形県"],
	["0.7","/job/result/福島県"],
	["0.7","/job/result/茨城県"],
	["0.7","/job/result/栃木県"],
	["0.7","/job/result/群馬県"],
	["0.7","/job/result/埼玉県"],
	["0.7","/job/result/千葉県"],
	["0.7","/job/result/東京都"],
	["0.7","/job/result/神奈川県"],
	["0.7","/job/result/新潟県"],
	["0.7","/job/result/富山県"],
	["0.7","/job/result/石川県"],
	["0.7","/job/result/福井県"],
	["0.7","/job/result/山梨県"],
	["0.7","/job/result/長野県"],
	["0.7","/job/result/岐阜県"],
	["0.7","/job/result/静岡県"],
	["0.7","/job/result/愛知県"],
	["0.7","/job/result/三重県"],
	["0.7","/job/result/滋賀県"],
	["0.7","/job/result/京都府"],
	["0.7","/job/result/大阪府"],
	["0.7","/job/result/兵庫県"],
	["0.7","/job/result/奈良県"],
	["0.7","/job/result/和歌山県"],
	["0.7","/job/result/鳥取県"],
	["0.7","/job/result/島根県"],
	["0.7","/job/result/岡山県"],
	["0.7","/job/result/広島県"],
	["0.7","/job/result/山口県"],
	["0.7","/job/result/徳島県"],
	["0.7","/job/result/香川県"],
	["0.7","/job/result/愛媛県"],
	["0.7","/job/result/高知県"],
	["0.7","/job/result/福岡県"],
	["0.7","/job/result/佐賀県"],
	["0.7","/job/result/長崎県"],
	["0.7","/job/result/熊本県"],
	["0.7","/job/result/大分県"],
	["0.7","/job/result/宮崎県"],
	["0.7","/job/result/鹿児島県"],
	["0.7","/job/result/沖縄県"],

	//0.6
	["0.6","/account/facebook-redirect"],
	["0.6","/account/twitter-redirect"],
	["0.6","/account/forgot-password"],
];