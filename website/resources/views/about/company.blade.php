<?php 
	$class_primary_box = 'u-mt20';
	$__title = _helper::getWebInfo('title_about_company');
	$web_title = _helper::set_title($__title);
	$description = trans('webinfo.meta_desc_about_company');
	$keywords = trans('webinfo.keywords_about_company');
?> 

@extends('layouts.default')

@section('column_right')
	@include('shared.right_column.no_content')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> '運営会社'
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<h1 class="column-left__ttl--normal u-fs--xxl--contents">運営会社</h1>
		<div class="table--primary u-mt10">
			<table>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">
						商
						&nbsp;
						号
					</td>
					<td class="table--primary__detail">
						<div><span class="u-fs--l">株式会社Wiz（株式会社ワイズ）</span></div>
					</td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">所在地</td>
					<td class="table--primary__detail">
						<div><span class="u-fs--l">東京都豊島区南大塚2-25-15 South新大塚ビル12F</span></div>
					</td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">
						設
						&nbsp;
						立
					</td>
					<td class="table--primary__detail"><span class="u-fs--l">2012年4月18日</span></td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">事業内容</td>
					<td class="table--primary__detail">
						<div><span class="u-fs--lh--contents">IT通信事業、広告事業、ヘルスケア事業、オフィスコンサル事業、メディア運営事業、HR事業、社内システム構築事業、VNO事業</span></div>
					</td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">代表取締役</td>
					<td class="table--primary__detail">
						<div><span class="u-fs--l">山﨑 俊</span></div>
					</td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">資本金</td>
					<td class="table--primary__detail">
						<div><span class="u-fs--l">6,000万円（資本準備金含む）</span></div>
					</td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">決算期</td>
					<td class="table--primary__detail">
						<div><span class="u-fs--l">11月30日</span></div>
					</td>
				</tr>
				<tr class="table--primary__row">
					<td class="table--primary__ttl u-fs--l u-fwb">HP</td>
					<td class="table--primary__detail">
						<div><a target="_blank" href="http://012grp.co.jp/"><span class="u-fs--l">http://012grp.co.jp/</span></a></div>
					</td>
				</tr>
			</table>
		</div>
	</div>
@stop