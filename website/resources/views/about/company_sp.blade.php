<?php
$__title = _helper::getWebInfo('title_about_company');
$web_title = _helper::set_title($__title);

$description = trans('webinfo.meta_desc_about_company');
$keywords = trans('webinfo.keywords_about_company');
?>

@extends('layouts.default_sp')

@section('under_header')

@stop


@section('page_sp')
    <div class="primary-box--noPadding">
        <!--.contents-box--recruit__top-->
        <div class="primary-box__topBlock">
            <h1 class="list__ttl--normal u-fs--m u-fwb">運営会社</h1>
        </div>
        <div class="list--primary u-mt15">
            <ul>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">
                        商
                        &nbsp;
                        号
                    </h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">株式会社Wiz</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">所在地</h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">東京都豊島区南大塚2-25-15 South新大塚ビル12F</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">
                        設
                        &nbsp;
                        立
                    </h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">2012年4月18日</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">事業内容</h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">IT通信事業、広告事業、ヘルスケア事業、オフィスコンサル事業、メディア運営事業、HR事業、社内システム構築事業、VNO事業</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">代表取締役</h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">
                  山﨑
                  &nbsp;
                  俊</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">資本金</h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">6,000万円 (資本準備金含む)</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">決算期</h3>
                    <div class="list--primary__text u-mt15"><span class="u-fs--sh">11月30日</span></div>
                </li>
                <li class="list--primary__row u-mt15">
                    <h3 class="u-fs--s u-fwb">HP</h3>
                    <div class="list--primary__text u-mt15"><a target="_blank" href="http://012grp.co.jp/"><span class="u-fs--sh">http://012grp.co.jp/</span></a></div>
                </li>
            </ul>
        </div>
    </div>
@stop