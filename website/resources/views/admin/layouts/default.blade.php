@extends('admin.master')
@section('page_content')
<!-- Header -->
@include('admin.shared.header.default')
<!-- /Header -->

<!-- Main -->
<div class="container admin_page">
    <div class="row">
        <div class="col-sm-2">
            <!-- left -->
            @include('admin.shared.item.menu')
        </div>
        <!-- /span-3 -->
        <div class="col-sm-10">
            <!-- column 2 -->
            <h3><i class="glyphicon glyphicon-dashboard"></i> {{ $title_page or "Dashboard" }}</h3>
            @if (Session::has('flash_message'))
                <div class="alert alert-info"> {{ Session::get('flash_message') }} </div>
            @endif
            <hr> @yield('content')
        </div>

    </div>
    <!--/row-->
</div>
<!--/container-->
<!-- /Main -->


<footer class="text-center">キレイにスルンダ|管理</footer>
@stop