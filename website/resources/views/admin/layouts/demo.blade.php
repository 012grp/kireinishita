@extends('admin.master')



@section('page_content')

<!-- Main -->
<div class="container-fluid">
	<h3><i class="glyphicon glyphicon-dashboard"></i> I EAT</h3>
	<hr> @yield('content')
</div>
<!--/container-->
<!-- /Main -->


<footer class="text-center">-- I EAT --</footer>


<div class="modal" id="addWidgetModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Add Widget</h4>
      </div>
      <div class="modal-body">
        <p>Add a widget stuff here..</p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dalog -->
</div><!-- /.modal -->
@stop