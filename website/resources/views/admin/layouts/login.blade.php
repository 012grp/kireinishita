@extends('admin.master')


@section('page_head')
<script type="text/javascript" src="/public/admin/js/common.js"></script>
@stop

@section('page_content')
    @yield('content')
@stop
