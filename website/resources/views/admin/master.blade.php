<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="/public/admin/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="/public/admin/apple-touch-icon-precomposed.png" />
    <title>{{ $title or '管理ページ'}} | キレイにスルンダ</title>

    <link rel="stylesheet" href="/public/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/plugins/bootstrap/dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/public/admin/css/style.css">
    <link rel="stylesheet" href="/public/admin/css/font-awesome-4.6.3/css/font-awesome.min.css">

    <script type="text/javascript" src="/public/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/public/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/public/admin/js/lib.js"></script>
    @yield('page_head')
    @yield('page_top')
</head>
<body>
@yield('page_content')
@yield('page_bottom')
</body>
</html>