@extends('admin.layouts.default')
@section('page_head')
    @include('admin.shared.item.froala_editor_css')
    @include('admin.shared.item.froala_editor_js')
    {!!HTML::style('public/admin/css/secret.css')!!}
    {!!HTML::style('public/admin/js/chosen_v1.6.1/chosen.css')!!}
    {!!HTML::script('public/admin/js/chosen_v1.6.1/chosen.jquery.min.js')!!}
    {!!HTML::style('public/admin/js/fileinput/css/fileinput.css')!!}
    {!!HTML::style('public/admin/js/bootstrap-datepicker/css/bootstrap-datepicker.css')!!}
    {!!HTML::script('public/admin/js/fileinput/fileinput.js')!!}
    {!!HTML::script('public/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
    {!!HTML::script('public/admin/js/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js')!!}
    {!!HTML::script('public/admin/js/fileinput/locales/ja.js')!!}
    {!!HTML::script('public/js/init-googlemap.js')!!}
    {!!HTML::script('https://maps.google.com/maps/api/js?language=ja&region=JP&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs')!!}
@append
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- if there are creation errors, they will show here -->
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['url' => $link_action,'method'=>'POST', 'files'=>true, 'id' => 'secret_job_form']) !!}

            <div class="form-group">
                {!! Form::label('company_id', '会社名') !!}
                {!! Form::select('company_id', $list_company,(!empty($secret['company_id'])) ? $secret['company_id'] : Input::old('company_id'), array('class' => 'form-control chosen-select')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_name', 'ジョブ名')  !!}
                {!! Form::text('job_name', (!empty($secret['job_name'])) ? $secret['job_name'] : Input::old('job_name'), array('class' => 'form-control' , 'required')) !!}
            </div>


            <div class="panel panel-default"> 
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                                {!! Form::label('meta_desc', 'ディスクリプション')  !!}
                                {!! Form::textarea('meta_desc', (!empty($article['meta_desc'])) ? $article['meta_desc'] : Input::old('meta_desc'), array('class' => 'form-control', 'maxlength' => '160', 'rows' => '4')) !!}
                            </div>

                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('meta_keyword', 'キーワード')  !!}
                                {!! Form::textarea('meta_keyword', (!empty($article['meta_keyword'])) ? $article['meta_keyword'] : Input::old('meta_keyword'), array('class' => 'form-control', 'maxlength' => '255', 'rows' => '4')) !!}
                            </div>
                        </div>
                    </div>
                </div> 
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group" id="image">
                        <?php
                            $required = (empty($secret['job_image'])) ?  array('required') : "";
                        ?>
                        {!! Form::label('file','PCの画像',array('id'=>'','class'=>'')) !!}
                        {!! Form::file('file',$required) !!}
                        <input type="hidden" value="" id="delete_image" name="delete_image">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" id="thumb">
                        {!! Form::label('image_sp','SPの画像',array('id'=>'','class'=>'')) !!}
                        {!! Form::file('image_sp') !!}
                        <input type="hidden" value="" id="delete_thumb" name="delete_thumb">
                    </div>
                </div>
            </div>

            
            <div class="form-group">
                {!! Form::label('list_em','雇用形態',array('id'=>'','class'=>'')) !!}
                <div class="list_check">
                    <?php $index = 0;
                    $employment_list = _helper::getEmployment();
                     $list_em = (isset($secret['list_em'])) ? $secret['list_em'] : "";
                    $trim_secret = substr($list_em, 1, -1);
                    $requestEm = explode(",",$trim_secret);
                    ?>
                    @foreach($employment_list as $record)
                        <?php
                        $key = $record['id'];
                        $em = $record['name'];
                        $index++;
                        $emChecked = (in_array($key,$requestEm)) ? 'checked' : '';
                        ?>
                        <input {{$emChecked}} type="checkbox" name="em[]" value="{{$key}}" id="check{{$index}}" class="search-checkbox">
                        <label for="check{{$index}}" class="search-checkbox__label">{{$em}}</label>
                    @endforeach
                </div>
            </div>
            
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					{!! Form::label('list_catagory', '職種')  !!}
					<?php
						$job_category = _helper::getJobCategory();
					   $current_list_job = (isset($secret['list_job'])) ? $secret['list_job'] : "";
					?>
					@include('admin.pages.secretjob.list_job_checkbox', array('modal_title' => '職種',
																	'modal_data' => $job_category,
																	'modal_prefix' => 'job',
																	'input_name' => 'cat[]',
																	'checked_items' => $current_list_job
																	))

					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('list_hangup','こだわり',array('id'=>'','class'=>'')) !!}
						<?php
						$hangup_keywords = _helper::getHangUp();
						$current_list_hangup = (isset($secret['list_hangup'])) ? $secret['list_hangup'] : "";
						?>
						@include('admin.pages.secretjob.list_job_checkbox', array('modal_title' => 'こだわり',
																					'modal_data' => $hangup_keywords,
																					'modal_prefix' => 'hangup',
																					'input_name' => 'hang[]',
																					'checked_items' =>                                                                                              $current_list_hangup
																					))
					</div>
				</div>
			</div>
      
            
			<hr>


            <div class="from-group">
                {!! Form::label('images_sp','PCの一覧画像',array('id'=>'','class'=>'')) !!}
                (画像<2メガバイト)
                <div class="list_images">
                    <?php
                    if(!empty($list_images)){
                        foreach ($list_images as $src){
                            if($src['type']==0){
                                echo "<div class='abcd' id='image_".$src['id']."'><img width='100' src='".$src['image']."' class='' />
                        <img id='img' data-id='".$src['id']."' class='delete_image' src='/public/admin/img/x.png' alt='delete' />
                    </div>";
                            }

                        }
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>

                <div class="group_image">
                    <div id="filediv"><input name="images[]" type="file" id="images"/></div>
                    <input type="button" id="add_more" class="btn btn-success" value="その他の画像を追加"/>

                    <div class="clearfix"></div>
                </div>
            </div>

            <hr>
            <div class="from-group">
                {!! Form::label('images_sp','SPの一覧画像',array('id'=>'','class'=>'')) !!}
                (画像<2メガバイト)
                <div class="list_images">
                    <?php
                    if(!empty($list_images)){
                        foreach ($list_images as $src){
                            if($src['type']==1){
                                echo "<div class='abcd_sp' id='image_".$src['id']."'><img width='100' src='".$src['image']."' class='' />
                        <img id='img' data-id='".$src['id']."' class='delete_image' src='/public/admin/img/x.png' alt='delete' />
                    </div>";
                            }

                        }
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>

                <div class="group_image">
                    <div id="filediv"><input name="images_sp[]" type="file" id="images"/></div>
                    <input type="button" id="add_more_sp" class="btn btn-success" value="その他の画像を追加"/>

                    <div class="clearfix"></div>
                </div>
            </div>


            <hr>
            <div class="column-box__secret">
            <div>マニュアル: <a target="_blank" href="/public/admin/img/manual/article-manual.jpg">私をクリック</a></div>
            <div class="form-group">
                {!! Form::label('content', 'コンテンツ') !!}
                {!! Form::textarea('content', (!empty($secret['content'])) ? $secret['content'] : Input::old('content'), array('class' => 'form-control froalaEditor')) !!}
            </div>
            </div>
			<hr>
          	
          	<div class="panel panel-default"> 
          		<div class="panel-heading"> 
          			<span class="c-tag--secret--notice u-fs--xs">注目ポイント！</span>
          		</div> 
          		<div class="panel-body">
         			    <div class="row">
         			    	<div class="col-md-4">
         			    		<div class="form-group" id="image_notice">
									{!! Form::label('job_notice_image','PCの画像') !!}
									{!! Form::file('job_notice_image') !!}
                                    <input type="hidden" value="" id="delete_image_notice" name="delete_image_notice">
								</div>


                                <div class="form-group" id="thumb_notice">
                                    {!! Form::label('job_notice_image_sp','SPの画像',array('id'=>'','class'=>'')) !!}
                                    {!! Form::file('job_notice_image_sp') !!}
                                    <input type="hidden" value="" id="delete_thumb_notice" name="delete_thumb_notice">
                                </div>
         			    	</div>
         			    	<div class="col-md-8">
         			    		{!! Form::label('job_notice_image','注目ポイントのコンテンツ') !!}
         			    		<div class="form-group">
									{!! Form::text('job_notice_header', (!empty($secret['job_notice_header'])) ? $secret['job_notice_header'] : Input::old('job_notice_header'), array('class' => 'form-control')) !!}
								</div>

								<div class="form-group">
									{!! Form::textarea('job_notice_content', (!empty($secret['job_notice_content'])) ? $secret['job_notice_content'] : Input::old('job_notice_content'), array('class' => 'form-control',
									'rows' => 7
									)) !!}
								</div>
         			    	</div>
         			    </div>
          		</div> 
          	</div>
			<hr>
            <div class="form-group">
                {!! Form::label('job_salary', '給与') !!}
                {!! Form::textarea('job_salary', (!empty($secret['job_salary'])) ? $secret['job_salary'] : Input::old('job_salary'), array('class' => 'form-control', 'required',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_qualification', '応募資格') !!}
                {!! Form::textarea('job_qualification', (!empty($secret['job_qualification'])) ? $secret['job_qualification'] : Input::old('job_qualification'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>
			

            <div class="form-group">
                {!! Form::label('job_description', '仕事内容') !!}
                {!! Form::textarea('job_description', (!empty($secret['job_description'])) ? $secret['job_description'] : Input::old('job_description'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('treatment_welfare', '待遇・福利厚生') !!}
                {!! Form::textarea('treatment_welfare', (!empty($secret['treatment_welfare'])) ? $secret['treatment_welfare'] : Input::old('treatment_welfare'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_holiday', '休日休暇') !!}
                {!! Form::textarea('job_holiday', (!empty($secret['job_holiday'])) ? $secret['job_holiday'] : Input::old('job_holiday'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>


            <div class="form-group">
                {!! Form::label('access', 'アクセス') !!}
                {!! Form::textarea('access', (!empty($secret['access'])) ? $secret['access'] : Input::old('access'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_location', '勤務地') !!}
                {!! Form::text('job_location', (!empty($secret['job_location'])) ? $secret['job_location'] : Input::old('job_location'), array('class' => 'form-control' )) !!}
            </div>

            <div class="form-group">
                <label for="map_location">地図位置 <sub>Google map address</sub></label>
                <div class="row form-group">
                    <div class="col-sm-9">
                        {!! Form::text('map_location',(!empty($secret['map_location'])) ? $secret['map_location'] : Input::old('map_location'), array('class' => 'form-control input-map-location ' , 'id' => 'map_location')) !!}
                    </div>
                    <div class="col-sm-3">
                        <input id="find_location" type="button" style="" class="btn btn-primary btn-map-location" value="見つけます"/>
                        <input id="remove_location" type="button" style="" class="btn btn-primary btn-map-location" value="場所を削除"/>
                    </div>
                </div>


                <div id="map" style="height: 500px;"></div>
                {!! Form::hidden('lat', (!empty($secret['lat'])) ? $secret['lat'] : Input::old('lat'),array('id'=>'lat')) !!}
                {!! Form::hidden('lng', (!empty($secret['lng'])) ? $secret['lng'] : Input::old('lng'),
                array('id'=>'lng')) !!}
                {!! Form::hidden('id',(!empty($secret['id'])) ? $secret['id'] : Input::old('id') ) !!}
                {!! Form::hidden('preview_kind', 'PC', array('id'=>'preview_kind'))!!}
                {!! Form::hidden('date_create', (!empty($secret['created_at'])) ? $secret['created_at'] : "" ,array('id'=>'date_create')) !!}
                {!! Form::hidden('list_job_parent', (!empty($secret['list_job_parent'])) ? $secret['list_job_parent'] : "" ,array('id'=>'list_job_parent')) !!}
            </div>


            <div class="row">
                <div class="form-group col-sm-2">
                    {!! Form::label('status', '状態') !!}
                    {!! Form::select('status', array(0=> '未発表', 1 => '公開'),(!empty($secret['status'])) ? $secret['status'] : 0, array('class' => 'form-control')) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::label('start_at', '掲載期間')  !!}
                    <?php
                    $date_start = !empty($secret['start_at']) ? $secret['start_at'] : Input::old('start_at');
                    $date_start = date_create($date_start);
                    if($date_start)
                    {
                        $date_start = date_format($date_start,"Y/m/d");
                    }

                    ?>
                    {!! Form::text('start_at', $date_start , array('class' => 'form-control datepicker')) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::label('end_at', '&nbsp;')  !!}
                    <?php
                    $date_end = !empty($secret['end_at']) ? $secret['end_at'] : Input::old('end_at');
                    $date_end = date_create($date_end);
                    if($date_end)
                    {
                        $date_end = date_format($date_end,"Y/m/d");
                    }

                    ?>
                    {!! Form::text('end_at', $date_end , array('class' => 'form-control datepicker')) !!}
                </div>
            </div>

            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::label('created_at', '作成日')  !!}
                        <?php 
                            $date_created = !empty($secret['created_at']) ? $secret['created_at'] : Input::old('created_at');
                            $date_created = date_create($date_created);
                            if($date_created)
                            {
                                $date_created = date_format($date_created,"Y/m/d");
                            }
                            
                        ?>
                        {!! Form::text('created_at', $date_created , array('class' => 'form-control datepicker')) !!}
                    </div>
                    <div class="col-md-10">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::submit('申し出る', ['class' => 'btn btn-primary', 'id' => 'btnSubmit']) !!}
                    </div>
                    <div class="col-md-10">
                        {!! Form::submit('PC-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewPC']) !!}
                        {!! Form::submit('SP-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewSP']) !!}
                    </div>
                </div>
            </div>

            {!! Form::close() !!}


        </div>
    </div>

    <script>
        //datepicker date created
        $('.datepicker').datepicker({
            format: 'yyyy/mm/dd',
            language: 'ja',
            //endDate: '+0d',
        });

        $("#btnPreviewPC").click(function(){
            //set list job parent in input hidden
            setListJobParent();
            $("#secret_job_form").removeAttr("onsubmit");
            $("#secret_job_form").attr('target', '_blank');
            $("#secret_job_form").attr('action', '/admin/secret-job-manager/preview');
            $("#preview_kind").val("PC");
        });

        $("#btnPreviewSP").click(function(){
            //set list job parent in input hidden
            setListJobParent();
            $("#secret_job_form").attr('onsubmit', 'target_popup(this)');
            $("#secret_job_form").attr('action', '/admin/secret-job-manager/preview');
            $("#preview_kind").val("SP");
        });

        function target_popup(form) {
            window.open('', 'formpopup', 'width=414,height=650,resizeable,scrollbars');
            form.target = 'formpopup';
        }

        $("#btnSubmit").click(function(){
            //set list job parent in input hidden
            setListJobParent();
            $("#secret_job_form").removeAttr("onsubmit");
            $("#secret_job_form").attr('target', '');
            $("#secret_job_form").attr('action', '/'+'<?php echo $link_action; ?>');
        });


        $("#image_sp").fileinput({
            initialPreview: [<?php if(!empty($secret['job_image_sp'])){ ?> '<img style="max-height:150px" src="<?php echo $secret['job_image_sp'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "auto", height: "150px"},
            },
            showUpload: false,
            showRemove: true,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            language: 'ja',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });

          $("#file").fileinput({
            initialPreview: [<?php if(!empty($secret['job_image'])){ ?> '<img width="150" src="<?php echo $secret['job_image'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "150px", height: "auto"},
            },
            showUpload: false,
            showRemove: false,
            language: 'ja',
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });


        $("#job_notice_image_sp").fileinput({
            initialPreview: [<?php if(!empty($secret['job_notice_image_sp'])){ ?> '<img style="max-height:150px" src="<?php echo $secret['job_notice_image_sp'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "auto", height: "150px"},
            },
            showUpload: false,
            showRemove: true,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            language: 'ja',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });



        $("#job_notice_image").fileinput({
            initialPreview: [<?php if(!empty($secret['job_notice_image'])){ ?> '<img width="150"  src="<?php echo $secret['job_notice_image'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "200px", height: "auto"},
            },
            showUpload: false,
            showRemove: false,
            language: 'ja',
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });


        $(document).ready(function () {
            $(".chosen-select").chosen({allow_single_deselect:true,no_results_text: "Oops, nothing found!"});


            $('.froalaEditor').froalaEditor({
                language: 'ja',
                height: 400,
				// Define new paragraph styles.
				paragraphStyles: {
					"_secret__text__block u-fs--lh--contents": "Secret block",
					"_secret__text__ttl u-fwb u-mt25": 'Secret H3',
					"_secret__text": 'Secret Text P',
					"_contents__img__block u-mt25": 'Image wrapper',
					"_contents__img_text u-mt10": 'Image text',
				},
				paragraphFormat: {
					N: 'Normal',
					H2: 'Heading 2',
					H3: 'Heading 3',
				},
				//enter: $.FroalaEditor.ENTER_BR
            });

            // set google map
            // input lat, lng , inforwindow, zoom
            <?php
            $lat = (!empty($secret['lat'])) ? $secret['lat'] : 0;
            $lng = (!empty($secret['lng'])) ? $secret['lng'] : 0;
            ?>
            <?php if($lat && $lng): ?>
                var contentString = "";
                var companyName = $("#company_id option:selected").text();
                contentString += "<h4 style='font-weight: bold; color: black;'>"+companyName+"</h4>";
                <?php if(!empty($secret['job_location'])): ?>
                    contentString += "<p><?php echo $secret['job_location']; ?></p>";
                <?php endif; ?>
                initMap(<?php echo $lat;?> ,<?php echo $lng;?>, contentString, 14, 'map');
            <?php else: ?>
                initMap(<?php echo $lat;?> ,<?php echo $lng;?>, "", 14, 'map');
            <?php endif; ?>

            $("#find_location").click(function () {
                var address = $("#map_location").val();
                var p = null;
                $.ajaxSetup({
                    async: false
                });
                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false', null, function (data) {
                    var p = data.results[0].geometry.location;
                    $('#lat').attr('value', p.lat);
                    $('#lng').attr('value', p.lng);
                    var contentString = "";
                    var __companyName = $("#company_id option:selected").text();
                    contentString += "<h4 style='font-weight: bold; color: black;'>"+__companyName+"</h4>";
                    var job_location = $('#job_location').val();
                    if(job_location != "")
                    {
                        contentString +="<p>"+job_location+"</p>";
                    }
                    initMap(p.lat, p.lng, contentString, 14, 'map');
                });
            });

            $("#remove_location").click(function () {
                var address = "";
                var p = null;
                $.ajaxSetup({
                    async: false
                });

                $("#map_location").val("");
                $('#lat').attr('value', 0);
                $('#lng').attr('value', 0);
                initMap(0, 0, address, 14, 'map');
            });

        });



        var abc = 0; //Declaring and defining global increement variable
        $(document).ready(function() {

            //delete image thumb
            $('#thumb .fileinput-remove').click(function() {
                $('#delete_thumb').val("image_thumb_delete");
            });

            $('#image .fileinput-remove').click(function() {
                $('#delete_image').val("image_delete");
            });

            //delete image thumb
            $('#image_notice .fileinput-remove').click(function() {
                $('#delete_image_notice').val("image_delete");
            });

            $('#thumb_notice .fileinput-remove').click(function() {
                $('#delete_thumb_notice').val("image_thumb_delete");
            });


//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
            $('#add_more').click(function() {
                $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                        $("<input/>", {name: 'images[]', type: 'file', id: 'images'}),
                        $("<br/>")
                ));
            });

            $('.delete_image').click(function() {
                if (confirm('あなたはこのイメージを削除してもよろしいですか？')) {
                    var  id_image = $(this).attr('data-id');
                    var  url = '{!!  route('admin.delete_image') !!}';
                    var  data = {id: id_image};
                    $.ajax({
                        url: url,
                        data: data,
                        type: 'POST',
                        success: function (resp) {
                            $("#image_"+id_image).hide(500);
                        }
                    });
                }
            });

            //following function will executes on change event of file input to select different file
            $('body').on('change', '#images', function(){
                if (this.files && this.files[0]) {
                    abc += 1; //increementing global variable by 1

                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");

                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);

                    $(this).hide();
                    $("#abcd"+ abc).append($("<img/>", {id: 'img', src: "{{ asset('public/admin/img/x.png') }}", alt: 'delete'}).click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });
//To preview image
            function imageIsLoaded(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };



            //To add new input file field dynamically, on click of "Add More Files" button below function will be executed
            $('#add_more_sp').click(function() {
                $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                        $("<input/>", {name: 'images_sp[]', type: 'file', id: 'images'}),
                        $("<br/>")
                ));
            });
            //following function will executes on change event of file input to select different file
            $('body').on('change', '#images_sp', function(){
                if (this.files && this.files[0]) {
                    abc += 1; //increementing global variable by 1

                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd"+ abc +"' class='abcd_sp'><img id='previewimg" + abc + "' src=''/></div>");

                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_sp;
                    reader.readAsDataURL(this.files[0]);

                    $(this).hide();
                    $("#abcd_sp"+ abc).append($("<img/>", {id: 'img', src: "{{ asset('public/admin/img/x.png') }}", alt: 'delete'}).click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });
//To preview image
            function imageIsLoaded_sp(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };


        });
        

        //set list_job_parent
        function setListJobParent()
        {
            var arrPrentId = [];
            var listJobParent = "";
            //get all parent ID of checkbox cat
            $('input[name="cat[]"]:checked').each(function () {
                var parentId = $(this).attr("data-parent");
                console.log(parentId);
                arrPrentId.push(parentId);
            });

            //get all parent ID of checkbox hang
            $('input[name="hang[]"]:checked').each(function () {
                var parentId = $(this).attr("data-parent");
                console.log(parentId);
                arrPrentId.push(parentId);
                
            });
            
            //filter to return the first item of each distinct value
            var unique=arrPrentId.filter(function(itm,i,a){
                return i==a.indexOf(itm);
            });

            for(var i in unique)
            {
                listJobParent += "," + unique[i];
            }

            $('#list_job_parent').val(listJobParent);

            console.log(listJobParent);
        }
    </script>

@append