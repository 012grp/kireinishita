@extends('admin.layouts.default')

@section('page_bottom')
<link rel="stylesheet" href="/public/admin/js/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
<script type="text/javascript" src="/public/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(function() {
	$('.datepicker').datepicker({format: 'yyyy-mm-dd',});
});
</script>
@stop

@section('content')
<?php 
    $secretUrl = _helper::getUrlName('alliancePage');
	$categoryList = array('0' => '選択してください');
	$categoryList = array_merge($categoryList ,_helper::getOnlyAllienceJobCategory());
?>
<div class="btn_add">
    <a class="btn btn-primary" href="/admin/alliance-manager/upload-csv" ><i class="glyphicon glyphicon-file"></i> Upload CSV</a>
</div>
{!! Form::open(['url' => '/admin/alliance-manager','method'=>'get', 'files'=>true]) !!}
<div class="box_search row">
    <div class="col-sm-2 mg-top-10">
    	職種
    </div>
    <div class="form-group col-sm-2">
        {!! Form::select('job_category',$categoryList ,Input::get('job_category'), array('class' => 'form-control','placeholder' => '')) !!}
    </div>
</div>

<div class="box_search row">
    <div class="col-sm-2 mg-top-10">
    	掲載開始日
    </div>
    <div class="form-group col-sm-2">
        {!! Form::text('from_date',Input::get('from_date'), array('class' => 'form-control datepicker','placeholder' => '')) !!}
    </div>
    <div class="col-sm-1 text-center mg-top-10">
    	～
    </div>
    <div class="form-group col-sm-2">
        {!! Form::text('to_date',Input::get('to_date'), array('class' => 'form-control datepicker','placeholder' => '')) !!}
    </div>
</div>

<div class="box_search row">
    <div class="col-sm-2 mg-top-10">
    	キーワード
    </div>
    <div class="form-group col-sm-7">
        {!! Form::text('keyword',Input::get('keyword'), array('class' => 'form-control','placeholder' => 'キーワード')) !!}
    </div>
    <div class="col-sm-2">
        {!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['type' => 'submit', 'class' => 'btn btn-default'] ) !!}
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="6%">管理ID</th>
                    <th width="6%">求人id</th>
                    <th >ジョブ名</th>
                    <th width="15%">職種</th>
                    <th width="11%">店舗名</th>
                    <th width="11%">企業名</th>
                    <th class="text-center" width="11%">掲載開始日</th>
                    <th class="text-center"  width="15%">ツールバー</th>
                </tr>
            </thead>
            <tbody>
                @foreach($list as $value)
                <tr>
                    <td><a target="_blank" href="/{{$secretUrl}}/{{ $value['id'] }}">{{ $value['id'] }}</a></td>
                    <td>{{ $value['job_requirement_id'] }}</td>
                    <td>{{ $value['job_name'] }}</td>
                    <td>{{ _helper::getAllianceJobCategoryNameByListId($value['list_job']) }}</td>
                    <td>{{ $value['shop_name'] }}</td>
                    <td>{{ $value['company_name'] }}</td>
                    <td class="text-center"><small>{{ date("Y/m/d", strtotime($value['created_at']))}}</small></td>
                    <td class="text-center">
<!--                       <a href="/admin/alliance-manager/edit/{{ $value->id }}"><i class="glyphicon glyphicon-pencil"></i> 修正</a> &#160;&#160;-->
                       <a href="/admin/alliance-manager/delete/{{ $value->id }}" onclick="if (confirm('Are you sure you, want to delete?')) { return true; } return false;"><i class="glyphicon glyphicon-trash"></i> 削除</a>
                    </td>
                </tr>
                @endforeach 
                
            </tbody>
        </table>

        <!--your page at here-->
        <div class="text-center">
        {!! $list->render() !!}
        </div>
    </div>
</div>
@append