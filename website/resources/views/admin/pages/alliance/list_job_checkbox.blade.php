<div class="modal__inner u-fs--xs">
	<?php $GroupMT30 = ''; $GroupIndex = 1; ?>
	@foreach($modal_data as $GroupName=>$List)
		<h2 class="modal__sub__ttl {{$GroupMT30}}">{{$GroupName}}</h2>
		<div class="modal__checkbox">
		@foreach($List as $record)
			<?php
			$Key = $record['id'];
			$Name = $record['name'];
			$checked='';
			$list_checked = substr($checked_items, 1, -1);
			$_check = explode(',',$list_checked );
			if(in_array($Key,$_check))
			{
				$checked='checked';
			}
			?>
			<input {{$checked}} type="checkbox" name="{{$input_name}}" id="{{$modal_prefix}}-{{$GroupIndex}}" data-num="{{$GroupIndex}}" data-parent="{{$record['parent_id']}}" class="search-checkbox" value="{{$Key}}">
			<label for="{{$modal_prefix}}-{{$GroupIndex}}" class="search-checkbox__label--modal u-mt10">{{$Name}}</label>
			<?php $GroupIndex++; ?>
		@endforeach 
		</div>
		<?php $GroupMT30 = 'u-mt10'; ?>
	@endforeach
</div>