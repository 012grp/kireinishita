@extends('admin.layouts.default')
@section('content')
<?php 
    $secretUrl = _helper::getUrlName('secretPage');
?>
<div class="box_search row">
    {!! Form::open(['url' => 'admin/alliance-manager/upload-csv','method'=>'post', 'files'=>true]) !!}
    <div class="form-group col-sm-7">
    <select name="file_csv" id="" class="form-control">
    @foreach($listCSV as $file)
    <option value="{{$file}}">{{$file}}</option>
    @endforeach 
    </select>
    </div>
    <div class="col-sm-2">
        {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'submit', 'class' => 'btn btn-default'] ) !!}
    </div>

    {!! Form::close() !!}

</div>

<style>
	.table-striped th, .table-striped tr{
		white-space: nowrap;
	}
	.txtwp{
		width: 400px;
		height: 100px;
		white-space: normal;
		overflow: auto;
		resize: both;
	}
</style>

@if($errors->first())
<div class="alert alert-danger"> {{$errors->first()}} </div>
@endif


@if(!empty($report))
<div class="row">
    <div class="col-md-12">
      	
       	<div>Total: {{count($report)}}</div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>求人id</th>
					<th>ジョブ名</th>
					<th>職種ID</th>
					<th>雇用形態ID</th>
					<th>駅ID</th>
					<th>店舗ID</th>
					<th>店舗名</th>
					<th>注目ポイント </th>
					<th>仕事内容</th>
					<th>給与</th>
					<th>都道府県ID</th>
					<th>市区町村ID</th>
					<th>勤務地</th>
					<th>アクセスタイプ</th>
					<th>アクセス時間</th>
					<th>アクセス地図(緯度)</th>
					<th>アクセス地図(経度)</th>
					<th>休日休暇</th>
					<th>求める人物について</th>
					<th>入社お祝い金</th>
					<th>企業名</th>
					<th>一覧画像</th>
					<th>詳細画像1</th>
					<th>送客URL</th>
                </tr>
            </thead>
            <tbody>
                @foreach($report as $value)
                <tr>
                    <td><span class="add_status_{{$value['add_status']}}">{{$value['add_status_message']}}</span></td>
                    <td>{{$value['job_requirement_id']}}</td>
					<td><div class="txtwp">{{$value['job_name']}}</div></td>
					<td>{{$value['list_job']}}</td>
					<td>{{$value['list_em']}}</td>
					<td>{{$value['list_station']}}</td>
					<td>{{$value['shop_id']}}</td>
					<td>{{$value['shop_name']}}</td>
					<td><div class="txtwp">{{$value['job_pr']}}</div></td>
					<td><div class="txtwp">{{$value['job_summary']}}</div></td>
					<td><div class="txtwp">{{$value['job_salary']}}</div></td>
					<td>{{$value['list_province']}}</td>
					<td>{{$value['list_city']}}</td>
					<td>.txtwp{{$value['job_location']}}</td>
					<td>{{$value['access_type']}}</td>
					<td>{{$value['duration']}}</td>
					<td>{{$value['lat']}}</td>
					<td>{{$value['lng']}}</td>
					<td><div class="txtwp">{{$value['job_holiday']}}</div></td>
					<td><div class="txtwp">{{$value['job_wanted_people']}}</div></td>
					<td>{{$value['job_celebration']}}</td>
					<td>{{$value['company_name']}}</td>
					<td><img src="{{$value['job_image']}}" height="70" alt=""></td>
					<td><img src="{{$value['job_thumbnail']}}" height="70" alt=""></td>
					<td><a class="nowarp" href="{{$value['url']}}" target="_blank">{{$value['url']}}</a></td>
                </tr>
                @endforeach 
                
            </tbody>
        </table>
    </div>
</div>
@endif


@append