<hr>
<div class="panel panel-default"> 
	<div class="panel-heading"> 
		<span class="c-tag--secret--notice u-fs--xs">顧問パネル</span>
	</div> 
	<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group" id="adviser_image_block">
						<label for="adviser_image">PCの画像</label>
						<input name="adviser_image" type="file" id="adviser_image">
						<input type="hidden" value="" id="delete_image_adviser" name="delete_image_adviser">
					</div>


					<div class="form-group" id="adviser_thumb_block">
						<label for="adviser_image_sp" id="" class="">SPの画像</label>
						<input name="adviser_image_sp" type="file" id="adviser_image_sp">
						<input type="hidden" value="" id="delete_thumb_adviser" name="delete_thumb_adviser">
					</div>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<input value="{{(!empty($article['adviser_header'])) ? $article['adviser_header'] : Input::old('adviser_header')}}" class="form-control" placeholder="タイトル" name="adviser_header" type="text">
					</div>
					<div class="form-group">
						<input value="{{(!empty($article['adviser_address'])) ? $article['adviser_address'] : Input::old('adviser_address')}}" class="form-control" placeholder="きらめき社労士事務所" name="adviser_address" type="text">
					</div>
					<div class="form-group">
						<input value="{{(!empty($article['adviser_ceo'])) ? $article['adviser_ceo'] : Input::old('adviser_ceo')}}" class="form-control" placeholder="代表取締役 社長" name="adviser_ceo" type="text">
					</div>
					
					<div class="form-group">
						<textarea placeholder="コンテンツ" class="form-control" rows="4" name="adviser_content" cols="50">{{(!empty($article['adviser_content'])) ? $article['adviser_content'] : Input::old('adviser_content')}}</textarea>
					</div>
					<label for="">＜きらめき社労士事務所＞</label>
					<div class="form-group">
						<textarea placeholder="〒169-0075東京都新宿区高田馬場4丁目8-9 NY企画ビル3Ｆ\r\n
TEL:03-6304-0468 FAX:03-6304-0469" class="form-control" rows="2" name="adviser_contact" cols="50">{{(!empty($article['adviser_contact'])) ? $article['adviser_contact'] : Input::old('adviser_contact')}}</textarea>
					</div>
					
					<label for="">ホームページはコチラ</label>
					<div class="form-group">
						<input value="{{(!empty($article['adviser_website'])) ? $article['adviser_website'] : Input::old('adviser_website')}}" class="form-control" placeholder="" name="adviser_website" type="text">
					</div>
					
					<label for="">メールはコチラ</label>
					<div class="form-group">
						<input value="{{(!empty($article['adviser_email'])) ? $article['adviser_email'] : Input::old('adviser_email')}}" class="form-control" placeholder="メールはコチラ" name="adviser_email" type="email">
					</div>
				</div>
			</div>
	</div> 
</div>
<hr>
<script>
$(function() {
	$("#adviser_image").fileinput({
				initialPreview: [<?php if(!empty($article['adviser_image'])){ ?> '<img style="max-height:100px" src="/public/uploads/article/<?php echo $article['adviser_image'] ?>" class="" >' <?php } ?>],
				previewSettings: {
					image: {width: "auto", height: "100px"},
				},
				showUpload: false,
				showRemove: true,
				maxFileSize: 2048,
				msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
				allowedFileExtensions: ["jpg", "gif", "png"],
				overwriteInitial: true,
				allowedFileTypes: ["image"],
				previewFileType: ["image"],
				language: 'ja',
				browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
	});

	$("#adviser_image_sp").fileinput({
				initialPreview: [<?php if(!empty($article['adviser_image_sp'])){ ?> '<img style="max-height:100px" src="/public/uploads/article/<?php echo $article['adviser_image_sp'] ?>" class="" >' <?php } ?>],
				previewSettings: {
					image: {width: "auto", height: "100px"},
				},
				showUpload: false,
				showRemove: true,
				maxFileSize: 2048,
				msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
				allowedFileExtensions: ["jpg", "gif", "png"],
				overwriteInitial: true,
				allowedFileTypes: ["image"],
				previewFileType: ["image"],
				language: 'ja',
				browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
	});

	$('#adviser_image_block .fileinput-remove').click(function() {
		 $('#delete_image_adviser').val("ok");
	});
	$('#adviser_thumb_block .fileinput-remove').click(function() {
		 $('#delete_thumb_adviser').val("ok");
	});

});
</script>
