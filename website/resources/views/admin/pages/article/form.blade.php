@extends('admin.layouts.default')
@section('page_head')
 @include('admin.shared.item.froala_editor_css')
 @include('admin.shared.item.froala_editor_js')
 {!!HTML::style('public/admin/js/fileinput/css/fileinput.css')!!}
 {!!HTML::style('public/admin/js/bootstrap-datepicker/css/bootstrap-datepicker.css')!!}
 {!!HTML::script('public/admin/js/fileinput/fileinput.js')!!}
 {!!HTML::script('public/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
 {!!HTML::script('public/admin/js/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js')!!}
 {!!HTML::script('public/admin/js/fileinput/locales/ja.js')!!}
 {!!HTML::script('public/js/init-googlemap.js')!!}
 {!!HTML::script('https://maps.google.com/maps/api/js?language=ja&region=JP&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs')!!}
@append
@section('content')

<div class="row">
    <div class="col-md-12">

    <!-- if there are creation errors, they will show here -->
        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif


        {!! Form::open(['url' => $link_action,'method'=>'POST', 'files'=>true, 'id' => 'article_form']) !!}
		
      	@if(isset($article['id']))
      		<input type="hidden" name="id" value="{{$article['id']}}">
      	@endif
      	
       	<input type="hidden" id="cat_type" value="{{(!empty($article['cat_id'])) ? $article['cat_id'] : '0'}}">
        
        <div class="form-group">
            {!! Form::label('cat_id', 'ジャンル') !!}
            {!! Form::select('cat_id', $list_cat,(!empty($article['cat_id'])) ? $article['cat_id'] : Input::old('cat_id'), array('class' => 'form-control')) !!}
        </div>
       
        <div class="form-group">
            {!! Form::label('title', 'タイトル')  !!}
            {!! Form::text('title', (!empty($article['title'])) ? $article['title'] : Input::old('title'), array('class' => 'form-control')) !!}
        </div>

        <div class="panel panel-default"> 
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                            {!! Form::label('meta_desc', 'ディスクリプション')  !!}
                            {!! Form::textarea('meta_desc', (!empty($article['meta_desc'])) ? $article['meta_desc'] : Input::old('meta_desc'), array('class' => 'form-control', 'maxlength' => '160', 'rows' => '4')) !!}
                        </div>

                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('meta_keyword', 'キーワード')  !!}
                            {!! Form::textarea('meta_keyword', (!empty($article['meta_keyword'])) ? $article['meta_keyword'] : Input::old('meta_keyword'), array('class' => 'form-control', 'maxlength' => '255', 'rows' => '4')) !!}
                        </div>
                    </div>
                </div>
            </div> 
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group" id="image_div">
                    {!! Form::label('file','PCの画像',array('id'=>'','class'=>'')) !!}
                    {!! Form::file('file') !!}
                    <input type="hidden" value="" id="delete_image" name="delete_image">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group" id="image_thumb_div">
                    {!! Form::label('image_thumb','PCのサムネイル',array('id'=>'','class'=>'')) !!}
                    {!! Form::file('image_thumb') !!}
                    <input type="hidden" value="" id="delete_image_thumb" name="delete_image_thumb">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group" id="image_sp_div">
                    {!! Form::label('image_sp','SPの画像',array('id'=>'','class'=>'')) !!}
                    {!! Form::file('image_sp') !!}
                    <input type="hidden" value="" id="delete_image_sp" name="delete_image_sp">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group" id="image_sp_thumb_div">
                    {!! Form::label('image_sp_thumb','SPのサムネイル',array('id'=>'','class'=>'')) !!}
                    {!! Form::file('image_sp_thumb') !!}
                    <input type="hidden" value="" id="delete_sp_thumb" name="delete_sp_thumb">
                </div>
            </div>
        </div>



        <div class="form-group">
            {!! Form::label('description', '抜粋') !!}
            {!! Form::textarea('description', (!empty($article['description'])) ? $article['description'] : Input::old('description'), array('class' => 'form-control',
            'rows' => 5
            )) !!}
        </div>
		<div class="column-box__contents" id="editor_wrapper">
        <div>マニュアル: <a target="_blank" href="/public/admin/img/manual/article-manual.jpg">私をクリック</a></div>
        <div class="form-group">
            {!! Form::label('content', 'コンテンツ') !!}
            {!! Form::textarea('content', (!empty($article['content'])) ? $article['content'] : Input::old('content'), array('class' => 'form-control froalaEditor')) !!}
        </div>
        </div>
		
       @include('admin.pages.article._adviser_block')
       
        <div class="form-group">
            {!! Form::label('url', 'ホームページ')  !!}
            {!! Form::text('url',(!empty($article['url'])) ? $article['url'] : Input::old('url'), array('class' => 'form-control')) !!}
        </div>
		
       	
       
        <div class="form-group">
            {!! Form::label('company_name', '会社名')  !!}
            {!! Form::text('company_name', (!empty($article['company_name'])) ? $article['company_name'] : Input::old('company_name') , array('class' => 'form-control')) !!}
        </div>
        <div class="form-group">
            {!! Form::text('ceo', (!empty($article['ceo'])) ? $article['ceo'] : Input::old('ceo') , array('class' => 'form-control','placeholder' => '代表取締役 社長 小嶋 雄治')) !!}
        </div>

        {{-- store information --}}
        <div class="form-group">
            {!! Form::label('store_infomation', '店舗情報') !!}
            {!! Form::textarea('store_infomation', (!empty($article['store_infomation'])) ? $article['store_infomation'] : Input::old('store_infomation'), array('class' => 'form-control',
            'rows' => 5
            )) !!}
        </div>

        {{-- Company Profile --}}
        <div class="form-group">
            {!! Form::label('company_profile', '会社概要') !!}
            {!! Form::textarea('company_profile', (!empty($article['company_profile'])) ? $article['company_profile'] : Input::old('company_profile'), array('class' => 'form-control',
            'rows' => 5
            )) !!}
        </div>

        {{--<div class="form-group">--}}
            {{--{!! Form::label('address', '住所')  !!}--}}
            {{--{!! Form::text('address', (!empty($article['address'])) ? $article['address'] : Input::old('address') , array('class' => 'form-control')) !!}--}}
        {{--</div>--}}

        <div>マニュアル: <a target="_blank" href="/public/admin/img/manual/find-location.jpg">私をクリック</a></div>
        <div class="form-group">
            {!! Form::label('map_location', '地図位置')  !!}
            <div class="row form-group">
                <div class="col-sm-9">
                    {!! Form::text('map_location',(!empty($article['map_location'])) ? $article['map_location'] : Input::old('map_location'), array('class' => 'form-control input-map-location ' , 'id' => 'map_location')) !!}
                </div>
                <div class="col-sm-3">
                    <input id="find_location" type="button" style="" class="btn btn-primary btn-map-location" value="見つけます" />
                    <input id="remove_location" type="button" style="" class="btn btn-primary btn-map-location" value="場所を削除" />
                </div>
            </div>

            <div id="map" style="height: 500px;"></div>
            {!! Form::hidden('lat', (!empty($article['lat'])) ? $article['lat'] : Input::old('lat'),array('id'=>'lat')) !!}
            {!! Form::hidden('lng', (!empty($article['lng'])) ? $article['lng'] : Input::old('lng'),
            array('id'=>'lng')) !!}
            {!! Form::hidden('id',(!empty($article['id'])) ? $article['id'] : Input::old('id') ) !!}
            {!! Form::hidden('preview_kind', 'PC', array('id'=>'preview_kind'))!!}
        </div>

        <div class="row">
            <div class="form-group col-sm-2">
                {!! Form::label('status', '状態') !!}
                {!! Form::select('status', array(0=> '未発表', 1 => '公開'),(!empty($article['status'])) ? $article['status'] : 0, array('class' => 'form-control')) !!}
            </div>
        </div>


        <div class="form-group">
            <div class="row">
                <div class="col-md-2">
                    {!! Form::label('created_at', '作成日')  !!}
                    <?php 
                        $date_created = !empty($article['created_at']) ? $article['created_at'] : Input::old('created_at');
                        $date_created = date_create($date_created);
                        if($date_created)
                        {
                            $date_created = date_format($date_created,"Y/m/d");
                        }
                        
                    ?>
                    {!! Form::text('created_at', $date_created , array('class' => 'form-control datepicker')) !!}
                </div>
                <div class="col-md-10">
                </div>
            </div>
        </div>

        <div class="form-group">
           <div class="row">
           	<div class="col-md-2">
           		{!! Form::submit('申し出る', ['class' => 'btn btn-primary', 'id' => 'btnSubmit']) !!}
           	</div>
           	<div class="col-md-10">
           		{!! Form::submit('PC-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewPC']) !!}
            	{!! Form::submit('SP-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewSP']) !!}
           	</div>
            </div>
        </div>

        {!! Form::close() !!}



    </div>
</div>

<script type="text/javascript">
    //datepicker date created
    $('.datepicker').datepicker({
        format: 'yyyy/mm/dd',
        language: 'ja',
        endDate: '+0d',
    });

    $("#btnPreviewPC").click(function(){
        $("#article_form").removeAttr("onsubmit");
        $("#article_form").attr('target', '_blank');
        $("#article_form").attr('action', '/admin/article-manager/preview');
        $("#preview_kind").val("PC");
    });

    $("#btnPreviewSP").click(function(){
        $("#article_form").attr('onsubmit', 'target_popup(this)');
        $("#article_form").attr('action', '/admin/article-manager/preview');
        $("#preview_kind").val("SP");
    });
    
    function target_popup(form) {
        window.open('', 'formpopup', 'width=414,height=650,resizeable,scrollbars');
        form.target = 'formpopup';
    }

    $("#btnSubmit").click(function(){
        $("#article_form").removeAttr("onsubmit");
        $("#article_form").attr('target', '');
        $("#article_form").attr('action', '/'+'<?php echo $link_action; ?>');
    });
    
    $("#image_sp").fileinput({
        initialPreview: [<?php if(!empty($article['image_sp'])){ ?> '<img style="max-height:150px" src="/public/uploads/article/<?php echo $article['image_sp'] ?>" class="" >' <?php } ?>],
        previewSettings: {
            image: {width: "auto", height: "150px"},
        },
        showUpload: false,
        showRemove: true,
        maxFileSize: 2048,
        msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: true,
        allowedFileTypes: ["image"],
        previewFileType: ["image"],
        language: 'ja',
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
    });

    $("#image_sp_thumb").fileinput({
        initialPreview: [<?php if(!empty($article['image_sp_thumb'])){ ?> '<img style="max-height:150px" src="/public/uploads/article/<?php echo $article['image_sp_thumb'] ?>" class="" >' <?php } ?>],
        previewSettings: {
            image: {width: "auto", height: "150px"},
        },
        showUpload: false,
        showRemove: true,
        maxFileSize: 2048,
        msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: true,
        allowedFileTypes: ["image"],
        previewFileType: ["image"],
        language: 'ja',
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
    });

    $("#file").fileinput({
        initialPreview: [<?php if(!empty($article['image'])){ ?> '<img style="max-height:150px" src="/public/uploads/article/<?php echo $article['image'] ?>" class="" >' <?php } ?>],
        previewSettings: {
            image: {width: "auto", height: "150px"},
        },
        showUpload: false,
        showRemove: true,
        maxFileSize: 2048,
        msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: true,
        allowedFileTypes: ["image"],
        previewFileType: ["image"],
        language: 'ja',
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
    });

    $("#image_thumb").fileinput({
        initialPreview: [<?php if(!empty($article['image_thumb'])){ ?> '<img style="max-height:150px" src="/public/uploads/article/<?php echo $article['image_thumb'] ?>" class="" >' <?php } ?>],
        previewSettings: {
            image: {width: "auto", height: "150px"},
        },
        showUpload: false,
        showRemove: true,
        maxFileSize: 2048,
        msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: true,
        allowedFileTypes: ["image"],
        previewFileType: ["image"],
        language: 'ja',
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
    });

    $( document ).ready(function() {
        //delete image thumb
        $('#image_sp_div .fileinput-remove').click(function() {
            $('#delete_image_sp').val("image_sp_delete");
        });

        $('#image_div .fileinput-remove').click(function() {
            $('#delete_image').val("image_delete");
        });

        $('#image_thumb_div .fileinput-remove').click(function() {
            $('#delete_image_thumb').val("image_sp_thumb_delete");
        });

        $('#image_sp_thumb_div .fileinput-remove').click(function() {
            $('#delete_sp_thumb').val("image_thumb_delete");
        });



        $('.froalaEditor').froalaEditor({
            language: 'ja',
            height: 700,
			// Define new paragraph styles.
			paragraphStyles: {
				"column-box_contents__subTtl u-fs--xxlh--contents u-fwb": 'Content H2',
				"column-box_contents__text u-fs--lh--contents": 'Content Text P',
				"column-box_contents__text u-fs--lh--contents u-mt15": 'Content Text P u-mt15',
                "column-box_contents__text u-fs--lh--contents u-mt25": 'Content Text P u-mt25',
			},
			paragraphFormat: {
				N: 'Normal',
				H2: 'Heading 2',
				H3: 'Heading 3',
			},
			//enter: $.FroalaEditor.ENTER_BR,
			toolbarButtons: [
							 'fullscreen', 
							 'bold', 
							 'italic', 
							 'underline', 
							 'strikeThrough', 
							 'subscript', 
							 'superscript', 
							 'fontFamily', 
							 'fontSize', 
							 '|', 
							 'color', 
							 'emoticons', 
							 'inlineStyle', 
							 'paragraphStyle', 
							 '|', 
							 'paragraphFormat', 
							 'align', 
							 'formatOL', 
							 'formatUL', 
							 'outdent', 
							 'indent', 
							 'quote', 
							 'insertHR', 
							 '|', 
							 'insertLink', 
							 'insertImage', 
							 'insertVideo', 
							 'insertFile', 
							 'insertTable', 
							 'undo', 
							 'redo', 
							 'clearFormatting', 
							 'selectAll', 
							 'html',
				
							 '-', //breakline 
							 'runda_h2_wrapper',
							 '|',
							 'runda_h2border_wrapper',
							 '|',
							 'runda_h3_wrapper',
							 '|',
							 'runda_h3_60_wrapper',
							 '|',
							 'runda_p_wrapper',
							 '|',
							 'runda_gray_area',
							 'runda_gray_area_qa',
							 '|',
							 'runda_qa_wrapper',
							 '|',
							 'runda_address_wrapper',
							 '|',
							 'runda_image_wrapper',
							 '|', 
							 'runda_image_wrapper_2image'
							]
        });

        // set google map
        // input lat, lng , inforwindow, zoom
         <?php
              $lat = (!empty($article['lat'])) ? $article['lat'] : 0;
              $lng =(!empty($article['lng'])) ? $article['lng'] : 0;
          ?>
         <?php if(!empty($article['map_location'])){?>
        var contentString = "<h4 style='font-weight: bold;'><?php echo $article['company_name']; ?></h4><p><?php echo $article['map_location']; ?> </p> ";
        <?php }else{ ?>
        var contentString = "";
        <?php   } ?>
        initMap(<?php echo $lat;?> ,<?php echo $lng;?>,contentString, 14,'map');
        $("#find_location").click(function() {
            var address =$("#map_location").val();
            var company_name =$("#company_name").val();
            var p = null;
            $.ajaxSetup({
                async: false
            });
            var ctString = "<h4 style='font-weight: bold;'>"+company_name+"</h4><p>"+address+"</p> ";
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false', null, function (data) {
                var p = data.results[0].geometry.location;
                $('#lat').attr('value', p.lat);
                $('#lng').attr('value', p.lng);
                initMap(p.lat, p.lng, ctString, 14, 'map' );
            });
        });

        $("#remove_location").click(function() {
            var address = "";
            var company_name = "";
            var p = null;
            $.ajaxSetup({
                async: false
            });
            var ctString = "";
            $("#map_location").val("");
            $('#lat').attr('value', 0);
            $('#lng').attr('value', 0);
            initMap(0, 0, ctString, 14, 'map' );
        });

    });
</script>
@append

