<?php

$isNew = $link_action == 'admin/category-manager/create' ? 1 : 0;
?>
@extends('admin.layouts.default')
@section('page_head')
    {!!HTML::script('public/js/init-googlemap.js')!!}
    {!!HTML::script('https://maps.google.com/maps/api/js?language=ja&region=JP&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs')!!}
@append
@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- if there are creation errors, they will show here -->
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if(Session::has('flash_error'))
                @if (Session::get('flash_error') != "8文字以上の半角英数字を入力してください")
                    <p class="alert alert-danger">{{ Session::get('flash_error') }}</p>
                @endif
            @endif

            {!! Form::open(['url' => $link_action,'method'=>'POST', 'files'=>true, 'id' => 'company_form']) !!}
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1">
                        {!! Form::label('name', 'ジャンル')  !!}
                    </div>
                    <div class="col-sm-8">
                        <select class="form-control" name="parent_id" id="parent_id">
                            @foreach($listParent as $item)
                                <option {{ !empty($JobCategory->parent_id) && $JobCategory->parent_id == $item->id ? 'selected' : '' }}  value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1">
                        {!! Form::label('name', '職種名')  !!}
                    </div>
                    <div class="col-sm-8">
                        @if($isNew)
                            {!! Form::text('name', (!empty($JobCategory->description)) ? $JobCategory->description : Input::old('name'), array('class' => 'form-control' )) !!}
                        @else
                            <select class="form-control" name="id" id="id">
                                @foreach($listCat as $item)
                                    <option {{ !empty($JobCategory->id ) && $JobCategory->id == $item->id ? 'selected' : '' }} data-parent="{{$item->parent_id}}"
                                            value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1">
                        {!! Form::label('name', '職種の説明')  !!}
                    </div>
                    <div class="col-sm-8">
                        {!! Form::textarea('description', (!empty($JobCategory->description)) ? $JobCategory->description : Input::old('name'), array('class' => 'form-control' )) !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-1">
                        {!! Form::label('name', '状態')  !!}
                    </div>
                    <div class="col-sm-8">
                        {!! Form::checkbox('status',1,$JobCategory->status) !!}
                    </div>
                </div>
            </div>


            <div class="form-group">
                <input type="hidden" name="action" id="action" value=""/>
                <div class="row">
                    <div class="col-md-4">
                        <a href="/admin/category-manager" class="btn btn-default">キャンセル</a>
                        {!! Form::submit('完了', ['class' => 'btn btn-primary', 'id' => 'btnSubmit']) !!}
                    </div>
                </div>
            </div>


            {!! Form::close() !!}


        </div>
    </div>

    <script>

        $(document).ready(function () {
            showjobCat();
            $('select[name=parent_id]').change(function (e) {
                showjobCat();
            });
        });

        function showjobCat() {
            var parent_id = $('select[name=parent_id]').val();
            $('select[name=id] option').hide();
            $('select[name=id] option[data-parent=' + parent_id + ']').show();

        }
    </script>

@append

