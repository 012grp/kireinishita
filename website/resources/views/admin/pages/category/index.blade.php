@extends('admin.layouts.default')
@section('content')
{{--<div class="btn_add">--}}
    {{--<a class="btn btn-primary" href="/admin/category-manager/create" ><i class="glyphicon glyphicon-file"></i> 新規作成</a>--}}
{{--</div>--}}

<div class="box_search row">
    {!! Form::open(['url' => '/admin/category-manager','method'=>'get', 'files'=>true]) !!}
    <div class="form-group col-sm-7">
        {!! Form::text('keyword',Input::get('keyword'), array('class' => 'form-control','placeholder' => '検索キーワードを入力してください')) !!}
    </div>
    <div class="col-sm-2">
        {!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['type' => 'submit', 'class' => 'btn btn-default'] ) !!}
    </div>

    {!! Form::close() !!}

</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="10%">番号ID</th>
                    <th>ジャンル</th>
                    <th class="text-center" >職種名</th>
                    <th class="text-center"  width="20%">ツールバー</th>
                </tr>
            </thead>
            <tbody>
                @foreach($content as $value)
                <tr>
                    <td>{{ $value['id'] }}</td>
                    <td>{{ _helper::getJobCategoryById($value['parent_id'])->name }}</td>
                    <td >{{ $value['name'] }}</td>
                    <td class="text-center">
                       <a href="/admin/category-manager/edit/{{ $value->id }}"><i class="glyphicon glyphicon-pencil"></i> 修正</a> &#160;&#160;      
                        <a href="/admin/category-manager/delete/{{ $value->id }}" onclick="if (confirm('Are you sure you, want to delete?')) { return true; } return false;"><i class="glyphicon glyphicon-trash"></i> 削除</a>
                    </td>
                </tr>
                @endforeach 
            </tbody>
        </table>
    </div>
</div>
@append