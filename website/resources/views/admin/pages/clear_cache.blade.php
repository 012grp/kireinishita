@extends('admin.layouts.default')

@section('content')
    <div class="box_clear_cache">
        <a href="/admin/clear-cache/?name=all"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete all cache</a>
        <h4>Cache list</h4>
        <ul>
            <?php if(Cache::has('__employments')){ ?>
            <li><a href="/admin/clear-cache/?name=__employments"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                    __employments</a></li>
            <?php } ?>
            <?php if(Cache::has('__category_level1')){ ?>
            <li><a href="/admin/clear-cache/?name=__category_level1"><i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete __category_level1</a></li>
            <?php } ?>
            <?php if(Cache::has('__job_category')){ ?>
            <li><a href="/admin/clear-cache/?name=__job_category"><i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete __job_category</a></li>
            <?php } ?>
            <?php if(Cache::has('__province_names')){ ?>
            <li><a href="/admin/clear-cache/?name=__province_names"><i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete __province_names</a></li>
            <?php } ?>
            <?php if(Cache::has('__city_names')){ ?>
            <li><a href="/admin/clear-cache/?name=__city_names"><i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete __city_names</a></li>
            <?php } ?>
            <?php if(Cache::has('__all_waysites')){ ?>
            <li><a href="/admin/clear-cache/?name=__all_waysites"><i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete __all_waysites</a></li>
            <?php } ?>
        </ul>
    </div>
@append