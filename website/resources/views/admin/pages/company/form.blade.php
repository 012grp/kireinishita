@extends('admin.layouts.default')
@section('page_head')
 {!!HTML::script('public/js/init-googlemap.js')!!}
 {!!HTML::script('https://maps.google.com/maps/api/js?language=ja&region=JP&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs')!!}
@append
@section('content')
<div class="row">
    <div class="col-md-12">

    <!-- if there are creation errors, they will show here -->
        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        @if(Session::has('flash_error'))
            @if (Session::get('flash_error') != "8文字以上の半角英数字を入力してください")
                <p class="alert alert-danger">{{ Session::get('flash_error') }}</p>
            @endif
        @endif
        
        {!! Form::open(['url' => $link_action,'method'=>'POST', 'files'=>true, 'id' => 'company_form']) !!}

        <div class="form-group">
            {!! Form::label('name', '企業名')  !!}<span style="color: red;">※必須</span>
            {!! Form::text('name', (!empty($company['name'])) ? $company['name'] : Input::old('name'), array('class' => 'form-control' , 'required')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('representative', '代表者名')  !!}
            {!! Form::text('representative', (!empty($company['representative'])) ? $company['representative'] : Input::old('representative'), array('class' => 'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('work_content', '事業内容')  !!}
            {!! Form::textarea('work_content', (!empty($company['work_content'])) ? $company['work_content'] : Input::old('work_content'), array('class' => 'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('address', '本社所在地')  !!}
            {!! Form::text('address', 
            (!empty($company['address'])) ? $company['address'] : Input::old('address'), 
            array('class' => 'form-control', 'placeholder' => '例: 東京都品川区東品川2-3-10 シーフォートスクエア1F')) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('email', 'Eメール')  !!}<span style="color: red;">※必須</span>
            {!! Form::email('email',
            (!empty($company['email'])) ? $company['email'] : Input::old('email'),
            array('class' => 'form-control','required', 'placeholder' => 'example@email.com')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Password')  !!}
            <div class="row">
                <div class="form-group col-sm-10">
                    {!! Form::text('password',"",array('class' => 'form-control', 'placeholder' => '8文字以上の半角英数字を入力してください')) !!}
                </div>

                <div class="form-group col-sm-2">
                    {!! Form::button('パスワードを作成', ['class' => 'btn btn-primary', 'id' => 'btnGenerate','onclick' => 'generate();']) !!}
                </div>
            </div>

            @if(Session::has('flash_error'))
                @if (Session::get('flash_error') == "8文字以上の半角英数字を入力してください")
                    <p class="alert alert-danger">{{ Session::get('flash_error') }}</p>
                @endif
            @endif
        </div>

        <div class="form-group">
            {!! Form::label('website', 'URL')  !!}
            {!! Form::text('website',
            (!empty($company['website'])) ? $company['website'] : Input::old('website'),
            array('class' => 'form-control', 'placeholder' => 'www.company.com')) !!}
        </div>
        <?php /*
        <hr>
        <div>マニュアル: <a target="_blank" href="/public/admin/img/manual/find-location.jpg">私をクリック</a></div>
        <div class="form-group">
            <label for="map_location">地図位置 <small>Google map address</small></label>
            <div class="row form-group">
                <div class="col-sm-9">
                    {!! Form::text('map_location',(!empty($company['map_location'])) ? $company['map_location'] : Input::old('map_location'), array('class' => 'form-control input-map-location ' , 'id' => 'map_location')) !!}
                </div>
                <div class="col-sm-3">
                    <input id="find_location" type="button" style="" class="btn btn-primary btn-map-location" value="見つけます" />
                    <input id="remove_location" type="button" style="" class="btn btn-primary btn-map-location" value="場所を削除" />
                </div>
            </div>
       

            <div id="map" style="height: 500px;"></div>
            {!! Form::hidden('lat', (!empty($company['lat'])) ? $company['lat'] : Input::old('lat'),array('id'=>'lat')) !!}
            {!! Form::hidden('lng', (!empty($company['lng'])) ? $company['lng'] : Input::old('lng'),
            array('id'=>'lng')) !!}
            {!! Form::hidden('id',(!empty($company['id'])) ? $company['id'] : Input::old('id') ) !!}
            {!! Form::hidden('preview_kind', 'PC', array('id'=>'preview_kind'))!!}
            </div>
          */ ?>


        <div class="row">
            <div class="form-group col-sm-2">
                {!! Form::label('status', '状態') !!}
                {!! Form::select('status', array(0=> '未発表', 1 => '公開'),(!empty($company['status'])) ? $company['status'] : 0, array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="form-group">
            <input type="hidden" name="action" id="action" value=""/>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::submit('保存する', ['class' => 'btn btn-primary', 'id' => 'btnSubmit']) !!}
                    {!! Form::submit('パスワード送信する', ['class' => 'btn btn-primary', 'id' => 'btnSendMail', 'disabled' => 'disabled']) !!}

                </div>
                <div class="col-md-8">
                    {!! Form::submit('PC-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewPC']) !!}
                    {!! Form::submit('SP-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewSP']) !!}
                </div>
            </div>
        </div>


        {!! Form::close() !!}



    </div>
</div>

<script>
    function target_popup(form) {
        window.open('', 'formpopup', 'width=414,height=650,resizeable,scrollbars');
        form.target = 'formpopup';
    }
    
    $(document ).ready(function() {
        if($('#password').val())
        {
            $('#btnSendMail').removeAttr("disabled");
        }

        $("#btnGenerate").click(function(){
            $('#btnSendMail').removeAttr("disabled");
        });

        $('#password').on('input',function(e){
            if($(this).val())
            {
                $('#btnSendMail').removeAttr("disabled");
            }
            else
            {
                $('#btnSendMail').attr("disabled", "disabled");
            }
        });

        $("#btnPreviewPC").click(function(){
            $("#company_form").removeAttr("onsubmit");
            $("#company_form").attr('target', '_blank');
            $("#company_form").attr('action', '/admin/company-manager/preview');
            $("#preview_kind").val("PC");
        });

        $("#btnPreviewSP").click(function(){
            $("#company_form").attr('onsubmit', 'target_popup(this)');
            $("#company_form").attr('action', '/admin/company-manager/preview');
            $("#preview_kind").val("SP");
        });

        

        $("#btnSubmit").click(function(){
            $("#action").val("1");
            $("#company_form").removeAttr("onsubmit");
            $("#company_form").attr('target', '');
            $("#company_form").attr('action', '/'+'<?php echo $link_action; ?>');
        });

        $("#btnSendMail").click(function(){
            $("#action").val("2");
        });

        // set google map
        // input lat, lng , inforwindow, zoom
         <?php
            $lat = (!empty($company['lat'])) ? $company['lat'] : 0;
            $lng =(!empty($company['lng'])) ? $company['lng'] : 0;
        ?>

        <?php if($lat && $lng): ?>
            var contentString = "";
            <?php if(!empty($company['name'])): ?>
                contentString += "<h4 style='font-weight: bold; color: black;'><?php echo $company['name']; ?></h4>";
            <?php endif; ?>

            <?php if(!empty($company['address'])): ?>
                contentString += "<p><?php echo $company['address']; ?></p>";
            <?php endif; ?>

            initMap(<?php echo $lat;?> ,<?php echo $lng;?>, contentString, 14, 'map');
        <?php else: ?>
            initMap(<?php echo $lat;?> ,<?php echo $lng;?>, "", 14, 'map');
        <?php endif; ?>

        $("#find_location").click(function() {
            var map_location = $("#map_location").val();
            var address = $("#address").val();
            var company_name = $("#name").val();
            var p = null;
            $.ajaxSetup({
                async: false
            });
            var ctString = "<h4 style='font-weight: bold;'>"+company_name+"</h4><p>"+address+"</p> ";
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + map_location + '&sensor=false', null, function (data) {
                var p = data.results[0].geometry.location;
                $('#lat').attr('value', p.lat);
                $('#lng').attr('value', p.lng);
                initMap(p.lat, p.lng, ctString, 14, 'map' );
            });
        });


        $("#remove_location").click(function () {
            var address = "";
            var p = null;
            $.ajaxSetup({
                async: false
            });

            $("#map_location").val("");
            $('#lat').attr('value', 0);
            $('#lng').attr('value', 0);
            initMap(0, 0, address, 14, 'map');
        });
    });

    function generate() {
        $("#password").val( createPassword(8));
    }

    function createPassword(length) {
        var alpha = "abcdefghijklmnopqrstuvwxyz";
        var caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var numeric = "0123456789";
        var special = "!$^&*-=+_?";

        var options = [alpha, numeric];

        var password = "";
        var passwordArray = Array(length);

        for (i = 0; i < length; i++) {
            var currentOption = options[Math.floor(Math.random() * options.length)];
            var randomChar = currentOption.charAt(Math.floor(Math.random() * currentOption.length));
            password += randomChar;
            passwordArray.push(randomChar);
        }

        checkPassword();

        function checkPassword() {
            var missingValueArray = [];
            var containsAll = true;

            options.forEach(function (e, i, a) {
                var hasValue = false;
                passwordArray.forEach(function (e1, i1, a1) {
                    if (e.indexOf(e1) > -1) {
                        hasValue = true;
                    }
                });

                if (!hasValue) {
                    missingValueArray = a;
                    containsAll = false;
                }
            });

            if (!containsAll) {
                passwordArray[Math.floor(Math.random() * passwordArray.length)] = missingValueArray.charAt(Math.floor(Math.random() * missingValueArray.length));
                password = "";
                passwordArray.forEach(function (e, i, a) {
                    password += e;
                });
                checkPassword();
            }
        }

        return password;
    }
</script>

@append

