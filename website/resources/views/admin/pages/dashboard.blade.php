@extends('admin.layouts.default')
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Statistic</h4></div>
            </div>
            <div class="panel-body">
                <div class="peacei">
                    <h3>Jobs: <span>{{ $contentCount }}</span></h3>
                </div>
                <div class="peacei">
                    <h3>Users: <span>{{ $userCount }}</span></h3>
                </div>
            </div>
        </div>


    </div>
    <div class="col-md-8">





    </div>
</div>
@stop