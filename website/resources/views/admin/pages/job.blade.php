<?php 
	$title_page = '求人の管理';
?>
@extends('admin.layouts.default')

@section('content')
<?php 
    $pattern = env('JOB_SALARY_REGEX', 0);
?>

<style>
	.show-job th > span {
		padding: 2px 5px;
		margin-right: 5px;
		text-align: center;
		background: #727272;
		color: white;
	}
	.scrollbar-wp {
		border: none 0px RED;
		overflow-x: scroll; 
		overflow-y:hidden;
	}
	.sub-scrollbar{
		width: 1000px;
		height: 1px; 
	}
    .table{
        table-layout: fixed;
    }
   
    
    .table td  .wp-height{
        height: auto;
    }
    
    input[type="checkbox"]{
        width: 20px;
        height: 18px;
        vertical-align: middle;
    }
    .wp-image{
        position: relative;
    }
    .wp-image .hv{
        position: absolute;
        z-index: 1;
        top: -176px;
        left: -317px;
        width: 300px;
        height: 100%;
        display: none;
        border: 2px solid black;
        overflow: hidden;
    }
    .wp-image .hv img{
        vertical-align: middle;
    }
    .wp-image:hover .hv{
        display: block;
    }
	.title_statistic {
		margin: 0;
		font-size: 16px;
		font-weight: bold;
		padding-top: 15px;
	}

	.padding-top-10 {
		padding-top: 10px;
	}

	.table-responsive {
		overflow-y: auto;
		height: 200px;
	}

</style>

<form action="" method="get">
	<div class="form-group">
		<div class="row">
			<div  class="col-md-5">
				<div class="row">
					<div class="col-md-6">
						Fields:
						<select name="action" id="action" class="form-control">
							<option id="Any" value="Any" selected>Any info</option>
							<option id="ID" value="ID">ID</option>
							<option id="Name" value="Name">Name</option>
							<option id="Detail" value="Detail">Detail</option>
							<option id="Company" value="Company">Company</option>
							<option id="Career" value="Career">Career</option>
							<option id="Salary" value="Salary">Salary</option>
							<option id="Expire" value="Expire">Expire</option>
							<option id="Location" value="Location">Location</option>
							<option id="Type" value="Type">Type</option>
							<option id="RequirementSpecial" value="RequirementSpecial">Requirement Special</option>
						</select>
						<script type="text/javascript">
							document.getElementById('{{$action or 'Any'}}').selected = true;
						</script>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							From (yyyy-mm-dd):
							<input value="{{$fromDate or ''}}" name="from_date" id="fromDate" type="text" class="form-control" placeholder="yyyy-mm-dd">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 padding-top-10">
						Keyword:
						<div class="input-group">
							<input value="{{$value or ''}}" name="value" id="value" type="text" class="form-control" placeholder="Search for...">
							<span class="input-group-btn"> <button class="btn btn-default" type="submit">Search</button> </span>
						</div>
					</div>
				</div>


			</div>
			<div  class="col-md-7">
				<div class="row">
					<div class="col-md-6"><h3 class="title_statistic">Statistics</h3></div>
					<div class="col-md-6 text-right"><button style="margin-bottom: 10px;" class="btn btn-primary"  onClick="window.location.reload()">Refresh</button></div>
				</div>


				<div class="table-responsive">
					<table class="table table-inverse">
						<tr>
							<th width="50%">URL</th>
							<th>Site Name</th>
							<th>Clicked</th>
						</tr>
						@if(!empty($statistic))

							@foreach($statistic as $vs)
								<tr>
									<td>{!! $vs->site_url !!}</td>
									<td>{!! $vs->site_name !!}</td>
									<td>{!! $vs->clicked !!}</td>
								</tr>

							@endforeach
						@endif
					</table>
				</div>
			</div>
		</div>


	</div>
</form>



<div class="row">
	<div class="col-md-12">
		<p>Total: <strong>{{ $contents->total() }}</strong> jobs</p>
	</div>
	<div class="col-md-12">
		<!--your page at here-->
		<div class="text-center top-paginator">
			{!! $contents->render() !!}
		</div>
		
		<div class="scrollbar-wp" id="topScroll">
			<div class="sub-scrollbar" id="topComponent"></div>
		</div>
		<div class="show-job" id="showJob">
			<table class="table table-striped" id="dataComponent">
				<thead>
					<tr>
						<th style="width:80px">Id</th>
						<th style="width:145px"><span>H</span>写真/Image</th>
						<th style="width:350px"><span>A</span>キャッチコピー/Name</th>
						<th style="width:200px"><span>B</span>会社・店舗名/Company</th>
						<th style="width:250px"><span>C</span>職種/Carrer</th>
						<th style="width:500px"><span>D</span>給与/Salary</th>
						<th style="width:280px"><span>E</span>勤務地/Location</th>
						<th style="width:250px"><span>F</span>雇用形態/Job type</th>
						<th style="width:500px"><span>G</span>こだわり/Requirement Special</th>
						<th style="width:150px">Clicked</th>
						<th style="width:150px">Created date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($contents as $i=>$x)
					
					<tr>
						<td><div class="wp">{!! $x['id'] !!}</div></td>
						<td>
							@if(!empty($x['job_image']))
							<div class="wp-image">
							    <div class="wp">
								    <img src="{!! $x['job_image'] !!}" alt="">
								</div>
							</div>
							@endif
						</td>
						<td><div class="wp">
							<a target="_blank" href="{!! $x['job_url'] !!}">{!! $x['job_name'] !!}</a>
						</div></td>
						<td><div class="wp">{!! $x['job_company'] !!}</div></td>
			        	<td><div class="wp"><div style="overflow-y: auto;max-height: 150px"> {!! $x['job_career'] !!}</div></div></td>
				        <td><div class="wp">{!! $x['job_salary'] !!}</div></td>
						<td><div class="wp">{!! $x['job_location'] !!}</div></td>
						<td><div class="wp">{!! $x['job_type'] !!}</div></td>
						<td><div class="wp">{!! $x['requirement_special'] !!}</div></td>
						<td><div class="wp">{!! $x['job_url_clicked'] !!}</div></td>
						<td><div class="wp">{!! $x['created'] !!}</div></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		
		<!--your page at here-->
		<div class="text-center">
			{!! $contents->render() !!}
		</div>
	</div>
</div>
@stop




@section('page_bottom')
	<script type="text/javascript">
		$(function() {
				$('#topComponent').width($('#dataComponent').width());
				
				
				$("#topScroll").scroll(function(){
					$("#showJob").scrollLeft($("#topScroll").scrollLeft());
				});

				$("#showJob").scroll(function(){
					$("#topScroll").scrollLeft($("#showJob").scrollLeft());
				});

				$('#checkAll').change(function () {
					$('input[name="id[]"]').prop('checked', this.checked);
				})

				$('#updatedForm').submit(function (e) {

					if($('input[name="id[]"]:checked').length == 0)
					{

						alert('No job to delete');
						e.preventDefault();
						return;
					}

					if(!confirm('Are you sure want to delete your checked jobs ?'))
					{
						e.preventDefault();
					}
				})
		});
	</script>
@stop