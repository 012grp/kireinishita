@extends('admin.layouts.default')

@section('content')
    
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Keyword</th>
                    <th>Found count</th>
                    <th>Job result</th>
                    <th>Last search</th>
                </tr>
            </thead>
            <tbody>
                @foreach($keywords as $keyword)
                <tr>
                    <td>{{ $keyword['keyword'] }}</td>
                    <td>{{ $keyword['count'] }}</td>
                    <td>{{ $keyword['result'] }}</td>
                    <td>{{ $keyword['updated_at'] }}</td>
                </tr>
                @endforeach 
                
            </tbody>
        </table>

        <!--your page at here-->
        <div class="text-center">
        {!! $keywords->render() !!}
        </div>
    </div>
</div>


@stop