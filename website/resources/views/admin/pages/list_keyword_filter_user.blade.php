<?php 
	$title_page = 'キーワード: ' .$title.'の管理';
?>
@extends('admin.layouts.default') 
@section('content')
<style>
	.sprite-social {
		background: url('/public/admin/images/16_pixel_social_icons.png') no-repeat;
		width: 16px;
		height: 16px;
		display: inline-block;
	}
	
	.sprite-0:after {
		content: '@';
	}
	
	.sprite-0 {
		background: none;
		color: gray;
		font-size: 12px;
	}
	
	.sprite-1 {
		background-position: -55px -19px;
	}
	
	.sprite-2 {
		background-position: -1px -37px;
	}
	.sprite-3 {
		background-position: -163px -19px;
	}
	.register-total,
	.register-find {
		margin-bottom: 25px;
	}
	
	.register-total {
		border: 1px solid #D8D8D8;
	}
	
	.register-find > div {
		line-height: 35px;
	}
	
	.register-find select {
		height: 35px;
		border: 0;
		outline: 0;
	}
	
	.select-inv {
		border: 0;
		outline: 0;
	}
	
	.register-total > div > * {
		vertical-align: middle;
	}
	
	.register-total .amount {
		display: inline-block;
		margin-right: 15px;
	}
	.txtKeyword{
		width: 100%;
		padding: 4px;
		border: 1px solid #dddddd;
		letter-spacing: 1px;
		color: #333b43;
		padding-right: 55px;
	}
	.update-key-form{
		position: relative;
	}
	.update-key-form button{
		position: absolute;
		right: 0;
		top: 0;
		font-size: 12px;
		color: #ffffff;
		background: #ddd;
		border: 0;
		height: 30px;
/*		display: none;*/
	}

	.update-key-form .txtKeyword:focus + button{
		background: #337ab7;
	}

	.update-key-form .txtKeyword:focus+ button:before{
		content: "入力の例: key1|key2|keyN";
		position: absolute;
		top: -29px;
		background: black;
		padding: 5px 10px;
		border-radius: 3px;
		display: block;
		width: 180px;
		right: 0;
	}
	
	.table-striped td{
		vertical-align: middle !important;
	}
	.m-select{
		border-color: #c6c6c6;
		font-size: 15px;
		margin-right: 5px;
		padding: 2px;
		vertical-align: middle;
	}
	
	.error{
		color: red;
	}
	.success{
		color: green;
		margin-right: 10px;
	}
	
	.info{
		background: #e5e5e5;
		padding: 0 5px;
		display: block;
		margin-bottom: 5px;
		line-height: 1.7;
		letter-spacing: 2px;
	}
	.note{
		font-size: 12px;
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
	<div class="row">
		<div class="col-md-3">
			<select class="m-select" id="select" onchange="window.location='/admin/list-keyword?list_type='+this.value">
				<option value="list_hangup">こだわり</option>
				<option value="list_job">職種</option>
				<option value="list_job_parent">職種+こだわりの親</option>
				<option value="list_em">雇用形態</option>
				<option value="list_province">都道府県</option>
				<option value="list_city">市区町村</option>
				<option value="list_waysite">沿線・駅</option>
			</select>管理の選択
			<script>
				document.querySelector('#select').value = '{{ empty($listType) ? 'list_job' : $listType}}'
			</script>
		</div>
		<div class="col-md-9">
		
		<?php 
			if(true && ($status = $errors->first('status')) != '')
			{
		
				if($status)
				{

					echo '<span class="success">'.$errors->first('message').'</span>';
					echo '<span class="info">キーワードを検索: '.$errors->first('keywords').'</span>';
					echo '<span class="info">前の検索数: '.$errors->first('before').'</span>';
					echo '<span class="info">後に検索数: '.$errors->first('after').'</span>';
					echo '<span class="info">工程所要時間: '.$errors->first('time').'秒</span>';
				}
				else
				{
					echo '<span class="error">'.$errors->first('message').'</span>';
				}
			}
		?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 note">
			クローラ時間: 午後2時 => 午後6時, 午後8時 => 午前6時(明日)
			<br>
			On those time, updated keywords won't update for crawler job.
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead>

					<tr>
						<th>No</th>
						<th>
							<form action="" method="get" id="">
								<input type="hidden" name="list_type" value="{{$listType}}">
								<input type="hidden" name="fkeyword" value="{{$fkeyword or ''}}">
								<input value="{{$fname or ''}}" name="fname" type="text" class="" placeholder="名を検索">
								<button class="" type="submit">OK</button>
							</form>
						</th>
						<th style="width: 55%">
							<form action="" method="get" id="">
								<input type="hidden" name="list_type" value="{{$listType}}">
								<input type="hidden" name="fname" value="{{$fname or ''}}">
								<input value="{{$fkeyword or ''}}" name="fkeyword" type="text" class="" placeholder="キーワードを検索">
								<button class="" type="submit">OK</button>
							</form>
						</th>
						<th>更新日時</th>
					</tr>

				</thead>
				<tbody>
					@foreach($list as $i=>$x)
					<tr id="tr-{{$x[$ColumnIdName]}}">
						<td>{{ $i+1 }}</td>
						<td>{{ $x->name }}</td>
						<td>
							<form action="/admin/list-keyword/update" method="get" class="update-key-form" >
								<input type="hidden" name="list_type" value="{{$listType}}">
								<input type="hidden" name="searching" value="fkeyword={{$fkeyword or ''}}&fname={{$fname or ''}}">
								<input type="hidden" name="id" value="{{$x[$ColumnIdName]}}">
								<input placeholder="key1|key2|keyN" tabindex="{{$i+1}}" class="txtKeyword" type="text" name="keywords" value="{{ $x->keywords }}">
								<button type="submit"><i class="glyphicon glyphicon-pencil"></i> 修正</button>
							</form>
						</td>
						<td>{{ $x->updated_at }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<!--your page at here-->
			<div class="text-center">
				{!! $list->render() !!}
			</div>
		</div>
	</div>
	<script>
	$('form.update-key-form').submit(function () {
		$('button[type="submit"]').prop('disabled', true);
		$('input[name="keywords"]').prop('readonly', true);
		$('#select').prop('disabled', true);
	});
	</script>
@stop