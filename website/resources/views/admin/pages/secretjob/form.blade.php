@extends('admin.layouts.default')
@section('page_head')
    @include('admin.shared.item.froala_editor_css')
    @include('admin.shared.item.froala_editor_js')
    {!!HTML::style('public/admin/css/secret.css')!!}
    {!!HTML::style('public/admin/js/chosen_v1.6.1/chosen.css')!!}
    {!!HTML::script('public/admin/js/chosen_v1.6.1/chosen.jquery.min.js')!!}
    {!!HTML::style('public/admin/js/fileinput/css/fileinput.css')!!}
    {!!HTML::style('public/admin/js/bootstrap-datepicker/css/bootstrap-datepicker.css')!!}
    {!!HTML::script('public/admin/js/fileinput/fileinput.js')!!}
    {!!HTML::script('public/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js')!!}
    {!!HTML::script('public/admin/js/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js')!!}
    {!!HTML::script('public/admin/js/fileinput/locales/ja.js')!!}
    {!!HTML::script('public/js/init-googlemap.js')!!}
    {!!HTML::script('https://maps.google.com/maps/api/js?language=ja&region=JP&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs')!!}
@append
@section('content')


<script>


function selectMain(checkbox){
    name = checkbox.getAttribute("name");
    gname = name + "-child";
    group = document.getElementsByClassName(gname);

    if(checkbox.checked){
        for(i = 0; i < group.length; i++){
            if(!group[i].checked){
                group[i].checked = true;
            }
        }
    }

    if(!checkbox.checked){
        for(i = 0; i < group.length; i++){
            if(group[i].checked){
                group[i].checked = false;
            }
        }
    }
}

function selectAll(checkbox){
    name = checkbox.getAttribute("name");
    gname = name + "-child";
    group = document.getElementsByClassName(gname);

    if(checkbox.checked){
        for(i = 0; i < group.length; i++){
            if(!group[i].checked){
                group[i].checked = true;
            }
        }
        checkTopParent(checkbox, true);
    }

    if(!checkbox.checked){
        for(i = 0; i < group.length; i++){
            if(group[i].checked){
                group[i].checked = false;
            }
        }
        checkTopParent(checkbox, false);
    }

}

function checkSelection(checkbox){
    if(!checkbox.checked){
        parent_name = checkbox.parentNode.id + "-checkbox";
        parent_checkbox = document.getElementById(parent_name);

        if(parent_checkbox.checked){
            parent_checkbox.checked = false;
            checkTopParent(checkbox, false);
        }
    }

    else{
        parent = checkbox.parentNode;
        parent_name = parent.id + "-checkbox";
        parent_checkbox = document.getElementById(parent_name);
    
        checkboxes = parent.getElementsByClassName(parent.id + "-child");
        all_checked = true;

        for(i = 0; i < checkboxes.length; i++){
            if(!checkboxes[i].checked){
                all_checked = false;
                checkTopParent(checkbox, false);
                break;
            }
        }

        if(all_checked){
            parent_checkbox.checked = true;
            checkTopParent(checkbox, true);
        }
    }
}

function checkTopParent(checkbox, state){
    temp = $(checkbox).attr('class').split(' ').pop();
    header = document.getElementById(temp.split('-')[0] + "-checkbox");
    checkboxes = document.getElementsByClassName(temp.split('-')[0] + "-child");

    if(state){
        all_checked = true;
        for(i = 0; i < checkboxes.length; i++){
            if(!checkboxes[i].checked){
                all_checked = false;
                break;
            }
        }

        if(all_checked){
            header.checked = true;
        }
    }

    else{
        header.checked = false;
    }
}

function checkAll(){
    //Check first row.
    contract_children = document.getElementsByClassName('雇用形態-child');

    contract_checked = true;
    for(i = 0; i < contract_children.length; i++){
        if(!contract_children[i].checked){
            contract_checked = false;
            break;
        }
    }

    if(contract_checked){
        contract_cb = document.getElementById('雇用形態-checkbox');
        contract_cb.checked = true;
    }

    //Check second row, left column.
    job_children = document.getElementsByClassName('sub-職種-child');

    job_checked = true;
    for(i = 0; i < job_children.length; i++){
        if(!job_children[i].checked){
            job_checked = false;
            break;
        }
    }

    if(job_checked){
        job_cb = document.getElementById('職種-checkbox');
        job_cb.checked = true;

        job_sub_cb = document.getElementsByClassName("sub-職種-header");

        for(i = 0; i < job_sub_cb.length; i++){
            job_sub_cb[i].checked = true;
        }
    }

    else{
        checkCategoryChildren();
    }

    //Check second row, right column.
    commitment_children = document.getElementsByClassName('sub-こだわり-child');

    commitment_checked = true;
    for(i = 0; i < commitment_children.length; i++){
        if(!commitment_children[i].checked){
            commitment_checked = false;
            break;
        }
    }

    if(commitment_checked){
        commitment_cb = document.getElementById('こだわり-checkbox');
        commitment_cb.checked = true;

        commitment_sub_cb = document.getElementsByClassName("sub-こだわり-header");

        for(i = 0; i < commitment_sub_cb.length; i++){
            commitment_sub_cb[i].checked = true;
        }
    }

    else{
        checkCommitmentChildren();
    }
}

function checkCategoryChildren(){
    //美容
    beauty_cb = document.getElementById("美容-checkbox");
    beauty_cb.checked = true;
    b_child = document.getElementsByClassName("美容-child");

    for(b = 0; b < b_child.length; b++){
        if(!b_child[b].checked){
            beauty_cb.checked = false;
            break;
        }
    }

    //リラクゼーション
    relaxation_cb = document.getElementById("リラクゼーション-checkbox");
    relaxation_cb.checked = true;
    r_child = document.getElementsByClassName("リラクゼーション-child");

    for(r = 0; r < r_child.length; r++){
        if(!r_child[r].checked){
            relaxation_cb.checked = false;
            break;
        }
    }

    //治療
    treatment_cb = document.getElementById("治療-checkbox");
    treatment_cb.checked = true;
    t_child = document.getElementsByClassName("治療-child");

    for(t = 0; t < t_child.length; t++){
        if(!t_child[t].checked){
           treatment_cb.checked = false;
            break;
        }
    }

}

function checkCommitmentChildren(){
    //応募条件
    application_cb = document.getElementById("応募条件-checkbox");
    application_cb.checked = true;
    a_child = document.getElementsByClassName("応募条件-child");

    for(a = 0; b < a_child.length; a++){
        if(!a_child[a].checked){
            application_cb.checked = false;
            break;
        }
    }

    //待遇
    treatment_cb = document.getElementById("待遇-checkbox");
    treatment_cb.checked = true;
    t_child = document.getElementsByClassName("待遇-child");

    for(t = 0; t < t_child.length; t++){
        if(!t_child[t].checked){
            treatment_cb.checked = false;
            break;
        }
    }

    //特徴
    character_cb = document.getElementById("特徴-checkbox");
    character_cb.checked = true;
    c_child = document.getElementsByClassName("特徴-child");

    for(c = 0; c < c_child.length; c++){
        if(!c_child[b].checked){
            character_cb.checked = false;
            break;
        }
    }
}

</script>

    <div class="row">
        <div class="col-md-12">
            <!-- if there are creation errors, they will show here -->
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if(Session::has('flash_error'))
                @if (Session::get('flash_error') != "8文字以上の半角英数字を入力してください")
                    <p class="alert alert-danger">{{ Session::get('flash_error') }}</p>
                @endif
            @endif

            {!! Form::open(['url' => $link_action,'method'=>'POST', 'files'=>true, 'id' => 'secret_job_form']) !!}

            <div class="form-group">
                {!! Form::label('job_name', 'ジョブ名')  !!}<span style="color: red;">※必須</span>
                {!! Form::text('job_name', (!empty($secret['job_name'])) ? $secret['job_name'] : Input::old('job_name'), array('class' => 'form-control' , 'required')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('company_id', '企業情報') !!}
                {!! Form::select('company_id', $list_company,(!empty($secret['company_id'])) ? $secret['company_id'] : Input::old('company_id'), array('class' => 'form-control chosen-select')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('store_name', '店名') !!}
                {!! Form::text('store_name', (!empty($secret['store_name'])) ? $secret['store_name'] : Input::old('store_name'), array('class' => 'form-control' )) !!}
            </div>

            <div class="panel panel-default"> 
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                                {!! Form::label('meta_desc', 'ディスクリプション')  !!}
                                {!! Form::textarea('meta_desc', (!empty($secret['meta_desc'])) ? $secret['meta_desc'] : Input::old('meta_desc'), array('class' => 'form-control', 'maxlength' => '160', 'rows' => '4')) !!}
                            </div>

                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('meta_keyword', 'キーワード')  !!}
                                {!! Form::textarea('meta_keyword', (!empty($secret['meta_keyword'])) ? $secret['meta_keyword'] : Input::old('meta_keyword'), array('class' => 'form-control', 'maxlength' => '255', 'rows' => '4')) !!}
                            </div>
                        </div>
                    </div>
                </div> 
            </div>

            <div class="form-group">
                {!! Form::label('job_benefit', '入社特典') !!}
                    {!! Form::textarea('job_benefit', (!empty($secret['job_benefit'])) ? $secret['job_benefit'] : Input::old('job_benefit'), array('class' => 'form-control',
                    'rows' => 5,
                    'placeholder' => '入社お祝い金1万円'
                    )) !!}
                </div>

            <div class="row">
                

                <div class="col-sm-6">
                    <div class="form-group" id="image">
                        <?php
                            $required = (empty($secret['job_image'])) ?  array('required') : "";
                        ?>
                        {!! Form::label('file','PCの画像',array('id'=>'','class'=>'')) !!}<span style="color: red;">※必須</span>
                        {!! Form::file('file',$required) !!}
                        <input type="hidden" value="" id="delete_image" name="delete_image">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" id="thumb">
                        {!! Form::label('image_sp','SPの画像',array('id'=>'','class'=>'')) !!}<span style="color: red;">※必須</span>
                        {!! Form::file('image_sp') !!}
                        <input type="hidden" value="" id="delete_thumb" name="delete_thumb">
                    </div>
                </div>
            </div>

            
            <div class="form-group">
                <input type="checkbox" name="雇用形態" id="雇用形態-checkbox" class="雇用形態-checkbox group-checkbox" onchange="selectMain(this)">
                {!! Form::label('list_em','雇用形態',array('id'=>'','class'=>'')) !!}
                <div class="list_check" id="雇用形態">
                    <?php $index = 0;
                    $employment_list = _helper::getEmployment();
                     $list_em = (isset($secret['list_em'])) ? $secret['list_em'] : "";
                    $trim_secret = substr($list_em, 1, -1);
                    $requestEm = explode(",",$trim_secret);
                    ?>
                    @foreach($employment_list as $record)
                        <?php
                        $key = $record['id'];
                        $em = $record['name'];
                        $index++;
                        $emChecked = (in_array($key,$requestEm)) ? 'checked' : '';
                        ?>
                        <label for="check{{$index}}" class="search-checkbox__label">
                            <input {{$emChecked}} type="checkbox" name="em[]" value="{{$key}}" id="check{{$index}}" class="search-checkbox 雇用形態-child" onchange="checkSelection(this)">
                        {{$em}}</label>
                    @endforeach
                </div>
            </div>
            
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
                    <input type="checkbox" name="職種" id="職種-checkbox" class="職種-checkbox group-checkbox" onchange="selectMain(this)">
					{!! Form::label('list_catagory', '職種')  !!}
					<?php
						$job_category = _helper::getJobCategory();
					   $current_list_job = (isset($secret['list_job'])) ? $secret['list_job'] : "";
					?>
					@include('admin.pages.secretjob.list_job_checkbox', array('modal_title' => '職種',
																	'modal_data' => $job_category,
																	'modal_prefix' => 'job',
																	'input_name' => 'cat[]',
																	'checked_items' => $current_list_job
																	))

					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="checkbox" name="こだわり" id="こだわり-checkbox" class="こだわり-checkbox group-checkbox" onchange="selectMain(this)">{!! Form::label('list_hangup','こだわり',array('id'=>'','class'=>'')) !!}
						<?php
						$hangup_keywords = _helper::getHangUp();
						$current_list_hangup = (isset($secret['list_hangup'])) ? $secret['list_hangup'] : "";
						?>
						@include('admin.pages.secretjob.list_job_checkbox', array('modal_title' => 'こだわり',
																					'modal_data' => $hangup_keywords,
																					'modal_prefix' => 'hangup',
																					'input_name' => 'hang[]',
																					'checked_items' =>                                                                                              $current_list_hangup
																					))
					</div>
				</div>
			</div>
      
            
			<hr>


            <div class="from-group">
                {!! Form::label('images_sp','PCの画像',array('id'=>'','class'=>'')) !!}
                (画像<2メガバイト)
                <div class="list_images">
                    <?php
                    if(!empty($list_images)){
                        foreach ($list_images as $src){
                            if($src['type']==0){
                                echo "<div class='abcd' id='image_".$src['id']."'><img width='100' src='".$src['image']."' class='' />
                        <img id='img' data-id='".$src['id']."' class='delete_image' src='/public/admin/img/x.png' alt='delete' />
                    </div>";
                            }

                        }
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>

                <div class="group_image">
                    <div id="filediv"><input name="images[]" type="file" id="images"/></div>
                    <input type="button" id="add_more" class="btn btn-success" value="その他の画像を追加"/>

                    <div class="clearfix"></div>
                </div>
            </div>

            <hr>
            <div class="from-group">
                {!! Form::label('images_sp','SPの画像',array('id'=>'','class'=>'')) !!}
                (画像<2メガバイト)
                <div class="list_images">
                    <?php
                    if(!empty($list_images)){
                        foreach ($list_images as $src){
                            if($src['type']==1){
                                echo "<div class='abcd_sp' id='image_".$src['id']."'><img width='100' src='".$src['image']."' class='' />
                        <img id='img' data-id='".$src['id']."' class='delete_image' src='/public/admin/img/x.png' alt='delete' />
                    </div>";
                            }

                        }
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>

                <div class="group_image">
                    <div id="filediv"><input name="images_sp[]" type="file" id="images"/></div>
                    <input type="button" id="add_more_sp" class="btn btn-success" value="その他の画像を追加"/>

                    <div class="clearfix"></div>
                </div>
            </div>

            <hr>
            <div class="column-box__secret">
            <div>マニュアル: <a target="_blank" href="/public/admin/img/manual/article-manual.jpg">私をクリック</a></div>
            <div class="form-group">
                {!! Form::label('content', 'コンテンツ') !!}
                {!! Form::textarea('content', (!empty($secret['content'])) ? $secret['content'] : Input::old('content'), array('class' => 'form-control froalaEditor')) !!}
            </div>
            </div>
			<hr>
          	
          	<div class="panel panel-default"> 
          		<div class="panel-heading"> 
          			<span class="c-tag--secret--notice u-fs--xs">注目ポイント！</span>
          		</div> 
          		<div class="panel-body">
         			    <div class="row">
         			    	<div class="col-md-4">
         			    		<div class="form-group" id="image_notice">
									{!! Form::label('job_notice_image','PCの画像') !!}
									{!! Form::file('job_notice_image') !!}
                                    <input type="hidden" value="" id="delete_image_notice" name="delete_image_notice">
								</div>


                                <div class="form-group" id="thumb_notice">
                                    {!! Form::label('job_notice_image_sp','SPの画像',array('id'=>'','class'=>'')) !!}
                                    {!! Form::file('job_notice_image_sp') !!}
                                    <input type="hidden" value="" id="delete_thumb_notice" name="delete_thumb_notice">
                                </div>
         			    	</div>
         			    	<div class="col-md-8">
         			    		{!! Form::label('job_notice_image','注目ポイントのコンテンツ') !!}
         			    		<div class="form-group">
									{!! Form::text('job_notice_header', (!empty($secret['job_notice_header'])) ? $secret['job_notice_header'] : Input::old('job_notice_header'), array('class' => 'form-control')) !!}
								</div>

								<div class="form-group">
									{!! Form::textarea('job_notice_content', (!empty($secret['job_notice_content'])) ? $secret['job_notice_content'] : Input::old('job_notice_content'), array('class' => 'form-control',
									'rows' => 7
									)) !!}
								</div>
         			    	</div>
         			    </div>
          		</div> 
          	</div>
			<hr>

            <div class="form-group">
                {!! Form::label('job_genre', 'ジャンル')  !!}
                {!! Form::text('job_genre', (!empty($secret['job_genre'])) ? $secret['job_genre'] : Input::old('job_genre'), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_salary', '給与') !!}<span style="color: red;">※必須</span>
                {!! Form::textarea('job_salary', (!empty($secret['job_salary'])) ? $secret['job_salary'] : Input::old('job_salary'), array('class' => 'form-control', 'required',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_qualification', '応募資格') !!}
                {!! Form::textarea('job_qualification', (!empty($secret['job_qualification'])) ? $secret['job_qualification'] : Input::old('job_qualification'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>
			

            <div class="form-group">
                {!! Form::label('job_description', '仕事内容') !!}
                {!! Form::textarea('job_description', (!empty($secret['job_description'])) ? $secret['job_description'] : Input::old('job_description'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('treatment_welfare', '待遇・福利厚生') !!}
                {!! Form::textarea('treatment_welfare', (!empty($secret['treatment_welfare'])) ? $secret['treatment_welfare'] : Input::old('treatment_welfare'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_holiday', '休日休暇') !!}
                {!! Form::textarea('job_holiday', (!empty($secret['job_holiday'])) ? $secret['job_holiday'] : Input::old('job_holiday'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_personnel', '求める人材について') !!}
                {!! Form::textarea('job_personnel', (!empty($secret['job_personnel'])) ? $secret['job_personnel'] : Input::old('job_personnel'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_appSelection', '応募・選考について') !!}
                {!! Form::textarea('job_appSelection', (!empty($secret['job_appSelection'])) ? $secret['job_appSelection'] : Input::old('job_appSelection'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>    

            <div class="form-group">
                {!! Form::label('access', 'アクセス') !!}
                {!! Form::textarea('access', (!empty($secret['access'])) ? $secret['access'] : Input::old('access'), array('class' => 'form-control',
                'rows' => 5
                )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_location', '勤務地') !!}
                {!! Form::text('job_location', (!empty($secret['job_location'])) ? $secret['job_location'] : Input::old('job_location'), array('class' => 'form-control' )) !!}
            </div>

            <div class="form-group">
                <label for="map_location">地図位置 <sub>Google map address</sub></label>
                <div class="row form-group">
                    <div class="col-sm-9">
                        {!! Form::text('map_location',(!empty($secret['map_location'])) ? $secret['map_location'] : Input::old('map_location'), array('class' => 'form-control input-map-location ' , 'id' => 'map_location')) !!}
                    </div>
                    <div class="col-sm-3">
                        <input id="find_location" type="button" style="" class="btn btn-primary btn-map-location" value="見つけます"/>
                        <input id="remove_location" type="button" style="" class="btn btn-primary btn-map-location" value="場所を削除"/>
                    </div>
                </div>


                <div id="map" style="height: 500px;"></div>
                {!! Form::hidden('lat', (!empty($secret['lat'])) ? $secret['lat'] : Input::old('lat'),array('id'=>'lat')) !!}
                {!! Form::hidden('lng', (!empty($secret['lng'])) ? $secret['lng'] : Input::old('lng'),
                array('id'=>'lng')) !!}
                {!! Form::hidden('id',(!empty($secret['id'])) ? $secret['id'] : Input::old('id') ) !!}
                {!! Form::hidden('preview_kind', 'PC', array('id'=>'preview_kind'))!!}
                {!! Form::hidden('date_create', (!empty($secret['created_at'])) ? $secret['created_at'] : "" ,array('id'=>'date_create')) !!}
                {!! Form::hidden('list_job_parent', (!empty($secret['list_job_parent'])) ? $secret['list_job_parent'] : "" ,array('id'=>'list_job_parent')) !!}
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        {!! Form::label('shopStyle', '店舗形態')  !!}
                        {!! Form::text('shopStyle', (!empty($secret['shopStyle'])) ? $secret['shopStyle'] : Input::old('shopStyle'), 
                        array('class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('business_hrs', '営業時間')  !!}
                        {!! Form::textarea('business_hrs', (!empty($secret['business_hrs'])) ? $secret['business_hrs'] : Input::old('business_hrs'), 
                        array('class' => 'form-control', 'rows'=>4)) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('reg_holiday', '定休日')  !!}
                        {!! Form::textarea('reg_holiday', (!empty($secret['reg_holiday'])) ? $secret['reg_holiday'] : Input::old('reg_holiday'), 
                        array('class' => 'form-control', 'rows'=>4)) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('customer_base', '客層')  !!}
                        {!! Form::textarea('customer_base', (!empty($secret['customer_base'])) ? $secret['customer_base'] : Input::old('customer_base'), 
                        array('class' => 'form-control', 'rows'=>4 , 'placeholder' => '20代 / 30代 / 40代 / 50代 / 60代以上男性が多い / 女性が多い')) !!}
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4 style="style='font-weight: bold; color: black;">よくあるご質問</h4>

                            <div class="form-group">
                                {!! Form::label('job_atmosphere', '職場の雰囲気について')  !!}
                                {!! Form::textarea('job_atmosphere', (!empty($secret['job_atmosphere'])) ? $secret['job_atmosphere'] : Input::old('job_atmosphere'), 
                                array('class' => 'form-control', 'rows'=>4)) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('job_education', 'スキルアップや教育制展度について')  !!}
                                {!! Form::textarea('job_education', (!empty($secret['job_education'])) ? $secret['job_education'] : Input::old('job_education'), 
                                array('class' => 'form-control', 'rows'=>4)) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('job_challenges', '仕事のやりがいについて')  !!}
                                {!! Form::textarea('job_challenges', (!empty($secret['job_challenges'])) ? $secret['job_challenges'] : Input::old('job_challenges'), 
                                array('class' => 'form-control', 'rows'=>4)) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('male_female_ratio', 'スタッフ数や男女比')  !!}
                        {!! Form::textarea('male_female_ratio', (!empty($secret['male_female_ratio'])) ? $secret['male_female_ratio'] : Input::old('male_female_ratio'), 
                        array('class' => 'form-control', 'rows'=>4)) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('job_url', 'URL')  !!}
                        {!! Form::text('job_url', (!empty($secret['job_url'])) ? $secret['job_url'] : Input::old('job_url'), array('class' => 'form-control' )) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-2">
                    {!! Form::label('status', '状態') !!}
                    {!! Form::select('status', array(0=> '未発表', 1 => '公開'),(!empty($secret['status'])) ? $secret['status'] : 0, array('class' => 'form-control')) !!}
                </div>


                
            </div>

            <div class="row">

                <div class="col-md-2">
                    {!! Form::label('start_at', '掲載期間')  !!}
                    <?php
                    $date_start = !empty($secret['start_at']) ? $secret['start_at'] : Input::old('start_at');
                    if($date_start != '0000-00-00 00:00:00') $date_start = date_create($date_start);

                    if($date_start && $date_start != '0000-00-00 00:00:00')
                    {
                        $date_start = date_format($date_start,"Y/m/d");
                    }else{
                        $date_start = '';
                    }

                    ?>
                    {!! Form::text('start_at', $date_start , array('class' => 'form-control datepicker')) !!}
                </div>

                <div class="form-group col-md-2">
                    {!! Form::label('end_at', '&nbsp;')  !!}
                    <?php
                    $date_end = !empty($secret['end_at']) ? $secret['end_at'] : Input::old('end_at');
                    
                    if($date_end != '0000-00-00 00:00:00') $date_end = date_create($date_end);
                    
                    if($date_end && $date_end != '0000-00-00 00:00:00')
                    {
                        $date_end = date_format($date_end,"Y/m/d");
                    } else{
                        $date_end = '';
                    }

                    ?>
                    {!! Form::text('end_at', $date_end , array('class' => 'form-control datepicker')) !!}
                </div>
            </div>

            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::label('created_at', '作成日')  !!}
                        <?php 
                            $date_created = !empty($secret['created_at']) ? $secret['created_at'] : Input::old('created_at');
                            $date_created = date_create($date_created);
                            if($date_created)
                            {
                                $date_created = date_format($date_created,"Y/m/d");
                            }
                            
                        ?>
                        {!! Form::text('created_at', $date_created , array('class' => 'form-control datepicker')) !!}
                    </div>
                    <div class="col-md-10">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::submit('申し出る', ['class' => 'btn btn-primary', 'id' => 'btnSubmit']) !!}
                    </div>
                    <div class="col-md-10">
                        {!! Form::submit('PC-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewPC']) !!}
                        {!! Form::submit('SP-プレビュ', ['class' => 'btn btn-default', 'id' => 'btnPreviewSP']) !!}
                    </div>
                </div>
            </div>

            {!! Form::close() !!}


        </div>
    </div>

    <script>
        //datepicker date created
        $('.datepicker').datepicker({
            format: 'yyyy/mm/dd',
            language: 'ja',
            //endDate: '+0d',
        });

        $("#btnPreviewPC").click(function(){
            //set list job parent in input hidden
            setListJobParent();
            $("#secret_job_form").removeAttr("onsubmit");
            $("#secret_job_form").attr('target', '_blank');
            $("#secret_job_form").attr('action', '/admin/secret-job-manager/preview');
            $("#preview_kind").val("PC");
        });

        $("#btnPreviewSP").click(function(){
            //set list job parent in input hidden
            setListJobParent();
            $("#secret_job_form").attr('onsubmit', 'target_popup(this)');
            $("#secret_job_form").attr('action', '/admin/secret-job-manager/preview');
            $("#preview_kind").val("SP");
        });

        function target_popup(form) {
            window.open('', 'formpopup', 'width=414,height=650,resizeable,scrollbars');
            form.target = 'formpopup';
        }

        $("#btnSubmit").click(function(){
            //set list job parent in input hidden
            setListJobParent();
            $("#secret_job_form").removeAttr("onsubmit");
            $("#secret_job_form").attr('target', '');
            $("#secret_job_form").attr('action', '/'+'<?php echo $link_action; ?>');
        });


        $("#image_sp").fileinput({
            initialPreview: [<?php if(!empty($secret['job_image_sp'])){ ?> '<img style="max-height:150px" src="<?php echo $secret['job_image_sp'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "auto", height: "150px"},
            },
            showUpload: false,
            showRemove: true,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            language: 'ja',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });

          $("#file").fileinput({
            initialPreview: [<?php if(!empty($secret['job_image'])){ ?> '<img width="150" src="<?php echo $secret['job_image'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "150px", height: "auto"},
            },
            showUpload: false,
            showRemove: false,
            language: 'ja',
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });


        $("#job_notice_image_sp").fileinput({
            initialPreview: [<?php if(!empty($secret['job_notice_image_sp'])){ ?> '<img style="max-height:150px" src="<?php echo $secret['job_notice_image_sp'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "auto", height: "150px"},
            },
            showUpload: false,
            showRemove: true,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            language: 'ja',
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });



        $("#job_notice_image").fileinput({
            initialPreview: [<?php if(!empty($secret['job_notice_image'])){ ?> '<img width="150"  src="<?php echo $secret['job_notice_image'] ?>" class="" >' <?php } ?>],
            previewSettings: {
                image: {width: "200px", height: "auto"},
            },
            showUpload: false,
            showRemove: false,
            language: 'ja',
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"],
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        });


        $(document).ready(function () {
            $(".chosen-select").chosen({allow_single_deselect:true,no_results_text: "Oops, nothing found!"});


            $('.froalaEditor').froalaEditor({
                language: 'ja',
                height: 400,
				// Define new paragraph styles.
				paragraphStyles: {
					"_secret__text__block u-fs--lh--contents": "Secret block",
					"_secret__text__ttl u-fwb u-mt25": 'Secret H3',
					"_secret__text": 'Secret Text P',
					"_contents__img__block u-mt25": 'Image wrapper',
					"_contents__img_text u-mt10": 'Image text',
				},
				paragraphFormat: {
					N: 'Normal',
					H2: 'Heading 2',
					H3: 'Heading 3',
				},
                toolbarButtons: [
                             'fullscreen', 
                             'bold', 
                             'italic', 
                             'underline', 
                             'strikeThrough', 
                             'subscript', 
                             'superscript', 
                             'fontFamily', 
                             'fontSize', 
                             '|', 
                             'color', 
                             'emoticons', 
                             'inlineStyle', 
                             'paragraphStyle', 
                             '|', 
                             'paragraphFormat', 
                             'align', 
                             'formatOL', 
                             'formatUL', 
                             'outdent', 
                             'indent', 
                             'quote', 
                             'insertHR', 
                             '|', 
                             'insertLink',
                             'insertImage', 
                             'insertVideo', 
                             'insertFile', 
                             'insertTable', 
                             'undo', 
                             'redo', 
                             'clearFormatting', 
                             'selectAll', 
                             'html',
                
                             '-', //breakline 
                             'runda_h2_wrapper',
                             '|',
                             'runda_h2u40_wrapper',
                             '|',
                             'runda_h2border_wrapper',
                             '|',
                             'runda_h3_wrapper',
                             '|',
                             'runda_h3_30_wrapper',
                             '|',
                             'runda_h3_60_wrapper',
                             '|',
                             'runda_p_wrapper',
                             '|',
                             'runda_gray_area',
                             'runda_gray_area_qa',
                             '|',
                             'runda_qa_wrapper',
                             '|',
                             'runda_address_wrapper',
                             '|',
                             'runda_image_wrapper',
                             '|', 
                             'runda_image_wrapper_2image'
                            ]
				//enter: $.FroalaEditor.ENTER_BR
            });

            // set google map
            // input lat, lng , inforwindow, zoom
            <?php
            $lat = (!empty($secret['lat'])) ? $secret['lat'] : 0;
            $lng = (!empty($secret['lng'])) ? $secret['lng'] : 0;
            ?>
            <?php if($lat && $lng): ?>
                var contentString = "";
                var companyName = $("#company_id option:selected").text();
                contentString += "<h4 style='font-weight: bold; color: black;'>"+companyName+"</h4>";
                <?php if(!empty($secret['job_location'])): ?>
                    contentString += "<p><?php echo $secret['job_location']; ?></p>";
                <?php endif; ?>
                initMap(<?php echo $lat;?> ,<?php echo $lng;?>, contentString, 14, 'map');
            <?php else: ?>
                initMap(<?php echo $lat;?> ,<?php echo $lng;?>, "", 14, 'map');
            <?php endif; ?>

            $("#find_location").click(function () {
                var address = $("#map_location").val();
                var p = null;
                $.ajaxSetup({
                    async: false
                });
                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false', null, function (data) {
                    var p = data.results[0].geometry.location;
                    $('#lat').attr('value', p.lat);
                    $('#lng').attr('value', p.lng);
                    var contentString = "";
                    var __companyName = $("#company_id option:selected").text();
                    contentString += "<h4 style='font-weight: bold; color: black;'>"+__companyName+"</h4>";
                    var job_location = $('#job_location').val();
                    if(job_location != "")
                    {
                        contentString +="<p>"+job_location+"</p>";
                    }
                    initMap(p.lat, p.lng, contentString, 14, 'map');
                });
            });

            $("#remove_location").click(function () {
                var address = "";
                var p = null;
                $.ajaxSetup({
                    async: false
                });

                $("#map_location").val("");
                $('#lat').attr('value', 0);
                $('#lng').attr('value', 0);
                initMap(0, 0, address, 14, 'map');
            });

        });



        var abc = 0; //Declaring and defining global increement variable
        $(document).ready(function() {

            //delete image thumb
            $('#thumb .fileinput-remove').click(function() {
                $('#delete_thumb').val("image_thumb_delete");
            });

            $('#image .fileinput-remove').click(function() {
                $('#delete_image').val("image_delete");
            });

            //delete image thumb
            $('#image_notice .fileinput-remove').click(function() {
                $('#delete_image_notice').val("image_delete");
            });

            $('#thumb_notice .fileinput-remove').click(function() {
                $('#delete_thumb_notice').val("image_thumb_delete");
            });


//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
            $('#add_more').click(function() {
                $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                        $("<input/>", {name: 'images[]', type: 'file', id: 'images'}),
                        $("<br/>")
                ));
            });

            $('.delete_image').click(function() {
                if (confirm('あなたはこのイメージを削除してもよろしいですか？')) {
                    var  id_image = $(this).attr('data-id');
                    var  url = '{!!  route('admin.delete_image') !!}';
                    var  data = {id: id_image};
                    $.ajax({
                        url: url,
                        data: data,
                        type: 'POST',
                        success: function (resp) {
                            $("#image_"+id_image).hide(500);
                        }
                    });
                }
            });

            //following function will executes on change event of file input to select different file
            $('body').on('change', '#images', function(){
                if (this.files && this.files[0]) {
                    abc += 1; //increementing global variable by 1

                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");

                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);

                    $(this).hide();
                    $("#abcd"+ abc).append($("<img/>", {id: 'img', src: "{{ asset('public/admin/img/x.png') }}", alt: 'delete'}).click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });
//To preview image
            function imageIsLoaded(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };



            //To add new input file field dynamically, on click of "Add More Files" button below function will be executed
            $('#add_more_sp').click(function() {
                $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                        $("<input/>", {name: 'images_sp[]', type: 'file', id: 'images'}),
                        $("<br/>")
                ));
            });
            //following function will executes on change event of file input to select different file
            $('body').on('change', '#images_sp', function(){
                if (this.files && this.files[0]) {
                    abc += 1; //increementing global variable by 1

                    var z = abc - 1;
                    var x = $(this).parent().find('#previewimg' + z).remove();
                    $(this).before("<div id='abcd"+ abc +"' class='abcd_sp'><img id='previewimg" + abc + "' src=''/></div>");

                    var reader = new FileReader();
                    reader.onload = imageIsLoaded_sp;
                    reader.readAsDataURL(this.files[0]);

                    $(this).hide();
                    $("#abcd_sp"+ abc).append($("<img/>", {id: 'img', src: "{{ asset('public/admin/img/x.png') }}", alt: 'delete'}).click(function() {
                        $(this).parent().parent().remove();
                    }));
                }
            });
//To preview image
            function imageIsLoaded_sp(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };


        });
        

        //set list_job_parent
        function setListJobParent()
        {
            var arrPrentId = [];
            var listJobParent = "";
            //get all parent ID of checkbox cat
            $('input[name="cat[]"]:checked').each(function () {
                var parentId = $(this).attr("data-parent");
                console.log(parentId);
                arrPrentId.push(parentId);
            });

            //get all parent ID of checkbox hang
            $('input[name="hang[]"]:checked').each(function () {
                var parentId = $(this).attr("data-parent");
                console.log(parentId);
                arrPrentId.push(parentId);
                
            });
            
            //filter to return the first item of each distinct value
            var unique=arrPrentId.filter(function(itm,i,a){
                return i==a.indexOf(itm);
            });

            for(var i in unique)
            {
                listJobParent += "," + unique[i];
            }

            $('#list_job_parent').val(listJobParent);

            console.log(listJobParent);
        }

        checkAll();
    </script>

@append