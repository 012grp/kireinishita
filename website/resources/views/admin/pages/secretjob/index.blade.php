@extends('admin.layouts.default')
@section('content')
<?php 
    $secretUrl = _helper::getUrlName('secretPage');
?>
<div class="btn_add">
    <a class="btn btn-primary" href="/admin/secret-job-manager/create" ><i class="glyphicon glyphicon-file"></i> 新規作成</a>
</div>

<div class="box_search row">
    {!! Form::open(['url' => '/admin/secret-job-manager','method'=>'get', 'files'=>true]) !!}
    <div class="form-group col-sm-7">
        {!! Form::text('keyword',Input::get('keyword'), array('class' => 'form-control','placeholder' => '検索キーワードを入力してください')) !!}
    </div>
    <div class="col-sm-2">
        {!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['type' => 'submit', 'class' => 'btn btn-default'] ) !!}
    </div>

    {!! Form::close() !!}

</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="6%">番号ID</th>
                    <th >タイトル</th>
                    <th class="text-center" width="9%">掲載期間</th>
                    <th class="text-center" width="8%">状態</th>
                    <th class="text-center" width="11%">作成日<small>年/月/日</small></th>
                    <th class="text-center" width="6%" title="プレビュー画面の共有">共有</th>
                    <th class="text-center"  width="15%">ツールバー</th>
                </tr>
            </thead>
            <tbody>
                @foreach($secretjob as $value)
                <tr>
                    <td><a target="_blank" href="/{{$secretUrl}}/{{ $value['id'] }}">{{ $value['id'] }}</a></td>
                    <td>{{ $value['job_name'] }}</td>
                    <td class="text-center">
                        <small title="開始" class="hfocus">
                        @if($value['start_at'] != '' && $value['start_at'] != '0000-00-00 00:00:00')
                        {{ date('Y/m/d', strtotime($value['start_at'])) }}
                        @endif
                        </small><br>
                        <small title="終わり" class="hfocus">
                        @if($value['end_at'] != '' && $value['end_at'] != '0000-00-00 00:00:00')
                                {{ date('Y/m/d', strtotime($value['end_at'])) }}
                        @endif
                        </small>
                    </td>
                    <td class="text-center">{{ $value['status']?'公開':'未発表' }}</td>
                    <td class="text-center"><small>{{ date("Y/m/d", strtotime($value['created_at']))}}</small></td>
                    <td class="text-center">
                        <a target="_blank" 
                        href="{{ _common::getShareLink('/'.$secretUrl.'/share/'.$value->id, $value->id)}}">1日</a><br><a target="_blank" 
                        href="{{ _common::getShareLink('/'.$secretUrl.'/share/'.$value->id, $value->id, 3)}}">3日</a>
                    </td>
                    <td class="text-center">
                       <a href="/admin/secret-job-manager/edit/{{ $value->id }}"><i class="glyphicon glyphicon-pencil"></i> 修正</a> &#160;&#160;
                        <a href="/admin/secret-job-manager/delete/{{ $value->id }}" onclick="if (confirm('Are you sure you, want to delete?')) { return true; } return false;"><i class="glyphicon glyphicon-trash"></i> 削除</a>
                    </td>
                </tr>
                @endforeach 
                
            </tbody>
        </table>

        <!--your page at here-->
        <div class="text-center">
        {!! $secretjob->render() !!}
        </div>
    </div>
</div>


@append