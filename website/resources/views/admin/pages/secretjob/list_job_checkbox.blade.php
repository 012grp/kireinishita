<div class="modal__inner u-fs--xs" id="{{$modal_title}}">
	<?php $GroupMT30 = ''; $GroupIndex = 1; ?>
	@foreach($modal_data as $GroupName=>$List)
		<h2 class="modal__sub__ttl {{$GroupMT30}}"><input type="checkbox" name="{{$GroupName}}" class="label-checkbox sub-{{$modal_title}}-header {{$modal_title}}-child" id="{{$GroupName}}-checkbox" onchange="selectAll(this)" style="margin-right: 5px">{{$GroupName}}</h2>
		<div class="modal__checkbox" id="{{$GroupName}}">
		@foreach($List as $record)
			<?php
			$Key = $record['id'];
			$Name = $record['name'];
			$checked='';
			$list_checked = substr($checked_items, 1, -1);
			$_check = explode(',',$list_checked );
			if(in_array($Key,$_check))
			{
				$checked='checked';
			}
			?>
			<label for="{{$modal_prefix}}-{{$GroupIndex}}" class="search-checkbox__label--modal u-mt10">
            <input {{$checked}} type="checkbox" name="{{$input_name}}" id="{{$modal_prefix}}-{{$GroupIndex}}" data-num="{{$GroupIndex}}" data-parent="{{$record['parent_id']}}" class="search-checkbox {{$GroupName}}-child {{$GroupName}} sub-{{$modal_title}}-child {{$modal_title}}-child" value="{{$Key}}" onchange="checkSelection(this)">
			{{$Name}}</label>
			<?php $GroupIndex++; ?>
		@endforeach 
		</div>
		<?php $GroupMT30 = 'u-mt10'; ?>
	@endforeach
</div>