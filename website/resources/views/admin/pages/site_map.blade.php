<?php 
    $title_page = "サイトマップ";
    $siteURL = url(); 
?>
@extends('admin.layouts.default')
@section('content')

<div class="btn_add">
    <a class="btn btn-primary" href="/admin/site-map/renew" ><i class="glyphicon glyphicon-file"></i>新規作成</a>
    <a href="/sitemap.xml" target="_blank" style="padding-left: 10px"><i class="glyphicon glyphicon-eye-open"></i>SiteMap.XML</a>
    <a href="/public/uploads/sitemap/sitemap.png" target="_blank" style="padding-left: 10px">マニュアル</a>
</div>

{!! Form::open(['url' => '/admin/site-map/create','method'=>'post', 'files'=>true]) !!}
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            @if(!empty($xml))
                <textarea class="form-control" rows="30" name="site_map" id="site_map">@foreach($xml as $x){{$x->loc}}<?php echo "\n"; ?><?php if($x->priority): echo $x->priority; echo "\n"; endif; ?>@endforeach</textarea>
            @endif
            @if(!empty($arrSiteMap))
                <textarea class="form-control" rows="30" name="site_map" id="site_map">@foreach($arrSiteMap as $site){{$siteURL . $site[1]}}<?php echo "\n"; ?>{{$site[0]}}<?php echo "\n"; ?>@endforeach</textarea>
            @endif
        </div>
    </div>
</div>

<div class="btn_add">
    <button class="btn btn-primary" type="submit">更新</button>
    <a class="btn btn-default" href="/admin/site-map" >キャンセル</a>
</div>
{!! Form::close() !!}

@stop