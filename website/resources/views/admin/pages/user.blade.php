<?php 
	$title_page = '会員の管理';
?>
@extends('admin.layouts.default') 
@section('content')
<style>
	.sprite-social {
		background: url('/public/admin/images/16_pixel_social_icons.png') no-repeat;
		width: 16px;
		height: 16px;
		display: inline-block;
	}
	
	.sprite-0:after {
		content: '@';
	}
	
	.sprite-0 {
		background: none;
		color: gray;
		font-size: 12px;
	}
	
	.sprite-1 {
		background-position: -55px -19px;
	}
	
	.sprite-2 {
		background-position: -1px -37px;
	}
	.sprite-3 {
		background-position: -163px -19px;
	}
	.register-total,
	.register-find {
		margin-bottom: 25px;
	}
	
	.register-total {
		border: 1px solid #D8D8D8;
	}
	
	.register-find > div {
		line-height: 35px;
	}
	
	.register-find select {
		height: 35px;
		border: 0;
		outline: 0;
	}
	
	.select-inv {
		border: 0;
		outline: 0;
	}
	
	.register-total > div > * {
		vertical-align: middle;
	}
	
	.register-total .amount {
		display: inline-block;
		margin-right: 15px;
	}
</style>
<form action="" method="get" id="findDetailForm">
	<div class="row register-find">

		<div class="col-md-2">Register date from</div>
		<div class="col-md-2">
			<select id="fromYear" name="fromYear">
				<option value="">Year</option>
			</select>
			<select id="fromMonth" name="fromMonth">
				<option value="">Month</option>
			</select>
		</div>
		<div class="col-md-1">to</div>
		<div class="col-md-2">
			<select id="toYear" name="toYear">
				<option value="">Year</option>
			</select>
			<select id="toMonth" name="toMonth">
				<option value="">Month</option>
			</select>
		</div>
		<div class="col-md-2">
			<button class="btn btn-primary" id="btnFind">Find</button>
			
		</div>
		<div class="col-md-3 text-right">
			<a class="btn btn-success" href="/admin/user">Reset</a>
		</div>
	</div>

	<div class="row register-total">
		<div class="col-md-2">Total: {{ $users->total() }}</div>
		<div class="col-md-1">with</div>
		<div class="col-md-9">
			<i class="sprite-social sprite-0"></i>
			<div class="amount">{{ $emailTotal }}</div>
			<i class="sprite-social sprite-1"></i>
			<div class="amount">{{ $facebookTotal }}</div>
			<i class="sprite-social sprite-2"></i>
			<div class="amount">{{ $twitterTotal }}</div>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead>

					<tr>
						<th>No</th>
						<th>
							<input id="findName" name="find_name" type="text" class="" placeholder="Find Name">
							<button class="" type="submit">OK</button>
						</th>
						<th>

							<input id="findEmail" name="find_email" type="text" class="" placeholder="Find Email">
							<button class="" type="submit">OK</button>

						</th>
						<th>

							<select name="find_type" id="findType" class="select-inv">
								<option value="">TYPE (all)</option>
								<option value="0">Email</option>
								<option value="1">Facebook</option>
								<option value="2">Twitter</option>
							</select>

						</th>
						<th>Registered date</th>
						<th></th>
					</tr>

				</thead>
				<tbody>
					@foreach($users as $i=>$x)
					<tr>
						<td>{{ $i+1 }}</td>
						<td>{{ $x->name }}</td>
						<td>{{ $x->email }}</td>
						<td><i class="sprite-social sprite-{{$x->style}}"></i></td>
						<td>{{ $x->created_at }}</td>
						<td>
							@if(!empty($x->email))
							<!--                    		<a href="javascript:void(0)" data-email="{{$x->email}}" data-id="{{ $x->id }}" data-resetPassword>Reset password</a>-->
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<!--your page at here-->
			<div class="text-center">
				{!! $users->render() !!}
			</div>
		</div>
	</div>
</form>
@stop 

@section('page_bottom')
<script type="text/javascript">
	function lib() {
    var gnoux = function () {
        //public  
        this.description = 'This is a library for javascript.';
        
        this.fnTest = function () {
            alert('Hello');
        }
        
		this.request = queryString();
		
		
        //private funtions
        function todo () {
            
        }
		
		
		//QueryString
		function queryString () {
		  var query_string = {};
		  var query = window.location.search.substring(1);
		  var vars = query.split("&");
		  for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
				// If first entry with this name
			if (typeof query_string[pair[0]] === "undefined") {
			  query_string[pair[0]] = decodeURIComponent(pair[1]);
				// If second entry with this name
			} else if (typeof query_string[pair[0]] === "string") {
			  var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
			  query_string[pair[0]] = arr;
				// If third or later entry with this name
			} else {
			  query_string[pair[0]].push(decodeURIComponent(pair[1]));
			}
		  } 
			return query_string;
		};
        
    };
    return new gnoux();
}
	$(function () {
		//[PROPERTIES]
		var l = new lib();
		var libRequest = l.request;

		var request = {
			fromYear: libRequest.fromYear || 0,
			fromMonth: libRequest.fromMonth || 0,
			toYear: libRequest.toYear || 0,
			toMonth: libRequest.toMonth || 0,


			find_email: libRequest.find_email || '',
			find_name: libRequest.find_name || '',
			find_type: libRequest.find_type || '',
		};
		var d = new Date();
		var thisYear = d.getFullYear();

		var makeSelect = function (id, selected, from, to) {
				var Options = '';
				for (var i = from; i <= to; i++) {
					var s = '';
					if (selected == i) {
						s = 'selected';
					}
					Options += '<option ' + s + ' value="' + i + '">' + i + '</option>';
				}
				$(id).append(Options);
			}
			//[/PROPERTIES]

		console.log(request);
		//[LOAD]
		makeSelect('#fromYear', request.fromYear, 2015, thisYear);
		makeSelect('#fromMonth', request.fromMonth, 1, 12);
		makeSelect('#toYear', request.toYear, 2015, thisYear);
		makeSelect('#toMonth', request.toMonth, 1, 12);

		$('#findEmail').val(request.find_email);
		$('#findName').val(request.find_name);
		$('#findType').val(request.find_type);

		//[/LOAD]


		//[PROCESS]
		$('[data-resetPassword]').click(function () {

			var email = $(this).data('email');
			if (confirm('Are you sure send the required reset password to ' + email + '?')) {
				var id = $(this).data('id');
				console.log('Send require to this email');
			}

		});

		$('#findType').change(function () {
				$('#findDetailForm').submit();
			})
			//[/PROCESS]

	});
</script>
@stop