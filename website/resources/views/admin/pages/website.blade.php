@extends('admin.layouts.default')

@section('content')
    
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Website</th>
                    <th>Amount of job</th>
                    <th>Amount of clicked</th>
                </tr>
            </thead>
            <tbody>
               @foreach($websites as $x)
                   <tr>
                    <td>
                    <a href="{{ $x['url'] }}" target="ext">
                    {{ $x['name'] }}
                    </a>
                    </td>
                    <td>{{ $x['job_number'] }}</td>
                    <td>{{ $x['clicked_total'] }}</td>
                </tr>
               @endforeach 
            </tbody>
        </table>

        <!--your page at here-->

    </div>
</div>


@stop