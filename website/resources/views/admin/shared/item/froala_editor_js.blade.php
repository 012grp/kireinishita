<!--  start  text editor js-->
{!!HTML::script('public/admin/js/froala-editor/js/codemirror.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/xml.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/froala_editor.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/code_beautifier.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/code_view.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/draggable.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/font_size.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/font_family.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/paragraph_format.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/paragraph_style.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/lists.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/image.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/link.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/video.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/image_manager.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/align.min.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/fullscreen.min.js')!!}

{!!HTML::script('public/admin/js/froala-editor/js/plugins/runda_content_wrapper.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/runda_gray_area.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/runda_image_wrapper.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/plugins/runda_image_wrapper_2image.js')!!}
{!!HTML::script('public/admin/js/froala-editor/js/languages/ja.js')!!}
<!--  end  text editor js-->