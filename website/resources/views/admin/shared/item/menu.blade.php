<h3>MENU</h3>

<hr>

<ul class="nav nav-stacked">
    <li><a href="/admin/job-manager">求人の管理<br><small>Jobs</small></a></li>
    <li><a href="/admin/user">会員の管理<br><small>Members</small></a></li>
    <li><a href="/admin/article-manager">コンテンツまとめ<br><small>Contents</small></a></li>
    <li><hr></li>
    <li><a href="/admin/secret-job-manager">オリジナル求人<br><small>Secret job</small></a></li>
    <li><a href="/admin/company-manager">会社情報<br><small>Company</small></a></li>
    <li><hr></li>
    <li><a href="/admin/alliance-manager">アライアンス求人の管理<br><small>Alliance job</small></a></li>
    <li><hr></li>
    <li><a href="/admin/list-keyword">キーワードの管理<br><small>Keywords</small></a></li>
    <li><hr></li>
    <li><a href="/admin/category-manager">職種の説明<br><small>Category</small></a></li>
    <li><hr></li>
    <li><a href="/admin/clear-cache">キャッシュの消去<br><small>Clear cache</small></a></li>
    <li><hr></li>
    <li><a href="/admin/site-map">サイトマップ<br><small>Site map</small></a></li>
</ul>


<hr>