<?php
$path = Request::path();

$job_name = _common::getJobName($job_name);

//会社
$job_company = strip_tags($company_name);

$job_shop = strip_tags($shop_name);

//給与
$job_salary = strip_tags($job_salary);

//勤務地
$job_location = _helper::getJobAddressSpecify($list_province, $list_city, $job_location);

$listCareer = _helper::getAllianceListHangup((object)array('shop_id' => $shop_id, 'id' => $id, 'job_holiday' => $job_holiday));

$data_secret = 2;

//職種
$job_category = _helper::getAllianceJobCategoryNameByListId($list_job);

//雇用形態
$job_career = !empty($listCareer) ? implode(' ', $listCareer) : '';

//こだわり
$job_type = _helper::getEmploymentNamesByListId($list_em);


$site_name = '';


if (empty($job_url)) {
    $secretUrl = _helper::getUrlName('alliancePage');
    $job_url = '/' . $secretUrl . '/' . $id;
} else {
    $job_url = _helper::make_job_url($id);
}


//TODO: check image
$job_image = $job_image;
if (empty($job_image)) {
    $job_image = _common::noImageUrlContentsBySiteId(0);
} else {
    //$rs = strpos($job_image, 'http');
    if ($job_image[0] != '/') {
        //$job_image = '//'.$job_image;
        $job_image = _common::proxy_image($job_image);
    }
}

//
////TODO: set name length to show
//if(mb_strlen($job_name) > 50)
//{
//	$job_name = mb_substr($job_name,0,50,'utf-8').'...';
//}
//
////TODO: set location length to show
//if(mb_strlen($job_location) > 30)
//{
//	$job_location = mb_substr($job_location,0,30,'utf-8').'...';
//}

$list_em = _helper::getAllianceEmploymentNamesByListId($list_em);
$is_new = _helper::checkNew($created_at, 7);
$url = str_replace('?fr=kirei','',$url);
?>

<div class="column-box--wide__list {{ $job_class or 'u-mt20'}}">
    <h2 class="u-fs--xlh -wide__list__ttl--multiTag u-fwb">
        <span class="_list__multiTag__text">{{ $job_name }}</span>
        <span class="_list__multiTag__block">
			@if($is_new)
                <span class="u-fs--xxs c-tag--new--flow">NEW</span>
            @endif
            @if(!empty($is_secret))
                <span class="u-fs--xxs c-tag--secret--flow">オススメ</span>
            @endif
		</span>
    </h2>
    <h3 class="-wide__list__subTtl u-fs--l u-mt20">{{ $job_shop.'('.$job_company.')' }}</h3>
    <div class="-wide__list__bottom u-mt20">
        <div style="background-image:url({{$job_image}})" class="-wide__list__img"></div>
        <ul class="-wide__list__details u-fs--xs--lh">
            <li class="-wide__list__detail--multiline"><span
                        class="_list__detail__title">職種</span><span>：{{$job_category or ''}}</span></li>
            <li class="-wide__list__detail--multiline"><span
                        class="_list__detail__title">給与</span><span>：{{$job_salary}}</span></li>
            <li class="-wide__list__detail"><span class="_list__detail__title">勤務地</span><span>：{{$job_location}}</span>
            </li>
            @if(!empty($list_em))
                <li class="-wide__list__detail"><span class="_list__detail__title">雇用形態</span><span>：{{$list_em}}</span>
                </li>
            @endif
            @if(!empty($job_career))
                <li class="-wide__list__detail"><span
                            class="_list__detail__title">こだわり</span><span>：{{$job_career}}</span>
                </li>
            @endif
        </ul>
        <div class="_list__button__block u-mt20">
            <div data-id="{{$id}}" data-secret="{{$data_secret}}"
                 class="c-button-square--mm--gray u-fs--l bookmark_job">
                @if($path == 'mybox')
                    削除する
                @else
                    <span class="c-font-icon--attachment"></span>キープする
                @endif
            </div>
            <div class="c-button-square--mm u-fs--l">
                <a onClick="ga('send','event','osusume list','click','{{$url}}');" href="{{$job_url}}">もっと詳しくみる</a>
            </div>
        </div>
    </div>
</div>