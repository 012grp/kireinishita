<?php
$path = Request::path();

$job_name = _common::getJobName($job_name);

//会社
$job_company = strip_tags($company_name);

$job_shop = strip_tags($shop_name);

$listCareer = _helper::getAllianceListHangup((object)array('shop_id' => $shop_id, 'id' => $id, 'job_holiday' => $job_holiday));

$data_secret = 2;

//職種
$job_category = _helper::getAllianceJobCategoryNameByListId($list_job);

//雇用形態
$job_career = !empty($listCareer) ? implode(' ', $listCareer) : '';

//こだわり
$job_type = _helper::getEmploymentNamesByListId($list_em);


//給与
$job_salary = strip_tags($job_salary);

//勤務地
$job_location = _helper::getJobAddressSpecify($list_province, $list_city, $job_location);

$site_name = '';

if (empty($job_url)) {
    $secretUrl = _helper::getUrlName('alliancePage');
    $job_url = '/' . $secretUrl . '/' . $id;
} else {
    $job_url = _helper::make_job_url($id);
}


//TODO: check image
$job_image = $job_image;
if (!isset($job_image) || empty($job_image)) {
    $job_image = _common::noImageUrlContentsBySiteId(0);
} else {
    if (isset($job_image_sp) && !empty($job_image_sp)) {
        $job_image = $job_image_sp;
    } else {
        //$rs = strpos($job_image, 'http');
        if ($job_image[0] != '/') {
            //$job_image = '//'.$job_image;
            $job_image = _common::proxy_image($job_image);
        }
    }
}
$list_em = _helper::getAllianceEmploymentNamesByListId($list_em);
$is_new = _helper::checkNew($created_at, 7);
$url = str_replace('?fr=kirei','',$url);
?>


<div class="column-box--wide__list {{'u-mt15'}}">
    <div class="-wide__list__tagBlock">
        @if($data_secret)
            <div class="-wide__list__tagBlock--l"><span class="u-fs--xxs c-tag--secret--flow">オススメ</span>
                @if(!empty($celebration))
                    <span class="u-fs--xxs c-tag--celebration">{{$celebration}}</span>
                @endif
            </div>
        @endif
        @if($is_new && $is_secret == 1)
            <div class="-wide__list__tagBlock--r"><span class="u-fs--xxs c-tag--new--flow">NEW</span></div>
        @endif
    </div>
    <h2 class="u-fs--xlh -wide__list__ttl u-fwb"><span class="_list__multiTag__text">{{ $job_name }}</span></h2>
    <h3 class="-wide__list__subTtl u-fs--xs u-mt10 u-fwb">{{ $job_company }}</h3>
    <div class="-wide__list__bottom u-mt15">
        <div style="background-image:url({{$job_image}})" class="-wide__list__img"></div>
        <ul class="-wide__list__details u-fs--sh">
            <li class="-wide__list__detail u-fs--s"><span
                        class="_list__detail__title">職種</span><span>：{{$job_category or ''}}</span>
            </li>
            <li class="-wide__list__detail"><span class="_list__detail__title">給与</span><span>：{{$job_salary}}</span>
            </li>
            <li class="-wide__list__detail--multiline"><span
                        class="_list__detail__title">勤務地</span><span>：{{$job_location}}</span>
            </li>
            @if(!empty($list_em))
                <li class="-wide__list__detail u-mt5"><span
                            class="_list__detail__title">雇用形態</span><span>：{{$list_em}}</span></li>
            @endif
            @if(!empty($job_career))
                <li class="-wide__list__detail"><span
                            class="_list__detail__title">こだわり</span><span>：{{$job_career}}</span>
                </li>
            @endif
        </ul>
    </div>
    <div class="_list__button__block u-mt20">

        <div data-id="{{$id}}" data-secret="{{$data_secret}}" class="c-button-square--mm--gray u-fs--xs bookmark_job">
            @if($path == 'mybox')
                削除する
            @else
                <span class="c-font-icon--attachment"></span>キープする
            @endif
        </div>
        <div class="c-button-square--mm u-fs--xs">
            <a onClick="ga('send','event','osusume list','click','{{$url}}');" href="{{$job_url}}">もっと詳しくみる</a>
        </div>
    </div>
    @if(!$data_secret)
        <p class="-wide__list__footer u-fs--xxs u-mt10">提供元：{{$site_name}}</p>
    @endif
</div>
