<?php

$class_primary_box = 'u-mt20';
$company_name = strip_tags($detail['company_name']);
$job_name = strip_tags($detail['job_name']);
$job_salary = strip_tags($detail['job_salary']);
$job_location = strip_tags($detail['job_location']);
$job_career = strip_tags($detail['job_career']);
$celebration = strip_tags($detail['job_celebration']);
$job_image = $detail['job_image'];

$employment_status = strip_tags($detail['employment_status']);
$job_description = strip_tags($detail['job_description']);
$job_notice_header = strip_tags($detail['job_notice_header']);
$job_notice_content = strip_tags($detail['job_notice_content']);
$job_notice_image = strip_tags($detail['job_notice_image']);
$job_salary_detail = $detail['job_salary_detail'];
$content = $detail['content'];
$job_qualification = $detail['job_qualification'];
$job_location_detail = $detail['job_location_detail'];
$treatment_welfare = $detail['treatment_welfare'];
$job_holiday = $detail['job_holiday'];
$job_crated_at = $detail['created_at'];

$list_hangup = _helper::getAllianceListHangup($detail);
$accessGide = _helper::getAccessGuide($detail);

$list_category = _helper::getAllianceJobCategoryNameByListId($detail['list_job']);
$list_parent_category = _helper::getAllienceParentCategory($detail['list_job']);

$list_em = _helper::getAllianceEmploymentNamesByListId($detail['list_em']);

$alliance_business_category = _helper::getAllianceBusinessCategoryNamesByListId($detail['list_industry']);


$access = $detail['access'];

$company_category = strip_tags($detail['category']);
$company_address = strip_tags($detail['address']);
$company_average_price = strip_tags($detail['average_price']);
$company_seat_number = strip_tags($detail['seat_number']);
$company_holiday = strip_tags($detail['holiday']);
$company_access = strip_tags($detail['company_access']);

if (empty($job_notice_image)) {
    $job_notice_image = _common::noImageUrl();
}

$key = Config::get('webconfig.google_map_api_key');

//social
$request_url = Request::url();
$title = _helper::set_title($job_name);

//SEO
$web_title = sprintf(_helper::set_title(trans('webinfo.title_secret_detail')), $job_name);

// $description = isset($detail['meta_desc']) ? $detail['meta_desc'] : trans('webinfo.meta_desc_secret_job');
// if($description == "")
// {
// 	$description = null;
// }

//$keywords = $detail['meta_keyword'];
$description = sprintf(trans('webinfo.meta_desc_secret_detail'), $company_name, $job_name);
$keywords = sprintf(trans('webinfo.keywords_secret_detail'), $company_name);



if (!empty($job_image)) {
    $image = $job_image;
}

//TODO: set company name length to show
$google_map_company_name = $company_name;
if (mb_strlen($google_map_company_name) > 17) {
    $google_map_company_name = mb_substr($google_map_company_name, 0, 17, 'utf-8') . '...';
}

$allianceUrl = _helper::getUrlName('alliancePage');

$shop_name = strip_tags($detail['shop_name']);

$job_wanted_people = strip_tags($detail['job_wanted_people']);
$is_new = $detail['is_new'];
$job_summary = strip_tags($detail['job_summary']);

$list_province = $detail['list_province'];
$list_job = $detail['list_job'];
$list_employment = $detail['list_em'];

$class_primary_box = 'u-mt20';
$company_name = strip_tags($detail['company_name']);
$job_name = strip_tags($detail['job_name']);
$job_salary = strip_tags($detail['job_salary']);
$job_location = _helper::getJobAddress($detail);
$job_career = strip_tags($detail['job_career']);
$celebration = strip_tags($detail['job_celebration']);
$job_image = $detail['job_image'];

$employment_status = strip_tags($detail['employment_status']);
$job_description = strip_tags($detail['job_description']);
$job_notice_header = strip_tags($detail['job_notice_header']);
$job_notice_content = strip_tags($detail['job_notice_content']);
$job_notice_image = strip_tags($detail['job_notice_image']);
$job_salary_detail = $detail['job_salary_detail'];
$content = $detail['content'];
$job_qualification = $detail['job_qualification'];
$job_location_detail = $detail['job_location_detail'];
$treatment_welfare = $detail['treatment_welfare'];
$job_holiday = $detail['job_holiday'];
$job_crated_at = $detail['created_at'];

//$list_hangup = _helper::getCategoryNamesByListId('',$detail['list_hangup']);
$list_hangup = _helper::getAllianceListHangup($detail);
$accessGide = _helper::getAccessGuide($detail);


$access = $detail['access'];

$company_category = strip_tags($detail['category']);
$company_address = strip_tags($detail['address']);
$company_average_price = strip_tags($detail['average_price']);
$company_seat_number = strip_tags($detail['seat_number']);
$company_holiday = strip_tags($detail['holiday']);
$company_access = strip_tags($detail['company_access']);

if (empty($job_notice_image)) {
    $job_notice_image = _common::noImageUrl();
}

$key = Config::get('webconfig.google_map_api_key');

//social
$request_url = Request::url();
$title = _helper::set_title($job_name);

//SEO
$web_title = sprintf(_helper::set_title(trans('webinfo.title_secret_detail')), $job_name);

//$keywords = $detail['meta_keyword'];
$description = sprintf(trans('webinfo.meta_desc_secret_detail'), $company_name, $job_name);
$keywords = sprintf(trans('webinfo.keywords_secret_detail'), $company_name);



if (!empty($job_image)) {
    $image = $job_image;
}

//TODO: set company name length to show
$google_map_company_name = $company_name;
if (mb_strlen($google_map_company_name) > 17) {
    $google_map_company_name = mb_substr($google_map_company_name, 0, 17, 'utf-8') . '...';
}

$allianceUrl = _helper::getUrlName('alliancePage');

$shop_name = strip_tags($detail['shop_name']);

$job_wanted_people = strip_tags($detail['job_wanted_people']);
$is_new = $detail['is_new'];
$job_summary = strip_tags($detail['job_summary']);

$list_province = $detail['list_province'];
$list_job = $detail['list_job'];
$list_employment = $detail['list_em'];

$urlNoKirei = str_replace(['?fr=kirei', 'fr=kirei'], '', $detail['url']); 
?>

@section('css')
    <style type="text/css">
        ._secret__detail__table.is-disable {
            display: block !important;
            visibility: hidden;
            height: 0px;
        }
    </style>
@stop
@extends('layouts.default')

@section('column_right')
    @include('shared.right_column.no_lastest_article')
@stop

@section('js_google')
    <input type="hidden" id="job_lat" value="<?php echo $detail['lat'];?>">
    <input type="hidden" id="job_lng" value="<?php echo $detail['lng'];?>">
    <input type="hidden" id="google_map_company_name" value="<?php echo $google_map_company_name; ?>">
    <input type="hidden" id="job_location" value="<?php echo $job_location; ?>">

    <input type="hidden" id="company_lat" value="<?php echo $detail['company_lat'];?>">
    <input type="hidden" id="company_lng" value="<?php echo $detail['company_lng'];?>">
    <input type="hidden" id="address_com" value="<?php echo $detail['address'];?>">

    <script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
@stop

@section('breadcrumb')
    @include('breadcrumb.default', array(
        'page' 		=> '株式会社'.$company_name .'の求人',
        'parent'	=> array(
            array('link' => '/'. $allianceUrl .'/', 'title' => '検索結果'),
            )
            ))
@stop

@section('column_left')

    <div data-target-leftcolumn="target" class="column-left">
        <article class="column-box__secret">
            <div class="column-box__secret__heading">
                <div class="_secret__heading__left u-fs--xs--recruit u-fwb">{{ $shop_name }}<br>{{$company_name}}</div>
                <div class="_secret__heading__right">
                    @if($job_crated_at > (new DateTime('-7 day'))->format('Y-m-d H:i:s'))
                        <span class="u-fs--xxs c-tag--new--flow">NEW</span>
                    @endif
                    <span class="u-fs--xxs c-tag--recommend">オススメ</span>
                    <span class="u-fs--xxs c-tag--celebration--flow">入社お祝い金{{$celebration/10000}}万円</span>
                </div>
                <div>&nbsp;</div>
                <div class="_secret__heading__status">
                    <div class="_secret__heading__status__item c-font-icon--attachment"></div>
                    <span class="_secret__heading__status__item__number">0</span>
                    <div class="_secret__heading__status__item u-ml30">
                        <i class="c-icon--eye"></i><span
                                class="_secret__heading__status__item__number">{{$detail['view']}}</span>
                    </div>
                </div>
                <h1 class="_secret__heading__ttl u-fs--xxlh--contents u-fwb">{{$job_name}}</h1>
            </div>


            <div class="column-box__secret__slider--alliance u-mt15">
                <div data-target-slider-alliance="target" class="_secret__slider__main">
                    @if($job_image)
                        <div class="_slider__main__img"><img src="{{$job_image}}" alt=""></div>
                    @endif
                    @if($detail['job_thumbnail'])
                        <div class="_slider__main__img"><img data-lazy="{{$detail['job_thumbnail']}}" alt=""></div>
                    @endif
                </div>
            </div>

            <div class="_secret__tag u-mt10">
                @if(!empty($list_hangup))
                    @foreach($list_hangup as $item)
                        <span class="c-tag--secret--detail c-tag--secret--detail--alliance u-fs--xs">{{ $item }}</span>
                    @endforeach
                @endif
            </div>


            {{-- static html --}}
            @if($category['status'] ==1)
            <div class="_secret__text__block u-fs--lh--contents">
                <div class="_secret__text__description__wrap">

                    <h3 class="_secret__text__description__heading">{{$list_category}}とは</h3>

                    @if($category['description'])
                        <p class="_secret__text__description__body">{{$category['description']}}</p>
                    @endif
                </div>
            </div>
            @endif

                <?php /*
                    <div data-target-textcount-wrap="target" class="_secret__text__wrap">
                         <h3 class="_secret__text__ttl u-fwb u-mt25 is-disable">海外ビールの輸入販売を手がける専門商社が手がける、新しいコンセプトのグローバルなレストラン。5/20　注目のNEW　OPEN！</h3>
                         <p class="_secret__text is-disable">独占輸入の樽生ビール、世界のクラフトビール生Tap15種～30種！日本地ビール、激レアビールも！季節のビールなども含む50種以上を提供するビアレストランが、この度、【マロニエゲート銀座】にNEW　OPEN！<br>店内は、ベルギーやロンドン、ヨークシャーなどの若者でごった返す「ビアハウス」を再現！世界のお酒の輸入を手がける弊社だからこそ運営できる専門店です。</p>
                         <h3 class="_secret__text__ttl u-fwb u-mt25 is-disable">キッチン職希望の方へ！</h3>
                         <p class="_secret__text is-disable">20代の方でもシェフを目指せるチャンスあり！海外の新しい料理など幅広い調理技術を経験できることも可能です。学んだ集客の知恵袋をもとに料理長だけでなく、SVなど更に上のポジションを目指すこともできるので、将来的なキャリアの部分でも安心できる環境です。<br>責任感ある方には大きな仕事をどんどん任せていきます。ヨーロッパ料理を習得したい人、海外で働くのを目標にしたい人、洋食希望者、経験者大集合！</p>
                    </div>
                    <p data-trigger-btn-more="trigger" class="c-button-square--more is-disable u-mt30 icon-plus">続きを表示</p>

            </div>                    */ ?>


                <div class="_secret__notice u-mt25">
                    <div><span class="c-tag--secret--notice u-fs--xs">注目ポイント！</span>
                        <div class="_secret__notice__text u-fs--xs--recruit u-mt5">
                            {{ $detail['job_pr'] }}
                        </div>
                    </div>
                </div>


                <div class="_secret__social__block u-mt30">
                    <a target="_blank"
                       href="https://www.facebook.com/sharer/sharer.php?u={{ $request_url }}&amp;src=sdkpreparse"
                       class="c-button-square--social c-facebook">
                        <i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i><span
                                class="u-fs--l">シェア</span>
                    </a>
                    <a href="https://twitter.com/intent/tweet?url={{ $request_url }}&amp;text={{ $title }}"
                       class="c-button-square--social c-twitter">
                        <i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i><span
                                class="u-fs--l">ツイート</span>
                    </a>
                </div>
        </article>
        <div class="column-box__secret--table">
            <ul class="_secret__detail__tab">
                <li data-trigger-tab="0" class="_detail__tab__inner is-active u-fs--lh--contents">募集情報</li>
                <li data-trigger-tab="1" class="_detail__tab__inner u-fs--lh--contents">企業情報・店舗情報</li>
            </ul>


            <div class="_secret__detail--recruit">
                <table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit">
                    <tbody>
                    @if($list_parent_category)
                        <tr>
                            <td class="_detail__table__left u-fwb">ジャンル</td>
                            <td class="_detail__table__right">
                                <?php _helper::explode_character($list_parent_category); ?>
                            </td>
                        </tr>
                    @endif


                    @if($list_category)
                        <tr>
                            <td class="_detail__table__left u-fwb">職種</td>
                            <td class="_detail__table__right">
                                <?php _helper::explode_character($list_category); ?>
                            </td>
                        </tr>
                    @endif

                    @if($list_em)
                        <tr>
                            <td class="_detail__table__left u-fwb">雇用形態</td>
                            <td class="_detail__table__right">
                                <p class="_detail__table__text">
                                    <?php _helper::explode_character($list_em); ?>
                                </p>
                            </td>
                        </tr>
                    @endif

                    @if($job_summary)
                        <tr>
                            <td class="_detail__table__left u-fwb">仕事内容</td>
                            <td class="_detail__table__right">
                                <p class="_detail__table__text"></p>{!! nl2br(e(_helper::addOneBr($job_summary))) !!}
                            </td>
                        </tr>
                    @endif

                    @if($job_salary)
                        <tr>
                            <td class="_detail__table__left u-fwb">給与</td>
                            <td class="_detail__table__right">
                                {!! nl2br(e(_helper::addBr($job_salary))) !!}
                            </td>
                        </tr>
                    @endif


                    @if($list_em)
                        <tr>
                            <td class="_detail__table__left u-fwb">応募資格</td>
                            <td class="_detail__table__right">
                                <p class="_detail__table__text">
                                    @if($list_em)
                                        <?php _helper::explode_character($list_em); ?>
                                    @endif
                                </p>
                                <?php /*
          						<ul class="_detail__table__text--list u-mt10">
          							<li>●オープニングスタッフ希望者</li>
          							<li>●美容店未経験者も歓迎</li>
          							<li>●調理が好きな方 、世界の料理を学びたい方、外国語に触れたい方！</li>
          							<li>●調理経験者（特に、イタリアン、ビストロ、カフェ等の洋食調理経験者大歓迎！）他ジャンルの経験をお持ちの方ももちろんOK！</li>
          							<li>●美容店でのサービス経験者（ジャンル不問）</li>
          							<li>●ビールやワイン好きな方、ビアテイスターやソムリエを目指したい方</li>
          							<li>などなど</li>
          						</ul>
                                        */ ?>

                            </td>
                        </tr>
                    @endif


                    @if($job_location)
                        <tr>
                            <td class="_detail__table__left u-fwb">勤務地</td>
                            <td class="_detail__table__right">
                                {{$job_location}}
                            </td>
                        </tr>
                    @endif


                    @if(!empty($detail['lat']) && !empty($detail['lng']) )
                        <tr>
                            <td class="_detail__table__left u-fwb">アクセス</td>
                            <td class="_detail__table__right">
                                @if($accessGide)
                                    {{$accessGide}}
                                @endif
                                <div class="_detail__table__map u-mt10" id="job_map"></div>
                                <a target="_blank"
                                   href="https://www.google.com/maps/place/{{$job_location}},+Japan/{{'@'}}{{ $detail['lat'] or ''}},{{$detail['lng'] or ''}},17z?hl=ja-JP"
                                   id="url_job_map">
                                    GoogleMapで確認する</a>
                            </td>
                        </tr>
                    @endif

                    @if($treatment_welfare)
                        <tr>
                            <td class="_detail__table__left u-fwb">待遇・福利厚生</td>
                            <td class="_detail__table__right">
                                {{$treatment_welfare}}
                            </td>
                        </tr>
                    @endif

                    @if($job_holiday)
                        <tr>
                            <td class="_detail__table__left u-fwb">休日休暇</td>
                            <td class="_detail__table__right">
                                {{$job_holiday}}
                            </td>
                        </tr>
                    @endif

                    @if($job_wanted_people)
                        <tr>
                            <td class="_detail__table__left u-fwb">求める人材<br>について</td>
                            <td class="_detail__table__right">
                                {!! nl2br(e(_helper::addOneBr($job_wanted_people))) !!}
                            </td>
                        </tr>
                    @endif

                    </tbody>
                </table>
                <table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit is-disable">
                    <caption class="_secret__detail__table__heading">店舗情報</caption>
                    <tbody>
                    @if($shop_name)
                        <tr>
                            <td class="_detail__table__left u-fwb">店名</td>
                            <td class="_detail__table__right">{{$shop_name}}</td>
                        </tr>
                    @endif

                    @if($alliance_business_category)
                        <tr>
                            <td class="_detail__table__left u-fwb">店舗形態</td>
                            <td class="_detail__table__right">{{$alliance_business_category}}</td>
                        </tr>
                    @endif

                    <?php /* //store_info
						          				
          				<tr>
          					<td class="_detail__table__left u-fwb">事務所</td>
          					<td class="_detail__table__right">会社情報の本所在地が入ります</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">営業時間</td>
          					<td class="_detail__table__right">【月～金 】<br>9:00 〜21:00<br>【土】<br>8:00 〜20:00</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">定休日</td>
          					<td class="_detail__table__right">定休日が入ります</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">客層</td>
          					<td class="_detail__table__right">20代 / 30代</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">よくあるご質問</td>
          					<td class="_detail__table__right">
          						<h4 class="_detail__table__right__heading">職場の雰囲気について</h4>
          						<p>職場の雰囲気についてが入ります。職場の雰囲気についてが入ります。職場の雰囲気についてが入ります。職場の雰囲気についてが入ります。</p>
          						<h4 class="_detail__table__right__heading">スキルアップや教育制度について</h4>
          						<p>スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。</p>
          						<h4 class="_detail__table__right__heading">仕事のやりがいについて</h4>
          						<p>仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。</p>
          					</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">スタッフ数や男女比</td>
          					<td class="_detail__table__right">スタッフ数や男女比が入ります</td>
          				</tr>

                    */?>
                    </tbody>
                </table>
                <table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit is-disable u-mt25">
                    <caption class="_secret__detail__table__heading">企業情報</caption>
                    <tbody>

                    @if($company_name)
                        <tr>
                            <td class="_detail__table__left u-fwb">企業名</td>
                            <td class="_detail__table__right">{{$company_name}}</td>
                        </tr>
                    @endif


                    <?php /*
          				<tr>
          					<td class="_detail__table__left u-fwb">代表者名</td>
          					<td class="_detail__table__right">代表者名が入ります</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">事業内容</td>
          					<td class="_detail__table__right">事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。事業内容が入ります。</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">本社所在地</td>
          					<td class="_detail__table__right">本社所在地が入ります</td>
          				</tr>
          				<tr>
          					<td class="_detail__table__left u-fwb">HP</td>
          					<td class="_detail__table__right">http://xxx.xxx</td>
          				</tr>

                        */ 
					?>
                    </tbody>
                </table>
            </div>

            <div class="column-box__secret__btnBLock">
                <div data-id="{{$detail['id']}}" data-secret="2"
                     class="c-button-square--l--gray u-fs--xxl u-fwb bookmark_job"><span
                            class="c-font-icon--attachment"></span>キープする
                </div>
               
                <div class="c-button-square--l u-fs--xxl u-fwb">
					  <a 
					   target="_blank"
					   onClick="ga('send','event','link-jobnote','click','{{$urlNoKirei}}');"
					   href="{{$detail['url']}}">応募する</a>
           		</div>
            </div>
        </div>

        {{-- @include('secret_kyujin._similar_job', array('jobId'=> $detail['id'],'companyId'=> $detail['company_id'])) --}}
        <?php
        $alliances = _helper::getAllienceJobBookmark();
        $alliance_jobs = _modules::get_related_allliance_by_company_or_shop($company_name, $detail['shop_id'], $detail['id']);
        ?>
        @include('shared.item.alliance_related_content',
            array(
                'title' => 'あなたに似た人はこちらの求人をキープしています',
                'alliances' => $alliances
                ))

        @include('shared.item.alliance_related_content',
            array(
                'title' => 'この企業・店舗が募集しているほかの求人',
                'alliances' => $alliance_jobs
                ))
    </div>
@stop
