<?php
/*
@section('css')
<link rel="stylesheet" href="/public/sp/css/content.css">
@stop
*/
$request_url = Request::url();


$company_name = strip_tags($detail['company_name']);
$job_name = strip_tags($detail['job_name']);
$job_salary = strip_tags($detail['job_salary']);
$job_location = strip_tags($detail['job_location']);
$job_career = strip_tags($detail['job_career']);
$celebration = strip_tags($detail['job_celebration']);
$job_image = $detail['job_image'];

$employment_status = strip_tags($detail['employment_status']);
$job_description = strip_tags($detail['job_description']);
$job_notice_header = strip_tags($detail['job_notice_header']);
$job_notice_content = strip_tags($detail['job_notice_content']);
$job_notice_image_sp = strip_tags($detail['job_notice_image_sp']);
$job_salary_detail = $detail['job_salary_detail'];
$content = $detail['content'];
$job_qualification = $detail['job_qualification'];
$job_location_detail = $detail['job_location_detail'];
$treatment_welfare = $detail['treatment_welfare'];
$job_holiday = $detail['job_holiday'];
$job_crated_at = $detail['created_at'];

$list_hangup = _helper::getAllianceListHangup($detail);
$accessGide = _helper::getAccessGuide($detail);


$list_category = _helper::getAllianceJobCategoryNameByListId($detail['list_job']);
$list_parent_category = _helper::getAllienceParentCategory($detail['list_job']);

$list_em = _helper::getAllianceEmploymentNamesByListId($detail['list_em']);

$alliance_business_category = _helper::getAllianceBusinessCategoryNamesByListId($detail['list_industry']);

$access = $detail['access'];

$company_category = strip_tags($detail['category']);
$company_address = strip_tags($detail['address']);
$company_average_price = strip_tags($detail['average_price']);
$company_seat_number = strip_tags($detail['seat_number']);
$company_holiday = strip_tags($detail['holiday']);
$company_access = strip_tags($detail['company_access']);

if (empty($job_notice_image_sp)) {
    if (!empty($detail['job_notice_image'])) {
        $job_notice_image_sp = $detail['job_notice_image'];
    } else {
        $job_notice_image_sp = _common::noImageUrl();
    }

}

$key = Config::get('webconfig.google_map_api_key');

//social
$request_url = Request::url();
$title = _helper::set_title($job_name);

//SEO
$web_title = sprintf(_helper::set_title(trans('webinfo.title_secret_detail')), $job_name);
// $description = isset($detail['meta_desc']) ? $detail['meta_desc'] : trans('webinfo.meta_desc_secret_job');
// if($description == "")
// {
//     $description = null;
// }

//$keywords = $detail['meta_keyword'];
$description = sprintf(trans('webinfo.meta_desc_secret_detail'), $company_name, $job_name);
$keywords = sprintf(trans('webinfo.keywords_secret_detail'), $company_name);


if (!empty($job_image)) {
    $image = $job_image;
}

//TODO: set company name length to show
$google_map_company_name = $company_name;
if (mb_strlen($google_map_company_name) > 17) {
    $google_map_company_name = mb_substr($google_map_company_name, 0, 17, 'utf-8') . '...';
}

$HP = _helper::getRelatedSecretJobs($detail['id'], $detail['company_id']);

$content = _common::changeCSSContentSecret($content);


$allianceUrl = _helper::getUrlName('alliancePage');

$shop_name = strip_tags($detail['shop_name']);
$job_wanted_people = strip_tags($detail['job_wanted_people']);
$is_new = $detail['is_new'];
$job_summary = strip_tags($detail['job_summary']);

$list_province = $detail['list_province'];
$list_job = $detail['list_job'];
$list_employment = $detail['list_em'];

$job_location = _helper::getJobAddress($detail);

$urlNoKirei = str_replace(['?fr=kirei', 'fr=kirei'], '', $detail['url']); 
?>

@extends('layouts.default_sp')

@section('css')
    <style type="text/css">
        ._secret__detail__table.is-disable {
            display: block !important;
            visibility: hidden;
            height: 0px;
            overflow: hidden;
        }
    </style>
@stop

@section('js_google')
    <input type="hidden" id="job_lat" value="<?php echo $detail['lat']; ?>">
    <input type="hidden" id="job_lng" value="<?php echo $detail['lng']; ?>">
    <input type="hidden" id="google_map_company_name" value="<?php echo $google_map_company_name; ?>">
    <input type="hidden" id="job_location" value="<?php echo $job_location; ?>">

    <input type="hidden" id="company_lat" value="<?php echo $detail['company_lat']; ?>">
    <input type="hidden" id="company_lng" value="<?php echo $detail['company_lng']; ?>">
    <input type="hidden" id="address_com" value="<?php echo $detail['address']; ?>">

    <script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>

@stop

@section('page_sp')
    <div class="primary-box--noPadding">
        <div class="contents-box--recruit__top">
            <h1 class="-recruit__top__ttl u-fwb u-fs--xs">{{ $shop_name }}<br>{{$company_name}}</h1>
            <div class="-recruit__top__inner">
                <div class="-wide__list__tagBlock u-mt20">
                    <div class="-wide__list__tagBlock--l">


                        @if($job_crated_at > (new DateTime('-7 day'))->format('Y-m-d H:i:s'))
                            <span class="u-fs--xxs c-tag--new--flow">NEW</span>
                        @endif
                        <span class="u-fs--xxs c-tag--recommend">オススメ</span>
                        <span class="u-fs--xxs c-tag--celebration">入社お祝い金{{$celebration/10000}}万円</span>
                    </div>
                </div>
                <div class="-recruit__top__status u-mt15">
                    <div class="-recruit__top__status__item">
                        <div class="c-font-icon--attachment"></div>
                        <div class="-recruit__top__status__item__number">0</div>
                    </div>
                    <div class="-recruit__top__status__item u-ml15">
                        <div class="c-icon--eye--s"></div>
                        <div class="-recruit__top__status__item__number">{{$detail['view']}}</div>
                    </div>
                </div>
                <h2 class="contents-box--recruit__ttl u-fs--xlh u-mt15 u-fwb">{{$job_name}}</h2>
            </div>
            <div class="column-box__secret__slider u-mt15">
                <div data-target-slider="target" class="_secret__slider__main">
                    <div class="_slider__main__img">
                        <img src="{{$job_image}}" alt="">
                    </div>
                    @if($detail['job_thumbnail'])
                        <div class="_slider__main__img">
                            <img data-lazy="{{$detail['job_thumbnail']}}" alt="">
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="contents-box--recruit__middle u-mt15">
            @if(!empty($list_hangup))
                @foreach($list_hangup as $item)
                    <span class="c-tag--secret--detail--alliance u-fs--xs">{{ $item }}</span>
                @endforeach
            @endif

            <div class="contents-box--recruit__description u-mt30">
         
                 <h2 class="contents-box--recruit__description__heading">{{$list_category}}とは</h2>

                @if($category['description'])
                    <p class="contents-box--recruit__description__content">{{$category['description']}}</p>
                @endif
            </div>

            <?php /*

		<div data-target-secret-text-wrap="target" class="contents-box__text__wrap">
			<h2 data-target-textlength="target" class="contents-box--recruit__ttl u-fs--xlh u-mt30 u-fwb">海外ビールの輸入販売を手がける専門商社が手がける、新しいコンセプトのグローバルなレストラン。5/20注目のNEWOPEN！</h2>
			<p data-target-textlength="target" class="contents-box__text u-fs--mh u-mt15">
				ロールキャベツはタネ作りに少し手聞がかかるイメージがあります。でも薄切り肉を使えばもっと簡単！
				春キャベツは生のまま、適当な大きさに切って薄切り肉を巻きます。火の通りも早いのでサッと煮こむのはもちろん、
				焼いたり蒸したり、衣をつけて揚げたりすれば、お好みのロールキャベツに。食感も楽しい新感覚の口ールキャベツをお試しください。
			</p>
			<h2 data-target-textlength="target" class="contents-box--recruit__ttl u-fs--xlh u-mt25 u-fwb">キッチン職希望の方へ！</h2>
			<p data-target-textlength="target" class="contents-box__text u-fs--mh u-mt15">
				シェフ（料理長）をめざしていただくことも可能です。シェフになると、メニュー開発、原価や売上などの数値管理、
				スタッフの採用ならびに育成、仕入先との折衝業務など、多岐に渡っておまかせします。
				ご利用になるすべての方にとって、居心地の良い空間になるような料理・サービスを心がけています。
			</p>
		</div>
		<p data-trigger-btn-more="trigger" class="c-button-square--more u-mt20"><span class="c-icon--plus"></span>続きを表示</p>
		
		*/ ?>

            <div class="contents-box--recruit__attention u-mt30"><span
                        class="c-tag--notice u-fs--xxs u-fwb">注目ポイント！</span>
                <p class="-recruit__attention__text u-fs--sh--recruit u-mt10">
                    {{ $detail['job_pr'] }}
                </p>
            </div>
        </div>
        <div class="column-box__secret--table u-mt20">
            <ul class="_secret__detail__tab">
                <li data-trigger-tab="0" class="_detail__tab__inner is-active u-fs--xs">募集情報</li>
                <li data-trigger-tab="1" class="_detail__tab__inner u-fs--xs">企業情報・店舗情報</li>
            </ul>
            <div class="_secret__detail--recruit">
                <table data-target-tabinner="target" class="_secret__detail__table u-fs--sh--recruit">
                    <tbody>
                    @if($list_parent_category)
                        <tr>
                            <td class="_detail__table__left u-fwb">ジャンル</td>
                            <td class="_detail__table__right">
                                <?php _helper::explode_character($list_parent_category); ?>
                            </td>
                        </tr>
                    @endif

                    @if($list_category)
                        <tr>
                            <td class="_detail__table__left">職種</td>
                            <td class="_detail__table__right">
                                <?php _helper::explode_character($list_category); ?>
                            </td>
                        </tr>
                    @endif

                    @if($list_em)
                        <tr>
                            <td class="_detail__table__left">雇用形態</td>
                            <td class="_detail__table__right">
                                <?php _helper::explode_character($list_em); ?>
                            </td>
                        </tr>
                    @endif

                    @if($job_summary)
                        <tr>
                            <td class="_detail__table__left">仕事内容</td>
                            <td class="_detail__table__right">
                                <p class="_detail__table__text"></p>{!! nl2br(e(_helper::addOneBr($job_summary))) !!}
                            </td>
                        </tr>
                    @endif


                    @if($job_salary)
                        <tr>
                            <td class="_detail__table__left">給与</td>
                            <td class="_detail__table__right">
                                {!! nl2br(e(_helper::addBr($job_salary))) !!}
                            </td>
                        </tr>
                    @endif

                    @if($list_em)
                        <tr>
                            <td class="_detail__table__left">応募資格</td>
                            <td class="_detail__table__right">
                                <p class="_detail__table__text">
                                    @if($list_em)
                                        <?php _helper::explode_character($list_em); ?>
                                    @endif
                                </p>
                                <?php /*
							<ul class="_detail__table__text--list u-mt10">
								<li>●オープニングスタッフ希望者</li>
								<li>●飲食店未経験者も歓迎</li>
								<li>●調理が好きな方 、世界の料理を学びたい方、外国語に触れたい方！</li>
								<li>●調理経験者（特に、イタリアン、ビストロ、カフェ等の洋食調理経験者大歓迎！）他ジャンルの経験をお持ちの方ももちろんOK！</li>
								<li>●飲食店でのサービス経験者（ジャンル不問）</li>
								<li>●ビールやワイン好きな方、ビアテイスターやソムリエを目指したい方</li>
								<li>などなど</li>
							</ul>
							*/ ?>
                            </td>
                        </tr>
                    @endif


                    @if($job_location)
                        <tr>
                            <td class="_detail__table__left">勤務地</td>
                            <td class="_detail__table__right">
                                {{$job_location}}
                            </td>
                        </tr>
                    @endif

                    @if(!empty($detail['lat']) && !empty($detail['lng']) )
                        <tr>
                            <td class="_detail__table__left">アクセス</td>
                            <td class="_detail__table__right">
                                @if($accessGide)
                                    {{$accessGide}}
                                @endif
                                <div class="_detail__table__map u-mt10" id="job_map"></div>
                                <a target="_blank"
                                   href="https://www.google.com/maps/place/{{$job_location}},+Japan/{{'@'}}{{ $detail['lat'] or ''}},{{$detail['lng'] or ''}},17z?hl=ja-JP"
                                   id="url_job_map">
                                    GoogleMapで確認する</a>
                            </td>
                        </tr>
                    @endif


                    @if($treatment_welfare)
                        <tr>
                            <td class="_detail__table__left">待遇・<br>福利厚生</td>
                            <td class="_detail__table__right">
                                {{$treatment_welfare}}
                            </td>
                        </tr>
                    @endif

                    @if($job_holiday)
                        <tr>
                            <td class="_detail__table__left">休日休暇</td>
                            <td class="_detail__table__right">
                                <ul class="_detail__table__text--list">
                                    <li>{{$job_holiday}}</li>
                                </ul>
                            </td>
                        </tr>
                    @endif

                    @if($job_wanted_people)
                        <tr>
                            <td class="_detail__table__left">求める人材について</td>
                            <td class="_detail__table__right">
                                {!! nl2br(e(_helper::addOneBr($job_wanted_people))) !!}
                            </td>
                        </tr>
                    @endif

                    </tbody>
                </table>
                <h3 data-target-tabinner="target" class="_secret__detail__table__heading is-disable">店舗情報</h3>
                <table data-target-tabinner="target" class="_secret__detail__table u-fs--sh--recruit is-disable">
                    <tbody>
                    @if($shop_name)
                        <tr>
                            <td class="_detail__table__left">店名</td>
                            <td class="_detail__table__right">{{$shop_name}}</td>
                        </tr>
                    @endif

                    @if($alliance_business_category)
                        <tr>
                            <td class="_detail__table__left">店舗形態</td>
                            <td class="_detail__table__right">{{$alliance_business_category}}</td>
                        </tr>
                    @endif


                    <?php /* //store_info
					<tr>
						<td class="_detail__table__left">事務所</td>
						<td class="_detail__table__right">会社情報の本社所在地が入ります</td>
					</tr>
					<tr>
						<td class="_detail__table__left">営業時間</td>
						<td class="_detail__table__right">【月～金】<br>9:00〜21:00<br>【土】<br>8:00〜20:00</td>
					</tr>
					<tr>
						<td class="_detail__table__left">定休日</td>
						<td class="_detail__table__right">定休日が入ります</td>
					</tr>
					<tr>
						<td class="_detail__table__left">客層</td>
						<td class="_detail__table__right">20代 / 30代</td>
					</tr>
					<tr>
						<td class="_detail__table__left">よくあるご質問</td>
						<td class="_detail__table__right">
							<dl class="_detail__table__right__content">
								<dt class="_detail__table__right__content__ttl">職場の雰囲気について</dt>
								<dd class="_detail__table__right__content__data">職場の雰囲気についてが入ります。職場の雰囲気についてが入ります。職場の雰囲気についてが入ります。職場の雰囲気についてが入ります。</dd>
								<dt class="_detail__table__right__content__ttl">スキルアップや教育制度について</dt>
								<dd class="_detail__table__right__content__data">スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。スキルアップや教育制度についてが入ります。</dd>
								<dt class="_detail__table__right__content__ttl">仕事のやりがいについて</dt>
								<dd class="_detail__table__right__content__data">仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。仕事のやりがいについてが入ります。</dd>
							</dl>
						</td>
					</tr>
					<tr>
						<td class="_detail__table__left">スタッフ数や男女比</td>
						<td class="_detail__table__right">スタッフ数や男女比が入ります</td>
					</tr>

					*/?>

                    </tbody>
                </table>
                <h3 data-target-tabinner="target" class="_secret__detail__table__heading is-disable u-mt20">企業情報</h3>
                <table data-target-tabinner="target" class="_secret__detail__table u-fs--sh--recruit is-disable">
                    <tbody>
                    @if($company_name)
                        <tr>
                            <td class="_detail__table__left">企業名</td>
                            <td class="_detail__table__right">{{$company_name}}</td>
                        </tr>
                    @endif

                    <?php /*
					<tr>
						<td class="_detail__table__left">代表者名</td>
						<td class="_detail__table__right">代表者名が入ります</td>
					</tr>
					<tr>
						<td class="_detail__table__left">事業内容</td>
						<td class="_detail__table__right">事業内容が入ります</td>
					</tr>
					<tr>
						<td class="_detail__table__left">本社所在地</td>
						<td class="_detail__table__right">本社所在地が入ります</td>
					</tr>
					<tr>
						<td class="_detail__table__left">HP</td>
						<td class="_detail__table__right">http://xxx.xxx</td>
					</tr>

					*/ ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <?php
    $alliances = _helper::getAllienceJobBookmark();
    $alliance_jobs = _modules::get_related_allliance_by_company_or_shop($company_name, $detail['shop_id'], $detail['id']);
    $allianceUrl = _helper::getUrlName('alliancePage');
    ?>

    @if($alliances)
        <div class="contents-box--recruit__related u-mt30">
            <h3 class="-recruit__related__heading">あなたに似た人はこちらの求人を<br>キープしています</h3>
            <div class="column-box__secret__slider u-mt15">
                <div data-target-slider="target" class="_secret__slider__main">
                   @foreach($alliances as $record)
                   <div class="_slider__main__img">
                        <img data-lazy="{{$record['job_image']}}" alt="">
                   		<div class="_secret__slider__caption">
                   		<a href="/{{$allianceUrl}}/{{$record['id']}}">{{$record['job_name']}}</a>
                   		</div>
                   </div>
                   @endforeach 
                </div>
                
            </div>
        </div>
    @endif

    @if($alliance_jobs)
        <div class="contents-box--recruit__related u-mt30">
            <h3 class="-recruit__related__heading">この企業・店舗が募集している<br>ほかの求人</h3>
            <div class="column-box__secret__slider u-mt15">
                <div data-target-slider="target" class="_secret__slider__main">
                   	@foreach($alliance_jobs as $record)
					<div class="_slider__main__img">
						<img data-lazy="{{$record['job_image']}}" alt="">
						<div class="_secret__slider__caption">
							<a href="/{{$allianceUrl}}/{{$record['id']}}">
								{{$record['job_name']}}
							</a>
						</div>
					</div>
                   	@endforeach 
                </div>
            </div>
        </div>
    @endif

    <div class="column-box__secret__btnBLock">
        <div data-id="{{$detail['id']}}" data-secret="2"
             class="c-button-square--l--gray u-fs--xxl u-fs--xs bookmark_job">
            <span class="c-font-icon--attachment"></span>キープする
        </div>
        
        <div class="c-button-square--l u-fs--xxl u-fs--xs"><a target="_blank"  
        onClick="ga('send','event','link-jobnote','click','{{$urlNoKirei}}');"
        href="{{$detail['url']}}">応募する</a>
        </div>
    </div>
@stop