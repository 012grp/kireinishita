<?php
$class_primary_box = 'u-mt20';
$web_title = _helper::set_title(trans('webinfo.title_secret_job'));
$description = trans('webinfo.meta_desc_secret_job');
$keywords = trans('webinfo.keywords_secret_job');
/*
    If exist bookmarkJob session and logged-in
    bookmark this session
    then remove bookmarkJob session
*/
if (Session::has('bookmarkJob')) {
    if (Session::has('user')) {
        $job_id = Session::get('bookmarkJob.id');
        $is_secret_job = Session::get('bookmarkJob.is_secret_job');
        Session::forget('bookmarkJob');

        _helper::bookmark_job_after_login($job_id, $is_secret_job);
    }
}

?>
@extends('layouts.default')

@section('column_right')
    @include('shared.right_column.default')
@stop

@section('breadcrumb')
    @include('breadcrumb.default', array(
                            'page' 	=> 'オススメ求人'
                        ))
@stop


@section('column_left')
    <div data-target-leftcolumn="target" class="column-left">
        <div class="column-left__heading--normal">
            <h1 class="column-left__ttl--normal u-fs--xxxl--contents"><span
                        class="column-left__ttl__inner">オススメ求人</span></h1>
            <p class="column-left__ttl__right--border u-fs--sh">特におすすめしたい“キレイ”にまつわる限定求人情報をまとめてご紹介！</p>
        </div>

        <?php $contents = $secret_jobs; ?>
        @include('job._search_info')

        <div class="column-box--wide">
            <?php $index = 0; ?>
            @foreach($secret_jobs as $secret_job)
                <?php
                if ($index != 0) {
                    $secret_job['job_class'] = 'u-mt30';
                }
                $secret_job['is_secret'] = true;
                $secret_job['url_target'] = '';
                $index++;
                ?>
                @include('alliance._search_item', $secret_job)
            @endforeach
        </div>
        @include('job._search_info', ['class' => 'u-mt30'])
    </div>
@stop
		
