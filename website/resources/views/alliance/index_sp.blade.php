<?php
$web_title = _helper::set_title(trans('webinfo.title_secret_job'));
$description = trans('webinfo.meta_desc_secret_job');
$keywords = trans('webinfo.keywords_secret_job');
/*
	If exist bookmarkJob session and logged-in
	bookmark this session
	then remove bookmarkJob session
*/
if(Session::has('bookmarkJob'))
{
	if(Session::has('user'))
	{
		$job_id = Session::get('bookmarkJob.id');
		$is_secret_job = Session::get('bookmarkJob.is_secret_job');
		Session::forget('bookmarkJob');

		_helper::bookmark_job_after_login($job_id, $is_secret_job);
	}
}

if($secret_jobs)
{
	$pageCount = $secret_jobs->lastPage();
	$currentPage = $secret_jobs->currentPage();
	$url_next = $secret_jobs->url($currentPage+1);
	$url_prev = $secret_jobs->url($currentPage-1);

}
else
{
	$pageCount = 1;
	$currentPage = 1;
	$url_next = '';
	$url_prev = '';
}
?>

@extends('layouts.default_sp')

@section('under_header')
    @include('shared_sp.item.alliance_under_header')
@stop


@section('page_sp')
    <div class="column-box--gray">
       	@foreach($secret_jobs as $index=>$secret_job)
			<?php
				$secret_job['is_secret'] = true;
				$secret_job['url_target'] = '';
				$secret_job['index'] = $index; 
			?>
			@include('alliance._search_item_sp', $secret_job)
		@endforeach  
        @include('shared_sp.item.pager', array(
			'pageCount' 	=> $pageCount,
			'currentPage' 	=> $currentPage,
			'url_next' 	=> $url_next,
			'url_prev' 	=> $url_prev,
		))
    </div>
@stop