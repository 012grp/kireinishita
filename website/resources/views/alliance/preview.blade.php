<?php
	$class_primary_box = 'u-mt20';
	$company_name = strip_tags($detail['name']);
	$job_name = strip_tags($detail['job_name']);
	$job_salary = strip_tags($detail['job_salary']);
	$job_location = strip_tags($detail['job_location']);
	//$job_career = strip_tags($detail['job_career']);
	$celebration = strip_tags($detail['celebration']);
	//$job_image = $detail['job_image'];

	//$employment_status = strip_tags($detail['employment_status']);
	$job_description = strip_tags($detail['job_description']);
	$job_notice_header = strip_tags($detail['job_notice_header']);
	$job_notice_content = strip_tags($detail['job_notice_content']);

	if(!empty($detail['job_notice_image']))
	{
		$job_notice_image = strip_tags($detail['job_notice_image']);
	}
	
	//$job_salary_detail = $detail['job_salary_detail'];
	$content = $detail['content'];
	$job_qualification = $detail['job_qualification'];
	//$job_location_detail = $detail['job_location_detail'];
	$treatment_welfare = $detail['treatment_welfare'];
	$job_holiday = $detail['job_holiday'];
	$job_crated_at = $detail['job_created_at'];



	$list_hangup = _helper::getCategoryNamesByListId('',$detail['list_hangup']);

	$list_category = _helper::getJobCategoryNameByListId($detail['list_job_parent'],$detail['list_job']);

	$list_em = _helper::getEmploymentNamesByListId($detail['list_em']);

	$access = $detail['access'];

	$company_category = strip_tags($detail['category']);
	$company_address = strip_tags($detail['address']);
	$company_average_price = strip_tags($detail['average_price']);
	$company_seat_number = strip_tags($detail['seat_number']);
	$company_holiday = strip_tags($detail['holiday']);
	$company_access = strip_tags($detail['company_access']);

	if(empty($job_notice_image))
	{
		$job_notice_image = _common::noImageUrl();
	}

	$key = Config::get('webconfig.google_map_api_key');

	//social
	$request_url = Request::url();
	$title = _helper::set_title($job_name);

	//SEO
	$web_title = _helper::set_title(trans('webinfo.title_secret_job'));
	$description = isset($detail['meta_desc']) ? $detail['meta_desc'] : trans('webinfo.meta_desc_secret_job');
	if($description == "")
	{
		$description = null;
	}
	//$keywords = _common::groupConcat(',', array($list_hangup,$list_category,$list_em));
	$keywords = $detail['meta_keyword'];
	if(!empty($job_image))
	{
		$image = $job_image;
	}

	//TODO: set company name length to show
	$google_map_company_name = $company_name;
	if(mb_strlen($google_map_company_name) > 17)
	{
		$google_map_company_name = mb_substr($google_map_company_name,0,17,'utf-8').'...';
	}

	$secretUrl = _helper::getUrlName('secretPage');
?>

@section('css')
<style type="text/css">
	._secret__detail__table.is-disable{
		display: block !important;
		visibility: hidden;
		height: 0px;
	}
</style>
@stop
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.default')
@stop

@section('js')
	<script src="/public/js/init-googlemap.js"></script>
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) {
			// set google map
			// input lat, lng , inforwindow, zoom
			var job_lat = <?php echo !empty($detail['lat']) ? $detail['lat'] : 'null'; ?>;
			var job_lng = <?php echo !empty($detail['lng']) ? $detail['lng'] : 'null'; ?>;
			if(job_lat && job_lng)
			{
				var contentString = "";
				//check if exists company name
				<?php if($google_map_company_name): ?>
					contentString += "<h4 style='font-weight: bold; color: black;'><?php echo $google_map_company_name; ?></h4>";
				<?php endif; ?>

				//check if exists job location
				<?php if($job_location): ?>
					contentString += "<p><?php echo $job_location; ?></p>";
				<?php endif; ?>
				
				initMap( job_lat, job_lng, contentString, 14, 'job_map');
			}

			var company_lat = <?php echo !empty($detail['company_lat']) ? $detail['company_lat'] : 'null' ; ?>;
			var company_lng = <?php echo !empty($detail['company_lng']) ? $detail['company_lng'] : 'null' ; ?>;
			if(company_lat && company_lng)
			{
				var contentString = "";
				//check if exists company name
				<?php if($google_map_company_name): ?>
					contentString += "<h4 style='font-weight: bold; color: black;'><?php echo $google_map_company_name; ?></h4>";
				<?php endif; ?>
				//check if exists company address
				<?php if($detail['address']): ?>
					contentString += "<p><?php echo $detail['address']; ?></p>";
				<?php endif; ?>
				
				initMap( company_lat, company_lng, contentString, 14, 'company_map');
			}
		});
	</script>
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> $company_name .'の求人',
							'parent'	=> array(
									array('link' => '/'. $secretUrl .'/', 'title' => '限定求人'),
								)
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<article class="column-box__secret">
			<div class="column-box__secret__heading">
				<div class="_secret__heading__left u-fs--xs--recruit u-fwb">{{ $company_name }}</div>
				<div class="_secret__heading__right"><span class="u-fs--xxs c-tag--secret--flow">限定</span>
				@if($job_crated_at > (new DateTime('-1 day'))->format('Y-m-d H:i:s'))
					<span class="u-fs--xxs c-tag--new--flow">NEW</span>
				@endif
				@if(!empty($celebration))
				<span class="u-fs--xxs c-tag--celebration">{{ $celebration }}</span>
				@endif
				</div>
				<h1 class="_secret__heading__ttl u-fs--xxlh--contents u-fwb">{{ $job_name }}</h1>
			</div>
			@if($slider_image)
				<div class="column-box__secret__slider u-mt15">
					<div data-target-slider="target" class="_secret__slider__main">
						@foreach($slider_image as $item)
							<div class="_slider__main__img"><img src="{{ $item }}" alt=""></div>
						@endforeach
					</div>
					<div class="_secret__slider-nav">
						<div data-trigger-slider="trigger" class="slider-nav">
							@foreach($slider_image as $key => $item)
								@if($key == 0)
									<div class="thumbnail__img"><img src="{{ $item }}" alt=""></div>
								@else
									<div class="thumbnail__img u-mt10"><img src="{{ $item }}" alt=""></div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
			@endif
			<div class="_secret__tag u-mt10">
				<?php $list_hangup = empty($list_hangup)?array():explode(',',$list_hangup); ?>
				@foreach($list_hangup as $item)
					<span class="c-tag--secret--detail u-fs--xs">{{ $item }}</span>
				@endforeach
			</div>
			<div class="_secret__text__block u-fs--lh--contents">
			{!! $content !!}
			</div>
			@if($job_notice_header)
				<div class="_secret__notice u-mt25">
					<div style="background-image:url({{ $job_notice_image }})" class="_secret__notice__img"></div>
					<div class="_secret__notice__right"><span class="c-tag--secret--notice u-fs--xs">注目ポイント！</span>
						<div class="_secret__notice__ttl u-fwb u-fs--sh--secret u-mt10">{{ $job_notice_header }}</div>
						<div class="_secret__notice__text u-fs--xs--recruit u-mt5">
							{{ $job_notice_content }}
						</div>
					</div>
				</div>
			@endif

			<div class="_secret__social__block u-mt30">
				<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $request_url }}&amp;src=sdkpreparse" class="c-button-square--social c-facebook">
					<i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i><span class="u-fs--l">シェア</span>
				</a>
				<a href="https://twitter.com/intent/tweet?url={{ $request_url }}&amp;text={{ $title }}" class="c-button-square--social c-twitter">
					<i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i><span class="u-fs--l">ツイート</span>
				</a>
			</div>
		</article>
		<div class="column-box__secret--table">
			<ul class="_secret__detail__tab">
				<li data-trigger-tab="0" class="_detail__tab__inner is-active u-fs--lh--contents">募集情報</li>
				<li data-trigger-tab="1" class="_detail__tab__inner u-fs--lh--contents">企業情報・店舗情報</li>
			</ul>
			<div class="_secret__detail--recruit">
				<table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit" >
					<tbody>
						<tr>
							<td class="_detail__table__left u-fwb">職種</td>
							<td class="_detail__table__right">
								@if($list_category)
									<?php _helper::explode_character($list_category); ?>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">雇用形態</td>
							<td class="_detail__table__right">
								<p class="_detail__table__text">
								@if($list_em)
									<?php _helper::explode_character($list_em); ?>
								@endif
								</p>
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">仕事内容</td>
							<td class="_detail__table__right">
								<p class="_detail__table__text"></p>{{$job_description}}
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">給与</td>
							<td class="_detail__table__right">
								@if($job_salary)
									<?php _helper::explode_ul($job_salary, '', ''); ?>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">応募資格</td>
							<td class="_detail__table__right">
								<p class="_detail__table__text">
									@if($list_em)
										<?php _helper::explode_character($list_em); ?>
									@endif
								</p>

								@if($job_qualification)
									<?php _helper::explode_ul($job_qualification, 'u-mt10'); ?>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">勤務地</td>
							<td class="_detail__table__right">{{$job_location}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">アクセス</td>
							<td class="_detail__table__right">
								@if($access)
									<?php _helper::explode_ul($access); ?>
								@endif
								@if(!empty($detail['lat']) && !empty($detail['lng']) )
								<div class="_detail__table__map u-mt10" id="job_map"></div>
								<a target="_blank" href="https://www.google.com/maps/place/{{$job_location}},+Japan/{{'@'}}{{ $detail['lat'] or ''}},{{$detail['lng'] or ''}},17z?hl=ja-JP" id="url_job_map">
								GoogleMapで確認する</a>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">待遇・福利厚生</td>
							<td class="_detail__table__right">
								@if($treatment_welfare)
									<?php _helper::explode_ul($treatment_welfare); ?>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">休日休暇</td>
							<td class="_detail__table__right">
								@if($job_holiday)
									<?php _helper::explode_ul($job_holiday); ?>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
				<table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit is-disable">
					<tbody>
						<tr>
							<td class="_detail__table__left u-fwb">ジャンル</td>
							<td class="_detail__table__right">{{$company_category}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">住所</td>
							<td class="_detail__table__right">{{$company_address}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">アクセス</td>
							<td class="_detail__table__right">
								@if($company_access)
									<?php _helper::explode_ul($company_access); ?>
								@endif

								@if(!empty($detail['company_lat']) && !empty($detail['company_lng']))
								<div class="_detail__table__map u-mt10" id="company_map" ></div>
								<a target="_blank" href="https://www.google.com/maps/place/{{$company_name}},+Japan/{{'@'}}{{ $detail['company_lat'] or ''}},{{$detail['company_lng'] or ''}},17z?hl=ja-JP" id="url_company_map">
								GoogleMapで確認する</a>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">客単価</td>
							<td class="_detail__table__right">{{$company_average_price}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">席数</td>
							<td class="_detail__table__right">{{$company_seat_number}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">定休日</td>
							<td class="_detail__table__right">{{$company_holiday}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">HP</td>
							<td class="_detail__table__right">
								{!!_helper::explode_hp($detail['hp'])!!}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="column-box__secret__btnBLock">
				<div data-id="0" data-secret="1" class="c-button-square--l--gray u-fs--xxl u-fwb bookmark_job"><span class="c-font-icon--attachment"></span>キープする</div>
				<div class="c-button-square--l u-fs--xxl u-fwb"><a href="#">応募する</a></div>
			</div>
		</div>
	</div>
@stop
