@extends('admin.layouts.login')
@section('content')
<div class="container">
    <div class="login">
        {!! Form::open(array('url' => '/admin/login', 'method' => 'post')) !!}
            <div class="form-group">
                <label for="exampleInputEmail1">User name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="User name">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember me
                </label>
            </div>
            @if($errors->any())
            <div class="alert-danger" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Error:</span>{{$errors->first()}}
            </div>
            @endif
            <div class="text-right">
            <button type="submit" class="btn btn-default">Submit</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@stop