<?php
/*
 array(
  'page' => STRING_T,
  'parent' => array(
   array('link' => '/', 'title' => STRING_T),
   array('link' => '/', 'title' => STRING_T),
   array('link' => '/', 'title' => STRING_T)
  )
 )
*/
?>

<div class="breadcrumb u-fs--xxs">
    <a href="/"><span>キレイにスルンダ</span></a>
    <?php if (isset($parent)): ?>
    <?php foreach($parent as $item):?>
    <span class="breadcrumb__arrow__text u-arrow__text">&gt;</span>
    <a href="{{ $item['link'] }}"><span>{{ $item['title'] }}</span></a>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php if (isset($page)): ?>
    <span class="breadcrumb__arrow__text u-arrow__text">&gt;
 		<span title="{{_helper::set_title($page)}}">{{ $page }}</span>
    </span>
    <?php endif; ?>
</div>