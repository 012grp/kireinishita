<?php
	$__title =  trans('webinfo.title_content_community');
	$web_title = _helper::set_title($__title);
	$description = trans('webinfo.meta_desc_content_community');
	$keywords = trans('webinfo.keywords_content_community');
?> 
@extends('layouts.errors')


@section('content')
	<div data-return="target" class="primary-box--full__inner">
		<div class="primary-box--full__content -full__content--high">
			<h2>Comming<br>Soon</h2>
			<p class="u-fs--xlh u-mt2--per">キレイにスルンダコミュニティは“しゃべるんだ”は、<br>飲食業界で働く人が自由にお話しできるコミュニティコンテンツです。<br>会員登録すれば、誰でも気の合う仲間でコミュニティを作成することも可能です。<br>同じ業界で働く皆さんが集まり情報共有や共通の悩み相談ができる、<br>働く側のためのコミュニティです。
			</p>
			<div class="c-button-square--l u-fs--xxl u-fwb u-mt2--per"><a href="/">前に戻るんだ</a></div>
		</div>
	</div>
@stop