<?php
	$class_primary_box = 'u-mt20';
	$company_name = strip_tags($detail['name']);
	$celebration = strip_tags($detail['celebration']);
	$company_category = strip_tags($detail['category']);
	$company_address = strip_tags($detail['address']);
	$company_average_price = strip_tags($detail['average_price']);
	$company_seat_number = strip_tags($detail['seat_number']);
	$company_holiday = strip_tags($detail['holiday']);
	$company_access = strip_tags($detail['access']);

	
	$key = Config::get('webconfig.google_map_api_key');

	//TODO: set company name length to show
	$google_map_company_name = $company_name;
	if(mb_strlen($google_map_company_name) > 17)
	{
		$google_map_company_name = mb_substr($google_map_company_name,0,17,'utf-8').'...';
	}
?>

@section('css')
<style type="text/css">
	._secret__detail__table.is-disable{
		display: block !important;
		visibility: hidden;
		height: 0px;
	}
</style>
@stop
@extends('layouts.default')

@section('column_right')
	@include('shared.right_column.default')
@stop

@section('js')
	<script src="/public/js/init-googlemap.js"></script>
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) {
			// set google map
			// input lat, lng , inforwindow, zoom
			var company_lat = <?php echo !empty($detail['company_lat']) ? $detail['company_lat'] : 'null' ; ?>;
			var company_lng = <?php echo !empty($detail['company_lng']) ? $detail['company_lng'] : 'null' ; ?>;
			if(company_lat && company_lng)
			{
				var contentString = "";
				//check if exists company name
				<?php if($google_map_company_name): ?>
					contentString += "<h4 style='font-weight: bold; color: black;'><?php echo $google_map_company_name; ?></h4>";
				<?php endif; ?>
				//check if exists company address
				<?php if($detail['address']): ?>
					contentString += "<p><?php echo $detail['address']; ?></p>";
				<?php endif; ?>
				
				initMap( company_lat, company_lng, contentString, 14, 'company_map');
			}
		});
	</script>
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<article class="column-box__secret">
			<div class="column-box__secret__heading">
				<div class="_secret__heading__left u-fs--xs--recruit u-fwb">{{ $company_name }}</div>
				<div class="_secret__heading__right"><span class="u-fs--xxs c-tag--secret--flow">限定</span>
					@if(!empty($celebration))
					<span class="u-fs--xxs c-tag--celebration">{{ $celebration }}</span>
					@endif
				</div>
			</div>
			
		</article>
		<div class="column-box__secret--table">
			<ul class="_secret__detail__tab">
				<li data-trigger-tab="1" class="_detail__tab__inner u-fs--lh--contents">店舗情報</li>
			</ul>
			<div class="_secret__detail--recruit">
				<table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit" >
					<tbody>
						<tr>
							<td class="_detail__table__left u-fwb">ジャンル</td>
							<td class="_detail__table__right">{{$company_category}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">住所</td>
							<td class="_detail__table__right">{{$company_address}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">アクセス</td>
							<td class="_detail__table__right">
								@if($company_access)
									<?php _helper::explode_ul($company_access); ?>
								@endif

								@if(!empty($detail['company_lat']) && !empty($detail['company_lng']))
								<div class="_detail__table__map u-mt10" id="company_map" ></div>
								<a target="_blank" href="https://www.google.com/maps/place/{{$company_name}},+Japan/{{'@'}}{{ $detail['company_lat'] or ''}},{{$detail['company_lng'] or ''}},17z?hl=ja-JP" id="url_company_map">
								GoogleMapで確認する</a>
								@endif
							</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">客単価</td>
							<td class="_detail__table__right">{{$company_average_price}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">席数</td>
							<td class="_detail__table__right">{{$company_seat_number}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">定休日</td>
							<td class="_detail__table__right">{{$company_holiday}}</td>
						</tr>
						<tr>
							<td class="_detail__table__left u-fwb">HP</td>
							<td class="_detail__table__right">
								{!!_helper::explode_hp($detail['hp'])!!}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop
