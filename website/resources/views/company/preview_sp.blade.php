<?php
$company_name = strip_tags($detail['name']);

$celebration = strip_tags($detail['celebration']);


$company_category = strip_tags($detail['category']);
$company_address = strip_tags($detail['address']);

$key = Config::get('webconfig.google_map_api_key');

//TODO: set company name length to show
$google_map_company_name = $company_name;
if(mb_strlen($google_map_company_name) > 17)
{
	$google_map_company_name = mb_substr($google_map_company_name,0,17,'utf-8').'...';
}

//$HP = _helper::getRelatedSecretJobs($detail['id'], $detail['company_id']);


?>

@extends('layouts.default_sp')

@section('css')
<style type="text/css">
	._secret__detail__table.is-disable{
		display: block !important;
		overflow: hidden;
	}
</style>
@stop

@section('js')
	<script src="/public/js/init-googlemap.js"></script>
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) { 
			// set google map
			// input lat, lng , inforwindow, zoom
			
            var company_lat = <?php echo !empty($detail['company_lat']) ? $detail['company_lat'] : 'null' ; ?>;
            var company_lng = <?php echo !empty($detail['company_lng']) ? $detail['company_lng'] : 'null' ; ?>;
            if(company_lat && company_lng)
            {
                var contentString = "";
                //check if exists company name
                <?php if($google_map_company_name): ?>
                    contentString += "<h4 style='font-weight: bold; color: black;'><?php echo $google_map_company_name; ?></h4>";
                <?php endif; ?>
                //check if exists company address
                <?php if($detail['address']): ?>
                    contentString += "<p><?php echo $detail['address']; ?></p>";
                <?php endif; ?>
                
                initMap( company_lat, company_lng, contentString, 14, 'company_map');
            }
		});

	</script>
	<script src="/public/sp/js/bookmark_job.js"></script>
@stop

@section('page_sp')
    <div class="primary-box--noPadding">
        <div class="contents-box--recruit__top">
            <h1 class="-recruit__top__ttl u-fwb u-fs--xs">{{ $company_name }}</h1>
            
            <div class="-recruit__top__inner">
                <div class="-wide__list__tagBlock u-mt20">
                    <div class="-wide__list__tagBlock--l"><span class="u-fs--xxs c-tag--secret--flow">限定</span>
                       @if(!empty($celebration))
                       <span class="u-fs--xxs c-tag--celebration">{{ $celebration }}</span>
                       @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="column-box__secret--table u-mt20">
            <ul class="_secret__detail__tab">
                <li data-trigger-tab="0" class="_detail__tab__inner is-active u-fs--xs">店舗情報</li>
            </ul>
            <div class="_secret__detail--recruit">
                @include('shared_sp.item.secret_kyujin_store_information')
            </div>
        </div>
    </div>
@stop