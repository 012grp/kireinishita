<?php
$class_primary_box = 'u-mt20';
$__title = _helper::getWebInfo('title_concept');
$web_title = _helper::set_title($__title);

$description = trans('webinfo.meta_desc_concept');
$keywords = trans('webinfo.keywords_concept');
?>

@extends('layouts.default')

@section('column_right')
    @include('shared.right_column.no_content')
@stop

@section('breadcrumb')
    @include('breadcrumb.default', array(
                            'page' 	=> $__title
                        ))
@stop


@section('column_left')
    <div data-target-leftcolumn="target" class="column-left column-left--bg">
        <article class="column-box__contents">
            <div class="column-box__contents__topImg"><img src="/public/img/concept/main.jpg"
                                                           alt="もっと多くの人たちに“食”の業界で働く魅力を伝えたい！"></div>
            <div class="_contents__detail__block">
                <p class="column-box_contents__text u-fs--lh--contents u-mt25">
                    美容業界は、いつも人手が不足しています。 そんな美容業界の人手不足問題を解消するため、もっともっと多くの人たちに美容業界で働く楽しさを知ってほしいと思っています。
                </p>
                <p class="column-box_contents__text u-fs--lh--contents u-mt25">
                    “キレイにスルンダ”を通じて、美容業界の魅力を、美容に関わる人たちだけでなく、“キレイ”になることが好きという一般の人たちにも、 情報発信して行きたいと考えています。
                </p>
            </div>
            <div class="_contents__detail__block--flex u-mt40">
                <div class="_contents__detail__l"><img src="/public/img/concept/concept-01.jpg" alt=""></div>
                <div class="_contents__detail__r">
                    <h2 title="{{$web_title}}" class="_contents__ttl--concept u-fs--xxl u-fwb">キレイにスルンダとは？</h2>
                    <p class="column-box_contents__text u-fs--lh--contents u-mt15">
                        キレイにスルンダは、“キレイ”に関わる求人と情報コンテンツがひとつにまとまったキュレーションサイトです。</p>
                </div>
            </div>
            <div class="_contents__detail__block--flex">
                <div class="_contents__detail__l">
                    <h2 class="_contents__ttl--concept u-fs--xxl u-fwb">求人掲載件数は、<span class="u-fs--xxl--contents">約45,000件</span>
                    </h2>
                    <p class="column-box_contents__text u-fs--lh--contents u-mt15">
                        “キレイ”に関わる21職種を美容・リラクゼーション・治療の３つのカテゴリに分類し、あなたの気になるお仕事を素早く探せます。</p>
                </div>
                <div class="_contents__detail__r"><img src="/public/img/concept/concept-02.jpg" alt=""></div>
            </div>
            <div class="_contents__detail__block--flex">
                <div class="_contents__detail__l"><img src="/public/img/concept/concept-03.jpg" alt=""></div>
                <div class="_contents__detail__r">
                    <h2 class="_contents__ttl--concept u-fs--xxl u-fwb">“キレイ”にまつわる情報コンテンツ</h2>
                    <p class="column-box_contents__text u-fs--lh--contents u-mt15">
                        就職・転職に役立つ情報をはじめ、“キレイ”にまつわるトレンドや業界人インタビューなど、お役立ちコンテンツを発信。</p>
                </div>
            </div>
        </article>
    </div>
@stop
