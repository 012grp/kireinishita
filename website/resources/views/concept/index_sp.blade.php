<?php
$__title = _helper::getWebInfo('title_concept');
$web_title = _helper::set_title($__title);

$description = trans('webinfo.meta_desc_concept');
$keywords = trans('webinfo.keywords_concept');
?>
@extends('layouts.default_sp')

@section('page_sp')
    <div class="primary-box--noPadding">
        <div data-target-maincolumn="target" class="primary-box__inner">
            <article>
                <div class="contents-box__contents__topImg"><img src="/public/img/concept/main.jpg"
                                                                 alt="もっと多くの人たちに“食”の業界で働く魅力を伝えたい！"></div>
                <div class="contents-box">
                    <div class="_contents__detail__block">
                        <p class="contents-box__text u-fs--mh u-mt20">
                            美容業界は、いつも人手が不足しています。 そんな美容業界の人手不足問題を解消するため、もっともっと多くの人たちに美容業界で働く楽しさを知ってほしいと思っています。
                        </p>
                        <p class="contents-box__text u-fs--mh u-mt20">
                            “キレイにスルンダ”を通じて、美容業界の魅力を、美容に関わる人たちだけでなく、“キレイ”になることが好きという一般の人たちにも、 情報発信して行きたいと考えています。
                        </p>
                    </div>
                    <div class="_contents__detail__block--flex u-mt20">
                        <div class="_contents__detail__l"><img src="/public/img/concept/concept-01.jpg" alt=""></div>
                        <div class="_contents__detail__r">
                            <h2 class="_contents__ttl--concept u-fs--xlh u-fwb">キレイにスルンダとは？</h2>
                            <p class="contents-box__text u-fs--sh--contents u-mt15">
                                キレイにスルンダは、“キレイ”に関わる求人と情報コンテンツがひとつにまとまったキュレーションサイトです。</p>
                        </div>
                    </div>
                    <div class="_contents__detail__block--flex">
                        <div class="_contents__detail__l"><img src="/public/img/concept/concept-02.jpg" alt=""></div>
                        <div class="_contents__detail__r">
                            <h2 class="_contents__ttl--concept u-fs--xlh u-fwb">求人掲載件数は、<span class="u-fs--xl u-mt5">約45,000件</span>
                            </h2>
                            <p class="contents-box__text u-fs--sh--contents u-mt15">
                                “キレイ”に関わる21職種を美容・リラクゼーション・治療の３つのカテゴリに分類し、あなたの気になるお仕事を素早く探せます。</p>
                        </div>
                    </div>
                    <div class="_contents__detail__block--flex">
                        <div class="_contents__detail__l"><img src="/public/img/concept/concept-03.jpg" alt=""></div>
                        <div class="_contents__detail__r">
                            <h2 class="_contents__ttl--concept u-fs--xlh u-fwb">“キレイ”にまつわる情報コンテンツ</h2>
                            <p class="contents-box__text u-fs--sh--contents u-mt15">
                                就職・転職に役立つ情報をはじめ、“キレイ”にまつわるトレンドや業界人インタビューなど、お役立ちコンテンツを発信。</p>
                        </div>
                    </div>
                    <a href="/contact" class="c-button-square--m u-fs--l u-fwb u-mt20"><span class="button-inner">掲載に関するお問い合わせはコチラ</span></a>
                </div>
            </article>
        </div>
    </div>
@stop