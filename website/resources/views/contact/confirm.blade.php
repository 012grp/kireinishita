<?php
	$class_primary_box = 'u-mt20';
	$__title = _helper::getWebInfo('title_contact_confirm');
	$web_title = _helper::set_title($__title);

	$company = null;
	if (Session::has('contact'))
	{
		$contact = Session::get('contact');
		$kind_id = $contact['kind_id'];
		$company = $contact['company'];
		$name = $contact['name'];
		$mail = $contact['mail'];
		$content = $contact['content'];
	}
?>
@extends('layouts.default')

@section('column_right')
	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 	=> 'お問い合わせ'
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<h1 class="column-left__ttl--normal u-fs--xxl--contents">お問い合わせ</h1>
		<div class="form--primary u-mt10">
			<p class="form--primary__repletion u-fs--xs--lh--form">
				お問い合わせ内容に関しては、編集部にて確認し対応を行っておりますが、
				ご返信にお時間をいただく場合がございます。また、お問い合せの内容によっては対応できない場合もございますので、
				ご了承ください。
			</p>
			<form action="/contact/thanks" method="post">
				<table class="form--primary__table u-mt15">
					@if($kind_id == 2)
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								種
								&nbsp;
								別<span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								<div><span class="u-fs--l">法人 &nbsp;</span><span class="u-fs--l">{{ $company or '' }}</span></div>
							</td>
						</tr>
					@endif
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">お名前<span class="c-tag--form--required u-fs--xxs">必須</span></td>
                	<td class="form--primary__detail">
							<div><span class="u-fs--l">{{ $name or '' }}</span></div>
						</td>
					</tr>
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">メールアドレス<span class="c-tag--form--required u-fs--xxs">必須</span></td>
						<td class="form--primary__detail"><span class="u-fs--l">{{ $mail or '' }}</span></td>
					</tr>
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">お問い合わせ内容<span class="c-tag--form--required u-fs--xxs">必須</span></td>
						<td class="form--primary__detail">
							<div>
								<span class="u-fs--lh--contents">{!! nl2br(e($content)) !!}</span>
							</div>
						</td>
					</tr>
				</table>
				<div class="-contact__button__block">
					<a class="c-button-square--l--gray u-fs--xxl u-fwb" href="/contact">修正する</a>
					<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xxl u-fwb"><span class="button-inner">送信する</span></button>
				</div>
				<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			</form>
		</div>
	</div>
@stop
		
			