<?php
	$__title = _helper::getWebInfo('title_contact_confirm');
	$web_title = _helper::set_title($__title);

	if (Session::has('contact'))
	{
		$contact = Session::get('contact');
		$kind_id = $contact['kind_id'];
		$kind_name = $contact['kind_name'];
		$company = $contact['company'];
		$name = $contact['name'];
		$mail = $contact['mail'];
		$content = $contact['content'];
	}
?>

@extends('layouts.default_sp')


@section('page_sp')
<div class="primary-box--noPadding">
	<h1 class="primary-box__ttl--back u-fwb u-fs--m">お問い合わせ</h1>
	<div class="form--primary form--primary--contact">
		<form action="/contact/thanks" method="post">
			<table class="form--primary__table">
				@if($kind_id == 2)
				<tr class="-contact__table__row -contact__table__row--noborder">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">
						種
						&nbsp;
						別<span class="c-tag--form--required u-fs--xxs">必須</span>
					</td>
					<td class="form--primary__detail u-mt15">
						<div class="form--primary__detail--multi"><span class="u-fs--l">{{ $kind_name or '' }}</span>&nbsp;&nbsp;<span class="u-fs--l">{{ $company or '' }}</span></div>
					</td>
				</tr>
				@endif
				<tr class="-contact__table__row u-mt20">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">お名前<span class="c-tag--form--required u-fs--xxs">必須</span></td>
					<td class="form--primary__detail u-mt15">
						<div><span class="u-fs--l">{{ $name or '' }}</span></div>
					</td>
				</tr>
				<tr class="-contact__table__row u-mt20">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">メールアドレス<span class="c-tag--form--required u-fs--xxs">必須</span></td>
					<td class="form--primary__detail u-mt15"><span class="u-fs--l">{{ $mail or '' }}</span></td>
				</tr>
				<tr class="-contact__table__row u-mt20">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">お問い合わせ内容<span class="c-tag--form--required u-fs--xxs">必須</span></td>
					<td class="form--primary__detail u-mt15">
						<div>
							<span class="u-fs--sh">{!! nl2br(e($content)) !!}</span></span>
						</div>
					</td>
				</tr>
			</table>
			<div class="form__button__block--multi u-mt30">
				<div class="_block--multi__inner u-mt10"><a href="/contact" class="c-button-square--l--gray u-fs--xs u-fwb" >修正する</a>
					<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">送信する</span></button>
				</div>
			</div>
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		</form>	
	</div>
	</div>
	@stop