<?php
	$class_primary_box = 'u-mt20';
	$__title = _helper::getWebInfo('title_contact');
	$web_title = _helper::set_title($__title);

	$description = trans('webinfo.meta_desc_contact');
	$keywords = trans('webinfo.keywords_contact');
	
	$styleDisplay = 'style="display: none;"';
	$required = '';
	$checked_user = 'checked="checked"';
	$checked_company = '';


	if (Session::has('contact'))
	{
		$contact = Session::get('contact');
		$kind_id = $contact['kind_id'];
		$company = $contact['company'];
		$name = $contact['name'];
		$mail = $contact['mail'];
		$content = $contact['content'];

		//if has company
		if($kind_id == 2)
		{
			$styleDisplay = 'style="display: block;"';
			$required = 'required';
			$checked_company = 'checked="checked"';
			$checked_user = '';
		}
	}

	$style_display_block = "style='display: block;'";

	// if($errors->has('kind'))
	// {
	// 	$style_kind = $style_display_block;
	// 	$error_kind = "Value is invalid";
	// }

	// if($errors->has('company'))
	// {
	// 	$style_company = $style_display_block;
	// 	$error_company = $errors->first('company');
	// }

	// if($errors->has('name'))
	// {
	// 	$style_name = $style_display_block;
	// 	$error_name = $errors->first('name');
	// }

	// if($errors->has('mail'))
	// {
	// 	$style_email = $style_display_block;
	// 	$error_email = $errors->first('mail');
	// }

	// if($errors->has('content'))
	// {
	// 	$style_content = $style_display_block;
	// 	$error_content = $errors->first('content');
	// }

?>
@extends('layouts.default')

@section('column_right')
	@include('shared.right_column.no_content')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 	=> 'お問い合わせ'
						))
@stop

@section('css')
	<link rel="stylesheet" href="/public/css/contact.css">
@stop


@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<!--.column-left__heading--single-->
		<h1 class="column-left__ttl--normal u-fs--xxl--contents">お問い合わせ</h1>

		@if (Session::has('flash_message'))
		    {{ Session::get('flash_message') }}
		@endif
		<div class="form--primary u-mt10">
            <p class="form--primary__repletion u-fs--xs--lh--form">
				お問い合わせ内容に関しては、編集部にて確認し対応を行っておりますが、
				ご返信にお時間をいただく場合がございます。また、お問い合せの内容によっては対応できない場合もございますので、
				ご了承ください。
			</p>
			<form novalidate class="u-mt15" action="/contact/confirm" id="contact_form" method="post">
				<table class="form--primary__table">
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">
							<label for="company">
								種
								&nbsp;
								別
							</label><span class="c-tag--form--required u-fs--xxs">必須</span>
						</td>
						<td class="form--primary__detail">
							<div class="-contact__radio__block">
								<input type="radio" name="kind" id="radio1" value="1" {!! $checked_user or '' !!} class="form--primary__radio" data-trigger-radioname="name">
								<label for="radio1" class="u-fs--l">個人</label>
								<input type="radio" name="kind" id="radio2" value="2" {!! $checked_company or '' !!} class="form--primary__radio"  data-trigger-radioname="company" >
								<label for="radio2" class="u-fs--l">法人</label>
							</div>
							<div data-target-inputcompany="target">
								<p data-error="required" class="form__error u-fs--xs--lh--form" ><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>会社名を入力してください。</p>
							</div>
							<input id="company" name="company" type="text" placeholder="会社名を入力してください" data-validate="target" class="form--primary__text u-fs--l" data-target-inputcompany="target" value="{!! $company or Input::old('company') !!}" {!! $styleDisplay or '' !!} required>
						</td>
					</tr>
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">
							<label for="name">お名前</label><span class="c-tag--form--required u-fs--xxs">必須</span>
						</td>
						<td class="form--primary__detail">
							<div>
								<p data-error="required" class="form__error u-fs--xs--lh--form" {><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>お名前を入力してください。</p>
							</div>
							<input id="name" name="name" type="text" placeholder="お名前を入力してください" data-validate="target" required class="form--primary__text u-fs--l" value="{!! $name or Input::old('name') !!}">
						</td>
					</tr>
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">
							<label for="mail">メールアドレス</label><span class="c-tag--form--required u-fs--xxs">必須</span>
						</td>
						<td class="form--primary__detail">
							<div>
								<p data-error="mail" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>メールアドレスの入力に誤りがあります。</p>
								<p data-error="required" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>メールアドレスを入力してください。</p>
							</div>
							<input id="mail" name="mail" type="email" placeholder="例） example@runda.com" data-mail="target" data-validate="target" autocomplete="off" required class="form--primary__text u-fs--l" value="{!! $mail or Input::old('mail') !!}">
                   			<p class="form--primary__repletion u-fs--xxs-lh u-mt5">※こちらのメールアドレスにご返信させていただきます。<br>お手数ですがメールが受信可能かご確認ください。</p>
						</td>
					</tr>
					<tr class="-contact__table__row">
						<td class="form--primary__ttl u-fs--l u-fwb">
							<label for="view">お問い合わせ内容</label><span class="c-tag--form--required u-fs--xxs">必須</span>
						</td>
						<td class="form--primary__detail">
							<div>
								<p data-error="required" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>お問い合わせ内容を入力してください。</p>
							</div>
							<textarea id="content" name="content" placeholder="ご意見・ご感想をご記入ください" data-validate="target" required class="form--primary__textarea u-fs--lh--contents">{!! $content or Input::old('content') !!}</textarea>
						</td>
					</tr>
				</table>
				<div class="-contact__button__block">
					<div data-reset="reset" class="c-button-square--l--gray u-fs--xxl u-fwb" onclick="resetForm();">リセット</div>
					<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xxl u-fwb"><span class="button-inner">確認する</span></button>
				</div>

				<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			</form>
		</div>
	</div>
@stop




