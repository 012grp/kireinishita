<?php
	//Session::forget('contact');
	$__title = _helper::getWebInfo('title_contact');
	$web_title = _helper::set_title($__title);

	$description = trans('webinfo.meta_desc_contact');
	$keywords = trans('webinfo.keywords_contact');
	
	$data_validate = '';
	$styleDisplay = 'style="display: none;"';
	$required = '';
	$checked_user = 'checked="checked"';
	$checked_company = '';

	if (Session::has('contact'))
	{
		$contact = Session::get('contact');
		$kind_id = $contact['kind_id'];
		$company = $contact['company'];
		$name = $contact['name'];
		$mail = $contact['mail'];
		$content = $contact['content'];

		//if has company
		if($kind_id == 2)
		{
			$data_validate = 'data-validate="target"';
			$styleDisplay = 'style="display: inline-block;"';
			$required = 'required';
			$checked_company = 'checked="checked"';
			$checked_user = '';
		}
	}
	
	$style_display_block = "style='display: block;'";

	// if($errors->has('kind'))
	// {
	// 	$style_kind = $style_display_block;
	// 	$error_kind = "Value is invalid";
	// }

	// if($errors->has('company'))
	// {
	// 	$style_company = $style_display_block;
	// 	$error_company = $errors->first('company');
	// }

	// if($errors->has('name'))
	// {
	// 	$style_name = $style_display_block;
	// 	$error_name = $errors->first('name');
	// }

	// if($errors->has('mail'))
	// {
	// 	$style_email = $style_display_block;
	// 	$error_email = $errors->first('mail');
	// }

	// if($errors->has('content'))
	// {
	// 	$style_content = $style_display_block;
	// 	$error_content = $errors->first('content');
	// }
?>
@extends('layouts.default_sp')


@section('page_sp')
<div class="primary-box--noPadding">
	<h1 class="primary-box__ttl--back u-fwb u-fs--m">お問い合わせ</h1>

	@if (Session::has('flash_message'))
	    {{ Session::get('flash_message') }}
	@endif

	<div class="form--primary form--primary--contact u-mt10">
		<p class="form--primary__repletion--top u-fs--xsh">
			お問い合わせ内容に関しては、編集部にて確認し対応を行っておりますが、
			ご返信にお時間をいただく場合がございます。また、お問い合せの内容によっては対応できない場合もございますので、
			ご了承ください。
		</p>
		<form novalidate class="u-mt15" action="/contact/confirm" id="contact_form" method="post">
			<table class="form--primary__table">
				<tr class="-contact__table__row">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">
						<label for="company">
							種
							&nbsp;
							別
						</label><span class="c-tag--form--required u-fs--xxs">必須</span>
					</td>
					<td class="form--primary__detail u-mt15">
						@if($errors->has('kind'))
							<p class="form__error u-fs--xs" style="display:block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>Value is invalid</p>
						@endif
						<div class="-contact__radio__block">
							<input type="radio" name="kind" id="radio1" value="1" {!! $checked_user or '' !!} data-trigger-radioname="name" class="form--primary__radio">
							<label for="radio1" class="u-fs--l">個人</label>
							<input type="radio" name="kind" id="radio2" value="2" {!! $checked_company or '' !!} data-trigger-radioname="company" class="form--primary__radio">
							<label for="radio2" class="u-fs--l">法人</label>
						</div>
						@if($errors->has('company'))
							<p class="form__error u-fs--xs" style="display:block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('company')}}</p>
						@endif
						<div data-target-inputcompany="error">
							<p data-error="required" class="form__error u-fs--xs"><span class="c-icon--attention u-fs--xxxs u-fwb"></span>会社名を入力してください。</p>
						</div>
	                    <input id="company" name="company" type="text" placeholder="会社名を入力してください" data-validate="target" data-target-inputcompany="input" class="form--primary__text u-fs--l" {!! $styleDisplay or '' !!} value="{!! $company or Input::old('company') !!}" required>
					</td>
				</tr>
				<tr class="-contact__table__row u-mt20">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">
						<label for="name">お名前</label><span class="c-tag--form--required u-fs--xxs">必須</span>
					</td>
					<td class="form--primary__detail u-mt15">
						@if($errors->has('name'))
							<p class="form__error u-fs--xs" style="display:block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('name')}}</p>
						@endif
						<div>
							<p data-error="required" class="form__error u-fs--xs"><span class="c-icon--attention u-fs--xxxs u-fwb"></span>お名前を入力してください。</p>
						</div>
						<input id="name" name="name" type="text" placeholder="お名前を入力してください" data-validate="target" required class="form--primary__text u-fs--l" value="{!! $name or Input::old('name') !!}">
					</td>
				</tr>
				<tr class="-contact__table__row u-mt20">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">
						<label for="mail">メールアドレス</label><span class="c-tag--form--required u-fs--xxs">必須</span>
					</td>
					<td class="form--primary__detail u-mt15">
						@if($errors->has('mail'))
							<p class="form__error u-fs--xs" style="display:block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('mail')}}</p>
						@endif
						<div>
							<p data-error="mail" class="form__error u-fs--xs"><span class="c-icon--attention u-fs--xxxs u-fwb"></span>メールアドレスの入力に誤りがあります。</p>
							<p data-error="required" class="form__error u-fs--xs"><span class="c-icon--attention u-fs--xxxs u-fwb"></span>メールアドレスを入力してください。</p>
						</div>
						<input id="mail" name="mail" type="text" placeholder="例） example@runda.com" data-mail="target" data-validate="target" autocomplete="off" required class="form--primary__text u-fs--l" value="{!! $mail or Input::old('mail') !!}">
						<p class="form--primary__repletion u-fs--xsh u-mt5">※こちらのメールアドレスにご返信させていただきます。<br>お手数ですがメールが受信可能かご確認ください。</p>
					</td>
				</tr>
				<tr class="-contact__table__row u-mt20">
					<td class="form--primary__ttl u-fs--s u-fwb u-mt20">
						<label for="view">お問い合わせ内容</label><span class="c-tag--form--required u-fs--xxs">必須</span>
					</td>
					<td class="form--primary__detail u-mt15">
						@if($errors->has('content'))
							<p class="form__error u-fs--xs" style="display:block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('content')}}</p>
						@endif
						<div>
							<p data-error="required" class="form__error u-fs--xs"><span class="c-icon--attention u-fs--xxxs u-fwb"></span>お問い合わせ内容を入力してください。</p>
						</div>
						<textarea id="content" name="content" placeholder="ご意見・ご感想をご記入ください" data-validate="target" required class="form--primary__textarea u-fs--sh">{!! $content or Input::old('content') !!}</textarea>
					</td>
				</tr>
			</table>
			<div class="-contact__button__block u-mt20">
				<div data-reset="reset" class="c-button-square--l--gray u-fs--xs u-fwb">リセット</div>
				<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">確認する</span></button>
			</div>
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		</form>
	</div>
</div>
@stop