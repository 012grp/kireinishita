<?php 
	$class_primary_box = 'u-mt20'; 
	$__title = _helper::getWebInfo('title_contact_thanks');
	$web_title = _helper::set_title($__title);
?>
@extends('layouts.default')

@section('column_right')
	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 	=> 'お問い合わせ'
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<h1 class="column-left__ttl--normal u-fs--xxl--contents">お問い合わせ</h1>
		<div class="form--primary u-mt10">
			<p class="form--primary__thanks u-fs--xxlh--contents u-fwb u-mt35">お問い合せが完了しました。<br>誠にありがとうございます。</p>
			<p class="-contact__thanks__text u-fs--lh--contents u-mt50">3~5営業日以内に担当よりご連絡いたしますので、今しばらくお待ちください。<br>万が一返信がない場合、恐れ入りますが、<br>{{env('MAIL_FEEDBACK','')}}まで直接ご連絡いただくようお願いいたします。</p>
			<div class="-contact__button__block u-mt50">
				<div class="c-button-square--l u-fs--xxl u-fwb"><a href="/">HOMEへ</a></div>
			</div>
		</div>
	</div>
@stop
