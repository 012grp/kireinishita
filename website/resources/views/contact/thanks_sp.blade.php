<?php 
	$__title = _helper::getWebInfo('title_contact_thanks');
	$web_title = _helper::set_title($__title);
?>
@extends('layouts.default_sp')

@section('page_sp')

	<!--.primary-box--fixHeight-->
<div class="primary-box--noPadding">
	<div class="primary-box--full__inner">
		<h1 class="primary-box__ttl--back u-fwb u-fs--m">お問い合わせ</h1>
		<div class="form--primary">
			<p class="form--primary__thanks u-fs--mh u-fwb u-mt30">お問い合せが完了しました。<br>誠にありがとうございます。</p>
			<p class="-contact__thanks__text u-fs--xsh u-mt30">3~5営業日以内に担当よりご連絡いたしますので、<br>今しばらくお待ちください。<br>万が一返信がない場合、恐れ入りますが、<br>{{env('MAIL_FEEDBACK','')}}まで<br>直接ご連絡いただくようお願いいたします。</p>
		</div>
		<div class="form__button__block--noborder form__button__block--thanks u-mt20">
			<p class="u-fs--s u-mt30 u-txtc"><a href="/" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">HOMEへ</span></a></p>
		</div>
	</div>
</div>
@stop