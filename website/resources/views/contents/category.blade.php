<?php 
	$class_primary_box = 'u-mt20';
	
	$web_title = _helper::set_title($articles[0]['web_title']);
	$description = $articles[0]['meta_desc'];
	$keywords = $articles[0]['meta_key'];
?>
@extends('layouts.default')

@section('column_right')
	@include('shared.right_column.no_lastest_article')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> $articles[0]['category'],
							'parent'	=> array(
									array('link' => '/contents/', 'title' => 'コンテンツまとめ'),
								)
						))
@stop


@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<div class="column-left__heading">
			<h1 class="column-left__ttl u-fs--xxxl--contents primary-box__ttl--side {{ $articles[0]['class_name'] }}">
				<span class="column-left__ttl__inner">{{$articles[0]['category']}}</span>
				<span class="u-mt10 column-left__text u-fs--xs">{{ strtoupper($articles[0]['category_name']) }}</span>
			</h1>
			<p class="column-left__ttl__right u-fs--sh">{{ $articles[0]['cat_description'] }}</p>
		</div>

		<h3 class="column-box--new__ttl u-fs--xl u-fwb u-mt20"><i class="c-icon--star"></i>新着コンテンツ</h3>

		<div class="column-box--new u-mt10">
			<ul class="column-box--new__list">
				@foreach ($articles as $article) 
					@if($article['id'])
						<li class="column-box--new__lists">
							@include('shared.item.box_main_article_content', $article)
						</li>
					@endif
				@endforeach
			</ul>


			{{-- contain category --}}
			<?php $arrCategory = _helper::get_list_menu(); ?>
			@include('shared.item.box_contain_category', 
				array(
					'name' => 'コンテンツカテゴリー',
					'categories' => $arrCategory,
					'active' => $articles[0]['category_alias']
				)
			)
			
		</div>
		<!--TODO paginate-->
		<div class="pager u-mt30">
			@include('pagination.default', ['paginator' => $articles])
		</div>
	</div>
@stop


			
	