<?php 
	$u_mt20 = 'u-mt20'; 

	$web_title = _helper::set_title($articles[0]['web_title']);
	$description = $articles[0]['meta_desc'];
	$keywords = $articles[0]['meta_key'];
	if($articles)
	{
		$pageCount = $articles->lastPage();
		$currentPage = $articles->currentPage();
		$url_next = $articles->url($currentPage+1);
		$url_prev = $articles->url($currentPage-1);
	}
	else
	{
		$pageCount = 1;
		$currentPage = 1;
		$url_next = '';
		$url_prev = '';
	}
?>

@extends('layouts.default_sp')

@section('under_header')

@stop 


@section('page_sp')
	<div class="primary-box--noPadding">
		<div class="_inner--sidepadding--side">
			<h1 class="primary-box__ttl--side u-fs--xxl u-mt15 {{ $articles[0]['class_name'] }}">{{$articles[0]['category']}}<span class="u-fs--m u-mt10"><?php echo strtoupper($articles[0]['category_name']) ?></span></h1>

			<p class="primary-box--side u-fs--xsh u-mt10">{{ $articles[0]['cat_description'] }}</p>
		</div>
		@include('shared_sp.item.contents_nav_horizontal', array('u_mt20'=> $u_mt20))
		@include('shared_sp.item.contents_new', array(
				'contents' => $articles
			)
		)

		@if(!(count($articles) == 1 && $articles[0]['id'] == null))
			@include('shared_sp.item.pager', array(
				'pageCount' 	=> $pageCount,
				'currentPage' 	=> $currentPage,
				'url_next' 	=> $url_next,
				'url_prev' 	=> $url_prev,
			))
		@endif
		<?php $popularArticle = _modules::popular_article_content(); ?>
		@include('shared_sp.item.contents_popular', array(
				'contents' => $popularArticle
			)
		)

		@include('shared_sp.item.contents_category')
	</div>
@stop