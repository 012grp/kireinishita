<div data-target-leftcolumn="target" class="column-left column-left--bg">
		<article class="column-box__contents {{$article_content['class_name']}}">
			<div class="column-box__contents__topImg">
				<img src="{{ $image }}" alt="テスト">
				<span class="c-tag--{{$article_content['class_name']}}--l--contents u-fs--xs u-fwb">{{ $article_content['category'] }}</span>
			</div>
			<div class="_contents__top__block">
				<h1 class="column-box__contents__ttl u-fs--xxxlh--contents u-fwb u-mt25">
					{{ $article_content['title'] }}
				</h1>
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
				<p class="column-box_contents__text u-fs--lh--contents u-mt25">
					{{ $article_content['description'] }}
				</p>
			</div>
			<div class="_contents__detail__block u-mt60">
				{!! $artContent !!}

				<div class="_contents__bottom__block--info u-fs--lh--contents u-mt20">
					@if(!empty($article_content['url']))
					<p>ホームページ：
						<a href="{{ $article_content['url'] }}">{{ $article_content['url'] }}</a>
					</p>
					@endif
					
					@if(!empty($article_content['address']))
					<p>住所：{{ $article_content['address'] }}</p>
					@endif
				</div>

				@if($article_content['store_infomation'])
					<div class="_contents__overview u-mt30 u-fs--lh--contents--other">
	                	<h3 class="u-fwb">[店舗情報]</h3>
						<?php _helper::explode_ul($article_content['store_infomation']); ?>
              		</div>
              	@endif
              	
              	@if($article_content['company_profile'])
	              	<div class="_contents__overview u-mt30 u-fs--lh--contents--other">
	                	<h3 class="u-fwb">[会社概要]</h3>
	                	<?php _helper::explode_ul($article_content['company_profile']); ?>
	              	</div>
              	@endif

				@if(!empty($article_content['lat']) && !empty($article_content['lng']))
				<div class="_contents__detail__map u-mt30" id="detail_map">
					<div class="_detail__table__map" id="map"></div>
				</div>
				@endif
				
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '--border',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
			</div>
		</article>


		<h3 class="column-box--normal__ttl u-fs--xl u-fwb u-mt40">関連コンテンツ</h3>
		<div class="column-box--normal u-mt10">
			<ul class="column-box--normal__list">
				@foreach($related_content as $content)
					<li class="column-box--normal__lists">
						@include('shared.item.box_main_article_content_lazyzr', $content)
					</li>
				@endforeach
			</ul>
		

			{{-- contain category --}}
			<?php 
				$arrCategory = _helper::get_list_menu();
			?>
			@include('shared.item.box_contain_category', 
				array(
					'name' => 'コンテンツカテゴリー',
					'categories' => $arrCategory,
					'active' => $article_content['category_alias'] 
				)
			)
		</div>
	</div>