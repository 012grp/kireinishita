<?php
	//set template to view
	$templateFolderName = _common::getArticleCatDir($article_content['category_alias']);

	//$web_custom_title = _helper::set_title($article_content['title']);

	//$web_custom_description = $article_content['description'];
	$keywords = $article_content['meta_keyword'];

	$web_title = _helper::set_title($article_content['title']);

	$class_primary_box = 'u-mt20';


	if(!empty($article_content['image']))
	{
		$image = '/public/uploads/article/'.$article_content['image'];
	}else{
		$image = _common::noImageUrlContents();
	}

	$title = $article_content['title'];

	$description = $article_content['meta_desc'];
	if($description == "")
	{
		$description = null;
	}
	
	if($article_content['adviser_image'])
	{
		$article_content['adviser_image'] = '/public/uploads/article/'.$article_content['adviser_image'];
	}

	$request_url = Request::url();

	//REGEX content: remove SP image
	$artContent = _common::removeImageOfSP($article_content['content']);

	$key = Config::get('webconfig.google_map_api_key');
?> 
@extends('layouts.default')

@section('column_right')
   @include('shared.right_column.contents_detail')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> $article_content['title'],
							'parent'	=> array(
									array('link' => '/contents/', 'title' => 'コンテンツまとめ'),
									array('link' => '/contents/'.$article_content['category_alias'], 'title' => $article_content['category']),
								)
						))
@stop



@section('js_google')
	<input type="hidden" id="lat" value="<?php echo $article_content['lat']?>">
	<input type="hidden" id="lng" value="<?php echo $article_content['lng']?>">
	<input type="hidden" id="company_name" value="<?php echo $article_content['company_name']?>">
	<input type="hidden" id="map_location" value="<?php echo $article_content['map_location']?>">
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
@stop

@section('column_left')
	@include('contents.'.$templateFolderName.'.detail')	
@stop


