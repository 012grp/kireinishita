<?php
	//set template to view
	$templateFolderName = _common::getArticleCatDir($article_content['category_alias']);

	//$web_custom_title = _helper::set_title($article_content['title']);
	//$web_custom_description = $article_content['description'];
    $keywords = $article_content['meta_keyword'];
    
    $web_title = _helper::set_title($article_content['title']);


    if (!empty($article_content['image_sp'])) {
        $image = '/public/uploads/article/'.$article_content['image_sp'];
    }else{
        if(!empty($article_content['image'])){
            $image = '/public/uploads/article/'.$article_content['image'];
        }else{
            $image = _common::noImageUrlContents('sp');
        }
    }

    $title = $article_content['title'];

    $description = $article_content['meta_desc'];
    if($description == "")
    {
        $description = null;
    }

	if($article_content['adviser_image_sp'])
	{
		$article_content['adviser_image_sp'] = '/public/uploads/article/'.$article_content['adviser_image_sp'];
	}

	if($article_content['adviser_image'])
	{
		$article_content['adviser_image'] = '/public/uploads/article/'.$article_content['adviser_image'];
	}

    $request_url = Request::url();
	
	//REGEX content: change class names and remove PC image
	$artContent = _common::changeCSSContentArticle($article_content['content'], $templateFolderName);
	$artContent = _common::removeImageOfPC($artContent);

    $key = Config::get('webconfig.google_map_api_key');
?>

@extends('layouts.default_sp')

@section('under_header')
    
@stop

@section('css')
<!--    <link rel="stylesheet" href="/public/sp/css/content.css">-->
@stop

@section('js_google')
    <input type="hidden" id="lat" value="<?php echo $article_content['lat']?>">
    <input type="hidden" id="lng" value="<?php echo $article_content['lng']?>">
    <input type="hidden" id="company_name" value="<?php echo $article_content['company_name']?>">
    <input type="hidden" id="map_location" value="<?php echo $article_content['map_location']?>">
    <script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
@stop


@section('page_sp')
    @include('contents.'.$templateFolderName.'.detail_sp')	
@stop