<?php
	$web_custom_title = _helper::set_title($article_content['title']);
	$web_custom_description = $article_content['description'];

    $web_title = _helper::set_title($article_content['web_title']);


    if (!empty($article_content['image_sp'])) {
        $image = '/public/uploads/article/'.$article_content['image_sp'];
    }else{
        if(!empty($article_content['image'])){
            $image = '/public/uploads/article/'.$article_content['image'];
        }else{
            $image = _common::noImageUrl();
        }
    }

    $title = $web_title;

    $description = $article_content['meta_desc'];

    $request_url = Request::url();

	$artContent = _common::changeCSSContentArticle($article_content['content']);
?>

@extends('layouts.default_sp')

@section('under_header')
    
@stop

@section('css')
<!--    <link rel="stylesheet" href="/public/sp/css/content.css">-->
@stop

@section('page_sp')
    <div class="primary-box--noPadding">
        <div style="background-image:url({{ $image }})" class="primaryImg-box"><span
                    class="u-fs--xs--contents c-tag--{{$article_content['class_name']}}--l--contents">{{ $article_content['category'] }}</span></div>
        <div class="contents-box {{$article_content['class_name']}}">
           	<h1 class="contents-box__ttl u-fs--xlh--contents u-mt30">{{ $article_content['title'] }}</h1>
            
            {!! $artContent !!}
            
            @include('shared_sp.item.box_contents_social', array(
                    'request_url'         => $request_url,
                    'title'               => $title
                )
            )
        </div>
        <div class="contents-box__num u-mt10"><span class="c-icon--eye"></span><span class="u-fs--xxs">{{$article_content['view'] + 1 }}</span></div>
        

        <?php $newArticle = _modules::new_article_content(); ?>
        @include('shared_sp.item.contents_new', array(
                'contents' => $newArticle
            )
        )

        <?php $popularArticle = _modules::popular_article_content(); ?>
        @include('shared_sp.item.contents_popular', array(
                'contents' => $popularArticle
            )
        )
        @include('shared_sp.item.contents_category')
    </div>
@stop