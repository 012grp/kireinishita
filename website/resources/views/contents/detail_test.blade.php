<?php

	$web_custom_title = _helper::set_title($article_content['title']);
	$web_custom_description = $article_content['description'];

	$web_title = _helper::set_title($article_content['web_title']);
	$class_primary_box = 'u-mt20';


	if(!empty($article_content['image']))
	{
		$image = '/public/uploads/article/'.$article_content['image'];
	}else{
		$image = '/public/img/media/trend-01.jpg';
	}

	$title = $article_content['title'];
	$description = $article_content['meta_desc'];

	$request_url = Request::url();

	$key = Config::get('webconfig.google_map_api_key');

?> 
@extends('layouts.default')

@section('column_right')
    @include('shared.item.total_job_today')
    @include('shared.item.menu_column')
    @include('shared.item.search_column')
    @include('shared.item.new_article_contents')
    @include('shared.item.popular_article_contents')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> $article_content['title'],
							'parent'	=> array(
									array('link' => '/contents/', 'title' => 'コンテンツまとめ'),
									array('link' => '/contents/'.$article_content['category_alias'], 'title' => $article_content['category']),
								)
						))
@stop

@section('js')
	<script src="/public/js/init-googlemap.js"></script>
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) { 
			// set google map
			// input lat, lng , inforwindow, zoom
			var lat = <?php echo !empty($article_content['lat']) ? $article_content['lat'] : 'null'; ?>;
			var lng = <?php echo !empty($article_content['lng']) ? $article_content['lng'] : 'null'; ?>;

			if(lat && lng)
			{
				var contentString = "<h4 style='font-weight: bold; color: black;'><?php echo $article_content['company_name']; ?></h4><p><?php echo $article_content['address']; ?></p> ";
				initMap( lat, lng, contentString, 14, 'map');
			}
		});
	</script>
@stop

@section('css')
	<!--<link rel="stylesheet" href="/public/css/content.css">-->
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left column-left--bg">
		<article class="column-box__contents {{$article_content['class_name']}}">
			<div class="column-box__contents__topImg">
				<img src="{{ $image }}" alt="テスト">
				<span class="c-tag--{{$article_content['class_name']}}--l--contents u-fs--xs u-fwb">{{ $article_content['category'] }}</span>
			</div>
			<div class="_contents__top__block">
				<h1 class="column-box__contents__ttl u-fs--xxxlh--contents u-fwb u-mt25">
					{{ $article_content['title'] }}
				</h1>
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
				<p class="column-box_contents__text u-fs--lh--contents u-mt25">
					{{ $article_content['description'] }}
				</p>
			</div>
			<div class="_contents__detail__block u-mt60">
				{!! $article_content['content'] !!}

				<div class="_contents__bottom__block--info u-fs--lh--contents u-mt20">
					@if(!empty($article_content['url']))
					<p>ホームページ：
						<a href="{{ $article_content['url'] }}">{{ $article_content['url'] }}</a>
					</p>
					@endif
					
					@if(!empty($article_content['address']))
					<p>住所：{{ $article_content['address'] }}</p>
					@endif
				</div>
				@if(!empty($article_content['lat']) && !empty($article_content['lng']))
				<div class="_contents__detail__map u-mt25" id="detail_map">
					<div class="_detail__table__map" id="map"></div>
				</div>
				@endif
				
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '--border',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
			</div>
		</article>


		<h3 class="column-box--normal__ttl u-fs--xl u-fwb u-mt40">関連コンテンツ</h3>
		<div class="column-box--normal u-mt10">
			<ul class="column-box--normal__list">
				@foreach($related_content as $content)
					<?php
					if(!empty($content['image_thumb'])){
						$image_other = '/public/uploads/article/'.$content['image_thumb'];
					}else{

						if(!isset($content['image']) || empty($content['image']))
						{
							$image_other = '/public/img/media/media-m01.jpg';
						}else{
							$image_other = '/public/uploads/article/thumbs/280_fw_'.$content['image'];
						}
					}

					?>
					<li class="column-box--normal__lists">
						<a href="/contents/{{ $content['category_alias'] }}/{{ $content['id'] }}">
							<div data-normal="bg" data-layzr-bg="{{ $image_other}}" class="-normal__list__img">
								{{-- created day > today - 1 day is new --}}
								@if($content['created_at'] > (new DateTime('-1 day'))->format('Y-m-d H:i:s'))
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								@endif
							</div>
							<div class="-normal__list__right">
								<p class="-normal__list__text u-fs--xxlh--contents u-fwb">
									{{ $content['title'] }}
								</p>
								<div class="_list__right__bottom">
									<span class="_list__right__tag c-tag--{{ $content['category_alias'] }}--l u-fs--xs u-fwb">{{ $content['category'] }}
									</span>
									<div class="_right__bottom__num">
										<span class="c-icon--eye--l"></span>
										<span class="u-fs--l">{{ $content['view'] }}</span>
									</div>
								</div>
							</div>
						</a>
					</li>
				@endforeach
			</ul>
		

			{{-- contain category --}}
			<?php 
				$arrCategory = _helper::get_list_menu();
			?>
			@include('shared.item.box_contain_category', 
				array(
					'name' => 'コンテンツカテゴリー',
					'categories' => $arrCategory,
					'active' => $article_content['category_alias'] 
				)
			)
		</div>
	</div>
@stop


