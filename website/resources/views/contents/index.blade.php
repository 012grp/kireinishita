<?php 
	$class_primary_box = 'u-mt10';

	$__title = _helper::getWebInfo('title_content');
	$web_title = _helper::set_title($__title);

	$description = _helper::getWebInfo('meta_desc_content');

	$keywords = _helper::getWebInfo('keywords_content');
?>

@extends('layouts.default')

@section('under_header') 
	@include('shared.header.banner')
@stop

@section('column_right')
   	@include('shared.right_column.no_lastest_article')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 	=> 'コンテンツまとめ'
						))
	<h1 class="primary-box__ttl u-fs--xl u-fwb u-mt20"><i class="c-icon--star"></i>新着コンテンツ</h1>
@stop


@section('column_left')
	{{-- new article content --}}
	@include('shared.item.main_article_contents',
		array(
			'contents' => $newArticle
		)
	)

@stop


