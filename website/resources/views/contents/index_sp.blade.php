<?php 
	$class_column_right = 'u-mt30'; 
	$__title = _helper::getWebInfo('title_content');
	$web_title = _helper::set_title($__title);

	$description = _helper::getWebInfo('meta_desc_content');
	$keywords = _helper::getWebInfo('keywords_content');

	if ($newArticle){
		$pageCount = $newArticle->lastPage();
		$currentPage = $newArticle->currentPage();
		$url_next = $newArticle->url($currentPage + 1);
		$url_prev = $newArticle->url($currentPage - 1);
	}

	else{
		$pageCount = 1;
		$currentPage = 1;
		$url_next = '';
		$url_prev = '';
	}


?>

@extends('layouts.default_sp')

@section('under_header')

@stop 

@section('page_sp')
	<div class="primary-box--noPadding">
		<?php $article = _modules::load_random_article_banner(); ?>
		@include('shared_sp.item.contents_primary_img_box', array('article' => $article))
		@include('shared_sp.item.contents_nav_horizontal')
		@include('shared_sp.item.contents_new', array(
				'contents' => $newArticle
			)
		)

		@if(!(count($newArticle) == 1 && $newArticle[0]['id'] == null))
            @include('shared_sp.item.pager', array(
                'pageCount'  => $pageCount,
                'currentPage'  => $currentPage,
                'url_next'  => $url_next,
                'url_prev'  => $url_prev,
            ))
        @endif

		<?php $popularArticle = _modules::popular_article_content(); ?>
		@include('shared_sp.item.contents_popular', array(
				'contents' => $popularArticle
			)
		)
		@include('shared_sp.item.contents_category')
	</div>
@stop