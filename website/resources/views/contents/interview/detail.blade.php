<div data-target-leftcolumn="target" class="column-left column-left--bg">
		<article class="column-box__contents {{$article_content['class_name']}}">
			<div class="column-box__contents__topImg">
				<img src="{{ $image }}" alt="テスト">
				<span class="c-tag--{{$article_content['class_name']}}--l--contents u-fs--xs u-fwb">{{ $article_content['category'] }}</span>
			</div>
			<div class="_contents__top__block">
				<h1 class="column-box__contents__ttl u-fs--xxxlh--interview u-fwb u-mt25">{{ $article_content['title'] }}</h1>
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
				<div class="_contents__overview u-mt30 u-fs--lh--contents--other">
					<h3 class="u-fwb">
							{{ $article_content['company_name'] }}
							<br>{{ $article_content['ceo'] }}
					</h3>
					<ul class="u-mt10">
					  <li>{{ $article_content['description'] }}</li>
					</ul>
              	</div>
			</div>
			<div class="_contents__detail__block">
				{!! $artContent !!}

				@if(!empty($article_content['lat']) && !empty($article_content['lng']))
				<div class="_contents__detail__map u-mt30" id="detail_map">
					<div class="_detail__table__map" id="map"></div>
				</div>
				@endif
				
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '--border',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
			</div>
		</article>


		<h3 class="column-box--normal__ttl u-fs--xl u-fwb u-mt40">関連コンテンツ</h3>
		<div class="column-box--normal u-mt10">
			<ul class="column-box--normal__list">
				@foreach($related_content as $content)
					<li class="column-box--normal__lists">
						@include('shared.item.box_main_article_content_lazyzr', $content)
					</li>
				@endforeach
			</ul>
		

			{{-- contain category --}}
			<?php 
				$arrCategory = _helper::get_list_menu();
			?>
			@include('shared.item.box_contain_category', 
				array(
					'name' => 'コンテンツカテゴリー',
					'categories' => $arrCategory,
					'active' => $article_content['category_alias'] 
				)
			)
		</div>
</div>