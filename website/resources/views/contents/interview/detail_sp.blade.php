<div class="primary-box--noPadding">
        <div style="background-image:url({{ $image }})" class="primaryImg-box"><span
                    class="u-fs--xs--contents c-tag--{{$article_content['class_name']}}--l--contents">{{ $article_content['category'] }}</span></div>
        <div class="contents-box {{$article_content['class_name']}}">
            <h1 class="contents-box__ttl u-fs--xlh--contents u-mt30 u-fwb">{{ $article_content['title'] }}</h1>

            @include('shared_sp.item.box_contents_social', array(
                    'request_url'         => $request_url,
                    'title'               => $title
                )
            )
            
            <div class="contents-box__num u-mt10"><span class="c-icon--eye"></span><span class="u-fs--xxs">{{$article_content['view'] + 1 }}</span></div>

            <div class="contents-box__overview u-mt20 u-fs--mh">
				<h3 class="u-fwb">
							{{ $article_content['company_name'] }}
							<br>{{ $article_content['ceo'] }}
					</h3>
				<ul>
				  <li>{{ $article_content['description'] }}</li>
				</ul>
          	</div>

            
            {!! $artContent !!}
            
            @if($article_content['store_infomation'])
                <div class="contents-box__overview u-mt20 u-fs--mh">
                    <h3 class="u-fwb">[店舗情報]</h3>
                    <?php _helper::explode_ul($article_content['store_infomation']); ?>
                </div>
            @endif

            @if($article_content['company_profile'])
                <div class="contents-box__overview u-mt20 u-fs--mh">
                    <h3 class="u-fwb">[会社概要]</h3>
                    <?php _helper::explode_ul($article_content['company_profile']); ?>
                </div>
            @endif

            @if(!empty($article_content['lat']) && !empty($article_content['lng']))
                <div class="_contents__detail__map u-mt30" id="detail_map">
                    <div class="_detail__table__map" id="map"></div>
                </div>
            @endif

            
        </div>
        
        

        <?php $newArticle = _modules::new_article_content(); ?>
        @include('shared_sp.item.contents_new', array(
                'contents' => $newArticle
            )
        )

        <?php $popularArticle = _modules::popular_article_content(); ?>
        @include('shared_sp.item.contents_popular', array(
                'contents' => $popularArticle
            )
        )
        @include('shared_sp.item.contents_category')
    </div>