<?php
	//set template to view
	$templateFolderName = _common::getArticleCatDir($article_content['category_alias']);

	$web_custom_title = _helper::set_title($article_content['title']);
	$web_custom_description = $article_content['description'];

	$web_title = _helper::set_title($article_content['web_title']);
	$class_primary_box = 'u-mt20';

	if(!empty($article_content['image_base64']))
	{
		$image = $article_content['image_base64'];
	}
	else
	{
		if(!empty($article_content['image']))
		{
			$image = '/public/uploads/article/'.$article_content['image'];
		}else{
			$image = _common::noImageUrl();
		}
	}
		
	if($article_content['adviser_image'])
	{
		if(strpos($article_content['adviser_image'], 'data') !== 0)
		{
			$article_content['adviser_image'] = '/public/uploads/article/'.$article_content['adviser_image'];
		}
	}

	$title = $article_content['title'];
	$description = $article_content['meta_desc'];

	$request_url = Request::url();

	$key = Config::get('webconfig.google_map_api_key');

	//REGEX content: remove SP image
	$artContent = _common::removeImageOfSP($article_content['content']);

?> 
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.contents_detail')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> $article_content['title'],
							'parent'	=> array(
									array('link' => '/contents/', 'title' => 'コンテンツまとめ'),
									array('link' => '/contents/'.$article_content['category_alias'], 'title' => $article_content['category']),
								)
						))
@stop

@section('js')
	<script src="/public/js/init-googlemap.js"></script>
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) { 
			// set google map
			// input lat, lng , inforwindow, zoom
			var lat = <?php echo !empty($article_content['lat']) ? $article_content['lat'] : 'null'; ?>;
			var lng = <?php echo !empty($article_content['lng']) ? $article_content['lng'] : 'null'; ?>;

			if(lat && lng)
			{
				var contentString = "";
				//check if exists company name
				<?php if($article_content['company_name']): ?>
					contentString += "<h4 style='font-weight: bold; color: black;'><?php echo $article_content['company_name']; ?></h4>";
				<?php endif; ?>
				//check if exists map location
				<?php if($article_content['map_location']): ?>
					contentString += "<p><?php echo $article_content['map_location']; ?></p>";
				<?php endif; ?>
				initMap( lat, lng, contentString, 14, 'map');
			}
		});
	</script>
@stop

@section('css')
	<!--<link rel="stylesheet" href="/public/css/content.css">-->
@stop

@section('column_left')
	@include('contents.'.$templateFolderName.'.detail')	
@stop


