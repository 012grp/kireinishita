<?php
	//set template to view
	$templateFolderName = _common::getArticleCatDir($article_content['category_alias']);

	$web_custom_title = _helper::set_title($article_content['title']);
	$web_custom_description = $article_content['description'];

    $web_title = _helper::set_title($article_content['web_title']);

    if(!empty($article_content['image_base64']))
    {
        $image = $article_content['image_base64'];
    }
    else
    {
        if (!empty($article_content['image_sp'])) {
        $image = '/public/uploads/article/'.$article_content['image_sp'];
        }else{
            if(!empty($article_content['image'])){
                $image = '/public/uploads/article/'.$article_content['image'];
            }else{
                $image = _common::noImageUrl();
            }
        }
    }
    

	if($article_content['adviser_image'])
	{
		if(strpos($article_content['adviser_image'], 'data') !== 0)
		{
			$article_content['adviser_image'] = '/public/uploads/article/'.$article_content['adviser_image'];
		}
	}

	if($article_content['adviser_image_sp'])
	{
		if(strpos($article_content['adviser_image_sp'], 'data') !== 0)
		{
			$article_content['adviser_image_sp'] = '/public/uploads/article/'.$article_content['adviser_image_sp'];
		}
	}
	
    $title = $web_title;

    $description = $article_content['meta_desc'];

    $request_url = Request::url();

	//REGEX content: change class names and remove PC image
	$artContent = _common::changeCSSContentArticle($article_content['content'], $templateFolderName);
	$artContent = _common::removeImageOfPC($artContent);

?>

@extends('layouts.default_sp')

@section('under_header')
    
@stop

@section('css')
<!--    <link rel="stylesheet" href="/public/sp/css/content.css">-->
@stop

@section('page_sp')
  	
    @include('contents.'.$templateFolderName.'.detail_sp')	
@stop