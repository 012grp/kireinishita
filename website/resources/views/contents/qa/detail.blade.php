<div data-target-leftcolumn="target" class="column-left column-left--bg">
		<article class="column-box__contents {{$article_content['class_name']}}">
			<div class="column-box__contents__topImg">
				<img src="{{ $image }}" alt="テスト">
				<span class="c-tag--{{$article_content['class_name']}}--l--contents u-fs--xs u-fwb">{{ $article_content['category'] }}</span>
			</div>
			<div class="_contents__top__block">
				<h1 class="column-box__contents__ttl u-fs--xxxlh--contents u-fwb u-mt25">
					{{ $article_content['title'] }}
				</h1>
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
				<p class="column-box_contents__text u-fs--lh--contents u-mt25">
					{{ $article_content['description'] }}
				</p>
			</div>
			<div class="_contents__detail__block u-mt60">
				{!! $artContent !!}

				@if(!empty($article_content['lat']) && !empty($article_content['lng']))
				<div class="_contents__detail__map u-mt30" id="detail_map">
					<div class="_detail__table__map" id="map"></div>
				</div>
				@endif
				
				@include('shared.item.box_contents_social', array(
			        'class'          	  => '--border',
			        'request_url'         => $request_url,
			        'title'               => $title,
			        'view'                => $article_content['view'] + 1
			    ))
			    
			    @if($article_content['adviser_header'])
			    <div class="_contents__qa__adviser__block">
					<div class="_contents__qa__adviser__top">
					  <div class="_contents__qa__adviser__l">
						<figure><img alt="{{ $article_content['adviser_header'] }}" src="{{ $article_content['adviser_image'] }}"></figure>
					  </div>
					  <div class="_contents__qa__adviser__r">
						<h3 class="_contents__qa__adviser__ttl u-fs--xlh u-fwb">{{ $article_content['adviser_header'] }}</h3>
						<p class="_contents__qa__adviser u-fs--sh u-mt15">{{ $article_content['adviser_address'] }}</p>
						<h4 class="_contents__qa__adviser__subTtl u-fs--xlh u-fwb u-mt5">{{ $article_content['adviser_ceo'] }}</h4>
					  </div>
					</div>
					<div class="_contents__qa__adviser__text u-fs--sh u-mt10">
					  {{ $article_content['adviser_content'] }}
					</div>
					<div class="_contents__overview u-fs--sh u-mt10">
					  <h3 class="u-fwb">＜きらめき社労士事務所＞</h3>
					  <ul>
					  	<?php 
							$arr = explode("\r\n", $article_content['adviser_contact']);
						  	foreach( $arr as $val)
						  	{
						  		echo "<li>$val</li>";
						  	}
						?>
					  </ul>
					  <div class="_contents__overview__btnBlock u-mt5">
					  	<a href="{{ $article_content['adviser_website'] }}" target="_blank" class="c-button-square--qa u-fs--xs--lh">&gt; ホームページはコチラ</a>
					  	<a href="mailto:{{ $article_content['adviser_email'] }}" class="c-button-square--qa u-fs--xs--lh">&gt; メールはコチラ</a></div>
					</div>
              </div>
              @endif
              
			</div>
		</article>


		<h3 class="column-box--normal__ttl u-fs--xl u-fwb u-mt40">関連コンテンツ</h3>
		<div class="column-box--normal u-mt10">
			<ul class="column-box--normal__list">
				@foreach($related_content as $content)
					<li class="column-box--normal__lists">
						@include('shared.item.box_main_article_content_lazyzr', $content)
					</li>
				@endforeach
			</ul>
		

			{{-- contain category --}}
			<?php 
				$arrCategory = _helper::get_list_menu();
			?>
			@include('shared.item.box_contain_category', 
				array(
					'name' => 'コンテンツカテゴリー',
					'categories' => $arrCategory,
					'active' => $article_content['category_alias'] 
				)
			)
		</div>
	</div>