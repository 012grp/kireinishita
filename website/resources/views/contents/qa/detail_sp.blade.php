<div class="primary-box--noPadding">
        <div style="background-image:url({{ $image }})" class="primaryImg-box"><span
                    class="u-fs--xs--contents c-tag--{{$article_content['class_name']}}--l--contents">{{ $article_content['category'] }}</span></div>
        <div class="contents-box {{$article_content['class_name']}}">
           	<h1 class="contents-box__ttl u-fs--xlh--contents u-mt30 u-mb15 u-fwb">{{ $article_content['title'] }}</h1>
 			@include('shared_sp.item.box_contents_social', array(
                    'request_url'         => $request_url,
                    'title'               => $title
                )
            )
            <div class="contents-box__num u-mt10"><span class="c-icon--eye"></span><span class="u-fs--xxs">{{$article_content['view'] + 1 }}</span></div>
            {!! $artContent !!}
            
            @if(!empty($article_content['lat']) && !empty($article_content['lng']))
                <div class="_contents__detail__map u-mt30" id="detail_map">
                    <div class="_detail__table__map" id="map"></div>
                </div>
            @endif
            
            @if($article_content['adviser_header'])
            <div class="_contents__qa__adviser__block u-mt35">
			<div class="_contents__qa__adviser__top">
				  <div class="_contents__qa__adviser__l">
					<figure><img alt="{{ $article_content['adviser_header'] }}" 
							src="{{ $article_content['adviser_image_sp']? $article_content['adviser_image_sp']: $article_content['adviser_image'] }}">
					</figure>
				  </div>
				  <div class="_contents__qa__adviser__r">
					<p class="_contents__qa__adviser u-fs--xs--qa u-mt15">{!! preg_replace('/　/', '<br>', $article_content['adviser_address'], 1) !!}</p>
					<h4 class="_contents__qa__adviser__subTtl u-fs--mh u-fwb u-mt5">{{ $article_content['adviser_ceo'] }}</h4>
				  </div>
				</div>
				<h3 class="_contents__qa__adviser__ttl u-fs--mh u-fwb u-mt10">{{ $article_content['adviser_header'] }}</h3>
				<div class="_contents__qa__adviser__text u-fs--xs--qa u-mt10">
				  {{ $article_content['adviser_content'] }}
				</div>
				<div class="contents-box__overview u-fs--xs--qa u-mt10">
				  <h3 class="u-fwb">＜きらめき社労士事務所＞</h3>
				  <ul>
					<?php 
							$arr = explode("\r\n", $article_content['adviser_contact']);
						  	foreach( $arr as $val)
						  	{
						  		echo "<li>$val</li>";
						  	}
					?>
				  </ul>
				  <div class="_contents__overview__btnBlock u-mt5">
				  	<a href="{{ $article_content['adviser_website'] }}" target="_blank" class="c-button-square--qa u-fs--xs--qa">&gt; ホームページはコチラ</a>
				  	<a href="mailto:{{ $article_content['adviser_email'] }}" class="c-button-square--qa u-fs--xs--qa">&gt; メールはコチラ</a></div>
				</div>
			  </div>
			@endif
           
            
        </div>
        

        <?php $newArticle = _modules::new_article_content(); ?>
        @include('shared_sp.item.contents_new', array(
                'contents' => $newArticle
            )
        )

        <?php $popularArticle = _modules::popular_article_content(); ?>
        @include('shared_sp.item.contents_popular', array(
                'contents' => $popularArticle
            )
        )
        @include('shared_sp.item.contents_category')
    </div>