<!DOCTYPE html>
<html lang="ja">
<head>
	<title>キレイにスルンダ</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="UTF-8">
	<meta content="" name="description">
	<meta content="" name="keywords">
	<meta name="viewport" content="width=1080">
	<link rel="stylesheet" href="/public/css/app.css">
	<link rel="shortcut icon" href="/public/favicon.ico">
	<link rel="icon" type="image/png" href="/public/favicon.png" sizes="32x32">
	<!--link(rel="apple-touch-icon-precomposed" href='#{domain}img/clipicon.png')-->
	<!--script.-->
	<!--    (function (i, s, o, g, r, a, m) {-->
	<!--        i['GoogleAnalyticsObject'] = r;-->
	<!--        i[r] = i[r] || function () {-->
	<!--                    (i[r].q = i[r].q || []).push(arguments)-->
	<!--                }, i[r].l = 1 * new Date();-->
	<!--        a = s.createElement(o),-->
	<!--                m = s.getElementsByTagName(o)[0];-->
	<!--        a.async = 1;-->
	<!--        a.src = g;-->
	<!--        m.parentNode.insertBefore(a, m)-->
	<!--    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');-->
	<!--    ga('create', 'UA-31867051-1', '012grp.co.jp');-->
	<!--    ga('require', 'linkid', 'linkid.js');-->
	<!--    ga('send', 'pageview');-->



</head>
<body>
	@include('shared.header.header')
	<div class="primary-box">
		<div class="breadcrumb u-fs--xxs">
			<a href="#"><span>HOME</span></a>
			<span class="breadcrumb__arrow__text u-arrow__text">&gt;</span>
			<a href="#"><span>コンテンツまとめ</span></a>
			<span class="breadcrumb__arrow__text u-arrow__text">&gt;</span>
			<a href="#"><span>トレンド</span></a>
			<span class="breadcrumb__arrow__text u-arrow__text">&gt;</span>
			<span>海や川が目の前？！ロケーション抜群のカフェ&バーで大阪の違う顔を味わいませんか？</span>
		</div>
		<div data-target-maincolumn="target" class="primary-box__inner u-mt20">
			<div data-target-leftcolumn="target" class="column-left column-left--bg">
				<article class="column-box__contents">
					<div class="column-box__contents__topImg">
						<img src="/public/img/media/trend-01.jpg" alt="テスト">
						<span class="c-tag--trend--l--contents u-fs--xs u-fwb">{{ $article_content['category'] }}</span>
					</div>
					<div class="_contents__top__block">
						<h1 class="column-box__contents__ttl u-fs--xxxlh--contents u-fwb u-mt25">
							{{ $article_content['title'] }}
						</h1>
						<div class="_contents__social__block">
							<a href="#" class="c-button-square--social c-facebook">
								<i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i>
								<span class="u-fs--l">シェア</span>
							</a>
							<a href="#" class="c-button-square--social c-twitter">
								<i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i>
								<span class="u-fs--l">ツイート</span>
							</a>
							<a href="#" class="c-button-square--social c-hatena">
								<i class="c-font-icon--hatebu _social__block__icon u-fs--xxl"></i>
								<span class="u-fs--l">ブックマーク</span>
							</a>
							<div class="_social__block__right">
								<span class="c-icon--eye--l"></span>
								<span class="u-fs--l">{{ $article_content['view'] }}</span>
							</div>
						</div>
						<p class="column-box_contents__text u-fs--lh--contents u-mt25">
							{{ $article_content['description'] }}
						</p>
					</div>
					<div class="_contents__detail__block u-mt60">
						{!! $article_content['content'] !!}

						<div class="_contents__bottom__block--info u-fs--lh--contents u-mt20">
							<p>ホームページ：
								<a href="{{ $article_content['url'] }}">{{ $article_content['url'] }}</a>
							</p>
							<p>住所：{{ $article_content['address'] }}</p>
						</div>
						<div class="_contents__detail__map u-mt25">
							<iframe data-normal="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13122.11578146955!2d135.5071726!3d34.6918364!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x549e50d7757f5c9f!2z44OO44O844K544K344On44Ki!5e0!3m2!1sja!2sjp!4v1466668259297" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<div class="_contents__social__block--border">
							<a href="#" class="c-button-square--social c-facebook">
								<i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i>
								<span class="u-fs--l">シェア</span>
							</a>
							<a href="#" class="c-button-square--social c-twitter">
								<i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i>
								<span class="u-fs--l">ツイート</span>
							</a>
							<a href="#" class="c-button-square--social c-hatena">
								<i class="c-font-icon--hatebu _social__block__icon u-fs--xxl"></i>
								<span class="u-fs--l">ブックマーク</span>
							</a>
							<div class="_social__block__right">
								<span class="c-icon--eye--l"></span>
								<span class="u-fs--l">{{ $article_content['view'] }}</span>
							</div>
						</div>
					</div>
				</article>


				<h3 class="column-box--normal__ttl u-fs--xl u-fwb u-mt40">関連コンテンツ</h3>
				<div class="column-box--normal u-mt10">
					<ul class="column-box--normal__list">
						<li class="column-box--normal__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-normal__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-normal__list__right">
									<p class="-normal__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--chopsticks--l u-fs--xs u-fwb">箸やすめ</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--normal__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-normal__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-normal__list__right">
									<p class="-normal__list__text u-fs--xxlh--contents u-fwb">世界では“板チョコ革命”が起きていた？！チョコレートの本当の美味しさを届ける「Bean to bar（ビーン・トゥ・バー）」</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--career--l u-fs--xs u-fwb">転職・キャリア</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span>
											<span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--normal__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-normal__list__img"></div>
								<div class="-normal__list__right">
									<p class="-normal__list__text u-fs--xxlh--contents u-fwb">世界では“板チョコ革命”が起きていた？！チョコレートの本当の美味しさを届ける「Bean to bar（ビーン・トゥ・バー）」</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--career--l u-fs--xs u-fwb">転職・キャリア</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--normal__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-normal__list__img"></div>
								<div class="-normal__list__right">
									<p class="-normal__list__text u-fs--xxlh--contents u-fwb">世界では“板チョコ革命”が起きていた？！チョコレートの本当の美味しさを届ける「Bean to bar（ビーン・トゥ・バー）」</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--career--l u-fs--xs u-fwb">転職・キャリア</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span>
											<span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--normal__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-normal__list__img"></div>
								<div class="-normal__list__right">
									<p class="-normal__list__text u-fs--xxlh--contents u-fwb">世界では“板チョコ革命”が起きていた？！チョコレートの本当の美味しさを届ける「Bean to bar（ビーン・トゥ・バー）」</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--career--l u-fs--xs u-fwb">転職・キャリア</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
					</ul>
				

				{{-- contain category --}}
				<?php $categories = _modules::load_category(); ?>
				@include('shared.item.box_contain_category', 
					array(
						'name' => 'コンテンツカテゴリー',
						'categories' => array(
											$categories[5],
											$categories[0],
											$categories[4],
											$categories[1],
											$categories[2],
											$categories[3],
										),
						'active' => $article_content['category_alias'] 
					)
				)
			</div>
		</div>
		<div class="column-right">
			<div data-target-rightcolumn="target" class="column-right__inner">
				@include('shared.right_menu.total_job_today')
				@include('shared.right_menu.menu')
				@include('shared.right_menu.search')
				@include('shared.right_menu.ranking_top')
				@include('shared.right_menu.ranking_bottom')
			</div>
		</div>
	</div>
</div>
@include('shared.footer.footer')
<script src="/public/js/app.js"></script>
</body>
</html>