<!DOCTYPE html>
<html lang="ja">
<head>
	<title>キレイにスルンダ</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="UTF-8">
	<meta content="" name="description">
	<meta content="" name="keywords">
	<meta name="viewport" content="width=1080">
	<link rel="stylesheet" href="/public/css/app.css">
	<link rel="shortcut icon" href="/public/favicon.ico">
	<link rel="icon" type="image/png" href="/public/favicon.png" sizes="32x32">
	<!--link(rel="apple-touch-icon-precomposed" href='#{domain}img/clipicon.png')-->
	<!--script.-->
	<!--    (function (i, s, o, g, r, a, m) {-->
	<!--        i['GoogleAnalyticsObject'] = r;-->
	<!--        i[r] = i[r] || function () {-->
	<!--                    (i[r].q = i[r].q || []).push(arguments)-->
	<!--                }, i[r].l = 1 * new Date();-->
	<!--        a = s.createElement(o),-->
	<!--                m = s.getElementsByTagName(o)[0];-->
	<!--        a.async = 1;-->
	<!--        a.src = g;-->
	<!--        m.parentNode.insertBefore(a, m)-->
	<!--    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');-->
	<!--    ga('create', 'UA-31867051-1', '012grp.co.jp');-->
	<!--    ga('require', 'linkid', 'linkid.js');-->
	<!--    ga('send', 'pageview');-->



</head>
<body>
	@include('shared.header.header')
	<div class="primary-box">
		<div class="breadcrumb u-fs--xxs">
			<a href="#"><span>HOME</span></a>
			<span class="breadcrumb__arrow__text u-arrow__text">&gt;</span>
			<a href="#"><span>コンテンツまとめ</span></a>
			<span class="breadcrumb__arrow__text u-arrow__text">&gt;</span>
			<span>トレンド</span>
		</div>
		<div data-target-maincolumn="target" class="primary-box__inner u-mt20">
			<div data-target-leftcolumn="target" class="column-left">
				<div class="column-left__heading">
					<h1 class="column-left__ttl u-fs--xxxl--contents">
						<span class="column-left__ttl__inner fix-kana">トレンド</span>
						<span class="u-mt10 column-left__text u-fs--xs">TREND</span>
					</h1>
					<p class="column-left__ttl__right u-fs--sh">飲食に関する最新のトレンド情報をキレイにスルンダが独自にピックアップ！海外ネタや健康・オーガニックまで役立つ情報を随時更新中！</p>
				</div>
				<h3 class="column-box--new__ttl u-fs--xl u-fwb u-mt20"><i class="c-icon--star"></i>新着コンテンツ</h3>
				<div class="column-box--new u-mt10">
					<ul class="column-box--new__list">
						<li class="column-box--new__lists">
							<a href="#">
								<div style="background-image:url(/public/img/media/media-m01.jpg)" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--chopsticks--l u-fs--xs u-fwb">箸やすめ</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div style="background-image:url(/public/img/media/media-m01.jpg)" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界では“板チョコ革命”が起きていた？！チョコレートの本当の美味しさを届ける「Bean to bar（ビーン・トゥ・バー）」</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--career--l u-fs--xs u-fwb">転職・キャリア</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div style="background-image:url(/public/img/media/media-m01.jpg)" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--skill--l u-fs--xs u-fwb">スキル</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div style="background-image:url(/public/img/media/media-m01.jpg)" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？テストテストテストテストテスト</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--chopsticks--l u-fs--xs u-fwb">箸やすめ</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div style="background-image:url(/public/img/media/media-m01.jpg)" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？テストテストテストテストテスト</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--chopsticks--l u-fs--xs u-fwb">箸やすめ</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<!--6＞layzer-->
						<li class="column-box--new__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？テストテストテストテストテスト</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--trend--l u-fs--xs u-fwb">トレンド</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？テストテストテストテストテスト</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--career--l u-fs--xs u-fwb">転職・キャリア</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--skill--l u-fs--xs u-fwb">スキル</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？テストテストテストテストテスト</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--chopsticks--l u-fs--xs u-fwb">箸やすめ</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
						<li class="column-box--new__lists">
							<a href="#">
								<div data-normal="bg" data-layzr-bg="/public/img/media/media-m01.jpg" class="-new__list__img">
									<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
								</div>
								<div class="-new__list__right">
									<p class="-new__list__text u-fs--xxlh--contents u-fwb">世界の料理をアルファベット順にご紹介！第13弾”M”「モモ」「マフェ」って知ってますか？テストテストテストテストテスト</p>
									<div class="_list__right__bottom">
										<span class="_list__right__tag c-tag--chopsticks--l u-fs--xs u-fwb">箸やすめ</span>
										<div class="_right__bottom__num">
											<span class="c-icon--eye--l"></span><span class="u-fs--l">2368</span>
										</div>
									</div>
								</div>
							</a>
						</li>
					</ul>
					
					<h3 class="nav-box--category__ttl u-fs--xl u-fwb"><i class="c-icon--circle--text__ttl"></i>コンテンツカテゴリー</h3>
					<ul class="nav-box--category u-fs--xs">
						<li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i class="c-icon--work"></i></span><span>お仕事図鑑</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span></a></li>
						<li class="nav-box__list is-selected">
							<p class="nav-box__list--selected"><span class="nav-box__list__icon"><i class="c-icon--trend"></i></span><span>トレンド</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span></p>
						</li>
						<li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i class="c-icon--skill"></i></span><span>スキル</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span></a></li>
						<li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i class="c-icon--interview"></i></span><span>インタビュー</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span></a></li>
						<li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i class="c-icon--career"></i></span><span>転職・キャリア</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span></a></li>
						<li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i class="c-icon--chopsticks"></i></span><span>箸やすめ</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span></a></li>
					</ul>
				</div>
				<div class="pager u-mt30">
					<ul class="pager__lists u-fs--l">
						<li class="pager__list is-disable">&lt; 前へ</li>
						<li class="pager__list is-selected">1</li>
						<li class="pager__list"><a href="#">2</a></li>
						<li class="pager__list"><a href="#">3</a></li>
						<li class="pager__list"><a href="#">4</a></li>
						<li class="pager__list"><a href="#">5</a></li>
						<li class="pager__list"><a href="#">6</a></li>
						<li class="pager__list"><a href="#">7</a></li>
						<li class="pager__list"><a href="#">8</a></li>
						<li class="pager__list"><a href="#">9</a></li>
						<li class="pager__list"><a href="#">10</a></li>
						<li class="pager__list"><a href="#">次へ &gt;</a></li>
					</ul>
				</div>
			</div>
			<div class="column-right">
				<div data-target-rightcolumn="target" class="column-right__inner">
					@include('shared.right_menu.total_job_today')
					@include('shared.right_menu.menu')
					@include('shared.right_menu.search')
					@include('shared.right_menu.ranking_bottom')
				</div>
			</div>
		</div>
	</div>
	@include('shared.footer.footer')
	<script src="/public/js/app.js"></script>
</body>
</html>