@extends('master')
@section('content')
<div class="search_content">
            <div class="wp_search">
				<p class="result_msg">
					<strong>
						{!! $contents->total() !!} Jobs for you
					</strong>
				</p>

                <ol id="lstContent">
					@foreach($contents as $content)
						<li id="{{$content->id}}">
	                        <div class="job_item">								
	                            <h3>
	                                <a target="_blank" href="{{$content->job_url}}">
									{!!html_entity_decode($content->job_name, ENT_COMPAT, 'UTF-8')!!}
	                                </a>
	                            </h3>
	                            <h4>{{$content->job_career}}</h4>
	                            <div class="job_detail">
									{!!html_entity_decode($content->job_detail, ENT_COMPAT, 'UTF-8')!!}
	                            </div>
								<div class="job_type">
									<ul>
									{!!html_entity_decode($content->job_type, ENT_COMPAT, 'UTF-8')!!}
									</ul>
								</div>
								<div class="job_info">
									<div class="job_label">
										会社
									</div>
									{!!html_entity_decode($content->job_company, ENT_COMPAT, 'UTF-8')!!}
		                            <div class="job_label">
		                                勤務地
		                            </div>
		                            {!!html_entity_decode(str_replace('<!--BR-->','<br>',$content->job_location), ENT_COMPAT, 'UTF-8')!!}
		                            <div class="job_label">
		                                給与
		                            </div>
									{!!html_entity_decode($content->job_salary, ENT_COMPAT, 'UTF-8')!!}
		                            <div class="job_label">
		                                募集終了日時
		                            </div>
									{!!html_entity_decode($content->job_expire, ENT_COMPAT, 'UTF-8')!!}
	                            </div>
								<div class="job_image">
									@if(!empty($content->job_image))
									<a target="_blank" href="{{$content->job_url}}">									 
									 <img src="{{$content->job_image}}" />									
									</a>
									@endif
								</div>
	                            <div class="detail_controller">
	                                <a class="detail_button" target="_blank" href="{{$content->job_url}}">詳しい情報を見る</a>
	                                @if(isset($user) && empty($content->contentId))
	                                   <a class="detail_button mark_style btnMark" id="{!! $content->id !!}">Mark this</a>
	                                @endif
	                            </div>
	                            
	                        </div>
	                    </li>
					@endforeach 
                </ol>
                
                <script type="text/javascript">
                    $(function() {
                        $('#lstContent .btnMark').on('click', function () {
                            var id = $(this).attr('id');
                            var me = this;
                            $.get('/bookmark/mark?id='+id, function (result) {
                                var refMe = me;
                                result>0&&$(refMe).remove()||alert('can not mark this job');
                            });
                        });
                    });
                </script>
            </div>
        </div>
		{!! $contents->render() !!}
@stop