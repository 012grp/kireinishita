@extends('master')
 
@section('content')

<h1>{!! $urls->total() !!} Jobs for you:</h1>
<ul>
	@foreach($urls as $url)
		<li>Job : <a href="{{$url->url}}" target="_blank">{{$url->title}}</a> | Date : {{$url->created_at}}</li>
	@endforeach
	
	</ul>
{!! $urls->render() !!}
@stop
