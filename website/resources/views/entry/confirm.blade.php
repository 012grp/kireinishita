<?php 
	$class_primary_box = 'u-mt20';
	$__title = _helper::getWebInfo('title_entry_confirm');
	$web_title = _helper::set_title($__title);

	if(Session::has('entry'))
	{
		$entryInfo = Session::get('entry');
	}
	
?>

@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
 	@include('breadcrumb.default', array(
							'page' 	=> '応募フォーム'
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<div class="form--primary__topBlock">
			<h1 class="column-left__ttl--normal u-fs--xxl--contents">応募フォーム</h1>
			<div class="flow__arrow u-fs--s"><span class="c-icon--flow--l">1.登録</span><span class="c-icon--flow--c--is">2.確認</span><span class="c-icon--flow--r">3.応募完了</span></div>
		</div>
		<div class="form--primary u-mt10">
			<p class="form--primary__repletion u-fs--xs--lh--form u-fwb">{{$entryInfo['company_name'] or ''}}</p>
			<p class="form--primary__repletion u-fs--xs--lh--form"><span>{{$secret['average_price'] or ''}}</span><span>{{$secret['address'] or ''}}</span></p>
			<form action="/entry/thanks" method="post" class="u-mt15">
				<h2 class="form--primary__heading u-fs--sh">基本情報</h2>
				<table class="form--primary__table--noBorder">
					<tbody>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">お名前<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['name'] or ''}}</span></td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">フリガナ<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['kana'] or ''}}</span></td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">性別<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['gender'] or ''}}</span></td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">生年月日<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['year'] or ''}}年{{$entryInfo['month'] or ''}}月{{$entryInfo['day'] or ''}}日</span></td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">メールアドレス<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['email'] or ''}}</span></td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">住所<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['province_name'] or ''}}{{$entryInfo['city_name'] or ''}}</span></td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">電話番号<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['tel'] or ''}}</span></td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">現在の就業状況<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">
								{{$entryInfo['situation_value'] or ''}}
							</span></td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">入社可能時期<span class="c-tag--form--required u-fs--xxs">必須</span></td>
							<td class="form--primary__detail"><span class="u-fs--l">
								{{$entryInfo['join_value'] or ''}}
							</span></td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">希望連絡時間・連絡方法</td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['contact'] or ''}}</span></td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">取得資格</td>
							<td class="form--primary__detail"><span class="u-fs--l">{{$entryInfo['capabilities'] or ''}}</span></td>
						 </tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">職務経歴・自己PR</td>
							<td class="form--primary__detail">
								<span class="u-fs--lh--contents">{!! nl2br(e($entryInfo['pr'])) !!}</span>
							</td>
						</tr>
					</tbody>
				</table>
				<h2 class="form--primary__heading u-fs--sh">希望職種</h2>
				<div class="-primary__table__check u-fs--xs"><span class="search-checkbox__label u-mt20">{{$entryInfo['job_objective'] or ''}}</span></div>
				<h2 class="form--primary__heading u-fs--sh u-mt20">希望雇用形態</h2>
				<div class="-primary__table__check u-fs--xs"><span class="search-checkbox__label u-mt20">{{$entryInfo['hope_employment'] or ''}}</span></div>
				
				<div class="form__button__block u-mt30">
					<p class="u-fs--s u-mt30 u-txtc"><a target="_blank" href="/about/privacypolicy" class="form__text--link">利用規約</a>に同意して</p>
				</div>
				
				<div class="form__button__block--multi u-mt10">
					<a href="{{ $entryInfo['entryURL'] or '#'}}" class="c-button-square--l--gray u-fs--xxl u-fwb">修正する</a>
					<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xxl u-fwb"><span class="button-inner">送信する</span></button>
				</div>
				<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			</form>
		</div>
	</div>
@stop