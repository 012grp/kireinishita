<?php
	$__title = _helper::getWebInfo('title_entry_confirm');
	$web_title = _helper::set_title($__title);

	if(Session::has('entry'))
	{
		$entryInfo = Session::get('entry');
	}
?>

@extends('layouts.default_sp')

@section('page_sp')
<div class="primary-box--noPadding">
	<div class="form--primary__topBlock">
		<h1 class="form--primary__topBlock__ttl u-fs--m u-fwb">応募フォーム</h1>
		<div class="flow__arrow u-fs--xs--contents"><span class="c-icon--flow--l">1.登録</span><span class="c-icon--flow--c--is">2.確認</span><span class="c-icon--flow--r">3.応募完了</span></div>
	</div>
	<div class="form--primary form--primary--recruit u-mt10">
		<form action="/entry/thanks" method="post" class="u-mt15">
			<h2 class="form--primary__heading u-fs--sh">基本情報</h2>
			<table class="form--primary__table">
				<tbody>
					<tr class="-contact__table__row -contact__table__row--noborder">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">お名前</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['name'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">フリガナ</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['kana'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">性別</td>
						<td class="form--primary__detail u-mt10">
							<div class="-contact__radio__block"><span class="u-fs--l">{{$entryInfo['gender'] or ''}}</span></div>
						</td>
					</tr>
					<tr class="-contact__table__row u-mt20 -contact__table__row--ovh">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">生年月日</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['year'] or ''}}年{{$entryInfo['month'] or ''}}月{{$entryInfo['day'] or ''}}日</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">メールアドレス</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['email'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row -contact__table__row--ovh u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">住所</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['province_name'] or ''}}{{$entryInfo['city_name'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">電話番号</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['tel'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">現在の就業状況</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['situation_value'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">入社可能時期</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['join_value'] or ''}}</span></td>
					</tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">希望連絡時間・連絡方法</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['contact'] or ''}}</span></td>
					 </tr>
					 <tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">取得資格</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{{$entryInfo['capabilities'] or ''}}</span></td>
					  </tr>
					<tr class="-contact__table__row u-mt20">
						<td class="form--primary__ttl u-fs--s u-fwb u-mt20">職務経歴・自己PR</td>
						<td class="form--primary__detail u-mt10"><span class="u-fs--l">{!! nl2br(e($entryInfo['pr'])) !!}</span></td>
					</tr>
				</tbody>
			</table>

			<h2 class="form--primary__heading u-fs--sh u-mt20">希望職種</h2>
			<div class="-primary__table__check u-fs--xs">
				<div class="u-fs--l u-mt20">
					<?php $entryInfo['job_objective'] = str_replace('、', ' / ', $entryInfo['job_objective'])?>
					{{$entryInfo['job_objective'] or ''}}
				</div>
			</div>
			<h2 class="form--primary__heading u-fs--sh u-mt20">希望雇用形態</h2>
			<?php $entryInfo['hope_employment'] = str_replace('、', ' / ', $entryInfo['hope_employment'])?>
			<div class="-primary__table__check u-fs--l u-mt20">{{$entryInfo['hope_employment'] or ''}}</div>
			
			
			
			<div class="form__button__block--multi u-mt30">	
				<p class="u-fs--s u-mt30 u-txtc">
					<a target="_blank" href="/about/privacypolicy" class="form__text--link" style="display:inline">利用規約</a>に同意して
				</p>
				<div class="_block--multi__inner u-mt10"><a href="{{ $entryInfo['entryURL'] or '#'}}" class="c-button-square--l--gray u-fs--xs u-fwb">修正する</a>
					<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">送信する</span></button>
				</div>
			</div>
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		</form>
	</div>
</div>
@stop