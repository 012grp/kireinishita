<?php
	$class_primary_box = 'u-mt20';
	$__title = _helper::getWebInfo('title_entry');
	$web_title = _helper::set_title($__title);
	$description = sprintf(trans('webinfo.meta_desc_entry'), $secret['name'],$secret['job_name']);
	$keywords = sprintf(trans('webinfo.keywords_entry'), $secret['name']);

	$str_error_birthday = "生年月日を入力してください。";
	$str_error_address = "住所を入力してください";
	$str_error_invalid = 'Value is invalid';
	$style_display_block = "style='display: block;'";

	if($errors->has('year') || $errors->has('month') || $errors->has('day'))
	{
		$style_birthday = $style_display_block;
		$error_birthday = $str_error_birthday;
	}

	if($errors->has('province') || $errors->has('city'))
	{
		$style_address = $style_display_block;
		$error_address = $str_error_address;
	}
	
	

	$job_objective_value = null;
	$hope_employment_value = null;
	$provinceId = null;
	$cityId = null;
	$listCity = null;
	if(Session::has('entry'))
	{
		$confirmInfo = Session::get('entry');

		if(isset($confirmInfo['gender_value']))
		{
			if($confirmInfo['gender_value'] == '1')
			{
				$radio_male_checked = 'checked = "checked"';
				$radio_female_checked = '';
			}
			else
			{
				$radio_female_checked = 'checked = "checked"';
				$radio_male_checked = '';
			}
		}

		if(isset($confirmInfo['job_objective_value']))
		{
			$job_objective_value = $confirmInfo['job_objective_value'];
		}
		else
		{
			$job_objective_value = Input::old('job_objective');
		}

		if(isset($confirmInfo['hope_employment_value']))
		{
			$hope_employment_value = $confirmInfo['hope_employment_value'];
		}
		else
		{
			$hope_employment_value = Input::old('hope_employment');
		}

		if(isset($confirmInfo['province']))
		{
			$provinceId = $confirmInfo['province'];
			//get list city by province id
			$listCity = _helper::getCitiesByProvince($provinceId);
		}

		if(isset($confirmInfo['city']))
		{
			$cityId = $confirmInfo['city'];
		}

		if(isset($confirmInfo['pr']))
		{
			$pr = $confirmInfo['pr'];
		}
		else
		{
			$pr = '';
		}
		
		if(isset($confirmInfo['situation']))
		{
			$situation = $confirmInfo['situation'];
		}
		else
		{
			$situation = 1;
		}
		
		if(isset($confirmInfo['join']))
		{
			$join = $confirmInfo['join'];
		}
		else
		{
			$join = 1;
		}
	}
	else
	{
		$job_objective_value = Input::old('job_objective');
		$hope_employment_value = Input::old('hope_employment');
		$situation = 1;
		$join = 1;
		$pr = Input::old('pr');
	}

	$currentYear = date("Y");
	$birthdayFromYear = $currentYear - 90;
	$birthdayToYear = $currentYear - 18;


	$listEm = [];
	$__listEm = $secret['list_em'];
	if($__listEm)
	{
		$listEm = _helper::getArrayEmByListEm($__listEm);
	}
	

	$listJob = [];
	$__listJob = $secret['list_job'];
	if($__listJob)
	{
		$listJob = _helper::getArrayJobCategoryByListJob($__listJob);
	}
	
?>
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
 	@include('breadcrumb.default', array(
							'page' 	=> '応募フォーム'
						))
@stop

@section('js')
	<script type="text/javascript">
		var provinceId = '<?php if(isset($provinceId)) echo $provinceId; else echo 'null'; ?>';
		var cityId = '<?php if(isset($cityId)) echo $cityId; else echo 'null'; ?>';
	</script>
@stop


@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">

		@if (Session::has('flash_message'))
		    <div class="alert alert-info">{{ Session::get('flash_message') }}</div>
		@endif

		<div class="form--primary__topBlock">
			<h1 class="column-left__ttl--normal u-fs--xxl--contents">応募フォーム</h1>
			<div class="flow__arrow u-fs--s"><span class="c-icon--flow--l--is">1.登録</span><span class="c-icon--flow--c">2.確認</span><span class="c-icon--flow--r">3.応募完了</span></div>
		</div>
		<div class="form--primary u-mt10">
			<p class="form--primary__repletion u-fs--xs--lh--form u-fwb">{{$secret['name'] or ''}}</p>
			<p class="form--primary__repletion u-fs--xs--lh--form"><span>{{$secret['average_price'] or ''}}</span><span>{{$secret['address'] or ''}}</span></p>
			<form class="u-mt15" method="POST" action="/entry/confirm">
				<h2 class="form--primary__heading u-fs--sh">基本情報</h2>
				<table class="form--primary__table--noBorder">
					<tbody>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="name">お名前</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								@if($errors->has('name'))
									<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('name')}}</span>
								@endif
								<div>
									<p data-error="required" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>お名前を入力してください。</p>
								</div>
								<input id="name" name="name" type="text" placeholder="お名前を入力してください" data-validate="target" required class="form--primary__text u-fs--l" value="{{$confirmInfo['name'] or Input::old('name')}}">
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="kana">フリガナ</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								@if($errors->has('kana'))
									<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('kana')}}</span>
								@endif
								<div>
									<p data-error="required" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>お名前を入力してください。</p>
									<p data-error="kana" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>カタカナで入力してください。</p>
								</div>
								<input id="kana" name="kana" type="text" placeholder="カタカナで入力してください" data-kana="target" data-validate="target" required class="form--primary__text u-fs--l" value="{{$confirmInfo['kana'] or Input::old('kana')}}">
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="name">性別</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								<div class="-contact__radio__block">
									@if($errors->has('gender'))
										<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('gender')}}</span>
									@endif
									<input type="radio" name="gender" id="radio1" value="1" {{$radio_male_checked or 'checked="checked"'}} class="form--primary__radio">
									<label for="radio1" class="u-fs--l">男</label>
									<input type="radio" name="gender" id="radio2" value="2" {{$radio_female_checked or ''}} class="form--primary__radio">
									<label for="radio2" class="u-fs--l">女</label>
								</div>
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="name">生年月日</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td data-validate-multi="target" class="form--primary__detail form--primary__txtr">
								@if($errors->has('birthday_invalid'))
									<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$str_error_invalid}}</span>
								@endif
								<div>
									<p data-error-multi="selected" class="form__error u-fs--xs--lh--form" {!! $style_birthday or ''!!}><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$error_birthday or '生年月日を入力してください。'}}</p>
								</div>
								<div class="quick-search__select__wrap c-icon__wrapper--triangle">
									<select name="year" class="quick-search__select--l u-fs--m">
										<option value="">--</option>
										@for($i = $birthdayToYear; $i >= $birthdayFromYear; $i--)
											@if((isset($confirmInfo['year']) && $confirmInfo['year'] == $i) || Input::old('year') == $i)
												<option value="{{ $i }}" selected>{{ $i }}</option>
											@else
												<option value="{{ $i }}">{{ $i }}</option>
											@endif
										@endfor 年
									</select>
								</div>
								<div class="quick-search__select__wrap c-icon__wrapper--triangle">
									<select name="month" class="quick-search__select--s u-fs--m">
										<option value="">--</option>
										@for($i = 1; $i <= 12; $i++)
											@if((isset($confirmInfo['month']) && $confirmInfo['month'] == $i) || Input::old('month') == $i)
												<option value="{{ $i }}" selected>{{ $i }}</option>
											@else
												<option value="{{ $i }}">{{ $i }}</option>
											@endif
										@endfor 月
									</select>
								</div>
								<div class="quick-search__select__wrap c-icon__wrapper--triangle">
									<select name="day" class="quick-search__select--s u-fs--m">
										<option value="">--</option>
										@for($i = 1; $i <= 31; $i++)
											@if((isset($confirmInfo['day']) && $confirmInfo['day'] == $i)|| Input::old('day') == $i)
												<option value="{{ $i }}" selected>{{ $i }}</option>
											@else
												<option value="{{ $i }}">{{ $i }}</option>
											@endif
										@endfor 日
									</select>
								</div>
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="mail">メールアドレス</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								@if($errors->has('email'))
									<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('email')}}</span>
								@endif
								<div>
									<p data-error="mail" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>メールアドレスの入力に誤りがあります。</p>
									<p data-error="required" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>メールアドレスを入力してください。</p>
								</div>
								<input id="mail" name="email" type="text" placeholder="例） example@runda.com" data-mail="target" data-validate="target" required class="form--primary__text u-fs--l" value="{{$confirmInfo['email'] or Input::old('email')}}">
								<p class="form--primary__repletion u-fs--xxs-lh u-mt5">※こちらのメールアドレスにご返信させていただきます。<br>お手数ですがメールが受信可能かご確認ください。</p>
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="name">住所</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td data-validate-multi="target" class="form--primary__detail form--primary__txtr">
								@if($errors->has('province_invalid') || $errors->has('city_invalid'))
									<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$str_error_invalid}}</span>
								@endif
								<div>
									<p data-error-multi="selected" class="form__error u-fs--xs--lh--form" {!! $style_address or ''!!}><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$error_address or '住所を入力してください'}}</p>
								</div>
								<div class="quick-search__select__wrap c-icon__wrapper--triangle">
									<select name="province" class="quick-search__select--l u-fs--m" id="pro_select_cities">
										<option value="">都道府県</option>
										<?php $provinces = _helper::getProvinceNames(); ?>
										@foreach($provinces as $province)
											@if((isset($confirmInfo['province']) && $confirmInfo['province'] == $province['id']))
												<option value="{{ $province['id'] }}" selected>{{ $province['name'] }}</option>
											@else
												<option value="{{ $province['id'] }}">{{ $province['name'] }}</option>
											@endif
										@endforeach 
									</select>
								</div>
								<div class="quick-search__select__wrap c-icon__wrapper--triangle">
									<input type="hidden" id="selectedCityId" value="{{$confirmInfo['city'] or ''}}">
									<select name="city" class="quick-search__select--l u-fs--m" id="city_checkboxes">
										<option value="">市区町村</option>
									</select>
								</div>
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="tel">電話番号</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								@if($errors->has('tel'))
									<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('tel')}}</span>
								@endif
								<div>
									<p data-error="required" class="form__error u-fs--xs--lh--form" ><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>電話を入力してください。</p>
									<p data-error="num" class="form__error u-fs--xs--lh--form"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>半角数字を入力してください。</p>
								</div>
								<input id="tel" name="tel" type="text" placeholder="09000000000" data-validate="target" data-num="target" required class="form--primary__text u-fs--l" value="{{$confirmInfo['tel'] or Input::old('tel')}}">
							</td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="situation">現在の就業状況</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								<div class="-contact__radio__block--secondary">
									@if($errors->has('situation'))
										<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('situation')}}</span>
									@endif
									@foreach(trans('entry.current_situation') as $index=>$item)
									<input {{ $situation == $index ? 'checked': '' }} type="radio" name="situation" id="situation{{$index}}" value="{{$index}}" class="form--primary__radio">
									<label for="situation{{$index}}" class="u-fs--l">{{$item}}</label>
									@endforeach
								</div>
							</td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="situation">入社可能時期</label><span class="c-tag--form--required u-fs--xxs">必須</span>
							</td>
							<td class="form--primary__detail">
								<div class="-contact__radio__block--tertiary">
									@if($errors->has('join'))
										<span class="form__error u-fs--xs--lh--form" style="display: block;"><span class="c-icon--triangle--attention u-fs--xxxs"><span>!</span></span>{{$errors->first('join')}}</span>
									@endif
									@foreach(trans('entry.available_join') as $index=>$item)
									<input {{ $join == $index ? 'checked': '' }} type="radio" name="join" id="join{{$index}}" value="{{$index}}" class="form--primary__radio">
									<label for="join{{$index}}" class="u-fs--l">{{$item}}</label>
									@endforeach 
								</div>
							</td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
							  <label for="contact">希望連絡時間・連絡方法</label>
							</td>
							<td class="form--primary__detail">
							  <input value="{{$confirmInfo['contact'] or Input::old('contact')}}" maxlength="300" name="contact" id="contact" type="text" placeholder="例）平日午前中/メール連絡希望" class="form--primary__text u-fs--l">
							  <p class="form--primary__repletion u-fs--xxs-lh u-mt5">※連絡のつく時間帯、ご希望の連絡方法があれば入力してください。</p>
							</td>
						</tr>
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
							  <label for="capabilities">取得資格</label>
							</td>
							<td class="form--primary__detail">
							  <input value="{{$confirmInfo['capabilities'] or Input::old('capabilities')}}" maxlength="300" name="capabilities" id="capabilities" type="text" placeholder="例) 美容師免許/柔道整復師/看護師 など" class="form--primary__text u-fs--l">
							</td>
						</tr>
						
						<tr class="-contact__table__row">
							<td class="form--primary__ttl u-fs--l u-fwb">
								<label for="view">職務経歴・自己PR</label>
							</td>
							<td class="form--primary__detail">
								<textarea id="pr" maxlength="2000" name="pr" placeholder="{{trans('entry.pr_placehoder')}}" class="form--primary__textarea form--primary__textarea--resize u-fs--lh--contents">{{$pr}}</textarea>
							</td>
						</tr>
						
					</tbody>
				</table>
				@if(!empty($listJob))
				<h2 class="form--primary__heading u-fs--sh">希望職種</h2>
				<div class="-primary__table__check u-fs--xs" style="padding-left: 10px;">
					<?php $type = count($listJob) == 1 ? 'hidden' : 'checkbox';
					$class = count($listJob) == 1 ? '' : 'search-checkbox';
					?>
					@foreach($listJob as $index=>$value)
						@if($job_objective_value)
							@if(in_array($value, $job_objective_value))
								<input checked="checked" type="{{$type}}" name="job_objective[]" id="check_em_{{$index}}" class="{{$class}}" value="{{$value}}">
							@else
								<input type="{{$type}}" name="job_objective[]" id="check_em_{{$index}}" class="{{$class}}" value="{{$value}}">
							@endif
						@else
							<input type="{{$type}}" name="job_objective[]" id="check_em_{{$index}}" class="{{$class}}" value="{{$value}}">
						@endif
						
						<label for="check_em_{{$index}}" class="search-checkbox__label u-mt20">{{$value}}</label>
					@endforeach
				</div>
				@endif
				@if(!empty($listEm))
				<h2 class="form--primary__heading u-fs--sh u-mt20">希望雇用形態</h2>
				<div class="-primary__table__check u-fs--xs" style="padding-left: 10px;">
					<?php $type = count($listEm) == 1 ? 'hidden' : 'checkbox';
                    $class = count($listEm) == 1 ? '' : 'search-checkbox'; ?>
					@foreach($listEm as $index=>$value)
						@if($hope_employment_value)
							@if(in_array($value, $hope_employment_value))
								<input checked="checked" type="{{$type}}" name="hope_employment[]" id="check_job_{{$index}}" class="{{$class}}" value="{{$value}}">
							@else
								<input type="{{$type}}" name="hope_employment[]" id="check_job_{{$index}}" class="{{$class}}" value="{{$value}}">
							@endif
						@else
							<input type="{{$type}}" name="hope_employment[]" id="check_job_{{$index}}" class="{{$class}}" value="{{$value}}">
						@endif
						<label for="check_job_{{$index}}" class="search-checkbox__label u-mt20">{{$value}}</label>
					@endforeach 
				</div>
				@endif
				<div class="form__button__block u-mt30">
					<p class="u-fs--s u-mt30 u-txtc"><a target="_blank" href="/about/privacypolicy" class="form__text--link">利用規約</a>に同意して
						<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xxl u-fwb"><span class="button-inner">確認する</span></button>
					</p>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div>
	</div>
@stop