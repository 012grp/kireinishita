<?php 
	$class_primary_box = 'u-mt20';
	$__title = _helper::getWebInfo('title_entry_thanks');
	$web_title = _helper::set_title($__title);
?>
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
 	@include('breadcrumb.default', array(
							'page' 	=> '応募フォーム'
						))
@stop

@section('column_left')
 	<div data-target-leftcolumn="target" class="column-left">
		<div class="form--primary__topBlock">
			<h1 class="column-left__ttl--normal u-fs--xxl--contents">応募フォーム</h1>
			<div class="flow__arrow u-fs--s"><span class="c-icon--flow--l">1.登録</span><span class="c-icon--flow--c">2.確認</span><span class="c-icon--flow--r--is">3.応募完了</span></div>
		</div>
		<div class="form--primary u-mt10">
			<p class="form--primary__thanks u-fs--xxlh--contents u-fwb u-mt35">応募が完了しました。<br>誠にありがとうございます。</p>
			<p class="-contact__thanks__text u-fs--lh--contents u-mt50">企業の担当者からご連絡いたしますので、<br>今しばらくお待ちください。</p>
			<div class="-contact__button__block u-mt50">
				<div class="c-button-square--l u-fs--xxl u-fwb"><a href="/">HOMEへ</a></div>
			</div>
		</div>
	</div>
@stop

			
