<?php 
  $class_primary_box = 'u-mt20';
  $__title = _helper::getWebInfo('title_entry_thanks');
  $web_title = _helper::set_title($__title);
?>
@extends('layouts.default_sp')

@section('page_sp')
<div class="primary-box--noPadding">
        <div class="form--primary__topBlock">
          <h1 class="form--primary__topBlock__ttl u-fs--m u-fwb">応募フォーム</h1>
          <div class="flow__arrow u-fs--xs--contents"><span class="c-icon--flow--l">1.登録</span><span class="c-icon--flow--c">2.確認</span><span class="c-icon--flow--r--is">3.応募完了</span></div>
        </div>
        <div class="primary-box--noPadding primary-box--fixHeight">
          <div class="form--primary form--primary--contact">
            <p class="form--primary__thanks u-fs--mh u-fwb u-mt35">応募が完了しました。<br>誠にありがとうございます。</p>
            <p class="-contact__thanks__text u-fs--xsh u-mt50">企業の担当者からご連絡いたしますので、<br>今しばらくお待ちください。</p>
          </div>
          <div class="form__button__block--noborder u-mt30">
            <p class="u-fs--s u-mt30 u-txtc"><a href="{{ Request::root() }}" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">HOMEへ</span></a></p>
          </div>
        </div>
      </div>
@stop