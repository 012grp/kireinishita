<?php 
/*
Note: couldn't declare 2 @extends, even in IF ELSE
*/
?>
@if (App\Http\Components\Util::isMobile())
	<?php $layout = 'default_sp'; ?>
	@section('page_sp')
	<div class="primary-box">
		<div data-return="target" class="primary-box--full__inner">
			<div class="primary-box--full__content">
				<h2>Out of date</h2>
				<p class="u-fs--s u-mt30">管理者に連絡してください。</p>
				<div class="c-button-square--l u-fs--l u-fwb u-mt30"><a href="/">HOMEへ戻るんだ</a></div>
			</div>
		</div>
	</div>
	@stop
@else
	<?php $layout = 'errors'; ?>
	@section('content')
	  <div class="primary-box--full">
		<div data-return="target" class="primary-box--full__inner">
		  <div class="primary-box--full__content">
			<h2>Out of date</h2>
			<p class="u-fs--xxl u-mt40">管理者に連絡してください。</p>
			<div class="c-button-square--l u-fs--xxl u-fwb u-mt60"><a href="/">HOMEへ戻るんだ</a></div>
		  </div>
		</div>
	  </div>
	@stop
@endif
    
@extends('layouts.'.$layout)

    