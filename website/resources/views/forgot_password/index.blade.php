<?php
$class_primary_box = 'u-mt20';
$__title = _helper::getWebInfo('title_account_forget_password');
$web_title = _helper::set_title($__title);
$description = trans('webinfo.meta_desc_account_forget_password');
$keywords = trans('webinfo.keywords_account_forget_password');
?>
@extends('layouts.default')

@section('column_right')
    @include('shared.right_column.no_article')
@stop

@section('breadcrumb')
    @include('breadcrumb.default', array(
                           'page' 	=> 'パスワードを再設定する',
                            'parent' => array(array('link' => '/account', 'title' => 'ログイン/新規登録'))
                       ))
@stop

@section('column_left')
    <div data-target-leftcolumn="target" class="column-left">
        <h1 class="column-left__ttl--normal u-fs--xxl--contents">パスワードを再設定する</h1>
        <div class="form--login u-mt10">
            <div class="form--login__middle u-mt50">
                <div class="-login__middle__ttl u-fs--xl u-fwb">パスワードを再設定する</div>
                @if ($errors->has('sent_reset_mail'))
                    <p class="u-fs--xxl u-mt40">{{ $errors->first('sent_reset_mail')}}</p>
                    <div class="c-button-square--l u-fs--xxl u-fwb u-mt60"><a href="/">HOMEへ戻るんだ</a></div>
                @else
                    @if($errors->has('fogot_password'))
                        <p class="form__error u-fs--xs--lh--form">{{ $errors->first('fogot_password')}}</p>
                    @endif
                    <form method="POST" action="/account/forget-password">
                        <input type="email" placeholder="メールアドレス" autocomplete="on"
                               class="form--primary__text u-fs--l u-mt30" name="email">
                        <button type="submit" action="" data-submit="trigger"
                                class="c-button-square--l u-fs--xl u-fwb u-mt20"><span
                                    class="button-inner">再設定メールを送信する</span></button>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    </form>
                @endif
            </div>
        </div>
    </div>
@stop