<?php
	$noEmpty = false;
	
	//change category to Array if not
	$requestCat = isset($request['cat'])?
			_common::changeToArray($request['cat'])
			:
			[];
	//change category to Array if not
	$requestHang = isset($request['hang'])?
			_common::changeToArray($request['hang'])
			:
			[];

	$requestCity = isset($request['city'])?
			_common::changeToArray($request['city'])
			:
			[];
	$requestWaysite = isset($request['way'])?
		_common::changeToArray($request['way'])
		:
		[];

	$requestStation = isset($request['station'])?
		_common::changeToArray($request['station'])
		:
		[];
	
	$aliasStation = empty($requestStation)?null:_helper::getStationByIds($requestStation);

	$job_category 			= _helper::getJobCategory();
	$job_mergedCategory 	= _common::createIdNameList($job_category);

	$hangup_keywords 		= _helper::getHangUp();
	$hangup_mergedKeywords 	= _common::createIdNameList($hangup_keywords);
?>

<h1 class="column-left__search__breadcrumb u-fs--xl u-fwb u-mt20">
	@if(!empty($requestCat))
		@foreach($requestCat as $cat)
			@if(isset($job_mergedCategory[$cat]))
				<a href="/job/result?cat={{$cat}}">{{$job_mergedCategory[$cat]}}</a>/
			@endif
		@endforeach
	<?php $noEmpty = true; ?>
	@endif

	@if(!empty($request['pro']))
		<a href="/job/result?pro={{$request['pro']}}">{{ _helper::getProvinceNameById($request['pro'])}}</a>/
		<?php $noEmpty = true; ?>
	@endif

	@if(!empty($requestCity))
		@foreach($requestCity as $city)
				<a href="/job/result?pro={{$request['pro']}}&city={{$city}}">{{_helper::getCityNameById($city)}}</a>/
		@endforeach
		<?php $noEmpty = true; ?>
	@endif

	@if(!empty($requestWaysite))
		@foreach($requestWaysite as $way)
			<a href="/job/result?pro={{$request['pro']}}&way={{$way}}">{{_helper::getWaysiteNameById($way)}}</a>/
		@endforeach
		<?php $noEmpty = true; ?>
	@endif

	@if($aliasStation)
		@foreach($aliasStation as $station)
			<a href="/job/result?pro={{$request['pro']}}&station={{$station['code']}}">{{$station['name']}}</a>/
		@endforeach
		<?php $noEmpty = true; ?>
	@endif

	@if(!empty($request['em']))
		@if (is_array($request['em']))
			@foreach($request['em'] as $emValue)
				<a href="/job/result?em={{$emValue}}">{{_helper::getEmploymentNameById($emValue)}}</a>/
			@endforeach 
		@else
			<a href="/job/result?em={{$request['em']}}">{{_helper::getEmploymentNameById($request['em'])}}</a>/
		@endif
		
		<?php $noEmpty = true; ?>
	@endif

	@if(!empty($requestHang))
		@foreach($requestHang as $hangName)
			@if(isset($hangup_mergedKeywords[$hangName]))
				<a href="/job/result?hang={{$hangName}}">{{$hangup_mergedKeywords[$hangName]}}</a>/
			@endif
		@endforeach
		<?php $noEmpty = true; ?>
	@endif

	@if(!empty($request['k']))
		<a href="/job/result?k={{$request['k']}}">{{$request['k']}}</a>/
		<?php $noEmpty = true; ?>
	@endif

	@if($noEmpty)
	の求人転職情報
	@else
	求人情報
	@endif
</h1>
<?php 
	$end2= _common::microtime();
	echo '<!--4. '.($end2 - $start2).'-->';
?>