@if(empty($is_custom_paginate))
<div class="column-left__info {{ $class or 'u-mt20'}}">
	<div class="column-left__info__text u-fs--l">
		<span class="u-fs--xxxl--recruit">{{ $total or '0' }}</span>件の求人
	</div>
	<div class="pager">
		@if($contents)
			@include('pagination.default', array('paginator' => $contents))
		@endif
	</div>
</div>
@else
<div class="column-left__info {{ $class or 'u-mt20'}}">
	<div class="column-left__info__text u-fs--l">
		<span class="u-fs--xxxl--recruit">{{ $total or '0' }}</span>件の求人
	</div>
	<div class="pager">
		@if($total > 0)
			@include('pagination.custom_paginate', array('paginator' => $search_result))
		@endif
	</div>
</div>
@endif
