<?php
$path = Request::path();

$job_name = _common::getJobName($job_name);

//会社
$job_company = strip_tags($job_company);

//給与
$job_salary = strip_tags($job_salary);

//勤務地
$job_location = strip_tags($job_location);

$rel = 'nofollow';

if(!empty($is_secret))
{
	$rel = '';
	
	$data_secret = 1;

    //職種
    $job_category = _helper::getCategoryNamesByListId($list_job_parent, $list_job);

    //雇用形態
    $job_career = _helper::getCategoryNamesByListId('', $list_hangup);

    //こだわり
    $job_type = _helper::getEmploymentNamesByListId($list_em);
} else {
    $data_secret = 0;
    $job_category = $job_career;
    $job_career = _common::getJobCareer($requirement_special);

}

$site_name = trans('job_site_name.' . $site_id);


if (empty($job_url)) {
    $secretUrl = _helper::getUrlName('secretPage');
    $job_url = '/' . $secretUrl . '/' . $id;
} else {
    $job_url = _helper::make_job_url($id);
}


//TODO: check image
if (!isset($job_image) || empty($job_image)) {
    $job_image = _common::noImageUrlContentsBySiteId($site_id);
} else {
    //$rs = strpos($job_image, 'http');
    if ($job_image[0] != '/') {
        //$job_image = '//'.$job_image;
        $job_image = _common::proxy_image($job_image);
    }
}

//
////TODO: set name length to show
//if(mb_strlen($job_name) > 50)
//{
//	$job_name = mb_substr($job_name,0,50,'utf-8').'...';
//}
//
////TODO: set location length to show
//if(mb_strlen($job_location) > 30)
//{
//	$job_location = mb_substr($job_location,0,30,'utf-8').'...';
//}
?>
<div class="column-box--wide__list {{ $job_class or 'u-mt20'}}">
    <h2 class="u-fs--xlh -wide__list__ttl--multiTag u-fwb">
        <span class="_list__multiTag__text">{{ $job_name }}</span>
        <span class="_list__multiTag__block">
			@if(!empty($is_secret))
                <span class="u-fs--xxs c-tag--secret--flow">オリジナル</span>
            @endif

            @if($is_new && $is_secret==1)
                <span class="u-fs--xxs c-tag--new--flow">NEW</span>
            @endif

            @if(!empty($is_secret) && isset($celebration) && !empty($celebration))
                <span class="u-fs--xxs c-tag--celebration">{{$celebration}}</span>
            @endif
		</span>
	</h2>
	<h3 class="-wide__list__subTtl u-fs--l u-mt20">
		{{ $job_company }}
		<?php echo (!empty($store_name)) ? '(' .$store_name . ')' : '' ;  ?>
	</h3>
	<div class="-wide__list__bottom u-mt20">
		<div style="background-image:url({{$job_image}})" class="-wide__list__img"></div>
		<ul class="-wide__list__details u-fs--xs--lh">
			<li class="-wide__list__detail--multiline"><span class="_list__detail__title">職種</span><span>：{{$job_category or ''}}</span></li>
			<li class="-wide__list__detail--multiline"><span class="_list__detail__title">給与</span><span>：{{$job_salary}}</span></li>
			<li class="-wide__list__detail"><span class="_list__detail__title">勤務地</span><span>：{{$job_location}}</span></li>
			<li class="-wide__list__detail"><span class="_list__detail__title">雇用形態</span><span>：{{$job_type}}</span></li>
			<li class="-wide__list__detail"><span class="_list__detail__title">こだわり</span><span>：{{$job_career}}</span></li>
		</ul>
		<div class="_list__button__block u-mt20">
			<div data-id="{{$id}}" data-secret="{{$data_secret}}" class="c-button-square--mm--gray u-fs--l bookmark_job">
				@if($path == 'mybox')
					削除する
				@else
					<span class="c-font-icon--attachment"></span>キープする
				@endif
			</div>
			<div class="c-button-square--mm u-fs--l">
				<a rel="{{$rel}}" target="{{$url_target or '_blank'}}" href="{{$job_url}}">もっと詳しくみる</a>
			</div>
		</div>
		@if(!$is_secret)
			<p class="-wide__list__footer u-fs--xxs">提供元：{{$site_name}}</p>
		@endif
	</div>
</div>