<?php
//Array: 勤務地
$province_list = _helper::getProvinceNames();

//雇用形態
$employment_list = _helper::getEmployment();
//職種
$job_category = _helper::getJobCategory();
$job_mergedCategory = _common::createIdNameList($job_category);
//こだわり
$hangup_keywords = _helper::getHangUp();
$hangup_mergedKeywords = _common::createIdNameList($hangup_keywords);

$requestPro = isset($request['pro']) ? $request['pro'] : '';

//change employ to Array if not
$requestEm = isset($request['em']) ?
        _common::changeToArray($request['em'])
        :
        [];

//change category to Array if not
$requestCat = isset($request['cat']) ?
        _common::changeToArray($request['cat'])
        :
        [];


//change category to Array if not
$requestHang = isset($request['hang']) ?
        _common::changeToArray($request['hang'])
        :
        [];

$requestCity = isset($request['city']) ?
        _common::changeToArray($request['city'])
        :
        [];

$requestWaysite = isset($request['way']) ?
        _common::changeToArray($request['way'])
        :
        [];


$requestStation = isset($request['station']) ?
        _common::changeToArray($request['station'])
        :
        [];
//Cities when selected province
$citiesBySelectedProvince = _helper::getCitiesByProvince($requestPro);
?>


<input type="hidden" id="requestWaysite" value="{{implode(',',$requestWaysite)}}">
<input type="hidden" id="requestStation" value="{{implode(',',$requestStation)}}">
<form method="get" action="/job/result" id="searchForm" name="searchForm" data-search="advance">
    <table class="search-table u-fs--xs">
        <tr class="search-table__row">
            <td class="search-table__type">職種</td>
            <td colspan="2" class="search-table__right">
                <div data-modal-trigger="jobmodal" class="c-button-square--ms--gray u-fs--xs u-mr10">
                    選択する<span>&gt;</span></div>
                <div data-target-addtag="job" class="check__tag__addblock">
                    @foreach($requestCat as $catName)
                        @if(isset($job_mergedCategory[$catName]))
                            <span class="c-tag--detail--table">{{$job_mergedCategory[$catName]}}</span>
                        @endif
                    @endforeach
                </div>
            </td>
        </tr>
        <tr class="search-table__row">
            <td class="search-table__type">勤務地</td>
            <td colspan="2" class="search-table__right--side">
                <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                    <select name="pro" id="pro_select" class="quick-search__select u-fs--m"
                            data-trigger-location="trigger">
                        <option value="">都道府県</option>
                        @foreach($province_list as $province)
                            @if ($province['id']==$requestPro)
                                <option value="{{$province['id']}}" selected>{{$province['name']}}</option>
                            @else
                                <option value="{{$province['id']}}">{{$province['name']}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div data-modal-trigger="prefmodal" class="c-button-square--ms--gray is-disable u-fs--xs">市区町村の選択<span>&gt;</span>
                </div>
                <div data-modal-trigger="waysidemodal" class="c-button-square--ms--gray is-disable u-fs--xs">
                    沿線・駅の選択<span>&gt;</span></div>
                <div data-target-addtag="pref" class="check__tag__addblock">
                    @foreach($citiesBySelectedProvince as $city)
                        @if(isset($requestCity[$city['id']]))
                            <span class="c-tag--detail--table">{{$city['name']}}</span>
                        @endif
                    @endforeach
                </div>
                <div data-target-addtag="wayside" class="check__tag__addblock"></div>
            </td>
        </tr>
        <tr class="search-table__row">
            <td class="search-table__type">雇用形態</td>
            <td colspan="2" class="search-table__right">
                <?php $index = 0; ?>
                @foreach($employment_list as $record)
                    <?php
                    $key = $record['id'];
                    $em = $record['name'];
                    $index++;
                    $emChecked = isset($requestEm[$key]) ? 'checked' : '';
                    ?>
                    <input {{$emChecked}} type="checkbox" name="em[]" value="{{$key}}" id="em_check{{$index}}"
                           class="search-checkbox em-checkbox">
                    <label for="em_check{{$index}}" class="search-checkbox__label">{{$em}}</label>
                @endforeach
            </td>
        </tr>
        <tr class="search-table__row">
            <td class="search-table__type">こだわり</td>
            <td colspan="2" class="search-table__right">
                <div data-modal-trigger="hangupmodal" class="c-button-square--ms--gray u-fs--xs u-mr10">
                    選択する<span>&gt;</span></div>
                <div data-target-addtag="hangup" class="check__tag__addblock">
                    @foreach($requestHang as $hangName)
                        @if(isset($hangup_mergedKeywords[$hangName]))
                            <span class="c-tag--detail--table">{{$hangup_mergedKeywords[$hangName]}}</span>
                        @endif
                    @endforeach
                </div>
            </td>
        </tr>
        <tr class="search-table__row">
            <td class="search-table__type">キーワード</td>
            <td class="search-table__right--multiple">
                <input id="search_keyword" type="text" class="search-table__text" name="k"
                       value="{{$request['k'] or ''}}">
            </td>
            <td class="_right--multiple--btn">
                <input id="btnAdvanceSubmit" type="submit" value="{{$total or '0'}}件を検索"
                       class="c-button-square--mm u-fs--l">
            </td>
        </tr>
    </table>
    <div data-remodal-id="jobmodal" data-search-id="job" class="modal">
        @include('job._search_table_modal_content', array(		'modal_title' => '職種',
                                                                'modal_data' => $job_category,
                                                                'modal_prefix' => 'job',
                                                                'input_name' => 'cat[]',
                                                                'checked_items' => $requestCat
                                                                ))
    </div>
    <div data-remodal-id="prefmodal" data-search-id="pref" class="modal prefmodal">
        <h1 class="modal__ttl u-fs--l">市区町村</h1>
        <div class="modal__inner u-fs--xs">
            <div class="modal__checkbox" id="city_checkboxes">
                @foreach($citiesBySelectedProvince as $city)
                    <?php
                    $checked = '';
                    if (isset($requestCity[$city['id']])) {
                        $checked = 'checked';
                    }
                    ?>
                    <input {{$checked}} value="{{$city['id']}}" form="searchForm" type="checkbox" name="city[]"
                           id="city{{$city['id']}}" data-num="{{$city['id']}}" class="search-checkbox">
                    <label for="city{{$city['id']}}"
                           class="search-checkbox__label--modal u-mt10">{{$city['name']}}</label>
                @endforeach
            </div>
            <div class="modal__btn">
                <div data-remodal-action="close" data-click-action="count"
                     class="c-button-square--mm close_modal_button">閉じる
                </div>
            </div>
        </div>
    </div>
    <div data-remodal-id="waysidemodal" data-search-id="wayside" class="modal waysidemodal">
        <h1 class="modal__ttl modal__ttl--fix u-fs--l">沿線・駅</h1>
        <div class="modal__inner u-fs--xs">
            <div class="modal__checkbox" id="waysite_checkboxes">
            </div>
            <div class="modal__btn modal__btn--fixed">
                <div data-remodal-action="close" data-click-action="count"
                     class="c-button-square--mm close_modal_button">閉じる
                </div>
            </div>
        </div>
    </div>
    <div data-remodal-id="hangupmodal" data-search-id="hangup" class="modal">
        @include('job._search_table_modal_content', array(		'modal_title' => 'こだわり',
                                                                'modal_data' => $hangup_keywords,
                                                                'modal_prefix' => 'hangup',
                                                                'input_name' => 'hang[]',
                                                                'checked_items' => $requestHang
                                                                ))
    </div>
</form>