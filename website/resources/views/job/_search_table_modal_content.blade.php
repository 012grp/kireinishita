<h1 class="modal__ttl u-fs--l">{{$modal_title}}</h1>
<div class="modal__inner u-fs--xs">
	<?php $GroupMT30 = ''; $GroupIndex = 1; ?>
	@foreach($modal_data as $GroupName=>$List)
		<h2 class="modal__sub__ttl {{$GroupMT30}}">{{$GroupName}}</h2>
		<div class="modal__checkbox">
		@foreach($List as $record)
			<?php
			$Key = $record['id'];
			$Name = $record['name'];
			$checked='';
			if(isset($checked_items[$Key]))
			{
				$checked='checked';
			}
			?>
			<input {{$checked}} form="searchForm" type="checkbox" name="{{$input_name}}" id="{{$modal_prefix}}-{{$GroupIndex}}" data-num="{{$GroupIndex}}" class="search-checkbox" value="{{$Key}}">
			<label for="{{$modal_prefix}}-{{$GroupIndex}}" class="search-checkbox__label--modal u-mt10">{{$Name}}</label>
			<?php $GroupIndex++; ?>
		@endforeach 
		</div>
		<?php $GroupMT30 = 'u-mt30'; ?>
	@endforeach
	<div class="modal__btn">
		<div data-remodal-action="close" data-click-action="count" class="c-button-square--mm close_modal_button">閉じる</div>
	</div>
</div>