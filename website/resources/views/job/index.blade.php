<?php 

	$class_primary_box = 'u-mt20';
	if(isset($custom_web_title))
	{
		$web_title = _helper::set_title($custom_web_title);
	}
	else
	{
		$__title = _helper::getWebInfo('title_job_result');
		$web_title = _helper::set_title($__title);
	}

	if(isset($custom_breadcrumb))
	{
		$breadcrumb = $custom_breadcrumb;
	}
	else
	{
		$breadcrumb = array('page' => '求人検索結果');
	}
	if(empty($keywords))
	{
		$keywords = _helper::getWebInfo('keywords_job_result');
	}

?> 
@extends('layouts.default')

@section('breadcrumb')
	<?php $start = _common::microtime(); ?>
	@include('breadcrumb.default', $breadcrumb)
	<?php 
		$end = _common::microtime();
		echo '<!--3. '.($end - $start).'-->';
	?>
@stop



@section('column_right')
	@include('shared.right_column.default')
@stop


@section('column_left')
	
	<div data-target-leftcolumn="target" class="column-left">

		@include('job._search_table')

		@if($style!='search')
		<?php $start2 = _common::microtime(); ?>
		@include('job._search_breadcrumb')
		<?php 
			$end2= _common::microtime();
			echo '<!--4. '.($end2 - $start2).'-->';
		?>
		@include('job._search_info')
		
		
		<div class="column-box--wide">
			<?php
			function putConciergeBanner($count){
				if($count!=0 && ($count+1)%5==0){
					echo View::make('shared.item.support_banner')->render();
				}
			}

	        _helper::viewMixData($search_result,
	            //job
	            function ($data, $index, $count) {
	                echo View::make('job._search_item', $data)->render();
	                putConciergeBanner($count);
	            },

	            //secret
	            function ($data, $index, $count) {
	            	$data['url_target'] = '';
	                echo View::make('job._search_item', $data)->render();
	                putConciergeBanner($count);
	            },

	            //alliance
	            function ($data, $index, $count) {
	                echo View::make('alliance._search_item', $data)->render();
	                putConciergeBanner($count);
	            }
	            // ,
	            // function ($data, $index) {
	            	
	            // 	if(($index + 1) % 5 == 0)
	            // 	{
	            // 		echo View::make('shared.item.support_banner');
	            // 	}

	            // 	if($data['current_total'] == $index + 1 && ($index + 1)%5 != 0)
            	// 	{
	            // 		echo View::make('shared.item.support_banner');
	            // 	}
	            // }
				
	            );
	        ?>
		</div>

		@include('job._search_info')
		@endif
		
	</div>
	
@stop