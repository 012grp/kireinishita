@extends('layouts.default_sp')

<?php 
if(isset($custom_web_title))
{
	$web_title = _helper::set_title($custom_web_title);
}
else
{
	$__title = _helper::getWebInfo('title_job_result');
	$web_title = _helper::set_title($__title);
}
if(empty($keywords))
{
	$keywords = _helper::getWebInfo('keywords_job_result');
}
?>
@section('page_sp')
    @include('shared_sp.item.job_result_job_search')
    @include('shared_sp.item.job_result')
@stop