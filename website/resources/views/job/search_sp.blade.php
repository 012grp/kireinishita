<?php 
	$__title = _helper::getWebInfo('title_job_search');
	$web_title = _helper::set_title($__title);
	$description = _helper::getWebInfo('meta_desc_search');
	$keywords = _helper::getWebInfo('keywords_job_search');
?>
@extends('layouts.default_sp')

@section('page_sp')
    @include('shared_sp.item.job_search_box')
@stop