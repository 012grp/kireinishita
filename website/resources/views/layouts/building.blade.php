<!DOCTYPE HTML>
<html lang="ja">

<head>
    @include('shared.item.html_head')
    <!--[if lt IE 9]> <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
    <link href="/public/css/building.css" rel="stylesheet" type="text/css" media="screen, print" />
</head>

<body>
    <div id="wrap">

        <div id="contents">
            <img class="position" src="/public/images/under_construction.png" />
            <a href="../index.html">
                <p class="text">TOPへ戻る</p>
            </a>
        </div>

    </div>
</body>

</html>