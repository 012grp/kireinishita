<!DOCTYPE HTML>
<html lang="ja">

<head>
    @include('shared.item.html_head')
    <link href="/public/css/searchstyle.css" rel="stylesheet" type="text/css" media="screen, print" />
    <link rel="stylesheet" href="/public/css/mosaic.css" type="text/css" media="screen, print" />
    <link rel="stylesheet" href="/public/css/jquery.sidr.light.css" type="text/css">
    <!--[if lt IE 9]> <script src="/public/js/html5shiv.min.js"></script> <![endif]-->
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>
</head>

<body>
    <div id="wrap">
        <a id="right-menu" href="#sidr" title="Open menu"><img src="/public/images/menu.png" width="32px" height="32px">
        </a>
        <div id="sidr-right">
            @include('shared.item.sp_right_menu')
        </div>
        
        <header class="header">
           @include('shared.header.content')
        </header>
        
        <div id="contents">
            @yield('content')
        </div>
        
        @if($contents->lastPage() > 1)
        <div class="clearfix"></div>
        <div class="button-group">
            <button class="button btn-primary" id="showMore">Show more</button>
            <span class="loading" id="loading" style="display:none"></span>
        </div>
        @endif
        
        <div class="clearfix"></div>
        <div class="bnrArea_PC">
           @include('shared.footer.default')
        </div>
    </div>
    
    <script type="text/javascript">var request = {!! json_encode($request) !!}; </script>
    <script type="text/javascript" src="/public/js/jquery.sidr.min.js"></script>
    <script type="text/javascript" src="/public/js/mosaic.1.0.1.js"></script>
    <script type="text/javascript" src="/public/js/app.js"></script>
    <script type="text/javascript" src="/public/js/content.min.js"></script>
</body>

</html>