<?php
$page_shared = Request::root();
//$current_url = URL::current();
//$image = '/public/img/concept/main.jpg';
$title_twitter = env('WEB_TITLE');
?>
        <!DOCTYPE html>
<html lang="ja">
<head>
    <title>{{ $web_title or env('WEB_TITLE') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0 maximum-scale=1.0,user-scalable=no">

    <meta name="title" content="{!! $web_title or env('WEB_TITLE') !!}">
    
     @if(isset($detail['meta_desc']) && $detail['meta_desc'] != '' )
    <meta name="description" content="{!! $detail['meta_desc'] !!}">
    @else
    <meta name="description" content="{!! $description or env('DESCRIPTION') !!}">
    @endif
     @if(isset($detail['meta_keyword']) && $detail['meta_keyword'] != '')
    <meta name="keywords" content="{!! $detail['meta_keyword'] !!}">
    @else
    <meta name="keywords" content="{!! $keywords or env('KEYWORDS') !!}">
    @endif
   
   	@if (isset($web_custom_title))
	<meta property="og:title" content="{!! $web_custom_title !!}">
	@else
	<meta property="og:title" content="{!! $web_title or env('WEB_TITLE') !!}">
	@endif
    
    @if (isset($web_custom_description))
    <meta property="og:description" content="{!! $web_custom_description !!}">
    @else
    <meta property="og:description" content="{!! $description or env('DESCRIPTION') !!}">
    @endif
   
    <meta property="og:url" content="{{ URL::current() }}">
    <meta property="og:image" content="{{ $image or '/public/img/concept/main.jpg' }}">
    <meta property="og:site_name" content="{{ 'キレイにスルンダ' }}">
    <meta property="og:locale" content="ja_JP">

    {{--twitter--}}
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="{{ 'キレイにスルンダ' }}" />
    @if (isset($web_custom_title))
        <meta property="twitter:title" content="{!! $web_custom_title !!}">
    @else
        <meta property="twitter:title" content="{!! $web_title or env('WEB_TITLE') !!}">
    @endif
    @if (isset($web_custom_description))
        <meta property="twitter:description" content="{!! $web_custom_description !!}">
    @else
        <meta property="twitter:description" content="{!! $description or env('DESCRIPTION') !!}">
    @endif
    <meta name="twitter:image" content="{{URL::to('/')}}{{ $image or '/public/img/concept/main.jpg' }}" />
    {{--twitter end--}}

    <link rel="stylesheet" href="/public/sp/css/app.css">
    <link rel="stylesheet" href="/public/sp/css/style.css">
    <link rel="shortcut icon" href="/public/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="/public/img/desktopicon.png">
    <link rel="icon" type="image/png" href="/public/favicon.png" sizes="32x32">
    @include('layouts._analysticstracking')
    @yield('css')
</head>
<body>
<div id="fb-root"></div>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    !function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';

        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, 'script', 'twitter-wjs');
</script>

<div data-target-wrap="hamburger" class="globalmenu__wrap">
    @yield('under_body_tag')
    @include('shared_sp.header.header')
    @yield('under_header')

    @yield('page_sp')

    @yield('above_footer')
    @include('shared_sp.footer.footer')
</div>

@include('shared_sp.header.list_menu_primary')
@include('shared_sp.header.list_menu_search')
@include('shared_sp.header.login_modal')
@yield('js_google')
<script src="/public/sp/js/app.js"></script>
@yield('js')
</body>
</html>