<!DOCTYPE html>
<html lang="ja">
<head>
<title>{{ $web_title or env('WEB_TITLE') }}</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=1080">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    
<meta name="title" content="{!! $web_title or env('WEB_TITLE') !!}">
<meta name="description" content="{!! $description or env('DESCRIPTION') !!}">
<meta name="keywords" content="{!! $keywords or env('KEYWORDS') !!}">

@if (isset($web_custom_title))
<meta property="og:title" content="{!! $web_custom_title !!}">
@else
<meta property="og:title" content="{!! $web_title or env('WEB_TITLE') !!}">
@endif

@if (isset($web_custom_description))
<meta property="og:description" content="{!! $web_custom_description !!}">
@else
<meta property="og:description" content="{!! $description or env('DESCRIPTION') !!}">
@endif

<meta property="og:url" content="{{ URL::current() }}">
<meta property="og:image" content="{{ $image or '/public/img/concept/main.jpg' }}">
<meta property="og:site_name" content="{{ 'キレイにスルンダ' }}">
<meta property="og:locale" content="ja_JP">
<meta name="viewport" content="width=1080">
<link rel="stylesheet" href="/public/css/app.css">
<link rel="shortcut icon" href="/public/favicon.ico">
<link rel="icon" type="image/png" href="/public/favicon.png" sizes="32x32">
<!--link(rel="apple-touch-icon-precomposed" href='#{domain}img/clipicon.png')-->
<!--script.-->
<!--    (function (i, s, o, g, r, a, m) {-->
<!--        i['GoogleAnalyticsObject'] = r;-->
<!--        i[r] = i[r] || function () {-->
<!--                    (i[r].q = i[r].q || []).push(arguments)-->
<!--                }, i[r].l = 1 * new Date();-->
<!--        a = s.createElement(o),-->
<!--                m = s.getElementsByTagName(o)[0];-->
<!--        a.async = 1;-->
<!--        a.src = g;-->
<!--        m.parentNode.insertBefore(a, m)-->
<!--    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');-->
<!--    ga('create', 'UA-31867051-1', '012grp.co.jp');-->
<!--    ga('require', 'linkid', 'linkid.js');-->
<!--    ga('send', 'pageview');-->



</head>
<body>
    @include('shared.header.header')

    <div data-target-maincolumn="target" class="primary-box--full">
      	@yield('content')
      	<div data-target-rightcolumn="target" class="is-static">
      	</div>
    </div>

    @include('shared.footer.footer')
    <script src="/public/js/app.js"></script>
</body>
</html>
