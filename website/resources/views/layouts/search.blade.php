<!DOCTYPE HTML>
<html lang="ja">

<head>    
    <link href="/public/css/style.css" rel="stylesheet" type="text/css" media="screen, print" />
    <link rel="stylesheet" href="/public/css/mosaic.css" type="text/css" media="screen, print" />

    <link rel="stylesheet" href="/public/css/jquery.sidr.light.css" type="text/css">
    <!--[if lt IE 9]> <script src="/public/js/html5shiv.min.js"></script> <![endif]-->
    <script type="text/javascript" src="/public/js/jquery.min.js"></script>

</head>

<body>
    <div>
    </div>
    <div id="wrap">
        <a id="right-menu" href="#sidr" title="Open menu"><img src="/public/images/menu.png" width="32px" height="32px" alt="メニューバー">
        </a>
        <div id="sidr-right">
            @include('shared.item.sp_right_menu')
        </div>
        
        <header class="clearfix header">
            @include('shared.header.index')
        </header>
        <div id="contents">
            @yield('content')
        </div>
    </div>

    <div id="footer">
        @include('shared.footer.index')
    </div>

    <script type="text/javascript" src="/public/js/jquery.sidr.min.js"></script>
    <script type="text/javascript" src="/public/js/mosaic.1.0.1.js"></script>
    <script type="text/javascript" src="/public/js/app.js"></script>
</body>

</html>