<?php
$class_primary_box = 'u-mt20';
$__title = _helper::getWebInfo('title_account');
$web_title = _helper::set_title($__title);

$description = trans('webinfo.meta_desc_account');
$keywords = trans('webinfo.keywords_account');
?>
@extends('layouts.default')

@section('column_right')
    @include('shared.right_column.no_article')
@stop

@section('breadcrumb')
    @include('breadcrumb.default', array(
                           'page' 	=> 'ログイン/新規登録'
                       ))
@stop

@section('column_left')
    <div data-target-leftcolumn="target" class="column-left">
        <h1 class="column-left__ttl--normal u-fs--xxl--contents">ログイン/新規登録</h1>
        <div class="form--login u-mt10">
            @if (Session::has('flash_message'))
                <div>
                    <p data-error="required" class="form__error u-fs--xs--lh--form"
                       style="display: block;">{{ Session::get('flash_message') }}</p>
                </div>
            @endif
            <div class="form--login__top">
                <div class="-login__top__l">
                    <h2 class="-login__top__ttl u-fs--xl u-fwb">SNSアカウントでログイン</h2><a href="/account/facebook-redirect"
                                                                                     class="c-button-square--social c-facebook u-mt30"><span
                                class="u-fs--l u-fwb">Facebookでログイン</span></a><a href="/account/twitter-redirect"
                                                                                 class="c-button-square--social c-twitter u-mt20"><span
                                class="u-fs--l u-fwb">Twitterでログイン</span></a>
                </div>
                <div class="-login__top__r">
                    <h2 class="-login__top__ttl u-fs--xl u-fwb">メールアドレスでログイン</h2>
                    @if($errors->has('login'))
                        <div>
                            <p data-error="required" class="form__error u-fs--xs--lh--form"
                               style="display: block;">{{ $errors->first('login') }}</p>
                        </div>
                    @endif
                    <form method="POST" action="/account">
                        <input type="email" placeholder="メールアドレス" autocomplete="on"
                               class="form--primary__text u-fs--l u-mt30" name="email">
                        <input type="password" placeholder="パスワード" autocomplete="on"
                               class="form--primary__text u-fs--l u-mt10" name="password">
                        <button type="submit" action="" data-submit="trigger"
                                class="c-button-square--l u-fs--xl u-fwb u-mt20">
                            <span class="button-inner">ログイン</span>
                        </button>
                        <a href="/account/forget-password" class="_top__r__link u-fs--s u-mt20">パスワードを忘れたかたはコチラ</a>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    </form>
                </div>
            </div>
            <div class="form--login__middle u-mt50">
                <div class="-login__middle__ttl u-fs--xl u-fwb">メールアドレスで新規登録</div>
                <form novalidate method="POST" action="/account/register">
                    @if($errors->has('register'))
                        <div>
                            <p class="form__error u-fs--xs--lh--form"
                               style="display: block;">{{ $errors->first('register') }}</p>
                        </div>
                    @endif
                    <input tabindex="1" type="email" placeholder="メールアドレス" data-mail="target" autocomplete="off"
                           data-validate-register="target" required class="form--primary__text u-fs--l u-mt30"
                           name="email">
                    @if($errors->has('email'))
                        <p class="form__error u-fs--xs--lh--form u-mt10 -login__error u-txtl"
                           style="display: block;">{{ $errors->first('email') }}</p>
                    @endif
                    <div class="-login__error u-txtl">
                        <p data-error="required" class="form__error u-fs--xs--lh--form u-mt10">メールアドレスを入力して下さい</p>
                        <p data-error="mail" class="form__error u-fs--xs--lh--form u-mt10">メールアドレスの入力に誤りがあります。</p>
                    </div>
                    <input tabindex="1000"
                           style="height: 0;overflow: hidden;border: 0;outline: none;line-height: 0;box-sizing: content-box;padding: 0;margin: 0;"
                           type="password" placeholder="パスワード" autocomplete="on"
                           class="form--primary__text u-fs--l u-mt10" name="fake_password">
                    <input tabindex="2" type="password" placeholder="パスワード" autocomplete="off"
                           data-validate-register="target" data-maxlength="8" data-minlength="6"
                           data-alphanumeric="target" required class="form--primary__text u-fs--l u-mt10"
                           name="password">
                    @if($errors->has('password'))
                        <p class="form__error u-fs--xs--lh--form u-mt10 -login__error u-txtl"
                           style="display: block;">{{ $errors->first('password') }}</p>
                    @endif
                    <div class="-login__error u-txtl">
                        <p data-error="required" class="form__error u-fs--xs--lh--form u-mt10">パスワードを入力してください。</p>
                        <p data-error="minlength" class="form__error u-fs--xs--lh--form u-mt10">6桁以上で入力して下さい</p>
                        <p data-error="maxlength" class="form__error u-fs--xs--lh--form u-mt10">8桁以下で入力して下さい</p>
                        <p data-error="alphanumeric" class="form__error u-fs--xs--lh--form u-mt10">半角英数で入力して下さい</p>
                    </div>
                    <button type="submit" data-submit-modal="trigger" class="c-button-square--l u-fs--xl u-fwb u-mt20">
                        <span class="button-inner">新規会員登録</span></button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                </form>
                <p class="form--login__repletion u-fs--xs--lh--form u-mt25">ログインまたは新規登録することにより、キレイにスルンダの利用規約、<br>プライバシーポリシーに同意したことになります。
                </p>
            </div>
        </div>
    </div>
@stop