管理者様へ

下記の内容でお問い合わせがありましたので、ご確認ください。

【種別】
{{ $contact["kind_name"] or '' }}

【会社名】
{{ $contact["company"] or '' }}

【お名前】
{{ $contact["name"] or '' }}

【メールアドレス】
{{ $contact["mail"] or '' }}

【お問い合わせ内容】
{{ $contact["content"] or '' }}

