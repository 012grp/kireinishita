--ご送信内容---------------------------------------------------------

[ お名前 ]　
{{ $entryInfo["name"] or ''}}

[ フリガナ ]　
{{ $entryInfo["kana"] or ''}}

[ 性別 ]　
{{ $entryInfo["gender"] or ''}}

[ 生年月日 ]　
{{$entryInfo['year'] or ''}}年{{$entryInfo['month'] or ''}}月{{$entryInfo['day'] or ''}}日

[ メールアドレス ]　
{{ $entryInfo["email"] or ''}}

[ 住所 ]　
{{$entryInfo['province_name'] or ''}} {{$entryInfo['city_name'] or ''}}

[ 電話番号 ]　
{{ $entryInfo["tel"] or ''}}

[ 現在の就業状況 ]　
{{ $entryInfo["situation_value"] or ''}}

[ 入社可能時期 ]　
{{ $entryInfo["join_value"] or ''}}

[ 希望連絡時間・連絡方法 ]　
{{ $entryInfo["contact"] or ''}}

[ 取得資格 ]　
{{ $entryInfo["capabilities"] or ''}}

[ 職務経歴・自己PR ]　
{{ $entryInfo["pr"] or ''}}

[ 希望職種 ]　
{{ $entryInfo["job_objective"] or ''}}

[ 希望雇用形態 ]　
{{ $entryInfo["hope_employment"] or ''}}

----------------------------------------------------------------------





