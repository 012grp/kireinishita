<?php
	$rootPath = Request::root();
	$urlLogin = $rootPath . '/corporation/login';
	$urlForgetPass = $rootPath . '/corporation/forget-password';

?>
{{ $info["company_name"] or '' }}　様

平素より「キレイにスルンダ」をご利用いただき誠にありがとうございます。

求人管理画面のログイン用IDと初期パスワードをご通知致します。

■求人管理画面URL
{{ $urlLogin or '' }}

■ログインID
求人広告掲載内容申請フォームに記載したメールアドレス

■初期パスワード
{{ $info["password"] or '' }}

※初期パスワードはログイン後に管理画面のパスワード再設定から、または下記のパスワード変更用URLから必ず変更してください。
（パスワードは覚えやすく他人に推測されにくいものに設定しておくことをお勧めします。）

■パスワード変更用URL
{{ $urlForgetPass or '' }}

署名の内容が統一されていないため、以下に修正

@include('mail._signature')
