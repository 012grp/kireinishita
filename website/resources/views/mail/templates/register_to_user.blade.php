このたびはキレイにスルンダにご登録いただきまして誠にありがとうございます。
会員登録が無事完了致しましたこと、ご連絡させていただきます。

ログインの際にご登録のメールアドレスが必要になりますので、本メールは大切に保管下さい。
【メールアドレス】
{{ $user['email'] or '' }}

今後ともキレイにスルンダのご利用よろしくお願いいたします。

@include('mail._signature')
