登録確認のご案内

【ご登録のメールアドレス】

{{ $mai }}

以下のパスワード再設定用URLにアクセスして、パスワードを再設定してください。

【パスワード再設定用URL】
<?php echo htmlspecialchars_decode($resetPasswordLink) ?>


※URL発行後24時間が過ぎてしまった場合は、パスワード再設定用URLは利用できません。
お手数おかけしますが、登録を再度やり直してください。

【このメールにお心当たりのない方】

どなたかが間違ってアドレスを入力した可能性がございます。
お心当たりのない場合は、このまま削除いただきますようにお願いいたします。

@include('mail._signature')
