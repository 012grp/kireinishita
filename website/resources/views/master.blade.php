<!DOCTYPE html>
<html lang="en">

	<head>
	    <meta charset="UTF-8">
	    <title>Web Crawler: Data crawled by Robots</title>
	  	<link rel="stylesheet" href="{{ URL::asset('public/css/style.css') }}">
		<script type="text/javascript" src="{{ URL::asset('public/js/jquery-1.9.1.min.js') }}"></script>
		<script type="text/javascript">
			function search(){
				window.location = '/'
				+document.getElementById('website').value
				+'/'
				+document.getElementById('province').value
				+'/'
				+document.getElementById('keyword').value;
			}
		</script>
	</head>
	<body>
	    <div class="header fixed_top search_header">
		   	<div class="center_contain">
		        <form action="/index.php" method="GET">
		            <div class="search_control">
						<script type="text/javascript">
							document.addEventListener("DOMContentLoaded", function(event) { 
								document.getElementById('website').value = {{ isset($request['website']) ? $request['website'] : 0 }};
								document.getElementById('province').value = {{ isset($request['website']) ? $request['province'] : 0 }};
								document.getElementById('keyword').value = '{{ isset($request['website']) ? $request['keyword'] : 0 }}';
							});
						</script>
					    
		            	<select name="website" id="website">
							<option value="0">全て</option>
							<option value="7">エン転職</option>
							<option value="10">ルルナビ</option>
							<option value="11">シフトワークス</option>
							<option value="15">マイナビパー</option>
							<option value="19">リビングおしごとnet</option>
		            	</select>
						
		            	<select name="province" id="province">
							<option value="0">全国の</option>
							<option value="1">関東</option>
							<option value="2">関西</option>
							<option value="3">東海</option>
							<option value="4">北海道・東北</option>
							<option value="5">甲信越・北陸</option>
							<option value="6">中国四国</option>
							<option value="7">九州・沖縄</option>
		            	</select>
		                <input type="text" name="keyword" id="keyword" placeholder="search..." value="">
		                <button onClick="search(); return false;">search</button>
						<!-- <a href="/" style="text-decoration: none; font-size: 18px;font-weight: bold;color: #696969;">View all</a>-->
		            </div>
		        </form>
			</div>
            <div class="user">
                <div class="wp-user clearfix">
                    <a href="/" class="logo">Crawler</a>
                    <div class="float-right toolbar">
                    @if(isset($user))
                        <a href="/account/me" class="username">{!! $user->name !!}</a>
                        <a href="/account/logout">logout</a>
                    @else
                        <a href="/account/login">Sign in/Sign up</a>
                    @endif
                    </div>
                </div>
            </div>
	    </div>
	    <div class="container next_fixed_top">      
			@yield('content')
	    </div>
	    <div class="footer">
	        <div class="search_footer"></div>
	    </div> 
	</body>
</html>