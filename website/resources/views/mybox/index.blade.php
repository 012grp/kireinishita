<?php 
	$class_primary_box = 'u-mt20'; 
	$__title = _helper::getWebInfo('title_mybox');
	$web_title = _helper::set_title($__title);

	$description = trans('webinfo.meta_desc_mybox');
	$keywords = trans('webinfo.keywords_mybox');

	if(Session::has('user'))
	{
		$user = Session::get('user');
	}

	$userInfo = isset($user['email']) ? $user['email'] : $user['nick_name'];
?>
@extends('layouts.default')

@section('column_right')
	 @include('shared.right_column.no_lastest_article')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 	=> 'お気に入りBOX'
						))
@stop


@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<div class="column-left__heading--normal">
			<h1 class="column-left__ttl--normal u-fs--xxxl--contents"><span class="column-left__ttl__inner">お気に入り&nbsp;BOX</span></h1>
			<p class="column-left__ttl__right--normal u-fs--sh"><span class="_ttl__right__num u-fs--l"><span class="u-fwd u-fs--xxl--contents" id="totalJobBookmark">{{$totalBookmark}}</span>件キープしています</span><span class="_ttl__right__name u-fs--l"><i class="c-icon--people"></i><span class="u-fwb">{{ $userInfo or '' }}</span>さん</span></p>
		</div>

		<div class="column-box--wide">
			<?php $index = 0; ?>

			@foreach($jobs as $job)
				<?php $job = (array)$job; ?>
				<?php
					if($index != 0)
					{
						$job['job_class'] = 'u-mt30';
					}
					//$job['is_secret_job'] = true;
					if($job['is_secret'])
					{
						$job['url_target'] = '';
					}
					else
					{
						$job['url_target'] = '_blank';
					}
					$index++;
				?>
				@include('job._search_item', $job)
			@endforeach 
		</div>
	</div>
@stop


	
		