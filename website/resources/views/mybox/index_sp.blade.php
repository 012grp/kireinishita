<?php
$__title = _helper::getWebInfo('title_mybox');
$web_title = _helper::set_title($__title);

$description = trans('webinfo.meta_desc_mybox');
$keywords = trans('webinfo.keywords_mybox');
$pageCount = 1;
$currentPage = 1;
$url_next = '';
$url_prev = '';
?>

@extends('layouts.default_sp')

@section('under_header')
    @include('shared_sp.item.mybox_under_header')
@stop

@section('page_sp')
    <div class="column-box--gray">
       	@foreach($jobs as $index=>$content)
			<?php
			$content = (array)$content;
			$content['index'] = $index;
			?>
			@include('shared_sp.item.job_result_box', $content)
		@endforeach 
        @include('shared_sp.item.pager', array(
			'pageCount' 	=> $pageCount,
			'currentPage' 	=> $currentPage,
			'url_next' 	=> $url_next,
			'url_prev' 	=> $url_prev,
		))
    </div>
@stop