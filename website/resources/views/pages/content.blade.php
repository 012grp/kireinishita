@extends('layouts.content')
@section('content')
            <?php $count = count($contents); ?>
            <?php $mosaic = 'shared.item.mosaic_block_no_pick'; ?>
            <?php $big_mosaic = 'shared.item.big_mosaic_block_no_pick'; ?>
            
            @if ($count < 1)
                <strong>No result</strong>
            @else
                {{-- Check existing user --}}
                @if (isset($user))
                    <?php $mosaic = 'shared.item.mosaic_block'; ?>
                    <?php $big_mosaic = 'shared.item.big_mosaic_block'; ?>
                @endif
                  
                <div class="imageBlock_ad">
                    <div class="imageBlock">
                        @include($big_mosaic, $contents[0])
                    </div>
                </div>
                
                @for ($i = 1; $i < $count; $i++)
                    <div class="imageBlock_left">
                        <div class="imageBlock">
                          @include($mosaic, $contents[$i])
                        </div>
                    </div>
                @endfor

                <div class="clearfix"></div>
                
                {{-- Paginator --}}
                {{-- $contents->render() --}}
            @endif
            
@stop
