@extends('layouts.search') 
@section('content')
<div class="search_boxArea">
    <a href="search/index.html"><img src="/public/images/mama_logo_s01.png" class="img2" alt="ママの求人">
    </a>
    <form class="form">
        <input type="search" id="txtKeyword" class="text">
        <input type="image" id="btnSearch" value="検索" src="/public/images/btn_search_001.png">
    </form>
    <p class="serach_text">
    @foreach($keywords as $x)
        <a href="/content/{{ $x->keyword }}">{{ $x->keyword }}</a>
    @endforeach 
    </p>
</div>
@stop