<?php $class_column_right = 'u-mt30'; ?>

@extends('layouts.default') 

@section('under_header')
	<div style="background-image:url(/public/img/topimg/main.jpg)" class="topImg-box topImg-box--center"></div>
	@include('shared.header.search_box') 
@stop 

@section('column_right')    
    @include('shared.right_column.default')
@stop


@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
	    {{-- 職種で検索 --}}
		@include('shared.item.search_in_job')

		{{-- こだわりで検索 --}}
		@include('shared.item.search_in_good')


		{{-- box secret jobs --}}
		<?php $secretJobs = _modules::top_secret_jobs(); ?>
		@include('shared.item.job_list',
					array(	'name' => 'オリジナル求人',
							'label_class' => 'c-tag--secret',
							'label_name' => '限定',
							'contents' => $secretJobs,
							'target_url' => '',
							'style' => 1 //is secrect job
						)
				)

		{{-- box new jobs --}}
		<?php $newJobs = _modules::top_new_jobs(); ?>
		@include('shared.item.job_list',
					array(	'name' => '新着求人',
							'label_class' => 'c-tag--new',
							'label_name' => 'NEW',
							'contents' => $newJobs,
							'target_url' => '_blank',
							'style' => 2 //is content
						)
				)
	</div>
@stop