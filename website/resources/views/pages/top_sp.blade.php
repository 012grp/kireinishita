<?php $class_column_right = 'u-mt30'; ?>

@extends('layouts.default_sp')

@section('under_header')
	<figure class="top__image">
		<img src="/public/sp/img/svg/logo.svg" alt="ロゴ" class="primary-box__logo">
	</figure>
	@include('shared_sp.item.top_search_box')
	@include('shared_sp.item.google_ads')
@stop 


@section('page_sp')
	<div class="primary-box">
		<div class="primary-box__inner">
			<ul class="tab__block u-mt25">
				<li data-trigger-tab="trigger" class="tab__block__button u-fs--xs is-active">職種で検索</li>
				<li data-trigger-tab="trigger" class="tab__block__button u-fs--xs"> こだわりで検索</li>
			</ul>
			{{-- search in job --}}
			@include('shared_sp.item.search_in_job')

			{{-- search in good --}}
			@include('shared_sp.item.search_in_good')

		</div>
	</div>
@stop