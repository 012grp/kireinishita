<?php
	$query=$_GET;
	unset($query['page']);

	$currentUrl = Request::url();
	$queryString = http_build_query($query);

	if($queryString)
	{
		$currentUrl = $currentUrl . '?' . $queryString;
	}

	if (strpos($currentUrl, '?')) // returns false if '?' isn't there
	{
		$paramPage = '&page=';
	}
	else
	{
		$paramPage = '?page=';
	}


	$pageCount = $paginator['total_page'];
	$currentPage = $paginator['current_page'];

	$modulus = 6;
	$haft  = (int)$modulus/2;
 
	if(($modulus+1) >= $pageCount)
	{
		$start = 1;
		$end = $pageCount;
	}
	else
	{
		if(($currentPage - $haft) <= 1)
		{
			$start = 1;
			$end = $start + $modulus;
		}
		else
		{
			if($currentPage > ($pageCount - $haft))
			{
				$end = $pageCount;
				$start = $end - $modulus;
			}
			else
			{
				$start = $currentPage - $haft;
				$end = $currentPage + $haft; 
			}
		}
	}
?>


<ul class="pager__lists u-fs--l">
	<?php if($pageCount > 1): ?>

		<?php if($currentPage <= 1): ?>
			<li class="pager__list is-disable">&#60; 前へ</li>			
		<?php else: ?>
			<li class="pager__list">
				<a href="<?php echo $currentUrl . $paramPage . 1; ?>">&#60; 前へ</a>
			</li>
		<?php endif; ?>
		
		<?php if($currentPage >= $modulus): ?>
			<li class="pager__list">
                <a href="<?php echo $currentUrl . $paramPage . 1; ?>">1</a>
            </li>
            <li class="is-disable" style="display:inline; font-size: 10px">・・・</li>
		<?php endif; ?>
		
		
		<?php for($i=$start; $i<=$end; $i++): ?>
			<?php if ($currentPage == $i): ?>
			<li class="pager__list is-selected">{{ $i }}</li>
			<?php else: ?>
			<li class="pager__list">
                <a href="<?php echo $currentUrl . $paramPage . $i; ?>">{{ $i }}</a>
            </li>
			<?php endif; ?>
		<?php endfor; ?>
		
		
		<?php if($pageCount >= $currentPage+$modulus): ?>
			<li class="is-disable" style="display:inline; font-size: 10px">・・・</li>
			<li class="pager__list">
                <a href="<?php echo $currentUrl . $paramPage . $pageCount; ?>">{{$pageCount}}</a>
            </li>
		<?php endif; ?>

		
		<?php if($currentPage >= $pageCount): ?>
			<li class="{{ 'pager__list is-disable' }}">
            次へ &#62;
        	</li>
		<?php else: ?>
		<li class="{{ 'pager__list' }}">
            <a href="<?php echo $currentUrl . $paramPage . ($currentPage + 1); ?>" >次へ &#62;</a>
        </li>
		<?php endif; ?>
	<?php endif; ?>
</ul>