<?php $class_primary_box = 'u-mt20'; ?>
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
 	@include('breadcrumb.default', array(
							'page' 	=> 'パスワードを再設定する'
						))
@stop

@section('column_left')
    <div data-target-leftcolumn="target" class="column-left">
      	<h1 class="column-left__ttl--normal u-fs--xxl--contents">パスワードを再設定する</h1>
      	<div class="form--login u-mt10">
	        <div class="form--login__middle u-mt50">
          		@if(isset($invalid_message))
          		<p class="u-fs--xxl u-mt40">{{$invalid_message}}</p>
	          	<div class="c-button-square--l u-fs--xxl u-fwb u-mt60"><a href="/">HOMEへ戻るんだ</a></div>
          		@else
					<div class="-login__middle__ttl u-fs--xl u-fwb">
						これはパスワードをリセットするフォームです。<br>
						覚えがない方は閉じてください。
					</div>
					
					<form method="POST" action="/account/set-new-password">
						<input type="password" required placeholder="新しいパスワード" autocomplete="on" class="form--primary__text u-fs--l u-mt10" name="password">
						<input type="password" required placeholder="確認パスワード" autocomplete="on" class="form--primary__text u-fs--l u-mt10" name="password_confirmation">
						
						@if($errors->has('reset_password'))
						<p class="form__error u-fs--xs--lh--form">{{ $errors->first('reset_password')}}</p>
						@endif
						
						<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xl u-fwb u-mt20"><span class="button-inner">リセット</span></button>
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						<input type="hidden" name="requestId" value="{{ $requestId or '' }}" />
						<input type="hidden" name="requestHash" value="{{ $requestHash or '' }}" />
					</form>
	          	@endif
	        </div>
      	</div>
    </div>
 @stop