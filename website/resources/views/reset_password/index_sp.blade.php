@extends('layouts.default_sp')


@section('page_sp')
<div class="primary-box--noPadding">
	<h1 class="primary-box__ttl--back u-fwb u-fs--m">パスワードを再設定する</h1>

	<div class="form--primary form--primary--contact u-mt10">
		@if(isset($invalid_message))
			<div class="form__button__block--noborder form__button__block--thanks u-mt20">
				<p class="u-fs--l u-mt40">{{$invalid_message}}</p>
				<p class="u-fs--l u-mt40"></p>
          		<div class="c-button-square--l u-fs--xs u-fwb u-mt20"><a href="/">HOMEへ戻るんだ</a></div>
			</div>
	    @else
			<p class="form--primary__repletion--top u-fs--xsh">
				これはパスワードをリセットするフォームです。<br>
				覚えがない方は閉じてください。
			</p>
			<form class="u-mt15" action="/account/set-new-password" method="post">
				<table class="form--primary__table">
					<tr class="-contact__table__row">
						<td class="form--primary__detail u-mt15">
							<input name="password" required type="password" placeholder="新しいパスワード" autocomplete="off" class="form--primary__text u-fs--l">
						</td>
						<td class="form--primary__detail u-mt15">
							<input name="password_confirmation" required type="password" placeholder="確認パスワード" autocomplete="off" class="form--primary__text u-fs--l">
						</td>
						@if($errors->has('reset_password'))
							<td class="form--primary__detail u-mt15">
								<p class="form__error u-fs--xs">{{$errors->first('reset_password')}}</p>
							</td>
						@endif
					</tr>
				</table>
				<div class="-contact__button__block u-mt20">
					<button type="submit" action="" data-submit="trigger" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">リセット</span></button>
				</div>
				<input type="hidden" name="requestId" value="{{ $requestId or '' }}" />
				<input type="hidden" name="requestHash" value="{{ $requestHash or '' }}" />
				<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			</form>
		@endif
	</div>
</div>
@stop