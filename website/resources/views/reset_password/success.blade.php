<?php 
	$class_primary_box = 'u-mt20'; 
	$web_title = _helper::set_title('求職者様お問い合わせ（完了ページ）');
?>
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.no_article')
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 	=> 'パスワードを再設定する'
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<h1 class="column-left__ttl--normal u-fs--xxl--contents">パスワードを再設定する</h1>
		<div class="form--primary u-mt10">
			<p class="form--primary__thanks u-fs--xxlh--contents u-fwb u-mt35">パスワードリセット完了しました</p>
			<div class="-contact__button__block u-mt50">
				<div class="c-button-square--l u-fs--xxl u-fwb"><a href="/">HOMEへ</a></div>
			</div>
		</div>
	</div>
@stop
