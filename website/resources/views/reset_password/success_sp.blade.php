@extends('layouts.default_sp')

@section('page_sp')

	<!--.primary-box--fixHeight-->
<div class="primary-box--noPadding">
	<h1 class="primary-box__ttl--back u-fwb u-fs--m">パスワードを再設定する</h1>
	<div class="form--primary">
		<p class="form--primary__thanks u-fs--mh u-fwb u-mt30">パスワードリセット完了しました</p>
    </div>
	<div class="form__button__block--noborder form__button__block--thanks u-mt20">
		<p class="u-fs--s u-mt30 u-txtc"><a href="{{ Request::root() }}" class="c-button-square--l u-fs--xs u-fwb"><span class="button-inner">HOMEへ</span></a></p>
	</div>
</div>
@stop