@extends('search_demo.layout')

@section('page_content')
    <header class="header--primary">
        <div class="header--primary__inner u-cf">
            <div data-trigger-quicksearch="quicksearch" class="header--primary__search"><i
                        class="c-icon--search"></i><span class="-primary__search__text u-fs--xxxs">SEARCH</span></div>
            <h2 class="header--primary__logo"><a href="#"><img src="/public/img/svg/top-logo.svg" alt="キレイにスルンダ"></a>
            </h2>
            <div class="header--primary__right"><a href="#" class="c-button-square--s"><span
                            class="u-fs--l u-fwb">ログイン</span></a>
                <div data-trigger-hamburgermenu="hamburger" class="nav--hamburger"><span
                            class="nav--hamburger__list"></span><span class="nav--hamburger__list"></span><span
                            class="nav--hamburger__list"></span><span
                            class="nav--hamburger__text u-fs--xxxs">MENU</span></div>
            </div>
            <form data-target-quicksearch="quicksearch" class="quick-search quick-search--inHeader is-disable">
                <div class="-search--inHeader__inner">
                    <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                        <select name="都道府県" class="quick-search__select u-fs--m">
                            <option value="" selected>都道府県</option>
                            <option value="北海道">北海道</option>
                            <option value="青森県">青森県</option>
                            <option value="岩手県">岩手県</option>
                            <option value="宮城県">宮城県</option>
                            <option value="秋田県">秋田県</option>
                            <option value="山形県">山形県</option>
                            <option value="福島県">福島県</option>
                            <option value="茨城県">茨城県</option>
                            <option value="栃木県">栃木県</option>
                            <option value="群馬県">群馬県</option>
                            <option value="埼玉県">埼玉県</option>
                            <option value="千葉県">千葉県</option>
                            <option value="東京都">東京都</option>
                            <option value="神奈川県">神奈川県</option>
                            <option value="新潟県">新潟県</option>
                            <option value="富山県">富山県</option>
                            <option value="石川県">石川県</option>
                            <option value="福井県">福井県</option>
                            <option value="山梨県">山梨県</option>
                            <option value="長野県">長野県</option>
                            <option value="岐阜県">岐阜県</option>
                            <option value="静岡県">静岡県</option>
                            <option value="愛知県">愛知県</option>
                            <option value="三重県">三重県</option>
                            <option value="滋賀県">滋賀県</option>
                            <option value="京都府">京都府</option>
                            <option value="大阪府">大阪府</option>
                            <option value="兵庫県">兵庫県</option>
                            <option value="奈良県">奈良県</option>
                            <option value="和歌山県">和歌山県</option>
                            <option value="鳥取県">鳥取県</option>
                            <option value="島根県">島根県</option>
                            <option value="岡山県">岡山県</option>
                            <option value="広島県">広島県</option>
                            <option value="山口県">山口県</option>
                            <option value="徳島県">徳島県</option>
                            <option value="香川県">香川県</option>
                            <option value="愛媛県">愛媛県</option>
                            <option value="高知県">高知県</option>
                            <option value="福岡県">福岡県</option>
                            <option value="佐賀県">佐賀県</option>
                            <option value="長崎県">長崎県</option>
                            <option value="熊本県">熊本県</option>
                            <option value="大分県">大分県</option>
                            <option value="宮崎県">宮崎県</option>
                            <option value="鹿児島県">鹿児島県</option>
                            <option value="沖縄県">沖縄県</option>
                        </select>
                    </div>
                    <span class="c-icon--cross"></span>
                    <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                        <select name="雇用形態" class="quick-search__select u-fs--m">
                            <option value="正社員">正社員</option>
                            <option value="その他">その他</option>
                            <option value="契約社員">契約社員</option>
                            <option value="業務委託">業務委託</option>
                            <option value="アルバイト">アルバイト</option>
                        </select>
                    </div>
                    <span class="c-icon--cross"></span>
                    <input type="text" placeholder="キーワード" class="quick-search__input u-fs--m">
                    <button type="submit" class="c-button-square--m u-fs--l"><span class="button-inner">カンタン検索</span>
                    </button>
                </div>
            </form>
            <ul data-target-hamburgermenu="hamburger" class="nav-box u-fs--xs nav-box--inHeader is-disable">
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i
                                    class="c-icon--home"></i></span><span>HOME</span></a></li>
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i
                                    class="c-icon--secret"></i></span><span>限定求人</span></a></li>
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><span class="nav-box__list__icon"><i
                                    class="c-icon--content"></i></span><span>コンテンツまとめ</span></a></li>
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                class="nav-box__list__icon"><i class="c-icon--book"></i></span><span>キレイJOB図鑑</span></a>
                </li>
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                class="nav-box__list__icon"><i
                                    class="c-icon--beauty"></i></span><span>キレイにナルンダ</span></a></li>
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                class="nav-box__list__icon"><i class="c-icon--hat"></i></span><span>集客の知恵袋</span></a>
                </li>
                <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                class="nav-box__list__icon"><i class="c-icon--star-other"></i></span><span>スタア発掘</span></a>
                </li>
            </ul>
        </div>
    </header>
    <div class="primary-box">
        <div class="breadcrumb u-fs--xxs"><a href="#"><span>HOME</span></a><span
                    class="breadcrumb__arrow__text u-arrow__text">&gt;</span><a href="#"><span>調理の求人</span></a><span
                    class="breadcrumb__arrow__text u-arrow__text">&gt;</span><a href="#"><span>パティシエの求人</span></a><span
                    class="breadcrumb__arrow__text u-arrow__text">&gt;</span><span>東京都のパティシエの求人</span></div>
        <div data-target-maincolumn="target" class="primary-box__inner u-mt20">
            <div data-target-leftcolumn="target" class="column-left">
                <input type="hidden" id="requestWaysite" value="">
                <form action="/search" method="get">
                    <table class="search-table u-fs--xs">
                        <tr class="search-table__row">
                            <td class="search-table__type">職種</td>
                            <td colspan="2" class="search-table__right">
                                <div data-modal-trigger="jobmodal" class="c-button-square--ms--gray u-fs--xs u-mr10">
                                    選択する<span>&gt;</span></div>
                                <div data-target-addtag="job" class="check__tag__addblock"></div>
                            </td>
                        </tr>
                        <tr class="search-table__row">
                            <td class="search-table__type">勤務地</td>
                            <td colspan="2" class="search-table__right--side">
                                <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                                    <select id="pro_select" name="pro" data-trigger-location="trigger"
                                            class="quick-search__select u-fs--m">
                                        <option value="">都道府県</option>
                                        <option value="1">北海道</option>
                                        <option value="2">青森県</option>
                                        <option value="3">岩手県</option>
                                        <option value="4">宮城県</option>
                                        <option value="5">秋田県</option>
                                        <option value="6">山形県</option>
                                        <option value="7">福島県</option>
                                        <option value="13">東京都</option>
                                        <option value="14">神奈川県</option>
                                        <option value="11">埼玉県</option>
                                        <option value="12">千葉県</option>
                                        <option value="9">栃木県</option>
                                        <option value="10">群馬県</option>
                                        <option value="8">茨城県</option>
                                        <option value="19">山梨県</option>
                                        <option value="15">新潟県</option>
                                        <option value="20">長野県</option>
                                        <option value="16">富山県</option>
                                        <option value="17">石川県</option>
                                        <option value="18">福井県</option>
                                        <option value="23">愛知県</option>
                                        <option value="21">岐阜県</option>
                                        <option value="22">静岡県</option>
                                        <option value="24">三重県</option>
                                        <option value="27">大阪府</option>
                                        <option value="28">兵庫県</option>
                                        <option value="26">京都府</option>
                                        <option value="25">滋賀県</option>
                                        <option value="29">奈良県</option>
                                        <option value="30">和歌山県</option>
                                        <option value="31">鳥取県</option>
                                        <option value="32">島根県</option>
                                        <option value="33">岡山県</option>
                                        <option value="34">広島県</option>
                                        <option value="35">山口県</option>
                                        <option value="36">徳島県</option>
                                        <option value="37">香川県</option>
                                        <option value="38">愛媛県</option>
                                        <option value="39">高知県</option>
                                        <option value="40">福岡県</option>
                                        <option value="41">佐賀県</option>
                                        <option value="42">長崎県</option>
                                        <option value="43">熊本県</option>
                                        <option value="44">大分県</option>
                                        <option value="45">宮崎県</option>
                                        <option value="46">鹿児島県</option>
                                        <option value="47">沖縄県</option>
                                    </select>
                                </div>
                                <div data-modal-trigger="prefmodal"
                                     class="c-button-square--ms--gray is-disable u-fs--xs">市区町村の選択<span>&gt;</span>
                                </div>
                                <div data-modal-trigger="waysidemodal"
                                     class="c-button-square--ms--gray is-disable u-fs--xs">沿線・駅の選択<span>&gt;</span>
                                </div>
                                <div data-target-addtag="pref" class="check__tag__addblock"></div>
                                <div data-target-addtag="wayside" class="check__tag__addblock"></div>
                            </td>
                        </tr>
                        <tr class="search-table__row">
                            <td class="search-table__type">雇用形態</td>
                            <td colspan="2" class="search-table__right">
                                <input type="checkbox" name="a" id="check01" checked class="search-checkbox">
                                <label for="check01" class="search-checkbox__label">正社員</label>
                                <input type="checkbox" name="a" id="check02" class="search-checkbox">
                                <label for="check02" class="search-checkbox__label">アルバイト</label>
                                <input type="checkbox" name="a" id="check03" class="search-checkbox">
                                <label for="check03" class="search-checkbox__label">パート</label>
                                <input type="checkbox" name="a" id="check04" class="search-checkbox">
                                <label for="check04" class="search-checkbox__label">契約</label>
                                <input type="checkbox" name="a" id="check05" class="search-checkbox">
                                <label for="check05" class="search-checkbox__label">派遣</label>
                                <input type="checkbox" name="a" id="check06" class="search-checkbox">
                                <label for="check06" class="search-checkbox__label">その他</label>
                            </td>
                        </tr>
                        <tr class="search-table__row">
                            <td class="search-table__type">こだわり</td>
                            <td colspan="2" class="search-table__right">
                                <div data-modal-trigger="hangupmodal" class="c-button-square--ms--gray u-fs--xs u-mr10">
                                    選択する<span>&gt;</span></div>
                                <div data-target-addtag="hangup" class="check__tag__addblock"></div>
                            </td>
                        </tr>
                        <tr class="search-table__row">
                            <td class="search-table__type">キーワード</td>
                            <td class="search-table__right--multiple">
                                <input type="text" value="{{$value or ''}}" name="value" class="search-table__text">
                            </td>
                            <td class="_right--multiple--btn">
                                <input type="submit" value="125件を検索" class="c-button-square--mm u-fs--l">
                            </td>
                        </tr>
                    </table>
                    <div data-remodal-id="jobmodal" data-search-id="job" class="modal">
                        <h1 class="modal__ttl u-fs--l">職種</h1>
                        <div class="modal__inner u-fs--xs">
                            <h2 class="modal__sub__ttl">調理</h2>
                            <div class="modal__checkbox">
                                <input type="checkbox" name="a" id="jobcheck01" data-num="1" class="search-checkbox">
                                <label for="jobcheck01" class="search-checkbox__label--modal u-mt10">シェフ・キッチン</label>
                                <input type="checkbox" name="a" id="jobcheck02" data-num="2" class="search-checkbox">
                                <label for="jobcheck02" class="search-checkbox__label--modal u-mt10">パティシエ</label>
                                <input type="checkbox" name="a" id="jobcheck03" data-num="3" class="search-checkbox">
                                <label for="jobcheck03" class="search-checkbox__label--modal u-mt10">バーテンダー</label>
                                <input type="checkbox" name="a" id="jobcheck04" data-num="4" class="search-checkbox">
                                <label for="jobcheck04" class="search-checkbox__label--modal u-mt10">ソムリエ</label>
                                <input type="checkbox" name="a" id="jobcheck05" data-num="5" class="search-checkbox">
                                <label for="jobcheck05" class="search-checkbox__label--modal u-mt10">ブランジェ・製パン</label>
                                <input type="checkbox" name="a" id="jobcheck06" data-num="6" class="search-checkbox">
                                <label for="jobcheck06" class="search-checkbox__label--modal u-mt10">バリスタ</label>
                                <input type="checkbox" name="a" id="jobcheck07" data-num="7" class="search-checkbox">
                                <label for="jobcheck07" class="search-checkbox__label--modal u-mt10">ピッツアイオーロ</label>
                                <input type="checkbox" name="a" id="jobcheck08" data-num="8" class="search-checkbox">
                                <label for="jobcheck08" class="search-checkbox__label--modal u-mt10">製造・加工</label>
                                <input type="checkbox" name="a" id="jobcheck09" data-num="9" class="search-checkbox">
                                <label for="jobcheck09" class="search-checkbox__label--modal u-mt10">寿司職人</label>
                                <input type="checkbox" name="a" id="jobcheck10" data-num="10" class="search-checkbox">
                                <label for="jobcheck10" class="search-checkbox__label--modal u-mt10">その他</label>
                            </div>
                            <h2 class="modal__sub__ttl u-mt30">接客・販売</h2>
                            <div class="modal__checkbox">
                                <input type="checkbox" name="a" id="jobcheck11" data-num="11" class="search-checkbox">
                                <label for="jobcheck11" class="search-checkbox__label--modal u-mt10">店長・マネージャー</label>
                                <input type="checkbox" name="a" id="jobcheck12" data-num="12" class="search-checkbox">
                                <label for="jobcheck12" class="search-checkbox__label--modal u-mt10">ホールスタッフ</label>
                                <input type="checkbox" name="a" id="jobcheck13" data-num="13" class="search-checkbox">
                                <label for="jobcheck13" class="search-checkbox__label--modal u-mt10">販売スタッフ</label>
                                <input type="checkbox" name="a" id="jobcheck14" data-num="14" class="search-checkbox">
                                <label for="jobcheck14" class="search-checkbox__label--modal u-mt10">デリバリースタッフ</label>
                                <input type="checkbox" name="a" id="jobcheck15" data-num="15" class="search-checkbox">
                                <label for="jobcheck15" class="search-checkbox__label--modal u-mt10">レセプショニスト</label>
                            </div>
                            <h2 class="modal__sub__ttl u-mt30">企画・運営</h2>
                            <div class="modal__checkbox">
                                <input type="checkbox" name="a" id="jobcheck16" data-num="16" class="search-checkbox">
                                <label for="jobcheck16" class="search-checkbox__label--modal u-mt10">経営幹部</label>
                                <input type="checkbox" name="a" id="jobcheck17" data-num="17" class="search-checkbox">
                                <label for="jobcheck17" class="search-checkbox__label--modal u-mt10">SV・営業</label>
                                <input type="checkbox" name="a" id="jobcheck18" data-num="18" class="search-checkbox">
                                <label for="jobcheck18" class="search-checkbox__label--modal u-mt10">本部スタッフ</label>
                                <input type="checkbox" name="a" id="jobcheck19" data-num="19" class="search-checkbox">
                                <label for="jobcheck19" class="search-checkbox__label--modal u-mt10">栄養士・管理栄養士</label>
                            </div>
                            <div class="modal__btn">
                                <div data-remodal-action="close" class="c-button-square--mm">閉じる</div>
                            </div>
                        </div>
                    </div>
                    <div data-remodal-id="prefmodal" data-search-id="pref" class="modal prefmodal">
                        <h1 class="modal__ttl u-fs--l">市区町村</h1>
                        <div class="modal__inner u-fs--xs">
                            <div id="city_checkboxes" class="modal__checkbox">
                                <input type="checkbox" name="a" id="pref01" data-num="1" class="search-checkbox">
                                <label for="pref01" class="search-checkbox__label--modal u-mt10">テキスト</label>
                                <input type="checkbox" name="a" id="pref02" data-num="2" class="search-checkbox">
                                <label for="pref02" class="search-checkbox__label--modal u-mt10">テキスト</label>
                                <input type="checkbox" name="a" id="pref03" data-num="3" class="search-checkbox">
                                <label for="pref03" class="search-checkbox__label--modal u-mt10">テキスト</label>
                                <input type="checkbox" name="a" id="pref04" data-num="4" class="search-checkbox">
                                <label for="pref04" class="search-checkbox__label--modal u-mt10">テキスト</label>
                            </div>
                            <div class="modal__btn">
                                <div data-remodal-action="close" class="c-button-square--mm">閉じる</div>
                            </div>
                        </div>
                    </div>
                    <div data-remodal-id="waysidemodal" data-search-id="wayside" class="modal waysidemodal">
                        <h1 class="modal__ttl modal__ttl--fix u-fs--l">沿線・駅</h1>
                        <div class="modal__inner u-fs--xs">
                            <div id="waysite_checkboxes" class="modal__checkbox"></div>
                            <div class="modal__btn modal__btn--fixed">
                                <div data-remodal-action="close" class="c-button-square--mm">閉じる</div>
                            </div>
                        </div>
                    </div>
                    <div data-remodal-id="hangupmodal" data-search-id="hangup" class="modal">
                        <h1 class="modal__ttl u-fs--l">職種</h1>
                        <div class="modal__inner u-fs--xs">
                            <h2 class="modal__sub__ttl">応募条件</h2>
                            <div class="modal__checkbox">
                                <input type="checkbox" name="a" id="hangupCheck01" data-num="1" class="search-checkbox">
                                <label for="hangupCheck01" class="search-checkbox__label--modal u-mt10">未経験OK</label>
                                <input type="checkbox" name="a" id="hangupCheck02" data-num="2" class="search-checkbox">
                                <label for="hangupCheck02" class="search-checkbox__label--modal u-mt10">年齢不問</label>
                                <input type="checkbox" name="a" id="hangupCheck03" data-num="3" class="search-checkbox">
                                <label for="hangupCheck03" class="search-checkbox__label--modal u-mt10">外国人歓迎</label>
                                <input type="checkbox" name="a" id="hangupCheck04" data-num="4" class="search-checkbox">
                                <label for="hangupCheck04" class="search-checkbox__label--modal u-mt10">WワークOK</label>
                            </div>
                            <h2 class="modal__sub__ttl u-mt30">待遇</h2>
                            <div class="modal__checkbox">
                                <input type="checkbox" name="a" id="hangupCheck05" data-num="5" class="search-checkbox">
                                <label for="hangupCheck05" class="search-checkbox__label--modal u-mt10">社会保険あり</label>
                                <input type="checkbox" name="a" id="hangupCheck06" data-num="6" class="search-checkbox">
                                <label for="hangupCheck06" class="search-checkbox__label--modal u-mt10">寮・社宅あり</label>
                                <input type="checkbox" name="a" id="hangupCheck07" data-num="7" class="search-checkbox">
                                <label for="hangupCheck07" class="search-checkbox__label--modal u-mt10">各種手当充実</label>
                                <input type="checkbox" name="a" id="hangupCheck08" data-num="8" class="search-checkbox">
                                <label for="hangupCheck08" class="search-checkbox__label--modal u-mt10">交通費支給</label>
                                <input type="checkbox" name="a" id="hangupCheck09" data-num="9" class="search-checkbox">
                                <label for="hangupCheck09" class="search-checkbox__label--modal u-mt10">研修あり</label>
                                <input type="checkbox" name="a" id="hangupCheck10" data-num="10"
                                       class="search-checkbox">
                                <label for="hangupCheck10" class="search-checkbox__label--modal u-mt10">制服貸与</label>
                                <input type="checkbox" name="a" id="hangupCheck11" data-num="11"
                                       class="search-checkbox">
                                <label for="hangupCheck11" class="search-checkbox__label--modal u-mt10">週休2日</label>
                                <input type="checkbox" name="a" id="hangupCheck12" data-num="12"
                                       class="search-checkbox">
                                <label for="hangupCheck12" class="search-checkbox__label--modal u-mt10">社員登用あり</label>
                            </div>
                            <h2 class="modal__sub__ttl u-mt30">企画・運営</h2>
                            <div class="modal__checkbox">
                                <input type="checkbox" name="a" id="hangupCheck13" data-num="13"
                                       class="search-checkbox">
                                <label for="hangupCheck13" class="search-checkbox__label--modal u-mt10">オープニング</label>
                                <input type="checkbox" name="a" id="hangupCheck14" data-num="14"
                                       class="search-checkbox">
                                <label for="hangupCheck14" class="search-checkbox__label--modal u-mt10">高収入</label>
                                <input type="checkbox" name="a" id="hangupCheck15" data-num="15"
                                       class="search-checkbox">
                                <label for="hangupCheck15" class="search-checkbox__label--modal u-mt10">定休日あり</label>
                                <input type="checkbox" name="a" id="hangupCheck16" data-num="16"
                                       class="search-checkbox">
                                <label for="hangupCheck16" class="search-checkbox__label--modal u-mt10">服装自由</label>
                                <input type="checkbox" name="a" id="hangupCheck17" data-num="17"
                                       class="search-checkbox">
                                <label for="hangupCheck17" class="search-checkbox__label--modal u-mt10">まかないあり</label>
                                <input type="checkbox" name="a" id="hangupCheck18" data-num="18"
                                       class="search-checkbox">
                                <label for="hangupCheck18" class="search-checkbox__label--modal u-mt10">個人経営</label>
                                <input type="checkbox" name="a" id="hangupCheck19" data-num="19"
                                       class="search-checkbox">
                                <label for="hangupCheck19" class="search-checkbox__label--modal u-mt10">海外勤務あり</label>
                                <input type="checkbox" name="a" id="hangupCheck20" data-num="20"
                                       class="search-checkbox">
                                <label for="hangupCheck20" class="search-checkbox__label--modal u-mt10">リゾート</label>
                                <input type="checkbox" name="a" id="hangupCheck21" data-num="21"
                                       class="search-checkbox">
                                <label for="hangupCheck21" class="search-checkbox__label--modal u-mt10">独立希望者歓迎</label>
                            </div>
                            <div class="modal__btn">
                                <div data-remodal-action="close" class="c-button-square--mm">閉じる</div>
                            </div>
                        </div>
                    </div>
                </form>
                <p class="column-left__search__breadcrumb u-fs--xl u-fwb u-mt20"><a href="#">{{$value or ''}}</a>の求人転職情報
                </p>
                <div class="column-left__info u-mt20">
                    <div class="column-left__info__text u-fs--l"><span class="u-fs--xxxl--recruit">{{ $contents->total() }}</span>件の求人</div>
                    <div class="pager">
                        @if($contents)
                            @include('pagination.default', array('paginator' => $contents))
                        @endif
                    </div>
                </div>
                <div class="column-box--wide">

                    @foreach($contents as $content)
                        @include('job._search_item', $content)
                    @endforeach

                </div>
                <div class="column-left__info u-mt30">
                    <div class="column-left__info__text u-fs--l"><span class="u-fs--xxxl--recruit">{{ $contents->total() }}</span>件の求人</div>
                    <div class="pager">
                        @if($contents)
                            @include('pagination.default', array('paginator' => $contents))
                        @endif
                    </div>
                </div>
            </div>
            <div class="column-right">
                <div data-target-rightcolumn="target" class="column-right__inner">
                    <div class="column-box--rightTop">
                        <div class="column-box__ttl u-fs--s u-fwb">〈今日のキレイにスルンダ!〉</div>
                        <div class="column-box--rightTop__num"><span
                                    class="-rightTop__num__subText u-fs--l">求人数</span><span data-target-numcheck="6"
                                                                                            class="-rightTop__num__text u-fs--xxxl u-fwb">00,000</span><span
                                    class="-rightTop__num__subText u-fs--l">件</span></div>
                        <div class="c-icon--circle--baloon"><i class="c-icon--triangle--rotate"></i><span
                                    class="-rightTop__new__subText u-fs--l">新着</span>
                            <p data-target-numcheck="3" class="-rightTop__new__text u-fs--xl u-fwb">000<span
                                        class="u-fs--xxs">件</span></p>
                        </div>
                    </div>
                    <ul class="nav-box u-fs--xs u-mt30">
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><span
                                        class="nav-box__list__icon"><i class="c-icon--home"></i></span><span>HOME</span></a>
                        </li>
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><span
                                        class="nav-box__list__icon"><i
                                            class="c-icon--secret"></i></span><span>限定求人</span></a></li>
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><span
                                        class="nav-box__list__icon"><i
                                            class="c-icon--content"></i></span><span>コンテンツまとめ</span></a></li>
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                        class="nav-box__list__icon"><i
                                            class="c-icon--book"></i></span><span>キレイJOB図鑑</span></a></li>
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                        class="nav-box__list__icon"><i
                                            class="c-icon--beauty"></i></span><span>キレイにナルンダ</span></a></li>
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                        class="nav-box__list__icon"><i
                                            class="c-icon--hat"></i></span><span>集客の知恵袋</span></a></li>
                        <li class="nav-box__list"><a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span
                                        class="nav-box__list__icon"><i
                                            class="c-icon--star-other"></i></span><span>スタア発掘</span></a></li>
                    </ul>
                    <div class="column-box--right__search u-mt30">
                        <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                            <select name="都道府県" class="quick-search__select u-fs--m">
                                <option value="" selected>都道府県</option>
                                <option value="北海道">北海道</option>
                                <option value="青森県">青森県</option>
                                <option value="岩手県">岩手県</option>
                                <option value="宮城県">宮城県</option>
                                <option value="秋田県">秋田県</option>
                                <option value="山形県">山形県</option>
                                <option value="福島県">福島県</option>
                                <option value="茨城県">茨城県</option>
                                <option value="栃木県">栃木県</option>
                                <option value="群馬県">群馬県</option>
                                <option value="埼玉県">埼玉県</option>
                                <option value="千葉県">千葉県</option>
                                <option value="東京都">東京都</option>
                                <option value="神奈川県">神奈川県</option>
                                <option value="新潟県">新潟県</option>
                                <option value="富山県">富山県</option>
                                <option value="石川県">石川県</option>
                                <option value="福井県">福井県</option>
                                <option value="山梨県">山梨県</option>
                                <option value="長野県">長野県</option>
                                <option value="岐阜県">岐阜県</option>
                                <option value="静岡県">静岡県</option>
                                <option value="愛知県">愛知県</option>
                                <option value="三重県">三重県</option>
                                <option value="滋賀県">滋賀県</option>
                                <option value="京都府">京都府</option>
                                <option value="大阪府">大阪府</option>
                                <option value="兵庫県">兵庫県</option>
                                <option value="奈良県">奈良県</option>
                                <option value="和歌山県">和歌山県</option>
                                <option value="鳥取県">鳥取県</option>
                                <option value="島根県">島根県</option>
                                <option value="岡山県">岡山県</option>
                                <option value="広島県">広島県</option>
                                <option value="山口県">山口県</option>
                                <option value="徳島県">徳島県</option>
                                <option value="香川県">香川県</option>
                                <option value="愛媛県">愛媛県</option>
                                <option value="高知県">高知県</option>
                                <option value="福岡県">福岡県</option>
                                <option value="佐賀県">佐賀県</option>
                                <option value="長崎県">長崎県</option>
                                <option value="熊本県">熊本県</option>
                                <option value="大分県">大分県</option>
                                <option value="宮崎県">宮崎県</option>
                                <option value="鹿児島県">鹿児島県</option>
                                <option value="沖縄県">沖縄県</option>
                            </select>
                        </div>
                        <span class="c-icon--cross"></span>
                        <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                            <select name="雇用形態" class="quick-search__select u-fs--m">
                                <option value="正社員">正社員</option>
                                <option value="その他">その他</option>
                                <option value="契約社員">契約社員</option>
                                <option value="業務委託">業務委託</option>
                                <option value="アルバイト">アルバイト</option>
                            </select>
                        </div>
                        <span class="c-icon--cross"></span>
                        <input type="text" placeholder="キーワード" class="quick-search__input u-fs--m">
                        <button type="submit" class="c-button-square--full u-fs--l u-mt20"><span class="button-inner">カンタン検索</span>
                        </button>
                    </div>
                    <div class="column-box--rightContents u-mt30">
                        <h3 class="column-box__ttl u-fs--l u-fwb"><i class="column-box__ttl__icon c-icon--heart"></i>人気コンテンツ
                        </h3>
                        <div class="column-box--rightContents__list"><a href="#">
                                <div data-normal="bg" data-layzr-bg="../../img/media/media-s01.jpg"
                                     class="-rightContents__list__img"><span class="c-icon--tag"><span
                                                class="u-fwb">人気</span></span></div>
                                <div class="-rightContents__list__right">
                                    <p class="-rightContents__list__text u-fs--sh">保存期間1ヶ月！作り置きに便利なみかんをたっ</p><span
                                            class="_list__right__tag c-tag--star u-fs--xxxs u-fwb">スタア発掘</span><span
                                            class="c-icon--eye"></span><span class="u-fs--xxs">2368</span>
                                </div>
                            </a></div>
                        <div class="column-box--rightContents__list"><a href="#">
                                <div data-normal="bg" data-layzr-bg="../../img/media/media-s01.jpg"
                                     class="-rightContents__list__img"><span class="c-icon--tag"><span
                                                class="u-fwb">人気</span></span></div>
                                <div class="-rightContents__list__right">
                                    <p class="-rightContents__list__text u-fs--sh">保存期間1ヶ月！作り置きに便利なみかんをたっ</p><span
                                            class="_list__right__tag c-tag--beauty u-fs--xxxs u-fwb">キレイにナルンダ</span><span
                                            class="c-icon--eye"></span><span class="u-fs--xxs">2368</span>
                                </div>
                            </a></div>
                        <div class="column-box--rightContents__list"><a href="#">
                                <div data-normal="bg" data-layzr-bg="../../img/media/media-s01.jpg"
                                     class="-rightContents__list__img"><span class="c-icon--tag"><span
                                                class="u-fwb">人気</span></span></div>
                                <div class="-rightContents__list__right">
                                    <p class="-rightContents__list__text u-fs--sh">
                                        保存期間1ヶ月！作り置きに便利なみかんをたったったったったったったったっ</p><span
                                            class="_list__right__tag c-tag--work u-fs--xxxs u-fwb">キレイJOB図鑑</span><span
                                            class="c-icon--eye"></span><span class="u-fs--xxs">2368</span>
                                </div>
                            </a></div>
                        <div class="column-box--rightContents__list"><a href="#">
                                <div data-normal="bg" data-layzr-bg="../../img/media/media-s01.jpg"
                                     class="-rightContents__list__img"><span class="c-icon--tag"><span
                                                class="u-fwb">人気</span></span></div>
                                <div class="-rightContents__list__right">
                                    <p class="-rightContents__list__text u-fs--sh">保存期間1ヶ月！作り置きに便利なみかんをたっ</p><span
                                            class="_list__right__tag c-tag--sns u-fs--xxxs u-fwb">集客の知恵袋</span><span
                                            class="c-icon--eye"></span><span class="u-fs--xxs">2368</span>
                                </div>
                            </a></div>
                        <div class="column-box--rightContents__list"><a href="#">
                                <div data-normal="bg" data-layzr-bg="../../img/media/media-s01.jpg"
                                     class="-rightContents__list__img"><span class="c-icon--tag"><span
                                                class="u-fwb">人気</span></span></div>
                                <div class="-rightContents__list__right">
                                    <p class="-rightContents__list__text u-fs--sh">保存期間1ヶ月！作り置きに便利なみかんをたっ</p><span
                                            class="_list__right__tag c-tag--sns u-fs--xxxs u-fwb">集客の知恵袋</span><span
                                            class="c-icon--eye"></span><span class="u-fs--xxs">2368</span>
                                </div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop




@section('page_bottom')
    <footer>
        <div class="footer__inner"><span data-trigger-totop="target"
                                         class="u-fs--s _inner__button--toTop c-button-square--toTop"><i
                        class="_button--toTop__icon c-icon--triangle--top"></i>上に戻る</span>
            <ul class="footer__nav">
                <li class="u-fs--xxs _inner__block--left">
                    <ul class="footer__inner__list">
                        <li><span class="u-arrow__text">&gt;</span><a href="#">HOME</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">限定求人</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">コンテンツまとめ</a></li>
                    </ul>
                </li>
                <li class="u-fs--xxs _inner__block--middle">
                    <p class="_inner__block__title u-fwb">コンテンツカテゴリー</p>
                    <ul class="footer__inner__rows footer__inner__list">
                        <li><span class="u-arrow__text">&gt;</span><a href="#">キレイにナルンダ</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">キレイJOB図鑑</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">集客の知恵袋</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">スタア発掘</a></li>
                    </ul>
                </li>
                <li class="u-fs--xxs _inner__block--right">
                    <p class="_inner__block__title u-fwb">キレイにスルンダについて</p>
                    <ul class="footer__inner__rows footer__inner__list">
                        <li><span class="u-arrow__text">&gt;</span><a href="#">キレイにスルンダとは</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">お問い合わせ</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">運営会社</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">プライバシーポリシー</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">求人掲載について</a></li>
                        <li><span class="u-arrow__text">&gt;</span><a href="#">ログイン/新規登録</a></li>
                    </ul>
                </li>
                <li class="footer__social__block"><a href="#" class="c-button-square--social c-facebook"><i
                                class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i><span class="u-fs--l">シェア</span></a><a
                            href="#" class="c-button-square--social c-twitter u-mt10"><i
                                class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i><span class="u-fs--l">ツイート</span></a>
                    <p class="_social__block__text u-mt20">&copy;2016 kireinista Co.,Ltd.</p>
                </li>
            </ul>
        </div>
    </footer>
    <script src="/public/js/app.js"></script>
@stop