<!DOCTYPE html>
<html lang="jp">
<head>
    <title>キレイにスルンダ</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta name="viewport" content="width=1080">
    <link rel="stylesheet" href="/public/css/app.css">
    <link rel="shortcut icon" href="/public/img/ico/favicon.ico">
    <link rel="icon" type="image/png" href="/public/img/ico/favicon.png" sizes="32x32">
    @yield('page_head')
    @yield('page_top')
</head>
<body>
@yield('page_content')
@yield('page_bottom')
</body>
</html>