<?php
	$similarJobsList = _modules::get_similar_secret_jobs($companyId,$jobId);

	$secretUrl = _helper::getUrlName('secretPage');

?>

@if(!empty($similarJobsList))
<div class="column-box__secret--bottom u-mt40">
    <h3 class="_secret--bottom__heading u-fs--l u-fwb">この企業・店舗が募集しているほかの求人</h3>
    @foreach($similarJobsList as $job)
        <ul class="_secret--bottom__list u-fwb"><a href="{{'/'. $secretUrl .'/'.$job['id']}}">
                <li class="_secret--bottom__lists -bottom__list__header u-fs--xs--secret">{{$job['store_name']}}/{{$job['company_name']}}</li>
                <li class="_secret--bottom__lists u-fs--xs--secret">
                    {{str_replace(",","/",_helper::getJobCategoryNameByListId(null,$job['list_job']))}}/{{$job['job_name']}}
                </li>
            </a></ul>
    @endforeach
</div>
@endif