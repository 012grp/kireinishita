<?php
	$class_primary_box = 'u-mt20';
	$company_name = strip_tags($detail['name']);
	$job_name = strip_tags($detail['job_name']);
	$job_salary = strip_tags($detail['job_salary']);
	$job_location = strip_tags($detail['job_location']);
	$job_career = strip_tags($detail['job_career']);
	$celebration = strip_tags($detail['celebration']);
	$job_image = $detail['job_image'];
	$job_genre = $detail['job_genre'];
	$job_appSelection = $detail['job_appSelection'];
	$job_url = $detail['job_url'];
	$job_personnel = $detail['job_personnel'];
    $meta_keyword = $detail['meta_keyword'];

	$employment_status = strip_tags($detail['employment_status']);
	$job_description = strip_tags($detail['job_description']);
	$job_notice_header = strip_tags($detail['job_notice_header']);
	$job_notice_content = strip_tags($detail['job_notice_content']);
	$job_notice_image = strip_tags($detail['job_notice_image']);
	$job_salary_detail = $detail['job_salary_detail'];
	$content = $detail['content'];
	$job_qualification = $detail['job_qualification'];
	$job_location_detail = $detail['job_location_detail'];
	$treatment_welfare = $detail['treatment_welfare'];
	$job_holiday = $detail['job_holiday'];
	$job_crated_at = $detail['job_created_at'];

	$list_hangup = _helper::getCategoryNamesByListId('',$detail['list_hangup']);


	$list_category = _helper::getJobCategoryNameByListId($detail['list_job_parent'],$detail['list_job']);

	$list_em = _helper::getEmploymentNamesByListId($detail['list_em']);

	$access = $detail['access'];

	$company_category = strip_tags($detail['category']);
	$company_address = strip_tags($detail['address']);
	$company_average_price = strip_tags($detail['average_price']);
	$company_seat_number = strip_tags($detail['seat_number']);
	$company_holiday = strip_tags($detail['holiday']);
	$company_access = strip_tags($detail['company_access']);
	$company_url = strip_tags($detail['website']);

	if(empty($job_notice_image))
	{
		$job_notice_image = _common::noImageUrl();
	}

	$key = Config::get('webconfig.google_map_api_key');

	//social
	$request_url = Request::url();
	$title = _helper::set_title($job_name);

	//SEO
	$web_title = sprintf(_helper::set_title(trans('webinfo.title_secret_detail')), $job_name);
	
	// $description = !empty($detail['meta_desc']) ? $detail['meta_desc'] : trans('webinfo.meta_desc_secret_job');
	// if($description == "")
	// {
	// 	$description = null;
	// }

	//$keywords = $detail['meta_keyword'];
	$description = sprintf(trans('webinfo.meta_desc_secret_detail'), $company_name,$job_name);
	$keywords = sprintf(trans('webinfo.keywords_secret_detail'), $company_name);

	

	if(!empty($job_image))
	{
		$image = $job_image;
	}

	//TODO: set company name length to show
	$google_map_company_name = $company_name;
	if(mb_strlen($google_map_company_name) > 17)
	{
		$google_map_company_name = mb_substr($google_map_company_name,0,17,'utf-8').'...';
	}

	$secretUrl = _helper::getUrlName('secretPage');

	$content = _common::removeImageOfSP($content);
?>

@section('css')
<style type="text/css">
	._secret__detail__table.is-disable{
		display: block !important;
		visibility: hidden;
		height: 0px;
	}
</style>
@stop
@extends('layouts.default')

@section('column_right')
  	@include('shared.right_column.no_lastest_article')
@stop

@section('js_google')
	<input type="hidden" id="job_lat" value="<?php echo $detail['lat']?>">
	<input type="hidden" id="job_lng" value="<?php echo $detail['lng']?>">
	<input type="hidden" id="google_map_company_name" value="<?php echo $google_map_company_name ?>">
	<input type="hidden" id="job_location" value="<?php echo $job_location?>">

	<input type="hidden" id="company_lat" value="<?php echo $detail['company_lat']?>">
	<input type="hidden" id="company_lng" value="<?php echo $detail['company_lng']?>">
	<input type="hidden" id="address_com" value="<?php echo $detail['address']?>">

	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>
@stop

@section('breadcrumb')
	@include('breadcrumb.default', array(
							'page' 		=> '株式会社'.$company_name .'の求人',
							'parent'	=> array(
									array('link' => '/'. $secretUrl .'/', 'title' => 'オリジナル求人'),
								)
						))
@stop

@section('column_left')
	<div data-target-leftcolumn="target" class="column-left">
		<article class="column-box__secret">
			<span style="font-size: 12px">{{$detail['store_name']}}</span>
			<div class="column-box__secret__heading">
				<div class="_secret__heading__left u-fs--xs--recruit u-fwb">{{ $company_name }}</div>
				<div class="_secret__heading__right"><span class="u-fs--xxs c-tag--secret--flow">オリジナル</span>
				@if($job_crated_at > (new DateTime('-1 day'))->format('Y-m-d H:i:s'))
					<span class="u-fs--xxs c-tag--new--flow">NEW</span>
				@endif
				@if(!empty($celebration))
				<span class="u-fs--xxs c-tag--celebration">{{ $celebration }}</span>
				@endif
				</div>
				<h1 class="_secret__heading__ttl u-fs--xxlh--contents u-fwb">{{ $job_name }}</h1>
			</div>
			@if($slider_image)
				<div class="column-box__secret__slider u-mt15">
					<div data-target-slider="target" class="_secret__slider__main">
						@foreach($slider_image as $item)
							<div class="_slider__main__img"><img src="{{ $item->image }}" alt=""></div>
						@endforeach
					</div>
					<div class="_secret__slider-nav">
						<div data-trigger-slider="trigger" class="slider-nav">
							@foreach($slider_image as $key => $item)
								@if($key == 0)
									<div class="thumbnail__img"><img src="{{ $item->image }}" alt=""></div>
								@else
									<div class="thumbnail__img u-mt10"><img src="{{ $item->image }}" alt=""></div>
								@endif
							@endforeach
						</div>
					</div>
				</div>
			@endif
			<div class="_secret__tag u-mt10">
				<?php $list_hangup = empty($list_hangup)?array():explode(',',$list_hangup); ?>
				@foreach($list_hangup as $item)
					<span class="c-tag--secret--detail u-fs--xs">{{ $item }}</span>
				@endforeach
			</div>
			<div class="_secret__detail__block u-fs--lh--contents">
			{!! $content !!}
			</div>
			@if($job_notice_header)
				<div class="_secret__notice u-mt25">
					<div style="background-image:url({{ $job_notice_image }})" class="_secret__notice__img"></div>
					<div class="_secret__notice__right"><span class="c-tag--secret--notice u-fs--xs">注目ポイント！</span>
						<div class="_secret__notice__ttl u-fwb u-fs--sh--secret u-mt10">{{ $job_notice_header }}</div>
						<div class="_secret__notice__text u-fs--xs--recruit u-mt5">
							{{ $job_notice_content }}
						</div>
					</div>
				</div>
			@endif

			<div class="_secret__social__block u-mt30">
				<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $request_url }}&amp;src=sdkpreparse" class="c-button-square--social c-facebook">
					<i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i><span class="u-fs--l">シェア</span>
				</a>
				<a href="https://twitter.com/intent/tweet?url={{ $request_url }}&amp;text={{ $title }}" class="c-button-square--social c-twitter">
					<i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i><span class="u-fs--l">ツイート</span>
				</a>
			</div>
		</article>
		<div class="column-box__secret--table">
			<ul class="_secret__detail__tab">
				<li data-trigger-tab="0" class="_detail__tab__inner is-active ">募集情報</li>
				<li data-trigger-tab="1" class="_detail__tab__inner ">企業情報・店舗情報</li>
			</ul>
			<div class="_secret__detail--recruit">
				<table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit" >
					<tbody>
						@if(!empty($job_genre))
						<tr>
							<td class="_detail__table__left u-fwb">ジャンル</td>
							<td class="_detail__table__right">{{$job_genre}}</td>
						</tr>
						@endif

						@if(!empty($list_category))
						<tr>
							<td class="_detail__table__left u-fwb">職種</td>
							<td class="_detail__table__right">
								@if($list_category)
									<?php _helper::explode_character($list_category); ?>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($list_em))
						<tr>
							<td class="_detail__table__left u-fwb">雇用形態</td>
							<td class="_detail__table__right">
								<p class="_detail__table__text">
								@if($list_em)
									<?php _helper::explode_character($list_em); ?>
								@endif
								</p>
							</td>
						</tr>
						@endif

						@if(!empty($job_description))
						<tr>
							<td class="_detail__table__left u-fwb">仕事内容</td>
							<td class="_detail__table__right">
								<p class="_detail__table__text"></p>
                                @if($job_description)
									<?php _helper::explode_ul($job_description); ?>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($job_salary))
						<tr>
							<td class="_detail__table__left u-fwb">給与</td>
							<td class="_detail__table__right">
								@if($job_salary)
									<?php _helper::explode_ul($job_salary, '', ''); ?>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($job_qualification))
						<tr>
							<td class="_detail__table__left u-fwb">応募資格</td>
							<td class="_detail__table__right"> 
							    <?php /*
								<p class="_detail__table__text">
									@if($list_em)
										<?php _helper::explode_character($list_em); ?>
									@endif
								</p>

								*/ ?>

								@if($job_qualification)
									<?php _helper::explode_ul($job_qualification, 'u-mt10'); ?>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($job_location))
						<tr>
							<td class="_detail__table__left u-fwb">勤務地</td>
							<td class="_detail__table__right">
                                    @if($job_location)
										<?php _helper::explode_ul($job_location); ?>
									@endif
                            </td>
						</tr>
						@endif

						@if(!empty($access))
						<tr>
							<td class="_detail__table__left u-fwb">アクセス</td>
							<td class="_detail__table__right">
								@if($access)
									<?php _helper::explode_ul($access); ?>
								@endif
								@if(!empty($detail['lat']) && !empty($detail['lng']) )
								<div class="_detail__table__map u-mt10" id="job_map"></div>
								<a target="_blank" href="https://www.google.com/maps/place/{{$job_location}},+Japan/{{'@'}}{{ $detail['lat'] or ''}},{{$detail['lng'] or ''}},17z?hl=ja-JP" id="url_job_map">
								GoogleMapで確認する</a>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($treatment_welfare))
						<tr>
							<td class="_detail__table__left u-fwb">待遇・福利厚生</td>
							<td class="_detail__table__right">
								@if($treatment_welfare)
									<?php _helper::explode_ul($treatment_welfare); ?>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($job_holiday))
						<tr>
							<td class="_detail__table__left u-fwb">休日休暇</td>
							<td class="_detail__table__right">
								@if($job_holiday)
									<?php _helper::explode_ul($job_holiday); ?>
								@endif
							</td>
						</tr>
						@endif

						@if(!empty($job_personnel))
						<tr>
							<td class="_detail__table__left u-fwb">求める人材について</td>
							<td class="_detail__table__right">
                                @if($job_personnel)
									<?php _helper::explode_ul($job_personnel); ?>
								@endif
                            </td>
						</tr>
						@endif

						@if(!empty($job_appSelection))
						<tr>
							<td class="_detail__table__left u-fwb">応募・選考について</td>
							<td class="_detail__table__right">
                                @if($job_appSelection)
									<?php _helper::explode_ul($job_appSelection); ?>
								@endif
                            </td>
						</tr>
						@endif
					</tbody>
				</table>
				<div data-target-table="target" class="_secret__detail__table is-disable" style="    border: none;">
					<br>
					<h1 style="font-size: 150%; color: #7e7d7d; font-weight: 700">店舗情報</h1>
					<br>
				</div>
				<table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit is-disable">
					<tbody>
						@if(!empty($detail['store_name']))
						<tr>
							<td class="_detail__table__left u-fwb">店名</td>
							<td class="_detail__table__right">{{$detail['store_name']}}</td>
						</tr>
						@endif

						@if(!empty($detail['shopStyle']))
						<tr>
							<td class="_detail__table__left u-fwb">店舗形態</td>
							<td class="_detail__table__right">{{$detail['shopStyle']}}</td>
						</tr>
						@endif

						@if(!empty($detail['address']) AND strlen($detail['address']) > 0)
						<tr>
							<td class="_detail__table__left u-fwb">事務所</td>
							<td class="_detail__table__right">{{$detail['address']}}</td>
						</tr>
						@endif

						@if(!empty($detail['business_hrs']))
						<tr>
							<td class="_detail__table__left u-fwb">営業時間</td>
							<td class="_detail__table__right">{!! nl2br($detail['business_hrs']) !!}</td>
						</tr>
						@endif

						@if(!empty($detail['reg_holiday']))
						<tr>
							<td class="_detail__table__left u-fwb">定休日</td>
							<td class="_detail__table__right">{!! nl2br($detail['reg_holiday']) !!}</td>
						</tr>
						@endif

						@if(!empty($detail['customer_base']))
						<tr>
							<td class="_detail__table__left u-fwb">客層</td>
							<td class="_detail__table__right">{!! nl2br($detail['customer_base']) !!}</td>
						</tr>
						@endif

						@if(!empty($detail['job_atmosphere']) OR !empty($detail['job_education']) OR !empty($detail['job_challenges']))
						<tr>
							<td class="_detail__table__left u-fwb">よくあるご質問</td>
							<td class="_detail__table__right">
							@if(!empty($detail['job_atmosphere']))
							<?php if (!empty($detail['job_atmosphere'])){ ?>
								<h1 class="faq_subheader u-fwb"">職場の雰囲気について</h1>
							<?php } ?>
							{!! nl2br($detail['job_atmosphere']) !!}
							@endif

							@if(!empty($detail['job_education']))
							<?php if (!empty($detail['job_education'])){ ?>
								<h1 class="faq_subheader u-fwb"">スキルアップや教育制度について</h1>
							<?php } ?>
							{!! nl2br($detail['job_education']) !!}
							@endif
							
							@if(!empty($detail['job_challenges']))
							<?php if (!empty($detail['job_challenges'])){ ?>
								<h1 class="faq_subheader u-fwb"">仕事のやりがいについて</h1>
							<?php } ?>
							{!! nl2br($detail['job_challenges']) !!}
							@endif
							</td>
						</tr>
						@endif

						@if(!empty($detail['male_female_ratio']))
						<tr>
							<td class="_detail__table__left u-fwb">スタッフ数や男女比</td>
							<td class="_detail__table__right">{!! nl2br($detail['male_female_ratio']) !!}</td>
						</tr>
						@endif

						@if(!empty($detail['job_url']))
						<tr>
							<td class="_detail__table__left u-fwb">URL</td>
							<td class="_detail__table__right"><a href="{{$detail['job_url']}}" target="_blank">{{$detail['job_url']}}</a></td>
						</tr>
						@endif
					</tbody>
				</table>

				@if($status == 1)
				<div data-target-table="target" class="_secret__detail__table is-disable" style="    border: none;">
					<br><br><br>
					<h1 style="font-size: 150%; color: #7e7d7d; font-weight: 700">企業情報</h1>
					<br>
				</div>
				<table data-target-table="target" class="_secret__detail__table u-fs--xs--recruit is-disable">
					<tbody>
						@if(!empty($detail['name']))
						<tr>
							<td class="_detail__table__left u-fwb">企業名</td>
							<td class="_detail__table__right">{{$detail['name']}}</td>
						</tr>
						@endif

						@if(!empty($detail['reprentative']))
						<tr>

							<td class="_detail__table__left u-fwb">代表者名</td>
							<td class="_detail__table__right">{{$detail['representative']}}</td>
						</tr>
						@endif

						@if(!empty($detail['work_content']))
						<tr>
							<td class="_detail__table__left u-fwb">事業内容</td>
							<td class="_detail__table__right">{{$detail['work_content']}}</td>
						</tr>
						@endif

						@if(!empty($detail['map_location']))
						<tr>
							<td class="_detail__table__left u-fwb">本社所在地</td>
							<td class="_detail__table__right">{{$detail['map_location']}}</td>
						</tr>
						@endif

						@if(!empty($detail['website']))
						<tr>
							<td class="_detail__table__left u-fwb">HP</td>
							<td class="_detail__table__right"><a href="{{$detail['website']}}" target="_blank">{{$detail['website']}}</a></td>
						</tr>
						@endif
					</tbody>
				</table>
				@endif
			</div>

			<!-- <div style="border: 1px solid black;">
				<?php foreach ($detail as $key => $value) {
        			echo $key . ' => ' . $value . '<br/>';
        		} ?>
			</div> -->

			<div class="column-box__secret__btnBLock">
				<div data-id="{{$detail['id']}}" data-secret="1" class="c-button-square--l--gray u-fs--xxl u-fwb bookmark_job"><span class="c-font-icon--attachment"></span>キープする</div>
				<div class="c-button-square--l u-fs--xxl u-fwb"><a href="/entry/{{$detail['id']}}">応募する</a></div>
			</div>
		</div>

		@include('secret_kyujin._similar_job', array('jobId'=> $detail['id'],'companyId'=> $detail['company_id']))
	</div>
@stop
