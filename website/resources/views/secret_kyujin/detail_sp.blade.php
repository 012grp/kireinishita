<?php
/*
@section('css')
<link rel="stylesheet" href="/public/sp/css/content.css">
@stop
*/
$request_url = Request::url();


$company_name = strip_tags($detail['name']);
$job_name = strip_tags($detail['job_name']);
$job_salary = strip_tags($detail['job_salary']);
$job_location = strip_tags($detail['job_location']);
$job_career = strip_tags($detail['job_career']);
$celebration = strip_tags($detail['celebration']);
$job_image = $detail['job_image'];
$job_genre = $detail['job_genre'];
$job_appSelection = $detail['job_appSelection'];
$job_url = $detail['job_url'];
$job_personnel = $detail['job_personnel'];

$employment_status = strip_tags($detail['employment_status']);
$job_description = strip_tags($detail['job_description']);
$job_notice_header = strip_tags($detail['job_notice_header']);
$job_notice_content = strip_tags($detail['job_notice_content']);
$job_notice_image_sp = strip_tags($detail['job_notice_image_sp']);
$job_salary_detail = $detail['job_salary_detail'];
$content = $detail['content'];
$job_qualification = $detail['job_qualification'];
$job_location_detail = $detail['job_location_detail'];
$treatment_welfare = $detail['treatment_welfare'];
$job_holiday = $detail['job_holiday'];
$job_crated_at = $detail['job_created_at'];

$list_hangup = _helper::getCategoryNamesByListId('',$detail['list_hangup']);
	
	
$list_category = _helper::getJobCategoryNameByListId($detail['list_job_parent'],$detail['list_job']);

$list_em = _helper::getEmploymentNamesByListId($detail['list_em']);

$access = $detail['access'];

$company_category = strip_tags($detail['category']);
$company_address = strip_tags($detail['address']);
$company_average_price = strip_tags($detail['average_price']);
$company_seat_number = strip_tags($detail['seat_number']);
$company_holiday = strip_tags($detail['holiday']);
$company_access = strip_tags($detail['company_access']);

if(empty($job_notice_image_sp))
{
    if(!empty($detail['job_notice_image'])){
        $job_notice_image_sp = $detail['job_notice_image'];
    }else{
        $job_notice_image_sp = _common::noImageUrl();
    }

}

$key = Config::get('webconfig.google_map_api_key');

//social
$request_url = Request::url();
$title = _helper::set_title($job_name);

//SEO
$web_title = sprintf(_helper::set_title(trans('webinfo.title_secret_detail')), $job_name);
// $description = isset($detail['meta_desc']) ? $detail['meta_desc'] : trans('webinfo.meta_desc_secret_job');
// if($description == "")
// {
//     $description = null;
// }

//$keywords = $detail['meta_keyword'];
$description = sprintf(trans('webinfo.meta_desc_secret_detail'), $company_name,$job_name);
$keywords = sprintf(trans('webinfo.keywords_secret_detail'), $company_name);


if(!empty($job_image))
{
	$image = $job_image;
}

//TODO: set company name length to show
$google_map_company_name = $company_name;
if(mb_strlen($google_map_company_name) > 17)
{
	$google_map_company_name = mb_substr($google_map_company_name,0,17,'utf-8').'...';
}

$HP = _helper::getRelatedSecretJobs($detail['id'], $detail['company_id']);

$content = _common::changeCSSContentSecret($content);

$content = _common::removeImageOfPC($content);
?>

@extends('layouts.default_sp')

@section('css')
<style type="text/css">
	._secret__detail__table.is-disable{
		display: block !important;
		visibility: hidden;
		height: 0px;
		overflow: hidden;
	}
</style>
@stop

@section('js_google')
    <input type="hidden" id="job_lat" value="<?php echo $detail['lat']?>">
    <input type="hidden" id="job_lng" value="<?php echo $detail['lng']?>">
    <input type="hidden" id="google_map_company_name" value="<?php echo $google_map_company_name ?>">
    <input type="hidden" id="job_location" value="<?php echo $job_location?>">

    <input type="hidden" id="company_lat" value="<?php echo $detail['company_lat']?>">
    <input type="hidden" id="company_lng" value="<?php echo $detail['company_lng']?>">
    <input type="hidden" id="address_com" value="<?php echo $detail['address']?>">

    <script src="https://maps.google.com/maps/api/js?libraries=geometry&key={{ $key }}"></script>

@stop

@section('page_sp')
    <div class="primary-box--noPadding">
        <div class="contents-box--recruit__top">
            <h1 class="-recruit__top__ttl u-fwb u-fs--xs">
            <span style="font-size: 12px">{{$detail['store_name']}}</span><br>
            {{ $company_name }}
            </h1>
            
            <div class="-recruit__top__inner">
                <div class="-wide__list__tagBlock u-mt20">
                    <div class="-wide__list__tagBlock--l"><span class="u-fs--xxs c-tag--secret--flow">オリジナル</span>
                       @if(!empty($celebration))
                       <span class="u-fs--xxs c-tag--celebration">{{ $celebration }}</span>
                       @endif
                    </div>
                                
                    <div class="-wide__list__tagBlock--r">
                    	@if($job_crated_at > (new DateTime('-1 day'))->format('Y-m-d H:i:s'))
                    	<span class="u-fs--xxs c-tag--new--flow">NEW</span>
                    	@endif
                    </div>
                </div>
                <h2 class="contents-box--recruit__ttl u-fs--xlh u-mt25 u-fwb">
                    {{ $job_name }}</h2>
               
            </div>
            @if($slider_image)
            <div class="column-box__secret__slider u-mt15">
                <div data-target-slider="target" class="_secret__slider__main">
                    @foreach($slider_image as $item)
                        <div class="_slider__main__img"><img src="{{ $item->image }}" alt=""></div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
        <div class="contents-box--recruit__middle u-mt15">
                  <?php $list_hangup = empty($list_hangup)?array():explode(',',$list_hangup); ?>
				  @foreach($list_hangup as $item)
					<span class="c-tag--secret--detail u-fs--xs">{{ $item }}</span>
				  @endforeach
			<div class="custom-detail-contens">
           	{!! $content !!}
            </div>
            
            @if($job_notice_header)
            <div class="contents-box--recruit__attention u-mt10"><span
                        class="c-tag--notice u-fs--xxs u-fwb">注目ポイント！</span>

                <div class="-recruit__attention__top">
                    <figure class="-recruit__attention__img"><img src="{{ $job_notice_image_sp }}"></figure>
                    <h3 class="-recruit__attention__ttl u-fs--mh--contents u-fwb">{{ $job_notice_header }}</h3>
                </div>
                <p class="-recruit__attention__text u-fs--sh--recruit u-mt10">{{ $job_notice_content }}</p>
            </div>
            @endif

        </div>

        <div class="column-box__secret--table u-mt20">
            <ul class="_secret__detail__tab">
                <li data-trigger-tab="0" class="_detail__tab__inner is-active u-fs--xs">募集情報</li>
                <li data-trigger-tab="1" class="_detail__tab__inner u-fs--xs">企業情報・店舗情報</li>
            </ul>
            <div class="_secret__detail--recruit">
                @include('shared_sp.item.secret_kyujin_recruitment_information')
                @include('shared_sp.item.secret_kyujin_store_information')
            </div>
        </div>
    </div>

    @include('secret_kyujin._similar_job_sp', array('jobId'=> $detail['id'],'companyId'=> $detail['company_id']))
    
    <div class="column-box__secret__btnBLock">
        <div data-id="{{$detail['id']}}" data-secret="1" class="c-button-square--l--gray u-fs--xxl u-fs--xs bookmark_job">
        	<span class="c-font-icon--attachment"></span>キープする
        </div>
        <div class="c-button-square--l u-fs--xxl u-fs--xs"><a href="/entry/{{$detail['id']}}">応募する</a></div>
   	</div>
@stop