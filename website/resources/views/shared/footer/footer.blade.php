<?php
	$arrCategory = _helper::get_list_menu();
	$secretUrl = _helper::getUrlName('secretPage');
?>

<footer>
	<div class="footer__inner"><span data-trigger-totop="target" class="u-fs--s _inner__button--toTop c-button-square--toTop"><i class="_button--toTop__icon c-icon--triangle--top"></i>上に戻る</span>
		<ul class="footer__nav">
			<li class="u-fs--xxs _inner__block--left">
				<ul class="footer__inner__list">
					<li><span class="u-arrow__text">&gt;</span><a href="/">HOME</a></li>
					<li><span class="u-arrow__text">&gt;</span><a href="/{{$secretUrl}}">オリジナル求人</a></li>
					<li><span class="u-arrow__text">&gt;</span><a href="/contents">コンテンツまとめ</a></li>
				</ul>
			</li>
			<li class="u-fs--xxs _inner__block--middle">
				<p class="_inner__block__title u-fwb">コンテンツカテゴリー</p>
				<ul class="footer__inner__rows footer__inner__list">
				@foreach($arrCategory as $category)
					<li><span class="u-arrow__text">&gt;</span><a href="{{$category['url']}}">{{$category['name']}}</a></li>
				@endforeach
				</ul>

			</li>
			<li class="u-fs--xxs _inner__block--right">
				<p class="_inner__block__title u-fwb">キレイにスルンダについて</p>
				<ul class="footer__inner__rows footer__inner__list">
					<li><span class="u-arrow__text">&gt;</span><a href="/concept">キレイにスルンダとは</a></li>
					<li><span class="u-arrow__text">&gt;</span><a href="/contact">お問い合わせ</a></li>
					<li><span class="u-arrow__text">&gt;</span><a href="/about/company">運営会社</a></li>
					<li><span class="u-arrow__text">&gt;</span><a href="/about/privacypolicy">プライバシーポリシー</a></li>
					<li><span class="u-arrow__text">&gt;</span><a href="/account/">ログイン/新規登録</a></li>
				</ul>
			</li>
			<li class="footer__social__block"><a target="_blank" data-href="{{ $page_shared or '' }}" href="https://www.facebook.com/sharer/sharer.php?u={{ $page_shared or '' }}&amp;src=sdkpreparse" class="c-button-square--social c-facebook"><i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i><span class="u-fs--l">シェア</span></a><a href="https://twitter.com/intent/tweet?url={{ $page_shared or ''}}&amp;text={{ $title_twitter or env('TITLE_TWITTER') }}" class="c-button-square--social c-twitter u-mt10"><i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i><span class="u-fs--l">ツイート</span></a>
				<p class="_social__block__text u-mt20">&copy;2016 Wiz Co.,Ltd.</p>
			</li>
		</ul>
	</div>
</footer>