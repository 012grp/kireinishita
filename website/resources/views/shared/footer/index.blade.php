<div class="bnrArea_PC">
    <div class="bnr_contentsl">
        <div class="bnr_contents_l">
            <p class="bnrArea_01">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_001.png" width="180px" alt="ママの為のお仕事相談所">
                </a>
            </p>
            <p class="bnrArea_02l">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_002.png" width="180px" alt="働くママの応援プロジェクト">
                </a>
            </p>
        </div>
        <div class="bnr_contents_r">
            <p class="bnrArea_02r">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_003.png" width="180px" alt="自分に合った働き方">
                </a>
            </p>
            <p class="bnrArea_02r">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_004.png" width="180px" alt="新着求人メルマガ">
                </a>
            </p>
            <p class="bnrArea_02r">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_005.png" width="180px" alt="新着求人メルマガ">
                </a>
            </p>
        </div>
    </div>
    <div class="bnr_contentsr">
        <p class="bnrArea_03">
            <a href="/account/bookmark" class="btn-push"><img src="/public/images/btn_bnr_000.png" width="180px" alt="お気に入りBOX">
            </a>
        </p>
    </div>
    <div class="clearfix"></div>
    <div class="footer_mainbox"><span class="footer_text01"><a href="/">求人情報掲載について︎</a></span><span class="footer_text02"><a href="/">会社運営︎</a></span><span class="footer_text02"><a href="/">プライバシーポリシー</a></span><span class="footer_text02"><a href="/#">お問い合わせ</a></span><span class="footer_text03">Copyright © 2015 Mama no Kyujin Ink.All Rights Reserved</span>
    </div>
</div>

<div class="bnrArea_TAB">
    <div class="bnr_contentsl">
        <div class="bnr_contents_l">
            <p class="bnrArea_01">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_001.png" width="150px" alt="ママの為のお仕事相談所">
                </a>
            </p>
            <p class="bnrArea_02l">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_002.png" width="150px" alt="働くママの応援プロジェクト">
                </a>
            </p>
        </div>
        <div class="bnr_contents_r">
            <p class="bnrArea_02r">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_003.png" width="150px" alt="自分に合った働き方">
                </a>
            </p>
            <p class="bnrArea_02r">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_004.png" width="150px" alt="新着求人メルマガ">
                </a>
            </p>
            <p class="bnrArea_02r">
                <a href="/" class="btn-push"><img src="/public/images/btn_bnr_005.png" width="150px" alt="新着求人メルマガ">
                </a>
            </p>
        </div>
    </div>
    <div class="bnr_contentsr">
        <p class="bnrArea_03">
            <a href="/account/bookmark" class="btn-push"><img src="/public/images/btn_bnr_000.png" width="150px" alt="お気に入りBOX">
            </a>
        </p>
    </div>
    <div class="clearfix"></div>
    <div class="footer_mainbox"><span class="footer_text01"><a href="/">求人情報掲載について︎</a></span><span class="footer_text02"><a href="/">会社運営︎</a></span><span class="footer_text02"><a href="/">プライバシーポリシー</a></span><span class="footer_text02"><a href="/">お問い合わせ</a></span><span class="footer_text03">Copyright © 2015 Mama no Kyujin Ink.All Rights Reserved</span>
    </div>
</div>

<div class="bnrArea_SP">
    <span class="footer_text04">Copyright © 2015 Mama no Kyujin Ink.<br />All Rights Reserved</span>
</div>