<?php $articles = _modules::load_article_banner();?>
<ul class="topImg-box">
	@foreach($articles as $article)
		<?php
		if(!empty($article['image_thumb'])){
			$image = '/public/uploads/article/'.$article['image_thumb'];
		}else{
			if(!isset($article['image']) || empty($article['image']))
			{
				$image = '/public/img/topimg/test_img.jpg';
			}else{
				$image = '/public/uploads/article/'.$article['image'];
			}

		}
		?>
		<li style="background-image:url({{ $image }})" class="topImg-box__list column-box__contents">

			<a href="/contents/{{ $article['category_alias'] }}/{{ $article['id'] }}" class="topImg-box__link column-box__contents__topImg">
				<p class="topImg-box__list__inner">
					<span class="topImg-box__list__text u-fs--xlh">{{ $article['title'] }}</span>
				</p>
				<span class="c-tag--{{ $article['class_name'] }}--l--contents u-fs--xs u-fwb">{{ $article['category'] }}</span>
			</a>
		</li>
	@endforeach
</ul>