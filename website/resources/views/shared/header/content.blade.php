<div class="header_left">
    <div class="header_left01">
        <a href="/"><img src="/public/images/mama_logo_s01.png" class="img" alt="ママの求人">
        </a>
    </div>
    <div class="header_left02">
        <div class="header_left02l">
            <p class="header_textLeft01">検索
            @if(isset($request['keyword']))
                 <span class="BGcolor" id="mainKeyword">{{ $request['keyword'] }}</span>
            @endif
            </p>
        </div>
        <div class="header_left03r">
           @if(isset($contents))
                <p class="header_textRight">検索結果
                    <?php $chars=str_split($contents->total()); ?>
                    <?php $count=count($chars); ?> 
                    @foreach($chars as $i=>$char)
                    <span class="count_No">{{ $char }}</span> 
                    @if(($count-$i-1)%3==0 && $i< $count-1) , 
                    @endif 
                    @endforeach 

                    件
                </p>
            @endif
        </div>
        <div class="header_left02r">
            <p class="header_textLeft02">and</p>
            <div class="search_boxArea_pc">
                <form>
                    <input type="search" id="txtKeyword" class="serach_text" placeholder="追加ワード"
                    value="{{ $request['subkeyword'] or '' }}"
                    >
                    <input type="image" id="btnFilter" value="検索" src="/public/images/btn_search_001.png">
                </form>
            </div>
            <div class="search_boxArea_smt">
                <a href="index_form_sp.html"><img src="/public/images/smt_btn.png">
                </a>
            </div>
        </div>
    </div>
</div>