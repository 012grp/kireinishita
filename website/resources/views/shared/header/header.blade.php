<?php
	$path = Request::path();

	$user = _helper::get_info();
	//Array: 勤務地
	$province_list 		= _helper::getProvinceNames();
	$employment_list 	= _helper::getEmployment();
?>
<header class="header--primary">
	<div class="header--primary__inner u-cf">
		<div data-trigger-quicksearch="quicksearch" class="header--primary__search"><i
					class="c-icon--search"></i><span class="-primary__search__text u-fs--xxxs">SEARCH</span></div>
		<h2 class="header--primary__logo"><a href="/"><img src="/public/img/svg/top-logo.svg" alt="キレイにスルンダ"></a>
		</h2>
		<div class="header--primary__right">
			@if($user)
				@if($path == 'mybox')
					<a href="/account/logout" class="c-button-square--s"><span class="u-fs--l u-fwb">ログアウト</span></a>
				@else
					<a href="/mybox" class="c-button-square--s"><span class="u-fs--l u-fwb">お気に入り BOX</span></a>
				@endif
			@else
				<a href="/account/" class="c-button-square--s"><span class="u-fs--l u-fwb">ログイン</span></a>
			@endif
			<div data-trigger-hamburgermenu="hamburger" class="nav--hamburger"><span
						class="nav--hamburger__list"></span><span class="nav--hamburger__list"></span><span
						class="nav--hamburger__list"></span><span
						class="nav--hamburger__text u-fs--xxxs">MENU</span></div>
		</div>
		<form data-target-quicksearch="quicksearch" class="quick-search quick-search--inHeader is-disable" action="/job/result">
			<div class="-search--inHeader__inner">
				<div class="quick-search__select__wrap c-icon__wrapper--triangle">
					<select name="pro" class="quick-search__select u-fs--m">
						<option value="" selected>都道府県</option>
						@foreach($province_list as $province)
							<option value="{{$province['id']}}">{{$province['name']}}</option>
						@endforeach
					</select>
				</div>
				<span class="c-icon--cross"></span>
				<div class="quick-search__select__wrap c-icon__wrapper--triangle">
					<select name="em" class="quick-search__select u-fs--m">
						@foreach($employment_list as $em)
							<option value="{{$em['id']}}">{{$em['name']}}</option>
						@endforeach
					</select>
				</div>
				<span class="c-icon--cross"></span>
				<input type="text" placeholder="キーワード" class="quick-search__input u-fs--m">
				<button type="submit" class="c-button-square--m u-fs--l"><span class="button-inner">カンタン検索</span>
				</button>
			</div>
		</form>
		<ul data-target-hamburgermenu="hamburger" class="nav-box u-fs--xs nav-box--inHeader is-disable">
			@include('shared.item.box_list_menu')
		</ul>
	</div>
</header>