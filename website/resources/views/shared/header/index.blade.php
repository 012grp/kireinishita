<div class="header_left">
    <a href="/"><img src="/public/images/mama_logo_s01.png" class="img" alt="ママの求人">
    </a>
    <p class="header_textLeft">ママの求人は、ママの求人・転職まとめメディアです。
        <br />アルバイト・パート・派遣など、ママを募集している求人情報が満載！</p>
</div>
<div class="header_right">
    <p class="header_textRight">ママの求人数
                <?php $chars=str_split($total); ?>
                <?php $count=count($chars); ?> 
                @foreach($chars as $i=>$char)
                <span class="count_No">{{ $char }}</span> 
                @if(($count-$i-1)%3==0 && $i< $count-1) , 
                @endif 
                @endforeach 
    件（毎日更新）</p>
</div>