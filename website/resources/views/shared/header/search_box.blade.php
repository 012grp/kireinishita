<?php
$province_list = _helper::getProvinceNames();
$employment_list = _helper::getEmployment();
?>
<div class="search-box">
    <div class="topSearch-box__inner">
        <h1 class="topSearch-box__inner__text u-fs--xlh u-fwb">美容業界の求人を<br>まとめカンタン検索！</h1>
        <form class="quick-search" action="/job/result">
            <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                <select name="pro" class="quick-search__select u-fs--m">
                    <option value="" selected>都道府県</option>
                    @foreach($province_list as $province)
                        <option value="{{$province['id']}}">{{$province['name']}}</option>
                    @endforeach
                </select>
            </div>
            <span class="c-icon--cross"></span>
            <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                <select name="em" class="quick-search__select u-fs--m">
                    <option value="">雇用形態</option>
                    @foreach($employment_list as $em)
                        <option value="{{$em['id']}}">{{$em['name']}}</option>
                    @endforeach
                </select>
            </div>
            <span class="c-icon--cross"></span>
            <input type="text" placeholder="キーワード" class="quick-search__input u-fs--m" name="k">
            <button type="submit" class="c-button-square--m u-fs--l"><span class="button-inner">カンタン検索</span></button>
        </form>
    </div>
</div>