<?php
$allianceUrl = _helper::getUrlName('alliancePage');

?>

@if($alliances)
<div class="column-box__secret--related u-mt30">
	<h2 class="_secret__related__heading">{{$title or ''}}</h2>
	<ul class="_secret__related__content u-mt30">
		@foreach($alliances as $alliance)
			@include('shared.item.box_alliance_related', 
                    array(
                         'allianceUrl' => $allianceUrl, 
                         'alliance' => $alliance
                    ))
		@endforeach
	</ul>
</div>
@endif