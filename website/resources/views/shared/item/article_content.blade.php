<div data-item="article_content" class="column-box--rightContents u-mt30">
  <h2 class="column-box__ttl u-fs--l u-fwb">
    <i class="column-box__ttl__icon {{ $class_icon_tile or '' }}"></i> {{ $name or '' }}
  </h2>

  @foreach($contents as $content) 
      @include('shared.item.box_article_content', array(
        'icon_title'          => $icon_title,
        'icon_class'          => $icon_class,
        'image'               => $content['image'],
        'image_thumb'         => $content['image_thumb'],
        'title'               => $content['title'],
        'id'                  => $content['id'],
        'category'            => $content['category'],
        'view'                => $content['view'],
        'category_alias'      => $content['category_alias'],
        'created_at'          => $content['created_at'],
        'class_name'          => $content['class_name'],
      ))
  @endforeach 

</div>