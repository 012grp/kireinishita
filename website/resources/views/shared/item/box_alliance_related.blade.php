<?php
	$job_category = _helper::getAllianceJobCategoryNameByListId($alliance['list_job']);
?>
<li class="_secret__related__item"><a href="/{{$allianceUrl}}/{{$alliance['id']}}" class="_secret__related__item__target"><img width="200" height="152" src="{{$alliance['job_image']}}" class="_secret__related__item__img"></a>
	<p data-target-textcount="3" class="_secret__related__item__description u-mt15">
		<a href="/{{$allianceUrl}}/{{$alliance['id']}}">
		{{$alliance['job_name']}}
		</a>
	</p>
	<ul class="_secret__related__item__detail">
		<li class="_secret__related__item__detail__content u-mt10"><strong>職種 :</strong>{{$job_category}}</li>
		<li class="_secret__related__item__detail__content u-mt10"><strong>給与 :</strong>{{$alliance['job_salary']}}</li>
		<li class="_secret__related__item__detail__content u-mt10"><strong>勤務地 :</strong>{{$job_location}}</li>
	</ul>
</li>
