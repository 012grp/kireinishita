<?php
if(!empty($image_thumb)){
	$image_show = '/public/uploads/article/'.$image_thumb;
}else{

	if(!isset($image) || empty($image))
	{
		$image_show = '/public/img/media/media-m01.jpg';
	}else{
		$image_show = '/public/uploads/article/thumbs/280_fw_'.$image;
	}
}
?>
<div class="column-box--rightContents__list">
	<a href="/contents/{{ $category_alias }}/{{ $id }}">
		<div data-normal="bg" data-layzr-bg="{!! $image_show !!}" class="-rightContents__list__img">
			@if($icon_title == 'NEW' && _helper::checkNew($created_at, 7) || $icon_title != 'NEW')
				<span class="{!! $icon_class !!}"><span class="u-fwb">{!! $icon_title !!}</span></span>
			@endif
		</div>
		<div class="-rightContents__list__right">
			<p class="-rightContents__list__text u-fs--sh">{!! $title !!}</p>
			<span class="_list__right__tag c-tag--{!! $class_name or '' !!} u-fs--xxxs u-fwb">{!! $category !!}</span>
			<span class="c-icon--eye"></span><span class="u-fs--xxs">{!! $view !!}</span>
		</div>
	</a>
</div>