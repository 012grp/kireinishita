<h3 class="nav-box--category__ttl u-fs--xl u-fwb">
	<i class="c-icon--circle--text__ttl"></i>{{ $name }}
</h3>
<ul data-item="box_contain_category" class="nav-box--category u-fs--xs">
	@foreach($categories as $cat)
		@if($cat['category_alias'] == $active)
			<li class="nav-box__list is-selected">
				<p class="nav-box__list--selected">
					<span class="nav-box__list__icon">
						<i class="c-icon--{{ $cat['class_name'] }}"></i>
					</span>
					<span>{{ $cat['name'] }}</span>
					<span class="nav-box__list__arrow u-fs--xs">&gt;</span>
				</p>
			</li>
		@else
			<li class="nav-box__list">
				<a href="/contents/{{ $cat['category_alias'] }}" class="nav-box__list__link">
					<span class="nav-box__list__icon"><i class="c-icon--{{ $cat['class_name'] }}"></i></span>
					<span>{{ $cat['name'] }}</span><span class="nav-box__list__arrow u-fs--xs">&gt;</span>
				</a>
			</li>
		@endif
		
	@endforeach
</ul>