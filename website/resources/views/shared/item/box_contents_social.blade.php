<div class="_contents__social__block{{ $class }}">
	<a target="_blank" data-href="{{ $request_url }}" href="https://www.facebook.com/sharer/sharer.php?u={{ $request_url }}&amp;src=sdkpreparse" class="c-button-square--social c-facebook">
		<i class="c-font-icon--facebook _social__block__icon u-fs--xxl"></i>
		<span class="u-fs--l">シェア</span>
	</a>
	<a href="https://twitter.com/intent/tweet?url={{ $request_url }}&amp;text={{ $title }}" class="c-button-square--social c-twitter">
		<i class="c-font-icon--twitter _social__block__icon u-fs--xxl"></i>
		<span class="u-fs--l">ツイート</span>
	</a>
	<a target="_blank" href="http://b.hatena.ne.jp/entry/{{ $request_url }}" class="c-button-square--social c-hatena">
		<i class="c-font-icon--hatebu _social__block__icon u-fs--xxl"></i>
		<span class="u-fs--l">ブックマーク</span>
	</a>
	<div class="_social__block__right">
		<span class="c-icon--eye--l"></span>
		<span class="u-fs--l">{{ $view }}</span>
	</div>
</div>