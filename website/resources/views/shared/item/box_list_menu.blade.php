<?php
	$secretUrl = _helper::getUrlName('secretPage');
	$url = strtolower(Request::path());
	if($url != '/')
	{
		$url = '/'.$url.'/';
	}

	$arrCategory = _helper::get_list_menu();

	$list_menu = array(
		0 => array(
				'name' 		 => 'HOME',
				'class_name' => 'home',
				'url' 		 => '/'
			),
		1 => array(
				'name' => 'オリジナル求人',
				'class_name'	=> 'original',
				'url'	=> '/'. $secretUrl .'/'
			),
		2 => array(
				'name' => 'コンテンツまとめ',
				'class_name'	=> 'content',
				'url'	=> '/contents/',
				'menu'	=> $arrCategory

			)
	);

	$contents = '/contents/';
?>
@foreach($list_menu as $item)
	@if((strpos($url, $item['url']) === 0 && $item['url'] != '/') || ($url == $item['url']))
		<li class="nav-box__list is-selected">
			<a href="{{ $item['url'] }}" class="nav-box__list--selected">
				<span class="nav-box__list__icon"><i class="c-icon--{{ $item['class_name'] }}"></i></span>
				<span>{{ $item['name'] }}</span>
			</a>
		</li>
	@else
		<li class="nav-box__list">
			<a href="{{ $item['url'] }}" class="nav-box__list__link">
				<span class="nav-box__list__icon"><i class="c-icon--{{ $item['class_name'] }}"></i></span>
				<span>{{ $item['name'] }}</span>
			</a>
		</li>
	@endif

	@if(isset($item['menu']))
		@foreach($item['menu'] as $sub_menu)
			@if(strpos($url, $sub_menu['url']) === 0)
				<li class="nav-box__list is-selected">
					<a href="{{ $sub_menu['url'] }}" class="nav-box__list--selected">
						{!! $sub_menu['i_tag'] !!}
						<span class="nav-box__list__icon"><i class="c-icon--{{ $sub_menu['class_name'] }}"></i></span>
						<span>{{ $sub_menu['name'] }}</span>
					</a>
				</li>
			@else
				<li class="nav-box__list">
					<a href="{{ $sub_menu['url'] }}" class="nav-box__list__link">
						{!! $sub_menu['i_tag'] !!}
						<span class="nav-box__list__icon"><i class="c-icon--{{ $sub_menu['class_name'] }}"></i></span>
						<span>{{ $sub_menu['name'] }}</span>
					</a>
				</li>
			@endif
		@endforeach
	@endif
@endforeach
<?php 
/*
<li class="nav-box__list">
	<a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span class="nav-box__list__icon"><i class="c-icon--hat"></i></span><span>集客の知恵袋</span></a>
</li>
<li class="nav-box__list">
	<a href="#" class="nav-box__list__link"><i class="c-icon--l"></i><span class="nav-box__list__icon"><i class="c-icon--star-other"></i></span><span>スタア発掘</span></a>
</li>

*/
 ?>