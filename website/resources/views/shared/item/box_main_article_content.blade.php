<?php
if(!empty($image_thumb)){
	$image_show = '/public/uploads/article/'.$image_thumb;
}else{

	if(!isset($image) || empty($image))
	{
		$image_show = '/public/img/media/media-m01.jpg';
	}else{
		$image_show = '/public/uploads/article/thumbs/280_fw_'.$image;
	}
}
?>

<a href="/contents/{{ $category_alias }}/{{ $id }}">
	<div style="background-image:url({{ $image_show }})" class="-new__list__img">
		@if(_helper::checkNew($created_at, 7))
			<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
		@endif
	</div>
	<div class="-new__list__right">
		<p class="-new__list__text u-fs--xxlh--contents u-fwb">
			{{ $title or '' }}
		</p>
		<div class="_list__right__bottom">
			<span class="_list__right__tag c-tag--{{ $class_name or '' }}--l u-fs--xs u-fwb">{{ $category or '' }}</span>
			<div class="_right__bottom__num">
				<span class="c-icon--eye--l"></span><span class="u-fs--l">{{ $view or '' }}</span>
			</div>
		</div>
	</div>
</a>