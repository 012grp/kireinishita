<?php
if(!empty($image_thumb)){
	$image_other = '/public/uploads/article/'.$image_thumb;
}else{

	if(!isset($image) || empty($image))
	{
		$image_other = '/public/img/media/media-m01.jpg';
	}else{
		$image_other = '/public/uploads/article/thumbs/280_fw_'.$image;
	}
}
?>
<a href="/contents/{{ $category_alias }}/{{ $id }}">
	<div data-normal="bg" data-layzr-bg="{{ $image_other}}" class="-normal__list__img">
		@if(_helper::checkNew($created_at, 7))
		<span class="c-icon--circle--m"><span class="u-fwb u-fs--xxs">NEW</span></span>
		@endif
	</div>
	<div class="-normal__list__right">
		<p class="-normal__list__text u-fs--xxlh--contents u-fwb">
			{{ $title }}
		</p>
		<div class="_list__right__bottom">
			<span class="_list__right__tag c-tag--{{ $class_name }}--l u-fs--xs u-fwb">{{ $category }}
									</span>
			<div class="_right__bottom__num">
				<span class="c-icon--eye--l"></span>
				<span class="u-fs--l">{{ $view }}</span>
			</div>
		</div>
	</div>
</a>