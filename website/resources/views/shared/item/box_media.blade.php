<?php
$job_career 	= strip_tags($job_career);
$job_name 		= _common::getJobName($job_name);
$job_salary 	= strip_tags($job_salary);
$job_location 	= strip_tags($job_location);
$job_url 		= strip_tags($job_url);

//TODO: match salary
$pattern = env('JOB_SALARY_REGEX', 0);
if($pattern)
{
    $matches = [];
    preg_match_all($pattern, $job_salary, $matches);
    if($matches)
    {
        $job_salary = implode($matches[0], '／');
    }
}

//TODO: set salary length to show
//if(mb_strlen($job_salary) > 7)
//{
//	$job_salary = mb_substr($job_salary,0,7,'utf-8').'...';
//}

//TODO: set name length to show
if(mb_strlen($job_name) > 35)
{
	$job_name = mb_substr($job_name,0,35,'utf-8');
}

$rel = 'nofollow';
if(empty($job_target_url))
{
	$rel = '';
}

?>
@if (empty($url))
<a rel="{{$rel}}" target="{{ $job_target_url or '' }}" href="{!! $job_url !!}">
@else
<a onclick="ga('send','event','osusume list','click','{!! $url !!}');" href="{!! $job_url !!}">
@endif

	<div style="background-image:url('{!! $job_image !!}')" class="column-box--media__img">
	</div>
	<span data-target-textcount="2" class="-media__img__text u-fwb">
		{!! $job_name !!}
	</span>
	<dl class="column-box--media__detail">
		<dt>職種</dt>
		<dd>：{!! $job_career !!}</dd>
	</dl>
	<dl class="column-box--media__detail">
		<dt>給与</dt>
		<dd>：{!! $job_salary !!}</dd>
	</dl>
	<dl class="column-box--media__detail">
		<dt>勤務地</dt>
		<dd>：{!! $job_location !!}</dd>
	</dl>
</a>