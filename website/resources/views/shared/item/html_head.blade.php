<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link rel="shortcut icon" href="/public/favicon_32px.png" />
<link rel="apple-touch-icon-precomposed" href="/public/apple-touch-icon-precomposed.png" />
<title>{{ $title or 'ママの求人・転職まとめメディア' }}｜ママの求人</title>
<meta name="title" content="{{ $title or 'ママの求人・転職まとめメディア' }}｜ママの求人">
<meta name="description" content="{{ $description or 'ママ必見！育児や保育園・幼稚園の送迎と両立可能な求人集めました！パート、アルバイト、派遣社員、契約社員、社員の雇用形態から選び、子育て（乳幼児、未就学時）や専業主婦（扶養枠内での勤務）の状況に合わせて検索してください！（検索条件：残業なし（少なめ）、土日休み、完全週休二日制、育児支援、産休・育休の実績、勤務地固定（転勤なし）など）' }}">
<meta name="keywords" content="{{ $keywords or '完全週休二日制,育児支援,残業なし,ママ　求人,専業主婦　仕事,残業なし' }}">

<meta property="og:title" content="{{ $title or 'ママの求人・転職まとめメディア' }}｜ママの求人">
<meta property="og:type" content="website">
<meta property="og:description" content="{{ $description or 'ママ必見！育児や保育園・幼稚園の送迎と両立可能な求人集めました！パート、アルバイト、派遣社員、契約社員、社員の雇用形態から選び、子育て（乳幼児、未就学時）や専業主婦（扶養枠内での勤務）の状況に合わせて検索してください！（検索条件：残業なし（少なめ）、土日休み、完全週休二日制、育児支援、産休・育休の実績、勤務地固定（転勤なし）など）' }}">
<meta property="og:url" content="http://mama-9jin.com/">
<meta property="og:image" content="http://mama-9jin.com/ogp.png">
<meta property="og:site_name" content="{{ $title or 'ママの求人・転職まとめメディア' }}｜ママの求人">
<meta property="og:locale" content="ja_JP">
