@if($contents && $contents->count() > 0)
<div data-item="job_list" class="column-box--media u-mt30">
	<h2 class="column-box__ttl u-fs--l u-fwb">
		{{ $name or '' }}
		<span class="column-box__ttl__tag {{ $label_class or '' }} u-fs--xxs u-fwd">{{ $label_name or '' }}</span>
	</h2>
	<?php 
		$target = $target_url;
		$secretUrl = _helper::getUrlName('secretPage');
	?>
	<ul class="column-box--media__list">
		@if($style == 1)
			@foreach($contents as $content)
			<li class="column-box--media__lists u-fs--sh">
				@include('shared.item.box_media', array(
					'job_career' => $content['job_career'],
					'is_disable' => '',
					'job_name' => $content['job_name'],
					'job_salary' => $content['job_salary'],
					'job_location' => $content['job_location'],
					'job_image' => _common::getSecretImage($content['job_image']),
					'job_url' => '/'. $secretUrl .'/'.$content['id'],
					'job_target_url' => $target,
				))
			</li>
			@endforeach 
		@else
			@foreach($contents as $content)		
			<li class="column-box--media__lists u-fs--sh">
				@include('shared.item.box_media', array(
					'job_career' => $content['job_career'],
					'is_disable' => '',
					'job_name' => $content['job_name'],
					'job_salary' => $content['job_salary'],
					'job_location' => $content['job_location'],
					'job_image' => $content['job_image'],
					'job_url' => $content['job_url'],
					'job_target_url' => $target,
					'site_id' => $content['site_id'],
					'url' => $content['url'],
				))
			</li>
			@endforeach 
		@endif
	</ul>
</div>
@endif