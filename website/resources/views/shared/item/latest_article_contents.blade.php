{{-- top 5 latest article content --}}
<?php $art = _modules::get_latest_contents(); ?>
@include('shared.item.article_content', 
            array(
                'class_icon_tile' => 'c-icon--star',
                'name' => '新着コンテンツ',
                'icon_title' => 'NEW',
                'icon_class' => 'c-icon--circle--s',
                'contents' => $art
            )
        )