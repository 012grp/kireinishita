<div data-item="main_article_contain" data-target-leftcolumn="target" class="column-left">
  <div class="column-box--new">
    <ul class="column-box--new__list">
      @foreach($contents as $content)

        <li class="column-box--new__lists">
          @include('shared.item.box_main_article_content', array(
            'id'             => $content['id'],
            'image'          => $content['image'],
            'image_thumb'    => $content['image_thumb'],
            'image_sp'       => $content['image_sp'],
            'title'          => $content['title'],
            'view'           => $content['view'],
            'category'       => $content['category'],
            'category_alias' => $content['category_alias'],
            'class_name'     => $content['class_name'],
            'created_at'     => $content['created_at']
          ))
        </li>
      @endforeach 
    </ul>
  </div>

  <div class="pager u-mt30">
    @include('pagination.default_center', ['paginator' => $contents])
  </div>

</div>