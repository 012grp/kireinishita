{{-- new article content --}}
<?php $newArticle = _modules::new_article_content(); ?>
@include('shared.item.article_content', 
        array(
            'class_icon_tile' => 'c-icon--star',
            'name' => '新着コンテンツ',
            'icon_title' => 'NEW',
            'icon_class' => 'c-icon--circle--s',
            'contents' => $newArticle
        )
    )