{{-- popular article content --}}
<?php $popularArticle = _modules::popular_article_content(); ?>
@include('shared.item.article_content', 
            array(
                'class_icon_tile' => 'c-icon--heart',
                'name' => '人気コンテンツ',
                'icon_title' => '人気',
                'icon_class' => 'c-icon--tag',
                'contents' => $popularArticle
            )
        )