<?php 
	//Array: 勤務地
	$province_list 		= _helper::getProvinceNames();
	$employment_list 	= _helper::getEmployment();
?>
<div class="column-box--right__search u-mt30">
<form action="/job/result" method="get">
  <div class="quick-search__select__wrap c-icon__wrapper--triangle">
    <select name="pro" class="quick-search__select u-fs--m">
      <option value="" selected>都道府県</option>
      @foreach($province_list as $province)
		<option value="{{$province['id']}}">{{$province['name']}}</option>
	  @endforeach 
    </select>
  </div><span class="c-icon--cross"></span>
  <div class="quick-search__select__wrap c-icon__wrapper--triangle">
    <select name="em" class="quick-search__select u-fs--m">
      <option value="" selected>雇用形態</option>
      @foreach($employment_list as $em)
      <option value="{{$em['id']}}">{{$em['name']}}</option>
      @endforeach 
    </select>
  </div><span class="c-icon--cross"></span>
  <input name="k" type="text" placeholder="キーワード" class="quick-search__input u-fs--m">
  <button type="submit" class="c-button-square--full u-fs--l u-mt20"><span class="button-inner">カンタン検索</span></button>
</form>
</div>