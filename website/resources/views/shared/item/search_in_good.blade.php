<?php 
	$jobCategoryLevel1 = _helper::getJobCategoryLevel1GroupByParentId();
	//応募条件
	$quatifications 	= $jobCategoryLevel1[50];
	//待遇
	$treatment 			= $jobCategoryLevel1[51];
	//特徴
	$characteristic 	= $jobCategoryLevel1[52];

	//$prefix_link = '/job/result/';
	$prefix_link = '/feature/';

	//応募条件: /job/result?hang%5B%5D=9&hang%5B%5D=10&hang%5B%5D=11&hang%5B%5D=12
	//待遇: /job/result?hang%5B%5D=13&hang%5B%5D=14&hang%5B%5D=15&hang%5B%5D=16&hang%5B%5D=17&hang%5B%5D=18&hang%5B%5D=20&hang%5B%5D=89
	//特徴: /job/result?hang%5B%5D=88&hang%5B%5D=21&hang%5B%5D=22&hang%5B%5D=23&hang%5B%5D=24&hang%5B%5D=25&hang%5B%5D=26&hang%5B%5D=27&hang%5B%5D=28
?>

<div data-item="search_in_good" class="column-box--search u-fs--l u-mt30">
	<h2 class="column-box__ttl u-fwb">こだわりで検索</h2>
	<dl class="column-box__list column-box__list--first">
		<dt class="column-box__list__ttl"><a href="/feature/qualifications">応募条件</a></dt>
		<dd class="column-box__list__right">
			<ul>
				@foreach($quatifications as $record)
				<li><a href="{{$prefix_link}}{{$record['alias']}}"><i class="c-icon--triangle"></i>{{$record['name']}}</a></li>
				@endforeach 
			</ul>
		</dd>
	</dl>
	<dl class="column-box__list column-box__list--second">
		<dt class="column-box__list__ttl"><a href="/feature/treatment">待遇</a></dt>
		<dd class="column-box__list__right">
			<ul>
				@foreach($treatment as $record)
				<li><a href="{{$prefix_link}}{{$record['alias']}}"><i class="c-icon--triangle"></i>{{$record['name']}}</a></li>
				@endforeach 
			</ul>
		</dd>
	</dl>
	<dl class="column-box__list column-box__list--third">
		<dt class="column-box__list__ttl"><a href="/feature/characteristic">特徴</a></dt>
		<dd class="column-box__list__right">
			<ul>
				@foreach($characteristic as $record)
				<li><a href="{{$prefix_link}}{{$record['alias']}}"><i class="c-icon--triangle"></i>{{$record['name']}}</a></li>
				@endforeach 
			</ul>
		</dd>
	</dl>
</div>