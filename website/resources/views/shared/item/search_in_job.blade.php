<?php 
	$jobCategoryLevel1 = _helper::getJobCategoryLevel1GroupByParentId();
	//調理
	$chouri 	= $jobCategoryLevel1[1];
	//接客・販売
	$service 	= $jobCategoryLevel1[2];
	//企画・運営
	$planning 	= $jobCategoryLevel1[3];

	//$prefix_link = '/category-alias';
	$prefix_link = '/';

	//調理: /job/result?cat%5B%5D=31&cat%5B%5D=32&cat%5B%5D=33&cat%5B%5D=34&cat%5B%5D=35&cat%5B%5D=36&cat%5B%5D=37&cat%5B%5D=38&cat%5B%5D=39&cat%5B%5D=40
	//接客・販売: /job/result?cat%5B%5D=41&cat%5B%5D=42&cat%5B%5D=43&cat%5B%5D=44&cat%5B%5D=45
	//企画・運営: /job/result?cat%5B%5D=46&cat%5B%5D=47&cat%5B%5D=48&cat%5B%5D=49
?>



<div data-item="search_in_job" class="column-box--search u-fs--l u-mt30">
	<h2 class="column-box__ttl u-fwb">職種で検索</h2>
	<dl class="column-box__list column-box__list--first">
		<dt class="column-box__list__ttl"><a href="/biyo">美容</a></dt>
		<dd class="column-box__list__right">
			<ul>
				@foreach($chouri as $record)
				<li><a href="{{$prefix_link.$record['alias']}}"><i class="c-icon--triangle"></i>{{$record['name']}}</a></li>
				@endforeach 
			</ul>
		</dd>
	</dl>
	<dl class="column-box__list column-box__list--second">
		<dt class="column-box__list__ttl"><a href="/relaxation">リラクゼーション</a></dt>
		<dd class="column-box__list__right">
			<ul>
				@foreach($service as $record)
				<li><a href="{{$prefix_link.$record['alias']}}"><i class="c-icon--triangle"></i>{{$record['name']}}</a></li>
				@endforeach 
			</ul>
		</dd>
	</dl>
	<dl class="column-box__list column-box__list--third">
		<dt class="column-box__list__ttl"><a href="/chiryo">治療</a></dt>
		<dd class="column-box__list__right">
			<ul>
				@foreach($planning as $record)
				<li><a href="{{$prefix_link.$record['alias']}}"><i class="c-icon--triangle"></i>{{$record['name']}}</a></li>
				@endforeach 
			</ul>
		</dd>
	</dl>
</div>