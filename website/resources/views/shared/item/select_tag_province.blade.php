<?php
	if(!isset($province_list))
	{
		$province_list 	= _helper::getProvinceNames();
	}
	if(!isset($requestPro))
	{
		$requestPro = 0;
	}
?>
<select name="pro" id="{{ $pro_select_id or '' }}" class="quick-search__select u-fs--m" data-trigger-location="trigger">
	<option value="" >都道府県</option>
	@foreach($province_list as $province)
		@if ($province['id']==$requestPro)
			<option value="{{$province['id']}}" selected>{{$province['name']}}</option>
		@else
			<option value="{{$province['id']}}">{{$province['name']}}</option>
		@endif
	@endforeach 
</select>