            <div class="account_area">
                <div class="logo_area">
                    <img src="/public/images/mama_logo_white.png" alt="ママの求人" width="175px">
                </div>
                
                @if (isset($user))
                <div class="login_user">
                    <a href="/account/me" title="Go to my profile">{{ $user->name }}</a>
                    <div class="logout">
                        <a href="/account/logout">Log out</a>
                    </div>
                </div>
                @else
                <div class="login_area">
                    <a href="/account/login"><img src="/public/images/login_btn_sp.png" width="200px" height="30px">
                    </a>
                </div>
                <div class="newaccount_area">
                    <a href="/account/login"><img src="/public/images/login_newaccount_btn_sp.png" width="200px" height="30px">
                    </a>
                </div>
                @endif
                
            </div>
            <ul>
                <li class="title">MENU</li>
                <li><a href="/">| ママの求人とは？</a>
                </li>
                <li><a href="/account/bookmark">| お気に入りBOX</a>
                </li>
                <li class="title">トピックス</li>
                <li><a href="/">▶︎ ママのお仕事相談所</a>
                </li>
                <li><a href="/">▶︎ 働くママの応援プロジェクト</a>
                </li>
                <li><a href="/">▶︎ 再就職支援ガイド</a>
                </li>
                <li><a href="/">▶︎ 時短勤務特集</a>
                </li>
                <li><a href="/">▶︎ 新着求人メルマガ</a>
                </li>
                <li class="title">企業情報</li>
                <li><a href="/">▶︎ 求人情報掲載について</a>
                </li>
                <li><a href="/">▶︎ 運営会社</a>
                </li>
                <li><a href="/">▶︎ プライバシポリシー</a>
                </li>
                <li><a href="/">▶︎ お問い合わせ</a>
                </li>
            </ul>
