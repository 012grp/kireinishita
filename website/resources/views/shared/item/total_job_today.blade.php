<?php $countJobToday = _modules::total_job_today(); ?>

<div class="column-box--rightTop">
	<div class="column-box__ttl u-fs--s u-fwb">〈今日のキレイにスルンダ!〉</div>
	<div class="column-box--rightTop__num">
		<span class="-rightTop__num__subText u-fs--l">求人数</span>
		<span data-target-numcheck="6" class="-rightTop__num__text u-fs--xxxl u-fwb">
		{{ number_format($countJobToday['total'])  }}</span><span class="-rightTop__num__subText u-fs--l">件</span>
	</div>
	<div class="c-icon--circle--baloon">
		<i class="c-icon--triangle--rotate"></i><span class="-rightTop__new__subText u-fs--l">新着</span>
		<p data-target-numcheck="3" class="-rightTop__new__text u-fs--xl u-fwb">
			{{ number_format($countJobToday['new']) }}<span class="u-fs--xxs">件</span>
		</p>
	</div>
</div>