<?php
	$secretUrl = _helper::getUrlName('secretPage');
?>
@if(Request::is('job/search')||Request::is( $secretUrl . '/*'))
<footer class="search--footer">
@else
<footer class="search--footer">
@endif
    <div class="footer__inner"><span class="u-fs--xs">&copy;<?php echo date('Y')?> Wiz Co.,Ltd.</span></div>
</footer>
<div data-target-close="target" class="overlay"></div>