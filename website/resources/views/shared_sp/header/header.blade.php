<header class="header--primary">
    <div class="header--primary__inner u-cf">
        @if (Request::path() != 'job/search')
        <div data-trigger-quicksearch="quicksearch" class="header--primary__search"><i
                    class="c-icon--search"></i><span class="-primary__search__text">SEARCH</span></div>
        @endif
        @if (Request::path() != '/')
            <h2 class="header--primary__logo">
            	<a href="/"><img src="/public/sp/img//svg/top-logo.svg" alt="キレイにスルンダ"></a>
            </h2>
        @endif
        <div class="header--primary__right">
            <div data-trigger-hamburgermenu="hamburger" class="nav--hamburger"><span
                        class="nav--hamburger__list"></span><span class="nav--hamburger__list"></span><span
                        class="nav--hamburger__list"></span><span class="nav--hamburger__text">MENU</span></div>
        </div>
    </div>
</header>