<?php
	$secretUrl = _helper::getUrlName('secretPage');
    $user = _helper::get_info();
	$arrCategory = _helper::get_list_menu();
	$path = Request::path();
	$list_menu_top = array(
		0 => array(
				'name' 		 => 'HOME',
				'class_name' => 'home',
				'url' 		 => '/'
			),
		1 => array(
				'name' => 'オリジナル求人',
				'class_name'	=> 'original',
				'url'	=> '/'. $secretUrl .'/'
			),
		2 => array(
				'name' => 'コンテンツ一覧',
				'class_name'	=> 'content',
				'url'	=> '/contents/'
			)
		);

	
	$list_menu_bottom = array(
		0 => array(
				'name' 		 => 'キレイにスルンダとは',
				'class_name' => 'triangle',
				'url' 		 => '/concept'
			),
		1 => array(
				'name' 		 => 'お問い合わせ',
				'class_name' => 'triangle',
				'url' 		 => '/contact'
			),
		2 => array(
				'name' 		 => '運営会社',
				'class_name' => 'triangle',
				'url' 		 => '/about/company'
			),
		3 => array(
				'name' 		 => 'プライバシーポリシー',
				'class_name' => 'triangle',
				'url' 		 => '/about/privacypolicy'
			),
//		4 => array(
//				'name' 		 => '求人掲載について',
//				'class_name' => 'triangle',
//				'url' 		 => '/about/advertising'
//			),
	);

	
?>
<ul data-target-hamburgermenu="hamburger" class="nav-box u-fs--xs">
	<li class="nav-box__list--top" id="naxBoxTop">
		@if ($user)
			@if($path == 'mybox')
			<div class="c-button-square--s" id="btnLogout">ログアウト</div>
			@else
			<div class="c-button-square--s"><a href="/mybox">お気に入り BOX</a></div>
			@endif
		@else
		<div data-modal-trigger="login" class="c-button-square--s" id="btnAuthen"><span>ログイン</span></div>
		@endif
		
		<div data-target-close="target" class="_list--top__r"><i class="c-icon--cross"></i><span class="u-fs--xxxs">MENU</span></div>
	</li>
	
	@foreach($list_menu_top as $menu)
	<li class="nav-box__list u-fs--s u-fwb">
		<a href="{{ $menu['url'] }}" class="nav-box__list__link">
            <span class="nav-box__list__icon"><i class="c-icon--{{ $menu['class_name'] }}"></i></span><span>{{ $menu['name'] }}</span>
        </a>
	</li>
	@endforeach 

	@foreach($arrCategory as $menu)
	<li class="nav-box__list u-fs--xs u-fwb">
                   <a href="{{ $menu['url'] }}" class="nav-box__list__link nav-box__list__back">
                   	   <i class="c-icon--l"></i>
                       <span class="nav-box__list__icon">
                       <i class="c-icon--{{ $menu['class_name_sp'] }}"></i></span>
                       <span>{{ $menu['name'] }}</span>
                       <i class="c-icon--triangle"></i>
                   </a>
    </li>
	@endforeach
	<?php /*
	<li class="nav-box__list u-fs--xs u-fwb"><a href="#" class="nav-box__list__link nav-box__list__back"><i class="c-icon--l"></i><span class="nav-box__list__icon"><i class="c-icon--hat"></i></span><span>集客の知恵袋</span><i class="c-icon--triangle"></i></a></li>
	<li class="nav-box__list u-fs--xs u-fwb"><a href="#" class="nav-box__list__link nav-box__list__back"><i class="c-icon--l"></i><span class="nav-box__list__icon"><i class="c-icon--star"></i></span><span>スタア発掘</span><i class="c-icon--triangle"></i></a></li>
    */ ?>      
                   
    @foreach($list_menu_bottom as $menu)
    <li class="nav-box__list">
    	<a href="{{ $menu['url'] }}" class="nav-box__list__link"><i class="c-icon--{{ $menu['class_name'] }} u-mr5"></i>
    	<span>{{ $menu['name'] }}</span></a>
	</li>
    @endforeach 


</ul>