<?php
	//is-empty
	//---
	if(isset($request))
	{
		$history = $request;
	}
	else
	{
		$history = _modules::load_history_search();
	}
	
	$area	 	 	= '';
	$areaName 	 	= '';
	$em		 		= '';
	$emName			= '';
	$cat	 		= '';
	$catName	 	= '';
	$hang	 		= '';
	$hangName	 	= '';
	$fullUrl		= _common::makeFullPars($history);

	if($history)
	{
		if(isset($history['pro']))
		{
			
			$areaName = _helper::getProvinceNameById($history['pro']);
			if(isset($history['city']))
			{
				//$area = _common::makeUrlPars($history['city'], 'city');
				if (!is_array($history['city']))
				{
					$history['city'] = array($history['city']);
				}
				foreach( $history['city'] as $val)
				{
					$areaName .= ','._helper::getCityNameById($val);
					
				}
				
			}
			if(isset($history['way']))
			{
				//$area = _common::makeUrlPars($history['way'], 'way');
				if (!is_array($history['way']))
				{
					$history['way'] = array($history['way']);
				}
				foreach( $history['way'] as $val)
				{
					$areaName .= ','._helper::getWaysiteNameById($val);
					
				}
			}

			if(isset($history['station']))
			{
				//$area = _common::makeUrlPars($history['way'], 'way');
				if (!is_array($history['station']))
				{
					$history['station'] = array($history['station']);
				}
				$history['station'] = array_unique($history['station']);
				foreach( $history['station'] as $val)
				{
					$areaName .= ','._helper::getStationNameById($val);
					
				}
			}
			
//			if(empty($area))
//			{
//				$area = '?pro='.$history['pro'];
//			}
//			else
//			{
//				$area .= '&pro='.$history['pro'];
//			}
			
		}
		
		if(isset($history['em']))
		{
			//$em = _common::makeUrlPars($history['em'], 'em');
			
			if (is_array($history['em']))
			{
				$list_em = implode(',',$history['em']);
			}
			else
			{
				$list_em = $history['em'];
			}
			$emName = _helper::getEmploymentNamesByListId($list_em);
		}
		
		if(isset($history['cat']))
		{
			//$cat = _common::makeUrlPars($history['cat'], 'cat');
			if (is_array($history['cat']))
			{
				$list_job = implode(',',$history['cat']);
			}
			else
			{
				$list_job = $history['cat'];
			}
			$catName = _helper::getCategoryNamesByListId('',$list_job);
		}
		
		if(isset($history['hang']))
		{
			//$hang = _common::makeUrlPars($history['hang'], 'hang');
			
			if (is_array($history['hang']))
			{
				$list_hangup = implode(',',$history['hang']);
			}
			else
			{
				$list_hangup = $history['hang'];
			}
			$hangName = _helper::getCategoryNamesByListId('',$list_hangup);
		}
	}
?>
<ul data-target-searchmenu="search" class="nav-box--left u-fs--xs">
      <li class="nav-box__list--top">
        <div data-target-close="target" class="_list--top__r"><i class="c-icon--cross"></i><span class="u-fs--xxxs">SEARCH</span></div>
      </li>
      <li class="nav-box__list u-fs--s u-fwb"><a href="/job/search{{$fullUrl}}#box_job" class="nav-box__list__link"><span class="nav-box__text">職種</span>
      	@if (empty($catName))
      	<span data-target-history="target" class="nav-box__search__history is-empty">---</span>
      	@else
      	<span data-target-history="target" class="nav-box__search__history">{{$catName}}</span>
      	@endif
      </a></li>
      <li class="nav-box__list u-fs--s u-fwb"><a href="/job/search{{$fullUrl}}#box_area" class="nav-box__list__link"><span class="nav-box__text">勤務地</span>
      	@if (empty($areaName))
      	<span data-target-history="target" class="nav-box__search__history is-empty">---</span>
      	@else
      	<span data-target-history="target" class="nav-box__search__history">{{$areaName}}</span>
      	@endif
      </a></li>
      <li class="nav-box__list u-fs--s u-fwb"><a href="/job/search{{$fullUrl}}#box_em" class="nav-box__list__link"><span class="nav-box__text">雇用形態</span>
      	@if (empty($emName))
      	<span data-target-history="target" class="nav-box__search__history is-empty">---</span>
      	@else
      	<span data-target-history="target" class="nav-box__search__history">{{$emName}}</span>
      	@endif
      </a></li>
      <li class="nav-box__list u-fs--s u-fwb"><a href="/job/search{{$fullUrl}}#box_hangup" class="nav-box__list__link"><span class="nav-box__text">こだわり</span>
      	@if (empty($hangName))
      	<span data-target-history="target" class="nav-box__search__history is-empty">---</span>
      	@else
      	<span data-target-history="target" class="nav-box__search__history">{{$hangName}}</span>
      	@endif
      </a></li>
</ul>