<div data-remodal-id="login" class="modal">
    <i data-remodal-action="close" class="c-icon--cross" id="btnCloseLoginModal"></i>
    <div class="modal__inner u-fs--xs">
        <a href="/account/facebook-redirect" class="c-button-square--social c-button-square--social--wide c-facebook">
            <span class="u-fs--xs">Facebookでログイン</span></a>
        <a href="/account/twitter-redirect"
           class="c-button-square--social c-button-square--social--wide c-twitter u-mt15">
            <span class="u-fs--xs">Twitterでログイン</span></a>

        <form id="formLogin">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input id="txtEmail" name="email" required type="email" placeholder="メールアドレスでログイン" autocomplete="on"
                   class="form--primary__text u-fs--l u-mt15">
            <input id="txtPassword" name="password" required type="password" placeholder="パスワード" autocomplete="on"
                   class="form--primary__text u-fs--l u-mt5">
            <p id="alertLoginMessage" data-error="required" class="form__error u-fs--xs--lh--form  u-mt15"
               style="display: none;">メールかパスワードが間違っています</p>

            <p class="form--login__repletion u-fs--xs--lh--form u-mt10 u-txtr"><a href="/account/forget-password"
                                                                                  data-modal-trigger="password">パスワードを忘れたかたはコチラ</a>
            </p>
            <button id="btnLogin" type="button" data-submit="trigger" class="c-button-square--full u-fs--xs u-mt10">
                <span class="button-inner">ログイン</span></button>
        </form>
        <a href="#" data-modal-trigger="registration" class="c-button-circle u-mt20">メールアドレスで新規登録</a>
    </div>
</div>


<!--modal分岐password再設定-->
<div data-remodal-id="password" class="modal"><i data-remodal-action="close" class="c-icon--cross"></i>
    <div class="modal__inner u-fs--xs">
        <h3 class="modal__inner__ttl u-fs--s u-fwb">パスワードの再設定</h3>
        <div class="modal__inner__text">
            登録したメールアドレスに、
            パスワードのリセット用のURLが届きます。
        </div>
        <p id="alertForgetPasswordSendMail" style="display:none;" class="u-fs--xxshl u-mt30">リセット確認メールが送信されました</p>
        <form novalidate id="formForgetPassword" target="iframeAccount">
            <p id="alertForgetPasswordMessage" class="form__error u-fs--xs--lh--form  u-mt15" style="display: none;">
                メールかパスワードが間違っています</p>
            <input id="txtUserEmail" type="email" placeholder="登録時のメールアドレス" data-mail="target"
                   data-validate-password="target" autocomplete="on" required
                   class="form--primary__text u-fs--l u-mt15">
            <div class="u-txtl">
                <p data-error="required" class="form__error u-fs--xxsh u-mt10">メールアドレスを入力して下さい</p>
                <p data-error="mail" class="form__error u-fs--xxsh u-mt10">メールアドレスの入力に誤りがあります。</p>
            </div>
            <button id="btnForgetPassword" type="button" data-submit-modalother="trigger"
                    class="c-button-square--full u-fs--xs u-mt10"><span class="button-inner">送信</span></button>
        </form>
    </div>
</div>
<!--modal分岐新規登録-->
<div data-remodal-id="registration" class="modal"><i data-remodal-action="close" id="btnCloseRegisterModal"
                                                     class="c-icon--cross"></i>
    <div class="modal__inner u-fs--xs">
        <h3 class="modal__inner__ttl u-fs--s u-fwb">メールアドレスで新規登録</h3>
        <form novalidate id="formRegister" name="formRegister">
            <input type="email" id="txtRegisterEmail" placeholder="メールアドレスでログイン" data-mail="target" autocomplete="on"
                   data-validate-register="target" required class="form--primary__text u-fs--l u-mt15">
            <div class="u-txtl">
                <p data-error="required" class="form__error u-fs--xxsh u-mt10">メールアドレスを入力して下さい</p>
                <p data-error="mail" class="form__error u-fs--xxsh u-mt10">メールアドレスの入力に誤りがあります。</p>
                <p id="alertRegisterMessage" class="form__error u-fs--xxsh u-mt10" style="display: none;"></p>
            </div>
            <input type="password" id="txtRegisterPassword" placeholder="パスワード(6桁以上)" autocomplete="on"
                   data-validate-register="target" data-maxlength="8" data-minlength="6" data-alphanumeric="target"
                   required class="form--primary__text u-fs--l u-mt5">
            <div class="u-txtl">
                <p data-error="required" class="form__error u-fs--xxshl u-mt10">パスワードを入力してください。</p>
                <p data-error="minlength" class="form__error u-fs--xxsh u-mt10">6桁以上で入力して下さい</p>
                <p data-error="maxlength" class="form__error u-fs--xxsh u-mt10">8桁以下で入力して下さい</p>
                <p data-error="alphanumeric" class="form__error u-fs--xxsh u-mt10">半角英数で入力して下さい</p>
            </div>
            <button id="btnRegister" data-submit-modal="trigger" type="button"
                    class="c-button-square--full u-fs--xs u-mt10"><span class="button-inner">新規登録</span></button>
        </form>
        <div class="modal__inner__text--notes u-fs--xxsh u-txtl u-mt15">
            ログインまたは新規登録することにより、キレイにスルンダの利用規約、プライバシーポリシーに同意したことになります。
        </div>
    </div>
</div>