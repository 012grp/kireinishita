<div class="primary-box--noPadding">
    <div class="primary-box__inner--sidepadding">
        <h1 class="primary-box__ttl u-fs--xxl u-mt15">オススメ求人</h1>

        <p class="primary-box__sub__text u-fs--xsh u-mt10">特におすすめしたい“キレイ”にまつわる限定求人情報をまとめてご紹介！</p>
    </div>
    <p class="result__num__block--gray u-mt5"><span class="result__num u-fs--xl u-fwb">{{$total or '00'}}</span><span
                class="u-fs--xs">件の求人</span></p>
</div>