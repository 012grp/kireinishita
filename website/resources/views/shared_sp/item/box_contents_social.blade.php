<ul class="contents-box--social u-mt10">
	<li class="contents-box--social__list"><a target="_blank" data-href="{{ $request_url }}" href="https://www.facebook.com/sharer/sharer.php?u={{ $request_url }}&amp;src=sdkpreparse" class="c-button-square--social c-facebook"><i
					class="c-font-icon--facebook _social__block__icon u-fs--xxxl"></i></a></li>
	<li class="contents-box--social__list"><a href="https://twitter.com/intent/tweet?url={{ $request_url }}&amp;text={{ $title }}" class="c-button-square--social c-twitter u-mt10"><i
					class="c-font-icon--twitter _social__block__icon u-fs--xxxl"></i></a></li>
	<li class="contents-box--social__list"><a href="http://line.me/R/msg/text/?{{$title}}%0D%0A{{ $request_url}}" class="c-button-square--social c-line u-mt10"><i
					class="c-font-icon--line _social__block__icon u-fs--xxxxl"></i></a></li>
	<li class="contents-box--social__list"><a target="_blank" href="http://b.hatena.ne.jp/entry/{{ $request_url }}" class="c-button-square--social c-hatena u-mt10"><i
					class="c-font-icon--hatebu _social__block__icon u-fs--xxxl"></i></a></li>
</ul>