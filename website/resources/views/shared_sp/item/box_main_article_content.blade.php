<?php
if(!empty($image_sp_thumb)){
	$image_show = '/public/uploads/article/'.$image_sp_thumb;
}else{
	if(!empty($image_sp)){
		$image_show = '/public/uploads/article/thumbs/280_fw_'.$image_sp;
	}else{
		if(!empty($image_thumb)){
			$image_show = '/public/uploads/article/'.$image_thumb;
		}else{
			$image_show = (!empty($image)) ? '/public/uploads/article/thumbs/280_fw_'.$image : _common::noImageUrl();
		}

	}

}
?>
<a href="/contents/{{ $category_alias }}/{{ $id }}">
	<div style="background-image:url({{ $image_show }})" class="-new__list__img">
		@if($icon_title == 'NEW' && _helper::checkNew($created_at, 7) || $icon_title != 'NEW')
			<span class="c-icon--{{$icon_class or ''}}"><span class="u-fwb">{{ $icon_title or '' }}</span></span>
		@endif
	</div>
	<div class="-new__list__right">
		<p class="-new__list__text u-fs--sh--contents u-fwb">
			{{ $title or '' }}</p>
		<div class="_list__right__bottom"><span class="_list__right__tag c-tag--{{ $class_name or '' }}--l u-fs--xs u-fwb">{{ $category or '' }}</span>
				<div class="_right__bottom__num"><span class="c-icon--eye"></span><span class="u-fs--xxs">{{ $view or '' }}</span></div>
		</div>
	</div>
</a>