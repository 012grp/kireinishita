<?php $arrCategory = _helper::get_list_menu(); 
	  $current_cat = isset($current_cat)?$current_cat: '';
?>
	<h3 class="nav-box--category__ttl u-fs--xs--contents u-fwb">
	<span><i class="c-icon--circle--text__ttl"></i>コンテンツカテゴリー</span>
</h3>
	<ul class="nav-box--category u-fs--s u-mt10">
		@foreach($arrCategory as $category)
		<li class="nav-box__list">
           	@if ($current_cat == $category['category_alias'])
           	<p class="nav-box__list--selected u-fwb">
           		<span>{{ $category['name'] or '' }}</span>
           		<i class="c-icon--triangle"></i>
           	</p>
           	@else
           	<a href="{{ $category['url'] or '#' }}" class="nav-box__list__link">
                   <span>{{ $category['name'] or '' }}</span>
                   <i class="c-icon--triangle"></i></a>
           	@endif
            
        </li>
		@endforeach
	</ul>