<?php $arrCategory = _helper::get_list_menu(); ?>

<nav class="nav-horizontal {{ $u_mt20 or '' }}">
	<div class="nav-horizontal__mask">
		<ul class="nav-horizontal__list u-fs--s">
			@foreach($arrCategory as $category)
				<li class="nav-horizontal__lists"><a href="{{ $category['url'] or '#' }}">{{ $category['name'] or '' }}</a></li>
			@endforeach
		</ul>
	</div>
</nav>