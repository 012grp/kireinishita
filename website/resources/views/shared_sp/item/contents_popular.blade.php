<h3 class="column-box--new__ttl u-fs--xs--contents u-fwb u-mt20"><i class="c-icon--heart"></i>人気コンテンツ</h3>
<ul class="column-box--new__list">
    @foreach($contents as $content)
        <li class="column-box--new__lists">
            @include('shared_sp.item.box_main_article_content', array(
                    'id'             => $content['id'],
                    'image'          => $content['image'],
                    'image_thumb'    => $content['image_thumb'],
                    'image_sp'       => $content['image_sp'],
                    'image_sp_thumb' => $content['image_sp_thumb'],
                    'title'          => $content['title'],
                    'view'           => $content['view'],
                    'category'       => $content['category'],
                    'category_alias' => $content['category_alias'],
                    'class_name'     => $content['class_name'],
                    'icon_title'     => '人気',
                    'icon_class'     => 'tag',
                )
            )
    </li>
    @endforeach
</ul>