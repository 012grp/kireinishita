<?php
	$image_sp = $article['image_sp'];
	$image = $article['image'];
	if(!empty($image_sp))
	{
		$image_thumb = '/public/uploads/article/'.$image_sp;

	}else{
		$image_thumb = (!empty($image)) ? '/public/uploads/article/'.$image : _common::noImageUrl();
	}
?>

<div style="background-image:url({{$image_thumb}})" class="primaryImg-box"><span class="u-fs--xs--contents c-tag--{{ $article['class_name'] }}--l--contents">{{ $article['category'] or '' }}</span><a href="/contents/{{ $article['category_alias'] }}/{{ $article['id'] }}" class="primaryImg-box__link">
<p class="primaryImg-box__list__inner"><span class="primaryImg-box__list__text u-fs--xsh">{{ $article['title'] or '' }}</span></p></a>
</div>