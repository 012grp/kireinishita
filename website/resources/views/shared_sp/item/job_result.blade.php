<?php 
if($search_result['total_data'] > 0)
{
	$query=$_GET;
	unset($query['page']);

	$currentUrl = Request::url();
	$queryString = http_build_query($query);

	if($queryString)
	{
		$currentUrl = $currentUrl . '?' . $queryString;
	}

	if (strpos($currentUrl, '?')) // returns false if '?' isn't there
	{
		$paramPage = '&page=';
	}
	else
	{
		$paramPage = '?page=';
	}


	$pageCount = $search_result['total_page'];
	$currentPage = $search_result['current_page'];


	
	$url_next = $currentPage + 1;
	$url_next = $currentUrl . $paramPage . $url_next;
	$url_prev = $currentPage - 1;
	$url_prev = $currentUrl . $paramPage . $url_prev;
}
else
{
	$pageCount = 1;
	$currentPage = 1;
	$url_next = '';
	$url_prev = '';
}
?>
<div class="column-box--gray">
	<div class="u-mt-15"></div>
	<?php
	        _helper::viewMixData($search_result,
	            //job
	            function ($data, $index, $count) {
	            	$data['index'] = $index;
	            	$data['count'] = $count;
	                echo View::make('shared_sp.item.job_result_box', $data)->render();
	            },

	            //secret
	            function ($data, $index, $count) {
	            	$data['url_target'] = '';
					$data['index'] = $index;
					$data['count'] = $count;
	                echo View::make('shared_sp.item.job_result_box', $data)->render();
	            },
	            //alliance
	            function ($data, $index, $count) {
	            	$data['index'] = $index;
	            	$data['count'] = $count;
	                echo View::make('alliance._search_item_sp', $data)->render();
	            },

	            function ($data, $index) {
	            	
	            }

	            );
	        ?>
	        
	@include('shared_sp.item.pager', array(
		'pageCount' 	=> $pageCount,
		'currentPage' 	=> $currentPage,
		'url_next' 	=> $url_next,
		'url_prev' 	=> $url_prev,
	))
</div>