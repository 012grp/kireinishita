<?php
$path = Request::path();

$job_name = _common::getJobName($job_name);

//会社
$job_company = strip_tags($job_company);

if (!empty($is_secret)) {
    $data_secret = 1;

    //職種
    $job_category = _helper::getCategoryNamesByListId($list_job_parent, $list_job);

    //雇用形態
    $job_career = _helper::getCategoryNamesByListId('', $list_hangup);

    //こだわり
    $job_type = _helper::getEmploymentNamesByListId($list_em);
} else {
    $data_secret = 0;
    $job_category = $job_career;
    $job_career = _common::getJobCareer($requirement_special);
}


//給与
$job_salary = strip_tags($job_salary);
//勤務地
$job_location = strip_tags($job_location);

$site_name = trans('job_site_name.' . $site_id);

if (empty($job_url)) {
    $secretUrl = _helper::getUrlName('secretPage');
    $job_url = '/' . $secretUrl . '/' . $id;
} else {
    $job_url = _helper::make_job_url($id);
}


//TODO: check image
if (!isset($job_image) || empty($job_image)) {
    $job_image = _common::noImageUrlContentsBySiteId($site_id);
} else {
    if (isset($job_image_sp) && !empty($job_image_sp)) {
        $job_image = $job_image_sp;
    } else {
        //$rs = strpos($job_image, 'http');
        if ($job_image[0] != '/') {
            //$job_image = '//'.$job_image;
            $job_image = _common::proxy_image($job_image);
        }
    }

}

?>


<div class="column-box--wide__list {{'u-mt15'}}">
    <div class="-wide__list__tagBlock">
        @if($data_secret)
            <div class="-wide__list__tagBlock--l"><span class="u-fs--xxs c-tag--secret--flow">オリジナル</span>
                @if(!empty($celebration))
                    <span class="u-fs--xxs c-tag--celebration">{{$celebration}}</span>
                @endif
            </div>
        @endif
        @if($is_new && $is_secret == 1)
            <div class="-wide__list__tagBlock--r"><span class="u-fs--xxs c-tag--new--flow">NEW</span></div>
        @endif
    </div>
    <h2 class="u-fs--xlh -wide__list__ttl u-fwb"><span class="_list__multiTag__text">{{ $job_name }}</span></h2>
    @if(!empty($detail['store_name']))
        <h3 class="-wide__list__subTtl u-fs--xs u-mt10 u-fwb">
        @if($is_secret==1)
        {{ $store_name }} 
        @endif
        ({{ $job_company }})</h3>
    @else
        <h3 class="-wide__list__subTtl u-fs--xs u-mt10 u-fwb">
            {{ $job_company }}
            <?php echo (!empty($store_name)) ? '(' .$store_name . ')' : '' ;  ?>
        </h3>
    @endif
    <div class="-wide__list__bottom u-mt15">
        <div style="background-image:url({{$job_image}})" class="-wide__list__img"></div>
        <ul class="-wide__list__details u-fs--sh">
            <li class="-wide__list__detail u-fs--s"><span
                        class="_list__detail__title">職種</span><span>：{{$job_category or ''}}</span>
            </li>
            <li class="-wide__list__detail"><span class="_list__detail__title">給与</span><span>：{{$job_salary}}</span>
            </li>
            <li class="-wide__list__detail--multiline"><span
                        class="_list__detail__title">勤務地</span><span>：{{$job_location}}</span>
            </li>
            <li class="-wide__list__detail u-mt5"><span
                        class="_list__detail__title">雇用形態</span><span>：{{$job_type}}</span></li>
            <li class="-wide__list__detail"><span class="_list__detail__title">こだわり</span><span>：{{$job_career}}</span>
            </li>
        </ul>
    </div>
    <div class="_list__button__block u-mt20">

        <div data-id="{{$id}}" data-secret="{{$data_secret}}" class="c-button-square--mm--gray u-fs--xs bookmark_job">
            @if($path == 'mybox')
                削除する
            @else
                <span class="c-font-icon--attachment"></span>キープする
            @endif
        </div>
        <div class="c-button-square--mm u-fs--xs">
            <a target="{{$url_target or '_blank'}}" href="{{$job_url}}">もっと詳しくみる</a>
        </div>
    </div>
    @if(!$data_secret)
        <p class="-wide__list__footer u-fs--xxs u-mt10">提供元：{{$site_name}}</p>
    @endif
</div>