<?php
//$fullUrl = str_replace('/job/result','/job/search',Request::fullUrl());
$param = (strpos(Request::fullUrl(), "?") > 0) ? explode("?", Request::fullUrl()) : "";

if (!empty($param)) {
    $ext_link = "?" . $param[1];
} else {
    $ext_link = "";
    if (!empty($request['cat'])) {
        if (is_array($request['cat'])) {
            $ext_link .= "?";
            foreach ($request['cat'] as $cat) {
                $ext_link .= "cat%5B%5D=" . $cat . '&';
            }
        } else {
            $ext_link .= "?cat=" . $request['cat'];
        }
    }
    if (!empty($request['pro'])) {
        $ext_link .= "?pro=" . $request['pro'];
    }
    if (!empty($request['city'])) {
        $ext_link .= "&city=" . $request['city'];
    }
}

$fullUrl = '/job/search' . $ext_link;
$result_list = '';
$keyword = '';
if (isset($request['k'])) {
    $keyword = $request['k'];
    $result_list .= $keyword . '/';
}

$area = '';
if (!empty($request['pro'])) {
    $area = _helper::getProvinceNameById($request['pro']);
    $result_list .= $area . '/';
};

if (!empty($request['city'])) {
    $city = '';
    if (is_array($request['city'])) {
        foreach ($request['city'] as $val) {
            $city = _helper::getCityNameById($val);
            $area .= ',' . $city;
            $result_list .= $city . '/';
        }
    } else {
        $city = _helper::getCityNameById($request['city']);
        $area .= ',' . $city;
        $result_list .= $city . '/';
    }
}

if (!empty($request['way'])) {
    $way = '';
    if (is_array($request['way'])) {
        foreach ($request['way'] as $val) {
            $way = _helper::getWaysiteNameById($val);
            $area .= ',' . $way;
            $result_list .= $way . '/';
        }
    } else {
        $way = _helper::getWaysiteNameById($request['way']);
        $area .= ',' . $way;
        $result_list .= $way . '/';
    }
}


if (!empty($request['station'])) {
    $request['station'] = array_unique($request['station']);
    $aliasStation = _helper::getStationByIds($request['station']);
    foreach ($aliasStation as $val) {
        $area .= ',' . $val['name'];
        $result_list .= $val['name'] . '/';
    }
}


$em = '';
if (!empty($request['em'])) {
    if (is_array($request['em'])) {
        $list_em = implode(',', $request['em']);
    } else {
        $list_em = $request['em'];
    }
    $em = _helper::getEmploymentNamesByListId($list_em);

    if (!empty($em)) {
        $result_list .= str_replace(',', '/', $em) . '/';
    }
}

$job = '';
if (!empty($request['cat'])) {
    if (is_array($request['cat'])) {
        $list_job = implode(',', $request['cat']);
    } else {
        $list_job = $request['cat'];
    }
    $job = _helper::getCategoryNamesByListId('', $list_job);

    if (!empty($job)) {
        $result_list .= str_replace(',', '/', $job) . '/';
    }
}

$hangup = '';
if (!empty($request['hang'])) {
    if (is_array($request['hang'])) {
        $list_hangup = implode(',', $request['hang']);
    } else {
        $list_hangup = $request['hang'];
    }
    $hangup = _helper::getCategoryNamesByListId('', $list_hangup);

    if (!empty($hangup)) {
        $result_list .= str_replace(',', '/', $hangup) . '/';
    }
}

if (empty($result_list)) {
    $result_list = '求人情報';
} else {
    $result_list .= 'の求人情報';
}
?>


<div class="secondary-box">
    <div class="secondary-box__inner">
        <ul class="result-box u-mt15">
            <li class="result-box__list">
                <a href="{{$fullUrl}}#box_job"><span
                            class="result-box__list__category u-fs--m">職種</span><span
                            class="result-box__list__job u-fs--m u-fwb">{{$job}}</span><span
                            class="result-box__list__change u-fs--xs">変更 &gt;</span></a></li>
            <li class="result-box__list">
                <a href="{{$fullUrl}}#box_area"><span
                            class="result-box__list__category u-fs--m">勤務地</span><span
                            class="result-box__list__job u-fs--m u-fwb">{{$area}}</span><span
                            class="result-box__list__change u-fs--xs">変更 &gt;</span></a></li>
            <li class="result-box__list">
                <a href="{{$fullUrl}}#box_em"><span
                            class="result-box__list__category u-fs--m">雇用形態</span><span
                            class="result-box__list__job u-fs--m u-fwb">{{$em}}</span><span
                            class="result-box__list__change u-fs--xs">変更 &gt;</span></a></li>
            <li class="result-box__list">
                <a href="{{$fullUrl}}#box_hangup"><span
                            class="result-box__list__category u-fs--m">こだわり</span><span
                            class="result-box__list__job u-fs--m u-fwb">{{$hangup}}</span><span
                            class="result-box__list__change u-fs--xs">変更 &gt;</span></a></li>
            <li class="result-box__list">
                <a href="{{$fullUrl}}#box_keyword"><span
                            class="result-box__list__category u-fs--m">キーワード</span><span
                            class="result-box__list__job u-fs--m u-fwb">{{$keyword}}</span><span
                            class="result-box__list__change u-fs--xs">変更 &gt;</span></a></li>
        </ul>
        <p class="result__text__list u-fs--mh u-mt10 u-fwb">{{$result_list}}</p>

        <p class="result__num__block u-mt5">
            <span class="result__num u-fs--xl u-fwb">{{$total or 0}}</span>
            <span class="u-fs--xs">件の求人</span>
        </p>
    </div>
</div>