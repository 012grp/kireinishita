<?php
//Array: 勤務地
$province_list 			= _helper::getProvinceNames();
$requestPro				= isset($request['pro'])?$request['pro']:'';

//Cities when selected province
$citiesBySelectedProvince = _helper::getCitiesByProvince($requestPro);

$requestCity = isset($request['city'])?
					_common::changeToArray($request['city'])
					:
					[];

//Waysites when selected province
$waysitesBySelectedProvince = _helper::getWaysitesByProvince($requestPro);

$requestWaysite = isset($request['way'])?
					_common::changeToArray($request['way'])
					:
					[];

$requestStation = isset($request['station'])?
					_common::changeToArray($request['station'])
					:
					[];
//雇用形態
$employment_list 		= _helper::getEmployment();
//change employ to Array if not
$requestEm = isset($request['em'])?
				_common::changeToArray($request['em'])
				:
				[];

//職種
$job_category 			= _helper::getJobCategory();
//change category to Array if not
$requestCat = isset($request['cat'])?
					_common::changeToArray($request['cat'])
					:
					[];
//こだわり
$hangup_keywords 		= _helper::getHangUp();
//change category to Array if not
$requestHang = isset($request['hang'])?
				_common::changeToArray($request['hang'])
				:
				[];

$rq = new Illuminate\Http\Request();
$rq->replace($request);
$totalJob = _helper::countOfSearch($rq);

?>


<input type="hidden" id="requestWaysite" value="{{implode(',',$requestWaysite)}}">
<input type="hidden" id="requestStation" value="{{implode(',',$requestStation)}}">
<form action="/job/result" id="searchForm">
<div class="secondary-box">
    <div class="secondary-box__inner">
        <h1 class="u-fs--m u-fwb secondary-box__ttl u-mt15">求人検索</h1>
        <div class="search-box u-mt15">
                <div class="search-box__ttl u-fs--m" id="box_job">職種</div>
                <div class="search-box__contents">
                   	@foreach($job_category as $groupName => $group)
                   	<div class="search-box__contents__ttl u-fs--xs {{$groupName=='美容'?'':'u-mt20'}}">{{$groupName}}</div>
                   	<div class="search-box__contents__form u-fs--xs">
						@foreach($group as $item)
						<input {{isset($requestCat[$item['id']])?'checked':''}} type="checkbox" value="{{$item['id']}}" name="cat[]" id="cat_check{{$item['id']}}" class="search-checkbox">
                        <label for="cat_check{{$item['id']}}" class="search-checkbox__label u-mt15">{{$item['name']}}</label>
						@endforeach 
					</div>
                   	@endforeach
                </div>
                <div class="search-box__ttl u-fs--m u-mt15" id="box_area">勤務地</div>
                <div class="search-box__contents">
                    <div class="quick-search__select c-icon__wrapper--triangle">
                        <select id="pro_select" name="pro" data-trigger-location="trigger" class="quick-search__select u-fs--m">
                            <option value="">都道府県</option>
                            @foreach($province_list as $item)
								@if ($item['id']==$requestPro)
									<option value="{{$item['id']}}" selected>{{$item['name']}}</option>
								@else
									<option value="{{$item['id']}}">{{$item['name']}}</option>
								@endif
							@endforeach 
                        </select>
                    </div>
                    
                    <div class="search-boxselect__detail u-mt10">
                  		<div data-modal-trigger="prefmodal" class="c-button-square--ms--gray is-disable u-fs--xs">市区町村</div>
                  		<div data-modal-trigger="waysidemodal" class="c-button-square--ms--gray is-disable u-fs--xs">沿線・駅を選択</div>
                	</div>
                    <div data-target-addtag="pref" class="check__tag__addblock">
                    @foreach($citiesBySelectedProvince as $city)
						@if(isset($requestCity[$city['id']]))
							<span class="c-tag--detail--table">{{$city['name']}}</span>
						@endif
					@endforeach
                    </div>
                	<div data-target-addtag="wayside" class="check__tag__addblock">
                	 @foreach($waysitesBySelectedProvince as $waysite)
						@if(isset($requestWaysite[$waysite['line_cd']]))
							<span class="c-tag--detail--table">{{$waysite['name']}}</span>
						@endif
					 @endforeach
                	</div>
                </div>
                
                <div class="search-box__ttl u-fs--m u-mt15" id="box_em">雇用形態</div>
                <div class="search-box__contents search-box__contents--noTitle">
                	<div class="search-box__contents__form u-fs--xs">
                	@foreach($employment_list as $item)
						<input {{isset($requestEm[$item['id']])?'checked':''}} type="checkbox" value="{{$item['id']}}" name="em[]" id="em_check{{$item['id']}}" class="search-checkbox">
                        <label for="em_check{{$item['id']}}" class="search-checkbox__label u-mt15">{{$item['name']}}</label>
					@endforeach 
					</div>
				</div>
                
                <div class="search-box__ttl u-fs--m u-mt15" id="box_hangup">こだわり</div>
                <div class="search-box__contents">
                    @foreach($hangup_keywords as $groupName => $group)
                   	<div class="search-box__contents__ttl u-fs--xs {{$groupName=='応募条件'?'':'u-mt20'}}">{{$groupName}}</div>
                   	<div class="search-box__contents__form u-fs--xs">
						@foreach($group as $item)
						<input {{isset($requestHang[$item['id']])?'checked':''}} type="checkbox" value="{{$item['id']}}" name="hang[]" id="hang_check{{$item['id']}}" class="search-checkbox">
                        <label for="hang_check{{$item['id']}}" class="search-checkbox__label u-mt15">{{$item['name']}}</label>
						@endforeach 
					</div>
                   	@endforeach
                </div>
                <div class="search-box__ttl u-fs--m u-mt15" id="box_keyword">キーワード</div>
                <div class="search-box__contents">
                    <div class="search--text__wrap">
                        <input id="search_keyword" type="text" class="search--text" name="k" value="{{ $request['k'] or '' }}">
                    </div>
                    <!--label.search--text__labeli.c-icon--search-->
                </div>
            
        </div>
    </div>
</div>
<div class="search__button__block">
    <button type="submit" class="c-button-square--m u-fs--l"><span class="button-inner" id="btnAdvanceSubmit">{{$totalJob}}件を検索</span></button>
</div>

<div data-remodal-id="prefmodal" data-search-id="pref" class="modal prefmodal">
	<h1 class="modal__ttl u-fs--l">市区町村</h1>
	<div class="modal__inner u-fs--xs">
	  <div id="city_checkboxes" class="modal__checkbox">
	  @foreach($citiesBySelectedProvince as $city)
		<?php 
			$checked = ''; 
			if(isset($requestCity[$city['id']]))
			{
				$checked = 'checked';
			}
		?>
		<input {{$checked}} value="{{$city['id']}}" form="searchForm" type="checkbox" name="city[]" id="city{{$city['id']}}" data-num="{{$city['id']}}" class="search-checkbox">
		<label for="city{{$city['id']}}" class="search-checkbox__label--modal u-mt10">{{$city['name']}}</label>
		@endforeach 
	  </div>
	  <div class="modal__btn">
		<div data-remodal-action="close" class="c-button-square--mm close_modal_button">閉じる</div>
	  </div>
	</div>
</div>

<div data-remodal-id="waysidemodal" data-search-id="wayside" class="modal waysidemodal">
	<h1 class="modal__ttl modal__ttl--fix u-fs--l">沿線・駅</h1>
	<div class="modal__inner u-fs--xs">
	  	<div id="waysite_checkboxes" class="modal__checkbox waysite_checkbox"></div>
	</div>
	<div class="modal__btn modal__btn--fixed">
	  	<div data-remodal-action="close" class="c-button-square--mm">閉じる</div>
	</div>
</div>
</form>