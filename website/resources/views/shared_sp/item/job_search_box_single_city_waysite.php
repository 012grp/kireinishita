<?php
//Array: 勤務地
$province_list 			= _helper::getProvinceNames();
$requestPro				= isset($request['pro'])?$request['pro']:'';

//Cities when selected province
$citiesBySelectedProvince = _helper::getCitiesByProvince($requestPro);

$requestCity = isset($request['city'])?is_array($request['city'])?$request['city'][0]:$request['city']:'';


//Waysites when selected province
$waysitesBySelectedProvince = _helper::getWaysitesByProvince($requestPro);

$requestWaysite = isset($request['way'])?is_array($request['way'])?$request['way'][0]:$request['way']:'';

//雇用形態
$employment_list 		= _helper::getEmployment();
//change employ to Array if not
$requestEm = isset($request['em'])?
				_common::changeToArray($request['em'])
				:
				[];

//職種
$job_category 			= _helper::getJobCategory();
//change category to Array if not
$requestCat = isset($request['cat'])?
					_common::changeToArray($request['cat'])
					:
					[];
//こだわり
$hangup_keywords 		= _helper::getHangUp();
//change category to Array if not
$requestHang = isset($request['hang'])?
				_common::changeToArray($request['hang'])
				:
				[];

$rq = new Illuminate\Http\Request();
$rq->replace($request);
$totalJob = _helper::countOfSearch($rq);

?>

@section('js')
<script src="/public/sp/js/advance_search.js"></script>
<script src="/public/sp/js/count_search.js"></script>
@stop

<form action="/job/result">
<div class="secondary-box">
    <div class="secondary-box__inner">
        <h1 class="u-fs--m u-fwb secondary-box__ttl u-mt15">求人検索</h1>

        <div class="search-box u-mt15">
            
                <div class="search-box__ttl u-fs--m" id="box_job">職種</div>
                <div class="search-box__contents">
                   	@foreach($job_category as $groupName => $group)
                   	<div class="search-box__contents__ttl u-fs--xs {{$groupName=='調理'?'':'u-mt20'}}">{{$groupName}}</div>
                   	<div class="search-box__contents__form u-fs--xs">
						@foreach($group as $item)
						<input {{isset($requestCat[$item['id']])?'checked':''}} type="checkbox" value="{{$item['id']}}" name="cat[]" id="cat_check{{$item['id']}}" class="search-checkbox">
                        <label for="cat_check{{$item['id']}}" class="search-checkbox__label u-mt15">{{$item['name']}}</label>
						@endforeach 
					</div>
                   	@endforeach
                </div>
                <div class="search-box__ttl u-fs--m u-mt15" id="box_area">勤務地</div>
                <div class="search-box__contents">
                    <div class="quick-search__select c-icon__wrapper--triangle">
                        <select id="pro_select" name="pro" data-trigger-location="trigger" class="quick-search__select u-fs--m">
                            <option value="">都道府県</option>
                            @foreach($province_list as $item)
								@if ($item['id']==$requestPro)
									<option value="{{$item['id']}}" selected>{{$item['name']}}</option>
								@else
									<option value="{{$item['id']}}">{{$item['name']}}</option>
								@endif
							@endforeach 
                        </select>
                    </div>
                    <div class="search-boxselect__detail u-mt10">
                        <select id="city_select" 
                                name="city" data-target-location="target"
                                class="c-button-square--ms--gray
                                		{{empty($citiesBySelectedProvince)?'is-noClick':''}} 
                                		u-fs--xs">
                            <option value="">市区町村</option>
                            @foreach($citiesBySelectedProvince as $item)
								@if ($item['id']==$requestCity)
									<option value="{{$item['id']}}" selected>{{$item['name']}}</option>
								@else
									<option value="{{$item['id']}}">{{$item['name']}}</option>
								@endif
							@endforeach 
                        </select>
                        <select id="waysite_select"
                               	name="way" data-target-location="target"
                                class="c-button-square--ms--gray 
                                {{empty($waysitesBySelectedProvince)?'is-noClick':''}} 
                                u-fs--xs">
                            <option value="">沿線・駅を選択</option>
                            @foreach($waysitesBySelectedProvince as $item)
								@if ($item['line_cd']==$requestWaysite)
									<option value="{{$item['line_cd']}}" selected>{{$item['name']}}</option>
								@else
									<option value="{{$item['line_cd']}}">{{$item['name']}}</option>
								@endif
							@endforeach 
                        </select>
                    </div>
                </div>
                
                <div class="search-box__ttl u-fs--m u-mt15" id="box_em">雇用形態</div>
                <div class="search-box__contents">
                	<div class="search-box__contents__form u-fs--xs">
                	@foreach($employment_list as $item)
						<input {{isset($requestEm[$item['id']])?'checked':''}} type="checkbox" value="{{$item['id']}}" name="em[]" id="em_check{{$item['id']}}" class="search-checkbox">
                        <label for="em_check{{$item['id']}}" class="search-checkbox__label u-mt15">{{$item['name']}}</label>
					@endforeach 
					</div>
				</div>
                
                <div class="search-box__ttl u-fs--m u-mt15" id="box_hangup">こだわり</div>
                <div class="search-box__contents">
                    @foreach($hangup_keywords as $groupName => $group)
                   	<div class="search-box__contents__ttl u-fs--xs {{$groupName=='応募条件'?'':'u-mt20'}}">{{$groupName}}</div>
                   	<div class="search-box__contents__form u-fs--xs">
						@foreach($group as $item)
						<input {{isset($requestHang[$item['id']])?'checked':''}} type="checkbox" value="{{$item['id']}}" name="hang[]" id="hang_check{{$item['id']}}" class="search-checkbox">
                        <label for="hang_check{{$item['id']}}" class="search-checkbox__label u-mt15">{{$item['name']}}</label>
						@endforeach 
					</div>
                   	@endforeach
                </div>
                <div class="search-box__ttl u-fs--m u-mt15" id="box_keyword">キーワード</div>
                <div class="search-box__contents">
                    <div class="search--text__wrap">
                        <input id="search_keyword" type="text" class="search--text" name="k" value="{{ $request['k'] or '' }}">
                    </div>
                    <!--label.search--text__labeli.c-icon--search-->
                </div>
            
        </div>
    </div>
</div>
<div class="search__button__block">
    <button type="submit" class="c-button-square--m u-fs--l"><span class="button-inner" id="btnAdvanceSubmit">{{$totalJob}}件を検索</span></button>
</div>
</form>