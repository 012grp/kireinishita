<?php 
$user = _helper::get_info();
$userInfo = isset($user['email']) ? $user['email'] : $user['nick_name'];
?>
<!--.primary-box--noPadding-->
<div class="secondary-box">
	<div class="secondary-box__topBlock">
		<h1 class="secondary__ttl--side u-fs--m u-fwb u-mt15">お気に入りBOX</h1>

		<div class="secondary__side u-mt15"><span class="result__num u-fs--xl u-fwb" id="totalJobBookmark">{{$totalBookmark or '00'}}</span><span
				class="u-fs--xs">件キープしています</span></div>
	</div>
	<p class="secondary-box__sub__text u-fs--xs u-mt10"><i class="c-icon--people"></i>
		<span class="u-fs--xs">{{$userInfo}}</span>さん
	</p>
</div>