<?php 
	$_count = isset($pageAmount)?$pageAmount:10;
?>
<div class="pager">
	<ul class="pager__lists">
		@if ($currentPage > 1)
		<li class="pager__list u-fs--xs"><a href="{{ $url_prev }}">&lt; 前の{{$_count}}件</a></li>
		@else
		<li class="pager__list is-disable u-fs--xs">&lt; 前の{{$_count}}件</li>
		@endif
		
		
		<li class="pager__list__num u-fs--m">{{$currentPage}}/{{$pageCount}}</li>
		
		@if($pageCount > $currentPage)
		<li class="pager__list u-fs--xs"><a href="{{ $url_next }}">次の{{$_count}}件 &gt;</a></li>
		@else
		<li class="pager__list is-disable u-fs--xs">次の{{$_count}}件 &gt;</li>
		@endif
	</ul>
</div>