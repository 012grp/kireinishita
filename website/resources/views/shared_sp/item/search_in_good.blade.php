<?php
$jobCategoryLevel1 = _helper::getJobCategoryLevel1GroupByParentId();
//応募条件
$quatifications = $jobCategoryLevel1[50];
//待遇
$treatment = $jobCategoryLevel1[51];
//特徴
$characteristic = $jobCategoryLevel1[52];

$prefix_link = '/job/result?hang=';
?>
<div data-target-tabinner="target" class="tab__block__inner is-disable">
    <ul class="tab__block--top">
        <li class="tab__block__category u-fs--xs"><a
                    href="/feature/qualifications">応募条件</a></li>
        <li>
            <ul class="tab__block__lists u-fwb u-fs--s">
                @foreach($quatifications as $item)
                    <li><a href="{{$prefix_link}}{{$item['id']}}">{{$item['name']}}</a><span
                                class="c-icon--triangle"></span></li>
                @endforeach
                <li></li>
            </ul>
        </li>
    </ul>
    <ul class="tab__block--middle u-mt15">
        <li class="tab__block__category u-fs--xs"><a
                    href="/feature/treatment">待遇</a>
        </li>
        <li>
            <ul class="tab__block__lists u-fwb u-fs--s">
                @foreach($treatment as $item)
                    <li><a href="{{$prefix_link}}{{$item['id']}}">{{$item['name']}}</a><span
                                class="c-icon--triangle"></span></li>
                @endforeach
                <li></li>
            </ul>
        </li>
    </ul>
    <ul class="tab__block--bottom u-mt15">
        <li class="tab__block__category u-fs--xs"><a
                    href="/feature/characteristic">特徴</a>
        </li>
        <li>
            <ul class="tab__block__lists u-fwb u-fs--s">
                @foreach($characteristic as $item)
                    <li><a href="{{$prefix_link}}{{$item['id']}}">{{$item['name']}}</a><span
                                class="c-icon--triangle"></span></li>
                @endforeach
                <li>&nbsp;</li>
            </ul>
        </li>
    </ul>
</div>