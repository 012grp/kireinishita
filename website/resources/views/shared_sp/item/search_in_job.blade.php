<?php 
	$jobCategoryLevel1 = _helper::getJobCategoryLevel1GroupByParentId();
	//調理
	$chouri 	= $jobCategoryLevel1[1];
	//接客・販売
	$service 	= $jobCategoryLevel1[2];
	//企画・運営
	$planning 	= $jobCategoryLevel1[3];

	$prefix_link = '/job/result?cat=';
?>
<div data-target-tabinner="target" class="tab__block__inner">
	<ul class="tab__block--top">
		<li class="tab__block__category u-fs--xs"><a href="/biyo">美容</a></li>
		<li>
			<ul class="tab__block__lists u-fwb u-fs--s">
				@foreach($chouri as $item)
				<li><a href="{{$prefix_link.$item['id']}}">{{$item['name']}}</a><span class="c-icon--triangle"></span></li>
				@endforeach
				<li> </li>
			</ul>
		</li>
	</ul>
	<ul class="tab__block--middle u-mt15">
		<li class="tab__block__category u-fs--xs"><a href="/relaxation">リラクゼーション</a></li>
		<li>
			<ul class="tab__block__lists u-fwb u-fs--s">
				@foreach($service as $item)
				<li><a href="{{$prefix_link.$item['id']}}">{{$item['name']}}</a><span class="c-icon--triangle"></span></li>
				@endforeach
			</ul>
		</li>
	</ul>
	<ul class="tab__block--bottom u-mt15">
		<li class="tab__block__category u-fs--xs"><a href="/chiryo">治療</a></li>
		<li>
			<ul class="tab__block__lists u-fwb u-fs--s">
				@foreach($planning as $item)
				<li><a href="{{$prefix_link.$item['id']}}">{{$item['name']}}</a><span class="c-icon--triangle"></span></li>
				@endforeach
			</ul>
		</li>
	</ul>
</div>