<table data-target-tabinner="target" class="_secret__detail__table u-fs--sh--recruit">
    <tbody>
    
    @if(!empty($job_genre))
    <tr>
        <td class="_detail__table__left">ジャンル</td>
        <td class="_detail__table__right">{{$job_genre}}</td>
    </tr>
    @endif

    @if(!empty($list_category))
    <tr>
        <td class="_detail__table__left">職種</td>
        <td class="_detail__table__right">
            @if($list_category)
			<?php _helper::explode_character($list_category); ?>
			@endif
        </td>
        @endif
    </tr>

    @if(!empty($list_em))
    <tr>
        <td class="_detail__table__left">雇用形態</td>
        <td class="_detail__table__right">
            <p class="_detail__table__text">
                @if($list_em)
				<?php _helper::explode_character($list_em); ?>
				@endif
            </p>
        </td>
    </tr>
    @endif

    @if(!empty($job_description))
    <tr>
        <td class="_detail__table__left">仕事内容</td>
        <td class="_detail__table__right">
            <?php _helper::explode_ul($job_description, 'u-mt10'); ?>
        </td>
    </tr>
    @endif

    @if(!empty($job_salary))
    <tr>
        <td class="_detail__table__left">給与</td>
        <td class="_detail__table__right">
            @if($job_salary)
			<?php _helper::explode_ul($job_salary, '', ''); ?>
			@endif
        </td>
    </tr>
    @endif

    @if(!empty($job_qualification))
    <tr>
        <td class="_detail__table__left">応募資格</td>
        <td class="_detail__table__right">
            <?php /*
            <p class="_detail__table__text">
            @if($list_em)
			<?php _helper::explode_character($list_em); ?>
			@endif
            </p>
            */ ?>
            
            @if($job_qualification)
			<?php _helper::explode_ul($job_qualification, 'u-mt10'); ?>
			@endif
        </td>
    </tr>
    @endif

    @if(!empty($job_location))
    <tr>
        <td class="_detail__table__left">勤務地</td>
        <td class="_detail__table__right">
           	@if($job_location)
			<?php _helper::explode_ul($job_location); ?>
			@endif
        </td>
    </tr>
    @endif

    @if(!empty($access))
    <tr>
        <td class="_detail__table__left">アクセス</td>
        <td class="_detail__table__right">
            @if($access)
			<?php _helper::explode_ul($access); ?>
			@endif
           	
           	@if(!empty($detail['lat']) && !empty($detail['lng']) )
			<div class="_detail__table__map u-mt10" id="job_map"></div>
			<a target="_blank" href="https://www.google.com/maps/place/{{$job_location}},+Japan/{{'@'}}{{$detail['lat'] or ''}},{{$detail['lng'] or ''}},17z?hl=ja-JP" id="url_job_map">
			GoogleMapで確認する</a>
			@endif
        </td>
    </tr>
    @endif

    @if(!empty($treatment_welfare))
    <tr>
        <td class="_detail__table__left">待遇・<br>福利厚生</td>
        <td class="_detail__table__right">
            @if($treatment_welfare)
				<?php _helper::explode_ul($treatment_welfare); ?>
			@endif
        </td>
    </tr>
    @endif

    @if(!empty($job_holiday))
    <tr>
        <td class="_detail__table__left">休日休暇</td>
        <td class="_detail__table__right">
        @if($job_holiday)
			<?php _helper::explode_ul($job_holiday); ?>
		@endif
        </td>
    </tr>
    @endif

    @if(!empty($job_personnel))
    <tr>
        <td class="_detail__table__left">求める人材について</td>
        <td class="_detail__table__right">
            @if($job_personnel)
			<?php _helper::explode_ul($job_personnel); ?>
		@endif
        </td>
    </tr>
    @endif

    @if(!empty($job_appSelection))
    <tr>
        <td class="_detail__table__left">応募・選考について</td>
        <td class="_detail__table__right">
        @if($job_appSelection)
			<?php _helper::explode_ul($job_appSelection); ?>
		@endif
        </td>
    </tr>
    @endif

    </tbody>
</table>