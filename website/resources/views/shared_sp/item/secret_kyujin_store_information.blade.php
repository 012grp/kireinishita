<div data-target-tabinner="target" class="_secret__detail__table is-disable" style="border: none">

<h1 class="is-disable" style="font-size: 150%; color: #7e7d7d; font-weight: 700">店舗情報</h1>
<br>

<table data-target-tabinner="target" class="_secret__detail__table u-fs--sh--recruit is-disable">
    <tbody>
        @if(!empty($detail['store_name']))
        <tr>
            <td class="_detail__table__left">店名</td>
            <td class="_detail__table__right">{{$detail['store_name']}}</td>
        </tr>
        @endif
       
        @if(!empty($detail['shopStyle']))
        <tr>
            <td class="_detail__table__left">店舗形態</td>
            <td class="_detail__table__right">{{$detail['shopStyle']}}</td>
        </tr>
        @endif
        
        @if(!empty($detail['address']))
        <tr>
            <td class="_detail__table__left">事務所</td>
            <td class="_detail__table__right">{{$detail['address']}}</td>
        </tr>
        @endif
        
        @if(!empty($detail['business_hrs']))
        <tr>
            <td class="_detail__table__left">営業時間</td>
            <td class="_detail__table__right">{!! nl2br($detail['business_hrs']) !!}</td>
        </tr>
        @endif
        
        @if(!empty($detail['reg_holiday']))
        <tr>
            <td class="_detail__table__left">定休日</td>
            <td class="_detail__table__right">{!! nl2br($detail['reg_holiday']) !!}</td>
        </tr>
        @endif
        
        @if(!empty($detail['customer_base']))
        <tr>
            <td class="_detail__table__left">客層</td>
            <td class="_detail__table__right">{!! nl2br($detail['customer_base']) !!}</td>
        </tr>
        @endif
        
        @if(!empty($detail['job_atmosphere']) OR !empty($detail['job_education']) OR !empty($detail['job_challenges']))
        <tr>
            <td class="_detail__table__left">よくあるご質問</td>
            <td class="_detail__table__right">
            
            <?php if (!empty($detail['job_atmosphere'])){ ?>
                <h1 class="faq_subheader u-fwb">職場の雰囲気について</h1>
            <?php } ?>
            {!! nl2br($detail['job_atmosphere']) !!}

            <?php if (!empty($detail['job_education'])){ ?>
                <h1 class="faq_subheader u-fwb">スキルアップや教育制度について</h1>
            <?php } ?>
            {!! nl2br($detail['job_education']) !!}               
                            
            <?php if (!empty($detail['job_challenges'])){ ?>
                <h1 class="faq_subheader u-fwb">仕事のやりがいについて</h1>
            <?php } ?>
            {!! nl2br($detail['job_challenges']) !!}
            </td>
        </tr>
        @endif

        @if(!empty($detail['male_female_ration']))
        <tr>
            <td class="_detail__table__left">スタッフ数や男女比</td>
            <td class="_detail__table__right">{!! nl2br($detail['male_female_ratio']) !!}</td>
        </tr>
        @endif
        
        @if(!empty($detail['job_url']))
        <tr>
            <td class="_detail__table__left">URL</td>
            <td class="_detail__table__right"><a href="{{$detail['job_url']}}" target="_blank">{{$detail['job_url']}}</a></td>
        </tr>
        @endif
    </tbody>
</table>


@if($status == 1)

<br><br><br>
<h1 class="is-disable" style="font-size: 150%; color: #7e7d7d; font-weight: 700">企業情報</h1>
<br>

<table data-target-tabinner="target" class="_secret__detail__table u-fs--sh--recruit is-disable">

    <tbody>

    @if(!empty($detail['name']))
    <tr>
        <td class="_detail__table__left">企業な</td>
        <td class="_detail__table__right">{{$detail['name']}}</td>
    </tr>
    @endif

    @if(!empty($detail['representative']))
    <tr>
        <td class="_detail__table__left">代表者名</td>
        <td class="_detail__table__right">{{$detail['representative']}}</td>
    </tr>
    @endif

    @if(!empty($detail['work_content']))
    <tr>
       <td class="_detail__table__left u-fwb">事業内容</td>
        <td class="_detail__table__right">{{$detail['work_content']}}</td>
    </tr>
    @endif

    @if(!empty($detail['map_location']))
    <tr>
        <td class="_detail__table__left u-fwb">本社所在地</td>
        <td class="_detail__table__right">{{$detail['map_location']}}</td>
    </tr>
    @endif

    @if(!empty($detail['website']))
    <tr>
        <td class="_detail__table__left u-fwb">HP</td>
        <td class="_detail__table__right"><a href="{{$detail['website']}}" target="_blank">{{$detail['website']}}</a></td>
    </tr>
    @endif
</table>

</div>


@endif