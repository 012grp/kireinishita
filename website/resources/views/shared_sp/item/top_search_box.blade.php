<?php 
	$province_list 		= _helper::getProvinceNames();
	$employment_list 	= _helper::getEmployment();
?>
<div class="topSearch-box u-mt25">
    <form class="quick-search" action="/job/result">
        <div class="quick-search__inner">
            <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                <select name="pro" class="quick-search__select u-fs--m">
                    <option value="" selected>都道府県</option>
                    @foreach($province_list as $province)
						<option value="{{$province['id']}}">{{$province['name']}}</option>
					@endforeach 
                </select>
            </div>
            <div class="quick-search__select__wrap c-icon__wrapper--triangle">
                <select name="em" class="quick-search__select u-fs--m">
                    <option value="" selected>雇用形態</option>
                   @foreach($employment_list as $em)
					<option value="{{$em['id']}}">{{$em['name']}}</option>
				   @endforeach 
                </select>
            </div>
        </div>
        <button type="submit" class="c-button-square--top u-fs--l u-mt10"><span class="button-inner">カンタン検索</span>
        </button>
    </form>
</div>