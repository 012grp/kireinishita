<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
<head>
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
	<title>test search</title>
	<style>
		select{
			min-width: 150px;
		}
		.show{
			width: 100%;
			border-collapse: collapse;
		}
		.show td{
			padding: 5px 10px;
			border-bottom: 1px solid #ccc;
		}
		.total{
			padding: 10px;
		}
		
		.modal__sub__ttl{
			padding: 10px;
			text-align: left;
			color: #7e7d7d;
			border-radius: 2px;
			background-color: #e5e5e5;
		}
		
		.modal__checkbox {
			padding: 0 10px;
			text-align: left;
			color: #7e7d7d;
			margin: 10px 0;
		}
		
		.search-checkbox__label--modal {
			display: inline-block;
			width: 150px;
			font-size: 12px;
		}
		
		.modal__sub__ttl .search-checkbox__label--modal{
			width: auto;
		}
		
		#line-group{
			max-height: 300px;
			overflow-y: auto;
		}
	</style>
</head>
<body>
	<h1>Search</h1>
	<form action="/job/api" method="get">
		<nav>
			<p><a href="/job/api">条件リセット</a></p>
			都道府県
			<select id="pro" name="pro">
						<option value="0" selected>都道府県</option>
						<option value="1">北海道	</option>
						<option value="2">青森県	</option>
						<option value="3">岩手県	</option>
						<option value="4">宮城県	</option>
						<option value="5">秋田県	</option>
						<option value="6">山形県	</option>
						<option value="7">福島県	</option>
						<option value="8">茨城県	</option>
						<option value="9">栃木県	</option>
						<option value="10">群馬県	</option>
						<option value="11">埼玉県	</option>
						<option value="12">千葉県	</option>
						<option value="13">東京都	</option>
						<option value="14">神奈川県</option>
						<option value="15">新潟県	</option>
						<option value="16">富山県	</option>
						<option value="17">石川県	</option>
						<option value="18">福井県	</option>
						<option value="19">山梨県	</option>
						<option value="20">長野県	</option>
						<option value="21">岐阜県	</option>
						<option value="22">静岡県	</option>
						<option value="23">愛知県	</option>
						<option value="24">三重県	</option>
						<option value="25">滋賀県	</option>
						<option value="26">京都府	</option>
						<option value="27">大阪府	</option>
						<option value="28">兵庫県	</option>
						<option value="29">奈良県	</option>
						<option value="30">和歌山県</option>
						<option value="31">鳥取県	</option>
						<option value="32">島根県	</option>
						<option value="33">岡山県	</option>
						<option value="34">広島県	</option>
						<option value="35">山口県	</option>
						<option value="36">徳島県	</option>
						<option value="37">香川県	</option>
						<option value="38">愛媛県	</option>
						<option value="39">高知県	</option>
						<option value="40">福岡県	</option>
						<option value="41">佐賀県	</option>
						<option value="42">長崎県	</option>
						<option value="43">熊本県	</option>
						<option value="44">大分県	</option>
						<option value="45">宮崎県	</option>
						<option value="46">鹿児島県</option>
						<option value="47">沖縄県	</option>
					</select>
			<button type="submit">検索</button>
			
			<p>線区・駅情報 </p> 
			<div id="line-group"></div>
		</nav>
	<hr>
	<div class="total">{{$total<50?$total:50}}/{{$total}}件の求人</div>
	<hr>
	<table class="show">
		<tr>
			<th>ID</th>
			<th>URL</th>
			<th style="width: 40%">Name</th>
			<th>Location</th>
		</tr>
		@foreach($contents as $content)
		<tr>
			<td>{{$content->id}}</td>
			<td><a href="{{$content->job_url}}" target="_blank">{{$content->job_url}}</a></td>
			<td>{{$content->job_name}}</td>
			<td>{!!$content->job_location!!}</td>
		</tr>
		@endforeach 
		
	</table>
	
	<div id="group_template" style="display:none">
			<div class="modal__sub__ttl">@line</div>
			<div class="modal__checkbox">@station</div>
	</div>
	<div id="checkbox_template" style="display:none">
		
		<label class="search-checkbox__label--modal">
			<input type="checkbox" name="@name" class="search-checkbox" value="@value">@text
		</label>
	</div>
	
	</form>
</body>

<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>

<script type="text/javascript">
	
	var groupTemplate = $('#group_template').html();
	var checkboxTemplate = $('#checkbox_template').html();
	

	//internal
	function createAGroup(line)
	{
		var line_checkbox = checkboxTemplate.replace('@text', line['name'])
											.replace(/@value/g, line['code'])
											.replace('@name', 'way[]');
		
		var station_checkboxes = '';
		var len = line['stations'].length;
		for(var i=0; i<len; i++)
		{
			var station = line['stations'][i];
			station_checkboxes += checkboxTemplate.replace('@text', station['name'])
											.replace(/@value/g, station['code'])
											.replace('@name', 'station[]');
		}
		
		var group =  groupTemplate.replace('@line', line_checkbox)
					 		.replace('@station', station_checkboxes);
		
		return group;
	}
	
	//internal
	function createAModal(lines)
	{
		var html = '';
		for(var i in lines)
		{
			html += createAGroup(lines[i]);
		}
		return html;
	}
	
	//method
	function getLine(id)
	{
		$('#line-group').html('');
		if(id<1||id==""){
			return;
		}
		$.get('/train-api/line-station/'+id, function(lines){
			if(lines !== undefined)
			{
				$('#line-group').html(createAModal(lines));
				chkStationEvent();
			}
		});
	}
	
	//declare event for input[name"station[]"]
	function chkStationEvent()
	{
		$('input[name="station[]"]').change(function () {
			var val = $(this).val();
			$('input[name="station[]"][value="'+val+'"]').prop("checked", this.checked);
		});
	}
	

	function findGetParameter(parameterName) {
		var result = [],
			tmp = [];
		location.search
		.substr(1)
			.split("&")
			.forEach(function (item) {
			tmp = item.split("=");
			if (tmp[0] === parameterName) {
				result.push(decodeURIComponent(tmp[1]))
			};
		});
		if(result)
		{
			return result.length==1?result[0]:result;
		}
		return '';
	}
	
	//loadding
	$('#pro').change(function () {
		getLine($('#pro').val());
	})

	var pro = findGetParameter('pro');
	if(pro != "")
	{
		$('#pro').val(pro);	
		getLine(pro);
		var way = findGetParameter('way%5B%5D');
		var station = findGetParameter('station%5B%5D');
		var checkOn = function(values,name){
			if(values){
				if(typeof values === 'string')
				{
					$(name+'[value="'+values+'"]').prop('checked',true);
				}
				else
				{
					values.forEach(function (value) {
						$(name+'[value="'+value+'"]').prop('checked',true);
					});
				}
			}
		}
		setTimeout(function(){
			checkOn(way, 'input[name="way[]"]');
			checkOn(station, 'input[name="station[]"]');
		}, 1000);
		
	}

	

	</script>
</html>