@extends('layouts.default')
@section('content')
<div class="form-panel">
<div class="wp-panel">
    <div class="clearfix"></div>
    <span class="h1">Bookmark</span>
    <table id="bookmark" class="table table-list">
        <tr>
            <th class="s1">Id</th>
            <th>Job name</th>
            <th>Company name</th>
            <th class="s3"></th>
        </tr>
        
        @foreach($bookmarks as $x)
            <tr id="bk-{!! $x->id !!}">
                <td>{!! $x->id !!}</td>
                <td>
                    <div class="text-left">
                    <a target="_blank" href="{!! $x->job_url !!}">{!! $x->job_name !!}</a>
                    </div>
                    
                </td>
                <td>{!! $x->job_company !!}</td>
                <td>
                    <span class="unmark detail_button" id="{!! $x->id !!}">Unmark</span>
                </td>
            </tr>
        @endforeach 
    </table>
    <script type="text/javascript">
        $(function() {
            $('#bookmark .unmark').on('click', function () {
                if(confirm('are you sure unmark this job ?'))
                {
                    var id = $(this).attr('id');
                    $.get('/bookmark/unmark?id='+id, function (result) {
                        result==1&&$('#bk-'+id).empty()||alert('can not unmark this job');
                    });
                }
            });
        });
    </script>
</div>
</div>
@stop