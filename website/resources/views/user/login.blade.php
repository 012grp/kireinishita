@extends('layouts.default')
@section('content')
<div class="form-panel">
<div class="wp-panel">
<div class="contain-fluid">
   <div>
    @if($errors->any())
        <h4>{{$errors->first()}}</h4>
    @endif
   </div>
    <table id="login" class="table table-top">
        <tr>
            <td>
                
                <span class="h3">Sign in</span>
                <div class="social">
                    <a class="facebook" href="{!!URL::to('facebook')!!}">Login with Facebook</a>
                    <a class="twitter" href="{!!URL::to('twitter')!!}">Login with Twitter</a>
                    <a class="google" href="{!!URL::to('google')!!}">Login with Google</a>
                </div>
                
                <div class="form-style form-full normal-login">
                    {!! Form::open(array('url' => '/account/email', 'method' => 'post')) !!}
                        <div class="input-group"><input type="text" name="email" placeholder="email" required></div>
                        <div class="input-group"><input type="password" name="password" placeholder="password" required></div>
                        <div class="button-group">
                        <button type="submit" class="button btn-primary">Login</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </td>
            <td></td>
            <td>
               <div class="sign-up">
                <span class="h3">Sign up</span>
                <div class="form-style">
                {!! Form::open(array('url' => '/account/register', 'method' => 'post')) !!}
                    <div>
                        <input type="password" hidden class="hidden">
                        <div class="input-group"><label for="name">Name</label><input type="text" name="name" id="name" placeholder="name"></div>
                        <div class="input-group"><label for="email">Email</label><input type="text" name="email" id="email" placeholder="email"></div>
                        <div class="input-group"><label for="password">Password</label><input type="password" name="password" id="password" placeholder="password"></div>                        
                    </div>
                    <div class="button-group group-style">
                        <button class="button btn-primary">Regiter</button>
                    </div>
                {!! Form::close() !!}
                </div>
                </div>
            </td>
        </tr>
    </table>
</div>
</div>
</div>
@stop