@extends('layouts.default')
@section('content')
<div class="form-panel">
<div class="wp-panel">
<div class="form-layout">
   <div>
    @if($errors->any())
                <h4>{{$errors->first()}}</h4>
    @endif
   </div>
    <table class="table table-top">
        <tr>
            <td>
                <span class="h3">Profile</span>
                <div class="form-style">
                {!! Form::open(array('url' => '/account/edit', 'method' => 'post')) !!}
                    <div>
                        <div class="input-group"><label for="name">Name</label><input type="text" name="name" id="name" placeholder="name" 
                        required
                        value="{!! $user->name !!}"
                        ></div>
                        <div class="input-group"><label for="email">Email</label><span>{{ $user->email }}</span></div>           
                    </div>
                    <div class="button-group group-style">
                        <button class="button btn-primary">Edit</button>
                    </div>
                {!! Form::close() !!}
                </div>
            </td>
            <td>
                <span class="h3">Change password</span>
                <div class="form-style">
                {!! Form::open(array('url' => '/account/changepassword', 'method' => 'post')) !!}
                    <div>
                        @if(!empty($user->password))
                            <div class="input-group"><label for="password">Old PW</label><input type="password" name="oldPassword" id="oldPassword" placeholder="Old password" required></div>    
                        @endif        
                                
                        <div class="input-group"><label for="password">New PW</label><input type="password" name="newPassword" id="newPassword" placeholder="New password" required></div>                        
                        <div class="input-group"><label for="password">Repeat PW</label><input type="password" name="repeatNewPassword" id="repeatNewPassword" placeholder="Repeat new password" required></div>                        
                    </div>
                    <div class="button-group group-style">
                        <button class="button btn-primary">Change</button>
                    </div>
                {!! Form::close() !!}
                </div>
            </td>
        </tr>
    </table>
</div>
</div>
</div>
@stop