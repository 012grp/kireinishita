<?php 
	header('Content-Type: text/html; charset=utf-8');
	require_once ('mysql_connect.php');
	$k = '';
	if(isset($_GET['keyword']) && !empty($_GET['keyword']))
	{
		$k = $_GET['keyword'];
		$query  = "	select a.url, c.* 
					from contents c join available_urls a on c.urlId = a.id 
					where 
						c.job_name like '%$k%' 
						or c.job_qualification like '%$k%' 
						or c.job_salary like '%$k%' 
						or c.job_address like '%$k%'
					limit 0,50";
		$data = @mysql_query($query);
	}
	
	function replaceAdvance($content, $keyword)
	{
		$rpKeyword = "<span class=\"key_word\">$keyword</span>";
		$strReplaced =  str_replace( 	array($keyword, '■',	'◆', '【', '★'), 
										
										array($rpKeyword, '<br>■ ', '<br>◆ ', '<br><br>【' , '<br>★ '), 
								
										$content
									);
		return $strReplaced;
	}
	
	function listJob($start, $limit) {
		//REPLACE(c.job_image,'<img src=\"','<img src=\"https://employment.en-japan.com') as c.job_image  
		$query  = "	select a.url, c.*
					from contents c join available_urls a on c.urlId = a.id 
					order by job_image DESC 
					limit $start, $limit";
		return @mysql_query($query);
	}
	
	function countJob() {
		$query  = "	select a.url  
					from contents c join available_urls a on c.urlId = a.id";		 
		return mysql_num_rows(@mysql_query($query));
	}
	
class Pagination{
  
	public $limit = 10; // số record hiển thị trên một trang
	protected $_baseUrl;
  
	public function __construct(){
		$this->_baseUrl = $_SERVER['PHP_SELF'];
	}
 /**
   - Tìm ra vị trí start
 */
	public function start(){
		if(isset($_GET['start'])){
			$start = $_GET['start'];
		}else{
			$start = 0;
		}
		return $start;
	}
  
 /**
   - Tìm ra tổng số trang
 */
	public function totalPages($totalRecord){
		if(isset($_GET['pages'])){
			$totalPages = $_GET['pages'];
		}else{
			$totalPages = ceil($totalRecord/$this->limit);
		}
		return $totalPages;
	}
  
 /**
   - Gọi ra list phân trang
 */
	public function listPages($totalPages){
		$start = $this->start();
		$limit = $this->limit;
		$listPage = '';
		$limitNave = 10;

		if($totalPages > 1){ // số trang phải từ 2 trang trở lên
			$current = ($start/$limit) + 1; // trang hiện tại
			if($current != 1){ // Nút prev
				$newstart = $start - $limit;
				$listPage .= "<li><a href='".$this->_baseUrl."?pages=$totalPages&start=$newstart'>Prev</a></li>";
			}	
			$startShow = 1;
			if ($current >= $limitNave-3)
				$startShow = $current - $limitNave + 3;

			$endShow = $current + $limitNave;
			if ($endShow > $totalPages)
				$endShow = $totalPages;
			
			for($i=$startShow;$i<=$endShow;$i++){  // Tất cả các trang tìm được
				$newstart = ($i - 1)*$limit;
				if($i == $current){
					$listPage .= "<li class='active'>".$i."</li>";
				}else{
					$listPage .= "<li><a href='".$this->_baseUrl."?pages=$totalPages&start=$newstart'>".$i."</a></li>";
				}
			}
    
			if($current != $totalPages){ // Nút next
				$newstart = $start + $limit;
				$listPage .= "<li><a href='".$this->_baseUrl."?pages=$totalPages&start=$newstart'>Next</a></li>";
			}
		}
   
		return $listPage . "( of $totalPages pages)";
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Web scrawler</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="header fixed_top search_header">

                <div class="center_contain">
        <form action="search.php" method="GET">
            <div class="search_control">
                <input type="text" name="keyword" placeholder="search..." value="<?php echo $k; ?>">
                <button>search</button>
            </div>
        </form>
           </div>
    </div>
    <div class="container next_fixed_top">
        <div class="search_content">
            <div class="wp_search">
				<p class="result_msg">
					<strong>
				<?php
					$more = 0;
					if ($k) {												
						$number = mysql_num_rows($data);
						if($number > 0)
						{
							echo "There are $number jobs by $k";
						}
						/*
						else
						{
							echo "Not result by $k";
						}*/
					} else {
						$number = countJob();	
						$Pagination = new Pagination();
						$limit = $Pagination->limit; // Số record hiển thị trên một trang
						$start = $Pagination->start(); // Vị trí của record
						$totalRecord = $number; // Tổng số user có trong database
						$totalPages = $Pagination->totalPages($totalRecord); // Tổng số trang tìm được 
						$data = listJob($start, $limit);		
						echo "There are $number jobs by $k";
						$more = $start;
					}
				?>
					</strong>
				</p>
                <ol>
					<?php 
						//Start loop
						$i = 0;
						while($row = @mysql_fetch_assoc($data)){
							$i++;
					?>  
					
                    <li>
                        <div class="job_item">
							
                            <h3>
                                <a target="_blank" href="<?php echo $row['url']; ?>">
								<?php
									echo "Job " . ($i + $more) . ". " . $row['job_name'];
								?>
                                </a>
                            </h3>
							<div class="job_info">
                            <div class="job_label">
                                応募資格
                            </div>
								<?php
									echo replaceAdvance($row['job_qualification'], $k);
								?>
                            <div class="job_label">
                                給与
                            </div>
								
								<?php
									echo replaceAdvance($row['job_salary'], $k);
								?>
								
                            <div class="job_label">
                                勤務地
                            </div>
								
								<?php									
									echo replaceAdvance($row['job_address'], $k);
								?>
                            </div>
							<div class="job_image">
								 <a target="_blank" href="<?php echo $row['url']; ?>">
									<?php		
										if ($row['id'] > 1503) {
											echo str_replace('<img src="/', '<img src="https://employment.en-japan.com/', $row['job_image']);
										} else {
											echo $row['job_image'];
										}
									?>
								</a>
							</div>
                            <div class="detail_controller">
                                <a class="detail_button" target="_blank" href="<?php echo $row['url']; ?>">詳しい情報を見る</a>
                            </div>
                            
                        </div>
                    </li>
                 <?php 
					//End loop
					}
				 ?>
                </ol>
            </div>
        </div>
        <?php
			if (!$k) {
		?>
        <div class="page_content">
            <ul>
				<?php echo $Pagination->listPages($totalPages); ?>
                <!--<li><a href="/search.html?page=1">Preview</a></li>
                <li class="active"><a href="/search.html">1</a></li>
                <li><a href="/search.html">2</a></li>
                <li><a href="/search.html">3</a></li>
                <li><a href="/search.html">4</a></li>
                <li><a href="/search.html">5</a></li>
                <li><a href="/search.html">6</a></li>
                <li><a href="/search.html">7</a></li>
                <li><a href="/search.html">8</a></li>
                <li><a href="/search.html">9</a></li>
                <li><a href="/search.html">10</a></li>
                <li><a href="/search.html?page=2">Next</a></li>-->
            </ul>
        </div>
			<?php }?>
    </div>
    <div class="footer">
        <div class="search_footer"></div>
    </div>
    
</body>

</html>